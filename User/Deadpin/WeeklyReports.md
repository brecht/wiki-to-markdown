# Weekly Reports

## Week 05 (Dec 04 - Dec 10)

Just over 2 days lost due to electrical contractors needing to come work
on the home.

Collection IO support is lagging behind and we'll be meeting this coming
week to try and get it back on track. Design needs to be rationalized
against the newly added "File Handlers" and the UI location needs to be
confirmed as well.

USD code review is taking up quite a lot of time.

**USD**

  - Reviewing [PR
    \#115076](https://projects.blender.org/blender/blender/pulls/115076):
    USD: import scenegraph instances.
  - Reviewing [PR
    \#115623](https://projects.blender.org/blender/blender/pulls/115623):
    Alembic/USD: use geometry sets to import data
  - Reviewing [PR
    \#113267](https://projects.blender.org/blender/blender/pulls/113267):
    USD: optionally author subdivision schema on export

<!-- end list -->

  - Committed
    [2f0633f5fa](https://projects.blender.org/blender/blender/commit/2f0633f5fa):
    Fix: Send more appropriate notification at end of USD/Alembic import
  - Reviewed [PR
    \#112466](https://projects.blender.org/blender/blender/pulls/112466):
    IO: Add initial support for File Handlers registration

<!-- end list -->

  - In review [PR
    \#115224](https://projects.blender.org/blender/blender/pulls/115224):
    Fix \#106326: Add scale-bias processing for UsdUVTexture

**General**

  - Committed
    [0f67cb9c2f](https://projects.blender.org/blender/blender/commit/0f67cb9c2f)
    Cleanup: Fully remove legacy displacement\_method property
  - Committed
    [4eec817823](https://projects.blender.org/blender/blender-addons/commit/4eec817823):
    Fix: Use correct displacement\_method property
  - Committed
    [dff97f88ed](https://projects.blender.org/blender/blender-addons/commit/dff97f88ed):
    Adjust Real Snow addon to Principled BSDF changes

**Triage (Involved in 12 reports)**

  - Confirmed: 1
  - Closed as Resolved: 1
  - Closed as Duplicate: 1
  - Needs Info from User: 0
  - Needs Info from Developers: 0
  - Actions total: 15

## Week 04 (Nov 27 - Dec 03)

**USD**

  - Reviewing [PR
    \#115076](https://projects.blender.org/blender/blender/pulls/115076):
    USD: import scenegraph instances.
  - Reviewing [PR
    \#113374](https://projects.blender.org/blender/blender/pulls/113374):
    bugfix/usd\_material\_color\_space
  - Reviewing [PR
    \#115623](https://projects.blender.org/blender/blender/pulls/115623):
    Alembic/USD: use geometry sets to import data
  - To help keep track of pending USD work there's
    [USDWorkstreams](USDWorkstreams.md) as well as
    [\#115690](https://projects.blender.org/blender/blender/issues/115690),
    [\#115691](https://projects.blender.org/blender/blender/issues/115691),
    and
    [\#115692](https://projects.blender.org/blender/blender/issues/115692)

<!-- end list -->

  - In review [PR
    \#115224](https://projects.blender.org/blender/blender/pulls/115224):
    Fix \#106326: Add scale-bias processing for UsdUVTexture

**General**

  - Committed
    [abf59eb23a](https://projects.blender.org/blender/blender/commit/abf59eb23ab):
    Selection: Remove limit on number of items which can be selected at
    once

<!-- end list -->

  - [PR
    \#105042](https://projects.blender.org/blender/blender-addons/pulls/105042):
    Fix \#105040: Adjust Real Snow addon to Principled BSDF changes
  - [PR
    \#115738](https://projects.blender.org/blender/blender/pulls/115738):
    Cleanup: Fully remove legacy displacement\_method property

**Triage (Involved in 39 reports)**

  - Confirmed: 5
  - Closed as Resolved: 2
  - Closed as Archived: 22
  - Closed as Duplicate: 6
  - Needs Info from User: 7
  - Needs Info from Developers: 1
  - Actions total: 61

## Week 03 (Nov 20 - Nov 26)

Quick poke of Kevin Dietrich: He says it's still his intent to retry the
Alembic/USD Geometry Set PR sometime soon. Has found a regression since
his last pull of main and wants to fix that first.

**USD**

  - Reviewing [PR
    \#115076](https://projects.blender.org/blender/blender/pulls/115076):
    USD: import scenegraph instances.
  - Reviewing [PR
    \#113374](https://projects.blender.org/blender/blender/pulls/113374):
    bugfix/usd\_material\_color\_space

<!-- end list -->

  - In review [PR
    \#115224](https://projects.blender.org/blender/blender/pulls/115224):
    Fix \#106326: Add scale-bias processing for UsdUVTexture

**General**

  - [PR
    \#115437](https://projects.blender.org/blender/blender/pulls/115437):
    Fix \#115431: Crash during right-click of StringProperty with search

<!-- end list -->

  - In progress [PR
    \#112491](https://projects.blender.org/blender/blender/pulls/112491)
    Selection: Remove limit on number of items which can be selected at
    once

**Triage (Involved in 16 reports)**

  - Confirmed: 3
  - Closed as Resolved: 0
  - Closed as Archived: 5
  - Closed as Duplicate: 1
  - Needs Info from User: 3
  - Needs Info from Developers: 0
  - Actions total: 23

## Week 02 (Nov 13 - Nov 19)

Still working to sort through, catalog, and discuss the ongoing USD work
which is spread across several tasks and several groups of folks.

**USD**

  - Reviewing [PR
    \#114821](https://projects.blender.org/blender/blender/pulls/114821):
    USD: Improved Texture Coordinate Translation (UsdTransform2d)
  - In progress
    [\#106326](https://projects.blender.org/blender/blender/issues/106326):
    UsdPreviewSurface does not respect normals texture bias and scale

<!-- end list -->

  - Pending Sync with Kevin about his Geometry Set changes. Need to
    determine who will attempt another round of updates and review etc.

**General**

  - Fix
    [\#114847](https://projects.blender.org/blender/blender/issues/114847):
    Skip past more newlines when parsing PLY files
    ([3a312babe6](https://projects.blender.org/blender/blender/commit/3a312babe6))
  - Fix: Allow Win32 clipboard to properly handle large images
    ([9eba3902c9](https://projects.blender.org/blender/blender/commit/9eba3902c9))

<!-- end list -->

  - In progress [PR
    \#112491](https://projects.blender.org/blender/blender/pulls/112491)
    Selection: Remove limit on number of items which can be selected at
    once

**Triage (Involved in 26 reports)**

  - Confirmed: 6
  - Closed as Resolved: 1
  - Closed as Archived: 16
  - Closed as Duplicate: 8
  - Needs Info from User: 4
  - Needs Info from Developers: 0
  - Actions total: 43

## Week 01 (Nov 08 - Nov 12)

First, partial, week after officially coming on board.

**USD**

  - Committed
    [360684d5a4](https://projects.blender.org/blender/blender/commit/360684d5a4):
    Fix
    [\#114047](https://projects.blender.org/blender/blender/issues/114047):
    Use correct collection during USD import
  - Reviewed [PR
    \#114728](https://projects.blender.org/blender/blender/pulls/114728)
    Fix: USD: Wrong UsdUVTexture rgb output type
  - Ramp-up / Research / Issue Backlog
      - Resurrected Sonny Campbell's and Charles Wardlaw's work on a
        Collection IO prototype. This will not be checked in as there's
        design feedback to handle first which will fundamentally change
        the code. However, it might be useful to have available to
        demonstrate how tying USD IO to Blender's Collections might
        function.
      - Investigated
        [\#100448](https://projects.blender.org/blender/blender/issues/100448):
        USD metersPerUnit setting not used on import / scene units not
        used on export
      - Investigated
        [\#106326](https://projects.blender.org/blender/blender/issues/106326):
        UsdPreviewSurface does not respect normals texture bias and
        scale (as well as the "token visibility" processing)

**General**

  - Committed
    [8178b12af4](https://projects.blender.org/blender/blender/commit/8178b12af4):
    Windows: Remove wait for key-press in --debug mode
  - Committed
    [da1a04580b](https://projects.blender.org/blender/blender-addons/commit/da1a04580b):
    Fix
    [\#104981](https://projects.blender.org/blender/blender-addons/issues/104981):
    Archimesh: Remove usage of SHARP distribution on Glossy BSDF

**Triage**

  - Closed as Duplicate:
      - [\#114154](https://projects.blender.org/blender/blender/issues/114154):
        import OBJ \> 10 millions meshs bug or crash
      - [\#114744](https://projects.blender.org/blender/blender/issues/114744):
        OPTIX Viewport denoiser does not work after switching from Solid
        to Rendered view an arbitrary number of times
      - [\#114717](https://projects.blender.org/blender/blender/issues/114717):
        Viewport Optix Denoise not working Blender 3.6.5
  - Confirmed
      - [\#114736](https://projects.blender.org/blender/blender/issues/114736):
        Crash from bpy.context.temp\_override and bpy.ops.console.clear
      - [\#114766](https://projects.blender.org/blender/blender/issues/114766):
        Crash with \`line.body = ""\`
  - Needs Info from User
      - [\#114747](https://projects.blender.org/blender/blender/issues/114747)
