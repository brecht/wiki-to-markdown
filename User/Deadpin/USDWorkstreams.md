# USD Workstreams

Query: [All "Interest/USD"
issues](https://projects.blender.org/blender/blender/issues?q=&type=all&sort=&state=open&labels=405&milestone=0&project=0&assignee=0&poster=0)

<table>
<tbody>
<tr class="odd">
<td><p>4.1 Milestone Targets</p></td>
<td><p><a href="https://projects.blender.org/blender/blender/issues/111492">https://projects.blender.org/blender/blender/issues/111492</a></p></td>
<td><p>General Plans</p></td>
<td><p>Targets for 4.1 (Scene Instancing, Armatures, Point instancing/clouds, Hooks, Misc)</p></td>
</tr>
<tr class="even">
<td><p>Collection I/O</p></td>
<td><p><a href="https://projects.blender.org/blender/blender/issues/68933">https://projects.blender.org/blender/blender/issues/68933</a><br />
<a href="https://projects.blender.org/blender/blender/issues/100367">https://projects.blender.org/blender/blender/issues/100367</a><br />
<br />
<br />
<a href="https://projects.blender.org/blender/blender/issues/100569">https://projects.blender.org/blender/blender/issues/100569</a><br />
<br />
<a href="https://archive.blender.org/developer/D16176">https://archive.blender.org/developer/D16176</a></p></td>
<td><p>Design<br />
Design<br />
<br />
<br />
Design<br />
<br />
Prototype</p></td>
<td><p>Collections for Import/Export<br />
Add Existing Import/Export Functionality to Collection Properties<br />
<br />
USD Layers referencing, MaterialX, and USD Hydra rendering Addon for Blender</p></td>
</tr>
<tr class="odd">
<td><p>Unified I/O</p></td>
<td><p><a href="https://projects.blender.org/blender/blender/issues/68935">https://projects.blender.org/blender/blender/issues/68935</a><br />
<br />
<a href="https://projects.blender.org/blender/blender/pulls/112466">https://projects.blender.org/blender/blender/pulls/112466</a></p></td>
<td><p>Design<br />
<br />
PR</p></td>
<td><p>Unified object and geometry I/O handling<br />
<br />
FileHandler approach</p></td>
</tr>
<tr class="even">
<td><p>Geometry Sets</p></td>
<td><p><a href="https://projects.blender.org/kevindietrich/blender/commits/branch/abc_usd_geometry_sets_review">https://projects.blender.org/kevindietrich/blender/commits/branch/abc_usd_geometry_sets_review</a><br />
<a href="https://archive.blender.org/developer/D11592">https://archive.blender.org/developer/D11592</a><br />
<a href="https://projects.blender.org/blender/blender/pulls/115623">https://projects.blender.org/blender/blender/pulls/115623</a></p></td>
<td><p>Branch<br />
Old PR<br />
PR</p></td>
<td><p>Alembic/USD: Point Clouds and Mesh Sequence Cache</p></td>
</tr>
<tr class="odd">
<td><p>Point Instancing</p></td>
<td><p><a href="https://projects.blender.org/blender/blender/issues/106398">https://projects.blender.org/blender/blender/issues/106398</a><br />
<br />
<a href="https://projects.blender.org/blender/blender/issues/96747">https://projects.blender.org/blender/blender/issues/96747</a><br />
<br />
<a href="https://projects.blender.org/makowalski/blender/commit/84c72a83836200069343f6b6dbf43849e8e13514">https://projects.blender.org/makowalski/blender/commit/84c72a83836200069343f6b6dbf43849e8e13514</a><br />
<a href="https://projects.blender.org/blender/blender/pulls/113107">https://projects.blender.org/blender/blender/pulls/113107</a></p></td>
<td><p>Design<br />
<br />
Bug<br />
<br />
PR<br />
PR</p></td>
<td><p>UsdGeomPoints / Point instancing<br />
<br />
Support for USD PointInstancer<br />
<br />
USDPointsReader (<em>UsdGeomPoints</em>) - needs Geometry Sets<br />
WIP: USD PointInstancer import support (<em>UsdGeomPointInstancer</em>)</p></td>
</tr>
</tbody>
</table>
