## GSoC 2020 Proposal: Editing Grease Pencil Strokes using Curves

### Synopsis

Since the Blender 2.8 update, that introduced the Grease Pencil object
type, there are a couple of primitives that can be added in the Grease
Pencil draw mode. The user can draw a shape (e.g. a circle, arc,
rectangle, etc.) using the respective tool, edit it and confirm the
creation of the stroke. After this step, the shape can no longer be
manipulated as a curve. I propose to add an interactive edit mode tool,
that allows the user to manipulate any Grease Pencil stroke using curve
handles.

### Benefits

There are already multiple ways to shape a Grease Pencil stroke,
including proportional editing and sculpt mode. But when it comes to,
for example, inking a drawing, precise control over the curve of a
stroke is essential. Moreover, users who are used to working with
curves, as is common in most 2D illustration software, will be able to
work in an environment that they are familiar with. This will make it
possible for more people to join the Blender community. And finally, it
will open up the possibility to build new tools on top of. One idea is
to extend the curve primitive to be able to add more points similar to
the “Polyline” tool.

### Deliverables

The plan is for the user to have access to a new tool in Grease Pencil
edit mode. There it can be selected as the active tool in the toolbar or
called through an operator. By activating the tool, the user will be
able to manipulate any selected stroke as a curve using handles.

### Interface

In terms of workflow, the interface will be similar to the “Knife” and
“Bisect” tools when editing meshes: the tool will stay active until
confirmation. Upon activation, a curve including handles will be drawn
over the selected stroke. Then the user can edit the curve and finally
confirm the changes.

![Mock up showing the different steps in the operator. 1) Select stroke
to edit. 2) Activate tool/run operator; a curve is displayed over the
stroke including handles. 3) Edit the curve with the handles 4) Exit
operation; transformations are
applied.](../../images/Mockup_grease_pencil_edit_curve_tool.png
"Mock up showing the different steps in the operator. 1) Select stroke to edit. 2) Activate tool/run operator; a curve is displayed over the stroke including handles. 3) Edit the curve with the handles 4) Exit operation; transformations are applied.")

The user should be free to move the view around and also move the
handles in the 3rd dimension. Furthermore there should be a way of
adding/deleting a control point, e.g. Ctrl-clicking on a handle removes
it and Ctrl-clicking on the curve adds a new handle.

I also would like to add options to the operator such as:

  - Re-sampling the stroke after confirmation to make it fit the curve
    better.
  - Allow the user to adjust how good the initial curve fitting is
    (number of initial control points)

There are a whole lot more possibilities, but this should be a good
start.

### Implementation

Because Grease Pencil strokes are a chain of line segments, we need to
use a temporary curve data structure that we will convert to and from.
This data structure will also be used to draw the curve to the screen.
For doing the curve fitting, I plan to use `curve_fit_nd`, which is
already included in Blender. Overall, I will try to adapt as much as
possible from existing interactive operators such as the “Knife Tool”.

### Bio

I am a 24 years old computer science student from Germany studying at
the Technical University of Berlin. Currently, I am living in Vienna and
finishing my bachelor thesis. Through my work and studies, I have over 5
years of experience programming in C/C++ and Python. I am also used to
working in teams using Git and familiar with Phabricator. Blender became
interesting to me in the 2.46 era (2008). I have been following the
development and production of the open movies since then. The first time
I contributed to the code was during the “Task Force” when the 2.8 Code
Quest was happening. In the last couple of months I have been in contact
with Antonio Vazquez (Grease Pencil coordinator), working on improving
the fill tools for Grease Pencil.
