## Grease Pencil Curves

### Background

Curves were first introduced to the grease pencil workflow with the
Google Summer of Code 2020 project: [ Editing grease pencil strokes
using curves](../GSoC_2020/index.md). The goal of
this project was to create a tool that would allow the user to select
strokes, edit them with beziér curve handles, and then confirm the
action. During development, the tool evolved into its own mode: Curve
Edit Mode. The feature made it into Blender 2.92 (see the [ release
notes](../../../Release_Notes/2.92/Grease_Pencil.md#Curve_edit_mode)
for more information).

### Techincal Documentation

#### File structure

Here are some of the most important files and functions related to
curves in grease pencil.

  - Operators that are specific to grease pencil curves like "Set Handle
    Type" are located in
    \`blender/source/blender/editors/gpencil/gpencil\_edit\_curve.c\`.
  - Blender kernel functions related to grease pencil curves can be
    found in
    \`blender/source/blender/blenkernel/intern/gpencil\_curve.c\`. This
    is for example where the functions for curve \<-\> stroke conversion
    are located.
  - The DNA structs for the curve data are declared in
    \`blender/source/blender/makesdna/DNA\_gpencil\_types.h\` and are
    called \`bGPDcurve\` for the curve and \`bGPDcurve\_point\` for the
    individual points. The handles are stored as a \`BezTriple\`.
    Instead of defining the curve segments the "traditional" way with 4
    control points, Blender stores the handle itself (1 control point
    and 2 handles from the previous and the next curve segment).
