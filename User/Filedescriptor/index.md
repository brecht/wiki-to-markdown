## About me

Hi, my name is Falk David. I am a computer science student at the
Technical University of Berlin. You can contact me on
[blender.chat](https://blender.chat/direct/filedescriptor).

## Work

  - [ Grease pencil
    curves](Work/grease_pencil_curves.md)

## Google Summer of Code

  - [ GSoC 2020](GSoC_2020/index.md)

## Weekly Reports

  - [ 2021](Reports/2021.md)
  - [ 2023](Reports/2023.md)
