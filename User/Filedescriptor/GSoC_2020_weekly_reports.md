## Week 1 (June 01 - 07)

  - Added a dummy operator mainly to test the curve data structure that
    was added to the \`bGPDstroke\` structure.
  - The \`bGPDstroke\` data structure now has a \`bGPDcurve\` structure.
    This holds an array of \`BezTriple\` that will represent the curve
    points.
  - Implemented the RNA structure for \`bGPDcurve\`.
  - Added reading and writing of the \`bGPDcurve\` structure to file.
  - Added a toggle button for the edit curve mode in the header bar of
    grease pencil edit mode

![Curve edit mode toggle
button](../../images/Curve_edit_mode_toggle_button.png
"Curve edit mode toggle button")

  - Added a RNA property for the \`point\_index\_array\` which is part
    of the \`bGPDcurve\` RNA structure (for debugging).

## Week 2 (June 08 - 14)

  - Implemented a function to create a bézier curve from a grease pencil
    stroke. It uses the algorithms defined in \`curve\_fit\_nd.h\`.
  - When the user switches to curve edit mode, the selected strokes will
    automatically be used to generate their curves.
  - Implemented a function to (re)create grease pencil stroke points
    from a bézier curve.
  - Implemented interpolation for pressure, strength and vertex colors
    between curve points. Currently this uses linear interpolation.
  - When the coordinates, strength, pressure or vertex color of the
    curve points change, an update of the stroke points is automatically
    triggered.
  - Implemented the \`select\_all\` operator to make it work with curve
    points if the object is in curve edit mode. This includes
    deselection and invert selection.
  - Implemented the \`select\_linked\`, \`select\_more\`,
    \`select\_less\` operator for curve points.
  - Added a \`curve\_resolution\` parameter to the \`bGPdata\`
    structure. This parameter controls the number of segments that get
    created when the editcurve is transformed back into a stroke
    (visually this controls how smooth the stroke looks).
  - When the \`curve\_resolution\` parameter changes, the stroke is
    automatically updated.

## Week 3 (June 15 - 21)

  - Added a \`error\_threshold\` property to the \`select\_all\`
    operator. When a stroke is first converted to a curve, the fitting
    can be adjusted with this parameter.
  - Implemented click selection for curve points and handles. This
    includes deselection for clicking in empty space, toggle selection
    and extend selection (with \`shift\`). The selection also takes the
    overlay settings into account, e.g. if a handle is not displayed it
    cannot be selected.
  - Implemented basic transformation for curve points. This includes
    translation, rotation, scaling, changing the thickness and changing
    the strength/opacity for every curve point. The handles are treated
    as type \`free\` and do not move with the control point for now.
  - Exiting curve edit mode deselects all the stroke points and strokes
    on the current frame. This will change later, as we aim to
    synchronize the selection between strokes and edit curves.

## Week 4 (June 22 - 28)

  - Worked on selection synchronization when entering/exiting curve edit
    mode. Currently, if a handle is selected, the corresponding stroke
    point of the control point will be selected. If two neighbouring
    handles are selected, all of the stroke points in between will
    additionally be selected.
  - More conceptual work was done on solving a code design issue, when
    it comes to the curve fitting. We would want the fitting to be
    triggered when the user selects a stroke in curve edit mode. More
    details on this issue can be found in [this devtalk
    thread](https://devtalk.blender.org/t/code-desgin-issue-with-operators/14072).

## Week 5 (June 29 - July 05)

  - Added an operator to set the handle type of a curve point
    (\`GPENCIL\_OT\_stroke\_editcurve\_set\_handle\_type\`).
  - Added a BKE function to recalculate the handle positions depending
    on the type (\`BKE\_gpencil\_editcurve\_recalculate\_handles\`).
  - Support transformation of all handle types.
  - Added custom shortcuts for curve edit mode. Currently there are only
    two shortcuts (\`V\` to toggle the handle type and \`U\` to toggle
    curve edit mode). All the usual shortcuts in edit mode still work in
    curve edit mode, unless they are overwritten (e.g. \`V\` is "split"
    in edit mode, but "toggle handle type" in curve edit mode). They can
    all be changed through the keymap editor.
  - Fixed some crashes when editing points in edit mode and when editing
    individual handles.
  - Reworked parts of the handling of the conversion from curve to
    stroke and vice versa. Both the stroke and the curve now have a flag
    that indicates they are no longer synchronized with the other data
    structure. This allows us to recalculate stroke and curve data only
    when needed.

## Week 6 (July 06 - 12)

  - Support cyclic strokes
    ([rB96eca6c3d447](https://developer.blender.org/rB96eca6c3d44709dde799865935a0361b9ea09052),
    [rBfa662dceef37](https://developer.blender.org/rBfa662dceef378f9c68d56c7d1e5e403d14bc8b46),
    [rBab91491963f5](https://developer.blender.org/rBab91491963f555609c95dfe1bdc5317dcfd0a665)).
    The fitting algorithm now can fit a curve to a cyclic stroke.
  - Allow individual handles to have different handle types
    ([rB486a06d71be1](https://developer.blender.org/rB486a06d71be1a27ba2f90c51140a0d5e39471de1)).
  - Prepare all editmode operators to be used in curve edit mode
    ([rB967c5b7da555](https://developer.blender.org/rB967c5b7da55507b8302b65a7dc813449321232c4),
    [rBc62bc08f45b7](https://developer.blender.org/rBc62bc08f45b7714c889c147d87a3f8a145984d33),
    [rB596f10a0cf14](https://developer.blender.org/rB596f10a0cf14741679a4b3228662e12a0ae591ff),
    [rB6b12994ccf11](https://developer.blender.org/rB6b12994ccf119c953dc05635597016a2587b5fda)).
    All the editmode operators need to be adapted to support curve edit
    mode. Some of the operators already work (e.g.
    \`GPENCIL\_OT\_stroke\_apply\_thickness\`,
    \`GPENCIL\_OT\_stroke\_caps\_set\`,
    \`GPENCIL\_OT\_stroke\_change\_color\`) some will need to be ported.
    All the operators are now adapted, so that they don't crash when
    being executed.
  - Implement circle select operator
    ([rB6fa8efd81592](https://developer.blender.org/rB6fa8efd8159206da65a2d6c6da1ca71fd113da90)).
  - Implement subdivide operator
    ([rB9fa9ee613688](https://developer.blender.org/rB9fa9ee61368860833e535a51aa789ed04fd2db72)).
    The algorithm will not change the shape of curve and supports cyclic
    curves.

## Week 7 (July 13 - 19)

  - Implemented box and lasso select for curves
    ([rB5122c75fd62c](https://developer.blender.org/rB5122c75fd62c244473e10e3242f3312378bebca9)).
  - Implement a more accurate algorithm for selecting fill shapes
    ([rBeb22e7a650a2](https://developer.blender.org/rBeb22e7a650a27cfd4abc49a0d7e32fa3d3ee209e)).
  - Implement all remaining select operators:
    \`GPENCIL\_OT\_select\_grouped\`
    ([rBfcd22007b520](https://developer.blender.org/rBfcd22007b52033de52757f824ea1018e0d36b1aa)),
    \`GPENCIL\_OT\_select\_more\` and \`GPENCIL\_OT\_select\_less\`
    ([rBbd71007ebda7](https://developer.blender.org/rBbd71007ebda79c0cadabdb83dae1c01bbd71fcfa)),
    \`GPENCIL\_OT\_select\_first\` and \`GPENCIL\_OT\_select\_last\`
    ([rB95d0192308f5](https://developer.blender.org/rB95d0192308f515e466a5b1447549bb69df28d35f)),
    \`GPENCIL\_OT\_select\_alternate\`
    ([rBd0d9490ac009](https://developer.blender.org/rBd0d9490ac00967ddb3451502ca2423b0e1ec0bab)).
  - Convert stroke to curve when a stroke with no editcurve is clicked
    on in curve edit mode
    ([rB5bac474903e9](https://developer.blender.org/rB5bac474903e99adbed89e9b610f6905076c7ede8)).
  - Recalculate curve data when entering edit mode and the stroke data
    has changed
    ([rB9d69d642ff17](https://developer.blender.org/rB9d69d642ff17fad683840d055a9b4b8393de92da)).

## Week 8 (July 20 - 26)

  - Implemented an algorithm for adaptive curve resolution
    ([rBf16a50ba946a](https://developer.blender.org/rBf16a50ba946ae4ddea18d4f7058fef3a359078b8)).
    This algorithm makes sure that the sample points are more evenly
    distributed along the curve, resulting in uniform detail.

![Adaptive curve resolution
off](../../images/Adaptive_curve_resolution_off.png
"Adaptive curve resolution off") ![Adaptive curve resolution
on](../../images/Adaptive_curve_resolution_on.png
"Adaptive curve resolution on")

  - Implement multi-dimensional curve fitting
    ([rBc18eb01eb641](https://developer.blender.org/rBc18eb01eb6416f06cd3ee59e428d24b6b6d2164a),
    [rB2d9e869b18b9](https://developer.blender.org/rB2d9e869b18b96c66072e9fa1568b1abe3e10c6b1),
    [rB48e52c5a5ced](https://developer.blender.org/rB48e52c5a5ced06b49f1fee6a8bd3f3a51838bd8a)).
    Previously the fitting algorithm would only fit to the shape of the
    stroke and ignore pressure, strength and vertex colour. This meant
    that detail in those parameters would be lost when converting to a
    curve. Now the algorithm will also fit the curve to pressure,
    strength and vertex colour.
  - Implemented dissolve operator
    ([rB075276ee803a](https://developer.blender.org/rB075276ee803a1bb03f694f1707868fbc7ed82850)).

## Week 9 (July 27 - August 03)

  - Fixed a bug related to single point curves. In general, we would
    like to avoid single point curves (as a curve needs at least one
    segment to be an actual curve, which requires two points)
    ([rBf2e2913a5838](https://developer.blender.org/rBf2e2913a5838082ba1af65cf2c551316f2a0c889)).
  - Finished the implementation for curve point deletion
    ([rB331e1639c499](https://developer.blender.org/rB331e1639c49989e83da3467e1d9a3dc41a197ac3)).
  - Implemented snapping to the viewport grid of curve points
    ([rB944ba1b38d01](https://developer.blender.org/rB944ba1b38d01d6d5f578c57bf0efe519e8a79da2)).
  - Implemented basic support for extruding. Currently only end points
    can be extruded
    ([rBc825d70811f3](https://developer.blender.org/rBc825d70811f38202b2ec9fc945db61b104f39139)).

## Week 10 (August 04 - 09)

  - Rewrote the transform code from scratch. Previously the transform
    code had multiple issues:
      - center point of rotation and scale is not in the right location
        when control point is selected
      - scaling and rotating of handle points should scale and rotate
        the control point (and not try to scale or rotate the handle
        which does nothing)
      - proportional editing is not working at all

To solve these issues the transform code for strokes and curves were
separated and the transform code for the curves was rewritten from
scratch. The issues mentioned were thereby fixed
([rB0ae02b130a78](https://developer.blender.org/rB0ae02b130a78237a8850b616da99edfe8e5fc66d)).

## Week 11 (August 10 - 16)

  - Make the handle types update when moved. Previously, transforming
    vector or auto handles was not possible. These handles will now
    update their handle type when moved
    ([rBe1e1b303fa32](https://developer.blender.org/rBe1e1b303fa32a7857e0d34d4c29e6f2139c97ab6)).
  - Update editcurve data when stroke data has changed. Previously, this
    caused a bug where the stroke and curve data would be out of sync
    (e.g. when the user switched to sculpt mode, changed the stroke,
    then switched back to curve edit mode). This now has been fixed
    ([rBa8cc21f09ec4](https://developer.blender.org/rBa8cc21f09ec440f814c54060c33c46d90263ba5c)).
  - Curve points in the middle of the curve can now be extruded
    ([rBc6650146eb1c](https://developer.blender.org/rBc6650146eb1c615293b1371992270520fe262c57)).
  - Moved the checkbox "Adaptive Resolution" to the bottom of the panel
    for semantic consistency
    ([rBa0152042b846](https://developer.blender.org/rBa0152042b846887412c31df8ad4f779a2c400c1c)).
  - Added automatic corner detection for the curve fitting. A parameter
    "Corner Angle" was added to the Curve Editing panel to control the
    angle threshold for corners
    ([rB8bd1469d86d4](https://developer.blender.org/rB8bd1469d86d43fe9e896ea0b487a22ea896fcd17)).
  - Started to write documentation.

## Week 12 (August 17 - 23)

  - Compiled the \`soc-2020-greasepencil-curve\` branch into a single
    patch ([D8660](https://developer.blender.org/D8660)). All the
    changes in the branch will also be updated in the patch.
  - Fixed a memory issue in the transform code, that caused crashes on
    some systems
    ([rB3633a6197e8a](https://developer.blender.org/rB3633a6197e8a8ca5888e7e613d2f5a6b665bf823)).
  - Implemented transform gizmo drawing for curves. Previously the gizmo
    was not drawn in curve edit mode.
    ([rBe0985ffcc153](https://developer.blender.org/rBe0985ffcc153f0a66f50e013d096009000ca6411)).
  - Operators that are not supported yet, will display an error message
    when used
    ([rB062595b6f970](https://developer.blender.org/rB062595b6f9706cc89d0d903ddd6ca4f512ee2b4b)).
  - Wrote the first draft of my final GSoC report. Can be found
    [here](https://wiki.blender.org/wiki/User:Filedescriptor/GSoC_2020/Report).

## Week 13 (August 24 - 30)

  - Fixed a crash where a curve with a single or two points was not
    handled properly
    ([rB274c8ae9e27a](https://developer.blender.org/rB274c8ae9e27a7ba5290c72c72cbc2fcdb6d7b056)).
  - Convert stroke to curve while in curve edit mode using selection.
    This was disabled for some selection operators because there were
    issues with the conversions. The issues were fixed and conversion is
    enabled for all selection operators
    ([rB9c6d0ba92397](https://developer.blender.org/rB9c6d0ba92397435557ec19552460fedf25b58531),
    [rBf2174e082ae0](https://developer.blender.org/rBf2174e082ae070af7a9a8e0cab1104e17abf26a3)).
  - Implement duplicate operator
    ([rB21cac5fed364](https://developer.blender.org/rB21cac5fed36406b166178bd922bc2ffb389bb772)).
  - Worked on the [final
    report](https://wiki.blender.org/wiki/User:Filedescriptor/GSoC_2020/Report).
