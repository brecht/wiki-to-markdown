# GSoC 2020 Meeting Notes

## 19 May 2020

I met with Antonio Vazquez to discuss the general plan of action of the
summer of code project. Here are most of the ideas that were talked
about:

  - One major goal is to reuse as much code as possible, since blender
    already has a lot of functionality regarding curves, NURBS etc. We
    aim to reuse the code from the overlay drawing engine, the data
    structures used by the curve object and some of the code that runs
    the transformations of curve handles.
  - Antonio proposed an idea on how the user interacts with the tool.
    Similar to the [multiframe edit
    mode](https://docs.blender.org/manual/en/latest/grease_pencil/multiframe.html),
    we would have a submode in the edit mode (e.g. "beziercurve edit
    mode"). The user selects the strokes to be edited as curves and then
    enters the new mode. The stroke points then are no longer visible
    and instead curve handles appear over the strokes. The user can make
    a change and then exit the mode again by disabling it.
  - On a technical level, we want the "beziercurve edit mode" to be an
    abstraction layer on top of stroke edit mode.
      - This requires a change in the \`bGPDstroke\` DNA. We would add a
        pointer to a NURBS data structure (we are still unsure if this
        can be entirely reused from the curve object, or if we will
        create our own "greasepencil nurbs" struct on top to have more
        control/options, e.g. to add flags and more in the future).
      - This pointer will be \`NULL\` when "beziercurve edit mode" is
        disabled and will be populated when the mode is entered.
      - The data in this struct will be used by the overlay drawing
        engine to draw the handles etc. to the viewport (in GP edit
        mode).
      - When the user makes a change by e.g. moving a handle, we want to
        use the NURBS data to update the stroke points. This is
        important because we don't need to adapt the stroke data or
        anything in the stroke drawing engine, since we are indirectly
        still changing stroke points like we already do in edit mode.
      - We need to make sure the transformation from \`NURBS\` to
        \`bGPDstroke\` is fast enough for the "beziercurve edit mode" to
        be responsive.
  - Then we talked about what happens when a \`.blend\` file is written
    or read when the user is in "beziercurve edit mode". Basically we
    need to make sure, that the curve data is either saved or recreated
    when the file is opened. We don't expect to be anything problematic
    here.
  - I raised the concern, that when the user enters and exits the
    "beziercurve edit mode" right away, the strokes might change
    sightly, since they are approximated by curves and then this data
    get written back to the stroke. Antonio already talked about this
    with Daniel and Matias. They don't see it as a big issue, since in
    most cases this is desirable and undoing should work just fine.
  - There should be a setting that determines how good the curve
    approximation fits the stroke. Essentially this will control how
    smooth/jagged the curves are.
  - We need to make sure that the first and last point of a stroke
    always stay fixed and do not move. Otherwise this will create issues
    where two previously connected strokes can form gaps.

## 05 June 2020

We had a discussion together with Antonio Vazquez and Matias Mandiola
about the UI and further proceedings for the edit curve mode. Here are
some of the conclusions we came to:

  - The edit curve mode (or maybe bezier edit mode) can be activated in
    grease pencil edit mode by clicking on a toggle button next to the
    selection mode buttons in the header.
  - We had a long discussion about the selection modes. It was unclear
    if they should be active or disabled if the user is in edit curve
    mode. We came to the conclusion that they would indeed all be
    useful, but that we cannot prioritize working on them for now. That
    being said, point selection and stroke selection are the easier to
    adapt, so we would implement these first.
  - We will focus on having stable selection of the curve handles.
      - For this, all selection operators have to be adapted. These
        include \`GPENCIL\_OT\_select\_all\`,
        \`GPENCIL\_OT\_select\_circle\`, \`GPENCIL\_OT\_select\_box\`,
        \`GPENCIL\_OT\_select\_lasso\`, \`GPENCIL\_OT\_select\_linked\`,
        \`GPENCIL\_OT\_select\_grouped\`, \`GPENCIL\_OT\_select\_more\`,
        \`GPENCIL\_OT\_select\_less\`, \`GPENCIL\_OT\_select\_first\`,
        \`GPENCIL\_OT\_select\_last\`,
        \`GPENCIL\_OT\_select\_alternate\`.
      - We will start with \`select\_all\` since it easier to check, as
        all the control points should get selected (mouse position
        doesn't need to be checked).
  - Some of the edit mode tools in the toolbar make no sense in edit
    curve mode (like shear or bend). We might disable them for now.

## 03 July 2020

Antonio and I had a chat about the conversion from stroke to curve and
vice versa. There were some points that were unclear to me.

We came up with the following:

  - When should a conversion from stroke-\>curve happen?:
      - A stroke should be fitted to a curve either when it was
        selected, had no editcurve or the editcurve data is outdated,
        and the user switched to curve edit mode or when the user
        selects a curve in curve edit mode that has no editcurve or the
        editcurve data is outdated.
      - We change \`GP\_CURVE\_RECALC\_GEOMETRY\` to
        \`GP\_CURVE\_NEEDS\_STROKE\_UPDATE\` and add
        \`GP\_STROKE\_NEEDS\_CURVE\_UPDATE\` to the flags of the
        \`bGPDstroke\`. When \`GP\_CURVE\_NEEDS\_STROKE\_UPDATE\` is set
        it means that the stroke changed and the curve needs to be
        recalculated.
  - When should a conversion from curve-\>stroke happen?:
      - In short: every time the curve data is changed (e.g. a handle is
        moved, the vertex color of a curve point changes, etc.)
      - Make sure to set \`GP\_STROKE\_NEEDS\_CURVE\_UPDATE\` in thew
        stroke flag and then call
        \`BKE\_gpencil\_stroke\_geometry\_update\`. This will update the
        stroke geometry from the curve geometry.
  - When should we sync the selection from stroke-\>curve?
      - Only when we switch curve edit mode ON.
  - When should we sync the selection from curve-\>stroke?
      - Only when we switch curve edit mode OFF.
  - When should we (re)allocate the editcurve?
      - When the curve is fitted to the stroke (see first point).
  - When should we free the editcurve?
      - For now we won't free the data once generated, only update it
        (if needed). This is because the curve data is much smaller than
        the stroke data so it is not a big issue. The curve will be
        saved alongside the stroke to the file when saved.
      - One idea for later is to free the editcurve data when the user
        is outside of curve edit mode and changes the geometry of a
        stroke.
