# Editing Grease Pencil Strokes Using Curves - Final Report

## Summary

This summer I got to work on grease pencil in Blender. As a 2D/3D
animation-, drawing-, and illustration tool, grease pencil was missing
an important feature requested by many in the community: Curves. The
main goal of the project was to provide a way to edit strokes using
curve handles. All of the main goals in my
[proposal](https://wiki.blender.org/wiki/User:Filedescriptor/GSoC_2020_editing_grease_pencil_strokes_using_curves)
were achieved.

## Differences to the original proposal

In the proposal, I suggested adding an "interactive edit mode tool". As
it turns out, there are numerous reasons not to implement curve editing
as an interactive tool but rather as a "sub-mode" in edit mode. One of
the most important reasons was that an interactive tool like the knife
tool consumes all the inputs of the user while being active. This would
have meant that every interaction the user makes with the viewport would
have to be handled by said tool. For obvious reasons, this was not the
best approach. Implementing it as a "mode" not only meant that no inputs
would be blocked but also opened up the possibility to reuse existing
operators from edit mode in curve editing mode. These changes in the
plan don't affect the way the end-user interacts with the feature by
much but made it a lot easier to implement.

## Code

All of the work was done on the \`soc-2020-greasepencil-curve\` branch
on the main blender git repository. In the end, all of the changes were
compiled into a single patch and submitted to
[developer.blender.org](https://developer.blender.org) for review.

**Gitweb branch link:**
<https://git.blender.org/gitweb/gitweb.cgi/blender.git/shortlog/refs/heads/soc-2020-greasepencil-curve>

**Link to the final patch:** <https://developer.blender.org/D8660>

## How does curve edit mode work?

### Getting Started

''Currently, to use the features explained below, you need to
[build](https://wiki.blender.org/wiki/Building_Blender) the
\`soc-2020-greasepencil-curve\` branch yourself. Run \`git checkout
soc-2020-greasepencil-curve\` before building to switch to the branch.
''

Start Blender and choose the "2D Animation" template. Draw a stroke and
switch to edit mode in the top left corner of the 3D viewport. Select
your stroke with a box selection and click the toggle button next to the
"Curve Editing" panel in the top bar. Now the stroke will be
automatically converted to a curve (you can change the conversion
parameters in the "Curve Editing" panel). You can select handles and
control points by left-clicking on them. Either press \`G\` to move the
select points or press \`W\` until the active tool is the tweak tool and
then use click and drag to move. These are the basics on how to use
curve edit mode\!

![Curve Edit Mode Off](../../../images/Curve_edit_mode_off.png
"Curve Edit Mode Off") ![Curve Edit Mode
On](../../../images/Curve_edit_mode_on.png "Curve Edit Mode On")

### Details

Under the hood, curve edit mode acts as an abstraction layer on top of
normal edit mode. The idea is that the internal curve data structure
will write to the stroke data whenever it changes. One of the benefits
of this design is that all the code that draws a stroke in the viewport
does not have to be changed. Moreover, animation works right out of the
gate since everything gets converted to strokes at the end.

By default, the curve data of a stroke is \`NULL\`. When the user
selects a stroke and switches to curve edit mode, a curve will
automatically be fitted to the selected stroke(s) and saved to the curve
data. Immediately after, the stroke point data gets overwritten by
sampled points on the curve. This last step happens every time the curve
changes.

The curve data will be saved to file together with the stroke. Every
stroke can have at most one curve. This way, users can save their work
and not lose the ability to use the curve handles (e.g curve data is not
temporary, but persistent). Also, if a stroke has curve data attached to
it, it will only be overwritten when the stroke data changes outside of
curve edit mode (e.g. by transforming stroke points in sculpt mode). In
this case, when the user switches back to curve editing, the curve data
is deleted and a new curve is fitted to the changed stroke.

Additionally, a great number of design decisions and technical details
can be found in the [meeting
notes](https://wiki.blender.org/wiki/User:Filedescriptor/GSoC_2020_meeting_notes).

## What's left to do?

There were plans to have all the operators working, but due to the time
restriction, I decided to stabilize existing features instead. Some
operators have not been fully implemented yet. They are:

  - Copy & Paste
  - Snap to Cursor & Cursor to selected
  - Flip
  - Simplify & Simplify Fixed
  - Trim
  - Separate
  - Split

## Plans for the future

As I already mentioned in my proposal, there are a lot of possibilities
that can be built on top of the groundwork that was implemented during
this summer. For example:

  - more operators like *snapping*, *beveling*, *inserting curve points*
    at a specific location between two other curve points, etc.
  - more transformation features like *curve point slide*, *bending
    curve segments*, *rounded corners*, etc.
  - drawing tools that allow drawing a curve directly, e.g. a polyline
    tool with curves.

## Acknowledgments

A big thank you to all the blender developers and a special thanks to
Antonio Vazquez (\`antonioya\`), my mentor, who did an excellent job in
guiding me\! I would also thank Daniel Martinez Lara, Matias Mendiola,
and the community for their helpful feedback. I look forward to
contributing to grease pencil in Blender in the future\!
