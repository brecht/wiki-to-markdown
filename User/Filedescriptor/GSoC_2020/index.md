## Google Summer of Code 2020: Editing grease pencil strokes using curves

  - [Proposal](https://wiki.blender.org/wiki/User:Filedescriptor/GSoC_2020_editing_grease_pencil_strokes_using_curves)
  - [Meeting
    notes](https://wiki.blender.org/wiki/User:Filedescriptor/GSoC_2020_meeting_notes)
  - [Weekly
    reports](https://wiki.blender.org/wiki/User:Filedescriptor/GSoC_2020_weekly_reports)
  - [Final
    report](https://wiki.blender.org/wiki/User:Filedescriptor/GSoC_2020/Report)
