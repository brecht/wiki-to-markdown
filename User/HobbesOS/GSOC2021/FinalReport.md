# Blender Knife Tool Improvements - Final Report

**Update:** This project has been merged into master ahead of Blender
3.0 alpha release. The entire "Further Work" section of this report has
been completed and is included in the final merge. Read more -
<https://developer.blender.org/D12600>

## Summary

This summer I worked on various improvements to the Knife Tool in
Blender as outlined in my [project
proposal](https://wiki.blender.org/wiki/User:HobbesOS/GSOC2021/Proposal).
A majority of the features contained within this original proposal have
been compeleted. The remaining features were deemed unecessary as the
project progressed and were replaced by more important design tasks.

## Important Links

  - Git Branch -
    [**\`soc-2021-knife-tools\`**](https://developer.blender.org/diffusion/B/history/soc-2021-knife-tools/)
  - [Project
    Proposal](https://wiki.blender.org/wiki/User:HobbesOS/GSOC2021/Proposal)
  - [Weekly Reports
    Thread](https://devtalk.blender.org/t/gsoc-2021-knife-tool-improvements-weekly-reports/)
  - [Feedback
    Thread](https://devtalk.blender.org/t/gsoc-2021-knife-tool-improvements-feedback/19047)

## Completed Work

### Constrained Angle Mode Improvements

  - Added Knife Tool setting to control snapping angle increment
  - Added support for entering snapping angle increment via number keys
  - Added a local constrained angle mode (further discussed under
    "[Proposal
    Deviations](FinalReport.md#Proposal_Deviations)")
  - Added Knife Tool setting to set default mode to None, Screen or
    Local

Notable Commit -
[**\`1f3607a8f717\`**](https://developer.blender.org/rB1f3607a8f71791d3f97bd28a5a5cdf31f246b158)
| [Weekly
Report](https://devtalk.blender.org/t/gsoc-2021-knife-tool-improvements-weekly-reports/19046/2?u=hobbesos)

### Snapping to Global and Local Orientation

  - Press X, Y or Z to snap cut line to respective global axis
  - Pressing the same key again will snap to local axis
  - Respects scene orientation setting if set, allowing for custom
    orientations

Notable Commit -
[**\`4b11312335b5\`**](https://developer.blender.org/rB4b11312335b53326c64bcd252c545f69435fe5d3)
| [Weekly
Report](https://devtalk.blender.org/t/gsoc-2021-knife-tool-improvements-weekly-reports/19046/2?u=hobbesos)

### Visible Distance and Angle Measurements

  - Pressing 'S' will enable measurements
  - Three modes: Only Distance, Only Angles, Both
  - Shows length of current cut segment
  - Shows angle between current cut segment and previous cut segment or
    mesh edge
  - Shows angle between current cut segment and any hovered over cut
    segment or mesh edge

Notable Commit -
[**\`bec317858d3b\`**](https://developer.blender.org/rBbec317858d3b4924c649632234d8a8fab86508a6)
| [Weekly
Report](https://devtalk.blender.org/t/gsoc-2021-knife-tool-improvements-weekly-reports/19046/5?u=hobbesos)

### Undo Capability

  - Press Ctrl-Z to undo previous cut segment
  - Starting point for current cut segment is adjusted accordingly
  - If cut is a drag cut the entire drag cut is undone

Notable Commit -
[**\`219e0155c270\`**](https://developer.blender.org/rB219e0155c270de6104fd07c2937a3ea28ab1b27c)|
[Weekly
Report](https://devtalk.blender.org/t/gsoc-2021-knife-tool-improvements-weekly-reports/19046/7?u=hobbesos)

### Multi-Object Edit Mode

  - Added support for multi-object edit mode to Knife Tool
  - Cuts can be created on every object in multi-object edit mode
  - Cuts *cannot* be created from one object to another (see "[Further
    Work](FinalReport.md#Further_Work)")

Notable Commit -
[**\`bd6587cb524b\`**](https://developer.blender.org/rBbd6587cb524b24f3f43c4dde2a5eab845f54d82c)|
[Weekly
Report](https://devtalk.blender.org/t/gsoc-2021-knife-tool-improvements-weekly-reports/19046/13?u=hobbesos)

## Proposal Deviations

### Undo "Second Case"

After implementing undo's "first case", as discussed in the original
proposal, it was decided that the "second case" mentioned in that
proposal was not a necessary feature.

### Edge Removal Mode

As the project progressed it became clear that edge removal mode was a
niche feature as a result of the newly implemented undo capabilities. It
was decided to abandon it in favour of spending more time working on
further multi-object edit mode improvements as discussed below. Futher
explanation can be found in the [Weekly
Report](https://devtalk.blender.org/t/gsoc-2021-knife-tool-improvements-weekly-reports/19046/17?u=hobbesos).

### Local Constrained Angle Mode

Implementing visible distance and angle measurements showcased the need
for a local constrained angle mode. In the original constrained angle
mode the cut angle is constrained in screen space. This new local mode
constrains the cut along the current face and relative to the previous
cut segment. When a cut is started on an edge the angle is relative to
that edge and when it is started on a vertex the user can press 'R' to
cycle through edges for the cut to be relative to.

Notable Commits -
[**\`86f15dbcdcb3\`**](https://developer.blender.org/rB86f15dbcdcb3b091a0536d7062c6b6e013c6aa38),[**\`4581ce55c954\`**](https://developer.blender.org/rB4581ce55c954584198d7ca2220aef658b245b169)
| [Weekly
Report](https://devtalk.blender.org/t/gsoc-2021-knife-tool-improvements-weekly-reports/19046/20?u=hobbesos)

### Further Multi-Object Edit Mode Improvements

Based on feedback from the community I spent the remaining few weeks of
the project working on further improving the new multi-object edit mode
compatibility. As of writing I have successfully implemented a custom
BVH for the Knife Tool which lays the foundation for a much better
multi-object edit mode implementation in the future.

## Further Work

**Porting the Knife Tool to use all world-space code**

  - Majority of the current code is object-space
  - Custom Knife BVH allows world-space
  - Necessary to implement better multi-object edit mode

**Better Multi-Object Edit Mode**

  - Ability to make cuts across multiple objects at once
  - Ability to snap to multiple object's meshes (even objects occluded
    by other's when in wire-frame)
