## Google Summer of Code 2021

### Knife Tool Improvements

  - [Proposal](/Proposal)
  - [Feedback](https://devtalk.blender.org/t/gsoc-2021-knife-tool-improvements-feedback/19047)
  - [Weekly
    Report](https://devtalk.blender.org/t/gsoc-2021-knife-tool-improvements-weekly-reports/)
  - [Final Report](/FinalReport)
