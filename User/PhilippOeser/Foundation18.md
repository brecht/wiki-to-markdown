## Warmup (December 2017)

**Info**

after some time not being active did some warmup in the tracker

**Tracker Triaging**

  - [\#53639](http://developer.blender.org/T53639): confirmed,
    investigated, posted patch \[made a commit -- see Master Commits\]
  - [\#53630](http://developer.blender.org/T53630): found commit causing
    this, posted hacky workaround, mont29 did real fix and closed
  - [\#52977](http://developer.blender.org/T52977): found commit causing
    this, mont29 did real fix and closed
  - [\#53624](http://developer.blender.org/T53624): posted solution,
    outcome pending
  - [\#53646](http://developer.blender.org/T53646): confirmed, posted
    backtrace, still open
  - [\#53212](http://developer.blender.org/T53212): investigated -\> not
    reproducable
  - [\#53625](http://developer.blender.org/T53625): confirmed,
    investigated, suggested workaround, real solution pending
  - [\#53647](http://developer.blender.org/T53647): confirmed,
    investigated, posted patch \[somewhat made a commit -- see 2.8
    Commits\]
  - [\#53513](http://developer.blender.org/T53513): confirmed,
    investigated, shied away, but dfelinto fixed/closed
  - [\#53579](http://developer.blender.org/T53579): raised from
    incomplete to confirmed, investigated, found cause, lijenstina did
    fix and closed
  - [\#53645](http://developer.blender.org/T53645): investigated, asked
    for more info, closed later (see Week 1)

**Master Commits**

  - Fix T53639: text sequence strips no stamped into render
    [rB08b063a1](https://projects.blender.org/blender/blender/commit/08b063a19f21cd1bcafc9c0dff70d5ff6f0e099a)

**2.8 Commits**

  - patch that 'Fix T53647: Outliner crashes when active object is
    deleted' was based on
    [rBf2a24afe](https://projects.blender.org/blender/blender/commit/f2a24afe8c1aa0221b378585e3c1901cc568a6d7)

## Week 1 (January 2nd - 5th)

**Info**

spend time mostly triaging/investigating tracker reports. Also looked
into running tests for 2.79a release

**Tracker Triaging**

  - [\#53677](http://developer.blender.org/T53677): triaged incomplete,
    suggested solution, closed later (see Week 2)
  - [\#53676](http://developer.blender.org/T53676): triaged incomplete
    (missing info), closed later (see Week 2)
  - [\#53679](http://developer.blender.org/T53679): triaged incomplete,
    suggested solution, closed later (see Week 2)
  - [\#53212](http://developer.blender.org/T53212): triaged incomplete
    (not reproducable), response pending
  - [\#53662](http://developer.blender.org/T53662): investigated, still
    have to discuss with mont29, pending
  - [\#53650](http://developer.blender.org/T53650): confirmed,
    investigated, posted patch \[made a commit -- see Master Commits\]
  - [\#53681](http://developer.blender.org/T53681): closed as invalid
    (not a bug), posted solution
  - [\#53680](http://developer.blender.org/T53680): confirmed,
    investigated (tbc in Week 2)
  - [\#53668](http://developer.blender.org/T53668): triaged normal,
    investigated, solution pending
  - [\#53682](http://developer.blender.org/T53682): confirmed, but
    triaged invalid (2.8 report)
  - [\#53620](http://developer.blender.org/T53620): confirmed, assigned
    mont29 (fbx)
  - [\#53672](http://developer.blender.org/T53672): confirmed,
    investigated a bit, brecht claimed
  - [\#53673](http://developer.blender.org/T53673): triaged normal,
    investigated, solution pending
  - [\#53661](http://developer.blender.org/T53661): triaged normal,
    solution pending
  - [\#53686](http://developer.blender.org/T53686): confirmed,
    investigated, solution pending
  - [\#53687](http://developer.blender.org/T53687): confirmed,
    investigated, solution pending
  - [\#53664](http://developer.blender.org/T53664): marked as todo,
    solution pending
  - [\#53628](http://developer.blender.org/T53628): triaged incomplete
    (not reproducable), response pending
  - [\#53623](http://developer.blender.org/T53623): triaged low, posted
    workaround, reporter closed as resolved
  - [\#53258](http://developer.blender.org/T53258): triaged incomplete
    (expected behaviour), no response, closed later (see Week 2)
  - [\#53622](http://developer.blender.org/T53622): triaged invalid (not
    a bug per se), suggested/posted workaround
  - [\#53697](http://developer.blender.org/T53697): triaged incomplete
    (missing info), closed later (see Week 2)
  - [\#53695](http://developer.blender.org/T53695): triaged invalid (2.8
    report)
  - [\#53645](http://developer.blender.org/T53645): closed (more than a
    week without reply)
  - [\#53353](http://developer.blender.org/T53353): reopened (some
    information was given), but asked for even more info, closed later
    (see Week 2)
  - [\#53698](http://developer.blender.org/T53698): confirmed,
    investigated a bit, solution peding
  - [\#53693](http://developer.blender.org/T53693): triaged incomplete,
    suggested solution, reporter closed as resolved
  - [\#53699](http://developer.blender.org/T53699): triaged incomplete,
    response, then closed as invalid (not a bug)
  - [\#53694](http://developer.blender.org/T53694): closed as duplicate
    of [\#T45854](http://developer.blender.org/TT45854)
  - [\#53691](http://developer.blender.org/T53691): confirmed,
    investigated, assigned sergey, sergey did fix and closed
  - [\#53590](http://developer.blender.org/T53590): investigated, asked
    campbell, he closed as todo
  - [\#53601](http://developer.blender.org/T53601): investigated, closed
    as invalid (windows internal problem)
  - [\#53553](http://developer.blender.org/T53553): triaged normal,
    solution pending

**Master Commits**

  - Fix T53650: remove hard limits on force field size and max distance
    [rBc6abf41f](https://projects.blender.org/blender/blender/commit/c6abf41f71210b56c9484bf6bbd0df90d9c93fdc)

## Week 2 (January 8th - 12th)

**Info**

spend time mostly triaging/investigating tracker reports. Had to refresh
blender-deps and enabled them for some reports (alembic,...) Also tried
to investigate some of the pending solutions more in depth \[only
limited success\]

**Tracker Triaging**

  - [\#53704](http://developer.blender.org/T53704): confirmed,
    investigated, solution pending
  - [\#53708](http://developer.blender.org/T53708): confirmed, campbell
    investigated and closed as invalid (corrupt input file)
  - [\#53716](http://developer.blender.org/T53716): triaged incomplete
    (missing info), response pending
  - [\#53732](http://developer.blender.org/T53732): closed (graphics
    driver bug)
  - [\#53711](http://developer.blender.org/T53711): confirmed, assigned
    sybren (alembic), solution pending
  - [\#50227](http://developer.blender.org/T50227): reopened (could
    confirm the issue), assigned sybren (alembic), solution pending
  - [\#53745](http://developer.blender.org/T53745): confirmed, assigned
    sybren (alembic), solution pending
  - [\#53679](http://developer.blender.org/T53679): closed (more than a
    week without reply)
  - [\#53677](http://developer.blender.org/T53677): closed (more than a
    week without reply)
  - [\#53676](http://developer.blender.org/T53676): closed (more than a
    week without reply)
  - [\#53746](http://developer.blender.org/T53746): triaged incomplete
    (missing info), response pending
  - [\#53733](http://developer.blender.org/T53733): \[LONG\] confirmed,
    investigated, solution pending
  - [\#53744](http://developer.blender.org/T53744): confirmed, campbell
    moved to todo
  - [\#53701](http://developer.blender.org/T53701): confirmed, tested
    provided patch, bjornmose commited and closed
  - [\#53692](http://developer.blender.org/T53692): assigned sergey
    (cycles), brecht fixed and closed
  - [\#53700](http://developer.blender.org/T53700): assigned aligorith,
    solution pending
  - [\#53710](http://developer.blender.org/T53710): confirmed, assigned
    campbell, he closed as todo
  - [\#53690](http://developer.blender.org/T53690): triaged incomplete
    (graphics driver issue), response pending
  - [\#53596](http://developer.blender.org/T53596): investigated (cannot
    reproduce), added Windows tag
  - [\#53582](http://developer.blender.org/T53582): triaged incomplete
    (file renders fine here), response pending
  - [\#53455](http://developer.blender.org/T53455): investigated, found
    commit causing this, assigned campbell, he fixed and closed
  - [\#53751](http://developer.blender.org/T53751): investigated, closed
    as invalid (not a bug)
  - [\#53752](http://developer.blender.org/T53752): investigated,
    suggested patch \[made a commit -- see Master Commits\], but WARNING
    / OOPS: there are issues with it and should be reverted for now...
  - [\#53754](http://developer.blender.org/T53754): moved to todo (not a
    bug per se), but want to discuss with campbell also
  - [\#53757](http://developer.blender.org/T53757): triaged incomplete
    (missing info), response pending
  - [\#53240](http://developer.blender.org/T53240): closed/archived
    (graphics driver bug and no response)
  - [\#53680](http://developer.blender.org/T53680): investigated more,
    got closer, but this was a time-sink also..., gave up and assigned
    campbell for now
  - [\#53762](http://developer.blender.org/T53762): triaged incomplete
    (caused by addon, which one? response pending)
  - [\#53765](http://developer.blender.org/T53765): triaged incomplete
    (missing info), response pending
  - [\#53766](http://developer.blender.org/T53766): confirmed, solution
    pending
  - [\#53768](http://developer.blender.org/T53768): investigated,
    suggested workaround
  - [\#53258](http://developer.blender.org/T53258): closed (more than a
    week without reply)
  - [\#53697](http://developer.blender.org/T53697): closed (more than a
    week without reply)
  - [\#53353](http://developer.blender.org/T53353): closed (more than a
    week without reply)

**Master Commits**

  - Fix T53752: Draw curve fails w/ stylus
    [rBd0cc5d89](https://projects.blender.org/blender/blender/commit/d0cc5d89485f0c34cd3b79752320a40c140af050)
    WARNING / OOPS: there are issues with it and should be reverted for
    now...

**Next Week**

I'd really like to spend more time on actually fixing (pending previous
but also older bugs), but priority will still be triaging what's
incoming/already there... Also need to make entries in the wiki TODO for
the things I marked as TODO

## Week 3 (January 15th - 19th)

**Info**

More time was spent on reading code and trying to fix bugs myself \[with
limited success in the timespan of 1hr / report...\]. Most of the time I
hopped on tho other reports if I hit that timelimit (marked by
'\[LONG\]'). Had a day off on tuesday but made up for this on Saturday.
Summary: involved in 30 reports, 4 longer investigations

**Tracker Triaging**

  - [\#53744](http://developer.blender.org/T53744): \[LONG\]
    investigated, solution halfway there...
  - [\#53791](http://developer.blender.org/T53791): confirmed, solution
    pending
  - [\#53788](http://developer.blender.org/T53788): assigned to sergey,
    he closed
  - [\#53792](http://developer.blender.org/T53792): confirmed, some
    investigation, solution pending
  - [\#53786](http://developer.blender.org/T53786): \[LONG\] some
    investigation, asked campbell, he fixed and closed
  - [\#53814](http://developer.blender.org/T53814): invesigated, not
    reproducable
  - [\#53812](http://developer.blender.org/T53812): investigated,
    assigned mont29, he fixed and closed
  - [\#53809](http://developer.blender.org/T53809): investigated, not a
    bug, suggested solution, reporter then closed
  - [\#53813](http://developer.blender.org/T53813): checked, was already
    fixed in master
  - [\#53816](http://developer.blender.org/T53816): checked, suggested
    'nota bug', brecht as well and closed
  - [\#53746](http://developer.blender.org/T53746): closed (more than a
    week without reply)
  - [\#53628](http://developer.blender.org/T53628): \[LONG\] dived into
    dupli(group) code but 1hr was not enough to find cause/solution
    yet...
  - [\#53820](http://developer.blender.org/T53820): closed (3rd party
    addon)
  - [\#53821](http://developer.blender.org/T53821): investifated,
    assigend mont29, solution pending / is TODO
  - [\#53827](http://developer.blender.org/T53827): closed after short
    investigation (not a bug)
  - [\#53823](http://developer.blender.org/T53823): confirmed, made
    crash-preventing patch, assigned campbell who fixed and closed
  - [\#53826](http://developer.blender.org/T53826): made sure every
    information is there, brecht took over (OSX issue)
  - [\#53828](http://developer.blender.org/T53828): investigated,
    moderated, bastien/campbell decided to remove old version
  - [\#53773](http://developer.blender.org/T53773): too little info, no
    response after suggestions, closed
  - [\#53768](http://developer.blender.org/T53768): claimed (py API
    addition), pending
  - [\#53829](http://developer.blender.org/T53829): closed, spam
  - [\#53802](http://developer.blender.org/T53802): invesigated,
    assigned (but will likely fix myself if still enough time)
  - [\#53783](http://developer.blender.org/T53783): invesigated,
    assigned, howardt already had this TODO in another task and closed
  - [\#53757](http://developer.blender.org/T53757): closed (more than a
    week without reply)
  - [\#53758](http://developer.blender.org/T53758): closed (more than a
    week without reply)
  - [\#53745](http://developer.blender.org/T53745): some more
    investigation (testing different file)
  - [\#53838](http://developer.blender.org/T53838): confirmed, assigned
  - [\#53246](http://developer.blender.org/T53246): investigated code,
    found cause, assigned sergey, pending
  - [\#53839](http://developer.blender.org/T53839): investigated, but
    missing information
  - [\#53763](http://developer.blender.org/T53763): \[LONG\]
    investigating custom normals code, assigned mont29 in the who closed
    as invalid (known limitation)

**Next Week**

General rate of incoming reports leaves about 4-5hrs \[out of 20\] of
longer investigations \[diving deeper in code\] for selected reports. I
have a couple of these still with pending results. Try to finish these
off while still keeping the priority of handling incoming (new) reports.

## Week 4 (January 22nd - 26th)

**Info**

Still struggeling to get to good results investigating certain reports
when constraining to 1-1.5hrs max (which is a good limit though to get
other stuff done)

Summary: involved in 37 reports, 4 longer investigations, 3 master
commits

**Tracker Triaging**

  - [\#53835](http://developer.blender.org/T53835): \[LONG\]
    investigated, found cause, assigned sergey
  - [\#53841](http://developer.blender.org/T53841): investigated,
    suggested patch, mont29 committed similar \[somewhat made a commit
    -- see Master Commits\]
  - [\#53801](http://developer.blender.org/T53801): checked, couldnt
    reproduce
  - [\#53843](http://developer.blender.org/T53843): confirmed, suggested
    patch, assigned campbell who fixed differently
  - [\#53842](http://developer.blender.org/T53842): assigned luciorossi
    who closed as invalid
  - [\#53845](http://developer.blender.org/T53845): \[LONG\]
    investigation, hit time limit, move on...
  - [\#53846](http://developer.blender.org/T53846): incomplete report,
    asked for specific info, closed later (no answer)
  - [\#53624](http://developer.blender.org/T53624): investigated,
    suggested solution, talked to lijenstina et.al, \[committed -- see
    Master Commits\]
  - [\#53861](http://developer.blender.org/T53861): closed spam
  - [\#53861](http://developer.blender.org/T53861): closed spam
  - [\#53870](http://developer.blender.org/T53870): triaged incomplete
    suggested something to try, no answer yet
  - [\#53871](http://developer.blender.org/T53871): triaged incomplete
    suggested something to try, no answer yet
  - [\#53856](http://developer.blender.org/T53856): added similar
    reports
  - [\#53766](http://developer.blender.org/T53766): investigated,
    suggested fix, talked to nBurn \[committed -- see Master Commits\]
  - [\#53779](http://developer.blender.org/T53779): closed (more than a
    week without reply)
  - [\#53793](http://developer.blender.org/T53793): closed (more than a
    week without reply)
  - [\#53796](http://developer.blender.org/T53796): closed (more than a
    week without reply)
  - [\#53797](http://developer.blender.org/T53797): closed (more than a
    week without reply)
  - [\#53686](http://developer.blender.org/T53686): \[LONG\] more
    investigations, got closer to cause but hit time limit, asked
    brecht/sergey instead
  - [\#53884](http://developer.blender.org/T53884): first incomplete,
    after more information: confirmed, assigned sybren
  - [\#53882](http://developer.blender.org/T53882): checked, was already
    fixed in master
  - [\#53595](http://developer.blender.org/T53595): checked, assigned
    luciorossi who closed as invalid
  - [\#50620](http://developer.blender.org/T50620): checked, involved
    campbell, probably todo rather then bug
  - [\#53886](http://developer.blender.org/T53886): confirmed, brecht
    resolved (tradeoff simple vs complex scene)
  - [\#53887](http://developer.blender.org/T53887): confirmed, assigned
    nexyon
  - [\#53888](http://developer.blender.org/T53888): confirmed, assigned
    nexyon
  - [\#53363](http://developer.blender.org/T53363): checked, assigned
    mont29 who fixed
  - [\#53904](http://developer.blender.org/T53904): \[LONGISH\] more a
    todo, but had a closer look at view code, no real result yet
  - [\#53902](http://developer.blender.org/T53902): confirmed, brecht
    closed as duplicate
  - [\#53615](http://developer.blender.org/T53615): discussing the
    report, no real solution yet
  - [\#53911](http://developer.blender.org/T53911): closed spam
  - [\#53891](http://developer.blender.org/T53891): checked, not
    reproducable, external addon? yes
  - [\#53907](http://developer.blender.org/T53907): marked incomplete
  - [\#53909](http://developer.blender.org/T53909): checked code
    closely, assigned aligorith
  - [\#53912](http://developer.blender.org/T53912): no bug but todo,
    suggested workaround
  - [\#53773](http://developer.blender.org/T53773): closed (more than a
    week without reply)
  - [\#53846](http://developer.blender.org/T53846): closed (more than a
    week without reply)

**Master Commits**

  - Fix T53841: Can not import .obj or .fbx generated from Marvelous
    Designer
    [rBA29ae03c](https://projects.blender.org/blender/blender-addons/commit/29ae03c572bda6278f5a515eb56f6bdd27296c1a)
  - fix T53624: A.N.T. Landscape should not be operating in edit mode
    [rBA87579bd](https://projects.blender.org/blender/blender-addons/commit/87579bd164c60ac7e6861be49bc7bcd4b84641ce)
  - fix T53766: MeasureIt measurements not appearing on all layers an
    object exists on
    [rBAdc6704a](https://projects.blender.org/blender/blender-addons/commit/dc6704ab42317256a0cad14728f275ee542f9904)

## Week 14 (April 2nd - 6th)

**Info**

Most time spent triaging incoming reports, also took a first round of
looking at older open/untriaged reports. Summary: involved in 46 reports

**Tracker Triaging**

  - [\#54468](http://developer.blender.org/T54468): sugested better
    python usage, crash involved needs confirmation still
  - [\#54485](http://developer.blender.org/T54485): confirmed, claimed,
    looked at code a bit, but postponed
  - [\#54482](http://developer.blender.org/T54482): no real bug, but
    asked @severin as well, not closed yet
  - [\#54489](http://developer.blender.org/T54489): confirmed, not
    solved yet
  - [\#54490](http://developer.blender.org/T54490): cannot reproduce,
    special hardware, needs triage still
  - [\#54502](http://developer.blender.org/T54502): investigated, asked
    for more information (no answer, later closed)
  - [\#54495](http://developer.blender.org/T54495): investigated,
    suggested to try it the right way (no answer, later closed)
  - [\#54497](http://developer.blender.org/T54497): asked reporter to
    try stuff, he then could resolve
  - [\#54504](http://developer.blender.org/T54504): investigated, found
    cause and workaround but not solution, assigned brecht
  - [\#54515](http://developer.blender.org/T54515): investigated,
    clarified, mont29 closed then
  - [\#54445](http://developer.blender.org/T54445): investigated, could
    not reproduce, user could resolve, closed then
  - [\#54511](http://developer.blender.org/T54511): investigated,
    suggested workaround, user happy, still need proper code fix
  - [\#54517](http://developer.blender.org/T54517): incomplete, no
    answer (closed later)
  - [\#54507](http://developer.blender.org/T54507): confirmed, assigned
    @aligorith, solution pending
  - [\#54506](http://developer.blender.org/T54506): assigned @akitula,
    solution pending
  - [\#54519](http://developer.blender.org/T54519): suggested
    workaround, assigned @severin, solution pending
  - [\#54520](http://developer.blender.org/T54520): checked, couldnt
    reproduce, but archived (as is 2.8 report)
  - [\#53745](http://developer.blender.org/T53745): added information
    given in IRC
  - [\#54442](http://developer.blender.org/T54442): confirmed, claimed,
    but not high prio atm
  - [\#54521](http://developer.blender.org/T54521): checked, suggested
    better .blend but closed as not a bug
  - [\#54493](http://developer.blender.org/T54493): incomplete, asked
    for info, user could resolve, closed then
  - [\#54494](http://developer.blender.org/T54494): confirmed, also
    checked suggested patch, campbell did real fix
  - [\#54524](http://developer.blender.org/T54524): investigated, found
    causing commit, assigned @severin, solution pending
  - [\#54525](http://developer.blender.org/T54525): investigated,
    claimed, suggested patch, review pending
  - [\#54528](http://developer.blender.org/T54528): investigated,
    suggested better/correct usage, closed as not a bug
  - [\#54526](http://developer.blender.org/T54526): investigated,
    claimed, solution pending
  - [\#54527](http://developer.blender.org/T54527): confirmed, @blendify
    fixed later
  - [\#54529](http://developer.blender.org/T54529): checked, found user
    error, closed as not a bug
  - [\#54532](http://developer.blender.org/T54532): investigated, asked
    for more information, Tamito Kajiyama took over later and closed as
    duplicate
  - [\#54057](http://developer.blender.org/T54057): closed (more than a
    week without reply)
  - [\#54291](http://developer.blender.org/T54291): closed (more than a
    week without reply)
  - [\#54307](http://developer.blender.org/T54307): closed (factory
    startup fixed issue in report)
  - [\#54318](http://developer.blender.org/T54318): needs triage again
    (user provided more info since incomplete triage)
  - [\#54355](http://developer.blender.org/T54355): closed as resolved
    (user feedback suggested issue is gone)
  - [\#54390](http://developer.blender.org/T54390): closed as resolved
    (user feedback suggested issue is gone)
  - [\#54394](http://developer.blender.org/T54394): closed as resolved
    (user feedback suggested issue is gone)
  - [\#54464](http://developer.blender.org/T54464): investigated, could
    not reproduce, solution pending
  - [\#54469](http://developer.blender.org/T54469): closed as invalid
    (external addon)
  - [\#54258](http://developer.blender.org/T54258): checked, could not
    reproduce, no answer, @lijenstina closed later
  - [\#54201](http://developer.blender.org/T54201): confirmed, claimed,
    will look into it later
  - [\#54303](http://developer.blender.org/T54303): triaged incomplete,
    @lijenstina closed later
  - [\#54352](http://developer.blender.org/T54352): needs triage again
    (user provided more info since incomplete triage)
  - [\#54294](http://developer.blender.org/T54294): closed as not a bug
    (with explanation to reporter what the misunderstanding was)
  - [\#54405](http://developer.blender.org/T54405): closed as archived
    (was on TODO already)
  - [\#54423](http://developer.blender.org/T54423): investigated,
    suggested code change, mont29 commited and closed later
  - [\#54536](http://developer.blender.org/T54536): investigated,
    suggested code change, mont29 commited and closed later

## Week 15 (April 9th - 13th)

**Info**

continued with triaging incomming reports, some more cleanup in older
reports Summary: involved in 34 reports, 1 master commit

**Tracker Triaging**

  - [\#54535](http://developer.blender.org/T54535): incomplete, asked
    for more info, no answer, closed later
  - [\#54541](http://developer.blender.org/T54541): investigated,
    clarified, claimed for the time being, solution pending
  - [\#54544](http://developer.blender.org/T54544): investigated,
    suggested substitution of code to @campbellbarton,solution pending
  - [\#54546](http://developer.blender.org/T54546): investigated, found
    code cause, assigned @campbellbarton, solution pending
  - [\#54547](http://developer.blender.org/T54547): investigated, found
    (user) cause, suggested better usage, closed as not a bug
  - [\#54548](http://developer.blender.org/T54548): investigated,
    explained cause in detail, mont29 then closed as not a bug
  - [\#54549](http://developer.blender.org/T54549): investigated, made
    patch \[committed -- see Master Commits\]
  - [\#54552](http://developer.blender.org/T54552): investigated,
    explained cause in detail, then closed as not a bug
  - [\#54257](http://developer.blender.org/T54257): \[LONG\]
    investigated in detail, archived as 'known bug, won't fix in 2.7x,
    will be fixed in 2.8'
  - [\#54557](http://developer.blender.org/T54557): first incomplete,
    but user could verify it was caused by external addon, closed then
  - [\#54553](http://developer.blender.org/T54553): confirmed, found
    relating issues, possibly fixed now, will need to recheck
  - [\#54228](http://developer.blender.org/T54228): confirmed, detailed
    report, assigned @howardt, solution pending
  - [\#54558](http://developer.blender.org/T54558): \[LONGISH\]
    confirmed, found causing commit, assigned @campbelbarton in the end
  - [\#54563](http://developer.blender.org/T54563): investigated, closed
    as feature request
  - [\#54567](http://developer.blender.org/T54567): investigated, closed
    as already fixed in master
  - [\#54560](http://developer.blender.org/T54560): \[LONGISH\]
    investigated, suggested possible patch workaround, but no easy fix,
    @sergey closed as TODO
  - [\#54509](http://developer.blender.org/T54509): closed (more than a
    week without reply)
  - [\#54517](http://developer.blender.org/T54517): closed (more than a
    week without reply)
  - [\#54502](http://developer.blender.org/T54502): closed (more than a
    week without reply)
  - [\#54495](http://developer.blender.org/T54495): closed (more than a
    week without reply)
  - [\#54478](http://developer.blender.org/T54478): needs triage again
    (user provided more info since incomplete triage)
  - [\#54352](http://developer.blender.org/T54352): needs triage again
    (user provided more info since incomplete triage)
  - [\#54080](http://developer.blender.org/T54080): incomplete, asked
    for more information
  - [\#54000](http://developer.blender.org/T54000): needs triage again
    (user provided more info since incomplete triage)
  - [\#53979](http://developer.blender.org/T53979): needs triage again
    (user provided more info since incomplete triage)
  - [\#53801](http://developer.blender.org/T53801): closed (wasnt closed
    though issue seems resolved)
  - [\#54569](http://developer.blender.org/T54569): investigated,
    explained somewhat hidden reason, closed as not a bug
  - [\#54578](http://developer.blender.org/T54578): investigated, found
    causing commit, assigned @campbellbarton, solution pending (though
    fix might be in already)
  - [\#54559](http://developer.blender.org/T54559): \[LONGISH\]
    investigated, but dont think this is a bug, asked@sergey, solution
    pending
  - [\#54554](http://developer.blender.org/T54554): \[LONGISH\]
    investigated, confirmed, also confirmed user provided patch,
    solution pending
  - [\#54585](http://developer.blender.org/T54585): \[LONGISH\]
    investigated, confirmed, found causing commit, assigned @nBurn (who
    fixed and closed)
  - [\#54581](http://developer.blender.org/T54581): \[LONGISH\]
    investigated, confirmed, assigned @sergey in the end
  - [\#54580](http://developer.blender.org/T54580): investigated,
    detailed report, asked @campbellbarton for advice, solution pending
  - [\#54593](http://developer.blender.org/T54593): investigated,
    detailed report, assigned @campbellbarton (who fixed and closed)

**Master Commits**

  - fix T54549: Vector Transform node in wrong nodeclass
    [rB5a3c1466](https://projects.blender.org/blender/blender/commit/5a3c146652b7e5d1b5e1b45bdced0d8c2d3c2554)

## Week 16 (April 16th - 20th)

**Info**

continued with triaging incomming reports, some more cleanup in older
reports Summary: involved in 48 reports, 1 master commit

**Tracker Triaging**

  - [\#54390](http://developer.blender.org/T54390): closed, was resolved
    by user actually
  - [\#54535](http://developer.blender.org/T54535): closed (more than a
    week without reply)
  - [\#54257](http://developer.blender.org/T54257): closed (after some
    of my investigation mont29 then also asked sergey and this will be
    solved by 2.8 CoW)
  - [\#54578](http://developer.blender.org/T54578): closed (checked, and
    was also resolved by a previous campbellbarton commit)
  - [\#54553](http://developer.blender.org/T54553): closed (checked, and
    was also resolved by a previous campbellbarton commit)
  - [\#54602](http://developer.blender.org/T54602): closed (checked, and
    was also resolved by a previous campbellbarton commit)
  - [\#54598](http://developer.blender.org/T54598): closed (2.8 report)
  - [\#54610](http://developer.blender.org/T54610): investigated, then
    closed as duplicate
  - [\#54637](http://developer.blender.org/T54637): investigated in some
    depth (UI code), claimed, but solution pending
  - [\#54597](http://developer.blender.org/T54597): investigated in some
    depth (bmesh op), claimed, but solution pending
  - [\#54684](http://developer.blender.org/T54684): investigated, was
    corrupted UVs, no answer how these were imported/generated, closed
    later
  - [\#54673](http://developer.blender.org/T54673): investigated,
    confirmed and claimed, solution pending \[might as well be corrupt
    .lwo\] but needs another look
  - [\#54671](http://developer.blender.org/T54671): investigated, closed
    as archived, suggested workaround \[cycles terminator issue\]
  - [\#54594](http://developer.blender.org/T54594): confirmed with some
    further investigation, assigened to sergey (busy)
  - [\#54584](http://developer.blender.org/T54584): confirmed, assigned
    campbellbarton (undo rewrite), might have another look myself if
    time permits
  - [\#54698](http://developer.blender.org/T54698): closed (feature
    request)
  - [\#54692](http://developer.blender.org/T54692): triaged incomplete,
    no answer, lijestina closed later
  - [\#54700](http://developer.blender.org/T54700): triaged incomplete,
    example file came a lot later, needs triage now actually
  - [\#54703](http://developer.blender.org/T54703): investigated but is
    a limitation of alembic, added to TODO
  - [\#54551](http://developer.blender.org/T54551): confirmed (though
    similar report existed), assigned sergey but he archived as not a
    bug
  - [\#54397](http://developer.blender.org/T54397): confirmed (surface
    deform modifier violating modifier stack), assigned lucarood, but no
    answer yet (might also have a go at it)
  - [\#54583](http://developer.blender.org/T54583): mixed report (user
    error which I explained) and graphics glitches (which I marked
    incomplete, no answer, lijenstina closed later)
  - [\#54704](http://developer.blender.org/T54704): was fixed already,
    but Chaosgroup didnt update their version of our addon, closed
  - [\#54701](http://developer.blender.org/T54701): second part of
    report was valid, comited fix (see master commits)
  - [\#54482](http://developer.blender.org/T54482): archived (design of
    scale manipulator), redirected to 2.8 design task
  - [\#54410](http://developer.blender.org/T54410): confirmed, found
    causing commit, assigned campbell, solution pending \[could also
    look into it\]
  - [\#54314](http://developer.blender.org/T54314): \[LONGISH\]
    interesting problem that boils down to have py access to renderings
    etc, claimed as TODO
  - [\#54478](http://developer.blender.org/T54478): could not reproduce,
    but newest blender version also solved it for user apparently,
    closed
  - [\#54542](http://developer.blender.org/T54542): closed (not a bug,
    there are options to control this already)
  - [\#54459](http://developer.blender.org/T54459): confirmed and
    claimed, but solution pending, needs a second look
  - [\#54729](http://developer.blender.org/T54729): not a valid report,
    closed later
  - [\#54730](http://developer.blender.org/T54730): more of a question
    about 2.8, closed
  - [\#54340](http://developer.blender.org/T54340): fancy hardware,
    asked for more information
  - [\#54358](http://developer.blender.org/T54358): confirmed and
    claimed for the time being \[needs a second thourough look later\]
  - [\#54734](http://developer.blender.org/T54734): cannot reproduce,
    even after second answer
  - [\#54736](http://developer.blender.org/T54736): closed, 2.8 report
  - [\#54735](http://developer.blender.org/T54735): marked incomplete,
    no answer, lijenstina closed later
  - [\#54349](http://developer.blender.org/T54349): more of a TODO, but
    claimed, no patch yet though
  - [\#53621](http://developer.blender.org/T53621): older report, one
    suggestion was there, but no answer anymore... closed later
  - [\#54080](http://developer.blender.org/T54080): closed (more than a
    week without reply)
  - [\#54574](http://developer.blender.org/T54574): closed (more than a
    week without reply)
  - [\#54579](http://developer.blender.org/T54579): needs triage again
    (user provided more info since incomplete triage)
  - [\#54321](http://developer.blender.org/T54321): confirmed and
    claimed, simple fix will do later
  - [\#54341](http://developer.blender.org/T54341): more a TODO, but
    claimed (sergey approved to work on it), patch follows (needs some
    cleanup still)
  - [\#54346](http://developer.blender.org/T54346): cannot reproduce,
    marked incomplete, lijenstina closed later due to no answer
  - [\#54325](http://developer.blender.org/T54325): \[LONGISH\]
    investigation, claimed, but no solution yet
  - [\#54743](http://developer.blender.org/T54743): confirmed, assigned
    campbell (undo refactor)

**Master Commits**

  - Material Library VX: fix error when clearing category
    [rBA8d3301f](https://projects.blender.org/blender/blender-addons/commit/8d3301f66c77e7b5a1f310a311d2055dbd473e09)

## Week 17 (April 23rd - 27th)

**Info**

continued with triaging incomming reports, some more cleanup in older
reports. Also gathered a couple of claimed reports now that I also
prepared some patches for. Next week I want to push some of these.
Untriaged reports are down from 70 to around 40 now, some remaining are
a bit fuzzy, but these also have to go down next week...

**Tracker Triaging**

Will provide info in a more compressed form from now on, going over
every report again seems more timeconsuming than its benifits justify.

Summary: involved in 38 reports, 3 master commits

**Master Commits**

  - Fix T54789: A Simple Typo in the "Node Wrangler" Addon
    [rBAb678ef7](https://projects.blender.org/blender/blender-addons/commit/b678ef752f129647e94bf21eb72c0fd30428d1ba)
  - Fix T54803: Materials Library VX: follow strict class naming
    [rBA9e97716](https://projects.blender.org/blender/blender-addons/commit/9e97716c99af066ba578c8ac5685a7e92a363b59)
  - Fix discontinuity in default UVs for a torus
    [rBd37dcc48](https://projects.blender.org/blender/blender/commit/d37dcc488045242cabd15b1e4e6df95f4ac1c508)

## Week 18 (April 30th - May 4th)

**Info**

continued with triaging incomming reports, some more cleanup in older
reports. Also going over claimed reports now.

**Tracker Triaging**

Will provide info in a more compressed form from now on, going over
every report again seems more timeconsuming than its benifits justify.

Summary: involved in XXX (havent counted yet) reports, 1 master commit

**Master Commits**

  - Fix T54341: Particle Instance Modifier doesn't preserve edge datas
    [rB288d7794](https://projects.blender.org/blender/blender/commit/288d7794d4c6836e1322d5ed943c4fbd4b0b0e54)

## Week 19 (May 7th - May 11th)

**Info**

continued with triaging incomming reports, some more cleanup in older
reports. Also going over claimed reports now.

**Tracker Triaging**

Will provide info in a more compressed form from now on, going over
every report again seems more timeconsuming than its benifits justify.
Summary: involved in XXX (havent counted yet) reports, 5 master commits

**Master Commits**

  - Fix T54324: remove stencil entries from weight paint keymap
    [rB9eb5aa32](https://projects.blender.org/blender/blender/commit/9eb5aa32b843fdc7be5a7be264909170b725874c)
  - Fix T54525: crash when setting number of frames to use between
    dupoff frames
    [rB1f76e6c3](https://projects.blender.org/blender/blender/commit/1f76e6c326f6)
  - Fix Movieclip editors 'graph' and 'dopesheet' view using themes
    region background setting
    [rB66ec3c7e](https://projects.blender.org/blender/blender/commit/66ec3c7e7f9c)
  - Fix T54997: simple typo in property description
    [rB34c474e6](https://projects.blender.org/blender/blender/commit/34c474e69541)
  - Fix T54992: Lattice modifier on another Lattice object does not take
    the

Influence vertexgroup into account
[rB69f23054](https://projects.blender.org/blender/blender/commit/69f2305415a2)

## Week 20 (May 14th - May 18th)

**Info**

continued with triaging incomming reports, some more cleanup in older
reports. Also going over claimed reports now.

**Tracker Triaging**

Will provide info in a more compressed form from now on, going over
every report again seems more timeconsuming than its benifits justify.
Summary: involved in XXX (havent counted yet) reports, 2 master commits

**Master Commits**

  - Fix T55065: lipSynch Importer not handling relative file paths
    correctly
    [rBA4747021](https://projects.blender.org/blender/blender-addons/commit/474702157831f1a58bb50f5240ab8b1b02b6ba37)
  - Fix T55115: crash when iterating SmokeDomainSettings color\_grid
    property through python
    [rB5e7a21a5](https://projects.blender.org/blender/blender/commit/5e7a21a5f549f5db73385820cafcd8f2bb9c91da)

## Week 21 (May 21st - May 25th)

**Info**

continued with triaging incomming reports, some more cleanup in older
reports. Also going over claimed reports now.

Note: one public holiday, also took one day off

**Tracker Triaging**

Will provide info in a more compressed form from now on, going over
every report again seems more timeconsuming than its benifits justify.
Summary: involved in XXX (havent counted yet) reports, 3 master commits

**Master Commits**

  - Fix T54336: Extend property of Lasso select tool in Node editor does
    not work
    [rB54f2e584](https://projects.blender.org/blender/blender/commit/54f2e58452bba1db8c50d26ca6999473fa45809d)
  - Fix Extend property of Lasso select tool in Mask editor not working
    [rB3e9b592b](https://projects.blender.org/blender/blender/commit/3e9b592b08b614e29acdfd7906f572953c528378)
  - Fix T55034: Setting duplication group for multiple selected items
    only affects one item
    [rB1318660b](https://projects.blender.org/blender/blender/commit/1318660b0449b6e6afe6d349826af89005bc29dd)

## Week 22 (May 28st - June 1st)

**Info**

continued with triaging incomming reports, some more cleanup in older
reports. Also going over claimed reports now. Also started porting old
wiki over to new wiki (see T48096) and started looking into porting over
the TODO list to phabricator.

**Tracker Triaging**

Will provide info in a more compressed form from now on, going over
every report again seems more timeconsuming than its benifits justify.
Summary: involved in XXX (havent counted yet) reports
