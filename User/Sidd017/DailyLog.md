## Daily Reports

I plan on writing daily reports throughout the course of my GSoC
project. While it is not required to do so, I would like to maintain
this to keep track of my daily progress and make sure that I'm able to
keep up with approaching deadlines.

### Results announced

I've successfully passed GSoC 2021. While GSoC is over, there's still
some work left for me to do before I can start working on getting my
code merged. In his GSoC review, my mentor did mention that progress
slowed down during the final weeks. I really hope that I can speed
things up ASAP, and improve the code quality that I produce.

For the first week of September (at least), I will be busy with some
internship assignments, but once that's done I hope to come back and
finish my patch for adding edge selection support for the UV editor. The
current goal for me now is to get my code merged before 3.0 release.

### Final evaluations week

I've completed my final evaluations and submitted my final report that
details everything I worked on during this summer. This week mentors
complete their final review and evaluations, and on 31st August I will
get to know if I have successfully passed GSoC or not.

### Final Week

Summary of work done from August 9 to August 15 - [Weekly
Report](https://devtalk.blender.org/t/gsoc-2021-uv-editor-improvements-weekly-reports/19060/24?u=sidd017)

#### Rest of the week

For the rest of this week I mostly focused on writing my final report
and completing final evaluations. There was some college work that again
kept me occupied for the most part, but I hope that once I'm free from
the upcoming college exams I can solely focus on creating proper patches
of my GSoC branches and set them up for review.

#### August 17

  - Had a final meeting with my mentor today. Did a progress review and
    discussed a few things that needed work/could be improved.
  - Also had an informal conversation with my mentor today which really
    nice. This was probably our last meeting today before GSoC ends.
  - Started with a first draft for my final report.
  - I have been working on clean ups for both my soc branches, but
    haven't been satisfied with it no matter how much time I put into
    it. I guess I will clean it up as best as I can and then get
    feedback from during code review. This is mostly because I need
    developers to criticize my work, else I am just running around in
    circles with my not so professional coding skills/knowledge.

#### August 16

  - Wrote my weekly report
  - Didn't do much today since I had to work for another meeting
    (college work)

### Week 10

Summary of work done from August 2 to August 8 - [Weekly
Report](https://devtalk.blender.org/t/gsoc-2021-uv-editor-improvements-weekly-reports/19060/23?u=sidd017)

#### August 15

  - Committed changes made to the subdividing grid
  - Updated experimental builds, although it seems no users are
    interested in testing and providing feedback for it.
  - Updated the patch for edge selection support

#### August 14

  - Refactored the code for subdividing grid. The grid dimensions can
    now be adjusted to any desirable value.
  - Did some code review, mostly deciding on what to clean up and what
    needs refactoring
  - Started working on the final report. For user documentation I will
    be writing a simple document that explains all the implemented
    features to the end user on by utilizing simple use cases.
  - For code submission, I will have to talk with my mentor about this,
    but I think simple patch using arc diff on the soc branches would
    suffice.

#### August 13

  - Revisited the code written in \`soc-uv-editor-improvements branch\`
  - Cleaned up some code in the soc-uv-edge-select-support branch

#### August 10, 11, 12

Didn't work much since I had some urgent college work that needed my
complete attention.

#### August 9

  - Finally got the buildbot working for windows. Turns out long branch
    names don't work well when packaging
  - Fell sick today so I couldn't work much
  - For next few days (till thursday) I will be occupied with some
    mandatory college work. This will definitely affect the time I will
    spend working on code cleanups and fixes.
  - Since this is the last week of GSoC I will try my best to clean code
    on both soc branches. Especially address these issues :
      - Is subdividing grid required for UV editor? If yes, then clean
        up the code and use proper logic in code/shaders that can be
        easily changed based on the desired grid dimensions (NxN).
      - Make a final call on the new RectBoxPack algorithm. While it
        works, the heuristic for rotating islands used currently, is
        incorrect.
      - The mess made in DNA structs needs to be cleaned up
      - Ask Campbell about versioning code.
      - Should the final code submission be done as separate patches or
        git branches will suffice?

### Week 9

Summary of work done from July 26 to August 1 - [Weekly
Report](https://devtalk.blender.org/t/gsoc-2021-uv-editor-improvements-weekly-reports/19060/21?u=sidd017)

#### August 8

  - Added edge selection support for UV operators :
      - UV edge ring select
      - UV loop select
  - Tested both edge ring and loop select operators to ensure that they
    work as intended in all permutations of UV select modes.
  - Tried pushing for new builds again, but it seems that the edge
    selection branch always fails on windows.
  - Started looking into how final code submission should be done for
    GSoC final evalautions. For now I am thinking about creating 3
    separate patches :
      - Separate patch for all improvements/changes to packing UV
        islands and offsetting selected UVs
      - Separate patch for all improvements/changes to UV grid and
        snapping
      - Separate patch for all improvements/changes to UV edge
        selection.

#### August 3, 4, 5, 6, 7

  - Changed the edge selection branch to
    \`soc-2021-uv-editor-improvements-edge-selection\` and pushed to
    remote repository
  - Created test builds for both soc branches using buildbot. For some
    reason the first builds were crashing and the second builds are
    failing on windows platform. Will try again this weekend.
  - Added edge selection support for a few more operators :
      - Select split
      - Select pinned
  - Fix for select invert operator. The operator now supports UV face
    select mode as well. This overrides the previous behavior where it
    used vertex select mode logic as a fallback.
  - Fixed a bug with edge selection when used with sticky location
    enabled
  - Fixed the logic/bug for select invert operator when using edge
    select and sticky select mode.
  - Apparently edge loop and edge ring select operators for the UV
    editor seemed to be fixed from the previous commits I made. I will
    still look into the code and see if it needs refactoring

#### August 2

  - Updated the diff on d.b.o with the changes I made last week
  - Since I haven't reviewed my soc branch in the past 2 weeks, I'm a
    bit skeptical about pushing builds for users to test. I'll do an
    first pass review tomorrow and check with my mentor to see if I
    should create builds for user testing or not
  - Semester workload is getting heavier and more difficult to manage
    with every passing day.
  - After a quick review, I feel that there are probably a lot more
    areas that require edge selection support than I earlier
    anticipated.

### Week 8

Summary of work done from July 19 to July 25 - [Weekly
Report](https://devtalk.blender.org/t/gsoc-2021-uv-editor-improvements-weekly-reports/19060/20?u=sidd017)

#### July 30, 31 and August 1

  - Worked on adding edge selection support for a bunch of UV selection
    operators :
      - Mouse select
      - Box select
      - Circle select
      - Lasso select
      - Select/deselect all
      - Invert selection
      - Select more/less
  - Added 2 function for flushing selection between UV verts and edges

#### July 29

  - Committed a fix to the bug : deselecting UV faces would deselect
    neighboring faces as well.
  - Worked on refactoring the \`uv\_select\_flush\_from\_tag\_face()\`
    and \`uv\_select\_flush\_from\_tag\_loop()\` functions, but got
    stuck while trying to understand the \`UvVertMap\` and \`UvMapVert\`
    structs. From a initial study I think they create a mapping from the
    UV vertices to the 3D mesh elements which would simplify accessing
    the connected mesh elements for a corresponding UV vertex without
    having to iterate over the entire mesh over and over again.
  - I decided to take a step back and study the selection code for 3D
    viewport. The changes I have made in the UV selection code have
    become extremely messy and I need to organize it a bit better for
    reusability and maintenance.
  - Hopefully I can get started with cleanups tomorrow so that the code
    on my branch can be reviewed before the final week of GSoC.

#### July 26, 27 and 28

  - With the heavy workload for my current semester, things have been
    falling off-schedule this week.
  - I tried refactoring the existing box select and select more/less
    operator to work with the edge selection flag. Got a general
    solution working, but in retrospect the code became a bigger mess to
    keep track of.
  - Discovered a bug while trying to deselect faces in face + sticky
    loc/vertex selection mode. Neighbouring faces would get deselected
    since the new solution would deselect the shred edges between the 2
    faces.

### Week 7

Summary of work done from July 12 to July 18 - [Weekly
Report](https://devtalk.blender.org/t/gsoc-2021-uv-editor-improvements-weekly-reports/19060/19?u=sidd017)

#### July 23 and 24

  - Spent time working on refactoring the existing selection code and
    testing to ensure that the new design works as intended.
  - A few inconsistencies due to the present drawing code for UV edges.
  - From an initial analysis, it seems that the changes in UV selection
    design will impact a lot of Blender addons. Will discuss this with
    my mentor next week.
  - Also, since I expect to spend most of the time working on cleanups
    for the soc branch next week, I'll start looking into versioning
    code tomorrow (hopefully).
  - Started working on refactoring the selection operators

#### July 22

  - Had a meeting with my mentor to discuss some design decisions for UV
    selection involving sticky modes.
  - Tried to look through the drawing code for UV edges again. Couldn't
    get a temporary solution working, but I was able to get a better
    understanding of how the shaders work.
  - Turns out all the shaders collectively draw half an UV edge which
    consists of 4 vertices. Each vertex is assigned a color (orange or
    white) based on the selection state. I'll look into the drawing code
    again once I have completed refactoring the UV selection part

#### July 21

  - Got the UV edge selection code for sticky modes working. Did some
    minor testing and this seems to work perfectly and as expected. This
    took about 5-6 hours to finally get working correctly.
  - I couldn't find time to work on the UV face selection code for
    sticky modes. I had some other work that needed to be completed
    (approaching deadline).
  - Finally understood how BMesh radial and disk cycles work. The
    iterating part is still a bit fuzzy, but I got the general idea of
    how to do it.
  - Tomorrow I plan on completing the UV face selection code for sticky
    modes and then start looking through the rest of the UV selection
    operators.

#### July 20

  - Experimenting with the drawing code didn't go so well. I lost an
    entire night's sleep trying to find the relevant code and trying to
    make sense of the logic used in the shaders. At most I was able to
    disable drawing edges, which didn't really help any use case for
    testing. For now I have abandoned looking through the drawing code
    since I had spent hours looking through the code and getting no
    where.
  - I started looking through the UV select all operator and rewrote the
    logic to use the edge selection flag. The select and deselect all
    parts were easy to do, but the real challenge was working on invert
    selection.
  - I got the invert all part for the vertex and edge selection working.
    Since the primary logic for face and island selection is not that
    well defined, I decided to have the invert all functionality for
    these selection modes fallback to the logic used by vertex
    selection.
  - Also looked into the selection functions for sticky modes and
    realized that the edge and face selection ones need to be rewritten.
    For now I have to go back and read the bmesh documentation to
    understand what a radial structure is and how to iterate through the
    loops associated with a radial structure for a given edge.

#### July 19

  - Looked through the UV lasso select operator code, mostly making
    minor changes to incorporate the edge selection flag. It didn't work
    so well.
  - I'm having trouble trying to manage time for working on my GSoC
    project and managing my current semester workload. Not sure how I'll
    make it through the coming days/weeks.
  - The code responsible for drawing the selections in the UV editor is
    a bit annoying to be honest. Example - In case I have 2 continuous
    vertices selected, but their corresponding edge isn't selected, then
    the edge is still marked orange which visually indicates that the
    edge is selected (while in reality it's not). So I decided to look
    into the drawing code and find a temporary solution so that I can
    visually identify unselected edges.

### Week 6

Summary of work done from July 5 to July 11 - [Weekly
Report](https://devtalk.blender.org/t/gsoc-2021-uv-editor-improvements-weekly-reports/19060/18?u=sidd017)

#### July 18

  - Made some changes to the primary selection functions for vertices,
    edges and faces in \`uvedit\_select.c\`. Mostly ensuring that the
    edge selection flag for all edge cases was properly addressed
  - Spent time testing and refining those changes since selection code
    for UV editor has many edge cases. Not everything works correctly
    and I still have to do a lot more testing, but this seems like a
    good start. Primary tests that I focused on were simple permutations
    of :
      - Element selection - Vertex, Edge, Face and Island
      - Sticky selection - Disabled, location and vertex
      - UV sync selection (this part was mostly untouched since that
        directly uses BMesh information)

#### July 17

  - Passed first GSoC evaluation.
  - Pushed the code for \`T78405\` to the soc branch. For now I haven't
    added any constraints for checking available UDIM space or if
    dynamic grid is enabled or not.
  - There's a bug where the operator doesn't work in certain use cases
    when UV Sync selection is enabled.
  - I finally started to look into the task for adding edge selection
    support to the UV editor. Added the deprecated flag back and started
    studying the code in \`uvedit\_select.c\`. From a quick look I'll
    first have to understand the cases involving Sync selection, sticky
    selection modes and flushing trough UV selections.

#### July 16

  - Final day for filling the GSoC evaluation form.
  - I tried getting some work done, but most of my time was taken up by
    work for my current semester and a series of meetings.
  - Finally I decided upon a working solution for edge select. The idea
    is to associate each MLoopUV with a UV vertex and an UV edge, since
    number of loops in a mesh face are equal to number of edges in
    corresponding UV face.

#### July 15

  - After a brainstorming session for new tasks, I decided to move
    forward with adding edge selection support for UV editor. Active
    element still requires a design task and would be best to work on
    once edge selection is fully supported in the UV editor.
  - Worked on a new operator for \`T78405\` - UV offset operator. I
    don't see many use cases for this and frankly I'm not even sure if
    it's a good addition to the UV editor. But since it's listed in my
    GSoC proposal, I'll complete it anyways.

#### July 14

  - I had a meeting with my mentor where we did a short recap on the
    progress so far and what tasks I should work on next. 2 tasks that
    really stood out and would make great improvements to the UV editor
    were : Active element support and Edge selection support for the UV
    editor. He also gave some guidance on tackling a few issues I had
    with the code on my branch.
  - Figured out how to add keymaps for operators and in general how to
    map RNA props for those keymaps.
  - Also filled out the GSoC evaluation form today.

#### July 13

  - Instead of using BMesh functionality for storing selection history
    in UV editor, I thought of coming up with a workaround that stores
    MLoopUV references in a stack or ListBase. This data structure can
    be used as a container for storing UV selection history, where the
    last entry always denotes the current active UV element.
  - Since the past week has not been very productive, I've decided to
    complete the one final task left on my proposal this week. Though
    it's not a much requested feature and personally I don't see much
    use of it, I'll just complete it for the sake of completing all the
    tasks on my proposal. Once that's done I can move on to working on
    Active element task.
  - Created a new operator for the final task, currently looking into
    how keymaps are added in Blender.

#### July 12

  - Mostly looked into implementation of active element in the 3D
    viewport.
  - Decided to create a small solution for active element pivot option
    in the UV editor that can act as a proof of concept. Might be far
    fetched since this means using the BMesh functionality which is
    currently lacking in the UV editor.
  - Honestly I'm a bit stuck, since I cannot decide what to work on
    next. I could always do clean ups on my branch but that is something
    that I'll have to deo towards the end eventually. With evaluations
    approaching I need to pick up the pace again and start committing
    work soon

### Week 5

Summary of work done from June 28 to July 4 - [Weekly
Report](https://devtalk.blender.org/t/gsoc-2021-uv-editor-improvements-weekly-reports/19060/17?u=sidd017)

#### July 11

  - Updated progress on the design task and added missing subtasks
  - With most of the deliverables from my proposal finished, I started
    planning on what tasks to tackle next. Possible candidates :
      - Add missing pivot option to UV editor - Active Element
      - Fix the issue with 2D cursor not being reset to UV space origin
        when using keymap - Shift+C
      - Fix the issue with all editors using the same snapping flag
        (\`SCE\_SNAP\`)
  - Started planning on clean ups and refactoring the code on soc branch
    for review

#### July 10

  - Tested the implementation for absolute grid snap and seems to work
    as expected (comparing with 3D viewport).
  - Pushed the implementation to the soc branch.
  - Since first evalution (GSoC) is on Monday, I plane on doing code
    cleanup and fixes. If possible I will also create patches for
    reviews and upload them to phabricator so that my mentor can review
    them easily rather than looking through my entire branch for a
    single feature.
  - Started looking through the changes on soc branch and planning clean
    up. Will begin refactoring and fixes tomorrow.
  - Since I am nearly halfway done with the tasks I had listed in my
    proposal, I'll probably schedule a meet with my mentor for further
    planning on what to do next.

#### July 9

  - Completed a rough implementation for grid snapping (absolute grid
    snap) in UV editor, but there is still a lot of testing to be done
    since I'm pretty confident I have missed many edge cases.
  - One thing that is bothering me is that there is no pivot option for
    active element in the UV editor. This actually defeats the whole
    purpose of having grid snapping for UV editor
  - Currently there are many pending design decisions that are holding
    back progress. I'll have to sort them out this weekend and schedule
    a proper meeting with my mentor/Daniel to resolve them.
  - So far this week has not been as productive as I hoped it would be.
    I'll have to compensate for lost time this weekend.
  - Discovered 2 possible issues :
      - Shift+C doesn't reset the 2D cursor to it's original position
      - \`SCE\_SNAP\` flag is used by many editors in Blender. So if it
        is enabled in one editor, then it is automatically enabled in
        other editors as well.

#### July 8

  - Clarified the design decision with my mentor. For now grid snapping
    shouldn't affect rotate and scale, if required then this can be
    added later. Also a toggle for absolute grid snap in the UV editor
    makes more sense.
  - I tried testing a basic implementation but got stuck due to a build
    error. Mostly spent time trying to fix it, but finally asked for
    help on blender-builds channel. The issue was resolved but I already
    lost about 5-6 hours on this, so I'll probably test the
    implementation tomorrow.

#### July 7

  - Mostly experimented with the code in \`transform\_snap.c\` and
    \`transform\_mode\_translate\`
  - There's a design decision which I'm stuck with. Currently absolute
    grid snap works with translation but isn't intended to work with
    rotation and scaling. As per the UI mockup given in
    [T78391](https://developer.blender.org/T78391) grid snapping must be
    added as a new option in the snapping panel, but I feel that a
    toggle similar to the 3D viewport makes more sense in this case.

#### July 5 and 6

I fell sick and unfortunately my college semester started early (on
Monday), so I couldn't work much. I mostly spent time debugging and
trying trying to trace the call stack for Absolute grid snap used in 3D
viewport. So far I have a rough overview of how translation works.

### Week 4

Summary of work done from June 21 to June 27 - [Weekly
Report](https://devtalk.blender.org/t/gsoc-2021-uv-editor-improvements-weekly-reports/19060/16?u=sidd017)

#### July 4

  - Did some cleanup and refactoring :
      - Replace struct variables with pointers
      - Refactor reusable code to separate functions
  - Since the default UV editor grid is now replaced by a subdividing
    grid and an option for dynamic grid has also been added, the default
    increment snapping value (0.0625) also needs to be changed. Today I
    implemented a solution which changes the increment snapping value
    based on the number of divisions present in the UV editor grid
  - Studied the code related to increment snapping in the UV editor.
    Still struggling with understanding how everything works in the
    bigger picture. I'll look into how absolute grid snap works in the
    3D viewport, that'll probably give me a better idea of how I should
    implement grid snapping in UV editor.

#### July 3

  - Pushed the fix to soc branch
  - Tried doing some cleanup by removing the RectSizeBoxPack struct and
    using the existing BoxPack struct instead. This approach actually
    failed since BoxPack isn't compatible for use with ListBase. Spent
    most of the day trying to come up with a better solution but had to
    stop since I had to work on grid snapping as well
  - Decided to continue working on cleanup after implementing grid
    snapping, since I only have a week left as per my proposal schedule
    to finish snapping.
  - Studied how increment snapping works in the UV editor and how
    absolute grid snap works in 3D viewport

#### July 2

  - Fixed the pending issues with pack islands to area operator :
      - When scale islands is disabled for packing, the islands that
        cannot fit into the packing area area packed at the bottom left
        corner of the packing area.
      - RNA property for displaying packing area coordinates is not
        clamped properly. Maximum coordinates can be set to values lower
        than the minimum coordinates.
  - Added new option for grid snapping in the UV editor, though no
    functionality is tied to it yet.

#### July 1

Couldn't work much today. Was busy with some college work.

  - Daniel Bystedt tested my branch and provided some very valuable
    feedback. In general all implemented features seem to work as
    expected/required, with a small exception of dynamic grid which is
    understandable since the task is only half done. Once I complete
    grid snapping, both features can be expected to work as described in
    their design tasks.
  - Pushed a small change for size factor of dynamic grid. This was done
    based on feedback received from Daniel Bystedt.
  - Tomorrow I plan on studying how absolute grid snap works in the 3D
    viewport and increment snapping in the UV editor. Also work on
    refactoring and fixing parts of the UV packing code.

#### June 30

  - Committed and pushed code for subdividing grid to the soc branch -
    [commit
    link](https://developer.blender.org/rB39aa0062605635be64d5422e54fad2a4ec08d708)
  - Tried uploading a patch for getting the grid code reviewed,
    accidentally uploaded a diff of the entire soc branch against
    master.
  - Since I think my mentor is busy, I'll hold off getting code reviewed
    for sometime. Tomorrow I plan to spend time refactoring the UV
    packing code and doing cleanup and minor bug fixing.

#### June 29

  - Completed implementation of dynamically subdividing grid

#### June 28

  - Investigated how the unit system used affects the Grid in the 3D
    viewport. Since UVs are scaled and packed in 0-1 UV space the
    concept of unit system is irrelevant in the UV editor (only
    preserving aspect ratio of islands is important here). Hence, I
    decided to proceed with using zoomFactor as a control variable.
  - Experimented with different values for zoomfactor to see which one
    makes the grid feel more intuitive to work with. This is mostly hit
    and trial, and will likely result in hardcoded grid divisions but at
    this point this is the best solution I could come up with. Also I
    might use linear interpolation for blend factor in case the default
    cubic interpolation doesn't work well.
  - Looked over the relevant code for increment snapping in the UV
    editor. Will work on adding an option for Grid snapping to the UI
    panel tomorrow.

### Week 3

Summary of work done from June 14 to June 20 - [Weekly
Report](https://devtalk.blender.org/t/gsoc-2021-uv-editor-improvements-weekly-reports/19060/7?u=sidd017)

#### June 27

  - Finally figured it out. The blend factor in the fragment shader is
    always set to 1 when in the UV/Image Editor. This is caused by
    grid\_res which gets initialized to zero.
  - Using a custom control variable (zoomFactor) I was able to replicate
    the fading in and out of the subdivided grid in the UV/Image editor
    similar to the 3D viewport. This is mostly hit and trial so a better
    control variable is required. Writing a patch for this and will
    discuss with my mentor tomorrow
  - Tough part of understanding the overlay engine code is the fact that
    I can't debug glsl shaders.

#### June 26

  - Committed and pushed the Dynamic Grid implementation to the soc
    branch.
  - Spent the entire day trying to understand how the Grid overlay is
    drawn in the 3D viewport. Investigated \`overlay\_grid.c\`,
    \`grid\_vert.glsl\` and \`grid\_frag.glsl\` and found the related
    draw calls, but still unable to replicate the behavior inside the
    UV/Image Editor. Might have to ask my mentor for guidance here.

#### June 25

  - Completed a rough implementation of Dynamic Grid based on the
    description given in the phabricator design task. Reply from Daniel
    Bystedt confirmed that my initial understanding of the task was
    indeed correct.

#### June 24

  - Messaged Daniel Bystedt for clarification on task description for
    Dynamic grid.
  - Couldn't work much since I was facing memory issues with windows.
    Trying to run VSCode or debug Blender would cause both programs to
    crash. The issue resolved itself before I could narrow down the
    apparent reason for this.
  - Implemented a rough version of the dynamic grid based on my
    understanding of the task. I expect this to change based on Daniel
    Bystedt's reply.

#### June 23

  - Did some minor cleanup and fixed an issue of unreleased memory in
    the pack islands to area operator
  - Added operator properties for displaying packing area coordinates
  - Committed and pushed changes to the soc branch
  - Continued studying the code in the overlay\_grid.c
  - Learned how to change the parameters for RNA properties inside an
    operator

#### June 22

  - Worked on some initial user feedback - expose packing area
    coordinates and lock proportions for the packing area
  - Couldn't work much today since I was busy elsewhere.

#### June 21

  - Short meeting with mentor to discuss progress so far.
  - Looked into the task description for Dynamic grid. My initial
    assumption of the task functionality might have been invalid. Will
    contact Daniel Bystedt for clarification. Mentor suggested that I
    could replicate the grid behavior from the graph editor, which seems
    like a nice replacement for the current grid in the UV editor.
  - Created a new devtalk post for feedback.

### Week 2

Summary of work done from June 7 to June 13 - [Weekly
Report](https://devtalk.blender.org/t/gsoc-2021-uv-editor-improvements-weekly-reports/19060/3?u=sidd017)

#### June 20

Took a day off. Had some college work to take care off.

#### June 19

  - Finally gave up and decided that I'll discuss this with my mentor
    for a final call since packing without scaling wasn't even a part of
    the initial design task. Also I've spent an entire week's worth of
    time trying to figure this out.
  - Did some code clean up, committed and pushed the code for pack
    islands to box area - [commit
    link](https://developer.blender.org/rBe5ab28d3921e77bac464c717610e2bf1b5c58b5c).
    Work is still required on this but for now I think I'll just take a
    break to work on dynamic grid and clear my head.

#### June 18

  - Did some more testing, seems like packing with scaling enabled works
    without any issues. Problems start to arise when scaling is
    disabled. Islands that don't fit in the packing area get placed in
    the bottom left corner and overlap with other islands. One solution
    to this would be to set up a callback that cancels the operator when
    such a case arises, but I still feel that a new algorithm could be
    used instead.

#### June 17

  - Finalized the use cases for the box packing and rectPack2D
    algorithms
      - The regular box packing algorithm is useful when packing islands
        that can be scaled. This comes with an added bonus that rotating
        islands is already implemented in code.
      - RectBox2D algorithm is useful when packing islands that can't be
        scaled (user-preference). An additional heuristic is required
        for rotating islands but that can be probably dealt with later.
  - Completed a rough implementation of pack islands to box area
    operator, although it needs to be tested extensively to make sure it
    covers maximum use cases.
  - The code currently looks like a mess. Will do some cleanup and
    probably commit tomorrow.

#### June 16

  - Got the operator UI working as required. Now focusing on
    implementing the packing algorithm given
    [here](https://github.com/TeamHypersomnia/rectpack2D).
  - Had to spend sometime trying to figure out the API for getting the
    screen coordinates for box select area defined by the user. The
    solution was simply using \`UI\_view2d\_region\_to\_view\_rctf()\`.
  - Tried implementing a rough version of the rectPack2D algorithm
    similar to \`BLI\_box\_pack\_2d\_fixedarea()\`. Still requires some
    work but packing without scaling and rotating islands seems to work
    ok provided the area defined for packing is large enough.

#### June 15

  - Got the box select UI working for defining area and since I'm using
    \`WM\_gesture\_box\_modal()\`, the screen coordinates for the
    defined rectangular area get stored in an instance of rctf struct.
    The available API makes it easier since I don't have to code the
    part for getting screen-space coordinates.
  - Instead of directly using \`WM\_gesture\_box\_invoke()\`, I created
    a separate invoke callback. This cancels the operator if no islands
    were selected in the UV editor. If selected then it calls
    WM\_gesture\_box\_invoke().

#### June 14

  - Mostly investigated and studied modal operators that use
    \`WM\_gesture\_box\_invoke()\` and \`WM\_gesture\_box\_modal()\`
    callbacks. The initial goal here is to get the box select part
    working so that it can be used to define the area for packing.
  - Created a new operator \`Pack islands to area\`, though it's mostly
    a placeholder that is an exact copy of the original pack islands
    code.
  - '''Also decided that I'll try to get a basic implementation of my
    tasks working first. ''' Work on cleanup and refactoring code once
    my mentor reviews it. That would save me the trouble of spending too
    much time on tasks without ever getting the satisfaction that it's
    finally done.

### Week 1

Summary of work done from May 17 to June 6 - [Weekly
Report](https://devtalk.blender.org/t/gsoc-2021-uv-editor-improvements-weekly-reports/19060?u=sidd017)

#### June 12 and June 13 (weekend)

I got vaccinated on Saturday (12 June) and was down with fever that kept
swinging between 100°F and 102°F. Tried getting some work done but was
mostly resting due to soreness and body ache.

#### June 11

  - Submitted a minor bug fix :
    [D11582](https://developer.blender.org/D11582) . Also committed and
    pushed this to the \`soc-2021-uv-editor-improvements\` branch.
  - Did some code cleanup, fixed some issues and committed the code for
    task 1 (pack islands to correct/specified UDIM)
  - Started working on task 2 - pack islands to box area

#### June 10

  - Completed the implementation of the first task - pack islands to
    correct/specified UDIM. Will do a code cleanup, fix a minor bug
    related to the DNA default not being assigned, move reusable code to
    utility functions and commit the changes tomorrow
  - Created a new remote repo on GitHub based on Campbell's advice. The
    concept of working with 2 remote repos is still a bit confusing so
    I'll probably spend sometime on weekend trying to figure it out.
  - Rewrote the logic for finding the center of selected UVs in my
    implementation. Previous logic simply took the average of all
    islands' location (weighted average of all faces would have been
    better). New implementation works by considering the bounding box
    center of all selected UVs together.

#### June 9

  - Created a new design task to keep track of my progress :
    [T88990](https://developer.blender.org/T88990)
  - Talked to my mentor (Campbell) and he suggested that I create a
    remote backup of my repo on GitHub. This could have all my temporary
    branches, I could experiment and regularly push changes/work to this
    remote that shouldn't be committed to the
    soc-2021-uv-editor-improvements branch . Will look into this
    tomorrow.
  - Implementation for the first task (pack islands to correct/specified
    UDIM) is half done. Packing to closest UDIM works, while packing to
    specified UDIM still needs work - logic for handling cases of
    specified UDIM not being defined within the current context.
  - I've created a new variable inside the ToolSettings struct, but for
    some reason Blender doesn't assign the DNA default I've specified.
    Needs to be investigated.

#### June 8

  - Created a new local branch : \`soc-2021-uv-editor-improvements\` and
    pushed a test commit to remote. Got the branch verified by my mentor
    (Campbell Barton) and seems to be set up correctly.
  - Ran into a bunch of struct alignment errors when trying to add a new
    int variable to \`Toolsettings\` struct. Found the solution for this
    in \`dna\_genfile.c\` and discovered that I could add a dummy
    variable (\_pad\#\[\]) to fill the memory gap I created when I added
    the new int variable.
  - Cleaned up a bunch of comments I had left when I was studying the
    pack islands code. Mostly ensured they followed the code style
    guide.
  - Created a draft for the GSoC design task. Will create the final task
    on phabricator by tomorrow.

#### June 7

Not much activity today since I had some college work to take care off.

  - Submitted my weekly report for 31 May to 6 June
  - Continued studying the overlay engine code. Figured out how to
    adjust the density of the UDIM grid in the UV editor.
