## GSOC 2021 - Blender UV Editor Improvements

### Name

Siddhartha Jejurkar

### Contact

Email : f20180617@goa.bits-pilani.ac.in

blender.chat nickname : sidd017

Devtalk ID : sidd017

developer.blender.org : sidd017

### Synopsis

Many users have requested features and suggested improvements to certain
existing UV Editor tools. A good subset of the suggested improvements
and smaller requests make ideal tasks that can be accomplished in a GSoC
project. The improvements made will provide more flexibility to the UV
editor for working with UDIM textures, and add some new tools for
speeding up UV Editing workflows.

### Benefits

This project will improve the support for working with UDIM textures in
the UV editor. After the completion of this project users will have a
bunch of new features to work with and find more flexibility to a few
existing tools. Operations such as packing and moving UV islands across
UDIM tiles will be improved, while new options for snapping will also be
added.

### Deliverables

  - Improvements to the Pack islands operator
  - New features and Improvements to Snapping option(s) in the UV editor
      - Dynamic grid
      - New option for grid snapping in the UV editor
  - Features to simplify moving UVs across UDIM tiles
      - Add new key bindings to navigate through UDIM tiles
      - Directly move selected UVs to user-specified UDIM
  - Secondary tasks - features and improvements based on community
    feedback
  - User documentation and notes

### Project Details

#### Improvements to the Pack islands operator :

Presently the operator will pack the selected islands to UDIM 1001, even
if the UV islands were originally placed on a different UDIM tile (see
T78397). Default behavior of the operator will be changed to pack the
selected islands to the same UDIM tile they were originally placed on
and an option will be added to the UI where the user can specify the
target UDIM for packing.  
![UI Mockup](../../images/Pack_islands_UI_mockup.png "UI Mockup") The
UDIM tile numbers/labels can be stored in a data structure (preferably a
ListBase) and the reference to the required UDIM tile specified by the
user can be returned using a search algorithm. Much of the code
implementation will be done in the existing operator inside the
pack\_islands\_exec() function.  
![../../images/Pack\_Islands\_example\_mockup.png](../../images/Pack_Islands_example_mockup.png
"../../images/Pack_Islands_example_mockup.png") Example use case -
Suppose the user selects UV islands present on UDIM 1012, the operator
will by default pack the islands to UDIM-1012 and in case a target UDIM
1002 is specified then they will be packed to 1002 instead.

#### New features and Improvements to UV Editor Snapping option(s):

Presently increment snapping in the UV editor works with fixed values of
0.0625 units and 0.03125 units with the shift key-binding. It would be
beneficial if the user could have more control over the increment values
when working with UDIM tiles of different texture sizes, for example -
1K and 8K.

  - **Dynamic grid**

I will be following the description for the dynamic grid implementation
given in T78389. The basic idea here is that the user can change the
number of grids that make up one UV unit. Note that the original UV unit
length will always remain the same.  
![Dynamic Grid UI mockup](../../images/Dynamic_Grid_UI_mockup.png
"Dynamic Grid UI mockup")  
I expect much of the code implementation for drawing the grid will be
done in the Overlay engine and some changes will be made to the draw
function for the UV grid. Dynamic grid implementation forms the basis
for grid snapping in the UV editor and will be the base for future
implementation of pixel resolution and pixel sapping in the UV editor.

  - **New option for Grid Snapping**

A new option for Grid snapping will be added to the snap settings in the
UV Editor. As the name suggests, selections will snap to the UV grid and
it will also work with the dynamic grid implementation specified
above.  
![../../images/Grid\_snapping\_UI.png](../../images/Grid_snapping_UI.png
"../../images/Grid_snapping_UI.png") Intended behavior for Grid Snapping
will be similar to T78391 and it will also affect rotation and scale of
UV selection. Code implementation for this will be similar to the
absolute grid snap option in 3D viewport and D6970 will be used as a
reference.

  - **Option to change the default increment value**

This option will let the user define the increment value by providing an
option to change the default value (0.0625 units).  
![../../images/Increment\_snapping\_UI.png](../../images/Increment_snapping_UI.png
"../../images/Increment_snapping_UI.png") I will be making changes to
the initSnapSpatial() function defined in transform.c, since this is
where the increment value is assigned.

#### Features to simplify moving UVs across UDIM tiles in UV editor

Presently UV islands can be moved to different UDIM tiles by moving them
freely in UV space. 2 functionalities can be added to make this task
more organised and simpler :

  - Use CTRL+ARROW KEYS to move the selected UVs to neighbouring UDIM
    tiles. Example - pressing CTRL+LEFT will shift the selected UVs to
    the neighbouring UDIM on the left. I will also implement some more
    features with their suggested key bindings described in T78405.
  - Provide an option in the UI where the user can specify the target
    UDIM tile for moving selected UVs to. This is useful when there are
    many UDIM tiles present in the editor (current limit is 10x10=100
    tiles). The UDIM tile numbers/labels can be stored in a data
    structure (preferably a ListBase) and a search algorithm can be used
    to return a reference to the required UDIM tile. Directly specifying
    the target tile would eliminate the need for the user to blindly
    navigate/search for the required UDIM in an already populated UV
    space.

#### Secondary tasks :

This will be a list of secondary targets for improving the UV Editor.
Ideas in this list will be populated based on user/community suggestions
and tasks listed on the UV editing workboard. I will prioritize these
ideas based on discussions with my mentors and begin working on them
during the final weeks(8/9) of GSoC. In case the original deliverables
are completed earlier than expected then I’ll start working on the
secondary tasks directly after. I plan to keep working on these tasks
after the completion of GSoC as well. A few examples of such tasks are :

  - Extend Constrain to image bounds feature to work with UDIM tiles in
    UV editor (see T81247)
  - Pack selected islands to user specified area (T78398)
  - Stitch by matching target edge length (T78400)

### Project Schedule

As per the official University schedule, final exams for the current
semester will conclude on 13th May. Minor interference could be expected
during the final weeks of the coding period as the next semester is
expected to start early (tentatively last week of July or first week of
August).

Until the official announcement (17th May), I will spend time learning
more about the code-base. I will also be working on smaller tasks and
bug fixes listed in the UV editing workboard to familiarize myself with
the existing UV Editor code.

#### Community Bonding period (May 17 to June 7)

During this period I will finalise the details of the tasks with mentors
and developers. I will also be actively collecting more ideas from the
community and RCS to include and in the list of secondary tasks. Since
the time commitment for GSoC this year has been reduced, I will be
prioritising those ideas based on my mentor’s suggestions. These ideas
will be worked upon during the final weeks of GSoC and I’ll continue
working on them after GSoC as well.

Tentative Schedule for the ideas listed in this proposal :

<table>
<thead>
<tr class="header">
<th><p>Week</p></th>
<th><p>Task Details</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1-2</p></td>
<td><ul>
<li>Discuss with mentors and finalize the implementation details for deliverable 1- Improvements to the Pack islands operator</li>
<li>Work on implementation of deliverable 1</li>
<li>Complete implementation of Deliverable 1</li>
</ul></td>
</tr>
<tr class="even">
<td><p>3-4-5</p></td>
<td><ul>
<li>Discuss with mentors and finalize the implementation details for deliverable 2- Improve Snapping option(s) for UV editor</li>
<li>Work on implementation of Dynamic Grid</li>
<li>Work on implementation of grid snapping in UV editor</li>
<li>Complete implementation of Deliverable 2</li>
</ul></td>
</tr>
<tr class="odd">
<td><p>6-7</p></td>
<td><ul>
<li>Discuss with mentors and finalize the implementation details for deliverable 3 - Simplify moving UVs across UDIM tiles in UV editor</li>
<li>Work on implementation of deliverable 3</li>
<li>Complete implementation of Deliverable 3</li>
</ul></td>
</tr>
<tr class="even">
<td><p>8-9</p></td>
<td><ul>
<li>Work on secondary tasks</li>
<li>Buffer period - keeping a light work schedule for this period since the next semester is expected to start around this time</li>
<li>Finish any pending tasks and backlogs</li>
<li>Collect user feedback and bug reports</li>
<li>Fix any major bugs and deviations from the intended behavior</li>
</ul></td>
</tr>
<tr class="odd">
<td><p>10</p></td>
<td><ul>
<li>Code clean up</li>
<li>User documentation and notes</li>
<li>Prepare for final evaluations</li>
</ul></td>
</tr>
</tbody>
</table>

The schedule given above is tentative and I expect tasks to be completed
earlier or take longer than expected. If there are any unanticipated
delays in completion they will be compensated during the buffer period
in week 8-9. In case tasks are completed early then I will begin working
on ideas collected in the list of secondary tasks.

### Bio

I’m Siddhartha Jejurkar, a 3rd year undergraduate student at BITS
Pilani, K K Birla Goa Campus, India pursuing my major in Electronics and
Electrical engineering. I’ve been using Blender for about 2 years now
and I first started by completing the famous doughnuts tutorial on 2.79.

I got into graphics programming towards the end of my second year and
since then I have been learning about OpenGL and various rendering
techniques. Regarding my background in programming :

  - Primarily use C++ for many personal projects :
      - Wrote a simple path tracer following Peter Shirley’s books - Ray
        Tracing in one weekend.
      - Wrote simple rasterizer using OpenGL and studying from online
        resources such as learnopengl.com
      - Problem solving for University course on DSA
  - Used C\# for gameplay programming in Unity
  - Used Java for University course on OOP
  - Also worked with Python but mostly for smaller tasks and problem
    solving

I started contributing to Blender about a month ago and have enjoyed my
journey so far. Till now, I have submitted 5 small patches, out which 3
have been accepted and 2 are under review :

  - D10658 (DNA defaults for Unified paint settings)
  - D10743 (DNA defaults for sculpt and paint structs)
  - D10694 (Minor bug fix for fluid diffusion preset)
  - D10833 (Add missing tooltips for UV pivot options)
  - D10830 (Bug fix - Fix the issue of new islands being created in UV
    Sync selection)

I haven’t worked on any major tasks or patches yet, since I am still
working on understanding the code-base. I hope that GSoC will provide
the opportunity to make valuable contributions to Blender and I can
continue contributing after the Summer of Code program as well.
