This list contains ideas and suggestion that have been collected during
GSoC. As mentioned in my proposal, this list contains secondary tasks
that have been suggested by the community and users.

  - **Display UVs for selected objects in the 3D viewport when in object
    mode**

Presently UVs for an object are displayed only when the object is in
edit mode and is selected in the 3D viewport. Users have suggested that
this could be improved by allowing UVs to be displayed when in Object
mode as well. Further discussion on this is required, but it is apparent
that this feature will come with serious impacts on performance.

  - **Arrange and align selected uv islands**

Task description given in [T78408](https://developer.blender.org/T78408)
is quite self explanatory.

  - **Average islands scale according to the active island**

Set the active selection (island) as a reference for scaling other
islands in UV space. Current average islands scale operator works by
scaling islands with respect to their size and area in 3D space. No
design task created on d.b.o. The exact details of its implementation
need to be discussed first.

  - **New property for UV box select operator : Extend selection
    infinitely along X or Y axis**

Useful for extending UV selections in a specified direction so that UVs
outside the visible region will also be selected. Can be added as a new
property to the existing UV box select operator that isn't visible in
the UI/redo panel, but can be triggered by a key binding like E or
CTRL+E. A clear idea for design needs to be refined further.

  - **Expose UV editor operators as icons in the tool shelf**

Currently many useful UV operators can be accessed through the UV menu
in the UV editor. It would be useful and/or convinient if they are
exposed as icons in the tool shelf similar to the Sculpting workspace.

  - **Add a new pivot option : Active element**

Add active element pivot option to the UV editor. This is mostly
required to extend the functionality of grid snapping in UV editor
