## Google Summer of Code 2021

  - [Proposal](Proposal.md)
  - [Weekly
    Report](https://devtalk.blender.org/t/gsoc-2021-uv-editor-improvements-weekly-reports/19060)
  - [Secondary Tasks](SecondaryTasks.md)
  - [Daily Log](DailyLog.md)
  - [Final Report](FinalReport.md)
