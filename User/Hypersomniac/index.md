## Weekly Reports

[2023](Foundation/2023.md)  
[2022](Foundation/2022.md)  
[2021](Foundation/2021.md)  
[2020](Foundation/2020.md)  
[2019](Foundation/2019.md)  
[2018](Foundation/2018.md)  
[2017](Foundation/2017.md)  
[2016](Foundation/2016.md)  

## Random

[Debugging selection with
Renderdoc](Selection_Debug.md)
