## Week 0: 12th - 18th December

  - <strong>General development</strong>
      - Went to BI to talk about new viewport design.
      - Got used to workflow, git, arcanist ...

## Week 1: 19th - 25th December

  - <strong>General development</strong>
      - Did a proposal for
        [Pynode](https://wiki.blender.org/index.php/Dev:2.8/Source/Viewport/PyNodeGLSL)
        changes.
      - Did a proposal for the [Engine
        API](https://wiki.blender.org/index.php/Dev:2.8/Source/Viewport/EngineAPI).
      - Started development on the Clay engine.

## Week 2: 26th - 31th December

  - <strong>General development</strong>
      - Had a hard time to figure out how to draw something without
        going through all the common drawing routines.
      - Overcome this issue by creating own temporary routine.
      - Implemented for the engine API : Shader creation, BatchList
        render with State and Uniforms change, Fullscreen pass.
