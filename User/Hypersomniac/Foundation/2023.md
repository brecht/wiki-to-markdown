## Week 331 : 15th - 21st May

  - <strong>Info</strong>

I reviewed patches for EEVEE-Next and Overlay-Next, making sure they
work on Metal. Also started porting the (screen-space) raytracing
pipeline to EEVEE-Next.

  - <strong>Main</strong>
      - EEVEE: Add Transparent Render-Pass option
        [2d66a0ef84](https://projects.blender.org/blender/blender/commit/2d66a0ef848).
      - GPU: Fix false positive include of debug\_draw
        [8fb6b51410](https://projects.blender.org/blender/blender/commit/8fb6b514104).
      - Metal: Always add GPU\_TEXTURE\_USAGE\_ATTACHMENT flag
        [c435d2f856](https://projects.blender.org/blender/blender/commit/c435d2f8568).
      - Metal: Force Compute shaders workgroup max thread count
        [b40b59935d](https://projects.blender.org/blender/blender/commit/b40b59935d1).
      - Workbench: Fix compiler warnings
        [39644e7f71](https://projects.blender.org/blender/blender/commit/39644e7f71e).

## Week 330 : 8th - 14th May

  - <strong>Info</strong>

I made EEVEE-Next Volumes and SSS pull requests work on Metal. I
investigated a bug inside the SSS implementation on metal but did not
found the root cause yet. I also exposed a new render pass for alpha
blended transparent objects in EEVEE for the need of the current open
movie production [PR
\#107890](https://projects.blender.org/blender/blender/pulls/107890).

  - <strong>Main</strong>
      - Workbench-Next: Fix shader compilation
        [4436909c1d](https://projects.blender.org/blender/blender/commit/4436909c1d9).

## Week 329 : 1st - 7th May

  - <strong>Info</strong>

I focused on making EEVEE-Next working on Metal. Fixing compilation &
validation errors.

  - <strong>Main</strong>
      - DRW: Fix mixed type comparison warning
        [5a88afeaec](https://projects.blender.org/blender/blender/commit/5a88afeaec2).
      - EEVEE-Next: Fix validation layer errors on Metal
        [29053c5c02](https://projects.blender.org/blender/blender/commit/29053c5c025).
      - EEVEE-Next: Fix warning when using Metal backend
        [5aa51cf607](https://projects.blender.org/blender/blender/commit/5aa51cf607a).
      - EEVEE-Next: Fix wrong texture usage flag
        [eab0cd475a](https://projects.blender.org/blender/blender/commit/eab0cd475a6).
      - EEVEE-Next: World: Fix missing SSBO bind
        [2f1bf2ff1e](https://projects.blender.org/blender/blender/commit/2f1bf2ff1e0).
      - Metal: Add atomicExchange and mat3x4 support
        [c796cbebef](https://projects.blender.org/blender/blender/commit/c796cbebefc).
      - Metal: Fix EEVEE-Next shader compilation errors
        [78d2f162e8](https://projects.blender.org/blender/blender/commit/78d2f162e8b).
      - Metal: Fix Workbench-Next shader compilation errors
        [aa31d9be80](https://projects.blender.org/blender/blender/commit/aa31d9be80c).
      - Metal: Shader: Remove assertion of compilation success
        [9d6659bf07](https://projects.blender.org/blender/blender/commit/9d6659bf07b).
      - Metal: ShaderLog: Add initial implementation
        [2815f46883](https://projects.blender.org/blender/blender/commit/2815f468830).

<!-- end list -->

  - <strong>EEVEE-Next</strong>
      - EEVEE-Next: Fix compilation error with Metal
        [12fc600d7f](https://projects.blender.org/blender/blender/commit/12fc600d7f8).
      - EEVEE-Next: Irradiance Bake: Avoid asserts on Metal
        [487ab2073b](https://projects.blender.org/blender/blender/commit/487ab2073bd).

## Week 326 : 24th - 30th April

  - <strong>Info</strong>

## Week 326 : 17th - 23rd April

  - <strong>Info</strong>

## Week 326 : 10th - 16th April

  - <strong>Info</strong>

This week, I helped a bit with bug fixing after the release.

Fixed versionning of the new split irradiance cache and merged it with
the EEVEE-Next irradiance cache branch. This required some refactoring
but everything is now working.

  - <strong>Main</strong>
      - DNA: Move irradiance grid light cache data to Object level
        [a8feb20e1c](https://projects.blender.org/blender/blender/commit/a8feb20e1c7).
      - Fix \#106440: EEVEE: World lighting does not affect volumetrics
        [28a11c007e](https://projects.blender.org/blender/blender/commit/28a11c007eb).
      - GPU: Texture: Expose depth dimension extent
        [7e764ec692](https://projects.blender.org/blender/blender/commit/7e764ec692f).

<!-- end list -->

  - <strong>EEVEE-Next</strong>
      - EEVEE-Next: Adapt code to new irradiance cache
        [a5310b7720](https://projects.blender.org/blender/blender/commit/a5310b77205).
      - EEVEE-Next: Make Spherical harmonic follow object transform
        [d3e761cda3](https://projects.blender.org/blender/blender/commit/d3e761cda3d).

## Week 325 : 3rd - 9th April

  - <strong>Info</strong>

Worked on the design of spliting the light cache in object data
[\#106449](https://projects.blender.org/blender/blender/issues/106449)
and did a base implementation [PR
\#106808](https://projects.blender.org/blender/blender/pulls/106808).

## Week 324 : 27th - 2nd April

  - <strong>Info</strong>

Worked on the light propagation and spherical harmonic accumulation. It
is now working as intended.

However during development it became clear that the current way of
storing the light cache is not good for many reasons. For that I created
a design task to discuss the pros and cons
[\#106449](https://projects.blender.org/blender/blender/issues/106449).

  - <strong>Main</strong>
      - BLI: Rotation: Add CartesianBasis \`transform\_point\` and
        \`invert\`
        [897a735151](https://projects.blender.org/blender/blender/commit/897a735151c).
      - GPU: Fix compilation with option
        WITH\_GPU\_BUILDTIME\_SHADER\_BUILDER
        [7592ec35d3](https://projects.blender.org/blender/blender/commit/7592ec35d34).
      - GPU: FrameBuffer: Fix empty framebuffer update
        [e652d0002b](https://projects.blender.org/blender/blender/commit/e652d0002b5).

<!-- end list -->

  - <strong>EEVEE-Next</strong>
      - EEVEE-Next: Fix wrong irradiance surfel spawning area
        [85bbf53f84](https://projects.blender.org/blender/blender/commit/85bbf53f849).
      - EEVEE-Next: IrradianceBake: Add spherical harmonic encoding
        [9461d7fc9a](https://projects.blender.org/blender/blender/commit/9461d7fc9ad).
      - EEVEE-Next: IrradianceBake: Fix crash caused by motion blur
        [4e3eb2f7a5](https://projects.blender.org/blender/blender/commit/4e3eb2f7a5d).
      - EEVEE-Next: IrradianceBake: Fix integration power and
        inconsistencies
        [5dc6535591](https://projects.blender.org/blender/blender/commit/5dc65355911).
      - EEVEE-Next: IrradianceBake: Fix light bounce
        [bfeb1a1a8d](https://projects.blender.org/blender/blender/commit/bfeb1a1a8d9).
      - EEVEE-Next: IrradianceBake: Fix light-leak
        [5de76bbbff](https://projects.blender.org/blender/blender/commit/5de76bbbffd).
      - EEVEE-Next: IrradianceBake: Implement light propagation
        [db3028a624](https://projects.blender.org/blender/blender/commit/db3028a6249).
      - EEVEE-Next: IrradianceBake: Implement multi bounce support
        [73f96fe216](https://projects.blender.org/blender/blender/commit/73f96fe2161).

## Week 323 : 20th - 26th March

![../../../images/Eevee\_surfel\_list.png](../../../images/Eevee_surfel_list.png
"../../../images/Eevee_surfel_list.png")
![../../../images/Eevee\_surfel\_normal.png](../../../images/Eevee_surfel_normal.png
"../../../images/Eevee_surfel_normal.png")
![../../../images/Eevee\_surfel\_albedo.png](../../../images/Eevee_surfel_albedo.png
"../../../images/Eevee_surfel_albedo.png")

  - <strong>Info</strong>

Focused my week on working on the new irradiance baking. I'm making good
progress. I've already made the surfel linked list and a proof of
concept irradiance propagation. However, one of the limitation of this
technique seems to be the light leaking produced by the surfel density.
All the challenge will be to avoid connecting surfels that should not
connect because of occlusion while not over-connecting as it would
produce too much darkening.

  - <strong>Main</strong>
      - BLI: Fix compilation when importing \`BLI\_math\_rotation.hh\`
        [9c8cb823a0](https://projects.blender.org/blender/blender/commit/9c8cb823a05).
      - BLI: Fix drawdata leak on Scene IDs
        [b57b22388c](https://projects.blender.org/blender/blender/commit/b57b22388cd).

## Week 322 : 13th - 19th March

  - <strong>Info</strong>

Worked mainly on porting EEVEE-Next deferred pipeline and started
working on the new irradiance cache.

Plus the usual bug fixing for the release.

  - <strong>Main</strong>
      - BLI: Math: Fix function description
        [bb593e5416](https://projects.blender.org/blender/blender/commit/bb593e54166).
      - DRW: View: Allow the possibility to disable the visibility test
        [0c2299e6b1](https://projects.blender.org/blender/blender/commit/0c2299e6b16).
      - EEVEE-Next: Deferred Pipeline
        [335688dd42](https://projects.blender.org/blender/blender/commit/335688dd422).
      - EEVEE-Next: Spherical Harmonics Library
        [65b2aed14b](https://projects.blender.org/blender/blender/commit/65b2aed14b2).

<!-- end list -->

  - <strong>3.5 Release</strong>
      - Fix \#104124: Grease Pencil fills cover the lines below when
        drawing
        [9332f27702](https://projects.blender.org/blender/blender/commit/9332f277020).
      - Fix \#105323: Compositor: Crash when using color-ramp with
        b-spline
        [fee6238d33](https://projects.blender.org/blender/blender/commit/fee6238d337).
      - Fix \#105711: Regression: Clone Tool in Image Editor not showing
        Stencil
        [1b34c466bc](https://projects.blender.org/blender/blender/commit/1b34c466bc3).

## Week 321 : 6th - 12th March

  - <strong>Info</strong>

This week I was in Amsterdam at the Blender HQ form reassessing the
plans for EEVEE-Next. Because of multiple constraints, we decided to
focus on a stripped down version of the feature set for the 4.0 release,
and expand if time allows. The updated task list can be found here
[\#93220](https://projects.blender.org/blender/blender/issues/93220).
The GI solution was chosen and a breakdown of the implementation tasks
is available here
[\#105643](https://projects.blender.org/blender/blender/issues/105643).

This was also the opportunity to work with engineers from Apple and talk
about the future of the Metal API.

The new C++ Rotation API was merged to main branch which will be a huge
productivity boost for the whole Blender project.

  - <strong>Main</strong>
      - BLI: AxisAngle: Fix wrong constructor
        [3982aa922e](https://projects.blender.org/blender/blender/commit/3982aa922e8).
      - BLI: AxisAngle: Make axis normalization mandatory
        [610cb16d5d](https://projects.blender.org/blender/blender/commit/610cb16d5d7).
      - BLI: Math: Move rotation types template out of \`math::detail\`
        [4805a54525](https://projects.blender.org/blender/blender/commit/4805a54525e).
      - BLI: Math: Support more vector swizzling and add alignment
        static assert
        [a736f1d638](https://projects.blender.org/blender/blender/commit/a736f1d638b).
      - BLI: Rotation C++ API
        [28a581d6cb](https://projects.blender.org/blender/blender/commit/28a581d6cb3).
      - BLI: Math: Remove \`Normalized\` template parameter for some
        conversion
        [219be2e755](https://projects.blender.org/blender/blender/commit/219be2e755d).
      - Cleanup: BLI: Rotation: Remove unneeded type forward
        declarations
        [bd3c6801da](https://projects.blender.org/blender/blender/commit/bd3c6801da4).

## Week 320 : 27th - 5th March

  - <strong>Info</strong>

This week I did the design of the selection engine using the new
draw-manager. It should allow nearly realtime selection and in the
future realtime preview of box selection etc. The code is no longer
mixed with the draw manager, is easier to extend, and isn't invasive. In
order to do this, I used the Overlay engine as a base and just injected
a few modifications for outputting the select IDs during the drawing. So
one can potentially run the whole select pipeline while also drawing the
viewport.

With this done the Select-Next and Overlay-Next project can be tackled
by others.

I also did a bit of cleanup in the GPU module by documenting the API and
fixing some other stuff like wrong barriers.

  - <strong>Main</strong>
      - Cleanup: GPU: Improve eGPUBarrier documentation
        [d782569682](https://projects.blender.org/blender/blender/commit/d782569682a).
      - GPU: Add GPU\_BARRIER\_BUFFER\_UPDATE barrier type
        [fcedc97d11](https://projects.blender.org/blender/blender/commit/fcedc97d11f).
      - GPU: Compute: Document and cleanup header
        [b4d36b3efe](https://projects.blender.org/blender/blender/commit/b4d36b3efe0).
      - GPU: Fix wrong barrier in tests
        [f5e9a78216](https://projects.blender.org/blender/blender/commit/f5e9a782167).
      - GPU: Replace GPU\_finish by correct memory barrier
        [0a10571501](https://projects.blender.org/blender/blender/commit/0a10571501b).
      - GPUFrameBuffer: Document and cleanup header
        [4862d56a0e](https://projects.blender.org/blender/blender/commit/4862d56a0ee).

<!-- end list -->

  - <strong>Overlay-Next</strong>
      - Overlay-Next: Add depth Prepass basics
        [78ae27bab3](https://projects.blender.org/blender/blender/commit/78ae27bab3c).
      - Overlay-Next: Add high level separation of selection code
        [84ff44769f](https://projects.blender.org/blender/blender/commit/84ff44769f0).
      - Overlay-Next: Add shader module
        [16cc2446f5](https://projects.blender.org/blender/blender/commit/16cc2446f5b).
      - Overlay-Next: Port Metaball code to use latest design and
        support selectid
        [68a0f9e01c](https://projects.blender.org/blender/blender/commit/68a0f9e01cd).
      - Select-Next: Add result buffer injection
        [3ac54f67bf](https://projects.blender.org/blender/blender/commit/3ac54f67bf4).
      - Select-Next: Fix IDs for meshes and metaballs handles
        [34afb6c05f](https://projects.blender.org/blender/blender/commit/34afb6c05f2).
      - Select-Next: Introduce shader level patching to output selection
        ids
        [ea66898bbe](https://projects.blender.org/blender/blender/commit/ea66898bbe7).
      - Select-Next: Reorganize and rename stuffs
        [a9397b5458](https://projects.blender.org/blender/blender/commit/a9397b54586).

## Week 319 : 20th - 26th February

  - <strong>Info</strong>

Started the week by making touchups to [PR
\#104941](https://projects.blender.org/blender/blender/pulls/104941).
Gave some help to [PR
\#105175](https://projects.blender.org/blender/blender/pulls/105175) and
[PR \#104599](https://projects.blender.org/blender/blender/pulls/104599)
Spent some time cleaning-up, documenting the GPU\_texture API.

  - <strong>Main</strong>
      - Cleanup: GPUTexture:
        [584e514561](https://projects.blender.org/blender/blender/commit/584e5145619),
        [9fb1f32f06](https://projects.blender.org/blender/blender/commit/9fb1f32f06f),
        [73da5ee90d](https://projects.blender.org/blender/blender/commit/73da5ee90d0),
        [75e3371aef](https://projects.blender.org/blender/blender/commit/75e3371aef6).
      - GPUTexture: Add header documentation and reorganize sections
        [7974557f0b](https://projects.blender.org/blender/blender/commit/7974557f0bd).
      - GPUTexture: Add texture usage flag to all texture creation
        [3d6578b33e](https://projects.blender.org/blender/blender/commit/3d6578b33e4).
      - GPUTexture: Make sure all available texture format are supported
        [52ded6ab56](https://projects.blender.org/blender/blender/commit/52ded6ab567).
      - GPUTexture: Remove data\_format from 3D texture creation
        function
        [e01b140fb2](https://projects.blender.org/blender/blender/commit/e01b140fb28).
      - GPUTexture: Remove obsolete GPU\_texture\_bind\_ex argument
        set\_number
      - Workbench: Remove unecessary GPU\_TEXTURE\_USAGE\_ATTACHMENT
        flag
        [afb6336857](https://projects.blender.org/blender/blender/commit/afb6336857b).

## Week 318 : 13th - 19th February

  - <strong>Info</strong>

Spent the whole week rewriting the entire C rotation API into the new
C++ rotation API with speedups and documentation everywhere. The [PR
\#104941](https://projects.blender.org/blender/blender/pulls/104941) is
almost finished.

  - <strong>Main</strong>
      - Fix \#104748: GPencil: Fill tool is not working at all
        [acf7f46b77](https://projects.blender.org/blender/blender/commit/acf7f46b779).
      - GPU: Fix wrong 2D shader used from 3D drawing
        [f4db58844d](https://projects.blender.org/blender/blender/commit/f4db58844d5).

## Week 317 : 6th - 12th February

  - <strong>Info</strong>

EEVEE-Next's new Virtual Shadow Map is now in \`master\` (now called
\`main\`). A [devtalk
post](https://devtalk.blender.org/t/eevee-next/23570/86) explain a bit
what to expect from it.

The BLI\_math\_float4x4.hh removal was also finally merge with all
compilation issues fixes.

With the Gitea transition, I took some time to cleanup in the workboard
and add some to-do tasks. I added some documentation about ou [GLSL
codestyle](../../../Style_Guide/GLSL.md) and updated documentation about
our shader system.

I did some cleanup on the GPU module API to improve contribution quality
and onboarding.

Spent a bit of time on Overlay-Next porting Empty drawing and finding a
good architecture to improve the code quality.

  - <strong>Main</strong>
      - BLI: Math: Fix vector operator \* with \`MutableMatView\`
        [0ab3ac7a41](https://projects.blender.org/blender/blender/commit/0ab3ac7a41e).
      - BLI: Use BLI\_math\_matrix\_type.hh instead of
        BLI\_math\_float4x4.hh
        [b0b9e746fa](https://projects.blender.org/blender/blender/commit/b0b9e746fa5).
      - Cleanup: EEVEE-Next: Add LIGHT\_FOREACH macros to clang-format
        exceptions
        [c7456272b1](https://projects.blender.org/blender/blender/commit/c7456272b19).
      - Cleanup: GPU: Move \`eGPUKeyframeShapes\` to shader shared
        [3ac2708eb5](https://projects.blender.org/blender/blender/commit/3ac2708eb56).
      - Cleanup: GPU: Remove commented lines without any comments or
        purpose
        [77aa9e8809](https://projects.blender.org/blender/blender/commit/77aa9e88090).
      - Cleanup: GPU: Rename some functions for consistency
        [9574f23213](https://projects.blender.org/blender/blender/commit/9574f232132).
      - Cleanup: GPUShader: Remove \`GPU\_shader\_uniform\_int/float\`
        [4ae48ac8b5](https://projects.blender.org/blender/blender/commit/4ae48ac8b54).
      - Cleanup: GPUShader: Rename \`GPU\_shader\_uniform\_vector\`
        [55bddeda74](https://projects.blender.org/blender/blender/commit/55bddeda74f).
      - Cleanup: GPUShader: Reorganize GPU\_shader.h to separate
        depecated API
        [c303b0bbde](https://projects.blender.org/blender/blender/commit/c303b0bbdef).
      - Cleanup: GPUShader: Split Builtins to their own header
        [0e4e6257a3](https://projects.blender.org/blender/blender/commit/0e4e6257a39).
      - EEVEE-Next: Shadow: Fix issue with last merge
        [9103978952](https://projects.blender.org/blender/blender/commit/91039789520).
      - EEVEE-Next: Shadows: Add global switch
        [94d280fc3f](https://projects.blender.org/blender/blender/commit/94d280fc3f4).
      - EEVEE-Next: Virtual Shadow Map initial implementation
        [a0f5240089](https://projects.blender.org/blender/blender/commit/a0f52400890).
      - EEVEE: Cleanup: light power / radiance code and document it
        [81f8d74f6d](https://projects.blender.org/blender/blender/commit/81f8d74f6d0).
      - Fix T104390: Regression: Object selection in viewport is not
        working
        [349350b304](https://projects.blender.org/blender/blender/commit/349350b3049).
      - GPU: Cleanup GPU\_batch.h documentation and some of the API for
        consistency
        [7e0e07657c](https://projects.blender.org/blender/blender/commit/7e0e07657c7).
      - GPU: Move gpu\_shader\_builtin.c to C++
        [2b77295f6c](https://projects.blender.org/blender/blender/commit/2b77295f6c9).
      - GPU: Remove GPU\_SHADER\_3D\_POINT\_FIXED\_SIZE\_VARYING\_COLOR
        [6a240982e8](https://projects.blender.org/blender/blender/commit/6a240982e8f).
      - GPU: Removes GPU\_shader\_get\_builtin\_ssbo
        [7d56e56d5e](https://projects.blender.org/blender/blender/commit/7d56e56d5ef).

## Week 316 : 30th - 5th February

  - <strong>Info</strong>

I focused my week on porting all source to the new matrix API. We had to
revert this change because of a sneaky build error on the windows
buildbot which could not be replicated locally. We will follow
investigation next week.

I also fixed the last remaining bug on EEVEE-Next virtual shadow maps
which should be merged to master soon.

  - <strong>Master</strong>
      - BLI: Math: Add \`xy()\` and \`xyz()\` swizzle functions
        [a837604d44](https://projects.blender.org/blender/blender/commit/a837604d44a).
      - BLI: Use BLI\_math\_matrix\_type.hh instead of
        BLI\_math\_float4x4.hh (reverted later)
        [52de84b0db](https://projects.blender.org/blender/blender/commit/52de84b0db1).
      - GPencil: Use BLI\_math\_matrix\_type.hh instead of
        BLI\_math\_float4x4.hh
        [c1e5359eba](https://projects.blender.org/blender/blender/commit/c1e5359eba8).

## Week 315 : 23rd - 29th January

  - <strong>Info</strong>

This week was focused on bug-fixing the new shadow, improve the depth
bias, improve the scalability, and implement a solution for the
orthographic views.

The first stage of the new shadows implementation is almost finished.
There in one major bug that still needs to be fixed in orthographic
views before it can be landed. The implementation of virtual shadow
mapping will then be considered done. A lot of performance improvement
can be implemented later on and are tracked by this task.

Addition of some more parameters can be discussed.

However, I won't be able to fix the remaining issue this coming week for
personal reason. For this reason, I postpone the merging of the new
shadow until I can solve the remaining issue.

  - <strong>Master</strong>
      - DRW: Debug Print: Fix print of vec3
        [607b814096](https://projects.blender.org/blender/blender/commit/607b8140965).
      - DRW: Fix Texture.ensure() function always recreating the texture
        [e0c8fa4ab9](https://projects.blender.org/blender/blender/commit/e0c8fa4ab95).

## Week 314 : 16th - 22nd January

  - <strong>Info</strong>

Started to port back commits in preparation of the EEVEE-Next shadow
landing.

Spend a lot of time polishing features for the shadows. Uncovered a bad
issue with transparent (alpha blended) object not tagging all needed
shadow pages, which might be addressed later. I estimate that we could
merge next week once I implement scalability options.

  - <strong>Master</strong>
      - BLI: Math: Fix perspective matrix function
        [17768b3df1](https://projects.blender.org/blender/blender/commit/17768b3df19)
        [6f206f713e](https://projects.blender.org/blender/blender/commit/6f206f713e5).
      - DRW: Add double buffering of objects matrices, bounds, and infos
        [9c54f2655d](https://projects.blender.org/blender/blender/commit/9c54f2655d7).
      - DRW: Fix DRW\_VIEW\_FROM\_RESOURCE\_ID
        [e6be3f96d8](https://projects.blender.org/blender/blender/commit/e6be3f96d80).
      - DRW: Fix display print
        [493e3230b4](https://projects.blender.org/blender/blender/commit/493e3230b42).
      - DRW: GPU Wrappers: Add swap to storage buffers, empty
        framebuffer and fixes
        [21b3689fb9](https://projects.blender.org/blender/blender/commit/21b3689fb9c).
      - DRW: Make intersect lib not dependent on common\_view\_lib.glsl
        [534214e65c](https://projects.blender.org/blender/blender/commit/534214e65c9).
      - DRW: Protect \`common\_math\_lib.glsl\` from duplicated
        declaration
        [9e5ada315f](https://projects.blender.org/blender/blender/commit/9e5ada315f6).
      - DRW: Tests: Add test for PassSimple::Sub
        [df74a9b624](https://projects.blender.org/blender/blender/commit/df74a9b6246).
      - DRW: View: Allow disabling view test by setting bound sphere
        radius to -1
        [c412d2dcfe](https://projects.blender.org/blender/blender/commit/c412d2dcfe7).
      - DRW: View: Allow for GPU side specification of view bounds
        [efe51f0220](https://projects.blender.org/blender/blender/commit/efe51f02200).

## Week 313 : 9th - 15th January

  - <strong>Info</strong>

Focused on working on EEVEE-next shadows. I now have shadow map
rendering working as intended. There is still some improvement that
needs to be done in order to make it shippable and this should be done
by next week.

Fixed 2 high priority bugs. Some of the commits in master are just
things that are needed by EEVEE-next.

  - <strong>Master</strong>
      - BLI: Math: Add sign() function
        [8b5d5cbf06](https://projects.blender.org/blender/blender/commit/8b5d5cbf06d).
      - DRW: Add debug print for matrix type
        [6da7436cce](https://projects.blender.org/blender/blender/commit/6da7436cce3).
      - EEVEE: Avoid glitchy displacement on curve geometry
        [b2746876f2](https://projects.blender.org/blender/blender/commit/b2746876f28).
      - Fix T103256 Viewport: Regression: Clipping Region is not working
        [87bb14ab5f](https://projects.blender.org/blender/blender/commit/87bb14ab5fc).
      - GPU: Fix math lib compilation and tests on AMD drivers
        [71ca339fe0](https://projects.blender.org/blender/blender/commit/71ca339fe04).
      - GPU: Fix uninitialized variable which created asan warning /
        errors
        [945d108ab8](https://projects.blender.org/blender/blender/commit/945d108ab8c).
      - GPU: Math: Add floatBitsToOrderedInt and orderedIntBitsToFloat
        [28db19433e](https://projects.blender.org/blender/blender/commit/28db19433ea).

## Week 312 : 2nd - 8th January

  - <strong>Info</strong>

Highlight of this week is the merge of the new Matrix API which is. I
took care there was no performance regression in the most used matrix
multiplications functions. This was followed by some cleanups of the
vector API. I also merged a GLSL version of the math BLI API so we can
use the same semantic in both GLSL and C++.

I did some work on EEVEE-Next shadows and started looking into an
automatic bias system.

  - <strong>Master</strong>
      - BLI: Add [\`nodiscard\`](\`nodiscard\`) to math
        vector lib
        [da2dccca61](https://projects.blender.org/blender/blender/commit/da2dccca610).
      - BLI: Math: Vector: Improve API documentation
        [ba9f0d5a15](https://projects.blender.org/blender/blender/commit/ba9f0d5a159).
      - BLI: Refactor matrix types & functions to use templates
        [8a16523bf1](https://projects.blender.org/blender/blender/commit/8a16523bf1f)
        [67e1b52568](https://projects.blender.org/blender/blender/commit/67e1b525684).
      - Cleanup: BLI: Remove BLI\_ENABLE\_IF((is\_math\_float\_type))
        from vector API
        [11ecde6a5a](https://projects.blender.org/blender/blender/commit/11ecde6a5af).
      - Cleanup: BLI: Rename vec\_base to VecBase
        [73594f4c9c](https://projects.blender.org/blender/blender/commit/73594f4c9c5).
      - Cleanup: BLI: Rename vector compare() to is\_equal() for
        consistency
        [56216e2a62](https://projects.blender.org/blender/blender/commit/56216e2a62d).
      - Cleanup: Rename BLI\_math\_vec\_types\* files to
        BLI\_math\_vector\_types
        [8f44c37f5c](https://projects.blender.org/blender/blender/commit/8f44c37f5cc).
      - GPU: Add Math libraries to GPU shaders code
        [125b283589](https://projects.blender.org/blender/blender/commit/125b2835897).
      - GPU: Fix test on metal
        [1a8675b48d](https://projects.blender.org/blender/blender/commit/1a8675b48db).
      - GPU: Texture: Fix missing cases in \`validate\_data\_format()\`
        [ecd4533615](https://projects.blender.org/blender/blender/commit/ecd45336156).
      - Metal: Add support for all matrix types
        [a6355a4542](https://projects.blender.org/blender/blender/commit/a6355a4542d).
