### [Project Home](index.md)

<h2>

Name

</h2>

Hans Goudey

<h2>

Contact

</h2>

Forums: HooglyBoogly on Devtalk and developer.blender.org

<h2>

Synopsis

</h2>

The bevel modifier is extremely powerful, but there is constantly a list
of requested improvements that could expand its use case and speed up
the modeling process. One of these is<strong> user-drawn
profiles</strong>, which is a commonly requested feature that is
somewhat separate from the main functionality of the bevel operation,
which makes it a good candidate for a GSoC project. There has been
successful GSoC projects with the Bevel modifier in the past few years,
and I hope with this project I can continue that success.

<h2>

Benefits

</h2>

Right now, the best way to get a custom profile along an edge is
probably to use a curve and add a bevel object for its profile. While
this can work, it’s awkward, and it’s not built in to an object. Whether
it’s for an architectural molding, a sharper edge, or any other bevel
that can’t be described by a profile number from 0 to 1, the ability to
use a custom profile would add a lot of flexibility.

The custom profiles feature would be a part of the bevel modifier as an
option, and as part of the bevel tool as well.

<h2>

Deliverables

</h2>

The user drawn profiles, or custom profiles task divides into a few
discrete parts

  - UI widget for capturing custom drawn profiles
  - Creating beveled edges with the profile
  - Creating corners for where the custom profiles intersect with each
    other
  - Testing the feature to verify that it works as expected
  - User and developer documentation. And in the (unlikely) case that
    there is more time at the end of the project, there are always more
    bevel improvements listed
    [here](https://developer.blender.org/T48583).

<h2>

Project Details

</h2>

**UI widget for capturing profile description**

Code can hopefully be adapted from the existing curve editor. Here is a
mockup for what the input could look like. The two end vertices would be
anchored, and new vertices could be added manually or sampled from a
curve drawn more like the existing curve editor widgets. If it was
useful, the widget created for this operation could become a template so
it could be used elsewhere in the program.

![|](../../../images/Bevel_Custom_Profile_Curve_Draw_Mockup.png "|")

**Creating beveled edges with the drawn profile**

This task should be the simplest part of the actual changes to the bevel
operation (which lives in bmesh\_bevel.c). The profile should not
necessarily need to be symmetric, so the orientation (which way is up
for the profile) will need to decided somewhat arbitrarily, and there
would be an option to flip this direction. This works well for a loop of
edges, but at intersections of more edges, it becomes more complicated.

**Beveling intersections**

With only two edges meeting, this code should not be too complicated, as
the profile can continue. But with three edges meeting at a point, the
problem becomes much more complicated. A mesh is built at each corner,
and the profiles coming into the point will have to be merged in a
meaningful way. The lack of a custom profile’s symmetry make it possible
that the heights in the profile will not match up. There isn’t
necessarily one correct solution to this issue, so user input and
brainstorming will be necessary to solve this issue.

<h2>

Project Schedule

</h2>

I finish with this semester at college around the 25<sup>th</sup> of
May. I don’t have other plans that interfere with this GSoC project, so
I can get right started situating myself in the bevel code.

I have heard that (and my time looking through the code so far supports
this) that the bevel operation has a steep learning curve, so I have
made sure to give myself plenty of time to go through notes and the code
to understand how the bevel works before I start to implement the custom
profiles feature. I’ve also given myself what seems like a generous
amount of time now to make sure that there is plenty of time in the
schedule, and to give myself time to focus on quality when the time
comes.

<table>

<tr>

<td>

Until May 27

</td>

<td>

Spend time with the bevel code with the goal of better understanding the
basic structure of the operation and the details I will need to know.

</td>

</tr>

<tr>

<td>

Week 1

</td>

<td>

Verify understanding of goals, UI requirements and bevel code. Devise a
goal for the more complicated cases of 3+ edge intersection with
community input.

</td>

</tr>

<tr>

<td>

Week 2

</td>

<td>

Verify a plan for the implementation of the features, start to implement
UI for receiving custom profile data.

</td>

</tr>

<tr>

<td>

Week 3

</td>

<td>

Create UI widget for receiving profile data.

</td>

</tr>

<tr>

<td>

Week 4

</td>

<td>

Complete connection between widget and bevel code. **Deliverable 1**

</td>

</tr>

<tr>

<td>

Week 5

</td>

<td>

Start implementation of edge bevel creation.

</td>

</tr>

<tr>

<td>

Week 6

</td>

<td>

Finish implementation of edge custom profiles and verify results.
**Deliverable 2**

</td>

</tr>

<tr>

<td>

Week 7

</td>

<td>

Start implementation of vertex beveling with the 2 edge intersection
case

</td>

</tr>

<tr>

<td>

Week 8

</td>

<td>

Start implement vertex beveling with 3+ edges, verify assumptions about
implementation method

</td>

</tr>

<tr>

<td>

Week 9

</td>

<td>

Continue vertex beveling implementation

</td>

</tr>

<tr>

<td>

Week 10

</td>

<td>

Complete vertex beveling feature, make sure that it works properly.
**Deliverable 3**

</td>

</tr>

<tr>

<td>

Week 11

</td>

<td>

Testing, extra time in case of delays. **Deliverable 4**

</td>

</tr>

<tr>

<td>

Week 12

</td>

<td>

Documentation, testing **Deliverable 5**

</td>

</tr>

</table>

<h2>

Bio

</h2>

Hi\! I’m Hans Goudey, a student at Middlebury College in Vermont, USA.
I’m in my fourth year, majoring in physics and computer science. I’ve
been a Blender use for years now, mostly as a hobbyist, but I’ve done
some professional work with engineering visualizations as well. I’ve
been interested in 3D graphics as long as I can remember, and for a
while I did some programming using the Unity engine, but I’ve always
loved building things in Blender, especially modeling and shading. I’ve
recently started working on a short film with my college’s animation
studio as well, and it’s been really interesting to see Blender used in
a production environment.

As I’ve learned more about programming, I’ve discovered a passion for
free software as well. I’ve switched to Linux and other free software,
but it’s so cool that Blender is software that so many people use
regardless of that. The whole community driven development process is so
empowering. Over the past few months I’ve started familiarizing myself
with Blender’s code by making some smaller UI changes. I’ve been
involved with [D4400](https://developer.blender.org/D4400),
[T61588](https://developer.blender.org/T61588),
[T57208](https://developer.blender.org/T57208),
[T62332](https://developer.blender.org/T62332), and
[D4706](https://developer.blender.org/D4706). Although not all of these
have made it in yet, I’ve learned a lot in the process and discovered
that I really enjoy contributing\! So the idea of being able to continue
this work full time for the summer is so exciting\!

I have plenty of experience with C from my classes and some personal
projects, and I feel comfortable working in the language. I have plenty
of python experience from some machine learning projects in the past,
and I know how to apply this experience to Blender with the experience
of my recent contributions.
