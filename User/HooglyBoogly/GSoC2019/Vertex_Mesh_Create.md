### [Project Home](index.md)

# Plan for Building Vertex Meshes with the ADJ Pattern

(Document in progress)
![../../../images/Vertex\_Mesh\_Custom\_Profile\_Decaying\_Influence.png](../../../images/Vertex_Mesh_Custom_Profile_Decaying_Influence.png
"../../../images/Vertex_Mesh_Custom_Profile_Decaying_Influence.png")

The vertex meshes are created by repeated subdivision of an initial
simple shape that fills the opening. During the subdivision the boundary
vertices are snapped to the profile. To maintain compatibility with the
other bevel features, this same general strategy will be used for the
custom profile vertex meshes.

## Pipe Case and Cube Corner Case

Because they're more symmetrical and simpler, both of these cases snap
the new points to the superellipsoid function currently. This means I'll
either have to disable this snapping and let the subdivision run its
course or enable new snapping.

## Functions That Need Custom Analogs

1.  **snap\_to\_superellipsoid:**
2.  **interp\_vmesh:**

# Other plans

It could be that I need a completely different solution to make the
vmesh corners look good with custom profiles. Another solution that's
been suggested by a few people is projecting the custom profile along
the edge's direction through the intersection and intersecting them with
the other edges. This might look especially good and correspond to the
real world when the custom profile is symmetrical.

## Weighted Average of Profile Influences

One other idea I had is that each profile could be extended through the
intersection to the other points. So after each subdivision step each
vertex would be snapped to all of the profiles, and the resulting
location would be a weighted average of all of those snapped positions.
If the snapping is too slow, the profile could be sampled at the number
of vertices in each ring. This sound like a fairly slow solution, but
not appreciably different from the other plans.
