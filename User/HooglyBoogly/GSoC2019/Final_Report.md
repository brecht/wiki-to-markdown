### [Project Home](index.md)

# GSoC 2019: Custom Bevel Profiles Final Report

By the end of the summer the bevel profiles branch is in a functional
state with just a few small bugs to fix and one smaller feature left to
add. The code is ready for review and shared with Blender developers and
the community on developer.blender.org.

**[Diff from master](https://developer.blender.org/D5516)**

**[Commit History vs.
master](https://developer.blender.org/diffusion/B/compare/?head=soc-2019-bevel-profiles&against=master)**

### Profile Widget

An important part of the usability of the feature is the ability to
define a profile in the UI rather than in the viewport. This widget
provides that ability.

  - The profile widget is derived from the existing "CurveMapping"
    widget, but the curve is built to be a path instead of a mapping
    between X and Y.
  - Profile sampling methods: More curved areas get more samples, and
    points can be sharp as well. There's also an "Even Length Sampling"
    option.
  - The interaction methods are changed for profiles and hotkeys are
    added.
  - The widget should be extendible for other situations where splines
    need to be edited directly in the UI.
  - RNA API for the widget included.

### Bevel Custom Profile

  - Added custom profile bevel cases, removed existing assumptions and
    special cases.
  - New "pipe case" for parallel edges which reuses profile positions.
  - Profile orientation regularization: Method to ensure that the side
    of the start of the profile is consistent along groups of edges.
  - Presets: Moldings and steps, etc...

### Bevel cutoff intersection method

  - Method chops off each profile at the boundary of each vertex mesh.
  - Leaves room for Boolean method in the future.

![../../../images/GSoC2019\_Bevel\_Cutoff\_Method.png](../../../images/GSoC2019_Bevel_Cutoff_Method.png
"../../../images/GSoC2019_Bevel_Cutoff_Method.png")

### Bevel tool and modifier UI

  - In addition to adding the new features, updates to streamline and
    unify both interfaces.

![../../../images/GSoc2019\_Bevel\_Tool\_Modifier\_UI.png](../../../images/GSoc2019_Bevel_Tool_Modifier_UI.png
"../../../images/GSoc2019_Bevel_Tool_Modifier_UI.png")

### Bevel code cleanup and documentation

  - Added Lots of comments on the existing bevel code where I worked,
    changed some variable names, and some other refactoring.
  - The code should be more readable, which has already helped me, so
    hopefully it will help someone else in the future.

## The Process

Feedback and communication with the community was an essential part of
this project. That took place in two threads on the Blender DevTalk
forum.

**[Weekly Reports
Thread](https://devtalk.blender.org/t/gsoc-2019-bevel-profiles-weekly-reports/7651)**
Here I posted the features I completed at the end of each week and my
plan for the next week.

**[Discussion and Feedback
Thread](https://devtalk.blender.org/t/gsoc-2019-custom-bevel-profiles/7214)**
Here we prioritized and planned new additions with feedback from the
awesome Blender community. This is an essential part of the process and
my project would have been much less successful without it. Notable
discussions included prioritizing my time working on intersection
methods and discussion about the bevel UI.

**[Daily Log](Log.md)** In addition
to the weekly reports I also kept a daily log over the course of the
summer, mostly to organize my own thoughts, but if anyone read some of
it during the summer that's an added bonus too.
