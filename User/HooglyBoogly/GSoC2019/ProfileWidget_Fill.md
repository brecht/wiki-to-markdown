## The Original Method

The fill has to be more complicated because the profile can loop back on
itself in the X direction. The fill can't just be a simple inequality
anymore. Here's the new strategy:

For every horizontal pixel, do the following:

1.  Find all of the edges between \`ProfilePoints\` in the profile's
    \`table\` that will have the same Y value as this pixel.
2.  Sort those edges by their intersection point with the current
    pixel's X value (the Y value at this X)
3.  Go through the list, switching between drawing a light and dark edge
    to the next intersection point after each edge. Start with light.

This could be sped up by making a list in the profile table sorted by
their X values. That way the first step would only have to be done once
at the beginning of the whole process.

## The Triangulation Method

I started to implement the profile color fill algorithm I described
above, but I've been dissapointed by how inefficient it seems, and I'm
pretty sure I thought of a better solution. I'm hoping to be able to use
an existing triangulation algorithm in \`polyfill\_2d.c\` to fill the
"polygon" of the custom profile with a darker color. My original method
would have been O(x\_width \* nlogn) (where n is the number of points in
the table), and it looks like a triangulation algorithm would at least
drop that x\_width term, which could be very helpful. The only new issue
this brings up is the translation from the \`ProfilePoints\` to the
triangulation code, and then from the triangulation code to primitive
triangles drawn by the GPU in the interface. If there isn't too much
overhead in these transitions, this technique will probably work a lot
better.
