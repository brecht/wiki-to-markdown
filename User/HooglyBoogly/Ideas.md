## Future Blender Development Ideas

### Large Projects

  - Bevel boolean vertex mesh method
  - Get the cutoff method and miter options working at the same time
  - Profile widget object import/sharing mode
  - Better edge / vertex attribute editing from \`ctrl\`+\`e/v/f\` menus
  - Node tooltips
  - See if inset offset even needs fixing. I may have encountered a bug
    there.
  - If nodes are very wide, draw inputs and outputs on the same line
  - Update bevel todo list
  - Expand some text fields when UI becomes wider
  - Icon-style overlays addon
  - Fix hardcoded string modal maps in status bar

### Medium Ideas

  - Add edge pan operator to node editor
  - Add the ability to call the modifier panels template with just one
    modifier
  - Use \`custom\_data\` instead of index for instanced panels. (Use RNA
    collection for the list)
  - \`CurveMapping\` template UI updates
  - Press shift during orbiting to pan
  - Ctrl middle mouse to zoom in file browser
  - Topology preserving bevels
    (https://blender.community/c/rightclickselect/n3fbbc/)
  - Too much extra space in some keymap settings
  - Align nodes and their inputs to the grid better
  - Automatically expand / unexpand enums based on the available width
  - RNA 64 bit int support
  - Animation for subpanels
  - Fix tooltips don't show during animation playback

### Small Ideas

  - Quick way to reset the rotation of the 3D cursor
  - Bevel weight and others are under the "Transform" panel of the
    N-panel
  - Switch color and label in annotation settings popover preview
  - Cursor position history
  - Slider \`uiItemR\` places text misaligned with other properties
  - Don't draw icons when their buttons' width gets smaller than the
    icon itself
  - Add Bevel modifier warning for unapplied object scale
  - Modifiers set auto smooth with operator
  - "+00" suffix for seconds display in timeline overlaps other text
  - Boxes around context buttons
  - Expose panel custom data to python
  - Expose panel custom data under mouse to python
  - File browser display size accessible in header does not work
  - Simple deform modifier angle / factor properties share the same dna
    float
  - Don't show "Keyframe" in driver editor subpanel for editing active
    bezier point
  - Consistent use of "layout" and "draw" in interface code and area.c
  - Move more runtime fields in structs to runtime structs
  - Property split layout in image editor panels

### Finished

  - Add profile widget custom curve to the "beveling" of curves
  - Rename "View Camera Center" to "Frame Camera Bounds"
  - "Duplicate" instead of "Copy" modifier button.
  - Assign, Select, and Deselect buttons in shader editor
  - Construct vs Add in add menu tooltips
  - Bevel: Optimize number of times \`calculate\_profile\` is called for
    each boundvert
  - Bevel active tool UI properties
  - Pass over modifier UI
  - Better warning message than "You are using a rather high poly as
    source or destination, computation might be slow"
  - Remove unused code in interface\_panel.c (overlapping code,
    UI\_PNL\_SOLID)
  - Skip mouse move and mousemove consecutive events in panel handling
    code
