### Name

Legrand Alexia

### Contact

  - *Mail :* ​alexleg33@outlook.fr  
  - *Github :* [TempoDev](https://github.com/TempoDev)  
  - *Instagram :* [alexleg33](https://www.instagram.com/alexleg33/)  
  - *Blender (talkdev, chat and developer) :* Tempo  

### Synopsis

Ideas from Ideas suggestion: menu editor (quick favorite menu)  
“Create a generic menu editor. See ​​Menu Editor for Quick Favorites​"  
The project goal is to create a menu in the preferences settings in
order to manage the quick favorites menu and to be able to create more
menu and customize them even more..

### Benefits

“Users can edit their own custom menus which currently only have limited
add/remove capabilities.”  
The quick favorites menu can be accessed with the ‘Q’ key and contain
the tools that the user has chosen by using right click -\> Add to Quick
favorites.  
But there is nothing to easily manage the menu. This project will solve
this issue.  
Moreover, being able to create your own menus and, as I’d like achieve
it in the end, being able to display them in an area can open a whole
new field of possibilities for users.  
They would be able to customize their blender to its core by adding
their own sorted functionalities and properties menu (most used as
favorite, for example), but also, why not their own mesh (to addmesh
functionality), their own functions (python implementation) or some
macro (like, a button that add a cube mesh and subdivide it)...  
This way, they wouldn’t have to search for something and switch menus
and actions will be sorted the way the user prefer.  
As they’ll be able to perform multiple actions with one click, they
could be more efficient (in case of macro or python script).  
What could be done next is very large : make the menu even more
customizable with the “menu features” (add color selector as parameter
option, add preview icon, add the vertical menu from properties area
available for customs menu, ...), add the macro system by creating a
drag and drop macro editor, extend custom menu to more formats.  
What could be interesting would be to make the operator selection more
understandable for lambdausers (because, not every users know all the
python commands to create their menus).  
\=== Deliverables === As shown in T73178 (​Menu Editor for Quick
Favorites​), I’ll add the quick menu editor to the preferences,
following what have already been proposed.  
I would also like follow​​ Alexander Dumproff​ idea by adding the
possibility to create differents menus, which would work just like
folders. I’ll add the possibility to keymap each menu independently as
well as the possibility to import/export the menu in some format.  
If I still have time after all this, I will add the quicks menu to the
displayable menus (the ones you can change as you want) so you can have
fully customizable menu and interface, and so add more customization in
preference mode, like properties displaying and modification, as well as
sections.  
\=== Project Details ===

#### Preferences Window

As suggested in T73178, I will follow this template:  
![../../../images/Mockup\_Custom\_Menu\_old\_preference.png](../../../images/Mockup_Custom_Menu_old_preference.png
"../../../images/Mockup_Custom_Menu_old_preference.png") But I’ll adapt
it to the different changes :  
\* Quick favorite will be renamed : Custom menus.  
\* Add import / Export option  
\* I’ll change the top bar with the buttons : \[Mode\] \[Menu\] \[+\]
\[-\]  
\* The part to the right will be reordered to be more intuitive  
\* It’ll be possible to use Pie/radial menus  
![../../../images/Mockup\_Custom\_Menu\_new\_preference.png](../../../images/Mockup_Custom_Menu_new_preference.png
"../../../images/Mockup_Custom_Menu_new_preference.png") The top bar is
the menu selection, just after is general menu option and last is the
custom menu editor.  
List of differents options :  
\* \[3D View\] \[Edit Mode (mesh)\] : menu directory  
\* \[my edit menu\] \[+\] \[-\] :

  -   - menu name (can be changed)
      - choice of which menu you’ll work on
      - buttons to add and remove menus  

  - \[Import\] / \[Export\] : Import or export a menu to use it in
    another blender or send it to someone.  

  - Display type \[list\] ou \[Pie/radial\] : Menu format (Box and
    properties not allowed in Pie mode,see below)  

  - shortcut : shortcut to access the menu  

  - operator options : options of the operator, so users can customize
    their operations  

  - Type : Operator, Box, property or Macro (more can be added during
    the development) :
    
      - Operator : a simple operator in Python that will be executed
      - Box : Box, or folder, used to sort the operators
      - Property : Used to get a customized display of properties in
        area mode (see areamode, below)
      - Macro : List of operators, executed in order  

Note : “⬇ My section” represent the beginning of a box and
”\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_” it’s end.  
Note 2 : the quick favorite will be the basic custom menu created before
you make changes, but the user will be able to delete and modify it.  
Note 3 : The Pie menus will forbid to create more than 8 operations, as
these kind of menu cannot support more.  

#### Shortcuts

As in ​​Alexander Dumproff​ suggestion, I’ll add shortcuts to easily add
or remove an operator from custom menu. What might be useful would be to
separate add and remove from menu depending on whether the operation
already is, or not in the menu and remove the button, or at least
deactivate it if it’s impossible (in short, the buttons will have to be
managed dynamically).  
![../../../images/Mockup\_Custom\_Menu\_shortcut.png](../../../images/Mockup_Custom_Menu_shortcut.png
"../../../images/Mockup_Custom_Menu_shortcut.png")  

#### Area Mode

For the Area display, something as simple as this would be efficient,  
![../../../images/Mockup\_Custom\_Menu\_view\_screenshot.png](../../../images/Mockup_Custom_Menu_view_screenshot.png
"../../../images/Mockup_Custom_Menu_view_screenshot.png")

but it might be useful to also add the possibility to add your own
properties, like position, size, etc.  
Those would only be available on the area and not with the shortcuts.  
This way, the user can have a menu where only the properties that they
look at and change often are displayed.  
The area display will not be available with radial Pie as it would make
empty and unstructured display once converted in list mode.

![../../../images/Mockup\_Custom\_Menu\_View.png](../../../images/Mockup_Custom_Menu_View.png
"../../../images/Mockup_Custom_Menu_View.png")  

### Project Schedule

<table>
<thead>
<tr class="header">
<th><p>Timeline</p></th>
<th><p>To do list</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Until 31​th​ May (before GSoC)</p></td>
<td><ul>
<li>Understand blender sourcecode better, especially the menu part</li>
<li>Get a better idea of whatoptions will need to be set inthe menu and how to implement them.</li>
</ul></td>
</tr>
<tr class="even">
<td><p>Until 3rd​ July (first month)</p></td>
<td><ul>
<li>Create menu in preference</li>
<li>Add/remove item</li>
<li>Re-order items</li>
</ul></td>
</tr>
<tr class="odd">
<td><p>Until 31​th​ July (second month)</p></td>
<td><ul>
<li>Customize lists (icon, content,name, ...)</li>
</ul></td>
</tr>
<tr class="even">
<td><p>Until 20st august (third month)</p></td>
<td><ul>
<li>Add different menus</li>
</ul></td>
</tr>
<tr class="odd">
<td><p>Until 31​th​ august (end of GSoC)</p></td>
<td><ul>
<li>Finish and clean code and documentation</li>
<li>If time to spare: make menu displayable as any other displayable menu.</li>
</ul></td>
</tr>
</tbody>
</table>

### Availability

All my schools projects should be finished when the GSoC begin, so I
should always be available, even if school isn’t finished (we got
schedule amenagement rights anyway).  
\=== Bio === Hello, my name Alexia, a french student at EPITECH (master
in computer science) and on my first year.  
I am 19 and I code since I am in middle school. I began with Java and I
continued with some other stuffs (Unity and c\#, c++). With my school I
got to know how to code in C and use tools like writing Makefiles,
valgrind, GDB or git.  
So I am fluent in C, know how to use C++ and a little of python (I mean,
I’ve used it to use tensorflow and some other little projects, but I’m
not as good as in C, that I use 5h per day).  
As for other languages, I know java, kotlin and c\#.  
My school education system is projects based which mean we did a lot of
projects, like creating ourown unix shell or creating video games with
the CSFML only. Putting this aside, I’ve created my own game engine
(available on my github).  
Because of our school systems, we’re already used to work on projects,
as much alone as in a team, more or less big, and to manage many
projects at the same time. This way, I can work on this project the same
way I work on the others.  
The reasons I want to work on this project is that I used blender
casually when I was younger and more often now, in my school, because we
do need models for our projects, mainly game jams and we have a 3D club
which uses blender. The 2.8 update made blender prettier and more
intuitive, encouraging more people to try 3D modeling and I would like
to participate to give a better access to such technologies by improving
the interface and making it customizable.  
As for what I did for Blender, I’m working on the ​T74983 and T75203
​bug right now.
