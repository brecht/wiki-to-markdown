## Damien Picard (pioverfour)

Python scripting, French.

### Add-ons

I maintain the following add-ons:

  - [AnimAll](https://docs.blender.org/manual/en/latest/addons/animation/animall.html)
  - [Sun
    Position](https://docs.blender.org/manual/en/latest/addons/lighting/sun_position.html)
  - [After Effects
    Exporter](https://developer.blender.org/diffusion/BAC/browse/master/io_export_after_effects.py)

I also wrote the code for the 2D camera rig in the [Camera
Rigs](https://docs.blender.org/manual/en/latest/addons/camera/camera_rigs.html#d-rig)
add-on (though I did not come up with the rig itself).

### User name

My user name is a play on my last name, which in French is pronounced
\\pi.kaʁ\\. This may also be interpreted as “pi-quarter”, which is very
occasionally used to mean π × ¼, or π/4. It is mildly amusing, not
really funny once explained, and probably just puzzling when translated
to a different language, but I still think it’s delightful. It turns out
other Picards have thought of it long before I did.
