# Curve Improvements - Final Report (WIP)

## Project Summary

### Changes from Original Proposal

The improvements proposed in the original proposal consisted of two main
parts:

  - A new curve editing tool
  - A tool for rounding bends similar to bevels in Mesh editing

While the curve editing tool (later named Curve Pen) remained mostly the
same, the 2nd tool was implemented as a Geometry Node by the name: Curve
Fillet Node. The functionality of the node was fairly similar to what
was proposed. The idea for the superellipse profile was dropped and the
design task [T89227](https://developer.blender.org/T89227) (authored by
Hans) was more or less followed. Both tools were primarily targeted for
Bezier splines. However, they have now been adapted for Poly and NURBS
splines, where possible.

### Curve Pen Tool

The purpose of this tool is to support a smooth workflow for the process
of editing curves. This is achieved by providing a set of intuitive
shortcuts for editing curves (while also adding a few new
functionalities) within a single tool which removes the overhead of
switching between tools.

The functionalities of the tool are as follows:

  - Add control point: Click on empty space to add a new point connected
    to the currently selected control point(s). If no point is currently
    selected, a new spline is created.
      - Only clicking adds a point with handles of type \`vector\`.
      - \`Click\` and \`drag\` to move handles as \`align\` type
        handles.
      - Press \`Shift\` while dragging to set and move handles as
        \`free\` type handles.
      - \`Click\` two endpoints consecutively to close the spline to
        form a cycle.
  - Make cut: \`Ctrl\` and \`click\` on a spline segment to insert a
    control point into the spline at the clicked location.
  - Delete control point: \`Ctrl\` and \`Click\` on a control point to
    dissolve it.
  - Move control points and handles: \`Click\` and \`drag\` control
    points or handles to move them.
  - Alter segment: \`Click\` and \`drag\` on a curve segment to alter
    its shape.

![Curve Pen Demo](../../../videos/Curve-pen-demo-vid.mp4
"Curve Pen Demo") ***Note:** The icon for the tool would definitely
change. It's set to the curve draw tool's icon temporarily.*

### Curve Fillet Node

![Fillet Node](../../../images/Fillet_Node.jpg "Fillet Node") The
purpose of this node is to be able to create a bevel-like rounding at
control points. The purpose of each socket on the node is as follows:

  - \`Mode\`: Determines how many segments are within a given arc.
    Either \`Adaptive\` (corresponds to the \`Angle\` field) or \`User
    Defined\` (corresponds to the \`Count\` field).
  - \`Radius Mode\`: Determines how the radius is defined for each arc.
    Either \`Float\` or \`Attribute\` (both correspond to the \`Radius\`
    field).
  - \`Curve\`: Takes the curve as input.
  - \`Angle\`: The threshold angle of the arc which when exceeded adds
    another control point. Lower the threshold angle, higher the
    resolution and thus, the arc becomes more circular.
  - \`Count\`: The number of segments per given arc. This count would be
    common to all vertices.
  - \`Limit Radius\`: Prevent overlapping of arcs that may occur due to
    the radius being larger than what can fit at the point.
  - \`Radius\`: The radius of each vertex (may be reduced if \`Limit
    Radius\` is enabled). Use \`Radius Mode\`=\`Float\` to define a
    common radius or \`Attribute\` to define a distribution of radii.

![Curve Fillet Demo](../../../videos/Curve-fillet-demo-vid.mp4
"Curve Fillet Demo") ***Note:** The gif above is a demo of the usage of
the curve fillet node on different types of splines. Poly splines are
used for demonstrating the \`Mode\` options and Bezier splines with
vector handles are mainly used to demonstrate the \`Radius\`
functionality. The built-in \`radius\` attribute is used for the
attribute field. Also, note how the \`Limit Radius\` option prevents the
radius from getting larger than what can fit.*

## Important Links

  - Proposal: [Curve
    Improvements](https://wiki.blender.org/wiki/User:Dilithjay/GSoC_2021/Curve_Improvements)
  - Weekly Reports Thread: [GSoC 2021: Curve Improvements: Weekly
    Reports](https://devtalk.blender.org/t/gsoc-2021-curve-improvements-weekly-reports/19108)
  - Feedback Thread: [GSoC 2021: Curve Improvements:
    Feedback](https://devtalk.blender.org/t/gsoc-2021-curve-improvements-feedback/19109)
  - Curve Pen Design Task:
    [T88957](https://developer.blender.org/T88957)
  - Curve Fillet Design Task:
    [T89227](https://developer.blender.org/T89227)
  - Curve Pen Branch:
    [soc-2021-curves](https://developer.blender.org/diffusion/B/browse/soc-2021-curves/)
  - Curve Fillet Branch:
    [soc-2021-curve-fillet](https://developer.blender.org/diffusion/B/browse/soc-2021-curve-fillet/)
  - Curve Pen Differential:
    [D12155](https://developer.blender.org/D12155)
  - Curve Fillet Differential:
    [D12115](https://developer.blender.org/D12115)

## What's Left?

Both the tools are currently (August 19th 2021) under review at the
differentials mentioned above. I will be trying my best to get them
merged with master in the upcoming weeks. I'll also look into fixing any
bugs that may arise in the tools.

## What's Next?

There has been minor discussion on using the curve fillet node's code
for an edit mode tool (similar to what was initially planned).
Additionally, there are some functionality improvements that can be made
to the curve pen tool such as support for snapping, precision control
and vertex slide. I would like to look into implementing these
improvements alongside helping out any other Blender development in
whichever way I can.

## Acknowledgement

I would like to thank my mentors, Hans Goudey and Falk David for the
constant support they provided and always being there to clear up any
questions I had. This was an amazing experience and I have them to thank
for a lot of what I learned along the way. Also, thank you to everyone
in the community who provided feedback and helped improve the project.
