## Week 1 (June 7th to June 13th)

  - Created a design task:
    [T88957](https://developer.blender.org/T88957)
  - Created a branch:
    [soc-2021-curves](https://developer.blender.org/diffusion/B/browse/soc-2021-curves/)
  - Started working on improving new curve edit tool.
  - Minor refactoring of the existing implementation of the curve edit
    tool.
  - Renamed the tool in the implementation as \`Curve Pen\`. The name
    may or may not change in the future, but for now, I will refer to
    the tool as \`Curve Pen\`.
  - Began working on a more efficient way of identifying the location of
    the cut on the curve.

Here's a small demo of the Curve Pen that I made for the design task.
![../../../images/Curve-edit-tool-demo.gif](../../../images/Curve-edit-tool-demo.gif
"../../../images/Curve-edit-tool-demo.gif")

### Plan for next week

  - Complete the implementation of the more efficient method mentioned
    above.
  - Refactor the code by breaking up into smaller functions.
  - Try using modal keymaps instead of hard coded key bindings.
