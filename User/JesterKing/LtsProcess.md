With the first Long Term Support (LTS) release coming up in the form of
Blender 2.83 we have to adjust some of our processes to incorporate the
long timespan for this release.

The LTS was proposed by Ton Roosendaal in the blog post for
[2020-2025](https://code.blender.org/2020/02/release-planning-2020-2025/)
planning.

The LTS releases will have a two-year support timeline. With a new LTS
release every year in May this will result in having to maintain no less
than three releases simultanously. This means extra care needs to be
taken for code and documentation.

<table>
<tbody>
<tr class="odd">
<td><div class="note_title">
<p><strong>Discussion</strong></p>
</div>
<div class="note_content">
<p>This topic is on devtalk in<strong><a href="https://devtalk.blender.org/t/long-term-support-and-its-implications/12525/">this thread</a></strong>.</p>
</div></td>
</tr>
</tbody>
</table>

### User Manual

We will have to stop working on the manual in trunk only, because with
the current workflow we have no way to separate out changes for
different manual versions.

From April 9 onwards we should create a blender-v2.83-release branch in
the documentation project alongside the blender-v2.83-release branch in
blender.git and its submodules.

When the branching is done documentation writers will have to take extra
care to what version of the documentation changes, additions and
deletions are made. At best there will be three concurrent versions to
maintain (for example 2.83 LTS, 2.93 LTS, 3.0 non-LTS).

### API

With a Long Term Support release comes the promise that APIs across the
board are stable. There should be no breakage. An add-on developed for
the first 2.83 LTS release should work also in the last patch release
for 2.83 two years down the line. Maintaining APIs should be carefully
planned for no less than three concurrent releases.

### Triaging, Bugfixing and Code

Triaging of bug reports should be done against all LTS versions, and the
latest non-LTS version available. In August 2021 that means current
patch release of 2.83 LTS, current patch release of 2.93, and the first
3.0 release of August 2021.

When a bug is reported we need information for what the earliest version
the bug exists, and what version still worked.

A bug should be fixed first in the oldest release branch, then
propagated through all necessary branches. In 2020 it will be in two
branches, but in May 2021 it will be three branches (2.83 LTS, 2.93 LTS,
master - work for 3.0 non-LTS).

#### Code Versioning and Splash

I have created a proposal diff
[D7319](https://developer.blender.org/D7319) that introduces a new part
in the versioning scheme. In BKE\_blender\_version.h there will be a
\`BLENDER\_VERSION\_SUPPORT\_TYPE\`, which is either empty or set to
\`lts\`. When set to \`lts\` on the splash screen \`LTS\` will be
suffixed after the release version, separated by a space. The \`LTS\`
string will be used also in the System Information output. Finally it
can be part of the release package file name.

### Patch Release Cadence

This is still not defined well, but I currently think that we should do
a monthly patch release as long as there are bugfixes available. I
expect the first few months to require the biggest efforts after a new
LTS release, but then gradually settle in a relatively low-effort run
until the EOL point for the LTS release. When a month has seen no bug
fixes for a particular LTS release then there is no need to create a
patch release either.

How well we will manage tripple-releases comes down to how strict and
careful developers make their bug fixes taking \_all\_ then current
releases into account.

It should be the task of the release manager to keep on top of the LTS
releases and bugs that are reported against them to be able to prepare
patch releases.

With a monthly patch release cycle there should be no need to try and
get a fix in quickly before the patch release goes out since the next
won't be out very long anyway.
