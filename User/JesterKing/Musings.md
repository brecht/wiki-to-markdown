\_\_TOC\_\_

Here be some open thoughts and ideas, my takes on Blender, development,
userbase.

# bug tracking / technical support

## current state

To me the current approach to bug tracking seems flawed. Through
triaging there is quite an aggressive approach to closing as many
reports as possible, with as goal to keep open reports low.

It appears that this is an artificial way of keeping the numbers down
instead of actually addressing all issues. Certainly the bug tracker
contains reports that are more along the lines of technical support.

Further closing issues that have no activity for a longer period of time
isn't a good thing. Sure, there was no input, but, unless fully proven
otherwise, the issue is still valid. Closing it makes the numbers look
nicer, but it doesn't mean they are actually representing the current
state of Blender correctly.

This approach to bug tracker where having 50-150 open bug items is the
goal has been used since 2002. But doing so with a heavy hand and
aggressively is not a great way to work together with the community.

Use of the word \`Invalid\` to close bug reports is problematic in
communication towards users. This is something we need to adress. I hope
that this page with thoughts/proposals help moving to a future where we
do better.

If there are X bugs in Blender, then there should be X reports for that,
not fewer. Open for 3 years? If the issue isn't fixed it is still a bug,
however much you wouldn't want it to be. To close it you'll have to
actually fix it.

## first contact point for users

To address the incoming issue flow in a more manageble way I'd propose
to remove the bug tracker as first point of contact for users.

In Blender GUI instead of having \`Help \> Report a bug\` that opens the
bug tracker there could be \`Help \> Ask for help\` where the user would
be sent to a support forum. This would be essentially a technical
support approach first, where there could be a team of tech support
people, along with community involvement helping out users. Existing
topics could be used to build out a knowledge base of steps for users to
take to fix their issues like updating drivers or maybe not
understanding a feature correctly, or differences between Blender
versions.

The bug tracker will still be open for those users who still want to log
bugs, but I believe that having the intial point of contact be a support
forum, well managed, will result in getting less noise in the tracker.

When a user comes to the tracker to reports an issue they are having we
must assume that there is an actual hindrance for the user. There are
types of reports that will end up closed, very likely as \`Invalid\`.

The problem is that this does not take into account the issue the user
is having, it is very real for the user.

On a support forum with good technical support available this can be
addressed in a good manner that also communicates to the user the issue
was there, and that there are steps through which it can be resolved.
This will leave the user with a good feeling. They no longer have to
wonder why their issue is \`Invalid\`, even though they are actually
experiencing it. Because nothing necessarily got solved for the user.

We'll end up with two useful results: 1) the issue for the user gets
resolved and is an accessible format available for reuse, and 2) the bug
tracker gets less noise

## testing of fixes

Currently a bug report will be closed the instance a fix is landed. I
believe this is wrong. Instead of marking it as \`Closed\` there should
be a new state: \`Needs Testing\`. If we had a technical support team we
could task them with testing as well. Once the test has landed there
could be some automatic way for our buildbot to create new builds of
that commit (or newer of that branch if more commits landed before the
build could be started).

We could have a tool (in Blender) that asks if that current build (based
on the SHA1 of the last commit that went in) contains the fix for the
issue (maniphest report TNNNNNN). The tester should test and then either
mark as \`Closed\` (or rather even move to documentation as I propose
below) or re-open the issue if the fix is incorrect in any way.

## documentation

As part of bug fixing, but also new feature landing, tasks could have
one extra state that could be useful to have. Instead of marking the
task as \`Closed\` it could be set to \`Needs Documentation\`. This
means the documentation team next considers the task and everything that
happened. If documentation needs changing then the necessary changes are
made, unless no changes are necessary. Only then the task would be
marked \`Closed\`.

## some graphs

Created using <https://dot-to-ascii.ggerganov.com/>

### proposed lifetime of a bug task

``` 

              +------------------------+
              v                        |
+-----+     +------+     +-----+     +------+     +-----+     +------+
| bug | --> | open | --> | fix | --> | test | --> | doc | --> | done |
+-----+     +------+     +-----+     +------+     +-----+     +------+
```

Once a bug has been identified it is logged in the tracker and open. A
developer will work on a fix and land code for it.

At the moment of writing (2020.06.24) the tracker will automatically
close the bug task, but with comprehensive testing should not be the
flow. Ideally the reporting user would test this, but if we have access
to testers who have the capability to test properly then we should use
that. As long as the tester is \_not\_ the person writing the fix,
\_nor\_ the person reviewing the code.

I propose the task be moved to a Needs Testing phase. If the fix is
found to be good then the task should be moved to documentation. Here
all necessary documentation locations are verified and updated if
necessary.

Only once all steps have been gone through a bug task can be closed,
until then it is to remain in an open state.

### proposed lifetime of a feature task

``` 
                  +------------------------------+
                  v                              |
+---------+     +------+     +-----------+     +------+     +-----+     +------+
| feature | --> | open | --> | implement | --> | test | --> | doc | --> | done |
+---------+     +------+     +-----------+     +------+     +-----+     +------+
```

A feature task is pretty much the same as a bug task flow-wise. The
testing and documentation stages may be more labour intensive, though.

### proposed flow of handling through a support forum

``` 
                                                       +----------------+
                                                       |     issue      |
                                                       +----------------+
                                                         |
                                                         |
                                                         v
                                                       +----------------+
                                                       |      post      |   <+
                                                       +----------------+    |
                                                         |                   |
                                                         |                   |
                                                         v                   |
                                                       +----------------+    |
                                                       | ask_questions  |   -+
                                                       +----------------+
                                                         |
                                                         |
                                                         |
                       +- - - - - - - - - - - -+         |
                       '                       '         |
                       '                       '         v
+----------------+     ' +-------------------+ '       +----------------+
| refer_existing | <-- ' |   support_issue   | ' <--   |    clarity     |
+----------------+     ' +-------------------+ '       +----------------+
                       '                       '
                       +- - - - - - - - - - - -+
                           |                             |
                           |                             |
                           |                             v
                           |                         +- - - - - - - - - - +
                           |                         '                    '
                           v                         '                    '
                         +-------------------+       ' +----------------+ '     +--------------------+
                         | mark_new_solution |       ' |      bug       | ' --> | refer_existing_bug |
                         +-------------------+       ' +----------------+ '     +--------------------+
                                                     '                    '
                                                     +- - - - - - - - - - +
                                                         |
                                                         |
                                                         v
                                                       +----------------+
                                                       | create_new_bug |
                                                       +----------------+
```
