# Continuos Integration for Patch Review

*problem*: code lands in main development branch, but doesn't compile on
all platforms

*problem*: code lands in main development branch, but not all tests pass

We have a [code review guideline](Tools/CodeReview) that
outlines many steps and points to check for a good patch and review
process. Despite that it still happens occasionally that the compile
process breaks, or tests start failing.

To solve these two problems we can use a form of continuos integration
that helps reviewers and patch submitters address these two issues.

Compiling and executing unreviewed code is always a security risk. One
way to prevent malicious code from wrecking havoc is to run the
compilation and tests in a virtualized environment. But this in itself
still doesn't mitigate the risk factor associated with landing
(unreviewed) external code in the repository. This would bring extra
tooling into play that adds to the complexity of the building
infrastructure.

I propose to provide compiling and testing a patch as functionality that
can be triggered explicitly by a reviewer. The reviewer essentially
functions as the security check of the patch before the compile and test
is conducted.

``` 
                                                      +-------------------------------------+
                                                   +> |            Submit Patch             |
                                                   |  +-------------------------------------+
                                                   |    |
                                                   |    |
                                                   |    v
                                                   |  +-------------------------------------+
                                                   |  |            Review Patch             | -+
                                                   |  +-------------------------------------+  |
                                                   |    |                                      |
                                                   |    | Patch according standards            |
                                                   |    v                                      |
+-----------------------------------+  Success     |  +-------------------------------------+  |
| Automatic Tag Patch with Build OK | <------------+- |        Compile and Run Tests        |  |
+-----------------------------------+              |  +-------------------------------------+  |
  |                                                |    |                                      |
  | CI ok, review ok                               |    | Fail                                 | Patch not according standards
  v                                                |    v                                      |
+-----------------------------------+              |  +-------------------------------------+  |
|       Accept And Land Patch       |              |  | Automatic Tag Patch with Build FAIL |  |
+-----------------------------------+              |  +-------------------------------------+  |
                                                   |    |                                      |
                                                   |    |                                      |
                                                   |    v                                      |
                                                   |  +-------------------------------------+  |
                                                   +- |              Fix Patch              | <+
                                                      +-------------------------------------+
```

### Resources

Graph source at <https://dot-to-ascii.ggerganov.com/?src_hash=06069a26>
