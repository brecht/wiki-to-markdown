\_\_TOC\_\_

## The person

  - Name  
    Nathan Letwory
  - On developer.blender.org  
    [jesterking](https://developer.blender.org/p/jesterking/)
  - On blender.chat  
    [jesterKing](https://blender.chat/direct/jesterKing)
  - E-mail  
    nathan at blender.org
  - Activities  
    developer infrastructure
    onboarding / help

## Help\!

Contact person for developers new and old. With questions and/or
suggestions about the way Blender is developed you can poke me on the
bf-committers mailing list, or in \#blender-coders over at
[blender.chat](https://blender.chat).

If you wish to contact me privately please use private messaging on the
devtalk.blender.org forum: [my
profile](https://devtalk.blender.org/u/jesterKing). You can also poke
with a private message on [blender.chat](https://blender.chat), but
[devtalk](https://devtalk.blender.org) is preferred.

## Tracking the work

  - [/Reports/](/Reports/)
  - [/ReleaseCycleNotes/](/ReleaseCycleNotes/)
  - [/Playbook\_Merge\_Handling/](/Playbook_Merge_Handling/)
  - [/CyclesRepositorySyncing/](/CyclesRepositorySyncing/)
  - [/InfraProject/](/InfraProject/)
  - [/LtsProcess/](/LtsProcess/)
  - [/CiProposal](/CiProposal)
  - [/BlenderLtsReleaseProcessProposal](/BlenderLtsReleaseProcessProposal)
  - [Process/Release\_Checklist/Actions\_Per\_Bcon](../../Process/Release_Checklist/Actions_Per_Bcon.md)
  - [/Musings](/Musings)
