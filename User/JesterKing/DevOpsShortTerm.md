## Targets and proposals for short term DevOps work

### For period 2020.04.21-2020.04.25

  - canned responses. Proposal: directly add drop-down with canned
    responses
      - canned responses pulled from Phabricator Paste
          - editable by administrator only
          - tagged with a Canned Response tag
          - can use built-in facilities to query pastes and parse
            necessary bits to populate dropdown and get content into
            text area on selection

<!-- end list -->

  - prevent certain projects/milestones from being set as reviewer
    ([T69671](https://developer.blender.org/T69671))

### For period 2020.04.27-2020.05.02

  - Move steam package building to buildbot
      - need to figure out when to actually let the steam sdk push files
      - maybe enable some beta program for Blender on steam?
  - Move windows store package building to buildbot
      - uploading has to be done manually still (many steps to go
        through manually on store page)

### Bigger tasks, undetermined order

  - Blender ID for phabricator
    ([T69300](https://developer.blender.org/T69300))
      - rebase existing patch
        [D6015](https://developer.blender.org/D6015)
      - improve patch
      - Set out steps how to apply and take into use once patch approved

<!-- end list -->

  - build differentials on buildbot
      - build workers for all major platforms.
      - determine if rebuild from pristine image necessary for each
        differential
      - start with building differentials against \`master\`
      - prepare clone copy (git + svn)
          - checkout correct sha1 + revision
          - apply differential
          - copy into shared folder for build workers
