\_\_TOC\_\_

  - get [phab on docker](/PhabOnDocker/) running
  - templatize config settings
      - settings to be made on command-line
      - settings that can be done through conduit
      - settings that need to be in config files
  - pull sensible data set from dev.b.o
      - put into docker phab for dev test
  - address issues
      - for instance [T64092](https://developer.blender.org/T64092) or
        [T72894](https://developer.blender.org/T72894) author can't
        change prio when author is not moderator. Related
        [D5521](https://developer.blender.org/D5521))
      - patch review workflow (checklist)
      - canned responses for triaging
          - make sure is part of template input when seeding

<!-- end list -->

  - get buildbot on docker as part of development environment

<!-- end list -->

  - release related task
      - code signing on mac
      - integrate steam updating
        [T68984](https://developer.blender.org/T68984)
      - windows store integration
        [T68984](https://developer.blender.org/T68984)
      - flatpak
  - ci/builds/tests for differentials
  - ci for regular commit work

<!-- end list -->

  - render performance evaluation and tracking. Potentially flag commits
    during which render times dramatically increase. Render correctness
    is assumed verified during normal testing.

<!-- end list -->

  - crashdump collection

<!-- end list -->

  - manage git hook better
      - control over hook via phab admin app?
