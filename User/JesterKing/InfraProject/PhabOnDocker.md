\_\_TOC\_\_

# Main steps

  - install docker and docker-compose
  - note that folder sharing is required, since this docker image
    depends on the phabricator repository from developer.blender.org to
    be cloned into the right location (initialize.py should take care of
    that)
  - on Linux ensure the user you run docker with is in in the docker
    group: \`sudo usermod -aG docker $USER\`. Don't forget to logout and
    back in
  - clone <https://github.com/jesterKing/phabricator>
  - check out \`blender\_phabricator\_docker\`
  - \`cp settings\_config.py.template settings\_config.py\`
  - run the \`initialize.py\` script (python 3.5 minimum)
  - update /etc/hosts as suggested by \`initialize.py\` example:
    \`192.168.1.100 phab.test.int\`
  - note that \`initialize.py\` suggests a number of steps to get config
    applied
      - change \`PHABRICATOR\_HOST=phab.test.int:62080\` if you use a
        different domain name
  - \`docker-compose up\` or for running in background \`docker-compose
    up -d\`
  - browse to <http://phab.test.int:62080>
  - to use command-line access in your phabricator container
      - \`docker exec -it phab\_container\_id bash\`
      - this gives you an interactive bash as root into the container
      - use \`su - git\` to become the phabricator user on the container
      - \`git\` user has home at \`/srv/phabricator\`

# Steps to set up policies and reconfigure

To get set up the policies properly you'll have to work around a bit of
chicken and egg problem:

  - Do all of the steps above
  - Log into your phabricator instance
  - Create the policies and note their PHIDs (it is useful to enable
    developer settings where such data is always easily accessible while
    using your phabricator in the browser)
  - Set the PHIDs to the respective policy variables in
    settings\_config.py
  - In your docker instance bash shell ensure the settings are visible
  - run the apply\_config.py script again

# Locations

ROOT will be your clone of the docker repository clone

## Phabricator

The location of the phabricator clone is

ROOT/srv/docker/phabricator/phabricator

## MySQL

The location of the MySQL database files is

ROOT/srv/docker/phabricator/mysql

# Would Be Nice TODOs

  - Use a Linux distribution that is more commonly used among Blender
    developers instead of OpenSuse
  - Complete blender-conduit.py to pull a usable subset of data from
    developer.blender.org to seed the docker instance with
  - Automate setting up of policies the when the database needs to be
    initialized (either very first run, or database location was cleared
