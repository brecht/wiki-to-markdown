## Thoughts regarding release managament

### Project milestone usage

To get a better picture of what is planned for releases we can use the
project milestones.

Devs are encouraged to tag their tasks and bug reports with the
milestone they want to have them done by.

The tag for [2.81](https://developer.blender.org/project/view/88/)

With milestones once fix/task/feature is released this tag for that item
should not be touched anymore.

![timeline of release](../../../images/Blender_release_cycle_281.png
"timeline of release")

Based on 3-month cycle, that gives around 2 months of free feature and
improvement time. Cut-off date at least one month prior to projected
release day. But preferrably minimum 6 weeks. On that day stabilizing
branches are created for main repository and all submodules.

Fixes go directly into stabilizing branch, merges go from stabilizing
branch to master, never the other way around. Release manager has right
to revert commits in stabilizing branch if they don't meet requirements.

## Stabilizing branch commit requirements

  - fixes only for bugs that have been reported in developer.blender.org
    *and* has been tagged for upcoming release
  - no commit should break unit tests
  - no commit should break compiling on any supported platform
  - only devs with tasks assigned for the branch work in stabilizing
    branch

## Release cadence

Three-month cycle, with release month in bold: October, **November**,
December, January, **February**, March, April, **May**, June, July,
**August**, September
