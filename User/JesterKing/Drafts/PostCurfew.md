# Tracker management

Currently there is a [Tracker
Curfew](https://www.youtube.com/watch?list=PLyGoWAGEPJ1DLzhm1_dhuLVe7wT1kBFNP&v=HDE2ARiN1a0&feature=emb_title)
in effect. This effort is focused on short-term tracker handling with as
goal to bring down the list of open bugs to a more overseeable amount.

I see there is still a problem once curfew gets lifted, since the influx
of reports is what got us into the current state.

As such I think it is important to address the problem at its root.
Simple handling the result is not going to be sustainable.

## Proposal

  - Limit bug reporting ability in developer.blender.org to a smaller
    group
  - User bug reports should go to a forum (say under devtalk), where the
    community can work together to distill reports to workable and good
    quality bug reports
  - Once enough good data has been gathered a bug be logged at
    developer.blender.org

## Projected results

Using a forum as a prescreening or pretriaging area I think we can
handle several issues in one go:

  - lower noise/signal ratio on developer.blender.org
  - create a easier-to-use knowledge base on issues
  - handle support issues outside developer.blender.org on the forum
  - involve the community at large more in improving Blender, without
    having to be a programmer
