# Goal

Facilitate work planning process for devs and help roadmapping Blender
development.

# Problem

Development planning is a difficult task. At this time of writing we're
working with a release cycle of around 3 months. The release cycle
dictates the planning efforts. The length of the release cycle, and thus
planning cycle, makes it hard to do proper planning. People are better
at estimating needed effort on short term than on long term. Currently
this results in overcommitting of resources or, differently put,
optimistic planning. Result is that especially towards the end of the
(release) cycle pressure on devs grows unnecessarily high.

This we need to address.

Instead of release cycle dictating planning effort, it would be better
to let planning effort dictate release cycle.

When all buildbot builds are properly signed and notarized the process
should allow to release in theory whenever we feel is ok, regardless of
planning cycle.

## ...

  - easier to plan short term
  - easier to oversee necessary changes in upcoming planned milestone
  - smaller chunks of work don't generate as much pressure as big chunks

steps to take regularly

  - check current todo list
      - can I manage current todo item? Ask for help or talk to somebody
        if you're unclear about steps to take to solve. This is a team
        effort, no need to keep problematic tasks to yourself without
        involving others and sharing
      - does rest of list fit current workload?
          - tell people involved if you think it is better to reschedule
            an item
              - to next cycle, or
              - further into future
  - preparing a fix for issue
      - ensure tests are done
      - ensure commit message is complete, including reference to issue
      - once landed ensure issue is properly handled (double-check
        status was actually changed)
  - balance maintenance/bug fix coding with new feature work when
    possible

# Below some pointers, well be re-incorporated in text above

## Potential Result: Moving towards frequent releases

  - (re-)introduce official test builds, akin release candidates. Weekly
    or once every two weeks.
  - if four (or two) builds ok, release new build.
  - assume major releases are 2.8\*, 2.9\*, 3.0\*, 3.1\*
      - slight adjustment of branching
          - blender-v2.8x
              - blender-v2.84-release
              - blendere-v2.85-release
              - ...
          - blender-v2.9x
              - blender-v2.90-release
              - blender-v2.91-release
              - blender-v2.93-release
              - ...
      - master would have number of next major release
      - despite increased release count
          - no need for a/b/c releases, next release is always \~4 weeks
            out, with 3/4 test releases inbetween
          - think ahead in \~10 shorter releases, keep bigger features
            in \~1 year schedule (can earlier, but then would possibly
            have major release earlier)
          - it is much easier for a person to see one month ahead when
            trying to plan what tasks to do
          - major releases once a year on average
  - monthly releases x.y1, x.y2, x.y3, in one year \~10 releases. Fewer
    if stable, or looks like new major release makes sense
  - devs should *not* worry about release cycle other than fixing
    scheduled bugs. test builds can be stopped, so can monthly releases,
    in case of critical bug.

# Steps

## Integration into Buildbot

  - macos codesigning and notarization
  - steam build generation / upload
  - windows store build generation / upload

## Automated testing of differentials

  - dev can trigger build for differential
      - apply to release branch or master
      - run tests
      - clang format?
      - set differential status: passed, not passed
  - if necessary differential should include new tests to cover new code

## Automation of localization submodule updating

  - Ask Bastien what steps are done
  - Figure out how this can be automated
  - Possibly part of testing
