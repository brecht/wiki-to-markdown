## todo

  - rewrite dev env pages
  - rewrite first steps (code changes)
  - ensure clear playbook for changes -\> differential -\> review -\>
    testing -\> landing (including clear picture)
  - set out timeline for doing [live
    get-started](LiveGetStarted.md)
    tutorials (twitch/youtube live streaming?)
      - git clone / arcanist setup / cmake usage
      - end january / february
  - improve developer info pages (communication IRC -\> blender.chat,
    etc, module owners, contact info), link up and make easily findable
