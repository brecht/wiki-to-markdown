\_\_TOC\_\_

# Blender LTS Release

*goal*: set out guidelines for LTS releases and testing for LTS.

*concern*: relative large number of patches after LTS release, not much
testing of LTS patches currently.

Releasing a Blender LTS release, or a Blender LTS bugfix release, is in
almost all aspects similar to a regular Blender version release. The
process for patching an LTS release branch is described in [the Blender
LTS process
description](../Jbakker/projects/BlenderLTS/ProcessDescription.md)
proposal by Jeroen Bakker. This process replaces in an LTS cycle
essentially phases bcon1 through bcon4 of a regular release.

It makes sense to reuse as much of the existing release procedure as
possible, to ensure LTS engineer can fully focus on verifying and
accepting patches into the LTS release branch.

## Approach

The following process will cover the releasing part of LTS:

  - After initial LTS release the branch needs to be rolled to next
    version (i.e. 2.83.0 -\> 2.83.1) and release cycle should be best to
    beta. Argument for this is after the process steps list.
  - Release weekly.
  - After any normal release (regular versions and first LTS) keep
    option to wait with first maintenance release for up to 4 weeks, but
    resume with weekly release after the first maintenance release.

## Process

The release process itself will be as follows:

1.  LTS engineer (LTSE) lands fixes in release branch
2.  LTSE or Release team (REL) rolls release cycles (beta -\> release)
3.  LTSE or REL tags (note, all involved submodules as well)
4.  REL/LTSE activates buildbot workerks
5.  REL+LTSE verify builds are ok (install, execute, no obvious
    misbehavior)
6.  LTSE Upload builds to download.blender.org
7.  REL Prepare packages for snapcraft.io, Steam, Windows
8.  REL Upload to snapcraft.io, Steam, windows Store
9.  REL Update maintenance release changelog on blender.org
10. REL Update blender.org links for maintenance release
11. LTSE or REL rolls version and cycle (i.e. 2.83.1 -\> 2.83.2, and
    release -\> beta)

Rolling of version and release cycle at the end of the release process
is necessary to ensure any following build up until next maintenance
release can be distinguished from official release.

In closing testing of releases is minimal. The weekly release rythm
gives in most cases enough throughput of patches to address issues.

## Roles

  - LTS Engineer: Jeroen Bakker
  - Release team: Nathan Letwory

## Open Issues

  - How to handle LTS and regular releases on
      - Steam
      - Windows Store
