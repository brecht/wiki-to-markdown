\_\_TOC\_\_

# Reports 2019

## December 30 - January 03 / \#27

  - Tuesday and Wednesday off - in Finland holidays
  - light presence for 'emergency'
  - updating dev machine
  - recreate dev environment and clean out old phabricator tinker
    installation
  - note: January 6th is national holiday in Finland

## December 23 - December 27 / \#26

  - light presence, daily on \#blender-coders
  - wiki
  - phabricator / conduit tinkering before getting started with dump
    creation

## December 16 - December 20 / \#25

  - Further [planning
    document](../Drafts/PlanningCycle.md)
  - Help a few devs getting started
  - Feedback on curfew
  - Meeting

## December 9 - December 13 / \#24

  - Release cycle / documentation
  - 12th I was away all day

## December 2 - December 5 / \#23

  - Peruse the developer documentation, planning content updates
  - Steam builds for 2.81a
  - 6th was national holiday
  - Note for \#24: 12th of December I'll be AFK

## November 25 - November 29 / \#22

  - Dev support
  - Turned back to wiki and documentation to figure out steps to take
  - Note for \#23 6th is national holiday

## November 18 - November 22 / \#21

  - Release 2.81
  - Land splash, start build process
  - Create Steam builds
  - Try to figure out Windows Store builds

## November 11 - November 15 / \#20

  - Release management (postpone 2.81)
  - Developer contact
  - Meetings
  - Note for week \#21: I will be AFK during Monday 18th from 12:00 EET,
    and on Thursday November 21st the whole day.

## November 4 - November 8 / \#19

  - Release management
  - Discuss patch review and development/module structure proposal
    documents by Dalai
  - Developer contact
  - Meetings

## October 27 - November 1 / \#18

  - Prepare signing for handover
  - Developer contact
  - On-boarding improvement brainstorming

## October 21 - October 25 / \#17

  - Discussions with Dalai
  - Feedback on Blender dev flow document
  - Follow bconf when possible

## October 13 - October 18 / \#16

  - developer contact (devtalk)
  - buildbot
  - switch tasks to cope with changed circumstances in resource
    allocation

## October 7 - October 11 / \#15

  - clean-up code signing patch for buildbot, took more time than
    anticipated
  - direct contact with developer (devtalk/e-mail)

## September 30 - October 4 / \#14

  - devtalk
  - buildbot
  - developer.blender.org (test patch for clang support on Windows, et
    al)

## September 23 - September 27 / \#13

  - devtalk, expectation management
  - release cycle
  - meetings
  - buildbot, discuss build signing (certificates, tools)

## September 16 - September 20 / \#12

  - Buildbot REST API usage with authentication through OAuth token
  - Figure out usefulness of a supporting app in Phabricator for driving
  - A few days of lowered productivity due to moldy air :/

## September 9 - September 13 / \#11

  - Buildbot / Phabricator tinkering continued

## September 2 - September 6 / \#10

  - Finalize patch [D5656](https://developer.blender.org/D5656)
  - Start integration phabricator \<-\> buildbot
  - meetings (infra, coordination)

## August 26 - August 30 / \#9

  - Phabricator patches ([D5656](https://developer.blender.org/D5656))
  - Phabricator docs re buildbot integration
  - Herald rules

## August 19 - August 23 / \#8

  - Phabricator patches
  - crashpad server
  - meetings
  - replace laundry machine that was shooting electric sparks

## August 12 - August 16 / \#7

  - create milestones 2.81 and 2.82
  - propose release date for 2.81: 2019.11.14.
  - additionally propose some dates for release cycle leading up to 2.81
    (cut-off 2019.09.13, stabilizing branch 2019.10.01) and commit
    policy
  - help localisation team get started with weblate service
  - get [crashpad](https://developer.blender.org/D3576) backend work
    started. In the next week or two we should be able to do some
    testing in the wild
  - log weekly progress bot task

## July 29 - August 9 / \#5+\#6

  - [prepare rocket.chat
    script](https://github.com/jesterKing/rocketman) for autolinks to
    Diffs, Diffusion and Maniphests
  - Tinker with Phabricator Conduit API to grok developer.blender.org
    and get better picture of what is happening on
    [developer.blender.org](https://developer.blender.org)
  - Investigate projects, custom fields for issue tracking in concert
    with release management
  - [Crashpad integration](https://developer.blender.org/D3576)
  - [Release cycle notes](../ReleaseCycleNotes/index.md)

## July 15 - July 26 / \#3+\#4

  - Private chats with devs.
  - Chat with Ray Molenkamp about [Crashpad
    integration](https://developer.blender.org/D3576)
  - Notes for meetings:
    [here](https://lists.blender.org/pipermail/bf-committers/2019-July/050053.html).

## July 1 - July 12 / \#1+\#2

  - The first two weeks has been filled with getting an overview of the
    current state of Blender development. We organised several meetings
    with [Brecht](../../Brecht/index.md) and
    [Dalai](../../Dfelinto/Reports/2019.md) to discuss where we
    currently stand, and where we want to go.
  - As part of the work to do I started [a discussion on bf-committers
    mailing
    list](https://lists.blender.org/pipermail/bf-committers/2019-July/050017.html)
    to get a better undestanding of how devs perceive the processes.
  - Notes for meetings have been posted
    [here](https://lists.blender.org/pipermail/bf-committers/2019-July/050032.html)
    and
    [here](https://lists.blender.org/pipermail/bf-committers/2019-July/050051.html).
