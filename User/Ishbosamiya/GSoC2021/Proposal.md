# Project Title

Continued Development on Adaptive Cloth Simulator for Blender

# Name

Ish Hitesh Bosamiya

# Contact

email id: <ishbosamiya@gmail.com>

blender id/chat:
[ish\_bosamiya](https://developer.blender.org/p/ish_bosamiya/)

twitter: [@ishbosamiya](https://twitter.com/ishbosamiya/)

# Synopsis

My GSoC project in 2019, on the Adaptive Cloth Simulator, was an
exploration in adding support for parametric on-the-fly remeshing for
Blender's Cloth Simulator in order to provide high-resolution-equivalent
fidelity at reduced computational cost. During GSoC 2021, I will utilize
the insights gained, as well as previously fleshed-out core ideas, to
stabilize this feature for production. This will involve introducing a
new mesh structure, fixing bugs and regressions, refactoring to
integrate well with the latest Blender updates, etc. As a stretch goal,
I would improve Blender's cloth caching system to introduce support for
Adaptive Remeshing.

# Benefits

Artists often need to simulate clothing, but this comes with a variety
of technical challenges– mesh topologies need to be manually optimized
per scene, a careful balance must be maintained between visual detail
and reasonable simulation time by picking the right mesh resolution,
etc. all while working on their creative end-goal. The 2019 GSoC project
shows that these challenges can be overcome automatically by using
Adaptive Cloth Simulation. This year's project would allow for this
automation to actually reach artists' hands and help them save crucial
time, by providing easy to use parameters that automate the process of
finding the optimal mesh for every frame of the simulation.

An additional benefit to this year's project is that it provides the
necessary foundation to add Adaptive Cloth Tearing support to Blender.

# Deliverables

A production-ready implementation of the Adaptive Cloth Simulator. In
particular, I aim to have it ready to put into the review queue to be
merged into master. (Note: Actual merging into master is considered out
of scope simply because of the duration that such a large review would
take.)

Blender's current caching system is unsuitable for situations with
changing mesh topology, and thus as a stretch goal, I aim to introduce
caching support for systems like the Adaptive Cloth Simulator.

# Project Details

The Adaptive Cloth Simulator uses the methods proposed by "Adaptive
Anisotropic Remeshing for Cloth Simulation" by Rahul Narain, Armin Samii
and James F. O'Brien \[1\].

GSoC 2019 was an exploratory R\&D project to implement the Adaptive
Cloth Simulator. During the project, my mentor and I realized that it
would not be sufficiently stabilizable to merge into Blender's `master`
branch, due to the large complexity of the project. However, the project
helped provide the necessary insights and core implementations that
would be needed for a stable version to be built. GSoC 2021 will focus
on this stable version of the Adaptive Cloth Simulator, so as to be
eventually included in `master`. In particular, I will rework and
refactor the necessary components, in order to better integrate with
Blender's updated playback system; and perform the necessary bug-fixes
and other miscellaneous improvements to bring the Adaptive Cloth
Simulator to practical usability. More details on the GSoC'19 project
can be found in my 2019 proposal \[2\] and final report \[3\].

Major bugs (in the 2019 implementation) to be fixed:

  - Collapsing of edges doesn't work as expected. In particular, the
    mesh complexity does not correctly reduce, due to incorrect
    selection of edges to be collapsed. This leads to a suboptimal
    simulation. I expect this might require a rewrite of the code that
    determines "sizing".

<!-- end list -->

  - Some custom-made `BMesh` operations do not work as expected. It
    leads to unwanted mesh topology that can sometimes make the
    simulation unstable. Introducing a new mesh structure can solve this
    issue and speed up the mesh edit operations. Currently, for the new
    mesh structure, I am opting for Adjacency and Incidence Framework
    (AIF) \[7\] with a few modifications. This might also help with
    future caching support.

Since GSoC'19, both Blender's dependency graph and its playback system
have undergone non-trivial changes that prevent playback of the GSoC'19
implementation of the Adaptive Cloth Simulator. Thus, relevant
refactoring will be necessary after understanding these changes.

As a related side note, I could extract the remeshing feature as a
separate modifier (instead of being integrated into the cloth
simulator). This would allow other modifiers/simulators, such as the
soft body simulator, to also take advantage of the remeshing feature. To
be discussed with the mentor.

**Stretch Goal:** Blender's current caching system for the cloth
simulator does not support changes in the mesh topology. This is because
the system only stores information per vertex. Thus, significant changes
to the existing caching system would be necessary. I currently have
multiple ideas to approach this– (i) store the complete mesh structure,
(ii) store the information per vertex and the changes made to the mesh
structure, (iii) run the remeshing step after reading each frame of the
vertex-based cache, etc.

# Project Schedule

|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |

**Schedule Conflicts:** None that would impact the above schedule. In
the highly unlikely event that a conflict appears, I will coordinate
with my mentor.

# Bio

I, Ish Bosamiya, am currently doing my B.Tech. in Computer Science and
Engineering at PES University, Bangalore, India. As part of GSoC 2019, I
worked on Adaptive Remeshing for Cloth Simulation. Since then,
independently, I have worked on building a fast and modular cloth
simulator from scratch, based upon ideas from multiple research papers
(in C++ \[4\]). I am currently in the process of refactoring and
rebuilding it in Rust \[5\]. In parallel, I have also continued spending
time learning about geometry processing. I have made contributions (in
my spare time, between academics and other side-projects) to Blender
outside of GSoC 2019 \[6\], and plan to continue doing so after GSoC
2021 too.

# References

1.  ["Adaptive Anisotropic Remeshing for Cloth Simulation" by Rahul
    Narain, Armin Samii and James F. O'Brien
    (SIGGRAPH 2012)](http://graphics.berkeley.edu/papers/Narain-AAR-2012-11/Narain-AAR-2012-11.pdf)
2.  [GSoC 2019
    Proposal](https://wiki.blender.org/wiki/User:Ishbosamiya/GSoC2019/Proposal)
3.  [GSoC 2019
    Report](https://wiki.blender.org/wiki/User:Ishbosamiya/GSoC2019/FinalReport)
4.  [Cloth Simulator in
    C++](https://github.com/ishbosamiya/cloth_simulator)
5.  [Cloth Simulator in
    Rust](https://github.com/ishbosamiya/cloth_simulator_rust)
6.  [Blender
    Contributions](https://developer.blender.org/p/ish_bosamiya/)
7.  ["Adjacency and incidence framework: a data structure for efficient
    and fast management of multiresolution meshes" by Frutuoso G. M.
    Silva and Abel J. P. Gomes (GRAPHITE
    '03)](https://doi.org/10.1145/604471.604503)
