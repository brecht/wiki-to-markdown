## Documentation for Adaptive Remeshing for Cloth Simulation

### Remeshing

<span class="literal">Physics</span> »
<span class="literal">Cloth</span> »
<span class="literal">Remeshing</span>

Remeshing allows the simulation system to analyze the cloth mesh and
determine the mesh's vertices, edges and faces that would be visually
equivalent to a high resolution mesh. This reduces the simulation time
while maintaining a similar visual appearance to a high resolution
simulation.

<table>
<tbody>
<tr class="odd">
<td><div class="note_title">
<p><em><strong>'Important Note</strong></em>'</p>
</div>
<div class="note_content">
<p>Remeshing requires the mesh to have UVs, the resolution of the mesh is determined by the UVs not the world space coordinates.</p>
</div></td>
</tr>
</tbody>
</table>

There are 2 different remeshing models to choose from:

  - **Dynamic Remeshing**

<!-- end list -->

  -   
    <span class="literal">Physics</span> »
    <span class="literal">Cloth</span> »
    <span class="literal">Remeshing</span> »
    <span class="literal">Dynamic</span>

![../../../images/Dynamic\_Remeshing\_Settings.png](../../../images/Dynamic_Remeshing_Settings.png
"../../../images/Dynamic_Remeshing_Settings.png")

  -   
    In this model, remeshing takes place every frame taking into account
    various different parameters. Note that the refine parameters are
    adjusted relative to each other.
      - **Aspect Minimum**
    <!-- end list -->
      -   
        This determines the minimum aspect ratio for the triangle with
        the smallest aspect ratio.
    <!-- end list -->
      - **Size Minimum**
    <!-- end list -->
      -   
        This determines the smallest triangle's size. Lower the value,
        smaller is the triangle, which means the simulation has the
        chance to be more detailed.
    <!-- end list -->
      - **Size Maximum**
    <!-- end list -->
      -   
        This determines the largest triangle's size. Lower the value,
        smaller the triangle, which means the starting mesh used in the
        simulation will be more detailed.
    <!-- end list -->
      - **Refine Angle**
    <!-- end list -->
      -   
        This parameter tries to estimate the size and orientation of the
        triangles necessary to have enough geometric detail when the
        mesh is curved. These estimations have a greater effect in
        determining the size and orientation of the triangles when this
        value is lower.
    <!-- end list -->
      - **Refine Compression**
    <!-- end list -->
      -   
        This parameter tries to predict the buckling of the mesh, so it
        allows for more realistic folds. These predictions have a
        greater effect in determining the size and orientation of the
        triangles when this value is lower.
    <!-- end list -->
      - **Refine Velocity**
    <!-- end list -->
      -   
        Geometric detail must be retained even when the vertex
        velocities change rapidly, thus this parameter tries to estimate
        the size and orientation of these triangles necessary to have
        enough geometric detail when the mesh's velocity is rapidly
        changing. These estimations have a greater effect in determining
        the size and orientation of the triangles when this value is
        lower.
    <!-- end list -->
      - **Refine Obstacle**
    <!-- end list -->
      -   
        This determines how the collision with other collision meshes
        plays a role in finding the size of a triangle in the mesh.
        Smaller the value, greater is the effect of this parameter while
        remeshing.

<!-- end list -->

  - **Static Remeshing**

<!-- end list -->

  -   
    <span class="literal">Physics</span> »
    <span class="literal">Cloth</span> »
    <span class="literal">Remeshing</span> »
    <span class="literal">Static</span>

![../../../images/Static\_Remeshing\_Settings.png](../../../images/Static_Remeshing_Settings.png
"../../../images/Static_Remeshing_Settings.png")

  -   
    In this model, remeshing takes place in the first frame and the size
    of the triangles is determined only by the below listed parameters.

<table>
<tbody>
<tr class="odd">
<td><div class="note_title">
<p><em><strong>'Note</strong></em>'</p>
</div>
<div class="note_content">
<p>Static remeshing will <strong>not</strong> change the mesh even if it comes near a collision mesh or if its velocity/compression/angle changes.</p>
</div></td>
</tr>
</tbody>
</table>

:\* **Aspect Minimum**

::This determines the minimum aspect ratio for the triangle with the
smallest aspect ratio.

:\* **Size Minimum**

  - 
    
      -   
        This determines the size of the triangles. Lower the value,
        smaller is the triangle, which means the simulation will be more
        detailed.
