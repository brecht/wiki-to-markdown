## Cloth Simulator Improvement

### Name:

Ish Hitesh Bosamiya

### Contact:

Email id: ishbosamiya@gmail.com

Blender id:
​[ish\_bosamiya](https://developer.blender.org/p/ish_bosamiya/)

Twitter: ​[@ishbosamiya](https://twitter.com/ishbosamiya/)

### Synopsis:

Blender has had a cloth simulator for quite a while now. It is based on
a system that now needs major changes.

A lot of research has been done in adaptive cloth simulation (which is
the next big step towards being able to do realistic cloth simulation in
reasonable computation time). By introducing adaptive cloth simulation
into Blender, we can decrease the computation time per frame, thereby
leading to a better quality of simulation in the same amount of time.
Furthermore, it would then be possible to build really powerful cloth
production pipelines. The current cloth production pipeline is slow and
requires a lot of user interaction (for example, for adding the correct
level of topology and redefining the stitches each time the topology
changes). With the introduction of adaptive cloth simulation, the
algorithm can automatically determine the necessary topology to get the
correct collisions and realistic folds and wrinkles. Additionally, this
project would act as the base for future improvements, such as adding
contact friction and dynamic tearing of cloth.

### Benefits:

The current cloth simulation system in Blender is highly dependent on
the artist creating the correct topology so that collision takes place
in a meaningful manner. This should not be the artist’s job- they should
not have to care about the number of polygons used, the spacing between
them, etc. that currently are necessary for the accurate simulation of
cloth. This project will allow artists to focus more on what’s important
to them- the art. They should be able to define the overall mesh and let
the computer handle the rest. Additionally, this project will change the
way clothing for characters can be designed. Currently Blender ​ does ​
have stitching functionality, but it is limited- new stitching edges
(loose edges are considered as stitching edges) need to be made each
time the topology is changed (to ensure correct collision). This is a
clunky and time consuming system that artists shouldn't need to bother
with. In short, adaptive cloth simulation will allow for a simplified
workflow for artists, where they just define the parts that need to be
stitched, everything else “Just Works<sup>TM</sup>”.

### Deliverables:

I will implement anisotropic adaptive remeshing that would be compatible
with the current cloth simulation code. I will also add some additional
settings for the remeshing part of the simulation to the user interface.

If time permits (and I hope it does), I will also implement a new object
type (namely, the cloth object), which will have its own interfacing
options to make stitching of cloth simpler.

### Project Details:

This project would add anisotropic adaptive remeshing to the cloth
simulation pipeline. This will not only help improve the performance on
large scale simulations, it will also make it easier for artists to use
the cloth simulator. The current cloth simulator as mentioned in ​[this
post​](https://blender.stackexchange.com/questions/8247/on-what-cloth-simulation-model-is-blender-cloth-physics-based)
is based on a very old pipeline. The recent addition of angular bending
springs did add a significant amount of realism, however, the cloth
simulator is lagging in quality in comparison to competitive products.

I will base most of my work on the foundations laid by the research
paper titled “Adaptive Anisotropic Remeshing for Cloth Simulation” by
Rahul Narain, Armin Samii and James F. O’Brien (published at SIGGRAPH,
found at this
[​link](http://graphics.berkeley.edu/papers/Narain-AAR-2012-11/Narain-AAR-2012-11.pdf)​).
Specifically, to implement this, it will involve adding additional steps
to the cloth simulation to allow for anisotropic adaptive remeshing. The
paper uses the sparse Cholesky-based solver in the TAUCS library;
however, this can be swapped for the existing Blender solver code, which
is what I’ll be doing.

An important point to note, in regards to this, the source code for the
implementation described in the paper is available
​[here​](http://graphics.berkeley.edu/resources/ARCSim/), but the
license is not compatible with that of Blender’s. While this means that
to add it into Blender, we will need to implement it from scratch (while
referring to and citing the paper), it also means that we have a
“ground-truth” implementation to compare against, in case we face any
unanticipated difficulties during implementation. This makes me more
confident about the timeline that I describe in the following section,
as well as in the overall success of this project.

Further improvements that I foresee (for after GSoC) are to the solver
(for example, to add an implicit frictional contact solver for the
adaptive cloth simulation implementation as per ​[this
paper](http://www-users.cselabs.umn.edu/~lixx4611/files/clothNodalContactSolver.pdf)
which would add the correct contact friction solver to further enhance
the realism).

### Project Schedule:

I’ve written a rough schedule below, based on my current understanding
of the research paper. I will continue refining it throughout the
project depending on discussions with my mentor.

<table>
<thead>
<tr class="header">
<th><p>Timeline</p></th>
<th><p>To Do</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Until 27th May (i.e., before GSoC begins, and during community bonding of GSoC)</p></td>
<td><ul>
<li>Familiarize with the current Blender cloth simulation source code</li>
<li>Understand the research paper thoroughly</li>
</ul></td>
</tr>
<tr class="even">
<td><p>27th May (GSoC Start) - 28th June</p></td>
<td><ul>
<li>First order approximation of implementation of anisotropic remeshing with simplified Blender cloth simulation code (remove “advanced” features and slowly add them back)</li>
<li>Test and refine this implementation, testing done by comparing the results against the examples part of the arcsim library</li>
</ul></td>
</tr>
<tr class="odd">
<td><p>29th June - 26th July</p></td>
<td><ul>
<li>Improve implementation, while integrating it completely with existing Blender code base</li>
<li>Ensure all existing features work as expected through integration test by plotting graph of error of distance separation between the existing high resolution cloth simulation vertices and the new integration vertices</li>
</ul></td>
</tr>
<tr class="even">
<td><p>27th July - 11th August</p></td>
<td><ul>
<li>Optimize code to speed up simulation time</li>
</ul></td>
</tr>
<tr class="odd">
<td><p>12th August - 26th August (i.e. until Official GSoC end)</p></td>
<td><ul>
<li>Reserve time period (in case anything unforeseen happens; if not, then I’ll be adding more features or making further quality-of-life improvements)</li>
<li>Cleaning up &amp; finalizing project documentation</li>
</ul></td>
</tr>
</tbody>
</table>

I will be updating the project documentation throughout the project, and
will be finalizing it during the reserve time period.

If time permits, I will also make some preliminary steps towards the
cloth production pipeline, which would involve making a new cloth object
type for faster structure generation, alongside a more user friendly
stitching method.

### Schedule conflicts:

I have college exams during community bonding period until May 16th.

College starts again on 12th August (tentative date). I expect to be
able to continue working on the project even once college starts,
however I expect my timings for working to change. I have factored this
into deciding the schedule above.

### Bio:

I, Ish Bosamiya, am currently doing my B.Tech. in Computer Science and
Engineering (2​<sup>nd</sup> Semester) at PES University, Bangalore,
India. I have delved into many different fields but design and
programming have been a constant interest of mine for many years now. I
first started to use Blender seriously 3\~4 years ago. At that time, I
made a few physics simulations, and then dived into modeling, texturing,
rigging and animation. Thereafter I moved on to shading. The math behind
the light calculations fascinated me, and this encouraged me to build my
own ​[software
rasterizer](https://github.com/ishbosamiya/3d_software_rasterizer)​ in
C++, a ​[ray tracer](https://github.com/ishbosamiya/ray_tracer_cpp)​ in
C++ and Python, and a simple ​[2D physics
engine​](https://github.com/ishbosamiya/2d_physics_engine) in C++ to
gain a better understanding of the computer graphics world and further
my programming skills. Recently, I have been spending my free time to
add new and cool features to Blender. A link to my profile can be found
​[here](https://developer.blender.org/p/ish_bosamiya/)​. My latest
addition has been to the Triangulate Modifier, to allow users to have
better control by adding a minimum number of vertices parameter to it.

Working on a physics engine from scratch has taught me a lot about the
complexities of simulations. The different patches that I have made for
Blender (some already merged into master,and a few more in progress\!)
have let me get accustomed with Blender’s code base, which means that I
will hit the ground running. Since I already have experience in
implementing simulations etc. from scratch, I have a good idea of the
specific complexities and issues that may come along the way. In
addition, I work well, both independently, as well as in a team (or with
a mentor). I am strongly motivated, and want to build the necessary
foundation to build a great cloth production tool in Blender in this
year’s GSoC; and make regular improvements to have a tool that is
extremely powerful and robust. All of these make me the perfect
candidate for this project.
