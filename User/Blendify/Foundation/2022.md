2022 Weekly Reports

## Week 43 (Oct 24 - Oct 28)

**Involved in 10 reports:**

  - Confirmed: 0
  - Closed as Resolved: 2
  - Closed as Archived: 1
  - Closed as Duplicate: 0
  - Needs Info from User: 1
  - Needs Info from Developers: 0
  - Actions total: 12

**Review: 2**

  - Aaron Carlisle (Blendify) edited reviewers for D16331: Fix T102027:
    Add set origin support for pointcloud objects, added: Germano
    Cavalcante (mano-wii); removed: Campbell Barton (campbellbarton).
    [D16331](https://developer.blender.org/D16331)
  - Aaron Carlisle (Blendify) requested changes to D16329: Sculpt Mode:
    Add missing auto-masking information.
    [D16329](https://developer.blender.org/D16329)

**Created patches: 0**

**Patches worked on: 2**

  - Accepted: UI: Rename "Vertex Colors" to "Color Attribute"
    [D14741](https://developer.blender.org/D14741)
  - Needs Review: Sculpt: Fix crash with no active brush data-block
    [D16330](https://developer.blender.org/D16330)

**Commits:**

  - Fix: Broken epub and html download link
    [rBMT8321](https://projects.blender.org/UNSUPPORTED/blender-manual-translations-svn/commit/8321)
  - Camera Data Node: View Vector is normalize
    [rBM9656](https://projects.blender.org/UNSUPPORTED/blender-manual-translations-svn/commit/9656)
  - Cleanup: Grammar fixe
    [rBM9655](https://projects.blender.org/UNSUPPORTED/blender-manual-translations-svn/commit/9655)
  - Shader Nodes: Improve note for light path node support in Eeve
    [rBM9654](https://projects.blender.org/UNSUPPORTED/blender-manual-translations-svn/commit/9654)
  - Cleanup: Remove broken lin
    [rBMT8311](https://projects.blender.org/UNSUPPORTED/blender-manual-translations-svn/commit/8311)

## Week 42 (Oct 17 - Oct 21)

**Involved in 4 reports:**

  - Confirmed: 0
  - Closed as Resolved: 1
  - Closed as Archived: 0
  - Closed as Duplicate: 0
  - Needs Info from User: 1
  - Needs Info from Developers: 0
  - Actions total: 4

**Review: 0**

**Created patches: 0**

**Patches worked on: 2**

  - Accepted: UI: Rename "Vertex Colors" to "Color Attribute"
    [D14741](https://developer.blender.org/D14741)
  - Needs Review: Sculpt: Fix crash with no active brush data-block
    [D16330](https://developer.blender.org/D16330)

**Commits:**

  - UI: Improve tooltip for texture shading mode
    [rBc7051192](https://projects.blender.org/blender/blender/commit/c7051192e217)
  - Small correction to previous commi
    [rBM9641](https://projects.blender.org/UNSUPPORTED/blender-manual-translations-svn/commit/9641)
  - Improve workbench documentatio
    [rBM9640](https://projects.blender.org/UNSUPPORTED/blender-manual-translations-svn/commit/9640)
  - Hair Curves: Update documentatio
    [rBM9639](https://projects.blender.org/UNSUPPORTED/blender-manual-translations-svn/commit/9639)
  - Animation: Add redo panel to Dopesheet and NL
    [rBM9638](https://projects.blender.org/UNSUPPORTED/blender-manual-translations-svn/commit/9638)
  - IO: Collada Import Ignores Vertex Normal
    [rBM9637](https://projects.blender.org/UNSUPPORTED/blender-manual-translations-svn/commit/9637)
  - Fix: Match page name with U
    [rBM9636](https://projects.blender.org/UNSUPPORTED/blender-manual-translations-svn/commit/9636)
  - Sculpt: Auto-masking UI improvement
    [rBM9635](https://projects.blender.org/UNSUPPORTED/blender-manual-translations-svn/commit/9635)
  - OBJ: add global scale factor import settin
    [rBM9634](https://projects.blender.org/UNSUPPORTED/blender-manual-translations-svn/commit/9634)
  - Cleanup: Sort index to match U
    [rBM9633](https://projects.blender.org/UNSUPPORTED/blender-manual-translations-svn/commit/9633)
  - Sculpt: Change operator for unhide face set
    [rBM9632](https://projects.blender.org/UNSUPPORTED/blender-manual-translations-svn/commit/9632)

## Week 41 (Oct 10 - Oct 14)

**Involved in 6 reports:**

  - Confirmed: 1
  - Closed as Resolved: 0
  - Closed as Archived: 0
  - Closed as Duplicate: 0
  - Needs Info from User: 0
  - Needs Info from Developers: 0
  - Actions total: 6

**Review: 2**

  - Aaron Carlisle (Blendify) closed D15621: Translate the chapter of
    "Attribute Statistic Node".
    [D15621](https://developer.blender.org/D15621)
  - Aaron Carlisle (Blendify) closed D15594: Improve "3D Viewport"
    section (Part 4 - display).
    [D15594](https://developer.blender.org/D15594)

**Created patches: 0**

**Patches worked on: 2**

  - Accepted: UI: Rename "Vertex Colors" to "Color Attribute"
    [D14741](https://developer.blender.org/D14741)
  - Needs Review: Sculpt: Fix crash with no active brush data-block
    [D16330](https://developer.blender.org/D16330)

**Commits:**

  - Py Docs: Update to sphinx 5.2.3
    [rB9cd99684](https://projects.blender.org/blender/blender/commit/9cd99684b02e)
  - Update Sphinx to 5.2.
    [rBM9629](https://projects.blender.org/UNSUPPORTED/blender-manual-translations-svn/commit/9629)
  - Cleanup: Pose Library operator
    [rBM9628](https://projects.blender.org/UNSUPPORTED/blender-manual-translations-svn/commit/9628)
  - Eevee: Add support for Nishita sky textur
    [rBM9627](https://projects.blender.org/UNSUPPORTED/blender-manual-translations-svn/commit/9627)
  - UI: show the windowing environment in the "About" splas
    [rBM9626](https://projects.blender.org/UNSUPPORTED/blender-manual-translations-svn/commit/9626)
  - UV: rename "Pixel Snap Mode" to "Pixel Round Mode
    [rBM9625](https://projects.blender.org/UNSUPPORTED/blender-manual-translations-svn/commit/9625)
  - GPencil: Improve Fill Tool Extend line
    [rBM9624](https://projects.blender.org/UNSUPPORTED/blender-manual-translations-svn/commit/9624)
  - Cleanup: Use enum style markup for menu item
    [rBM9623](https://projects.blender.org/UNSUPPORTED/blender-manual-translations-svn/commit/9623)
  - GP: Add RNA reference for Gap Closure Mode
    [rBM9622](https://projects.blender.org/UNSUPPORTED/blender-manual-translations-svn/commit/9622)
  - Dope Sheet: Add auto snapping documentatio
    [rBM9621](https://projects.blender.org/UNSUPPORTED/blender-manual-translations-svn/commit/9621)
  - Add notes to clarify the grease pencil modifiers are different then
    the general
    [rBM9620](https://projects.blender.org/UNSUPPORTED/blender-manual-translations-svn/commit/9620)
  - Fix: Animation channel mute is not a speake
    [rBM9619](https://projects.blender.org/UNSUPPORTED/blender-manual-translations-svn/commit/9619)
  - Cleanup: Remove Reference to not existent vide
    [rBM9618](https://projects.blender.org/UNSUPPORTED/blender-manual-translations-svn/commit/9618)
  - Add note about location of overlays in VS
    [rBM9617](https://projects.blender.org/UNSUPPORTED/blender-manual-translations-svn/commit/9617)
  - Cleanup: Remove negative not
    [rBM9616](https://projects.blender.org/UNSUPPORTED/blender-manual-translations-svn/commit/9616)
  - Cleanup: Compile warning
    [rBM9615](https://projects.blender.org/UNSUPPORTED/blender-manual-translations-svn/commit/9615)
  - Translate the chapter of "Attribute Statistic Node
    [rBMT8253](https://projects.blender.org/UNSUPPORTED/blender-manual-translations-svn/commit/8253)
  - Update r961
    [rBMT8251](https://projects.blender.org/UNSUPPORTED/blender-manual-translations-svn/commit/8251)
  - Mesh: Move edge crease out of MEdg
    [rBM9614](https://projects.blender.org/UNSUPPORTED/blender-manual-translations-svn/commit/9614)
  - Mesh: Move bevel weight out of MVert and MEdg
    [rBM9613](https://projects.blender.org/UNSUPPORTED/blender-manual-translations-svn/commit/9613)
  - Merge branch 'blender-3.3-release
    [rBM9612](https://projects.blender.org/UNSUPPORTED/blender-manual-translations-svn/commit/9612)
  - Merge branch 'blender-3.3-release
    [rBM9610](https://projects.blender.org/UNSUPPORTED/blender-manual-translations-svn/commit/9610)
  - Merge branch 'blender-3.3-release
    [rBM9605](https://projects.blender.org/UNSUPPORTED/blender-manual-translations-svn/commit/9605)

## Week 40 (Oct 03 - Oct 07)

**Involved in 2 reports:**

  - Confirmed: 0
  - Closed as Resolved: 0
  - Closed as Archived: 0
  - Closed as Duplicate: 0
  - Needs Info from User: 0
  - Needs Info from Developers: 0
  - Actions total: 2

**Review: 0**

**Created patches: 0**

**Patches worked on: 2**

  - Accepted: UI: Rename "Vertex Colors" to "Color Attribute"
    [D14741](https://developer.blender.org/D14741)
  - Needs Review: Sculpt: Fix crash with no active brush data-block
    [D16330](https://developer.blender.org/D16330)

**Commits:**

  - GPencil: Change property text for Fill Visual Aid
    [rBM9601](https://projects.blender.org/UNSUPPORTED/blender-manual-translations-svn/commit/9601)
  - Clarify save as render optio
    [rBM9600](https://projects.blender.org/UNSUPPORTED/blender-manual-translations-svn/commit/9600)
  - Fix: Description for shortcut Shift-Ctrl-O should read as "recently
    opened" not
    [rBM9599](https://projects.blender.org/UNSUPPORTED/blender-manual-translations-svn/commit/9599)
  - Modeling: Clarify "Best Fit" UV unwra
    [rBM9598](https://projects.blender.org/UNSUPPORTED/blender-manual-translations-svn/commit/9598)
  - Merge branch 'blender-3.3-release
    [rBM9597](https://projects.blender.org/UNSUPPORTED/blender-manual-translations-svn/commit/9597)

## Week 39 (Sep 26 - Sep 30)

**Involved in 5 reports:**

  - Confirmed: 0
  - Closed as Resolved: 0
  - Closed as Archived: 0
  - Closed as Duplicate: 0
  - Needs Info from User: 0
  - Needs Info from Developers: 0
  - Actions total: 5

**Review: 4**

  - Aaron Carlisle (Blendify) closed D14787: Rendering \> Shader Nodes
    \> Vector \> Vector Rotate Node - Missing/incorrect info.
    [D14787](https://developer.blender.org/D14787)
  - Aaron Carlisle (Blendify) closed D14723: Update: Documentation for
    add-on Magic UV. [D14723](https://developer.blender.org/D14723)
  - Aaron Carlisle (Blendify) closed D15565: Reformatting and Adding
    Extra Explanations.. [D15565](https://developer.blender.org/D15565)
  - Aaron Carlisle (Blendify) accepted D15998: Change the text referring
    to section Downloading blender so it match what reader will find
    there. [D15998](https://developer.blender.org/D15998)

**Created patches: 0**

**Patches worked on: 2**

  - Accepted: UI: Rename "Vertex Colors" to "Color Attribute"
    [D14741](https://developer.blender.org/D14741)
  - Needs Review: Sculpt: Fix crash with no active brush data-block
    [D16330](https://developer.blender.org/D16330)

**Commits:**

  - Nodes: Add missing vector rotate informatio
    [rBM9587](https://projects.blender.org/UNSUPPORTED/blender-manual-translations-svn/commit/9587)
  - Update: Documentation for add-on Magic U
    [rBM9586](https://projects.blender.org/UNSUPPORTED/blender-manual-translations-svn/commit/9586)
  - Getting Started: Reformatting and Adding Extra Explanation
    [rBM9585](https://projects.blender.org/UNSUPPORTED/blender-manual-translations-svn/commit/9585)
  - Add "Panel" and "Operator" to the glossar
    [rBM9584](https://projects.blender.org/UNSUPPORTED/blender-manual-translations-svn/commit/9584)
  - Bevel: Clarify when "Spread" is availabl
    [rBM9583](https://projects.blender.org/UNSUPPORTED/blender-manual-translations-svn/commit/9583)
  - Cleanup: Fix Screw operator exampl
    [rBM9582](https://projects.blender.org/UNSUPPORTED/blender-manual-translations-svn/commit/9582)
  - Fix outdated subdivied optio
    [rBM9581](https://projects.blender.org/UNSUPPORTED/blender-manual-translations-svn/commit/9581)
  - Update r958
    [rBMT8151](https://projects.blender.org/UNSUPPORTED/blender-manual-translations-svn/commit/8151)

## Week 38 (Sep 19 - Sep 23)

**Involved in 1 reports:**

  - Confirmed: 0
  - Closed as Resolved: 0
  - Closed as Archived: 0
  - Closed as Duplicate: 0
  - Needs Info from User: 0
  - Needs Info from Developers: 0
  - Actions total: 1

**Review: 2**

  - Aaron Carlisle (Blendify) closed D15670: Improve "3D Viewport"
    section (Part 5 - toolbar).
    [D15670](https://developer.blender.org/D15670)
  - Aaron Carlisle (Blendify) added a comment to D15998: Change the text
    referring to section Downloading blender so it match what reader
    will find there. [D15998](https://developer.blender.org/D15998)

**Created patches: 0**

**Patches worked on: 2**

  - Accepted: UI: Rename "Vertex Colors" to "Color Attribute"
    [D14741](https://developer.blender.org/D14741)
  - Needs Review: Sculpt: Fix crash with no active brush data-block
    [D16330](https://developer.blender.org/D16330)

**Commits:**

  - Improve "3D Viewport" section (Part 5 - toolbar
    [rBM9573](https://projects.blender.org/UNSUPPORTED/blender-manual-translations-svn/commit/9573)
  - Document sandboxed installation directories for windows stor
    [rBM9572](https://projects.blender.org/UNSUPPORTED/blender-manual-translations-svn/commit/9572)
  - Add note to clarify the grease pencle modifier is different then the
    general
    [rBM9571](https://projects.blender.org/UNSUPPORTED/blender-manual-translations-svn/commit/9571)

## Week 37 (Sep 12 - Sep 16)

**Involved in 5 reports:**

  - Confirmed: 0
  - Closed as Resolved: 1
  - Closed as Archived: 0
  - Closed as Duplicate: 1
  - Needs Info from User: 0
  - Needs Info from Developers: 0
  - Actions total: 6

**Review: 4**

  - Aaron Carlisle (Blendify) added a comment to D15829: Update links to
    contribute on Manual readme.
    [D15829](https://developer.blender.org/D15829)
  - Aaron Carlisle (Blendify) accepted D15981: Minor Fix about release
    period mismatch. [D15981](https://developer.blender.org/D15981)
  - Aaron Carlisle (Blendify) added inline comments to D15786:
    BLI\_array\_utils::gather().
    [D15786](https://developer.blender.org/D15786)
  - Aaron Carlisle (Blendify) closed D15579: Adding 3.0+ to the
    Version/Revision Milestones in the documentation.
    [D15579](https://developer.blender.org/D15579)

**Created patches: 0**

**Patches worked on: 0**

**Commits:**

  - Build: Update sphin
    [rBM9567](https://projects.blender.org/UNSUPPORTED/blender-manual-translations-svn/commit/9567)
  - Docs: Update sphinx
    [rB72253f42](https://projects.blender.org/blender/blender/commit/72253f427d4c)
  - Cycles: disable Scrambling Distance UI when using Sobol Burle
    [rBM9565](https://projects.blender.org/UNSUPPORTED/blender-manual-translations-svn/commit/9565)
  - Update RNA to User manual mappings
    [rBc8b6062d](https://projects.blender.org/blender/blender/commit/c8b6062d6c08)
  - Merge branch 'blender-3.3-release
    [rBM9564](https://projects.blender.org/UNSUPPORTED/blender-manual-translations-svn/commit/9564)
  - Fix: Improve layout / readability of multicam doc
    [rBM9562](https://projects.blender.org/UNSUPPORTED/blender-manual-translations-svn/commit/9562)
  - Fix: Copy Paste Mistake in the Fluid Physics doc
    [rBM9561](https://projects.blender.org/UNSUPPORTED/blender-manual-translations-svn/commit/9561)
  - Fix T100990: Typo in manua
    [rBM9560](https://projects.blender.org/UNSUPPORTED/blender-manual-translations-svn/commit/9560)
  - Obj: improve layout of UI fields and axis validatio
    [rBM9559](https://projects.blender.org/UNSUPPORTED/blender-manual-translations-svn/commit/9559)
  - UV: Add options for uv select similar in island mod
    [rBM9558](https://projects.blender.org/UNSUPPORTED/blender-manual-translations-svn/commit/9558)
  - UI: Avoid the word "Use" in checkbox label
    [rBM9557](https://projects.blender.org/UNSUPPORTED/blender-manual-translations-svn/commit/9557)
  - Update r955
    [rBMT8068](https://projects.blender.org/UNSUPPORTED/blender-manual-translations-svn/commit/8068)

## Week 36

  - Read commit logs for user affecting changes.
  - Update manual translations files
  - Update user manual with changes for the 3.3 manual, see T98243
    (completed)

**Involved in 2 reports:**

  - Confirmed: 0
  - Closed as Resolved: 0
  - Closed as Archived: 1
  - Closed as Duplicate: 0
  - Needs Info from User: 0
  - Needs Info from Developers: 0
  - Actions total: 2

**Review: 2**

  - Aaron Carlisle (Blendify) closed D15924: Correct error in
    similar.rst caused by missing line.
    [D15924](https://developer.blender.org/D15924)
  - Aaron Carlisle (Blendify) added a comment to D15910: Manual: check
    keymap 1 (refboxes). [D15910](https://developer.blender.org/D15910)

**Created patches: 0**

**Patches worked on: 0**

**Commits:**

  - Merge branch blender-3.3-releas
    [rBM9551](https://projects.blender.org/UNSUPPORTED/blender-manual-translations-svn/commit/9551)
  - Merge branch 'blender-3.3-release
    [rBM9547](https://projects.blender.org/UNSUPPORTED/blender-manual-translations-svn/commit/9547)
  - Merge branch 'blender-3.3-release
    [rBM9543](https://projects.blender.org/UNSUPPORTED/blender-manual-translations-svn/commit/9543)

## Week 35

  - Read commit logs for user affecting changes.
  - Update manual translations files
  - Update user manual with changes for the 3.3 manual, see T98243

## Week 33

  - Read commit logs for user affecting changes.
  - Update manual translations files
  - Update user manual with changes for the 3.3 manual, see T98243
  - Fix 2.93 manual builds on buildbot
  - Patch review: D15609

## Week 32

  - Read commit logs for user affecting changes.
  - Update manual translations files
  - Update user manual with changes for the 3.3 manual, see T98243
  - Patch review: D15666, D15668, D15567
  - Closed Tasks: T100307, T100364, T100371, T100381
  - Added new documentation member: Tobin

## Week 31

  - Worked on updating the docs.blender.org/manual theme to a new custom
    sphinx theme -- mostly worked on javascript pipeline
  - Read commit logs for user affecting changes.
  - Update user manual with changes for the 3.3 manual
  - Patch review and committed patches
  - Update manual translations files

## Week 30

  - Worked on updating the docs.blender.org/manual theme to a new custom
    sphinx theme -- mostly worked on javascript pipeline
  - Read commit logs for user affecting changes.
  - Update user manual with changes for the 3.3 manual
  - Patch review and committed patches
  - Update manual translations files

## Week 29

  - Worked on updating the docs.blender.org theme
  - Worked on updating the docs.blender.org/manual theme to a new custom
    sphinx theme
  - Fixed several issues in Blender's Web Assets causing issues for
    modern SASS compilers.
  - Read commit logs for user affecting changes.
  - Update user manual with changes for the 3.3 manual
  - Patch review and committed patches
  - Update manual translations files

## Week 19

  - Investigated a bug in Sphinx with WebP images for EPUB builds
  - Investigated a bug in older Sphinx versions with newer Jinja causing
    build issues on BuildBot
  - The usual reading commits for feature changing commits

Commits:

  - Update manual translations
  - Work towards documenting 3.2 release changes
  - Update Sphinx/Dependencies

## Week 18
