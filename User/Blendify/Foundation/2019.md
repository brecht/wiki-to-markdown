# Weekly Reports for Year 2019

## Week 2 - 12/23 to 12/29

This week was focused on cleaning up both code review and tasks. Some
work was done for Blender 2.81 and 2.82 documentation. One major task
for 2.81 is updating sculpting docs. Next week I want to work on getting
organized with a list of personal responsibilities and begin working on
finishing up 2.81 and start on 2.82 documentation.

Here is a brief summary of my tasks:

\- Patches Closed: 36

\- Tasks Closed: 49

\- Total Commits: 16

## Week 1 - 12/16 to 12/22

This week was mostly focused on getting up to speed with current Blender
development. I spent most of the week going through the commit logs
seeing what needs to be updated for 2.82. I spoke with Brendon Murphy on
the status of the new add-on docs and all is going well. I fixed a few
low hanging issues and addressed outstanding reviews. Next week I want
to work on getting organized with a list of personal responsibilities
and begin working on finishing up 2.81 and start on 2.82 documentation.

Here is a brief summary of my tasks:

\- Patches Closed: 10

\- Tasks Closed: 7

\- Total Commits: 12
