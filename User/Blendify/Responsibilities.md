# Documentation Project Administration Responsibilities

## Daily

  - Check for new commits that require documentation changes
  - Check on patches
  - Read manual commit logs

## Weekly

  - Update translation files
  - Run Tests on manual

## Per Release

  - Bump Manual Version
  - Move Files around on the server
  - Upload API Docs
