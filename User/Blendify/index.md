# Welcome

Welcome to the wiki page for Aaron Carlisle.

I currently manage the Documentation project see below for a page
describing my responsibilities.

[Documentation Project Administration
Responsibilities](Responsibilities.md)

[Development History](https://developer.blender.org/p/Blendify/)

## Weekly Reports

  - [2023](Foundation/2023.md)
  - [2022](Foundation/2022.md)
  - [2021](Foundation/2021.md)
  - [2020](Foundation/2020.md)
  - [2019](Foundation/2019.md)
