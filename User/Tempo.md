### GSOC'20

  - [Proposal](Tempo/GSoC2020/Proposal.md)
  - [Discussion](https://devtalk.blender.org/t/gsoc-2020-idea-to-improve-the-custom-menu-idea/12275/10)
  - [Weekly
    Report](https://devtalk.blender.org/t/gsoc-2020-custom-menus-weekly-reports/13721?u=tempo)
  - [Weekly
    Report](https://devtalk.blender.org/t/gsoc-2020-custom-menus-weekly-reports/13721?u=tempo)
  - [Final Report](Tempo/GSoC2020/Final_Report.md)
