# Weekly reports for year 2022

## Week 24

(Monday)

  - Surprise\! Monkeybutler for the week\!
  - Clarkson-mirror issue: fixed
  - Debugging T97663; Python changelog not updated; work with Bastien on
    deploying fix
  - Debugging issue with windows-builder. Running out or resources
  - Explorations in Phabricator export-logic using Conduit + local PHP

## Week 23

  - Blender 3.20 release
  - CI-CD ideas-exchange
  - Finalizing MacOs buildbot addition
  - Assembled 'Phi' hardware cluster
  - Msix release debug

## Week 22

  - Buildbot expansion with 2x MacM1 hardware
  - Buildbot debugging of SNAP-store issue
  - 'Xeon Phi' machine evaluation; hardware order
  - Studio-expansion: assessment of work/resources needed

## Week 015-016

  - Overhauled serverroom; discontinued boilerrrom
  - Gitea PoC setup; ci-cluster -\> Mobile cluster
  - initual oAuth integration testing
  - Created specialized blender-id oAuth module for Gitea
  - Further integration testing

## Week 014

## Week 013

  - Fix issues with macos-buildbot-worker provisioning
  - Documenting macos-buildbot-worker provisioning
  - Finished tlab-mpm-macos-monterey-arm64-01 in UATEST
  - Zpool work on Threadripper

## Week 012

  - Wifi debug issue (ipv6 enabled at the studio)
  - Cleanup of older hardware
  - Debug issue related to Blender.chat
  - First stages of provisioning Imacs
  - Fix issues regarding release-delivery (ACL)
  - 3.1.1 release (wednesday)
  - 3.1.2 release (friday)
  - Wifi access-point module replacement

## Week 011

  - Threadripper machine
      - Install of SSD, fixing ZFS setup
  - Unboxed/preparing 2 Mac Mini's for buildbot
  - python-doc-api naming issue: fixed
  - Wiki-server resource-issue debug; mitigated
  - Re-doing part of manual-build delivery toolchain to prevent too many
    changed files. (done)
  - small studio-support tasks (done)
  - oss-attribution-tool doc-changes (in progress)
  - Internet outage morning of 24th of March
  - Buildbot hanging workers (because of internet outage)
  - Buildbot outage (due to full disk: No logrotate on logs)

## Week 010

  - Finished up fixing buildbot code-sign issue; now in production
  - Finished up packaging oss-attribution-builder: now hosted on our own
    GIT
  - Sent out invitation for participation via Blender YT channel
  - Talked with wayland developer about possibility of doing
    wayland-support for blender

<!-- end list -->

  - added a user to buildbot
  - Upgrade of ProxMox babysitting (20:00 on Thursday)
  - Unboxing of several new Apple deliveries

## Week 009

Oss-attribution-tool fixed and added basic backup-scripts

  - Code-sign issue; fix identified
  - Koro: analyzing reason for slowdowns (database/cache)
  - A good number of firewall-debugging (pfsense related) Likely fixed
    Telegram now
  - Oss-attribution-builder work: got it to build and made initial fork
