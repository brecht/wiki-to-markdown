# Porting popular modifiers to Geometry Nodes

## Name

Fabian Schempp

## Contact

Email: fabianschempp@googlemail.com developer.blender.org:
fabian\_schempp Blender.chat: fabian\_schempp

## Synopsis

For a long time, the modifier system was the main way to procedurally
generate and modify 3D data in Blender. While modifiers can be stacked
to produce a large variety of results it has limited control over the
flow of data. The Geometry Nodes system, introduced in version 2.92 of
Blender allows the user to go way beyond these limitations by building
complex processing graphs with an advanced attribute system that allows
storing data into geometry to be available for later nodes in the graph
to operate on. At the time of writing, only a small set of basic
functionality is available as nodes. This project will port a selection
of functionality from the old modifier system to the new nodes system,
taking into account the atomic design, the attributes system, and the
variety of geometry types it operates on.

## Benefits

The currently available set of nodes mainly focuses on set decoration
via point instancing and basic math operations. This is already very
powerful and people use it to create an unexpected variety of results,
but it lacks much of the functionality that is available for the
modifier system. The potential to create stunning procedural 3D content
will grow exponentially with each new node this project adds to the
system. Additionally, since the existing modifiers are mostly limited to
be used on mesh data, users will benefit from the ability to use the
ported functionality with other geometry types like Volumes, Point
Clouds and Instances in a consistent way.

## Deliverables

Proper port of the functionality of the following modifiers to the new
Geometry Nodes system (In order of priority): Smooth Solidify Remesh
Decimate Bisect (optional)

Existing Modifiers will be split up into several atomic nodes where it
is appropriate. Nodes will support different available geometry types
(Mesh, Point Clouds, Volume, Instance) where possible. New nodes will be
tested and supplied with developer and user documentation.

## Project Details

1.  Analyse  
    Analyse existing modifiers and their functioning, by looking at the
    interface and codebase.
2.  Design
    1.  Dissect functionality from each modifier into smaller more
        atomic pieces where possible and appropriate.
    2.  Adapt the pieces of functionality to properly fit into the
        attribute system and work with additional geometry types.
    3.  Create a visual layout for each of the nodes UI that can be
        discussed and refined. NOTE: Some basic design is already
        supplied on wiki.blender.org and developer.blender.org. General
        overview: developer.blender.org/T86839  
        Smooth Node: developer.blender.org/T86903
3.  Implementation  
    After discussion and refinement, the nodes will be implemented and
    tested one after another. Each new node will be supplied and made
    available for review as a single patch on developer.blender.org
4.  Documentation  
    User documentation will be supplied for each new node. Preliminary
    Thoughts Since I already have a fully working environment setup for
    Blender I don’t have to take this into account in my Schedule. I
    already worked on implementing new nodes for Geometry Nodes and also
    worked in more depth with the UI system, so adding new nodes and
    building the user interface should be a straightforward process. For
    porting the old functionality of the modifier the main work should
    be to decide how to handle code duplication between modifiers and
    nodes and in fitting it to the new attribute system. Since Geometry
    Nodes are written in C++ transferring existing C code to C++ may
    also be desirable. Making the functionality work on other geometry
    types will require additional thoughts and exploration and therefore
    a greater portion of the time spend. Where existing modifiers used
    the concept of vertex groups to mask influence, attributes can be
    used in the Geometry Nodes system. developer.blender.org/T86907
    There is already an unfinished and abandoned patch for a Remesh node
    on developer.blender.org. Where possible I would use this as a base.

## Project Schedule

### Until June 7

Spend some time to understand how other geometry types like volumes and
instances are processed in existing nodes to get a better understanding
of how to apply existing functionality to these new types.

### Week 1

Study the code and the UI of Smooth, Solidify, Remesh and Decimate
modifier to get a detailed overview of the functionality, talk to the
community and developers to collect additional opinions and ideas, and
consolidate what I learn into design and mockups for the new nodes.

### Week 2 + 3

Implementation of the functionality derived from the Smooth modifier
followed by testing, refinement, and documentation.

### Week 4 + 5

Implementation of the functionality derived from the Solidify modifier
followed by testing, refinement and documentation.

### Week 6 + 7

Implementation of the functionality derived from the Remesh modifier
followed by testing, refinement, and documentation.

### Week 8 + 9

Implementation of the functionality derived from the Decimate modifier
followed by testing, refinement, and documentation.

### Week 10

Keep the last week as a time buffer and for additional cleanup.

## Bio

I’m Fabian Schempp. With the initial motivation to make video games, I
started my career in programming, design, and 3D at the age of 9 Years.
After I learned to use 3d studio max and Maya I had my first contact
with Blender version 2.43 in 2007 and instantly fell in love with it.
Since this day, I have used Blender for uncountable personal and
professional Projects. Over time my interest shifted from creating
content myself to creating tools that enable others to create content.
After I finished my bachelor's and master's degree in user experience
design at the University of Applied Sciences in Wiesbaden. I had a
one-year stipend at Fraunhofer-Institut for graphical data processing in
Darmstadt, where I worked on an AR remote interface. Followed by some
years as a freelance developer and UX designer where I had the chance to
work on some projects in the field of machine learning, VR, and
procedural 3d data generation for Siemens. I’m currently studying
Computer Science at the University of Hagen in Germany. I’m confident
with the most popular programming and scripting languages including C,
C++, Python and GLSL. I'm familiar with most aspects of Blender and its
history. Since the end of 2020, I contributed some patches to the
Geometry Nodes Project including the Multi-Input-Socket. I’m an active
member of Blender's Nodes & Physics Module.
