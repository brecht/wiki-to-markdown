# My Development Setup

I have several machines I switch between when developing for Blender.
Some machines I have at home and I have a machine at the Blender
Institute for the work I perform there. Basically all those machines
have a ubuntu 18.04 installed. I also have a dual boot to run Windows,
but I only use that to fix AMD/WIntel related issues in the 3d viewport.

## IDE

As main IDE I use Visual Studio Code with the next plugins

  - **C/C++** - Microsoft
  - **C/C++ Clang Command Adapter** - Yasuaki MITANI
  - **Clang-Format** - caver
  - **Code Spell Checker** - Street Side Software
  - **RST Preview** - homas Haakon Townsend
  - **Shader language support for VS Code** - slevesque

## GPU development

  - **renderdoc** Need to know what is going on inside a specific GPU
    task. This tool will give you insight in the commands, input data,
    output data and pipeline status. Useful to detect issues when
    developing OpenGL or doing driver support.

## Ninja build system

Bare instructions to build blender using the ninja build system. The
build system is supported by CMake, but when using blender there are
still some rough edges that needs to be dealt with. The next steps are
how I setup the ninja build system to compile Blender.

The nice time saver when using ninja is the condensed warning/error
output when compiling. If something is right it doesn't show it. When
developing Blender without this you sometimes need to scroll several
hundred lines of output to figure out what the actual error is.

``` shell
sudo apt-get install ninja-build
cd blender-git
rm -Rf build_linux
cd blender
cat BUILD_NOTES.txt
```

Add \`-G Ninja\` to the \`BUILD\_CMAKE\_ARGS\` and use \`make ninja\`
for example.

``` txt
make ninja -j8 BUILD_CMAKE_ARGS="-G Ninja -U *SNDFILE* -U *PYTHON* -U *BOOST* -U *Boost* -U *OPENCOLORIO* -U *OPENEXR* -U *OPENIMAGEIO* -U *LLVM* -U *CYCLES* -U *OPENSUBDIV* -U *OPENVDB* -U *COLLADA* -U *FFMPEG* -U *ALEMBIC* -D WITH_CODEC_SNDFILE=ON -D PYTHON_VERSION=3.7 -D WITH_OPENCOLORIO=ON -D OPENCOLORIO_ROOT_DIR=/opt/lib/ocio -D WITH_OPENIMAGEIO=ON -D OPENIMAGEIO_ROOT_DIR=/opt/lib/oiio -D WITH_CYCLES_OSL=ON -D WITH_LLVM=ON -D LLVM_VERSION=6.0 -D OSL_ROOT_DIR=/opt/lib/osl -D WITH_OPENSUBDIV=ON -D OPENSUBDIV_ROOT_DIR=/opt/lib/osd -D WITH_OPENVDB=ON -D WITH_OPENVDB_BLOSC=ON -D OPENVDB_ROOT_DIR=/opt/lib/openvdb -D BLOSC_ROOT_DIR=/opt/lib/blosc -D WITH_ALEMBIC=ON -D ALEMBIC_ROOT_DIR=/opt/lib/alembic -D WITH_CODEC_FFMPEG=ON -D FFMPEG_LIBRARIES='avformat;avcodec;avutil;avdevice;swscale;swresample;lzma;rt;theora;theoradec;theoraenc;vorbis;vorbisenc;vorbisfile;ogg;x264;openjp2'"
```

Both changes are needed otherwise one for initializing the ninja build
system and the other to compile using \`ninja\`.

I would propose that this can be streamlined a bit. Go into your build
folder and start the ninja build.

Debug symbols

-----

Follow instructions at
<https://wiki.ubuntu.com/Debug%20Symbol%20Packages>

    apt install libgl1-mesa-dri-dbgsym
