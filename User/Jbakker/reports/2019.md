\_\_TOC\_\_

Weekly reports of Jeroen Bakker (atmind) latest report will be on the
top.

## Week 201950: 2019/12/09 - 2019/12/15

This week I will spend a most of my time in the bug tracker code reviews
and work out some proposals for the viewport and color management.

### Bug Fixes

  - [https://developer.blender.org/T72161
    T72161](https://developer.blender.org/T72161_T72161):
    Material Attribute Node in Linear Space
  - [https://developer.blender.org/T72353
    T72353](https://developer.blender.org/T72353_T72353):
    Viewport: Camera Limits visible when overlays are turned off
  - [https://developer.blender.org/T72124
    T72124](https://developer.blender.org/T72124_T72124):
    Lookdev Spheres render black
  - [https://developer.blender.org/T70512
    T70512](https://developer.blender.org/T70512_T70512):
    Viewport clipping ALT+B in lookdev, rendered displays some data
    clipped, some not...
  - [https://developer.blender.org/D6391
    D6391](https://developer.blender.org/D6391_D6391):
    Cycles/OpenCL: Remove NULL PTR Workaround
  - [https://developer.blender.org/T72220
    T72220](https://developer.blender.org/T72220_T72220):
    Edit mode orange overlay visible when 'Show overlays' is OFF

### Designs

  - [https://developer.blender.org/T72424
    T72424](https://developer.blender.org/T72424_T72424):
    Design: Painting Color Management
  - [https://developer.blender.org/T72420
    T72420](https://developer.blender.org/T72420_T72420):
    Design: DrawEngine Color Management
  - [https://developer.blender.org/T72007
    T72007](https://developer.blender.org/T72007_T72007):
    Design: EEVEE Compositing (click for nice video)

Next week I am planning to do more bug fixing and code reviews

## Week 201949: 2019/12/02 - 2019/12/08

The unification of the overlay engine has been committed to master.
There are regressions that we need to fix, UI we need to improve.

## Week 201948: 2019/11/25 - 2019/12/01

This week Clement is visiting Blender Institute so I would like to go
over the outstanding patches. The new implementation of the overlay
engine has been send for review as this is a large patch this would take
me a lot of time to review.

We checked the long outstanding patches and reviewed the ones that could
land before the overlay unification project
([D6296](https://developer.blender.org/D6296)). These included patches
that improves drawing performance
([T70167](https://developer.blender.org/T70167)), and viewport rendering
performance ([T71364](https://developer.blender.org/T71364)), and
removal of EEVEE render artifacts when using many samples (\>256)
([D6245](https://developer.blender.org/D6245)).

Showing the EEVEE renderpasses in the viewport also landed. It requires
some changes to RenderResult but the main use cases are working.
([T69476](https://developer.blender.org/T69476))

## Week 201947: 2019/11/18 - 2019/11/24

If everything goes to plan we should be able to detect what the exact
issue with NAVI + Linux + Blender is and if it is possible to create a
work around for the issue. Rest of the week I would spend on code review
and color management proposal

  - [https://developer.blender.org/T70243
    T70243](https://developer.blender.org/T70243_T70243): AMD
    Linux NAVI
  - [https://developer.blender.org/T71357
    T71357](https://developer.blender.org/T71357_T71357):
    DrawManager Color Management Design
  - [https://developer.blender.org/T71178
    T71178](https://developer.blender.org/T71178_T71178):
    Image UV Editor: UI Changes
  - [https://developer.blender.org/T67530
    T67530](https://developer.blender.org/T67530_T67530): Add
    Drawing Of Image/UV Editor to the Draw Manager
  - [https://developer.blender.org/D6278
    D6278](https://developer.blender.org/D6278_D6278): UV
    Editing: Scale To Bounds (GSoC 2016 UV Editing tools)

## Week 201946: 2019/11/11 - 2019/11/17

  - [https://developer.blender.org/T71382
    T71382](https://developer.blender.org/T71382_T71382):
    EEVEE Renderpasses in 3D Viewport
  - [https://developer.blender.org/T71479
    T71479](https://developer.blender.org/T71479_T71479):
    Cycles OpenCL Performance
  - [https://developer.blender.org/D6245
    D6245](https://developer.blender.org/D6245_D6245): EEVEE
    renderlayers render artifacts

## Week 201945: 2019/11/04 - 2019/11/10

Spend time planning and discussing the projects I will be executing the
next few months.

  - Executed a GPU Cycles Render Benchmark for the release notes
  - [https://developer.blender.org/T71357
    T71357](https://developer.blender.org/T71357_T71357):
    Draw Manager Color Management Plan
  - \[[T71364](https://developer.blender.org/T71364),
    [D6195](https://developer.blender.org/D6195)\]: Viewport Rendering
    Performance: GPU Color Management
  - [https://developer.blender.org/T71382
    T71382](https://developer.blender.org/T71382_T71382):
    EEVEE Renderpasses in 3D Viewport
  - [https://developer.blender.org/D6206
    D6206](https://developer.blender.org/D6206_D6206): EEVEE
    Using GPU to calculate renderpasses

## Week 201944: 2019/10/28 - 2019/11/03

3 subjects on my desk, retopology drawing, checking UDIM branch for
workbench support, fixing bugs.

  - [https://developer.blender.org/T70735
    T70735](https://developer.blender.org/T70735_T70735)
    Vertex/Weight paint frame stroke with symmetry options enabled
  - [https://developer.blender.org/T70267
    T70267](https://developer.blender.org/T70267_T70267)
    Retopology overlay
  - [https://developer.blender.org/T67981
    T67981](https://developer.blender.org/T67981_T67981)
    Refactor Curve Extensions Horizontal vs Extrapolate according to new
    specifications.

## Week 201943: 2019/10/21 - 2019/10/27

BConf2019; Had meetings about cycles development, color management and
retopology. Started working on a prototype for migrating the image
editor to the draw manager. This prototype will indicate how much work
it is to migrate as-is so we can plan the rest of the editors.

## Week 201942: 2019/10/14 - 2019/10/20

  - [https://developer.blender.org/T70288
    T70288](https://developer.blender.org/T70288_T70288):
    Workbench remove background dithering.
  - [https://developer.blender.org/T57739
    T57739](https://developer.blender.org/T57739_T57739):
    Wireframe: Camera Background Images
  - [https://developer.blender.org/T69709
    T69709](https://developer.blender.org/T69709_T69709):
    Face Orientation: Support for transparent objects
  - [https://developer.blender.org/T70267
    T70267](https://developer.blender.org/T70267_T70267):
    Overlay: Retopology
  - [https://developer.blender.org/D6076
    D6076](https://developer.blender.org/D6076_D6076):
    Volumetrics: incorrect voxel size in debug overlay
  - [https://developer.blender.org/T70868
    T70868](https://developer.blender.org/T70868_T70868):
    Matching volumetrics debug overlay
  - [https://developer.blender.org/T70167
    T70167](https://developer.blender.org/T70167_T70167):
    Workbench NVIDIA: improve performance by reusing data.

## Week 201941: 2019/10/07 - 2019/10/13

Together with Sybren we gave training to Pipeline, Engine, AR/VR
Developers and TDs.

  - [https://developer.blender.org/T70512
    T70512](https://developer.blender.org/T70512_T70512):
    Disable Viewport clipping for material and rendered mode

## Week 201940: 2019/09/30 - 2019/10/06

The whole week I spend on implementing platform support level feature.
The idea is to better inform users when there platform is known to have
issues. This hope that this in the end leads to less bug reports and so
more time to spend on other tasks or on actual development.

This is quite a big feature as we had to be able to draw a message to
the user without using any fancy OpenGL. In Windows we use the
MessageBox/Dialog API but in Linux we uses low level X11 API so we don't
need to add other dependencies.

  - [https://developer.blender.org/T70416
    T70416](https://developer.blender.org/T70416_T70416):
    Platform Support Level

## Week 201939: 2019/09/23 - 2019/09/29

More bug-tracking. Mostly viewport related stuff and hard to
reproduceable stuff. Also code reviewing sculpting patches.

  - [https://developer.blender.org/T70187
    T70187](https://developer.blender.org/T70187_T70187): AMD
    MESA Driver Bug
  - [https://developer.blender.org/T70267
    T70267](https://developer.blender.org/T70267_T70267):
    Retopology Overlay
  - [https://developer.blender.org/T57739
    T57739](https://developer.blender.org/T57739_T57739):
    Show background images in Wireframe mode + Workbench

## Week 201938: 2019/09/16 - 2019/09/22

This week I will spend my time in reviewing patches, and solving
viewport and GPU driver related issues.

  - [https://developer.blender.org/T69972
    T69972](https://developer.blender.org/T69972_T69972):
    Solve driver issues when using AMD driver (Andeline 19.9.2)
  - [https://developer.blender.org/T68457
    T68457](https://developer.blender.org/T68457_T68457):
    Cycles: Fixed rendering using displacements wth simple scenes.
  - [https://developer.blender.org/T69995
    T69995](https://developer.blender.org/T69995_T69995):
    Object Solid Draw type coloring issues
  - [https://developer.blender.org/T65988
    T65988](https://developer.blender.org/T65988_T65988):
    Cycles: Camera Background Images
  - [https://developer.blender.org/D5860
    D5860](https://developer.blender.org/D5860_D5860):
    Cleanup: remove BLI\_bf.h

## Week 201937: 2019/09/09 - 2019/09/15

Last week for new features. The start of the week will be focused on
finalizing the Viewport Render passes option and helping out with some
patch review. Any time left will be spent on bug-fixing viewport related
issues.

  - [https://developer.blender.org/T69476
    T69476](https://developer.blender.org/T69476_T69476):
    Cycles: Show Render passes in the 3d viewport
  - [https://developer.blender.org/D5750
    D5750](https://developer.blender.org/D5750_D5750):
    Depsgraph: Add support for local view
  - \[[T61797](https://developer.blender.org/T61797),
    [T69784](https://developer.blender.org/T69784)\]: Cycles: Support
    for local view in 3d viewport
  - [https://developer.blender.org/T69780
    T69780](https://developer.blender.org/T69780_T69780):
    Local/Global View handles light differently between Blender 2.79 and
    Blender 2.80
  - [https://developer.blender.org/T69377
    T69377](https://developer.blender.org/T69377_T69377):
    Texture flickering when circle select in edit mode
  - [https://developer.blender.org/D5784
    D5784](https://developer.blender.org/D5784_D5784): Small
    memory reduction patch for cycles viewport rendering.

Code review \[[D5761](https://developer.blender.org/D5761),
[D5779](https://developer.blender.org/D5779)\]

## Week 201936: 2019/09/02 - 2019/09/08

This week was focused on the new setup of shading modes.

  - [https://developer.blender.org/D5612
    D5612](https://developer.blender.org/D5612_D5612):
    Shading Modes changes including HDRI selector for Cycles.
  - [https://developer.blender.org/T66950
    T66950](https://developer.blender.org/T66950_T66950):
    Bone selection in weightpaint mode
  - [https://developer.blender.org/T67229
    T67229](https://developer.blender.org/T67229_T67229):
    Cycles/EEVEE: Change Strength of Viewport HDRI
  - [https://developer.blender.org/T63755
    T63755](https://developer.blender.org/T63755_T63755):
    Added Support UV Stretching overlay when editing multiple objects
  - [https://developer.blender.org/T69476
    T69476](https://developer.blender.org/T69476_T69476):
    Cycles: Show Render passes in the 3d viewport

## Week 201935: 2019/08/26 - 2019/09/01

This week focused on smaller projects that fit within the 2.81 schedule.

  - [https://developer.blender.org/D5612
    D5612](https://developer.blender.org/D5612_D5612):
    Shading Modes changes including HDRI selector for Cycles.
  - [https://developer.blender.org/D5335
    D5335](https://developer.blender.org/D5335_D5335):
    Workbench: Specular Highlighting for MatCaps
  - [https://developer.blender.org/D5439
    D5439](https://developer.blender.org/D5439_D5439):
    Cycles: OpenCL Separate Compile Debug Flag
  - [https://developer.blender.org/T66950
    T66950](https://developer.blender.org/T66950_T66950):
    Bone selection in weightpaint mode
  - [https://developer.blender.org/T67229
    T67229](https://developer.blender.org/T67229_T67229):
    Cycles/EEVEE: Change Strength of Viewport HDRI
  - [https://developer.blender.org/rB93f1d76bf0eb5bcae43ffda4022c3d2018962994](https://developer.blender.org/rB93f1d76bf0eb5bcae43ffda4022c3d2018962994):
    Fixed Wrong UV Stretching calculation due to threading issues

Next week I will put my effort into UV stretching drawing and UV
stretching when having multiple objects in edit mode.

## Week 201934: 2019/08/19 - 2019/08/25

Took the week off for some R\&R.

## Week 201933: 2019/08/12 - 2019/08/18

Took the week off for some R\&R.

## Week 201932: 2019/08/05 - 2019/08/11

  - [https://developer.blender.org/T67638
    T67638](https://developer.blender.org/T67638_T67638):
    DrawManager stretched camera background images are squashed when
    rotated
  - [https://developer.blender.org/D5439
    D5439](https://developer.blender.org/D5439_D5439): Cycles
    Standalone: OpenCL Kernel Compilation
  - [https://developer.blender.org/D5437
    D5437](https://developer.blender.org/D5437_D5437): Camera
    Background Image Transparency (WIP)
  - [https://developer.blender.org/T68449
    T68449](https://developer.blender.org/T68449_T68449):
    Remove Unimplemented Display Edges option in UV Editor

## Week 201931: 2019/07/29 - 2019/08/04

This week I had some time off and visited SIGGRAPH2019.

## Week 201930: 2019/07/22 - 2019/07/28

**Bug Fixing**

  - [https://developer.blender.org/T67437
    T67437](https://developer.blender.org/T67437_T67437):
    Sculpting: Vertex Colors Not Visible During Sculpting
  - [https://developer.blender.org/T65924
    T65924](https://developer.blender.org/T65924_T65924):
    Cycles OpenCL: Windows + AMD Vega rendering stalls.
  - [https://developer.blender.org/D5335
    D5335](https://developer.blender.org/D5335_D5335):
    Workbench: Specular Highlighting for MatCaps
  - [https://developer.blender.org/T67587
    T67587](https://developer.blender.org/T67587_T67587):
    Render artifacts Weight/Vertex painting in wireframe mode (non
    x-ray)

## Week 201929: 2019/07/15 - 2019/07/21

**Blender 2.80 Release**

  - [https://developer.blender.org/T66811
    T66811](https://developer.blender.org/T66811_T66811):
    Render Glitch WeightPaint + EEVEE SSR
  - [https://developer.blender.org/T66489
    T66489](https://developer.blender.org/T66489_T66489): Fix
    Camera Images Interfere During Selection
  - [https://developer.blender.org/T65924
    T65924](https://developer.blender.org/T65924_T65924):
    Cycles OpenCL: Windows + AMD Vega rendering stalls.
  - [https://developer.blender.org/T66968
    T66968](https://developer.blender.org/T66968_T66968):
    Textual change texture paint opacity
  - [https://developer.blender.org/T67127
    T67127](https://developer.blender.org/T67127_T67127): Fix
    Crash Adding New Mask Layer
  - [https://developer.blender.org/T67105
    T67105](https://developer.blender.org/T67105_T67105): Fix
    Missing Updates Camera Background Images
  - [https://developer.blender.org/T67025
    T67025](https://developer.blender.org/T67025_T67025):
    Disable Viewport Clipping when Workbench isn't active
  - [https://developer.blender.org/D5301
    D5301](https://developer.blender.org/D5301_D5301): Tweak
    number of samples reported during cycles viewport rendering

**Look Development**

  - [https://developer.blender.org/T66746
    T66746](https://developer.blender.org/T66746_T66746): Use
    External Render Engines for Look Development
  - [https://developer.blender.org/T67128
    T67128](https://developer.blender.org/T67128_T67128): Use
    Cycles for Look Dev Mode
  - [https://developer.blender.org/T67220
    T67220](https://developer.blender.org/T67220_T67220):
    Change the intensity of the Look Dev HDRI

## Week 201928: 2019/07/08 - 2019/07/14

  - [https://developer.blender.org/T66100
    T66100](https://developer.blender.org/T66100_T66100):
    Banding Issues Workbench
  - [https://developer.blender.org/T66544
    T66544](https://developer.blender.org/T66544_T66544): Fix
    Linux/AMD Pro crashes during allocating textures
  - [https://developer.blender.org/T66746
    T66746](https://developer.blender.org/T66746_T66746): Use
    External Render Engines for Look Development (WIP)
  - [https://developer.blender.org/D5231
    D5231](https://developer.blender.org/D5231_D5231): Fix
    crash running GPU shader.format\_calc() with attributes like
    gl\_VertexID
  - [https://developer.blender.org/D5227
    D5227](https://developer.blender.org/D5227_D5227):
    BackgroundImages: use camera shift
  - [https://developer.blender.org/D5239
    D5239](https://developer.blender.org/D5239_D5239): Fix
    crash HD Graphics 4000 Series On Windows
  - [https://developer.blender.org/T66671
    T66671](https://developer.blender.org/T66671_T66671): Fix
    Memory Leak when making render previews

## Week 201927: 2019/07/01 - 2019/07/07

  - Blender 2.80 manual (NLA editor)
  - Defining projects + roadmap for Painting & Sculpting module
  - [https://developer.blender.org/D5171
    D5171](https://developer.blender.org/D5171_D5171): Grease
    Pencil: Fix Render Artifacts
  - [https://developer.blender.org/T66100
    T66100](https://developer.blender.org/T66100_T66100):
    Banding Issues Workbench
  - [https://developer.blender.org/T66405
    T66405](https://developer.blender.org/T66405_T66405):
    Removed \`glVertex\` from the Python documentation
  - [https://developer.blender.org/T66416
    T66416](https://developer.blender.org/T66416_T66416):
    Workbench Support MatCap + Texture
  - [https://developer.blender.org/T66421
    T66421](https://developer.blender.org/T66421_T66421):
    TexturePaint: Force Workbench Color mode to Texture

## Week 201926: 2019/06/24 - 2019/06/30

  - [https://developer.blender.org/T64796
    T64796](https://developer.blender.org/T64796_T64796):
    Texture Paint Smear Brush Default Radius
  - [https://developer.blender.org/D5137
    D5137](https://developer.blender.org/D5137_D5137): Grease
    Pencil: Remove unneeded view state in storage list
  - [https://developer.blender.org/D5136
    D5136](https://developer.blender.org/D5136_D5136):
    Workbench: Memory Leak during MultiView rendering
  - [https://developer.blender.org/T65893
    T65893](https://developer.blender.org/T65893_T65893): Fix
    edit mesh culling when deformed object is outside viewport
  - [https://developer.blender.org/T66175
    T66175](https://developer.blender.org/T66175_T66175):
    Paint mode face mask drawing
  - [https://developer.blender.org/T66209
    T66209](https://developer.blender.org/T66209_T66209):
    Compositor Fit background + UI Scale

## Week 201925: 2019/06/17 - 2019/06/23

  - [https://developer.blender.org/T62876
    T62876](https://developer.blender.org/T62876_T62876):
    Camera Background Images
  - [https://developer.blender.org/T65745
    T65745](https://developer.blender.org/T65745_T65745):
    Bone Selection X-Ray Drawing
  - [https://developer.blender.org/T65914
    T65914](https://developer.blender.org/T65914_T65914):
    Workbench Transparency Film
  - Blender 2.80 manual (Scene Layout / Object)

## Week 201924: 2019/06/10 - 2019/06/16

This week was mostly occupied with writing the Blender 2.80 Manual (3d
Viewport, Sculpting + Painting, Scene Layout/Object)

## Week 201923: 2019/06/03 - 2019/06/09

  - [https://developer.blender.org/D5021
    D5021](https://developer.blender.org/D5021_D5021):
    Workbench: Fix World Space Cavity Shader iterations during Rendering
  - [https://developer.blender.org/T65287
    T65287](https://developer.blender.org/T65287_T65287): GPU
    Create High Bitdepth MultiSample Offscreen buffers
  - [https://developer.blender.org/T61768
    T61768](https://developer.blender.org/T61768_T61768):
    EEVEE: Offscreen Rendering via API
  - [https://developer.blender.org/T65554
    T65554](https://developer.blender.org/T65554_T65554):
    Sequencer OpenGL Render : Annotations don't scale with resolution
    scale
  - [https://developer.blender.org/T65381
    T65381](https://developer.blender.org/T65381_T65381):
    Hair rendering in EEVEE and Workbench
  - [https://developer.blender.org/T64615
    T64615](https://developer.blender.org/T64615_T64615):
    Texture Paint Overlay Visual Glitch

## Week 201922: 2019/05/27 - 2019/06/02

  - [https://developer.blender.org/T64849
    T64849](https://developer.blender.org/T64849_T64849):
    DrawManager: Separate color management from image rendering
  - [https://developer.blender.org/T65206
    T65206](https://developer.blender.org/T65206_T65206):
    EEVEE: Fix Crash Baking with volumetrics
  - [https://developer.blender.org/T65191
    T65191](https://developer.blender.org/T65191_T65191):
    Grease Pencil: Cycles Rendering without overlays did not draw
    GPencil
  - [https://developer.blender.org/T65225
    T65225](https://developer.blender.org/T65225_T65225):
    Mesh Analysis: incorrect shaded with x-ray turned on
  - [https://developer.blender.org/T63862
    T63862](https://developer.blender.org/T63862_T63862):
    Quick Edit: rendered overlays + transparency
  - [https://developer.blender.org/T62282
    T62282](https://developer.blender.org/T62282_T62282):
    Sculpting: Multires + Smooth shading
  - [https://developer.blender.org/T64333
    T64333](https://developer.blender.org/T64333_T64333):
    Sculpting: modifiers not visible in workbench

## Week 201921: 2019/05/20 - 2019/05/26

  - [https://developer.blender.org/T64849
    T64849](https://developer.blender.org/T64849_T64849):
    Support MSAA during Viewport Rendering
  - [https://developer.blender.org/T64849
    T64849](https://developer.blender.org/T64849_T64849):
    Support TAA during Sequence strip preview
  - [https://developer.blender.org/T62070
    T62070](https://developer.blender.org/T62070_T62070):
    Sculptmode Modifiers in workbench + wireframe mode
  - [https://developer.blender.org/T64947
    T64947](https://developer.blender.org/T64947_T64947):
    FXAA Render artifacts
  - [https://developer.blender.org/T64988
    T64988](https://developer.blender.org/T64988_T64988):
    DrawEngines Depth of Field units
  - [https://developer.blender.org/T62767
    T62767](https://developer.blender.org/T62767_T62767):
    Compositor File Output Node (multiview)
  - [https://developer.blender.org/T63176
    T63176](https://developer.blender.org/T63176_T63176):
    Compositor stripes when using scale operation
  - [https://developer.blender.org/T65035
    T65035](https://developer.blender.org/T65035_T65035):
    Workbench Alpha in texture mode
  - [https://developer.blender.org/T63050
    T63050](https://developer.blender.org/T63050_T63050):
    Compositor File Output Node (data conversion)
  - [https://developer.blender.org/T64922
    T64922](https://developer.blender.org/T64922_T64922):
    EEVEE Volumetrics crash

## Week 201920: 2019/05/13 - 2019/05/19

  - [https://developer.blender.org/T62947
    T62947](https://developer.blender.org/T62947_T62947):
    Support Box Selection Windows + Intel HD620
  - [https://developer.blender.org/T64567
    T64567](https://developer.blender.org/T64567_T64567): UI:
    Sampling rename Viewport =\> Render Viewport
  - [https://developer.blender.org/T64505
    T64505](https://developer.blender.org/T64505_T64505):
    EEVEE: Incorrect Window Coordinates Viewport Rendering
  - [https://developer.blender.org/T64417
    T64417](https://developer.blender.org/T64417_T64417):
    Preferences/Workbench: Incorrect 3D Viewport AntiAliasing
  - [https://developer.blender.org/T64466
    T64466](https://developer.blender.org/T64466_T64466):
    EEVEE: Intel Compiler Bug
  - [https://developer.blender.org/T64646
    T64646](https://developer.blender.org/T64646_T64646):
    EEVEE: Viewport Rendering Anti-Aliasing
  - [https://developer.blender.org/T54649
    T54649](https://developer.blender.org/T54649_T54649):
    Cycles/EEVEE: Unify Film Transparency
  - [https://developer.blender.org/T54649
    T54649](https://developer.blender.org/T54649_T54649):
    Cycles/EEVEE: Unify Depth of Field

## Week 201919: 2019/05/06 - 2019/05/12

  - [https://developer.blender.org/T64083
    T64083](https://developer.blender.org/T64083_T64083):
    Draw border around themes icons
  - [https://developer.blender.org/T63099
    T63099](https://developer.blender.org/T63099_T63099):
    Grease Pencil Render Layers
  - [https://developer.blender.org/T55617
    T55617](https://developer.blender.org/T55617_T55617):
    Object Selection Multiple View Layers
  - [https://developer.blender.org/T64302
    T64302](https://developer.blender.org/T64302_T64302):
    Fix: Objects not visible when overlays where turned off in cycles
  - [https://developer.blender.org/T64132
    T64132](https://developer.blender.org/T64132_T64132):
    Workbench AA Method for viewport as user pref setting.

## Week 201918: 2019/04/29 - 2019/05/05

  - [https://developer.blender.org/T62517
    T62517](https://developer.blender.org/T62517_T62517):
    Performance Sequencer Scene Strips
  - [https://developer.blender.org/T60847
    T60847](https://developer.blender.org/T60847_T60847):
    Performance Viewport Rendering
  - [https://developer.blender.org/T63525
    T63525](https://developer.blender.org/T63525_T63525):
    Reuse Drawmanager Depth buffer in Cycles Viewport rendering
  - [https://developer.blender.org/T64083
    T64083](https://developer.blender.org/T64083_T64083):
    Draw border around themes icons
  - [https://developer.blender.org/D4778
    D4778](https://developer.blender.org/D4778_D4778):
    Manual: Render/Workbench

## Week 201917: 2019/04/22 - 2019/04/28

  - [https://developer.blender.org/T57000
    T57000](https://developer.blender.org/T57000_T57000):
    Vertex Color in Workbench
  - [https://developer.blender.org/T63680
    T63680](https://developer.blender.org/T63680_T63680):
    Mesh Analysis Overlay
  - [https://developer.blender.org/T63867
    T63867](https://developer.blender.org/T63867_T63867):
    Remove Dynamic Paint Preview Option
  - [https://developer.blender.org/T62070
    T62070](https://developer.blender.org/T62070_T62070):
    Workbench - Sculpting With Deformed Mesh

## Week 201916: 2019/04/15 - 2019/04/21

  - [https://developer.blender.org/T62449
    T62449](https://developer.blender.org/T62449_T62449):
    Hiding faces in Texture paint mode
  - [https://developer.blender.org/T57000
    T57000](https://developer.blender.org/T57000_T57000):
    Vertex Color in Workbench
  - [https://developer.blender.org/T63517
    T63517](https://developer.blender.org/T63517_T63517):
    Enable Axis Aligned Ortho Grid overlay
  - Mesh analysis overlay

## Week 201915: 2019/04/08 - 2019/04/14

  - [https://developer.blender.org/T63312
    T63312](https://developer.blender.org/T63312_T63312) Fix
    crash xray + border clipping
  - Participated in many meeting in the home stretch week

## Week 201914: 2019/04/01 - 2019/04/07

  - [https://developer.blender.org/T62876
    T62876](https://developer.blender.org/T62876_T62876) Show
    Camera Back Texture in wireframe
  - [https://developer.blender.org/T63077
    T63077](https://developer.blender.org/T63077_T63077)
    Support Mixing transparent + texture materials during deferred
    rendering.
  - [https://developer.blender.org/T63056
    T63056](https://developer.blender.org/T63056_T63056)
    EEVEE+Workbench: render particles in image rendering
  - [https://developer.blender.org/T63292
    T63292](https://developer.blender.org/T63292_T63292)
    Workbench Texture drawing when no material
  - [https://developer.blender.org/T63223
    T63223](https://developer.blender.org/T63223_T63223)
    EEVEE crash not able to allocate texture memory, when enough memory
    available

## Week 201913: 2019/03/25 - 2019/03/31

  - [https://developer.blender.org/T62874
    T62874](https://developer.blender.org/T62874_T62874) Fix
    Crash Transparency + Texture drawing workbench
  - [https://developer.blender.org/T62189
    T62189](https://developer.blender.org/T62189_T62189) Fix
    Occluded Wires when edit face drawing disabled
  - [https://developer.blender.org/T62965
    T62965](https://developer.blender.org/T62965_T62965) Fix
    Crash WeightPaint/VertexPaint
  - [https://developer.blender.org/T62479
    T62479](https://developer.blender.org/T62479_T62479) Show
    object outliner+workbench shadows for displist type of objects
  - [https://developer.blender.org/T62876
    T62876](https://developer.blender.org/T62876_T62876) Show
    Camera Back Texture in wireframe

## Week 201912: 2019/03/18 - 2019/03/24

  - [https://developer.blender.org/T60820
    T60820](https://developer.blender.org/T60820_T60820) Fix
    Workbench to render with odd number of AA samples
  - [https://developer.blender.org/T59046
    T59046](https://developer.blender.org/T59046_T59046) Fix
    LookDev QuadView
  - [https://developer.blender.org/T61023
    T61023](https://developer.blender.org/T61023_T61023) Fix
    world clipping with transparency
  - [https://developer.blender.org/T60211
    T60211](https://developer.blender.org/T60211_T60211) Fix
    memory leak converting Curve to Mesh
  - [https://developer.blender.org/T62248
    T62248](https://developer.blender.org/T62248_T62248) Fix
    Compositor Crop node by default not usable
  - [https://developer.blender.org/T62772
    T62772](https://developer.blender.org/T62772_T62772) Fix
    Alpha Texture Rendering in Texture painting
  - [https://developer.blender.org/T62785
    T62785](https://developer.blender.org/T62785_T62785)
    Improve Compositor Viewer Node Link
  - [https://developer.blender.org/T62810
    T62810](https://developer.blender.org/T62810_T62810) AA
    reversed in workbench
  - [https://developer.blender.org/T58727
    T58727](https://developer.blender.org/T58727_T58727)
    Implement drawing of stencil masks for texture painting.
  - [https://developer.blender.org/T62774
    T62774](https://developer.blender.org/T62774_T62774)
    Disable edit mode overlays when overlays are disabled.
  - [https://developer.blender.org/T62356
    T62356](https://developer.blender.org/T62356_T62356) Fix
    memory leak volumetrics rendering EEVEE.
  - [https://developer.blender.org/T61816
    T61816](https://developer.blender.org/T61816_T61816) Fix
    Array Modifer crashes when Merge, Adjust Edit Cage, Start/End Cap,
    and Edit Mode are enabled.
  - [https://developer.blender.org/T62717
    T62717](https://developer.blender.org/T62717_T62717)
    Support Switching Drawing Engines When Sculpting

## Week 201911: 2019/03/11 - 2019/03/17

  - [https://developer.blender.org/D4485
    D4485](https://developer.blender.org/D4485_D4485) Reduce
    the number of recompilation events
  - Fixed bug [https://developer.blender.org/T62145
    T62145](https://developer.blender.org/T62145_T62145)
  - Prototype precompiling kernels.

## Week 201910: 2019/03/04 - 2019/03/10

  - [https://developer.blender.org/T58953
    T58953](https://developer.blender.org/T58953_T58953) IES
    Light texture not working for Opencl GPU render
  - [https://developer.blender.org/D4485
    D4485](https://developer.blender.org/D4485_D4485) Reduce
    the number of recompilation events
  - [https://developer.blender.org/D4428
    D4428](https://developer.blender.org/D4428_D4428)
    Background Compilation of Kernels during BVH building
  - [https://developer.blender.org/D4481
    D4481](https://developer.blender.org/D4481_D4481) Removal
    of OpenCL Single program compilation
  - Research on SPIR to distribute some kernels with blender. (Note: not
    going to implement due to driver support)

## Week 201909: 2019/02/25 - 2019/03/03

  - \[[T61752](https://developer.blender.org/T61752),
    [D4428](https://developer.blender.org/D4428)\] Cycles OpenCL:
    Background Compilation of Kernels during BVH building. When in
    foreground render and the kernels are being build, the system will
    use an AO kernel. This way users can still interact with the scene.

## Week 201908: 2019/02/18 - 2019/02/24

  - [https://developer.blender.org/T61513
    T61513](https://developer.blender.org/T61513_T61513)
    Volumetric compilation times
  - [https://developer.blender.org/T61463
    T61463](https://developer.blender.org/T61463_T61463)
    Split bake kernels in own programs
  - [https://developer.blender.org/T61576
    T61576](https://developer.blender.org/T61576_T61576) Do
    not compile kernels that are not needed
  - [https://developer.blender.org/T61501
    T61501](https://developer.blender.org/T61501_T61501)
    Reduce kernel recompilations
  - [https://developer.blender.org/T61466
    T61466](https://developer.blender.org/T61466_T61466)
    Merge small serial kernels
  - [https://developer.blender.org/T61466
    T61700](https://developer.blender.org/T61466_T61700) Do
    not compile baking kernels twice.
  - [https://developer.blender.org/T61736
    T61736](https://developer.blender.org/T61736_T61736)
    Remove mega kernel

## Week 201907: 2019/02/11 - 2019/02/17

  - Researched what kernels with what compilation directives takes a
    long time to compile.
  - [https://developer.blender.org/T61461
    T61461](https://developer.blender.org/T61461_T61461)
    Researched what the distribution of a specific node compared to the
    total compilation time.
  - [https://developer.blender.org/T61459
    T61459](https://developer.blender.org/T61459_T61459)
    Created a list of tasks that we will do in the first iteration.
  - [https://developer.blender.org/T61513
    T61513](https://developer.blender.org/T61513_T61513)
    Reduced OpenCL volumetric compilation times.
  - [https://developer.blender.org/D2264
    D2264](https://developer.blender.org/D2264_D2264) Bundled
    kernels that compile quickly into a single program.
  - [https://developer.blender.org/D4349
    D4349](https://developer.blender.org/D4349_D4349)
    Reverting single kernel patch. multi kernel is faster since D2264
    was accepted.

## Week 201906: 2019/02/04 - 2019/02/10

  - Researched how
    [LuxRender](../projects/CyclesOpenCL2019/LuxRender.md),
    [ProRender](../projects/CyclesOpenCL2019/ProRender.md)
    structured their kernels.
  - Revamped support multithreaded compilation of kernels
    [D2264](https://developer.blender.org/D2264) (Original patch by
    Lukas Stockner). BMW compilation time reduced from 55 seconds to:
      - 40 seconds (\`cycles.debug\_opencl\_kernel\_single\_program =
        True\`)
      - 20 seconds (\`cycles.debug\_opencl\_kernel\_single\_program =
        False\`)
  - Research on compilation times of [ Cycles OpenCL
    kernels](../projects/CyclesOpenCL2019/Cycles.md)

## Week 201905: 2019/02/01 - 2019/02/03

  - Setting up work environment + [Cycles OpenCL
    2019](../projects/CyclesOpenCL2019/index.md) project
    documentation
