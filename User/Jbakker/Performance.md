Improve performance of Blender is a mind set. Many different areas work
together and all work together to user noticeable performance.

## Use a single loop for continuous memory

  - The branch prediction misses height times. This could be a single
    time.
  - Allow space in the branch prediction table

#### Don't

    for (y = 0; y < height; y++) {
      for (x = 0; x < width; x++) {
        copy_v4_v4(rect_float, color);
        rect_float += 4;
      }
    }

#### Do

    const int size = height * width;
    for (index = 0; index < size; index++) {
      copy_v4_v4(rect_float, color);
      rect_float += 4;
    }
