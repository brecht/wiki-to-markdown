# Blender LTS

I will be managing Blender LTS for 2.83 in the upcoming period. This
page describes the generic process and it will be updated when needed.
The goal of this page is that anyone would be able to take over when I
won't be able to handle it, eg conference or holiday.

## Process

Tasks will be performed once a week on Thursday morning between 08:00 to
12:00 AMS time.

  - Go over any commits of last week and decide what commits will be
    merged into blender 2.83.
      - Commits should fix critical issues (Crashes, file corruption)
      - To a small extent corrective issues are allowed. These issues
        should be approved by Dalai/Brecht
      - Commits should still match the implementation of Blender LTS to
        a certain extent. Ideally no development should be allowed as
        development would increase the risk to reduce the quality.
  - Go over the list of open fixes and indicate the commits that are
    still correct and what commits had a corrective change afterwards.
  - Add new commits and update commits that needed corrective changes
    and mark them. These commits won't be committed this week as we want
    fixes to be stable before merging to Blender LTS.
  - Cherrypick commits that were correct from master to Blender LTS.
    Mark them in the Phabricator task
    [http://developer.blender.org/T77348
    \#77348](http://developer.blender.org/T77348_#77348) that
    they have been committed.
  - execute test cases + test suits
  - increase file sub version (in \`BKE\_blender\_version.h\` increase
    \`BLENDER\_FILE\_SUBVERSION\`)

## Phabricator task

[\#77348](http://developer.blender.org/T77348) contains a table for
managing the commits. The ticket contains a table with the next columns.
Order: Latest changes are on top so we don't need to scroll.

  
**Report**: Reference to the original bug report(s) of the commit that
the issues solves.  
**Commits**: Reference to the commits in master or other release
branches (eg. \`blender-v2.90-release\`). This could become multiple
commits when the commit received corrective fixes before it got fixed in
Blender LTS. When a commit receives a corrective fix afterwards a new
row would be added to the table.  
**Status**: Status of this row.

  - **New**: This row has been added last week in master and is
    monitored to be added to Blender LTS the next week.
  - **Corrective Fix**: The commit received a fix in master last week.
    We would assume that the fix isn't stable yet. Action is to look
    next week if the patch didn't receive another fix. As long as a fix
    received a corrective fix we won't include it in Blender LTS.
  - **Reverted**: The commit was reverted in master and the commit was
    already applied to Blender LTS. The reason why it was reverted also
    applies to Blender LTS. These commits are directly executed. This
    state is there to monitor new fixes of the issue. When a new fix
    will be added to master, it would be added as a new row.
  - **Fix Applied**: Fix was applied, no more work to be done.
  - **Wait for Fix**: The issue has been discussed that it should be
    added to Blender LTS, when the fix becomes available. As the fix
    isn't available we should monitor it. Note that the eventual fix
    should still be checked on if the solution maps to Blender LTS code
    base.
  - **Invalid**: Fix could not be applied as the actual commit couldn't
    be merged clean to Blender LTS. Additional development is needed to
    apply fix what should not be allowed as this is Blender LTS.

In the end every commit in Blender LTS should also be in this table. We
should go over this on a regular basis so we know for certain that the
process is executed correctly.

``` 
                                            ┌−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−┐
                                            ╎                      Status column                        ╎
                                            ╎                                                           ╎
                                            ╎                                                           ╎
                                            ╎   ┌─────────────────────────────────────────┐             ╎
                                            ╎   │                                         │             ╎
                   ┌−−−−−−−−−−−−−−−−−−−−−−−−    │                                         │              −−−−−−−−−−−−−−−−−−┐
                   ╎                            │                                         ▼                                ╎
┌────────────┐     ╎ ┌──────────────┐         ┌────────────┐     ┌────────────────┐     ┌─────────────┐       ┌──────────┐ ╎
│ Bug report │ ──▶ ╎ │ Wait for Fix │ ──────▶ │    New     │ ──▶ │ Corrective fix │ ──▶ │ Fix applied │ ────▶ │ Reverted │ ╎
└────────────┘     ╎ └──────────────┘         └────────────┘     └────────────────┘     └─────────────┘       └──────────┘ ╎
                   ╎   │                                                                                                   ╎
                   ╎   │               −−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−┘
                   ╎   │              ╎         ▲
                   ╎   │              ╎         │
                   ╎   ▼              ╎         │
                   ╎ ┌──────────────┐ ╎       ┌────────────┐
                   ╎ │   Invalid    │ ╎       │ Git commit │
                   ╎ └──────────────┘ ╎       └────────────┘
                   ╎                  ╎
                   └−−−−−−−−−−−−−−−−−−┘
```

## Resources

<https://dot-to-ascii.ggerganov.com/>

    digraph {
        rankdir = "LR"
        
        BugReport[label="Bug report"]
        GitCommit[label="Git commit"]
    
       subgraph cluster_1 {
            label="Status column"
    
            New[label="New"]
            FixApplied[label="Fix applied"]
            Reverted[label="Reverted"]
            CorrectiveFix[label="Corrective fix"]
            WaitForFix[label="Wait for Fix"]
            Invalid[label="Invalid"]
        }
    
        GitCommit -> New -> FixApplied -> Reverted
        New -> CorrectiveFix -> FixApplied
        BugReport -> WaitForFix -> Invalid
        WaitForFix-> New
    }
