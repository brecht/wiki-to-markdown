# Blender LTS Release Script

This page is written in sequential order and was extracted from the
original release process and adapted for performance as we knew we had
to do this more often. When performing a release a release engineer can
do all the steps as pointing out in this page.

When performing a new release I copy the source of this page to a editor
and rename the previous version with the next version. (Bad excuse for
introducing batches and parameters :-) ) After the release the content
of the editor is copied back including modifications that have been
made.

## Last commit

  - Change BLENDER\_VERSION\_CYCLE \`BKE\_blender\_version.h\`
  - Check versioning\_280.c
  - compile, commit and push

## Start Builds

Start builds on the buildbot By pressing Force build on the next pages
state your e-mail and put "Blender 2.83.9 Release" as reason

    https://builder.blender.org/admin/#/builders/17
    https://builder.blender.org/admin/#/builders/18
    https://builder.blender.org/admin/#/builders/21

Now we need to wait for half an hour for the builds to complete. During
that time the source archives can be created and uploaded

### Build source archive

    cd /home/jeroen/blender-git/blender
    make source_archive
    scp /home/jeroen/blender-git/blender/blender-2.83.9.tar.xz jeroen@download.blender.org:/data/www/vhosts/download.blender.org/source/
    scp /home/jeroen/blender-git/blender/blender-2.83.9.tar.xz.md5sum jeroen@download.blender.org:/data/www/vhosts/download.blender.org/source/

### Create release notes

    cd /home/jeroen/blender-git/blender/release/lts/
    ./create_release_notes.py --task=T77348 --version=2.83.9 --format=txt
    ./create_release_notes.py --task=T77348 --version=2.83.9 --format=steam
    ./create_release_notes.py --task=T77348 --version=2.83.9 --format=html
    ./create_download_urls.py --version=2.83.9

The output of these commands are used on various platform by multiple
persons normally put the release notes on blender.chat in the
\`blender-release\` room. The html and download urls are directly copied
into www.blender.org cms.

## Upload builds to download.blender.org

Perform the next steps after all the builds have been successfully
build.

    cd /home/jeroen/Downloads
    mkdir blender-2.83.9
    cd blender-2.83.9
    wget https://builder.blender.org/download/blender-2.83.9-linux64.tar.xz
    wget https://builder.blender.org/download/blender-2.83.9-macOS.dmg
    wget https://builder.blender.org/download/blender-2.83.9-windows64.msi
    wget https://builder.blender.org/download/blender-2.83.9-windows64.zip
    
    md5sum blender-2.83.9-* > blender-2.83.9.md5
    sha256sum blender-2.83.9-* > blender-2.83.9.sha256
    
    scp /home/jeroen/Downloads/blender-2.83.9/blender-2.83.9-linux64.tar.xz jeroen@download.blender.org:/data/www/vhosts/download.blender.org/release/Blender2.83/
    scp /home/jeroen/Downloads/blender-2.83.9/blender-2.83.9-macOS.dmg jeroen@download.blender.org:/data/www/vhosts/download.blender.org/release/Blender2.83/
    scp /home/jeroen/Downloads/blender-2.83.9/blender-2.83.9-windows64.msi jeroen@download.blender.org:/data/www/vhosts/download.blender.org/release/Blender2.83/
    scp /home/jeroen/Downloads/blender-2.83.9/blender-2.83.9-windows64.zip jeroen@download.blender.org:/data/www/vhosts/download.blender.org/release/Blender2.83/
    scp /home/jeroen/Downloads/blender-2.83.9/blender-2.83.9.md5 jeroen@download.blender.org:/data/www/vhosts/download.blender.org/release/Blender2.83/
    scp /home/jeroen/Downloads/blender-2.83.9/blender-2.83.9.sha256 jeroen@download.blender.org:/data/www/vhosts/download.blender.org/release/Blender2.83/

## Update Stores

### Windows Store

Make sure it you start a developer command prompt. otherwise the process
will fail.

    cd \blender-git\blender\release\windows\msix
    set VERSION=2.83.9.0
    set URL=https://builder.blender.org/download/blender-2.83.9-windows64.zip
    set PUBID=CN=...
    python create_msix_package.py --version %VERSION% --url %URL% --publisher %PUBID% --lts

copy file to linux for uploading

    scp /home/jeroen/Downloads/blender-2.83.9/blender-2.83.9.0-windows64.msix jeroen@download.blender.org:/data/www/vhosts/download.blender.org/release/Blender2.83/

<https://partner.microsoft.com/en-us/dashboard/products/9PP3C07GTVRH>

### Steam

Normally this step is performed by Nathan Letwory. It requires a MacOS
but the process as described in <https://developer.blender.org/D8429>

### Snap

Follow snap/README.txt in the source folder

    cd /home/jeroen/blender-git/blender/release/freedesktop/snap
    python3 bundle.py --version 2.83.9 --url https://download.blender.org/release/Blender2.83/blender-2.83.9-linux64.tar.xz
    
    # Test the build
    sudo snap remove blender
    sudo snap install --dangerous --classic blender_2.83.9_amd64.snap
    blender
    sudo snap remove blender
    
    # Push the snap package
    snapcraft push --release=2.8,2.83lts blender_2.83.9_amd64.snap

## Post Actions

### File access on download.blender.org

    ssh jeroen@download.blender.org
    sudo -i
    
    cd /data/www/vhosts/download.blender.org/source/
    chown root blender-2.83.9*
    chmod a-w blender-2.83.9*
    cd /data/www/vhosts/download.blender.org/release/Blender2.83/
    chown root blender-2.83.9*
    chmod a-w blender-2.83.9*

Ask troubled to change the flags of the release files + source files.
But normally it is done automatically.

### Git tagging

Tag blender/add-ons/addons\_contrib i18n and tools

    cd ~/blender-git/blender
    git tag -a v2.83.9 -m "Tagging Blender 2.83.9 Release"
    git push origin v2.83.9
    
    cd ~/blender-git/blender/release/scripts/addons
    git tag -a v2.83.9 -m "Tagging Blender 2.83.9 Release"
    git push origin v2.83.9
    
    cd ~/blender-git/blender/release/scripts/addons_contrib
    git tag -a v2.83.9 -m "Tagging Blender 2.83.9 Release"
    git push origin v2.83.9
    
    cd ~/blender-git/blender/release/datafiles/locale
    git tag -a v2.83.9 -m "Tagging Blender 2.83.9 Release"
    git push origin v2.83.9
    
    cd ~/blender-git/blender/source/tools
    git tag -a v2.83.9 -m "Tagging Blender 2.83.9 Release"
    git push origin v2.83.9
