## LuxRender

This page describes how LuxRender organizes their OpenCL kernels. Please
note that OpenCL LuxRender runs 4 times slower then CPU.

LuxRender is split into two parts LuxRays and LuxCore. LuxRays provides
Ray Intersection, LuxCore the path tracer.

### LuxCore

Path trace process (kernels)

1.  Clear film
2.  Initialize random seeds
3.  Initialize task buffer
4.  Send start film (optional)
5.  Render loop:
    1.  Receive film buffer (optional; for updating sample counts and
        displaying intermediate result)
    2.  TraceRay (See LuxRays)
    3.  Path Kernel; in sequence the next (micro) kernels are called. It
        is a finite state machine where every kernel does a transition
        check
        1.  MK\_RT\_NEXT\_VERTEX
        2.  MK\_HIT\_NOTHING
        3.  MK\_HIT\_OBJECT
        4.  MK\_RT\_DL
        5.  MK\_DL\_ILLUMINATE
        6.  MK\_DL\_SAMPLE\_BSDF
        7.  MK\_GENERATE\_NEXT\_VERTEX\_RAY
        8.  MK\_SPLAT\_SAMPLE
        9.  MK\_NEXT\_SAMPLE
        10. MK\_GENERATE\_CAMERA\_RAY
6.  Receive film buffer

The path kernel generates rays and evaluates those rays in an finite
state. When rays are completed it generates new rays.

![LuxRender ray state](../../../../images/201902_LuxRender_ray_state.jpg
"LuxRender ray state")

The OpenCL source code is dynamic and can trigger a recompilation when

  - Texture evaluation code changes
  - Material evaluation code changes.

### LuxRays

LuxRays has its own clContext. LuxCore can push the rays it wants to
calculate to LuxRays in a RayBuffer. LuxRays uploads the RayBuffer to
the device, enqueues the kernel ('Accelerator\_Intersect\_RayBuffer')
and read the result back to host ram.

The kernel supports two acceleration structures (MBVH and BVH). MBVH is
selected when instance support or motion blur is needed.

### Memory Management

For memory management LuxRender uses a similar construction as Cycles.
It creates pages and store the needed data in these pages. A big
difference is that the kernel parameters are generated (and therefore
the kernel) based on the number of pages that actually contains data.

See \`BVH\_NODES\_PAGE\_COUNT\`,
\`ACCELERATOR\_INTERSECT\_PARAM\_DECL\`,
\`ACCELERATOR\_INTERSECT\_PARAM\`.

They have two kind of pages.

1.  only containing vertices
2.  only containing BVH nodes
