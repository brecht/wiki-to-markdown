### Render loop

ProRender has an Adaptive renderer and a Monte carlo based renderer. We
focus on the Monte Carlo based renderer.

1.  for each tile
    1.  generate tile domain (\`GenerateTileDomain\`; updates a list of
        indices (every output pixels gets a number), this index is used
        to reduce the work-group/item lookup in other kernels)
    2.  generate prim rays; Based on the type of camera the primary rays
        are generated. Every type of camera has its own Kernel
        (\`PerspectiveCamera\_GeneratePaths\`,
        \`PerspectiveCameraDof\_GeneratePaths\`,
        \`OrthographicCamera\_GeneratePaths\`)
    3.  estimate
        1.  InitPathData \`InitPathData\`
        2.  for each pass
            1.  Clear ray hit buffer
            2.  Query intersection: provided by RadeonRays
            3.  Sample volume \`SampleVolume\`; Applies volume effects
                (absorbtion, emission) and scattering, also happens for
                missed rays.
            4.  Shade miss (environment) \`ShadeMiss\` Illuminate
                missing rays
            5.  Filter path stream \`FilterPathStream\`; convert
                intersections to predicates (Kill, Alive)
            6.  Gather Opacity (optional) \`GatherOpacity\`; Gather
                opacity information and store it in opacity buffer
            7.  Compact batch: provided by RadeonRays
            8.  Advance indices to keep pixel indices up to date
                \`RestorePixelIndices\`;
            9.  Shade miss primary ray (background)
                \`ShadeBackgroundEnvMap\`; Illuminate missing rays
            10. Shade volume \`ShadeVolumeUberV2\`; It applies direct
                illumination and generates (only scattered paths)
            11. Shade surface \`ShadeSurfaceUberV2\`; Handle ray-surface
                interaction possibly generating path continuation
            12. for each transmission step
                1.  QueryIntersection
                2.  Apply volume transmission
                    \`ApplyVolumeTransmissionUberV2\`; Handle light
                    samples and visibility info and add contribution to
                    final buffer
            13. Query occlusion (shadow rays): provided by RadeonRays
            14. Gather light samples \`GatherLightSamples\`; Handle
                light samples and visibility info and add contribution
                to final buffer
            15. Gather visibility (optional) \`GatherVisibility\`;
                Handle light samples and visibility info and add
                contribution to final buffer
        3.  Gather opacity \`GatherOpacity\`; Gather opacity information
            and store it in opacity buffer
    4.  Fill AOV's \`GenerateTileDomain\`, \`GeneratePrimaryRays\`,
        \`FillAOVsUberV2\`

`Note: Source code of shaders is generated from the materials. Data of the shaders are passed along. So perhaps only a recompilation if the material is restructured?`
