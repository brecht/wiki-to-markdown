## Impact

### Phase 1

  - AOV's are currently only available in Cycles and defined inside the
    Cycles Add-on. They need to migrate to Blender internal structure so
    we could share it
  - AOV Output should also be selectable when the render engine is eevee
  - Similar to a render pass per aov an accum buffer should be created.
  - A new GPUPass should be added for the material for rendering AOV's.
    This GPUPass can contain all AOV's or just a single AOV. A GPUPass
    per AOV might be more user friendly during shading. A single GPUPass
    for all AOV's performs better when rendering. We need to make this
    more clear what the consequences are.
  - Converting Cycles files to Blender internal AOV structure.
  - NiceToHave: selector for AOV passes

### Phase 2

  - Add AOV as a viewport renderpass for EEVEE.

### Phase 3

  - Add AOV as a viewport renderpass for Cycles.
