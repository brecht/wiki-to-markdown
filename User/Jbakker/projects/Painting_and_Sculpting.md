## Roadmap

Areas of improvements:

  - Look Development
      - Use scene render engine for look development (**L**): would also
        expect the Spheres to be rendered by the external engines.
      - Select render passes to output on screen (only when using the
        scene render engine) (**M/L**)
      - Add options to control the background and lighting better
        (**S/M**)
      - Add more render passes to EEVEE (**M/L**)
  - UV/Image Editor
      - A Realtime baking unwrapped. With option for available render
        passes. (**L**)
      - Orientate (rotate) the image in the image editor. (Experiment by
        Pablo Dobarro
        [1](https://twitter.com/pablodp606/status/1146063560499318784) )
        (**S**)
      - Add symmetry options for drawing straight (image/view) axis
        aligned strokes; (**S**)
      - Introduce a Overlay pop-over and move appropriate settings to
        this popover. (Display Texture Paint UVs, Stretching etc)
        (**S**)
  - Brushes
      - Add rake option to tiled texture mapping for a 'paint roller'
        effect instead of the current stencil stamping (**L**)
      - Add Lock Border option to all tools to optionally not affect
        borders while sculpting (for example smoothing a piece of cloth)
        (**M**)
      - Add a depth setting and decouple the setting from the current
        brush radius. (**M**)
  - Texture Painting
      - Redesign texture based masking it is not really useful (**?**)
      - 3D brush behavior with 2d falloff option, in stead of the
        current 2d projection painting (**?**)
      - Performance improvements to the Soften tool (**?**)
      - Layered based texture painting (**?**)
  - Texture Nodes (**?**)
  - Workflow
      - Syncronise (both ways) Texture/image node selection between the
        3d viewport, image editor and shader editor (**M**)
  - Defaults
      - Add Eraser brush (**S**)
