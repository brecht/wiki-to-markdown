# OpenColorIO v2

This page will record findings during upgrading OpenColorIO to version
v2.0. This will eventually lead to a design that could be validated.

## Setting up

### Using installdeps

  - \`git checkout tmp-ocio-v2\`.
  - \`./build\_files/build\_environment/install\_deps.sh --build-ocio\`
  - Change ccmake configuration to use the ocio that is created by the
    previous command
      - \`-DOPENCOLORIO\_INCLUDE\_DIR=/opt/lib/ocio-2.0/include\`
      - \`-DOPENCOLORIO\_OPENCOLORIO\_LIBRARY=/opt/lib/ocio-2.0/lib/libOpenColorIOapphelpers.a\`

# Changes

## Float to double

Math calculations have increased the precision from float to double. We
are changing the interface to comply to double precision.

  - Rename \`matrixTransformSetValue\` to
    \`matrixTransformSetMatrixAndOffset\`
  - groupTransformPushBack -\> groupTransformAppendTransform
