## Feature: Scene render engine

Currently the look dev mode uses only EEVEE as render engine. Users are
requesting to use the render engine of the scene for look dev. Basically
this means that any external render engine should be able to be used in
look dev mode.

### Functional Changes

This section describes the functional changes for when selecting Cycles.
As other external engines can use the same API they can also use it, but
they need to update their plugin. In the UX this change is really small.
There will be a *Scene Render Engine* option in the shading popover when
a viewport is in lookdev mode. This option by default is unset.

Example how this might look like. (This is an example that was copied
paste together, not actual code :-) )

![../../../images/Using\_Cycles\_for\_Look\_Development.jpg](../../../images/Using_Cycles_for_Look_Development.jpg
"../../../images/Using_Cycles_for_Look_Development.jpg")

### Approach

The project is not difficult, fits well with the current architecture.
Would tag this as a small to medium project.

**Phase 1**: Preparation existing code-base When implementing I would
start with the small fixes in Blender as separate patches. For example

  - the introduction of the utility functions to detect if an external
    engine is being used.
  - the consistency inside *view3d\_duplicate*
  - the poll function for RENDER\_OT\_opengl

**Phase 2**: Enable external engines to be used for look dev

During this phase we implement the changes into the codebase of Blender
(not yet cycles) to For testing we can add the *bl\_use\_lookdev*
setting to *cycles/blender/addon/\_\_init\_\_.py*. It won't show the
right result, but we can detect that everything is working as expected.

**Phase 3**: Enable Cycles to be used as render engine for look dev

Changes to Cycles to support rendering of the look dev mode. Cycles will
not be able to render the look dev spheres.

**Phase 4**: Update documentation

  - RenderEngine Python API
  - Manual
      - /editors/3dviewport/
      - /editors/sequencer/
  - Release notes

### Impact on the code-base

This change fits in the current architecture. There are areas that we
can improve (and fix) in the existing code base during this project.

  - Add an option to *RenderEngine* to detect if the Look Dev is
    supported (*bl\_use\_lookdev*). Together with
    *bl\_use\_eevee\_viewport* this will control the availability of the
    look dev mode.
      - *bl\_use\_lookdev*: This configuration setting is used to
        determine if the render engine can render the look dev mode by
        it self.
      - *bl\_use\_eevee\_viewport*: when this option is available in the
        shading option a button will appear for *Scene Render Engine*.
      - When any of *bl\_use\_lookdev* or *bl\_use\_eevee\_viewport* is
        set to *true* the Look Dev option will appear in any 3d
        viewport.
      - When both *bl\_use\_lookdev* or *bl\_use\_eevee\_viewport* are
        set to *true* the Shading Pop-over will display the *Scene
        Render Engine* setting. By default this setting is unset so
        EEVEE is being used.
  - Look Dev Overlay: If the external engine supports *bl\_use\_preview*
    we will trigger preview rendering to generate the look dev previews.
    They will then be composited on the 3d view by the draw manager.
    When the external engine does not support *bl\_use\_preview* the
    look dev overlay will be deactivated (greyed out).
      - We will need to add a RE\_USE\_LOOKDEV\_SPHERES for render
        engines to take over the rendering of the lookdev spheres. This
        is needed to support the current way how EEVEE renders the
        spheres.
  - Cycles
      - Blender Sync: detect if we are in Look Dev. When in Look Dev
        mode the world and lights are constructed based on the settings
        of the viewport. Also the background mixing will be done in the
        world material that is being created.
      - The HDRI's in LookDev are not loaded in memory theu
  - Viewport
      - *check\_rendered\_viewport\_visible*
      - *view3d\_duplicate*: When duplicating a view with an active
        external engine we should switch to workbench. We should replace
        the current implementation as it also switched for EEVEE, but
        only when in Rendered mode.
      - *ED\_view3d\_calc\_render\_border*: should also take into
        account the OB\_MATERIAL + external engine. or remove this at
        all when there are no occurances.
      - *ED\_view3d\_engine\_type*: add test to use external engine in
        look dev
      - Viewport Render Image/Animation poll method should check if the
        render engine is an internal (RE\_INTERNAL).
      - *ED\_view3d\_smooth\_view\_ex*: Skip when external engine is
        being used.
  - Sequencer
      - *seq\_render\_scene\_strip*: Add special case for OB\_MATERIAL
        for *bl\_use\_eevee\_viewport = False*
      - There are more edge cases here as the current implementation
        does not check the engine capabilities. For example an external
        render engine could not support Material mode, but still the
        sequencer strip preview can be set to material mode. We should
        check the capabilities of the scene engine and force the scene
        strip renderer to solid mode or rendered mode.
  - We should add a central utility functions if an external engine is
    currently used to draw a Viewport or will be used for a certain
    configuration. Currently this check is decentralized and implemented
    differently due to history (see Sequencer vs Viewport). There is no
    location that fits well (due to the dependency with BKE\_sequencer,
    editors/view3d, draw and render library) my proposed location would
    be in (/source/blender/draw/).
