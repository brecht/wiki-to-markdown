# Blender Normal Release Script

Keep track of <https://wiki.blender.org/wiki/Process/Release_Checklist>
as the governance is written there. This page just has the scripted bits
extracted from that page.

## Last commit

  - Check versioning\_290.c
      - "versioning\_userdef.c", \#blo\_do\_versions\_userdef
      - "versioning\_userdef.c", \#do\_versions\_theme
  - Change BLENDER\_VERSION\_CYCLE \`BKE\_blender\_version.h\`
  - compile, commit and push

## Start Builds

Start builds on the buildbot By pressing Force build for linux, windows
and MacOS. Enter your e-mail and put "Blender 2.91.0 Release" as reason

Now we need to wait for half an hour for the builds to complete. During
that time the source archives can be created and uploaded

### Build source archive

    cd /home/jeroen/blender-git/blender
    make source_archive
    scp /home/jeroen/blender-git/blender/blender-2.91.0.tar.xz jeroen@download.blender.org:/data/www/vhosts/download.blender.org/source/
    scp /home/jeroen/blender-git/blender/blender-2.91.0.tar.xz.md5sum jeroen@download.blender.org:/data/www/vhosts/download.blender.org/source/

## Upload builds to download.blender.org

Perform the next steps after all the builds have been successfully
build.

Make sure the destination folder already exists.

    ssh jeroen@download.blender.org
    sudo -i
    
    cd /data/www/vhosts/download.blender.org/release/
    mkdir Blender2.91
    chown root Blender2.91

    cd /home/jeroen/Downloads
    mkdir blender-2.91.0
    cd blender-2.91.0
    wget https://builder.blender.org/download/blender-2.91.0-linux64.tar.xz
    wget https://builder.blender.org/download/blender-2.91.0-macOS.dmg
    wget https://builder.blender.org/download/blender-2.91.0-windows64.msi
    wget https://builder.blender.org/download/blender-2.91.0-windows64.zip
    
    md5sum blender-2.91.0-* > blender-2.91.0.md5
    sha256sum blender-2.91.0-* > blender-2.91.0.sha256
    
    scp /home/jeroen/Downloads/blender-2.91.0/blender-2.91.0-linux64.tar.xz jeroen@download.blender.org:/data/www/vhosts/download.blender.org/release/Blender2.91/
    scp /home/jeroen/Downloads/blender-2.91.0/blender-2.91.0-macOS.dmg jeroen@download.blender.org:/data/www/vhosts/download.blender.org/release/Blender2.91/
    scp /home/jeroen/Downloads/blender-2.91.0/blender-2.91.0-windows64.msi jeroen@download.blender.org:/data/www/vhosts/download.blender.org/release/Blender2.91/
    scp /home/jeroen/Downloads/blender-2.91.0/blender-2.91.0-windows64.zip jeroen@download.blender.org:/data/www/vhosts/download.blender.org/release/Blender2.91/
    scp /home/jeroen/Downloads/blender-2.91.0/blender-2.91.0.md5 jeroen@download.blender.org:/data/www/vhosts/download.blender.org/release/Blender2.91/
    scp /home/jeroen/Downloads/blender-2.91.0/blender-2.91.0.sha256 jeroen@download.blender.org:/data/www/vhosts/download.blender.org/release/Blender2.91/

## Update Stores

### Windows Store

Make sure it you start a developer command prompt. otherwise the process
will fail.

    cd \blender-git\blender\release\windows\msix
    set VERSION=2.91.0.0
    set URL=https://builder.blender.org/download/blender-2.91.0-windows64.zip
    set PUBID=CN=...
    python create_msix_package.py --version %VERSION% --url %URL% --publisher %PUBID%

copy file to linux for uploading

    scp /home/jeroen/Downloads/blender-2.91.0/blender-2.91.0.0-windows64.msix jeroen@download.blender.org:/data/www/vhosts/download.blender.org/release/Blender2.91/

<https://partner.microsoft.com/en-us/dashboard/products/9PP3C07GTVRH>

### Steam

Normally this step is performed by Nathan Letwory. It requires a MacOS
but the process as described in <https://developer.blender.org/D8429>

### Snap

Follow snap/README.txt in the source folder

    cd /home/jeroen/blender-git/blender/release/freedesktop/snap
    python3 bundle.py --version 2.91.0 --url https://download.blender.org/release/Blender2.91/blender-2.91.0-linux64.tar.xz
    
    # Test the build
    sudo snap remove blender
    sudo snap install --dangerous --classic blender_2.91.0_amd64.snap
    blender
    sudo snap remove blender
    
    # Push the snap package
    snapcraft push --release=2.9,2.91,latest blender_2.91.0_amd64.snap

## Post Actions

### File access on download.blender.org

    ssh jeroen@download.blender.org
    sudo -i
    
    cd /data/www/vhosts/download.blender.org/source/
    chown root blender-2.91.0*
    chmod a-w blender-2.91.0*
    cd /data/www/vhosts/download.blender.org/release/Blender2.91/
    chown root blender-2.91.0*
    chmod a-w blender-2.91.0*

Ask troubled to change the flags of the release files + source files.
But normally it is done automatically.

### Git tagging

Tag blender/add-ons/addons\_contrib i18n and tools

    cd ~/blender-git/blender
    git tag -a v2.91.0 -m "Tagging Blender 2.91.0 Release"
    git push origin v2.91.0
    
    cd ~/blender-git/blender/release/scripts/addons
    git tag -a v2.91.0 -m "Tagging Blender 2.91.0 Release"
    git push origin v2.91.0
    
    cd ~/blender-git/blender/release/scripts/addons_contrib
    git tag -a v2.91.0 -m "Tagging Blender 2.91.0 Release"
    git push origin v2.91.0
    
    cd ~/blender-git/blender/release/datafiles/locale
    git tag -a v2.91.0 -m "Tagging Blender 2.91.0 Release"
    git push origin v2.91.0
    
    cd ~/blender-git/blender/source/tools
    git tag -a v2.91.0 -m "Tagging Blender 2.91.0 Release"
    git push origin v2.91.0

Remind Bastian to update the API docs.
