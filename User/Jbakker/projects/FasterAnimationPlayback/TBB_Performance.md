This page is a note book to measure various implementation direction

## task\_range: subdivision

Base line is Monkey add subdiv (7 steps)

``` 
diff --git a/source/blender/modifiers/intern/MOD_subsurf.c b/source/blender/modifiers/intern/MOD_subsurf.c
index b3bc5a66e8c..26ad937e147 100644
--- a/source/blender/modifiers/intern/MOD_subsurf.c
+++ b/source/blender/modifiers/intern/MOD_subsurf.c
@@ -43,6 +43,8 @@
 
 #include "MOD_modifiertypes.h"
 
+#include "PIL_time_utildefines.h"
+
 #include "intern/CCGSubSurf.h"
 
 typedef struct SubsurfRuntimeData {
@@ -209,6 +211,7 @@ static SubsurfRuntimeData *subsurf_ensure_runtime(SubsurfModifierData *smd)
 static Mesh *applyModifier(ModifierData *md, const ModifierEvalContext *ctx, Mesh *mesh)
 {
   Mesh *result = mesh;
+  TIMEIT_START(subsurf);
 #if !defined(WITH_OPENSUBDIV)
   modifier_setError(md, "Disabled, built without OpenSubdiv");
   return result;
@@ -238,6 +241,7 @@ static Mesh *applyModifier(ModifierData *md, const ModifierEvalContext *ctx, Mes
   if (subdiv != runtime_data->subdiv) {
     BKE_subdiv_free(subdiv);
   }
+  TIMEIT_END(subsurf);
   return result;
 }
 
```

### master

    time start (subsurf):  /home/jeroen/blender-git/blender/source/blender/modifiers/intern/MOD_subsurf.c:214
    time end   (subsurf): 2.805545  /home/jeroen/blender-git/blender/source/blender/modifiers/intern/MOD_subsurf.c:244
    time start (subsurf):  /home/jeroen/blender-git/blender/source/blender/modifiers/intern/MOD_subsurf.c:214
    time end   (subsurf): 2.944890  /home/jeroen/blender-git/blender/source/blender/modifiers/intern/MOD_subsurf.c:244
    time start (subsurf):  /home/jeroen/blender-git/blender/source/blender/modifiers/intern/MOD_subsurf.c:214
    time end   (subsurf): 3.074867  /home/jeroen/blender-git/blender/source/blender/modifiers/intern/MOD_subsurf.c:244
    time start (subsurf):  /home/jeroen/blender-git/blender/source/blender/modifiers/intern/MOD_subsurf.c:214
    time end   (subsurf): 3.009764  /home/jeroen/blender-git/blender/source/blender/modifiers/intern/MOD_subsurf.c:244
    time start (subsurf):  /home/jeroen/blender-git/blender/source/blender/modifiers/intern/MOD_subsurf.c:214
    time end   (subsurf): 3.025626  /home/jeroen/blender-git/blender/source/blender/modifiers/intern/MOD_subsurf.c:244

### Test 1

MEM allocation per task, grainlevel

    time start (subsurf):  /home/jeroen/blender-git/blender/source/blender/modifiers/intern/MOD_subsurf.c:214
    time end   (subsurf): 2.547075  /home/jeroen/blender-git/blender/source/blender/modifiers/intern/MOD_subsurf.c:244
    time start (subsurf):  /home/jeroen/blender-git/blender/source/blender/modifiers/intern/MOD_subsurf.c:214
    time end   (subsurf): 2.580972  /home/jeroen/blender-git/blender/source/blender/modifiers/intern/MOD_subsurf.c:244
    time start (subsurf):  /home/jeroen/blender-git/blender/source/blender/modifiers/intern/MOD_subsurf.c:214
    time end   (subsurf): 2.695532  /home/jeroen/blender-git/blender/source/blender/modifiers/intern/MOD_subsurf.c:244
    time start (subsurf):  /home/jeroen/blender-git/blender/source/blender/modifiers/intern/MOD_subsurf.c:214
    time end   (subsurf): 2.713737  /home/jeroen/blender-git/blender/source/blender/modifiers/intern/MOD_subsurf.c:244
    time start (subsurf):  /home/jeroen/blender-git/blender/source/blender/modifiers/intern/MOD_subsurf.c:214
    time end   (subsurf): 2.608030  /home/jeroen/blender-git/blender/source/blender/modifiers/intern/MOD_subsurf.c:244
    time start (subsurf):  /home/jeroen/blender-git/blender/source/blender/modifiers/intern/MOD_subsurf.c:214
    time end   (subsurf): 2.713873  /home/jeroen/blender-git/blender/source/blender/modifiers/intern/MOD_subsurf.c:244

### Test 2

MEM Allocation per task, auto grain

    ime start (subsurf):  /home/jeroen/blender-git/blender/source/blender/modifiers/intern/MOD_subsurf.c:214
    time end   (subsurf): 2.542735  /home/jeroen/blender-git/blender/source/blender/modifiers/intern/MOD_subsurf.c:244
    time start (subsurf):  /home/jeroen/blender-git/blender/source/blender/modifiers/intern/MOD_subsurf.c:214
    time end   (subsurf): 2.616983  /home/jeroen/blender-git/blender/source/blender/modifiers/intern/MOD_subsurf.c:244
    time start (subsurf):  /home/jeroen/blender-git/blender/source/blender/modifiers/intern/MOD_subsurf.c:214
    time end   (subsurf): 2.821317  /home/jeroen/blender-git/blender/source/blender/modifiers/intern/MOD_subsurf.c:244
    time start (subsurf):  /home/jeroen/blender-git/blender/source/blender/modifiers/intern/MOD_subsurf.c:214
    time end   (subsurf): 2.806942  /home/jeroen/blender-git/blender/source/blender/modifiers/intern/MOD_subsurf.c:244
    time start (subsurf):  /home/jeroen/blender-git/blender/source/blender/modifiers/intern/MOD_subsurf.c:214
    time end   (subsurf): 2.731332  /home/jeroen/blender-git/blender/source/blender/modifiers/intern/MOD_subsurf.c:244

## OpenGL Viewport Render

Use ForYou.blend

without TBB 35.606 sec for 500 frames. with TBB 35.231 sec for 500
frames.
