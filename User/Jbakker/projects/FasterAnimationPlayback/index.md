\_\_TOC\_\_

# Faster Animation Playback

  - [Spring Scene
    Analysis](Spring_Scene_Analysis.md)
  - [TBB
    Performance](TBB_Performance.md)

## Software

For regular performance testing we use \`perf\` and hotspot
(https://github.com/KDAB/hotspot).

    sudo apt install linux-tools-common linux-tools-generic

From hotspot I downloaded the AppImage from
<https://github.com/KDAB/hotspot/releases/tag/v1.2.0> .

## Test scenes

I used production anim scenes from cloud.blender.org For now I selected
the following to warm up the project. Eventually other scenes will be
selected as actual benchmark scenes.

  - Daily Dweebs: A production with a lot of subsurf modifiers and
    effects that require them
    (https://cloud.blender.org/p/dailydweebs/59e7c62ef4885514ede4b8f9)
  - Spring 01\_025\_A: Relative small animation file.
    (https://cloud.blender.org/p/spring/5cb08a92808c0e75b5b2b478)

## Testing method

Compile blender using \`RelWithDebInfo\` This allows hotspot to show the
detailed information. Run a scene and playback in the viewport for at
least a minute. (set playback to nosync)

## Hardware

These tests will be executed on different hardware with different CPUs
and GPUs.

## Base lines
