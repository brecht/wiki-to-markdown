Blender is in the process to migrate to the Vulkan GL API. This is a
long running and ongoing project mostly done by Clement. The branch is
\`tmp-vulkan\` and requires to install the vulkan SDK.

The vulkan SDK can be downloaded and installed from
<https://www.lunarg.com/vulkan-sdk/>

On popos20.10 I used the same commands as for Ubuntu 20.04:

    wget -qO - https://packages.lunarg.com/lunarg-signing-key-pub.asc | sudo apt-key add -
    sudo wget -qO /etc/apt/sources.list.d/lunarg-vulkan-1.2.176-focal.list https://packages.lunarg.com/vulkan/1.2.176/lunarg-vulkan-1.2.176-focal.list
    sudo apt update
    sudo apt install vulkan-sdk

You need to compile blender with the \`WITH\_VULKAN\` and start blender
with --gpu-backend=vulkan.

Currently blender will crash during startup.

## Changes to shaders

The plan is to implement VK shaders and convert back to OpenGL shaders.
Keeping track of the shaders changes. So we might be able to create a
converter.

  - Renamed \`gl\_VertexID\` to \`gl\_VertexIndex\`.
    [https://projects.blender.org/blender/blender/commit/44c875f59aa
    44c875f59a](https://projects.blender.org/blender/blender/commit/44c875f59aa_44c875f59a)
