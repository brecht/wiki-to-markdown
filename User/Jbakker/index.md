  - [Reports](Reports.md)
  - [User:Jbakker/Dev Setup](Dev_Setup.md)
  - [Projects](projects/index.md)
  - [User:Jbakker/Performance](Performance.md)

## Issues to research

When I am off I spend time as a blender user. This is a list of issues I
come across that I want to research more and fix it

  - UV editing tools. Straighten, Align rotate. Straighten connected etc

## Small Improvements

  - Don't draw full eevee when only the shadow pass or ambient occusion
    pass is requested. (there are other passes, but start with these as
    they only need the Depth Pass)

## Code CleanUp Day

  - Remove unused code in glutils
  - \`TIME\_REGION\` weirdness in naming and functionality.
  - \`UI\_view2d\_scrollers\_calc\` does an allocation what could be
    removed.
  - \`SI\_DRAW\_TILE\`
  - \`SI\_SHOW\_GPENCIL\`
