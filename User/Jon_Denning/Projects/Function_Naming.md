## Intro to this Doc

I created this document as a simple guide to help me understand how
functions are currently named and called, especially since some
functions rely heavily on many levels of indirect calling. Presently,
this document is only based on my experience and understanding and some
feedback, so it likely contains errors.

I have two hopes for this document:

  - after being reviewed, corrected, and improved, that this document
    could be made available to newcomers as part of their onboarding,
    and
  - this document could morph into a function naming design doc

Notes: This is far from exhaustive or comprehensive, as I have only
explored a small subset of the codebase. Also, this is (mostly) my
understanding of the code base, which could be incorrect.

Let me know if you find bugs or other ways to improve this document\!

## General

  - A \`static\` function is only accessible within that file (limited
    scope, not a "global" function, which is the default). While this
    qualifier is not part of the function's name, it is still useful to
    know for understanding the scope of functions.

<!-- end list -->

  - Naming should follow the \`snake\_case\` convention.

<!-- end list -->

  - Different areas of code may adopt different coding styles, so
    prefixes and suffixes may not be correct or consistent.

<!-- end list -->

  - Many of these conventions will (hopefully\!) change as Blender moves
    away from C and toward C++ (yay, namespaces\!).

### Other Links

Always refer to official documentation. Listed below are some great
starting places.

  - [Blender Code Documentation](../../../Source/index.md)
  - [C/C++ Style Guide](../../../Style_Guide/C_Cpp.md)

## Maths

The mathematics code has its own naming convention.

### Vectors

Vector functions have a prefix that indicates what operation is
performed, a possible infix and 1+ suffixes to indicate vectors. The
first vector passed (whether infix or suffix if no infix) is usually
considered the destination, and the second vector passed (if applicable)
is considered the source. If three or more vectors are given in total,
the first is the destination, and the other two are sources.

Due to the limitations of C (no function overloading), a final suffix
could be added to indicate that the vector types are \`double\`
(\`db\`), \`int\` (\`i\`), \`short\` (\`short\`), etc. If no type is
given, \`float\` (\`fl\`) is assumed.

Note: a singleton vector (a single \`float\`) is denoted with \`i\`,
\`fl\`, or \`db\`.

| Prefix              | Meaning                      | Analogy                       | Description                                                                             |
| ------------------- | ---------------------------- | ----------------------------- | --------------------------------------------------------------------------------------- |
| \`zero\_A\`         | zero                         | \`A=0\`                       | sets \`A\` to 0                                                                         |
| \`is\_zero\_A\`     | test if zero                 | \`A==0\`                      | returns true if \`A\` is exactly 0 (no epsilon)                                         |
| \`negate\_A\`       | negation                     | \`A=-A\`                      | negates values of \`A\`                                                                 |
| \`negate\_A\_B\`    | negated store                | \`A=-B\`                      | stores in \`A\` the negated values of \`B\`                                             |
| \`copy\_A\_B\`      | copy                         | \`A=B\`                       | copies values from destination (\`B\`) to source (\`A\`)                                |
| \`swap\_A\_B\`      | swap                         | \`T=A; A=B; B=T\`             | swaps values in \`A\` with \`B\`                                                        |
| \`add\_A\_B\`       | incremental add              | \`A+=B\`                      | adds values in \`B\` into \`A\`                                                         |
| \`add\_A\_BC\`      | add (overwrite)              | \`A=B+C\`                     | stores sum of values in \`B\` and \`C\` in \`A\`                                        |
| \`madd\_A\_BC\`     | Multiply and incremental ADD | \`A+=B\*C\`                   | adds product of \`B\` and \`C\` to \`A\`                                                |
| \`madd\_A\_BCD\`    | weighted addition            | \`A=B+C\*D\`                  | stores in \`A\` the weighted sum of \`B\` (scaled by 1) and product of \`C\` and \`D\`  |
| \`sub\_A\_B\`       | decrement subtract           | \`A-=C\`                      | subtracts values in \`B\` from \`A\`                                                    |
| \`sub\_A\_BC\`      | subtract (overwrite)         | \`A=B-C\`                     | stores the difference of values of \`B\` and \`C\` in \`A\`                             |
| \`mul\_A\_B\`       | scaling                      | \`A\*=B\`                     | scales values of \`A\` by \`B\`                                                         |
| \`mul\_A\_BC\`      | multiply (overwrite)         | \`A=B\*C\`                    | stores in \`A\` the product of \`B\` and \`C\`                                          |
| \`invert\_A\`       | invert                       | \`A=1/A\`                     | inverts values of \`A\`                                                                 |
| \`invert\_A\_safe\` | safe invert                  | \`A=(1/A) if A\!=0\`          | inverts values of \`A\`, but only if non-zero                                           |
| \`abs\_A\`          | ABSolute                     | \`A=abs(A)\`                  | stores in \`A\` the absolute value of \`A\`                                             |
| \`abs\_A\_B\`       | ABSolute                     | \`A=abs(B)\`                  | stores in \`A\` the absolute value of \`B\`                                             |
| \`dot\_AB\`         | DOT (inner) product          | \`A·B\`                       | returns the inner product of \`A\` and \`B\`                                            |
| \`dot\_ABC\`        | DOT product                  | \`(B-A)·(C-A)\`               | returns the inner product of vector from \`A\` to \`B\` with vector from \`A\` to \`C\` |
| \`angle\_ABC\`      | angle                        | \`acos(norm(A-B)·norm(C-B))\` | returns angle between direction from \`B\` to \`A\` and direction from \`B\` to \`C\`   |
| ...                 | ...                          | ...                           | ...                                                                                     |

## Prefixes

Some general notes follow.

  - If the function begins with a capitalized letter, these functions
    should be considered as "public" and therefore part of the Blender
    API (callable from other parts of Blender code).

### Modules-ish

Module prefixes can be in all caps (indicating public functions), but
not always. A module prefix indicates the module to which that function
"belongs" (see [Modules](../../../Modules/index.md)).

The following list includes modules as well as subprojects, subareas,
and other module-like groups of functions.

| Prefix                | Meaning           | Link                                                                         | Description                                                                                                                              |
| --------------------- | ----------------- | ---------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------- |
| \`BKE\_\*\`           | Blender KErnel    | [Core Module](https://developer.blender.org/tag/core/)                       | These functions are usually core Blender functions and should be considered low-level                                                    |
| \`BM\*\` / \`BM\_\*\` | BMesh             | [Modeling Module](https://developer.blender.org/tag/modeling/)               | Deal with the BMesh data structure. Most (all?) mesh altering operations (think Edit Mode) manipulate the mesh through BMesh operations. |
| \`ED\_\*\`            | EDit              | [Modeling Module](https://developer.blender.org/tag/modeling/)               | Manipulate Object data (Mesh, Curve, Text, etc.), used in Edit Mode.                                                                     |
| \`EDBM\_\*\`          | EDit mesh + BMesh | ?                                                                            | Edit mesh / BMesh manipulation functions.                                                                                                |
| \`VIEW3D\_\*\`        | VIEWport 3D       | [EEVEE & Viewport Module](https://developer.blender.org/tag/eevee_viewport/) | Render code for 3D Viewport                                                                                                              |
| \`WM\_\*\`            | Window Management | [User Interface Module](https://developer.blender.org/tag/user_interface/)   | Deals with window interaction                                                                                                            |

Comments:

  - I'm not sure what distinguishes the \`EDBM\_\*\` fns differ from the
    \`BM\*\` / \`BM\_\*\` fns, other than the BMesh fns are low level
    while the \`EDBM\` fns are higher (calling the BMesh fns) and work
    to maintain correspondence between edit mesh and bmesh.

### Non-modules

Non-module prefixes give indication as to how or why the function is
called. Groups of functions can adopt a local prefix (only used in a
single file or small number of files) to show their relation (ex:
\`raycast\_\*\` in \`raycast\_all\_cb\`,
\`raycast\_tri\_backface\_culling\_test\`, and \`raycast\_obj\_fn\`).
The table below covers only the prefixes that are broadly adopted.

| Prefix           | Meaning  | Description                                                                                                                                                                                                                                    |
| ---------------- | -------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| \`cb\_\*\`       | CallBack | Passed as arguments to other functions or stored in data structures to be called back at a later time. Some example uses of callback functions are list filtering, operating on geometry, handling a timer event, etc. (See \`\*\_fn\` suffix) |
| \`register\_\*\` | Register | Register higher order functions (for types, operators, callbacks, etc.) into Blender ecosystem (API) for use in the rest of Blender                                                                                                            |
| \`test\_\*\`     | Test     | Perform some binary test (return a \`bool\`), without side-effects.                                                                                                                                                                            |

## Suffixes

### Operators

Operators are groups of functions with the same base function name that
all work together to perform some operation. The suffix indicates what
part that function performs for the overall operation.

Note: this follows the BPY \`Operator\` naming conventions. See
\`wmOperatorType\`.

| Suffix         | Meaning | Description                                                                                                                                                                                                                     |
| -------------- | ------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| \`\*\_exec\`   | Execute | Performs the operation, and then returns control to caller. Although there could be adjustable parameters, \`\_exec\` functions perform their job with needing user interaction. They do what they do, then get out of the way. |
| \`\*\_invoke\` | Invoke  | Performs the operation, but with a period of user interaction (adjusting parameters, cancelled, committed, etc.) before returning control to the caller.                                                                        |
| \`\*\_poll\`   | Poll    | Return a \`bool\` to indicate whether the context is correct for the related \`\_exec\` and/or \`\_invoke\` functions to operate.                                                                                               |

### Non-operators

| Suffix                                                      | Meaning                           | Description                                                                                                                                                                                                                                          |
| ----------------------------------------------------------- | --------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| \`\*\_clear\` / \`\*\_destroy\` / \`\*\_free\`              | Clear / Destroy / Free            | Frees memory use. Usually related directly with data structures or a process that involves data structures. Think of this as a deconstructor in C. Note: \`\*\_clear\` should probably not deallocate memory, only free / unset it.                  |
| \`\*\_create\` / \`\*\_init\` / \`\*\_new\` / \`\*\_setup\` | Create / Initialize / New / Setup | Allocates and initializes data structures. Usually related directly with data structures or a process that involves data structures. Think of this as a constructor in C.                                                                            |
| \`\*\_ensure\`                                              | Ensure                            | Make sure that lookups and caches are set up and ready to use                                                                                                                                                                                        |
| \`\*\_ex\`                                                  | EXecute                           | Typically a wrapper for another function that has fewer arguments, but sometime it's the implementation it.                                                                                                                                          |
| \`\*\_fn\`                                                  | callback FuNction                 | Passed as arguments to other functions or stored in data structures to be called back at a later time. Some example uses of callback functions are list filtering, operating on geometry, handling a timer event, etc. (see \`cb\_\*\` prefix above) |
| \`\*\_get\`                                                 | Getter                            | Return a value that requires some calculation (could be cached, conditional, computed, etc.)                                                                                                                                                         |
| \`\*\_impl\`                                                | ?                                 | A "backend implementation" (think virtual function in C++) helper function. Typically, a main function will take an object as an argument, and then call the correct implementation for the given object.                                            |

Comments:

  - Should \`get\` be a final suffix as in \`\*\_color\_get\`, or should
    it be a verb of the suffix as in \`\*\_get\_color\`?
