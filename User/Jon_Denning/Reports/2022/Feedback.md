Below is a lightly paraphrasing of comments from Julian Kaspar and
Daniel Bystedt along with some of my comments and thoughts.

  - [Retopology Mode
    Feedback](https://docs.google.com/document/d/12697bOtUuiouxzEHY7Nq9RRT4kcvT_uzvcg-hNAV7X4)
    from Daniel Bystedt

## to-do list of items below

The list below is a collection of suggestions mentioned in the feedback
that I still need to work on.

  - Retopology Tools add-on
      - clean up user prefs
  - GeoPen
      - apply active material to new geometry
      - hotkeys not showing in status bar? check on this
  - ...

## retopology mode

Although I did get a Retopology Mode implemented
([D14035](https://developer.blender.org/D14035)) with its own set of
tools, visualization settings, and overlays, Campbell decided that
adding another specific mode might not be the best approach, and that we
should wait for a more generalized submode implementation.

## Snapping Options

The following address the options under the snapping menu.

### Face Nearest Steps \> 1 is lagging behind mouse

  - When the nearest steps is greater than 1, the moved vertices seem to
    lag behind the mouse cursor, making the experience of dragging verts
    around a bit strange.

Unfortunately, this is going to be an effect of this type of snapping
method whenever the vert is far from the target's surface, even with
Nearest Steps set to 1. This effect can also be seen (to a degree) with
Face Project. The only methods where this won't be seen are the methods
that work in screen space (Vertex, Edge).

I think the important thing to note is that this option is used to
"smooth" out snapped transformations along the U-shaped surface,
especially when transforming many verts. When steps is set to 1, the
snapping will pop from one side to another very suddenly. When steps is
\> 1, the snapping will lag behind but be much smoother. (so think of
steps as a smoothing factor, which will behave similarly to other
smoothed edits, rather than a edit with a proportional falloff)

I can think of a few workarounds that might help in some contexts, but
they would break in spectacular ways in other contexts. For example,
having a "multiplier" option in which the overall transformation is
broken up into n steps that are m/n times the transform vector, where n
is Nearest Steps and m is the multiplier. When m=1, you'll get exactly
the same behavior as it is implemented now. when m\>1 (not too big,
though), the code will try to push the vert a bit farther to match
better the overall transformation. However, on straight areas, the vert
will go past the mouse when m\>1. i tested out a different
implementation where the transform vector is broken up non-linearly
(following a curve similar to proportional falloff), but this attempt
was not an improvement.

I created a [youtube video](https://youtu.be/re_Cmfs3Sp0) demoing where
this feature could be useful.

### Retopo Mode is Confusing

  - The retopology mode is not intuitive from the label or description,
    requiring some education via video description. For example, should
    enabling Retopo Mode gray out the Snap To methods? Should it gray
    out the Target Selection?

This option filters down / controls which snapping method is applied to
which target. It still requires the method(s) to be enabled and the
target(s) to be an option. Graying out these options is not ideal, as
different problems will require different methods enabled (ex: face
project will do the wrong thing when shifting an edge loop that could be
on the backside of target. ex: face project is exactly needed when
creating new geometry via geopen, poly build, etc.)

Would it make sense to change the presentation of the methods if the
retopo mode is enabled? for example, below I've stripped the options
down to those that would be used when in Retopo Mode.

` ┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓`  
` ┃ [X] Retopo Mode                ┃`  
` ┃                                ┃`  
` ┃ Snap To Edited:                ┃`  
` ┃ [ Vertex                     ] ┃`  
` ┃ [ Edge                       ] ┃`  
` ┃ [ Edge Center                ] ┃`  
` ┃ [ Edge Perpendicular         ] ┃`  
` ┃                                ┃`  
` ┃ Snap To Non-Edited:            ┃`  
` ┃ [ Face Project               ] ┃`  
` ┃                                ┃`  
` ┃ Snap With:                     ┃`  
` ┃ [Closest|Center|Median|Active] ┃`  
` ┃                                ┃`  
` ┃ Options:                       ┃`  
` ┃ [X] Exclude Non-Selectable     ┃`  
` ┃ [ ] Backface Culling           ┃`  
` ┃ [ ] Snap to Same Target        ┃`  
` ┃ Face Nearest Steps          1  ┃`  
` ┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛`

A few notes:

  - to keep verts on target surface(s):
      - Increment or Volume methods are hidden and disabled
      - Face Nearest will always be enabled and hidden (fall back method
        for all other snapping methods)
      - Project Individual Elements is always enabled and hidden
        (otherwise Face Project will treat entire selection as an
        object)
      - Affect Move,Rotate,Scale are always enabled and hidden
  - Include Active, Edited, and Non-Edited are always enabled, but
    artist can still control snapping via enabling the corresponding
    methods.
  - Snap With is the only option that's not great... it's needed only
    when transforming selection as a group, which is disabled for Face
    Project but an effect of Vertex/Edge snapping :(
      - Maybe Snap With could be hidden, and the Vertex / Edge snapping
        methods are grayed out / ignored when more than one vertex is
        selected?? this is an unfortunate problem with using the
        built-in snapping method.
      - Alternative: Face Nearest could be applied *always* after any
        other method is applied?

### Target Selection is Confusing

  - the distinction of "Active" and "Edited" vs the previous "Self" is
    confusing (requires some education). Also, having all the options is
    starting to grow the snapping menu.

I agree, the terms are confusing. Sadly, the term used before was also
confusing when editing multiple meshes. (self is incorrect, as it was
really the active object, so there were cases were snapping against
"self" was not working as expected) I'm guessing that editing multiple
meshes is a fairly uncommon task. As such, maybe the additional
complexity should only show when in that context. For example, only show
"Include Edited" target selection option when editing a single mesh
(which is effectively the old "Self" or new "Include Active"), but show
"Include Active Edited" and "Include Other Edited" when editing multiple
meshes?

An important note: the target selection is referring to what objects in
the scene will be considered as snapping targets. In particular, when
Include Non-Edited is enabled then all non-edited objects in the scene
(typically, this would be the high-res mesh that you're retopo'ing) will
be a potential snap target. When Include Active is enabled, the mesh
you're currently editing will be a potential snap target. When Include
Edited is enabled, the non-active mesh that you're also editing (when
editing two meshes at the same time) will be a potential snap target.

### Snapping Options Menu is Too Big / Complex

I'm not sure what the best approach is for this. The previous snapping
options were far too crude for retopology work. There were no ways to
finely control the options. This is why RetopoFlow does not use any of
the built-in snapping methods.

An important note: it is likely that artists won't need to have this
same granular level of control with snapping. in fact, unless an
operator / tool uses the built-in transform operator, snapping will not
get performed automatically (as the snapping is done within the
Transformation "modal"). but, perhaps the menu could be heavily
simplified, leaving the complexity in the API where operators and tools
can take advantage of it, like how GeoPen does now.

As a slightly-atypical-for-Blender alternative, maybe the fine control
features could be hidden behind an Advanced checkbox option?

### Missing Face Nearest Options

  - it seems that Snap With, backface culling, and snap individual
    elements are missing options for Face Nearest method.

backface culling would likely not do the correct thing. it makes sense
for Face Project, as this method works with the current view. face
nearest does not consider what is in view, so there really cannot be a
"backfacing" polygon.

Snapping group vs individual elements does make sense, but I haven't
implemented this yet. future feature? (Snap individual elements and Snap
With are very related options)

### Does not work with Rotate/Scale

  - rotation and scaling snapping is not supported, but you also can't
    disable those snappings holding CTRL

Actually, it is supported, but it must be enabled first in the snapping
menu (see very bottom, \`Affect: Move | Rotate | Scale\`). This is an
unfortunate limitation to using the transform+snapping(+proportional)
system. Now, some of my recent patches to the system expose options, and
GeoPen (or other tools) can specifically enable snapping for any type of
movement, but it still requires either the tool to explicitly specify or
the artist to enable these options.

### Snapping does not work in isolation mode

  - Spotted that this snap breaks when entering isolation mode or when
    basemesh is hidden or disabled in viewport (which can be useful for
    precise topological tweaks without observing basemesh), it would be
    awesome if it is possible to make something about that.

This is by design. Any visible object that isn't being edited is
considered a potential target. If it isn't visible, or if it isn't
selectable (if Exclude Non-selectable is enabled), then it cannot be a
target.

We \_could\_ expand the set of options to allow non-visible objects to
be potential targets, but this further complicates the setup.

## GeoPen

The following address feedback with the Geometry Pen tool.

### Tweak Fallback Not Working

  - Cannot tweak vertices/edges/faces without first selecting them,
    making adjusting slower.

the Tweak fallback always tweaks the selected geometry, regardless of
the tool's implementation. (And unfortunately, the tool cannot control
the snapping options without actually adjusting the scene tool
settings... which is not always ideal).

So, I'm not sure how to approach this without just implementing a
Select+Tweak option for GeoPen. Maybe have a Gizmo that follows the
mouse? This does break away from how most of the other tools work
(exceptions being Poly Build, Knife, others?), but it would perhaps
improve overall UX for retopo work.

### Quad-Only Insertion Mode

  - A Quad Only mode for GeoPen just makes sense.

totally agree\! not yet implemented.

### Key binding

  - Binding insert to OSKey is non-ideal. Perhaps use combinations of
    Shift / Ctrl / Alt to control modes of insertion?

I think we could use any key here. I do have a hesitation on using Ctrl,
though, as this is used to toggle snapping when transforming. This means
that an artist might have press Ctrl before inserting but then
immediately release Ctrl to prevent snapping from disabling.

Alternative: use a "normal" key, similar to how D is used to annotate.

### Merge Target Edge

  - It would be nice to be able to bridge between two edges in one
    click, or to merge a transformed edge into a hovered edge.

The former could be implemented in GeoPen: to automatically bridge
between selected edge and hovered edge when inserting. However, the
snapping system is too simple to handle the latter case. Again, this is
the reason behind the custom snapping code in RetopoFlow.

### Insertion Logic is Opaque

  - Sometimes it is hard to know which component will be used as a
    "base" for when adding new geometry.

a preview visualization (Gizmo?) would be ideal for communicating what
will happen when inserting a new vertex. Poly Build does this.

### Show Snapping with Previz

  - The previz would be more helpful if it also shows snapping. Right
    now it only does this during the operation.

totally agree. unfortunately, there isn't a good way to do this as of
right now. the five closest options are:

1.  calling \`closest\_point\_on\_mesh()\` on the edit object to find
    the nearest point. however, there are two serious drawbacks. first,
    this fn is done in World space, not screen space, so the results is
    not going to be the same as transformation snapping. second, this fn
    does not return a vertex or edge, but the point that's closest to
    the given location, which could be in the middle of a face.
2.  storing the vertices in some acceleration structure (e.g.,
    \`mathutils.bvhtree.BVHTree\` or \`mathutils.kdtree.KDTree\`) after
    projecting them to the screen. this would produce exactly the same
    result as xform snapping. we would have to iterate over all the
    vertices, which grows linearly with the size of the source mesh, but
    that effort could be amortized across many insertions (as long as
    the view doesn't change)... in other words, it's not too big of a
    deal. however, taking this approach does mean that we're duplicating
    code work, which isn't ideal. my hope for this project was to
    leverage the code that's already available, and not create new
    features unless it's needed.
3.  expose the snapping C++ code through \`bpy\`, which is implemented
    basically the same way as the previous point noted. this would
    benefit the broader add-on dev community quite significantly (i.e.,
    not just benefit this project), but it could be very tricky to do,
    and it's well beyond the scope of this project. (I've already
    implemented a new snapping method, but that's a critical feature for
    retopo work, and that took great effort and convincing)
4.  rewrite this tool in C++, similar to polybuild and knife. this
    approach would allow the tool to leverage any part of Blender that's
    already been written. However, this isn't ideal for a few reasons.
    first, it signals that proper tools written in Python are always
    going to be 2nd class to built-in C++ tools. It also means that
    future tool development will be slower, since Python development
    allows for faster iteration when compared to C++ dev.
5.  the final option is a bad hack, but it could work: insert a
    temporary vertex into the mesh, call \`bpy.ops.transform.translate\`
    on it to snap it with all the same snapping options (but make sure
    auto merge and auto split are disabled), get its location, then
    delete the temporary vertex when done.

### Create Vertex on Press and on Release

  - Currently we only have this behaviour: When holding Alt and left
    clicking it will insert a triangle, repeat next to a selected
    triangle and it will merge them into a quad. How about we add this:
    When holding Alt and click and dragging it will create a quad with
    the two new points being added on the press event and on the release
    event.

This is an interesting approach. it would basically halve the number of
clicks needed to insert a quad, and it would allow the artist to place
the verts *exactly* where they want the verts to go (as opposed to using
the mouse position and the two other vert positions to predict where the
two new verts should go). it should be a quick thing to implement to
test.

### Remove Tool Fallback / Add Geometry from Anywhere (without needing selection)

  - we should remove the fallback drag support from this tool and make
    it tweak by default.

This change would make this tool feel much more like sculpting (which is
how Poly Build feels now), which is different from every other edit mesh
tool, except knife. I can see well-supported arguments for either
paradigm. a few arguments for keeping it as is:

  - presently, there is no Retopology Mode, only Edit Mode, and nearly
    all the tools in edit mode look and feel exactly the same. the two
    exceptions are Knife and Poly Build. but, these two feel out of
    place from the rest of the tool set. i suspect that it will be
    harder to argue for including yet another edit mode tool that
    doesn't look, act, or feel like other edit mode tools. keep in mind,
    though, that I don't mind breaking away from the status quo\! :) I'm
    totally onboard with making a tool that does its job in (one of?)
    the best way.
  - presently, there isn't a good option to find the nearest existing
    geometry (see notes on **Show Snapping with Previz**). a hack might
    be to call select on every mouse move... but a few tests have simply
    failed to work or caused blender to crash.
  - it becomes VERY difficult to distinguish between selecting and
    creating when the mouse is "near" existing geometry, especially as
    the density of the geometry goes up. i see this very quickly with
    poly build. In fact, in a simple test, i quickly ran into a
    situation (see screenshot below) where there was no way to perform
    the operation that i wanted to do without changing the view
    (indirectly changing the density of the verts/edges on the screen).
    pre-visualization can help with this issue to a degree, but it only
    helps the artist know what the tool is thinking... not in helping
    the tool know what the artist is thinking.

![example of where poly build cannot create a desired
face](../../../../images/Polybuildfail.png
"example of where poly build cannot create a desired face")

again, i'm not tied to sticking with the Edit Mode paradigm. I'm just
trying to navigate creating a new tool that's going to exist in Edit
Mode.

### Tweak on Press

  - If tweaking becomes the primary action with left click, we should
    consider using it on Press. The Poly Build tool does the same thing
    and it just leads to a better feel since we don't have to worry
    about drag thresholds. Selecting and adding to the selection would
    still be possible but the adding and moving of geometry is the focus
    of this tool.

I totally agree\!

### Make sticky mode discoverable

  - Adding that to the tool settings and status bar in some way will
    also help to expose it. So more understand how to use it

I totally agree\! I just haven't polished that much yet :)

### new geometry should use active material

  - paint new polygons in active material instead the first one (also
    was made for F2, very useful for selection sets)

Agreed. This is addition polish.

### Location of settings

  - Move insert mode and other local prefs to N-panel. It can be a
    better place than user prefs, since is better accessible

I'm not sure this is a good idea. The insert mode (for example) is
already located in the tool header, because that setting is specific to
the GeoPen tool. all other Blender tool have tool settings located in
the tool header.

I probably do need to clean up the add-on settings (user prefs).

### Cannot use Alt keymaps with previsualizations

  - Alt blocks alt+T shortcut

This is due to a limitation in how I've set up the previz stuff. In
particular, when the Alt key is pressed, the GeoPen tool starts running
as modal. Now, we \_could\_ simply pass through any event that isn't
handled, but this starts to get tricky.

As an alternative, the previz could be done in its own Gizmo, rather
than drawn in modal. I think this is a better idea, but it still has
some limitations if I recall correctly.

### Hotkeys in Status Bar

  - Hotkey information is missing in the info bar when geo pen is the
    active. My guess is that this is planned but not implemented.

This should be there already. Not sure why it wasn't working for you.
Can double check.

### Using GeoPen Modifies Snapping

  - if snapping is off when using geo pen, blender will turn on snapping
    after the first left mouse down event

Yes, this is a bug (I believe) in the API. I've tried searching for
where it happens, but was unable to figure it out. I've pinged Campbell
about it, but I'll probably just need to file a bug report.

### Only one vertex merges in Quad Insertion

  - During insert mode quad only - only one vertex is snapping. My guess
    is that 2 vertex snapping to target edge vertices is not yet
    implmented, but thuought I should mention it just in case.

Yes, this is a limitation in using the transform+snapping system,
because it is designed to have either one or zero source snapping points
(Mean, Active, Center, etc.)

### Quad-only Keep Length Option

  - During insert mode "Quad only" - what does "keep length" do?

There are many ways of determining where the 2x verts go when using Quad
Only insertion. I have implemented two different approaches in GeoPen,
and the Keep Length option switches between them. The difference is
this: when Keep Length is enabled, the two new verts are positioned so
that the distance between them in screen space is the same screen space
distance as between the selected verts.

Really, we should just find an algorithm for placing the two verts that
feels the best, and use that.

### Have Key toggle between using selection as fallback and tweak

  - I agree with Julien that it would be nice with a button to activate
    tweak mode. I think default action should be selection, since that
    is the default behaviour on the majority of the tools in edit mode.

I agree. Unfortunately, this might not be simple to implement due to how
the fallback keymap system works. Again, an unfortunate limitation due
to using the built-in systems.

### Comparing F2 with GeoPen

  - Express workflow comparison between F2 and Geo on solving the same
    topo scheme. Am I doing everything the way I'm supposed to? It is
    necessary to know to calculate actions, aimings, required mouse path
    and estimate overall process energy.
  - [link to
    blender.chat](https://blender.chat/channel/retopology?msg=oCuvkDRLMPEDumdu2)

Yeah, these are two different sets of problems.

F2 is great for filling in holes. GeoPen is great for creating new
geometry. Both tools have an overlap. But in the end, the artist should
use the tool that best helps them solve the problem.

One thing that could improve your usage is to enable vert+edge
selection. Doing so can reduce the number of selection clicks that you
need for GeoPen.

The Quad Insertion could also be improved to speed up solving this
particular problem, but we start heading into writing parallel systems
to Blender's built in and limited systems.

## Retopology "Submode"

### Optimize Visualization Settings

  - Optimize settings are not temporal/undoable (and, fairly, has
    questionable setup)

I agree. This is a carry-over from an idea from a few months ago.
Without an official Retopology submode, this was intended to change
settings to make it feel like its own mode. It's lacking any sort of
configuration, restoring features, etc. This button is not needed once
we have a retopo submode, and it will always feel not-quite-right for
everyone.
