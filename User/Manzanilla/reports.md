## Sep 20 - Sep 24 / Sep 27 - Oct 01

Took first week off. Last week I've been mostly fixing bugs and issues
related to canvas compositing.

**Reviews:**

  - D12366: Cleanup: convert compositor nodes to c++.
    [D12366](https://developer.blender.org/D12366)

**Worked on Patches:**

  - Compositor: Add support for canvas compositing
    [D12466](https://developer.blender.org/D12466)
  - Compositor: Support backdrop offset for the Viewer node
    [D12750](https://developer.blender.org/D12750)

**Commits:**

  - Compositor: Fix Composite node using alpha when "Use Alpha" is off
    [rB66fe1c79](https://projects.blender.org/blender/blender/commit/66fe1c79f3ce)
  - Compositor: Fix Dilate/Erode node crash with Step option
    [rBf3274bfa](https://projects.blender.org/blender/blender/commit/f3274bfa70f0)
  - Compositor: Fix Flip node not flipping translation on Full Frame
    [rBe2df5c8a](https://projects.blender.org/blender/blender/commit/e2df5c8a56c0)
  - Compositor: Fix Movie Distortion node rendering an empty image
    [rB4569d9c0](https://projects.blender.org/blender/blender/commit/4569d9c0c3b0)
  - Compositor: Add support for canvas compositing
    [rBf84fb12f](https://projects.blender.org/blender/blender/commit/f84fb12f5d72)
  - Compositor: Full frame Glare node
    [rB283d76a7](https://projects.blender.org/blender/blender/commit/283d76a70dba)
  - Cleanup: Remove XRange and YRange in Compositor
    [rB0830211c](https://projects.blender.org/blender/blender/commit/0830211c9529)
  - Compositor: Replace resolution concept by canvas
    [rB76377f01](https://projects.blender.org/blender/blender/commit/76377f0176b9)

## Sep 06 - Sep 10 / Sep 13 - Sep 17

**Worked on patches:**

  - Compositor: Add support for canvas compositing
    [D12466](https://developer.blender.org/D12466)
  - Compositor: Replace resolution concept by canvas
    [D12465](https://developer.blender.org/D12465)

**Commits:**

  - Compositor: Add OIDN prefiltering option to Denoise node
    [rB276eebb2](https://projects.blender.org/blender/blender/commit/276eebb27474)
  - Compositor: Fix crash exporting buffers on debug
    [rBf256bfb3](https://projects.blender.org/blender/blender/commit/f256bfb3e26c)
  - Compositor: Fix Alpha Over node ignoring emissive colors
    [rB942c471c](https://projects.blender.org/blender/blender/commit/942c471ce969)
  - Compositor: Full frame previews
    [rB863460e5](https://projects.blender.org/blender/blender/commit/863460e5a5cb)
  - Compositor: Fix crash when hashing unconnected operations
    [rBcbe05edd](https://projects.blender.org/blender/blender/commit/cbe05edde596)

## Aug 23 - Aug 27 / Aug 30 - Sep 03

**Reviews:**

  - Compositor: New Posterize Node.
    [D12304](https://developer.blender.org/D12304)
  - Cleanup: convert compositor nodes to c++.
    [D12366](https://developer.blender.org/D12366)

**Worked on patches:**

  - Compositor: Add OIDN prefiltering option to Denoise node
    [D12342](https://developer.blender.org/D12342)
  - Compositor: Merge equal operations
    [D12341](https://developer.blender.org/D12341)

**Commits:**

  - Compositor: Full frame filter nodes
    [rB9d7cb5c4](https://projects.blender.org/blender/blender/commit/9d7cb5c4a115)
  - Compositor: Full frame vector nodes
    [rBd84c79a2](https://projects.blender.org/blender/blender/commit/d84c79a218e6)
  - Add compositor tests
    [rBL62687](https://projects.blender.org/blender/blender-dev-tools/commit/62687)
  - Fix T89998: Cryptomatte node output values doubled with Multi-View
    [rB7d17f2ad](https://projects.blender.org/blender/blender/commit/7d17f2addf80)
  - Compositor: Fix incorrect copying of uchar buffers
    [rB42f89b92](https://projects.blender.org/blender/blender/commit/42f89b9212d3)
  - Compositor: Fix crash enabling buffer groups on full frame
    [rB4c6d2073](https://projects.blender.org/blender/blender/commit/4c6d207343bc)
  - Compositor: Full frame matte nodes
    [rB153b4503](https://projects.blender.org/blender/blender/commit/153b45037f5d)
  - Compositor: Full frame Bokeh Blur and Blur nodes
    [rBdaa7c59e](https://projects.blender.org/blender/blender/commit/daa7c59e38c8)
  - Compositor: Full frame transform nodes
    [rB064167fc](https://projects.blender.org/blender/blender/commit/064167fce70e)
  - Compositor: Full frame distort nodes
    [rB344aca3b](https://projects.blender.org/blender/blender/commit/344aca3b1bf2)
  - Compositor: Add sampling methods for full frame
    [rBa95e56b7](https://projects.blender.org/blender/blender/commit/a95e56b74170)
  - Compositor: Full frame convert nodes
    [rB8f4730e6](https://projects.blender.org/blender/blender/commit/8f4730e66f45)

## Aug 09 - Aug 13 / Aug 16 - Aug 20

**Worked on patches:**

  - Compositor: Add more render tests
    [D12286](https://developer.blender.org/D12286)
  - Compositor: Full frame vector nodes
    [D12233](https://developer.blender.org/D12233)
  - Compositor: Full frame matte nodes
    [D12220](https://developer.blender.org/D12220)
  - Fix T89998: Cryptomatte node output values doubled with Multi-View
    [D12216](https://developer.blender.org/D12216)
  - Compositor: Full frame Bokeh Blur and Blur nodes
    [D12167](https://developer.blender.org/D12167)
  - \[WIP\] Compositor: Full frame Glare node
    [D12232](https://developer.blender.org/D12232)
  - Compositor: Full frame filter nodes
    [D12219](https://developer.blender.org/D12219)

**Commits:**

  - Fix T90572: "Render Region" is broken due to compositing
    [rBb6538e14](https://projects.blender.org/blender/blender/commit/b6538e1492bf)
  - Compositor: Full frame output nodes
    [rBeb03529a](https://projects.blender.org/blender/blender/commit/eb03529ab950)
  - Compositor: Full frame color nodes
    [rBd481c665](https://projects.blender.org/blender/blender/commit/d481c6651d14)
  - Compositor: Full frame curve nodes
    [rB8f6cc164](https://projects.blender.org/blender/blender/commit/8f6cc1649084)
  - Compositor: Full frame input nodes
    [rB1a9b9dd6](https://projects.blender.org/blender/blender/commit/1a9b9dd64df7)
  - Compositor: Fix memory leaks when initializing tiles multi-threaded
    [rBb81d88a8](https://projects.blender.org/blender/blender/commit/b81d88a8e245)
  - Compositor: Full frame Mask node
    [rB5deb3229](https://projects.blender.org/blender/blender/commit/5deb3229a075)
  - Compositor: Full frame Bilateral Blur node
    [rB079f3557](https://projects.blender.org/blender/blender/commit/079f35572b1d)
  - Compositor: Full frame Sun Beams node
    [rB0116a567](https://projects.blender.org/blender/blender/commit/0116a567dd06)

## Jul 26 - Jul 30 / Aug 02 - Aug 06

**Worked on patches:**

  - Compositor: Full frame distort nodes
    [D12166](https://developer.blender.org/D12166)
  - Compositor: Full frame transform nodes
    [D12165](https://developer.blender.org/D12165)
  - Compositor: Add sampling methods for full frame
    [D12164](https://developer.blender.org/D12164)
  - Compositor: Full frame convert nodes
    [D12095](https://developer.blender.org/D12095)
  - Compositor: Full frame curve nodes
    [D12093](https://developer.blender.org/D12093)
  - Compositor: Full frame color nodes
    [D12092](https://developer.blender.org/D12092)
  - Compositor: Full frame output nodes
    [D12091](https://developer.blender.org/D12091)
  - Compositor: Full frame input nodes
    [D12090](https://developer.blender.org/D12090)
  - Compositor: Full frame Mask node
    [D11751](https://developer.blender.org/D11751)
  - Compositor: Full frame Bokeh Blur and Blur nodes
    [D12167](https://developer.blender.org/D12167)

**Commits:**

  - Compositor: Buffer iterators tests
    [rB549e2b75](https://projects.blender.org/blender/blender/commit/549e2b753982)
  - Compositor: Fix wrong number of threads during constant folding
    [rB5762e7a6](https://projects.blender.org/blender/blender/commit/5762e7a67903)
  - Cleanup: unneeded default constructor definitions
    [rBd2675c3c](https://projects.blender.org/blender/blender/commit/d2675c3c5d7b)
  - Fix compile error on macos introduced in last commit
    [rB05315af8](https://projects.blender.org/blender/blender/commit/05315af81dec)
  - Compositor: Full frame Box Mask node
    [rBa4a72bff](https://projects.blender.org/blender/blender/commit/a4a72bffd33a)
  - Compositor: Full frame Levels node
    [rBe33814ef](https://projects.blender.org/blender/blender/commit/e33814ef6bae)
  - Compositor: Fix memory leak when exporting operations on debug
    [rB883fb49d](https://projects.blender.org/blender/blender/commit/883fb49d4f11)
  - Compositor: Full frame Scale node
    [rBa117794f](https://projects.blender.org/blender/blender/commit/a117794f8c05)
  - Update cryptomatte legacy test file
    [rBL62659](https://projects.blender.org/blender/blender-dev-tools/commit/62659)

## Jul 12 - Jul 16 / Jul 19 - Jul 23

**Reviews:**

  - D11881: Convert ID Mask node AntiAliasing to SMAA.
    [D11881](https://developer.blender.org/D11881)

**Worked on patches:**

  - Compositor: Buffer iterators tests
    [D12001](https://developer.blender.org/D12001)
  - Compositor: Full frame Scale node
    [D11944](https://developer.blender.org/D11944)
  - Compositor: Full frame Levels node
    [D11749](https://developer.blender.org/D11749)
  - Compositor: Full frame Box Mask node
    [D11627](https://developer.blender.org/D11627)

**Commits:**

  - Compositor: Fix crash when using empty input sources
    [rB1a91c573](https://projects.blender.org/blender/blender/commit/1a91c5732032)
  - Compositor: Full frame Texture node
    [rBb1bf4c2a](https://projects.blender.org/blender/blender/commit/b1bf4c2a0569)
  - Compositor: Full frame Movie Clip node
    [rB48e27ad1](https://projects.blender.org/blender/blender/commit/48e27ad12258)
  - Compositor: Add coordinates to BuffersIterator
    [rB5f28a90b](https://projects.blender.org/blender/blender/commit/5f28a90b3446)
  - Compositor: Fix buffer area iterating past the end
    [rB91e2b1dc](https://projects.blender.org/blender/blender/commit/91e2b1dcafe0)
  - Compositor: Fix crash when connecting multiple constant inputs
    [rB75c9788c](https://projects.blender.org/blender/blender/commit/75c9788c2753)
  - Compositor: Export operation results as debug option
    [rB468765d2](https://projects.blender.org/blender/blender/commit/468765d29e78)
  - Compositor: Full frame Brightness node
    [rB9aa88a66](https://projects.blender.org/blender/blender/commit/9aa88a66040e)
  - Compositor: Full frame Mix node
    [rBb35efa93](https://projects.blender.org/blender/blender/commit/b35efa932e03)
  - Compositor: Full frame Viewer node
    [rB300fe84b](https://projects.blender.org/blender/blender/commit/300fe84bf0ad)
  - Compositor: Full frame Double Edge Mask node
    [rBb848d5cd](https://projects.blender.org/blender/blender/commit/b848d5cdf5b6)
  - Compositor: Full frame Ellipse Mask node
    [rB0a0c2c02](https://projects.blender.org/blender/blender/commit/0a0c2c0217d4)
  - Compositor: Buffer iterators
    [rB45b46e5d](https://projects.blender.org/blender/blender/commit/45b46e5de9fb)
  - Compositor: Full frame Translate node
    [rB538f452e](https://projects.blender.org/blender/blender/commit/538f452ea97f)
  - Compositor: Full frame Render Layers node
    [rB96a4b54c](https://projects.blender.org/blender/blender/commit/96a4b54cfb5c)
  - Compositor: Fix pixels being wrapped outside buffer area
    [rB2ea47057](https://projects.blender.org/blender/blender/commit/2ea47057d33e)
  - Compositor: Fix convert resolutions linking different socket
    datatypes
    [rB209aff0a](https://projects.blender.org/blender/blender/commit/209aff0a3539)

## Jun 28 - Jul 02 / Jul 05 - Jul 09

**Worked on patches:**

  - Fix: Compositor wrapping pixels outside buffer area
    [D11784](https://developer.blender.org/D11784)
  - Compositor: Buffer iterators
    [D11882](https://developer.blender.org/D11882)
  - \[WIP\] Compositor: Add position to operations
    [D11783](https://developer.blender.org/D11783)
  - Compositor: Full frame Double Edge Mask node
    [D11750](https://developer.blender.org/D11750)
  - Compositor: Export operation results as debug option
    [D11722](https://developer.blender.org/D11722)
  - Compositor: Full frame Viewer node
    [D11698](https://developer.blender.org/D11698)
  - Compositor: Full frame Ellipse Mask node
    [D11635](https://developer.blender.org/D11635)
  - Compositor: Full frame Pixelate node
    [D11801](https://developer.blender.org/D11801)
  - Compositor: Full frame Mask node
    [D11751](https://developer.blender.org/D11751)
  - Compositor: Full frame Levels node
    [D11749](https://developer.blender.org/D11749)

**Commits:**

  - Compositor: Fix constant folded operations not being rendered
    [rB6ac3a106](https://projects.blender.org/blender/blender/commit/6ac3a106190c)
  - Compositor: Fix crash when executing works in constant folding
    [rB1657fa03](https://projects.blender.org/blender/blender/commit/1657fa039dcb)
  - Compositor: Fix execution system unset during constant folding
    [rB46a261e1](https://projects.blender.org/blender/blender/commit/46a261e108ee)
  - Compositor: Enable constant folding on operations
    [rB5780de2a](https://projects.blender.org/blender/blender/commit/5780de2ae052)
  - Cleanup: Set execution system as operations member in Compositor
    [rBa070dd8b](https://projects.blender.org/blender/blender/commit/a070dd8bdd6e)
  - Fix T89671: Crash when using Denoise node on Full Frame mode
    [rBcf17f7e0](https://projects.blender.org/blender/blender/commit/cf17f7e0cc6e)

## June 14 - June 27

Working on patches:

  - Compositor: Export operation results as debug option
    ([D11722](https://developer.blender.org/D11722))
  - Compositor: Graphviz improvements
    ([D11720](https://developer.blender.org/D11720))
  - Compositor: Full frame Viewer node
    ([D11698](https://developer.blender.org/D11698))
  - Compositor: Full frame Sun Beams node
    ([D11694](https://developer.blender.org/D11694))
  - Compositor: Full frame Render Layers node
    ([D11690](https://developer.blender.org/D11690))
  - Compositor: Full frame Mix node
    ([D11686](https://developer.blender.org/D11686))
  - Compositor: Full frame Brightness node
    ([D11677](https://developer.blender.org/D11677))
  - Compositor: Full frame ID Mask node
    ([D11638](https://developer.blender.org/D11638))
  - Compositor: Full frame Ellipse Mask node
    ([D11635](https://developer.blender.org/D11635))
  - Compositor: Full frame Bilateral Blur node
    ([D11634](https://developer.blender.org/D11634))
  - Compositor: Full frame Box Mask node
    ([D11627](https://developer.blender.org/D11627))
  - Compositor: Constant folding
    ([D11490](https://developer.blender.org/D11490))

## May 31 - June 13

Patches:

  - Compositor: Full frame Image node
    ([D11559](https://developer.blender.org/D11559))
  - Compositor: Full frame RGB node
    ([D11593](https://developer.blender.org/D11593))
  - Compositor: Full frame Value node
    ([D11594](https://developer.blender.org/D11594))
  - Compositor: Refactor recursive methods to iterative
    ([D11515](https://developer.blender.org/D11515))
  - Compositor: Constant folding
    ([D11490](https://developer.blender.org/D11490))

Bug fixing:

  - Fix: Image node alpha socket converted to operations twice
    ([D11566](https://developer.blender.org/D11566))
  - Fix: Compositor test desintegrate failing on arm64
    ([D11546](https://developer.blender.org/D11546))

## May 17-21/24-28

Experimenting on "Full Frame Compositor" branch for future work to do as
part of task [T88150](https://developer.blender.org/T88150):

  - Doing constant folding to reduce rendering time of operations that
    can have constant pixel values.
  - Trying to do more efficient rendering by cropping resolutions to
    rendered areas.
  - Adding offsets to operations to find a way to work on flexible
    canvases and fix cropping issues.

Working on patches:

  - Compositor: Full-frame base system
    ([D11113](https://developer.blender.org/D11113))
  - Compositor: WorkScheduler tests
    ([D11295](https://developer.blender.org/D11295))
  - Cleanup: Refactor PlaneTrack and PlaneDistort operations
    ([D11273](https://developer.blender.org/D11273))

## May 3-7/10-14

Mainly working on "Full Frame Compositor" task
([T88150](https://developer.blender.org/T88150))

Patches:

  - Add vars and methods for easier image looping
    ([D11015](https://developer.blender.org/D11015))
  - Only read input constants to determine input area
    ([D11090](https://developer.blender.org/D11090))
  - Full-frame base system
    ([D11113](https://developer.blender.org/D11113))
  - Cleanup: Refactor PlaneTrack and PlaneDistort operations
    ([D11273](https://developer.blender.org/D11273))
  - Exclude 'Render Result' and 'Viewer' images where not supported
    T73182 ([D11179](https://developer.blender.org/D11179))

## April 21-23/26-30

Compositor ongoing development:

  - Base classes for full-frame operations
    ([D11032](https://developer.blender.org/D11032))
  - Convert filter operations to full frame
    ([D11043](https://developer.blender.org/D11043))
  - Add vars and methods for easier image looping
    ([D11015](https://developer.blender.org/D11015))
  - Only read input constants to determine input area
    ([D11090](https://developer.blender.org/D11090))
  - Full-frame base system
    ([D11113](https://developer.blender.org/D11113))

Compositor bug fixing:

  - WorkScheduler task model deletes works
    ([D11102](https://developer.blender.org/D11102))
