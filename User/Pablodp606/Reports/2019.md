### December 23 - December 27

This week I finished the minimum requirements for the sculpt vertex
colors patch. Rendering, multiple layers, alpha and conversion operators
are working.

**Next week**

  - The way the default brush of sculpt vertex colors applies paint is
    not right, I need to investigate that.
  - Merge the remaining paint brush parameters and check if everything
    works.

**Patches**

  - [D6498](https://developer.blender.org/D6498) Fix T72747: Increase
    minimum voxel size in the voxel remesher
  - [D6497](https://developer.blender.org/D6497) Sculpt: Update brush
    defaults
  - [D6488](https://developer.blender.org/D6488) Fix T72700: Missing
    flush vert visibility on sculpt undo
  - [D6487](https://developer.blender.org/D6487) Fix T72690: Do not draw
    points behind the viewport camera in the paint cursor
  - [D6476](https://developer.blender.org/D6476) Fix T71712: Free the
    dynamic mesh preview when rebuilding the PBVH
  - [D6475](https://developer.blender.org/D6475) Fix T72647: Check if
    the PBVH type makes sense for the sampling mode

### December 16 - December 20

I worked on the sculpt vertex colors patch, updated the IK pose brush
patch and create a new operator to control the voxel size of the voxel
remesher

**Next week**

  - Work on sculpt vertex colors for 2.83

**Patches**

  - [D6449](https://developer.blender.org/D6449) Voxel Remesh: Edit
    Voxel Size operator
  - [D6444](https://developer.blender.org/D6444) Sculpt: Remove partial
    viewport updates from sculpt stroke code
  - [D6433](https://developer.blender.org/D6433) Sculpt: Add color alpha
    controls to the brush cursor
  - [D6426](https://developer.blender.org/D6426) Fix T72438: Use PBVH
    vert coordinates in sculpt API when using shape keys

### December 9 - December 13

I spent most of the week refactoring and testing the IK pose brush. I
also worked for the first time with the 3D texture projetion code to
plan what improvements can be done for UDIM painting.

**Next week**

  - Continue working on 2.82 projects

**Patches**

  - [D6389](https://developer.blender.org/D6389) Sculpt: Pose Brush with
    Inverse Kinematics
  - [D6385](https://developer.blender.org/D6385) Texture Paint: New
    Cursor and Spherical 3D falloff

**Commits**

  - Fix T72409: Remove references to Unified use\_pressure\_size and
    use\_pressure\_strength
    [c6ba941339](https://projects.blender.org/blender/blender/commit/c6ba9413393).

### December 2 - December 6

I continued working on the new tools, focusing on vertex colors. Tools
like the color filter, gradient tools or mask expand by color are
working.

**Next week**

  - Continue working on 2.82 projects

**Patches**

  - [D6376](https://developer.blender.org/D6376) Fix T72251: Add rotate
    brush as constrained by radius for automasking

**Commits**

  - Fix T72006: Clay brush with size pressure corrupts mesh
    [e77fdc62b7](https://projects.blender.org/blender/blender/commit/e77fdc62b75).
  - Fix T72054: Sculpt Mode crash when using Relax Mesh Filter with
    Dyntopo enabled
    [8a7851de3a](https://projects.blender.org/blender/blender/commit/8a7851de3a0).
  - Fix T72092: Clay Strips Brush stroke crash with Brush Detail Size
    [448669a630](https://projects.blender.org/blender/blender/commit/448669a630d).
  - Fix Mask Brush gradient artifacts
    [40e2f4469a](https://projects.blender.org/blender/blender/commit/40e2f4469a7).

### November 25 - November 29

This week I started working on the tools that use the Sculpt Face Groups
and Vertex Colors to have everything ready when the two main patches are
accepted.

**Next week**

  - Continue working on 2.82 projects
  - Rebase the Face Groups patch on top of the overlay engine

**Patches**

  - [D6346](https://developer.blender.org/D6346) Fix Sculpt Mask Overlay
    drawing
  - [D6341](https://developer.blender.org/D6341) Fix Mask Brush gradient
    artifacts
  - [D6339](https://developer.blender.org/D6339) Fix T72092: Clay Strips
    Brush stroke crash with Brush Detail Size
  - [D6333](https://developer.blender.org/D6333) Fix T72054: Sculpt Mode
    crash when using Relax Mesh Filter with Dyntopo enabled
  - [D6326](https://developer.blender.org/D6326) Fix T72006: Clay brush
    with size pressure corrupts mesh
  - [D6324](https://developer.blender.org/D6324) Sculpt: Sculpt plane
    cursor preview

**Commits**

  - Clay Brush: Input curves and update defaults
    [9e3006e507](https://projects.blender.org/blender/blender/commit/9e3006e5079).
  - Sculpt: Sample Voxel Size
    [47645a8db6](https://projects.blender.org/blender/blender/commit/47645a8db62).
  - Sculpt/Paint: Remove Unified SIZE\_PRESSURE and ALPHA\_PRESSURE
    [9251b07720](https://projects.blender.org/blender/blender/commit/9251b077203).
  - Sculpt/Paint: Move Elastic Deform Kelvinlets to BKE
    [0e4014ef92](https://projects.blender.org/blender/blender/commit/0e4014ef921).
  - Fix T71667: Use the Sculpt Mesh API in the Multires Smooth brush
    [b6d436ae28](https://projects.blender.org/blender/blender/commit/b6d436ae283).
  - Fix T71868: Clay Strips brush collapsed geometry
    [e85ab2cdc2](https://projects.blender.org/blender/blender/commit/e85ab2cdc2a).
  - Fix typo in versiong code
    [c9e7d8030f](https://projects.blender.org/blender/blender/commit/c9e7d8030f5)

### November 18 - November 22

This week I did more work in 2.82 sculpt mode project and I started
committing them to master. I rebased and finished the initial
implementation for both the Sculpt vertex colors and the Face groups
patches.

**Next week**

  - Continue working on 2.82 projects
  - Rename Face Maps to Face Groups in the new visibility system

**Patches**

  - [D6299](https://developer.blender.org/D6299) Fix T71868: Clay Strips
    brush collapsed geometry
  - [D6298](https://developer.blender.org/D6298) Clay Brush: Input
    curves and update defaults
  - [D6291](https://developer.blender.org/D6291) Sculpt/Paint: Remove
    Unified SIZE\_PRESSURE and ALPHA\_PRESSURE
  - [D6281](https://developer.blender.org/D6281) Sculpt/Paint: Move
    Elastic Deform Kelvinlets to BKE
  - [D6277](https://developer.blender.org/D6277) Fix T71667: Use the
    Sculpt Mesh API in the Multires Smooth brush

**Commits**

  - Paint: Smoother curve preset
    [71ddcf1a08](https://projects.blender.org/blender/blender/commit/71ddcf1a088).
  - Fix Windows Build
    [f903835370](https://projects.blender.org/blender/blender/commit/f903835370b).
  - Sculpt: Invert Scrape to Fill
    [a482d940bc](https://projects.blender.org/blender/blender/commit/a482d940bc1).
  - Sculpt: Topology Slide/Relax
    [a47f694b86](https://projects.blender.org/blender/blender/commit/a47f694b865).
  - Pose brush: Smooth Iterations Brush Property
    [186bd1759f](https://projects.blender.org/blender/blender/commit/186bd1759f0).
  - Sculpt: Mask Slice
    [470fe59f7e](https://projects.blender.org/blender/blender/commit/470fe59f7e8).
  - Sculpt: Multiplane Scrape Brush
    [c3279be222](https://projects.blender.org/blender/blender/commit/c3279be222b).
  - Sculpt: Enable symmetrize operator with Dyntopo disabled
    [6c9be3b27c](https://projects.blender.org/blender/blender/commit/6c9be3b27ce).
  - Sculpt: New Clay Brush
    [316107d4cd](https://projects.blender.org/blender/blender/commit/316107d4cd1).
  - Clay Strips: Input pressure curve and new defaults
    [c7ade50dee](https://projects.blender.org/blender/blender/commit/c7ade50deee).
  - Sculpt/Paint: Dash Ratio and Dash Samples
    [15f82278d5](https://projects.blender.org/blender/blender/commit/15f82278d5d).

**Other Projects**

  - I tried to refactor the clay brush to fit all work possible into a
    single multithreaded task, but I didn't notice any performance
    improvement. In my computer the sculpt mode is still faster when
    multithreading is disabled, so maybe I should try to fix that first.

### November 11 - November 15

I continued working on 2.82 sculpt mode projects, fixes and
optimizations. I also updated most of the sculpting patches that were on
review. I'm waiting to commit them until the release of 2.81 to avoid
merge conflicts in sculpt.c

**Next week**

  - Continue working on 2.82 projects
  - Commit all 2.82 to master if 2.81 releases

**Patches**

  - [D6269](https://developer.blender.org/D6269) Sculpt: Delay Viewport
    Updates
  - [D6266](https://developer.blender.org/D6266) Workbench: X-Ray
    Fresnel
  - [D6251](https://developer.blender.org/D6251) Sculpt: Sample Voxel
    Size
  - [D6238](https://developer.blender.org/D6238) Sculpt: Clay Thumb
    Brush
  - [D6228](https://developer.blender.org/D6228) Sculpt: New Clay Brush

**Commits**

  - Sculpt: Sculpt template defaults
    [9bd0d8b550](https://projects.blender.org/blender/blender/commit/9bd0d8b5505).

### November 4 - November 8

I spent most of the week testing 2.81 before the release and making
changes to the default brushes. The IK pose brush is almost there
(symmetry is not that easy to do).

**Next week**

  - Prepare things for the 2.81 release

**Patches**

  - [D6219](https://developer.blender.org/D6219) Fix T71434: Rebuild
    PBVH only on sculpt geometry changes
  - [D6214](https://developer.blender.org/D6214) Clay Strips: Input
    pressure curve and new defaults
  - [D6209](https://developer.blender.org/D6209) Paint: Use float for
    brush size
  - [D6207](https://developer.blender.org/D6207) Fix T71373: Sculpt Mask
    not extracting correctly on scaled objects
  - [D6194](https://developer.blender.org/D6194) Sculpt: Use more
    saturated colors in the cursor

**Commits**

  - Fix 2D paint antialiasing offset
    [a58c1d4497](https://projects.blender.org/blender/blender/commit/a58c1d4497d).
  - Fix Voxel Remesher preserve volume artifacts
    [265295e6a6](https://projects.blender.org/blender/blender/commit/265295e6a6c).
  - Clay Strips: Set default normal radius to 1.55
    [9b944e530e](https://projects.blender.org/blender/blender/commit/9b944e530ea).
  - Fix T70687: Deleting sculpting mask extraction causes crash
    [0bd51f4fc0](https://projects.blender.org/blender/blender/commit/0bd51f4fc09).
  - Paint: Option to disable antialiasing
    [cd827194f7](https://projects.blender.org/blender/blender/commit/cd827194f7c).
  - Fix EEVEE sculpt mask rendering
    [be2bdaf6aa](https://projects.blender.org/blender/blender/commit/be2bdaf6aab).
  - Fix T70866: Missing PBVH updates after mask undo
    [90fd75c869](https://projects.blender.org/blender/blender/commit/90fd75c869c).

### October 28 - November 1

This week I continued implementing 2.82 sculpt mode planned features and
fixing the remaining bugs in 2.81. At this point, almost all features
planned for 2.82 that don't have dependencies on other projects are
implemented (I think the only one missing is the symmetry implementation
in the pose brush with IK).

**Next week**

  - Prepare things for the 2.81 release
  - Finish the IK Pose brush
  - Investigate texture rake deformation

**Patches**

  - [D6186](https://developer.blender.org/D6186) Fix 2D paint
    antialiasing offset
  - [D6180](https://developer.blender.org/D6180) Sculpt: Enable
    symmetrize operator with Dyntopo disabled
  - [D6176](https://developer.blender.org/D6176) Fix Voxel Remesher
    preserve volume artifacts
  - [D6174](https://developer.blender.org/D6174) Sculpt: Multiplane
    Scrape Brush
  - [D6165](https://developer.blender.org/D6165) Sculpt: Clay Strips
    Roundness property
  - [D6163](https://developer.blender.org/D6163) Elastic Deform:
    Integrate Grab Kelvinlets
  - [D6160](https://developer.blender.org/D6160) Sculpt: Mask Slice
  - [D6158](https://developer.blender.org/D6158) Sculpt: QuadriFlow
    Guides
  - [D6157](https://developer.blender.org/D6157) Pose brush: Smooth
    Iterations Brush Property
  - [D6156](https://developer.blender.org/D6156) Sculpt: Texture mesh
    filter

**Commits**

  - Fix T71053: Poly Build tool crashes blender when deleting wire
    vertices
    [c6180c2249](https://projects.blender.org/blender/blender/commit/c6180c2249e).
  - Fix EEVEE sculpt mask rendering
    [be2bdaf6aa](https://projects.blender.org/blender/blender/commit/be2bdaf6aab).
  - Fix EEVEE sculpt mask rendering
    [be2bdaf6aa](https://projects.blender.org/blender/blender/commit/be2bdaf6aab).
  - Fix T70687: Deleting sculpting mask extraction causes crash
    [0bd51f4fc0](https://projects.blender.org/blender/blender/commit/0bd51f4fc09).

### October 21 - 25

**Blender Conference**

### October 14 - 18

This week I finished two of the main patches for 2.82 sculpt mode, the
topology slide/relax tools and the sculpt face maps.

**Next week**

  - Blender Conference

**Patches**

  - [D6088](https://developer.blender.org/D6088) Fix T70866: Missing
    PBVH updates after mask undo
  - [D6080](https://developer.blender.org/D6080) Fix EEVEE sculpt mask
    rendering
  - [D6070](https://developer.blender.org/D6070) Sculpt Facemap
  - [D6059](https://developer.blender.org/D6059) Sculpt: Topology
    Slide/Relax

**Commits**

  - Fix T70790: Crash in sculpt mode switching from two meshes after
    reload saved file
    [970d7ed860](https://projects.blender.org/blender/blender/commit/970d7ed860f).
  - Fix T70839: Sculpt brushes stop affecting after using move, rotate
    or scale tools
    [082fb6603d](https://projects.blender.org/blender/blender/commit/082fb6603d5).

**Other Projects**

  - I updated the sculpt vertex colors patch to support all new brush
    parameters introduced for texture paint
  - I made 2 demo files for the 2.81 release.

### October 7 - 11

This week I started coding the final patches for 2.82 sculpt mode. I
hope to have all bigger features ready next week to make a demo at the
Blender Conference.

**Next week**

  - Finish the 2.82 sculpt mode bigger features
  - Fix new bugs that may appear in the tracker for 2.81

**Patches**

  - [D6044](https://developer.blender.org/D6044) Paint: Option to
    disable antialiasing
  - [D6042](https://developer.blender.org/D6042) Fix T70687: Deleting
    sculpting mask extraction causes crash
  - [D6022](https://developer.blender.org/D6022) Sculpt: Invert Scrape
    to Fill
  - [D6020](https://developer.blender.org/D6020) Fix T70636: Detect
    resize direction based on initial mouse movement
  - [D6011](https://developer.blender.org/D6011) Enable normal radius
    fix for all brushes

**Commits**

  - Sculpt: Fix wireframe drawing
    [d95bb087d0](https://projects.blender.org/blender/blender/commit/d95bb087d03).
  - Fix T70544: Mesh extracted from Mask crash Blender when using
    Dyntopo
    [a630e46a58](https://projects.blender.org/blender/blender/commit/a630e46a582).
  - Fix multires cursor not displaying the active vertex
    [e79fc33fda](https://projects.blender.org/blender/blender/commit/e79fc33fda7).
  - Fix T70554: Snake Hook + Ctrl does not set the brush stroke in its
    normal direction
    [06c5520bda](https://projects.blender.org/blender/blender/commit/06c5520bda4).
  - Sculpt: Fix projection artifacts by changing the voxel remesh
    isovalue
    [abc36cad83](https://projects.blender.org/blender/blender/commit/abc36cad833).
  - Paint: 2D paint brush stroke antialiasing
    [ce22efb425](https://projects.blender.org/blender/blender/commit/ce22efb4258).
  - Sculpt/Paint: Invert curve alpha overlay
    [525a8bdf1d](https://projects.blender.org/blender/blender/commit/525a8bdf1dd).
  - Sculpt: Update only modified grids on multires undo
    [afb9bdc320](https://projects.blender.org/blender/blender/commit/afb9bdc320a).
  - Fix T70387: Sculpting with Original Normal rips edges
    [29707e3437](https://projects.blender.org/blender/blender/commit/29707e34376).

### September 30 - October 4

2.81 sculpt mode is ready. This week I did the last tweaks, bug fixes
paches and performance optimizations. Next week I can start with new
projects.

**Next week**

  - Start planning the 2.82 projects
  - Fix new bugs that may appear in the tracker

**Patches**

  - [D5998](https://developer.blender.org/D5998) Sculpt/Paint: Invert
    curve alpha overlay
  - [D5996](https://developer.blender.org/D5996) Sculpt: Change default
    min\_iter\_per\_thread
  - [D5994](https://developer.blender.org/D5994) Sculpt: Update only
    modified grids on multires undo
  - [D5993](https://developer.blender.org/D5993) Sculpt: Split normal
    radius and area radius
  - [D5982](https://developer.blender.org/D5982) Fix T70387: Sculpting
    with Original Normal rips edges
  - [D5975](https://developer.blender.org/D5975) Sculpt Vertex Colors
  - [D5960](https://developer.blender.org/D5960) Sculpt: Vertex group to
    mask conversion operators
  - [D5949](https://developer.blender.org/D5949) Sculpt/Paint: Dash
    Ratio and Dash Samples
  - [D5938](https://developer.blender.org/D5938) Fix T70342: QuadriFlow
    with the new symmetry option creates holes at the center

**Commits**

  - Sculpt: Clay Strips brush tweaks
    [d590db8370](https://projects.blender.org/blender/blender/commit/d590db83705).
  - Fix T70499: Can't change Brush Texture Rotation without holding the
    Shift key.
    [2b55a1da50](https://projects.blender.org/blender/blender/commit/2b55a1da50d).
  - Fix sculpt normal update in SculptDraw brush
    [67f9b94291](https://projects.blender.org/blender/blender/commit/67f9b94291c).
  - Fix T70299: Grab brush not working as expected when Ctrl is press
    [3bbe01dad3](https://projects.blender.org/blender/blender/commit/3bbe01dad33).
  - Fix T66312: Undo does not restore normals correctly in sculpt mode
    [ae7bea265d](https://projects.blender.org/blender/blender/commit/ae7bea265d0).
  - Sculpt: Disable remesh operators with Multires
    [6db6267d85](https://projects.blender.org/blender/blender/commit/6db6267d85e).
  - Fix T69227: Ignore inbetween mouse events in Snake Hook
    [d6c440bd7a](https://projects.blender.org/blender/blender/commit/d6c440bd7ad).
  - Fix T70102: Mask Extract bad solution
    [8a1f4fc8a7](https://projects.blender.org/blender/blender/commit/8a1f4fc8a78).
  - Use PBVH\_FullyMasked flag in mesh filter
    [a1f16ba67f](https://projects.blender.org/blender/blender/commit/a1f16ba67fc).
  - Fix T70358: Use face count in Quadriflow by default
    [3052229264](https://projects.blender.org/blender/blender/commit/3052229264c).
  - Fix T70310: Difficult to change brush size from big to small
    [606af693fb](https://projects.blender.org/blender/blender/commit/606af693fb5).
  - Fix T70324: Layer Brush has bad behaviours and create artifacts
    [29f25da769](https://projects.blender.org/blender/blender/commit/29f25da7692).
  - Fix default cursor blue color
    [fb62c578bc](https://projects.blender.org/blender/blender/commit/fb62c578bc0).
  - Fix bug in nearest\_vertex\_get\_finalize
    [ed59c32b0c](https://projects.blender.org/blender/blender/commit/ed59c32b0c4).
  - PBVH: PBVH\_FullyMasked and PBVH\_FullyUnmasked flags
    [e8f6f70846](https://projects.blender.org/blender/blender/commit/e8f6f70846e).
  - Fix T70385: Pose brush breaks when using pose origin offset
    [3dc027add3](https://projects.blender.org/blender/blender/commit/3dc027add39).
  - Fix PBVH search callback in pose and elastic deform
    [61efeb6df9](https://projects.blender.org/blender/blender/commit/61efeb6df98).
  - Sculpt: Only redraw nodes where the mask changed
    [6b419c18b0](https://projects.blender.org/blender/blender/commit/6b419c18b05).

**Other projects**

  - I made the first version of the Pose Brush with inverse kinematics.
    The patch is almost ready.
  - The sculpt vertex colors patch is on review. We need to agree on
    what features and changes should include the first patch of this
    feature.
  - I added brush stroke antialiasing to the 2D paint line fixes patch.

### September 23 - 27

Sculpt mode for 2.81 is almost finished. Almost all UI/UX changes are in
master. Next weeks I will focus on performance I will start designing
the 2.82 features.

**Next week**

  - Sculpt mode performance
  - Multilayer/multichannel painting design task
  - Fix bugs

**Patches**

  - [D5935](https://developer.blender.org/D5935) PBVH: PBVH\_FullyMasked
    and PBVH\_FullyUnmasked flags
  - [D5934](https://developer.blender.org/D5934) Fix T70324: Layer Brush
    has bad behaviours and create artifacts
  - [D5933](https://developer.blender.org/D5933) Fix T70326: Some bad
    behaviours with Voxel Remesh
  - [D5931](https://developer.blender.org/D5931) Fix T70310: Difficult
    to change brush size from big to small
  - [D5929](https://developer.blender.org/D5929) Sculpt: Update bounding
    boxes only on stroke done
  - [D5927](https://developer.blender.org/D5927) Sculpt: Enable pinch in
    draw sharp
  - [D5923](https://developer.blender.org/D5923) Sculpt: Only redraw
    nodes where the mask changed
  - [D5920](https://developer.blender.org/D5920) Fix T70299: Grab brush
    not working as expected when Ctrl is press
  - [D5895](https://developer.blender.org/D5895) Fix T66312: Undo don't
    restor proper geometry in sculpt mode
  - [D5871](https://developer.blender.org/D5871) Fix topbar padding to
    fit the tool icons

**Commits**

  - Sculpt: Sculpt cursor UX improvements
    [be985bdde2](https://projects.blender.org/blender/blender/commit/be985bdde27).
  - Voxel remesh: Enable adaptivity
    [c372318165](https://projects.blender.org/blender/blender/commit/c372318165c).
  - Sculpt: Use func\_finalize instead of mutex
    [82136bb36e](https://projects.blender.org/blender/blender/commit/82136bb36ef).
  - Sculpt: Pose brush origin offset
    [6a74b7c14b](https://projects.blender.org/blender/blender/commit/6a74b7c14b3).
  - Fix T70291: Sculpt remesh inverts normal when fix poles is activated
    [7ae549448e](https://projects.blender.org/blender/blender/commit/7ae549448ea).
  - Fix T56497: Snake hook sculpt brush slips off and affects the part
    of the mesh behind
    [9df9fb3324](https://projects.blender.org/blender/blender/commit/9df9fb33244).
  - Fix T70280: QuadriFlow remesh with "Use Paint Symmetry" and "Smooth
    Normals" may give wrong normals
    [97f4d4c66e](https://projects.blender.org/blender/blender/commit/97f4d4c66eb).
  - Sculpt: Enable accumulate in scrape brush
    [2bef8c5ea8](https://projects.blender.org/blender/blender/commit/2bef8c5ea8b).
  - Fix accumulate in Draw Sharp brush
    [a933237d68](https://projects.blender.org/blender/blender/commit/a933237d687).
  - Quadriflow: Symmetry support
    [e5c9bf3aa2](https://projects.blender.org/blender/blender/commit/e5c9bf3aa28).
  - Voxel Remesh: Fix poles and preserve volume
    [454c1a5de4](https://projects.blender.org/blender/blender/commit/454c1a5de4c).
  - Sculpt: Brush default settings
    [a0d4c2e607](https://projects.blender.org/blender/blender/commit/a0d4c2e607f).
  - Sculpt: UI fixes and improvements
    [7c544626b7](https://projects.blender.org/blender/blender/commit/7c544626b71).
  - Fix T70140: Topology Automasking and 2D Falloff don't work correctly
    together
    [aea8c0102a](https://projects.blender.org/blender/blender/commit/aea8c0102ac).
  - Fix T69875: Sculpt Scene Spacing breaks with Adjust Strength for
    Spacing enabled
    [a9d3e95bea](https://projects.blender.org/blender/blender/commit/a9d3e95bea0).
  - Fix crash in dynamic mesh preview edge list update
    [a7904dff4b](https://projects.blender.org/blender/blender/commit/a7904dff4be).

**Other projects**

  - I did some performance experiments to make the sculpt tools update
    only clipped nodes when previewing the result. This still needs a
    final implementation.

### September 16 - 20

This week I worked on improvements for some of the sculpt tools,
operators and UI. Patches to fix the volume preservation and symmetry in
the remeshers are ready to review, which were the most important issues
we have in the behaviour of the new features.

**Next week**

  - Continue fixing bugs in the tools
  - Continue working on the UI/UX of sculpt mode

**Patches**

  - [D5863](https://developer.blender.org/D5863) Voxel remesh: Fix poles
    and preserve volume
  - [D5855](https://developer.blender.org/D5855) Quadriflow: Symmetry
    support
  - [D5849](https://developer.blender.org/D5849) Fix T69875: Sculpt
    Scene Spacing breaks with Adjust Strength for Spacing enabled
  - [D5848](https://developer.blender.org/D5848) Fix T70020: Sculpt mode
    transform tools fail with cursor orientation
  - [D5846](https://developer.blender.org/D5846) Sculpt: Separate option
    for dynamic mesh preview
  - [D5841](https://developer.blender.org/D5841) Sculpt: Pose brush
    origin offset
  - [D5833](https://developer.blender.org/D5833) Paint: 2D paint line
    improvements
  - [D5826](https://developer.blender.org/D5826) Sculpt: Enable
    accumulate in scrape brush
  - [D5824](https://developer.blender.org/D5824) Fix crash in dynamic
    mesh preview edge list update
  - [D5814](https://developer.blender.org/D5814) Fix accumulate in draw
    sharp brush
  - [D5813](https://developer.blender.org/D5813) Sculpt: Brush default
    settings

**Commits**

  - Fix remesher operator poll function
    [85acd72c9b](https://projects.blender.org/blender/blender/commit/85acd72c9be).
  - Fix T69580: Smooth brush freezes on highpoly mesh
    [a1318d2415](https://projects.blender.org/blender/blender/commit/a1318d24155).
  - Fix T69984: Cursor curve Alpha doesn't display curves other than
    Custom
    [16a384b485](https://projects.blender.org/blender/blender/commit/16a384b4855).
  - Sculpt: Split original normal into original normal and plane
    [04843d1572](https://projects.blender.org/blender/blender/commit/04843d1572f).
  - Fix enabled dynamic size in pose brush
    [fb39f1776f](https://projects.blender.org/blender/blender/commit/fb39f1776f6).
  - Sculpt: Rename pinch to magnify in some brushes
    [0927e7b835](https://projects.blender.org/blender/blender/commit/0927e7b8355).
  - Fix topology automasking when starting from a boundary vertex
    [54b690897e](https://projects.blender.org/blender/blender/commit/54b690897ec).
  - Sculpt: Add pose origin preview to the Pose Brush
    [58214ab52a](https://projects.blender.org/blender/blender/commit/58214ab52a6).

### September 9 - 13

All sculpting features of the sculpt branch are now in master. I spent
the week updating the patches, fixing bugs and doing some UX
improvements to the new tools.

**Next week**

  - Continue fixing bugs in the tools
  - Work on the UI/UX of sculpt mode

**Patches**

  - [D5789](https://developer.blender.org/D5789) Fix T69747: Texture
    Paint: alpha not used in gradient
  - [D5761](https://developer.blender.org/D5761) Sculpt: add pose origin
    preview to the pose brush
  - [D5792](https://developer.blender.org/D5792) Cursor color theme
    setting
  - [D5791](https://developer.blender.org/D5791) Show the number of
    verts & faces for selected objects in object mode
  - [D5780](https://developer.blender.org/D5780) Fix assert when
    canceling a sculpt mode transform operation

**Commits**

  - Fix T69816: Using Transform gizmos in Sculpt Mode while MultiRes is
    activated crashes Blender
    [27b82bbb75](https://projects.blender.org/blender/blender/commit/27b82bbb75c).
  - Fix T69804: Transform tools in sculpt mode fails with transformed
    objects
    [f71d89bb04](https://projects.blender.org/blender/blender/commit/f71d89bb047).
  - Fix T69737: Crash using the Elastic Deform brush and Compressibility
    [70a9347705](https://projects.blender.org/blender/blender/commit/70a93477052).
  - Fix T69722: Pose brush crashes with multires modifier
    [d84d49280b](https://projects.blender.org/blender/blender/commit/d84d49280bd).
  - Sculpt: Transform tool
    [309cd047ef](https://projects.blender.org/blender/blender/commit/309cd047ef4).
  - Fix T69729: Missing Direction buttons in Draw Sharp brush
    [fa12428ede](https://projects.blender.org/blender/blender/commit/fa12428ede9).
  - Fix T69723: Crash with mask expand operator while in rendered or
    material view
    [841df2b98e](https://projects.blender.org/blender/blender/commit/841df2b98e7).
  - Sculpt: Grab Active Vertex and Dynamic Mesh Preview
    [a3e7440cfd](https://projects.blender.org/blender/blender/commit/a3e7440cfdd).
  - Fix warning and naming in mask expand
    [e0f7ada0d2](https://projects.blender.org/blender/blender/commit/e0f7ada0d2b).
  - Sculpt: Mask Extract operator
    [cfb3011e52](https://projects.blender.org/blender/blender/commit/cfb3011e521).
  - Sculpt: Mask Expand operator
    [0083c96125](https://projects.blender.org/blender/blender/commit/0083c961259).
  - Sculpt: Pose Brush
    [bfbee87831](https://projects.blender.org/blender/blender/commit/bfbee878313).
  - Sculpt: Topology automasking
    [2b2739724e](https://projects.blender.org/blender/blender/commit/2b2739724e6).
  - Sculpt: Mask Filter and Dirty Mask generator
    [13206a6dc0](https://projects.blender.org/blender/blender/commit/13206a6dc04).
  - Sculpt: Mesh Filter Tool
    [6a4df70d41](https://projects.blender.org/blender/blender/commit/6a4df70d411).
  - Sculpt: Elastic Deform Brush
    [70c1aaf59b](https://projects.blender.org/blender/blender/commit/70c1aaf59ba).
  - Sculpt: Draw Sharp Brush
    [70991bfd94](https://projects.blender.org/blender/blender/commit/70991bfd94a).

### September 2 - 6

This week I updated all sculpt branch patches following the revision.
Most of them are now ready for master. I also started looking at the 2D
texture painting code and I did some initial improvements there as it is
the next area I want to focus on.

**Next week**

  - Rebase and commit the sculpt branch patches
  - Fix bugs in the sculpt branch tools
  - Depending on the number of bugs, improve the 2D painting stabilizer

**Patches**

  - [D5717](https://developer.blender.org/D5717): Sculpt: Transform tool
  - [D5697](https://developer.blender.org/D5697): Texture paint: 2D
    painting improvements
  - [D5676](https://developer.blender.org/D5676): Paint: Stroke step
    queue
  - [D5658](https://developer.blender.org/D5658): Edit Mesh: loop at
    cursor position - WIP

**Commits**

  - Fix T69548: Sculpt scene spacing breaks when object pivot not at
    origin
    [6d8a86c07d5f](https://developer.blender.org/rB6d8a86c07d5f5c290a9e606a7e03dccac9ddcf43)

**Other projects**

  - I started doing some experiments to improve the 2D painting
    stabilizer
  - Add scaling support to the sculpt mode transform tool

### August 26 - 30

All patches from the sculpt branch that I would like to include in 2.81
are now ready to review except for the transform tool. The sculpt
related part of that tool is working fine in the sculpt branch, but the
gizmo/pivot point part needs to be reworked. Maybe it's easier now that
we have the transform origins option.

**Next week**

  - Continue fixing the patches for the sculpt branch
  - Sculpt: transform tool
  - Edit mesh: loop at cursor position

**Patches**

  - [D5384](https://developer.blender.org/D5384): Sculpt: Mask extract
    operator
  - [D5634](https://developer.blender.org/D5634): Sculpt: Regularized
    Kelvinlets brushes
  - [D5645](https://developer.blender.org/D5645): Sculpt: Topology
    automasking
  - [D5646](https://developer.blender.org/D5646): Sculpt: Grab active
    vertex
  - [D5647](https://developer.blender.org/D5647): Sculpt: Pose brush
  - [D5657](https://developer.blender.org/D5657): Sculpt: Mask expand
    operator

**Commits**

  - Sculpt/Paint: Brush world spacing option for sculpt mode
    [87cafe92ce2f](https://developer.blender.org/rBS87cafe92ce2f99d8da620b80e1c26f8078554f93)
  - Sculpt: new brush cursor, active vertex and normal radius
    [e0c792135adf](https://developer.blender.org/rBe0c792135adf8de3e6a54ddcbab53cab95b2b019)
  - Edit Mesh: Poly build tool improvements
    [d8baafd693eb](https://developer.blender.org/rBSd8baafd693ebf830d6153bd31bd63521c7569984)
  - Fix T68763: Smooth Brush not working in LookDev or Rendered Mode
    [8e4f3b2bb070](https://developer.blender.org/rBS8e4f3b2bb070a5022200c43866e81b6d5b8d3d1a)
  - Fix Poly Build crash with empty meshes
    [d5ed3de65451](https://developer.blender.org/rBSd5ed3de654513f6a53d0db79e912ba179e2833c3)

**Other projects**

  - Sculpt branch: fix pose brush with multiple symmetry axis active,
    add radius preview.
  - Merge mask by normal and mask expand into a single operator
