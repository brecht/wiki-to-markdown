## Kevin Curry (KevinCurry\_Unity)

#### Contact

  -   
    **blender.chat:**
    [KevinCurry\_Unity](https://blender.chat/direct/KevinCurry_Unity)
    **developer.blender.org:** [/
    KevinCurry\_Unity](https://developer.blender.org/p/KevinCurry_Unity)
    **devtalk.blender.org:** [/
    KevinCurry\_Unity](https://devtalk.blender.org/u/KevinCurry_Unity)
    **Twitter:** [@kmcurry](https://twitter.com/kmcurry) (not affiliated
    with Unity)

#### About me

Working as a Senior Engineering Manager at Unity. U.S. expat working in
Dublin. I've spent most of my career on 3D on the web and how to make it
easier for people to create and share 3D and immersive content.
