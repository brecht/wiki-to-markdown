## December 21 - January 10

Non-working weeks.

## December 7 - 18

Attribute license document and geometry nodes.

  - [Licensing](https://developer.blender.org/D9791)
  - [Tuesday
    Talks](https://devtalk.blender.org/t/2020-12-08-tuesday-talks-module-meetings/16526)
  - Finished the [attribution requirements
    doc](https://developer.blender.org/D9798)
  - Initial review of the [poisson
    distribution](https://developer.blender.org/D9787)
  - Finished and committed the point distribution patch port (thank you
    Jacques) - original patch by Parborg
  - Reviewed Chis posts (top contributors and new projects)
  - geometry-nodes topics (sprint retrospective, make sure things are
    clear for the few that continue the next 2 weeks, some tasks review)
  - Give feedback for Clément about his pyGPU proposal

## November 30 - December 4

  - [Tuesday
    Talks](https://devtalk.blender.org/t/2020-12-01-tuesday-talks-module-meetings-ongoing/16396)
  - Finished the merge preparation for the geometry nodes branch.
  - Geometry nodes discussions:

`- Point cloud objects or not - will try to make it possible to ditch point cloud objects from 2.92.`  
`- Instancing/make real discussion - to resume tomorrow with Ton`  
`- Explore/define nodes with Simon for the leaves use case`

## November 23 - 27

  - New sample file (splashscreen) for download
  - Helped reviewing 2.91 press release
  - Polishing on [geometry nodes editor
    context](https://developer.blender.org/T82691) design
  - Tested 2.91 to find showstoppers
  - 2.91 steam wrap up (cropping images for the store)
  - Geometry nodes: finished with Simon and other the nodes for the tree
    and flower use case, and the tasks for the tree leaves (i.e.,
    particles edit mode) use case.

## November 16 - 20

Help coordinate 2.91 release, a bug fix, geometry-nodes sprint and
documentation for the nodes (still a long way to go).

![../../../images/Geometry-nodes.png](../../../images/Geometry-nodes.png
"../../../images/Geometry-nodes.png")

**Commits master:**

  - [256e77c987](https://projects.blender.org/blender/blender/commit/256e77c987d2)
    Fix T82833: GreasePencil crash when changing to Vertex Color mode.

**Commits geometry-nodes:**

  - [9524019d58](https://projects.blender.org/blender/blender/commit/9524019d584)
    Geometry Nodes: Rename category Scattering to Point and sort
  - [eb25446b9d](https://projects.blender.org/blender/blender/commit/eb25446b9d2)
    Geometry Nodes: Categories for the nodes
  - [0c7df205a5](https://projects.blender.org/blender/blender/commit/0c7df205a5d)
    Geometry Nodes: T82701 Name for initial Node Groups
  - [0f1fe96e0b](https://projects.blender.org/blender/blender/commit/0f1fe96e0b2)
    Geometry Nodes - Internal rename + API change: NODES for anything
    but UI
  - [083cde0b43](https://projects.blender.org/blender/blender/commit/083cde0b43f)
    Geometry Nodes: Unify icons -\> use ICON\_NODETREE for everything
  - [0783a9a194](https://projects.blender.org/blender/blender/commit/0783a9a1949)
    Cleanup: Remove all of "\#ifdef WITH\_POINT\_CLOUD"
  - [b05f841c3a](https://projects.blender.org/blender/blender/commit/b05f841c3a5)
    Cleanup: Remove most of "\#ifdef WITH\_GEOMETRY\_NODES"
  - [4db4177587](https://projects.blender.org/blender/blender/commit/4db4177587d)
    Remove Point Cloud object from experimental
  - [c0f3d31998](https://projects.blender.org/blender/blender/commit/c0f3d319988)
    Remove Geometry Nodes Editor from experimental

## November 9 - 13

No weekly report.

## November 2 - 6

  - [Article about the geometry nodes
    project](https://code.blender.org/?p=4661) (to be published on
    Monday).
  - Organized the design for the [Modifier
    Nodes](../../../Source/Nodes/Modifier_Nodes.md) project.
  - Re-organized [2.92 planned
    schedule](https://developer.blender.org/project/view/119/).
  - Found, reported and fixed a high priority bug.

**Commits:**

  - [b35e3f2460](https://projects.blender.org/blender/blender/commit/b35e3f24600)
    Fix T82457: Python error when clicking on a tool in the viewport

## October 26 - 30

**Commits:**

  - [e3395093ff](https://projects.blender.org/blender/blender/commit/e3395093fff)
    Geometry Nodes: Create the node group when adding a new Nodes
    modifier

## October 19 - 23

No progress on the projects proposal, but bcon3 went well (with new
splashscreen\!), and worked with others on the design of the scattering
system for the geometry nodes project.

![../../../images/Nodes.png](../../../images/Nodes.png
"../../../images/Nodes.png")

**Next week:** Help the modules to finish the [list of to-document
areas](https://devtalk.blender.org/t/2020-10-20-tuesday-talks-module-meetings/15848).
Prepare the backlog for the next-next week of geometry nodes. The
options of use case are:

  - More specific/production case for scattering
  - Tree moss - with parametric geometry
  - Camp fire - Sebastián started on that, maybe is worth prioritizing
    it then

## October 12 - 16

Focused a bit on high-priority issuse (fixed two of them), did an
initial brief of the particles design for the team, and work in the
proposal of projects organization with Francesco Siddi.

![../../../images/Projects.jpg](../../../images/Projects.jpg
"../../../images/Projects.jpg")

**Commits:**

  - [d48d8b3456](https://projects.blender.org/blender/blender/commit/d48d8b34567)
    Fix T81761: EEVEE enabled AO pass affects render result
  - [89ffdad0f3](https://projects.blender.org/blender/blender/commit/89ffdad0f35)
    Fix misuse of alloc inside a loop
  - [eb55ca72f4](https://projects.blender.org/blender/blender/commit/eb55ca72f4e)
    UI: Make node theme settings to conform to UI rules
  - [c866075dfb](https://projects.blender.org/blender/blender/commit/c866075dfbd)
    Fix T81580: No doversion for Emission Strength

**Next week**: Half the time on starting the geometry nodes work with
the squad. bcon3 coordination and advance in the projects proposal.

## September 14 - October 9

Blender structure organization is taking most of the time. Nothing to
report until we advance with the proposals to share with the community.

## August 31 - September 11

  - [Particles
    workshop](https://devtalk.blender.org/t/2020-09-03-particle-workshop/15172).

**Next week**: The main plain is to go over the LTS fixes and map them
to when they were introduced (\<2.83? 2.83 bcon1, bcon2, bcon3, bcon4?)
Still need to collect code design and engineer plans with Sergey. Need
to discuss the hair plan of attack with Sebbas.

## August 24 - 28

  - Helped coordinate 2.90 release (postponed to Monday though).
  - Added "Code Review" menu entries for all the modules.
  - Organized the upcoming [Tuesday
    Talks](https://lists.blender.org/pipermail/bf-committers/2020-August/050647.html).

**Next week**: Final 2.90 Release, First edition of Tuesday Talk,
roadmap updates, code design and engineering examples.

## August 17 - 21

Wrote the drafts for new articles on code.blender.org:

  - [Code Quality Day](https://code.blender.org/?p=4552&preview=true)
  - [Intel Embree](https://code.blender.org/?p=4539&preview=true)

**Next week**: 2.90 release mainly, specifically get the sample files
published.

## August 10 - 14

  - Update [2.91 page](https://developer.blender.org/project/view/117/)
    with latest dates (and added the sprints to the release documents).
  - A few discussions about sculpting (e.g.,
    [D8558](https://developer.blender.org/D8558))
  - Had a few design meetings about asset browser, and particles with
    Ton and Brecht (+ Julien and Bastien for the asset browser).

![../../../images/Asset-brower.png](../../../images/Asset-brower.png
"../../../images/Asset-brower.png")

**Next week**: Article with Sergey about code quality day. Possibly
article about EMBREE if Stefan can't do it.

## August 4 - 7

  - Worked on the preparation week for the release communication -
    contacted artists for artwork, did a final pass in big regressions
    (e.g., [\#79567](http://developer.blender.org/T79567)).
  - [Bf-usd mailing
    list](https://lists.blender.org/pipermail/bf-committers/2020-August/050636.html)
    created.
  - [VSE and Compositor stereoscopy
    fix](https://developer.blender.org/D8472)

![../../../images/3-post-fix.jpg](../../../images/3-post-fix.jpg
"../../../images/3-post-fix.jpg")

  - Trying to stremline the Monday meetings:

![../../../images/0-monday-meeting.jpg](../../../images/0-monday-meeting.jpg
"../../../images/0-monday-meeting.jpg")

## July 28 - 31

  - [Meeting about
    particles](https://devtalk.blender.org/t/2020-07-28-particle-nodes-status/14588)
  - [Meeting about property
    search](https://devtalk.blender.org/t/2020-07-28-property-search-status/14594)
  - Prepared Particles presentation material (and design polishing) for
    Monday.

![../../../images/Particles\_v1.jpg](../../../images/Particles_v1.jpg
"../../../images/Particles_v1.jpg")

**Next Week**: bug spring week and release communication week.

## July 21 - 24

  - CHAOSS/GrimoireLab setup for testing
  - Exercise on VSE user story mapping
  - [Failed attempt to help the Blender animation studio for collection
    origin display](https://developer.blender.org/D8382)

![../../../videos/Grimoirelab-blender-2020-07-23\_12.49.35.mp4](../../../videos/Grimoirelab-blender-2020-07-23_12.49.35.mp4
"../../../videos/Grimoirelab-blender-2020-07-23_12.49.35.mp4")

![../../../images/VSE.png](../../../images/VSE.png
"../../../images/VSE.png")

## June 30 - July 3

  - [LTS
    post-mortem](https://devtalk.blender.org/t/2020-06-29-blender-2-83-1-post-mortem/14098)
    with Pablo, Nathan and Jeroen.
  - [Bug sprint proposal](https://code.blender.org/2020/07/bug-sprints/)
    finalized and published.
  - Job interview for potential new DevOps & Operations.
  - New demo file: [Blender 2.83
    splashscreen](https://www.blender.org/download/demo-files/).

<!-- end list -->

  - **Next Week**: (July 20) catch up, meeting with
    [CHAOSS](https://chaoss.github.io/grimoirelab/) developer to quickly
    setup a dashboard for Blender (to test their system), focus on
    bcon3/beta.

## June 23 - 26

  - Helped the 2.83.1 LTS release
  - Worked on the upcoming job openings for Blender (developer community
    manager and DevOps & operations)
  - Reorganized the experimental features
  - Tested motion blur and property search

<!-- end list -->

  - **Next Week**: Better define the dashboard project implementation
    (to show recent graphs from phabricator - new bugs, ...). Finish the
    job openings.

<!-- end list -->

  - **Commits**

<!-- end list -->

  - [dd9c0dbf5e](https://projects.blender.org/blender/blender/commit/dd9c0dbf5ed)
    Experimental Features: Use different "categories" for different
    features

## June 15 - 19

  - Finished the [canned responses
    patch](https://developer.blender.org/D8034).
  - Worked with the module owners to get their roadmaps up to date and
    update their landing page.
  - Meetings about the future of the UI module, as well as the sculpting
    module.
  - New blogpost about the [modules
    roadmaps](https://code.blender.org/2020/06/modules-roadmap-june-2020).
  - Bonus: Presented 2020/2021 10 big projects [live-stream in
    Portuguese for
    Visgraf/IMPA](https://www.youtube.com/watch?v=udZoZ5j6T5o).

  
![../../../images/June-2020-roadmaps.png](../../../images/June-2020-roadmaps.png
"../../../images/June-2020-roadmaps.png")

![../../../videos/Canned-responses.mp4](../../../videos/Canned-responses.mp4
"../../../videos/Canned-responses.mp4")

![../../../images/Visgraf.jpg](../../../images/Visgraf.jpg
"../../../images/Visgraf.jpg")  

## June 9 - 12

[Blender 2.90
Roadmap](https://devtalk.blender.org/t/2020-06-09-blender-2-90-roadmap/13788)
meetings and write up, new modules organizations, and started
phabricator canned responses. The 2.90 roadmap was also posted on the
[2.90 project page](https://developer.blender.org/project/view/110/).

![../../../images/Nodes-physics.png](../../../images/Nodes-physics.png
"../../../images/Nodes-physics.png")

New modules organized:

  - [Sculpt, Paint &
    Texture](https://developer.blender.org/tag/sculpt_paint_texture/)
  - [Animation &
    Rigging](https://developer.blender.org/tag/animation_rigging/)
  - [Data, Assets &
    I/O](https://developer.blender.org/tag/data_assets_i_o/)
  - [Nodes & Physics](https://developer.blender.org/tag/nodes_physics/)

**Next Week**: Remaining module pages updates, follow up meetings for
modules (UI & Sculpt) and finish the canned responses.

## June 2 - 5

This week was mostly dedicated to the Blender 2.83 release. The
port-mortem meeting notes from part of the release team is coming later.
Also wrapped up the dailypoke discussions, [clarified the
LTS](https://lists.blender.org/pipermail/bf-committers/2020-June/050594.html).

**Next Week**: Roadmap meetings, and module pages updates.

## May 25 - 29

I could not find time to work in a dashboard proposal, but have been
focusing on the release (bcon changes, checking the tracker, testing old
files, ...). Besides investigating the stereoscopic issue with Clément
that in the end was reported upstream.

  - Wrap up bug fixing spring (graph, mini-report on weekly dev
    meeting).
  - Help to add new override bug/targets based on feedback by Tangent
    [\#73318](http://developer.blender.org/T73318)
  - bcon4 regressions hunting
    ([\#77173](http://developer.blender.org/T77173),
    [\#77167](http://developer.blender.org/T77167)).
  - General bcon4 coordination (check bugs, change bcon everywhere, ...)
  - New entry in the [bug triaging
    playbook](../../../Process/Bug_Reports/Triaging_Playbook.md) to
    include "Blender doesn't run / crashes" scenario

<!-- end list -->

  - **Commits**

<!-- end list -->

  - Fix T77105: Crashes when clicking "new scene" button
    [1c3b2b5dd8](https://projects.blender.org/blender/blender/commit/1c3b2b5dd8c).
  - Cleanup: Remove uneeded NULL checks on ed\_screen\_context
    [14b1c55e11](https://projects.blender.org/blender/blender/commit/14b1c55e110).
  - Revert "UI: View3D Cursor Changes"
    [219ce574b5](https://projects.blender.org/blender/blender/commit/219ce574b56).

**Next Week**: More Testing for release, decide on the daily-poke
future, and if possible some dashboard discussion.

## May 18 - 22

  - Helped refine and post the [LTS
    pilot](https://code.blender.org/2020/05/long-term-support-pilot/).
  - Tracker to help the bug sprint fortnight (fixing bugs, finding
    untagged, closing resolved reports).
  - Discussions on the UI process (result in [reverting the cursor
    change so it can be presented in a more comprehensive way
    in 2.90](https://developer.blender.org/rB219ce574b56585eb5db056983a8acf08f7ff9991)).

**Bug sprint progress graph**:
![../../../images/Bug-sprint.png](../../../images/Bug-sprint.png
"../../../images/Bug-sprint.png")

**Next Week**: Testing for release, help with at least an issue, if
possible some dashboard discussion.

## May 11 - 15

  - Published <https://code.blender.org/2020/05/tracker-curfew-wrap-up/>
  - Organizing the [Bug Fixing
    Sprint](https://lists.blender.org/pipermail/bf-committers/2020-May/050574.html).
  - Fixed manual Python API mapping of camera properties.
  - Triaging/closing fixed reports to help the bug fixing sprint.

**Next Week**: Decide on dashboard project, more help on bug fixing
sprint, probably move forward with the LTS discussion.

## May 4 - 8

  - Welcome students to Google Summer of Code, onboarding (wiki + git
    access).
  - Report on remote productivity.
  - Finished first pass for the tracker curfew metrics code.blender.org
    post.
  - Notes on UI feedback meeting on \#blendercoders from April 20th (not
    published, but to be used for meeting with UI team).

![../../../images/Report-1.jpg](../../../images/Report-1.jpg
"../../../images/Report-1.jpg")

## April 27 - May 1

  - Finished coordinating the Google Summer of Code slots assignment.
  - More work in metrics for developer.blender.org
    (https://gitlab.com/dfelinto/blender-metrics).
  - First pass of LTS proposal discussion with Ton and Francesco.
  - Draft for job position for development infrastructure.

**Work in Progress Graphs**:

![../../../images/High\_priority\_bugs.jpg](../../../images/High_priority_bugs.jpg
"../../../images/High_priority_bugs.jpg")

![../../../images/Unclassified\_reports.jpg](../../../images/Unclassified_reports.jpg
"../../../images/Unclassified_reports.jpg")

![../../../images/Bugs-per-priority.jpg](../../../images/Bugs-per-priority.jpg
"../../../images/Bugs-per-priority.jpg")

![../../../images/Bugs-per-priority-distribution.jpg](../../../images/Bugs-per-priority-distribution.jpg
"../../../images/Bugs-per-priority-distribution.jpg")

**Next Week**: Wrapping metrics with a post on code.blender.org, LTS
proposal and Google Summer of Docs application (couldn't do this week
again).

## April 20 - 24

Google Summer of Code (final slots assigned/selected); Working in
metrics for developer.blender.org; Meeting on [Infrastructure plan of
action](https://devtalk.blender.org/t/20-4-23-development-infrastructure-plan-of-attack/12778/7).

**Work in Progress Graphs**:

![../../../images/New\_Reports\_Daily.jpg](../../../images/New_Reports_Daily.jpg
"../../../images/New_Reports_Daily.jpg")
![../../../images/Open\_Tasks.jpg](../../../images/Open_Tasks.jpg
"../../../images/Open_Tasks.jpg")
![../../../images/Needs\_Developer\_Attention.jpg](../../../images/Needs_Developer_Attention.jpg
"../../../images/Needs_Developer_Attention.jpg")

**Next Week**: Wrapping metrics, LTS proposal and Google Summer of Docs
application (couldn't do this week).

## April 13 - 17

Google Summer of Code and working in metrics for phabricator. This
should help wrapping up the tracker curfew, assess where triaging and
will be the foundation for a dashboard in the future.

**Next Week**: Wrapping metrics, GSoC (final pass, sending slots
request, ...) and Google Summer of Docs application.

## April 6 - 10

Meeting about [Development Management -
Communication](https://devtalk.blender.org/t/20-4-8-development-management-communication/12577),
fixed a bug on VSE and stereoscopy and recorded the entire process.

Videos:

  - [VSE - Stereoscopy 3D bug
    fixing](https://youtube.com/watch?v=tQzKjf2_Hmk) (3h30)
  - [Dive into the code | Blender Everyday
    \#11](https://www.youtube.com/watch?v=tCdx7gzp0Ac)
  - [Developers Q\&A | BlenderToday
    \#98](https://www.youtube.com/watch?v=U1pqhZqF2c4&feature=youtu.be)

<!-- end list -->

  - **Commits**

<!-- end list -->

  - Fix T71546: VSE stereoscopic strips issues with mismatched
    dimensions
    [9c5b054206](https://projects.blender.org/blender/blender/commit/9c5b0542069).
  - Cleanup: VSE rename i to view\_id for stereo 3d code
    [07bb7206c2](https://projects.blender.org/blender/blender/commit/07bb7206c20).

**Next Week**: Wrap up the tracker curfew (and metrics from
phabricator).

## March 30 - April 3

Finished WBSO, meeting about [Modules and
Roles](https://devtalk.blender.org/t/20-3-31-modules-and-roles/12482),
GSoC proposals triaging, and code quality cleanup (LISTBASE\_FOREACH).

  - **Commits**

<!-- end list -->

  - Code Quality: Replace for loops with LISTBASE\_FOREACH
    [d138cbfb47](https://projects.blender.org/blender/blender/commit/d138cbfb47e).
  - Cleanup: Including "BLI\_listbase.h" for LISTBASE\_FOREACH macro
    [b0c1184875](https://projects.blender.org/blender/blender/commit/b0c1184875d).
  - Revert "Fix T72688: Vertex Group Weights in Edit Mode Occludes In
    Front Armatures"
    [fa8a3c8f26](https://projects.blender.org/blender/blender/commit/fa8a3c8f26f).

**Next Week:** More WBSO (meeting with consulting firm on Monday, it may
requires extra work afterwards); and want to think/propose a good way to
handle communication with bf-admins and eventually all the developers
(regarding decision making, ...).

## March 23 - 27

A lot of the week was taken by WBSO (subsidies form) and an on going
early discussion about the release management process and roles. The
final outcome of the release management proposal will still be made
public.

Next week: Finish WBSO, and wrap up the release management process with
all involved parties happy and go public about it.

## March 16 - 22

Worked with Sem Mulder on finishing up the blender metrics projects
(still needs work), started a table to organize the weekly work of every
developer.

Finished and committed the code-cleanup "SortedIncludes". Also spent
considerable time debugging and simplifying
[\#74984](http://developer.blender.org/T74984) for the studio.

Next week: Finish queries with Sem, WBSO with Francesco.

## March 9 - 15

Fixed some old-standing 2.8 stereo issues in the viewport. Final touches
and publishing the [asset manager
article](https://code.blender.org/2020/03/asset-manager/).

Next week: Organize online coordination, phabricator analize (queries,
...).

  - **Commits**

<!-- end list -->

  - Fix stereoscopy reference image drawing in the viewport
    [6bcb6a0ea6](https://projects.blender.org/blender/blender/commit/6bcb6a0ea6e).
  - Fix stereoscopy drawing for camera background
    [5593efec01](https://projects.blender.org/blender/blender/commit/5593efec01c).
  - Bug report preset update (last worked version)
    [16ed7aebf2](https://projects.blender.org/blender/blender/commit/16ed7aebf2b).
  - Fix T74516: Armature Crash on Select Similar Group
    [b198cef89f](https://projects.blender.org/blender/blender/commit/b198cef89fa).
  - Cleanup: Clarify places to look for subversion bump
    [dc8bb3f3a0](https://projects.blender.org/blender/blender/commit/dc8bb3f3a0b).

## March 2 - 6

Moved asset manager article to code.blender.org (unpublished), and did
an monthly delivery planning for the [10 big projects
of 2020](https://code.blender.org/2020/01/2020-blender-big-projects/).

## February 24 - 28

Article about asset manager (to be published), LTS discussions, and
overall testing of the pending branches for 2.83 (reported a few bugs in
greasepencil and volume branches.

## February 17 - 21

UI/UX Workshop, coordinating the coding day for the full-time triagers,
and organizing second phase of tracker curfew.

## February 10 - 14

Blender 2.82 release management, cleaned up sample files for the [demo
page](https://www.blender.org/download/demo-files/), and organized some
of the remaining curfew tasks (i.e., cleanup the non-bf\_blender tasks).

![../../../images/New-sample-files.jpg](../../../images/New-sample-files.jpg
"../../../images/New-sample-files.jpg")

## February 3 - 7

Worked mostly in the release logs.

## January 27 - 31

Published the big projects for 2020 in code.blender.org, finished the
[FOSDEM
presentation](https://fosdem.org/2020/schedule/event/gamedev_blender_projects/),
and started to think about the big topic for February: modules, the
roles in it, what to expect from developers, how to use devtalk
(subforums as work places), patch review process. The week was a bit
short for me due the the DoS attack on blender.org (need access to
developer.blender.org for my next two tasks).

  - **Links**

<!-- end list -->

  - [Blender Big Projects
    for 2020](https://code.blender.org/2020/01/2020-blender-big-projects/).
  - [FOSDEM
    slides](https://fosdem.org/2020/schedule/event/gamedev_blender_projects/attachments/slides/3688/export/events/attachments/gamedev_blender_projects/slides/3688/Fosdem_2020_Blender_projects_for_2020.odp)

![../../../images/Code-blender-org.jpg](../../../images/Code-blender-org.jpg
"../../../images/Code-blender-org.jpg")

![../../../images/Fosdem\_2020\_-\_Dalai.jpg](../../../images/Fosdem_2020_-_Dalai.jpg
"../../../images/Fosdem_2020_-_Dalai.jpg")

  - **Next Week**

Organize the code quality day (on Friday, but send the email and create
the tasks prior to that), formalize the ideas for modules and to what to
do with the backlog of patches (still think closing all pre-2.8 may be
ok, but need developer.blender.org for that), reserve some time for
meetings with Jacques Luck.

## January 20 - 24

Presented the 2020 big projects to Ton, then to the studio, then created
the tasks online (failed to find time to do the FOSDEM presentation).

![../../../images/2-blender-2020.png](../../../images/2-blender-2020.png
"../../../images/2-blender-2020.png")

![../../../images/Projects-2020.png](../../../images/Projects-2020.png
"../../../images/Projects-2020.png")

  - **New projects tasks**

<!-- end list -->

  - [\#73317](http://developer.blender.org/T73317) - Multires
  - [\#73318](http://developer.blender.org/T73318) - Library overrides
  - [\#73324](http://developer.blender.org/T73324) - Particles Nodes
  - [\#73201](http://developer.blender.org/T73201) - New volume object
    type
  - [\#68981](http://developer.blender.org/T68981) - New hair object
    type
  - [\#68908](http://developer.blender.org/T68908) - Faster animation
    playback
  - [\#73359](http://developer.blender.org/T73359) - Scene editing in
    object mode
  - [\#73360](http://developer.blender.org/T73360) - Fast highpoly mesh
    editing
  - [\#73363](http://developer.blender.org/T73363) - Alembic / USD
  - [\#73366](http://developer.blender.org/T73366) - Asset Manager
    “Basics”

<!-- end list -->

  - **Next Week**

Add the projects to their corresponding module pages, blog post about
the 2020 projects, final FOSDEM presentation.

## January 13 - 17

Planning for 2020 projects, draft of FOSDEM presentation, [devtalk
changes](https://lists.blender.org/pipermail/bf-committers/2020-January/050420.html),
and regular tracker/management work.

  - **Next Week**

Formalize and bring developers up to pair with the main 2020 projects
and finalize FOSDEM presentation.

## January 6 - 10

Synced the entire team regarding the tracker curfew, built an initial
graph system to keep track of the progress of the work.

<https://www.blender.org/get-involved/dashboard/>

  - **Next Week**

FOSDEM presentation, devtalk new scope proposal, finish up the graphs to
follow the curfew.
