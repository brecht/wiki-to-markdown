## MEM Debug Blender

``` diff
diff --git a/intern/guardedalloc/intern/mallocn_guarded_impl.c b/intern/guardedalloc/intern/mallocn_guarded_impl.c
index ef74f394fb1..2caf08b08a8 100644
--- a/intern/guardedalloc/intern/mallocn_guarded_impl.c
+++ b/intern/guardedalloc/intern/mallocn_guarded_impl.c
@@ -42,13 +42,13 @@
  * but this introduces some overhead to memory header and makes
  * things slower a bit, so better to keep disabled by default
  */
-//#define DEBUG_MEMDUPLINAME
+#define DEBUG_MEMDUPLINAME
 
 /* Only for debugging:
  * lets you count the allocations so as to find the allocator of unfreed memory
  * in situations where the leak is predictable */
 
-//#define DEBUG_MEMCOUNTER
+#define DEBUG_MEMCOUNTER
 
 /* Only for debugging:
  * defining DEBUG_THREADS will enable check whether memory manager
@@ -59,14 +59,14 @@
  * guarded allocator is not thread-safe, however this check will
  * fail to check allocations from openmp threads.
  */
-//#define DEBUG_THREADS
+#define DEBUG_THREADS
 
 /* Only for debugging:
  * Defining DEBUG_BACKTRACE will store a backtrace from where
  * memory block was allocated and print this trace for all
  * unfreed blocks.
  */
-//#define DEBUG_BACKTRACE
+#define DEBUG_BACKTRACE
 
 #ifdef DEBUG_BACKTRACE
 #  define BACKTRACE_SIZE 100
@@ -74,7 +74,7 @@
 
 #ifdef DEBUG_MEMCOUNTER
 /* set this to the value that isn't being freed */
-#  define DEBUG_MEMCOUNTER_ERROR_VAL 0
+#  define DEBUG_MEMCOUNTER_ERROR_VAL 82160
 static int _mallocn_count = 0;
 
 /* breakpoint here */
```
