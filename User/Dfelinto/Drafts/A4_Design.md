# A4 Design

  - Inspired by Toyota A3 process, the idea is to synthetize all the
    information relevant to a design proposal in a piece of A4 (or
    1920x1080).

<!-- end list -->

  - This prevents a more extensive presentation (e.g., slideshow) to get
    derailed because of small details.

<!-- end list -->

  - A good strategy is to organize the different sections in context,
    from the big picture to specific details.

## Examples

Those are a few examples of design documents that were presented to Ton
Roosendaal between 2021 and 2022.

### Spreadsheet Editor

Implemented in 2.93 LTS.

![../../../images/A4\_-\_Spreadsheet\_Presentation.jpg](../../../images/A4_-_Spreadsheet_Presentation.jpg
"../../../images/A4_-_Spreadsheet_Presentation.jpg")

### Asset Bundle

Proposal, for Blender 3.0.

![../../../images/A4\_document\_-\_Asset\_Bundle.jpg](../../../images/A4_document_-_Asset_Bundle.jpg
"../../../images/A4_document_-_Asset_Bundle.jpg")

### Hair

Incomplete A4 Design, in the end the information was better presented as
a more complete [blog
post](https://code.blender.org/2022/07/the-future-of-hair-grooming/).

![../../../images/A4\_-\_Layer\_Design.jpg](../../../images/A4_-_Layer_Design.jpg
"../../../images/A4_-_Layer_Design.jpg")
