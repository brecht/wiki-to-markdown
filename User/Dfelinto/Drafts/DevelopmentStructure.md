# Development Structure

*"Everyone should have all the information they need to work."* -
<sub>Ton Roosendaal, October 2019</sub>

The [modules](../../../Modules/index.md) should be empowered, the design steps
should be formalized and reinforced, and we should have a clear
definition of everyone's roles and its expectations for the entire team,
as well as contributors.

![../../../images/Blender-Development-Structure.png](../../../images/Blender-Development-Structure.png
"../../../images/Blender-Development-Structure.png")

There is a static part of the Blender development structure that is
responsible for everyday [patch
review](PatchReview.md), roadmaps, [bug
fix](../../../Process/A_Bugs_Life.md) and general maintenance.

Another side of the development team is responsible for new
[projects](User:Dfelinto/Drafts/Projects) and research &
development.

When [module owners](../../../Modules/Roles.md) are assigned as a project
leader, they should stay away from the regular maintenance task as much
as possible. This allows for housekeeping, as well as the vision being
passed "down the line".
