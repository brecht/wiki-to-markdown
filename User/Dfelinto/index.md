\_\_TOC\_\_

## The person

  - Name  
    Dalai Felinto
  - On developer.blender.org  
    [dfelinto](https://developer.blender.org/p/dfelinto/)
  - On blender.chat  
    [dfelinto](https://blender.chat/direct/dfelinto)
  - E-mail  
    dalai at blender.org
  - Activities  
    Product Manager

## Tracking the work

  - [/Reports/](/Reports/)
  - [/Drafts/](/Drafts/)

## Notes

  - [/Notes/](/Notes/)
