## October 01 - October 08

  - Proposal for a single column context menu
    [\#113115](https://projects.blender.org/blender/blender/pulls/113115)
  - Verb oriented edit mode menus
    [\#113122](https://projects.blender.org/blender/blender/pulls/113122)

## September 18 - September 30

  - Finish initial design of new brush thumbnails. Open for further
    feedback and polishing ([Devtalk
    Thread](https://devtalk.blender.org/t/brush-thumbnails-design-feedback/31322))
  - Working on new Human Base Meshes for the asset bundles
  - Small feedback adjustments to IC keymap
    [\#112628](https://projects.blender.org/blender/blender/pulls/112628)
  - Add back sculpt mode brush shortcuts with improvements
    [\#112898](https://projects.blender.org/blender/blender/pulls/112898)

## September 04 - September 10

  - Recording and editing on a YouTube video to give an overview of
    important keymap changes
  - IC Keymap: Regions shortcuts and fixed inconsistencies
    [\#112042](https://projects.blender.org/blender/blender/pulls/112042)
  - Status bar UI proposal
    [\#107094](https://projects.blender.org/blender/blender/issues/107094#issuecomment-1014866)
  - Preparing new Human Base Meshes for the asset bundles

## August 01 - August 13

  - More polishing of keymap patches
  - More consistency improvements for selection shortcuts
    [\#110960](https://projects.blender.org/blender/blender/pulls/110960)
  - Continuing work on brush essentials thumbnail design
    [\#104476](https://projects.blender.org/blender/blender/issues/104476)

## July 17 - July 30

  - Worked together with Campbell on implementing important keymap
    features
      - Keymap preference to use a pie menu to toggle editor regions
        ([ab7161e41a](https://projects.blender.org/blender/blender/commit/ab7161e41a0ea6e5645524e6344a5410c06f2143))
      - Keymap: improve consistency for paint modes
        ([6de6d7267f](https://projects.blender.org/blender/blender/commit/6de6d7267f3d0eb913ce08675c9d73e27b8785e4))
      - Avoiding 3D View transform inheritance
        ([d7558a243c](https://projects.blender.org/blender/blender/commit/d7558a243c6b3b48669d7c084ff0cfbbc5324e6a))
      - Selection shortcut improvements
        ([fb54d3f865](https://projects.blender.org/blender/blender/commit/fb54d3f865928ebe4042eb1a3a12eb02709332b8))
      - Selection mode shortcuts
        ([827918c925](https://projects.blender.org/blender/blender/commit/827918c925d3ab21dbba27016cf1218cb70c6551))
      - Simplify keymap for modes with brushes
        ([a7dd864dcf](https://projects.blender.org/blender/blender/commit/a7dd864dcf92e01e68e5b24564995dcd78098e50))

<!-- end list -->

  - More work on
    [\#105298](https://projects.blender.org/blender/blender/issues/105298)
    to finalize the patches.
      - Adjustments and fixes based on review, feedback and testing.
      - Splash screen design to inform users of important changes
        ([\#110425](https://projects.blender.org/blender/blender/issues/110425))

## July 03 - July 09

  - Worked on Industry Compatible keymap changes for Blender 4.0
    ([\#109750](https://projects.blender.org/blender/blender/pulls/109750))

## June 05 - June 11

  - Started implementing changes for 4.0 keymap based on
    ([\#105298](https://projects.blender.org/blender/blender/issues/105298))
      - [Simplify Default Keymap
        \#108712](https://projects.blender.org/blender/blender/pulls/108712)
      - [Added/updated selection mode shortcuts
        \#108714](https://projects.blender.org/blender/blender/pulls/108714)

## May 29 - June 04

  - Started work on various patches for the keymap improvements for 4.0
    ([105298](https://projects.blender.org/blender/blender/issues/105298))
  - Started work on a video for the new Base Meshes asset bundle

## May 22 - May 28

**Blender**

  - More work on new Brush Thumbnail design
    ([104476](https://projects.blender.org/blender/blender/issues/104476#issuecomment-948125))

**User Manual**

  - Added new documentation for 3.6 release
    ([c1ef18415a](https://projects.blender.org/blender/blender-manual/commit/c1ef18415afbba51325278772edeb7167b860fe3))

## May 15 - May 21

**Blender**

  - Provided a proposal with mockups for a header status info overlay
    ([\#107094](https://projects.blender.org/blender/blender/issues/107094#issuecomment-941233))
  - Added multiple minor tasks for the workboard
    ([\#108107](https://projects.blender.org/blender/blender/issues/108107),
    [\#108105](https://projects.blender.org/blender/blender/issues/108105).
    [\#108109](https://projects.blender.org/blender/blender/issues/108109),
    [\#108111](https://projects.blender.org/blender/blender/issues/108111))

**Dyntopo Refactor**

  - Started testing the PR extensively and [collected
    notes](https://hackmd.io/@JHyBAjeSQFyYQD5sicVWNA/BkhLE1rS3) together
    with Daniel Bysedt
  - Proposed new header layout to save space and increase clarity on
    features
  - Provided multiple mockups and UI layouts for new "Boundary
    Smoothing" features and new "Inherit" concept

**Asset Bundles**

  - Finalized the asset bundle and made it ready for announcement next
    week ([asset bundle
    thread](https://devtalk.blender.org/t/asset-bundle-base-meshes/21535))

## May 8 - May 14

  - Adjusted Keymap proposals based on feedback
    ([\#105298](https://projects.blender.org/blender/blender/issues/105298))
  - Making the Human Base Meshes asset bundle ready for release
    ([Devtalk
    Thread](https://devtalk.blender.org/t/asset-bundle-base-meshes/21535/186?u=julienkaspar))
  - Feedback and design for the Dyntopo Refactor ([Design
    Mockups](https://projects.blender.org/blender/blender/pulls/104613#issuecomment-940146))

## May 1 - May 7

**Blender**

  - Finalized current design and created prototype keymaps for [\#105298
    - Sculpt/Painting Modes: Revamp
    Keymap](https://projects.blender.org/blender/blender/issues/105298)
  - Created devtalk threads for feedback and testing of the keymap
    proposals
      - [Default Keymap
        Proposal](https://devtalk.blender.org/t/draw-paint-sculpting-keymap-proposal-feedback-request/29253/18)
      - [IC Keymap
        Proposal](https://devtalk.blender.org/t/industry-compatible-keymap-proposal-feedback-requested/29267)

## April 24 - April 30

**Blender**

  - Collected long standing issues with Multires and planned for most
    important tasks after Dyntopo refactor
    [\#107118](https://projects.blender.org/blender/blender/issues/107118)
  - Worked on [\#106898 - Brush Assets: Create polished mockup for the
    Draft
    operators](https://projects.blender.org/blender/blender/issues/106898#issuecomment-926299)
  - A lot of research and testing for revamped keymaps
    [\#105298](https://projects.blender.org/blender/blender/issues/105298)

## April 10 - April 16

After a month of absence I'm back.

**Blender**

  - Worked on keymap revamp research and design
    [\#105298](https://projects.blender.org/blender/blender/issues/105298#issuecomment-920110)

## February 27 - March 5

**Blender**

  - Transitioned Asset Shelf wireframes to Penpot and refined them based
    on feedback.
  - Updated brush thumbnail design proposal. Waiting for feedback
    [https://projects.blender.org/blender/blender/issues/104476\#issuecomment-893698
    \#104476](https://projects.blender.org/blender/blender/issues/104476#issuecomment-893698_#104476)
  - Wrapped up VDM demo file with Daniel Bystedt (Will be uploaded for
    3.5 release)

## February 20 - February 26

**Blender**

  - Tested VDM feature and worked on demo material with Daniel Bystedt
  - Worked further on brush thumbnail design proposal
    ([\#104476](https://projects.blender.org/blender/blender/issues/104476#issuecomment-889409))
  - Worked on Asset Shelf high fidelity wireframes to test alignments
    and scrolling behavior

## February 13 - February 19

''' User Manual '''

  - Added release notes and user manual entries for VDM sculpting
    feature ([Draw
    brush](https://docs.blender.org/manual/en/dev/sculpt_paint/sculpting/tools/draw.html))

**Blender**

Tested various patches.

## February 6 - February 12

**Blender**

  - Helped transitioning the module workboard to Gitea
  - Worked on a design for improved brush thumbnails for upcoming
    releases
    [\#104476](https://projects.blender.org/blender/blender/issues/104476)
  - Started up
    [discussions](https://devtalk.blender.org/t/asset-bundle-base-meshes/21535)
    again on base meshes for the upcoming "Essential" assets library

## January 30 - February 5

''' User Manual '''

  - Clarified and added missing Simplify use case info
    ([rBM9923](https://projects.blender.org/UNSUPPORTED/blender-manual-translations-svn/commit/9923))

**Blender**

  - Collected previous work on updated Matcaps
    ([T104285](https://developer.blender.org/T104285))
  - Tested and reviewed various patches

## January 23 - 29

''' User Manual '''

  - Overhauled Trim Tool pages based on feedback & visual material from
    Daniel Bystedt
    ([rBM9913](https://projects.blender.org/UNSUPPORTED/blender-manual-translations-svn/commit/9913))

## January 16 - 22

''' User Manual '''

  - Overhauled Editing pages for sculpting manual, including a new
    Expand page
    ([rBM9885](https://projects.blender.org/UNSUPPORTED/blender-manual-translations-svn/commit/9885))

## January 9 - 15

''' User Manual '''

  - Improved sculpt mode tool pages
    ([rBM9865](https://projects.blender.org/UNSUPPORTED/blender-manual-translations-svn/commit/9865))
    ([rBM9866](https://projects.blender.org/UNSUPPORTED/blender-manual-translations-svn/commit/9866))
    ([rBM9880](https://projects.blender.org/UNSUPPORTED/blender-manual-translations-svn/commit/9880))
  - Various fixes like
    ([rBM9876](https://projects.blender.org/UNSUPPORTED/blender-manual-translations-svn/commit/9876))
    ([rBM9877](https://projects.blender.org/UNSUPPORTED/blender-manual-translations-svn/commit/9877))
    ([rBM9878](https://projects.blender.org/UNSUPPORTED/blender-manual-translations-svn/commit/9878))

''' Design '''

  - Simple task on gizmo visualization
    ([T103782](https://developer.blender.org/T103782))

## January 2 - 8

''' User Manual '''

  - Overhauled the brush pages documentation
    ([rBM9839](https://projects.blender.org/UNSUPPORTED/blender-manual-translations-svn/commit/9839)),
    ([rBM9856](https://projects.blender.org/UNSUPPORTED/blender-manual-translations-svn/commit/9856))
  - Added info to the Multiresolution modfiier page
    ([rBM9859](https://projects.blender.org/UNSUPPORTED/blender-manual-translations-svn/commit/9859))

''' Design '''

  - More explicit action to sync image editor images
    ([T103636](https://developer.blender.org/T103636))
