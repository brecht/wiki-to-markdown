### Info

I'm a 3D artist at the Blender Studio and coordinator of the
Sculpt-Paint & Texture module.

### Pages

[Weekly Reports 2023](2023-weekly-reports.md)
