## GSoC 2019 - Intel Embree BVH for GPU

### Name

Matillat Quentin

### Contact

  - **Email:** [qmatillat@gmail.com](http://mailto:qmatillat@gmail.com)
  - **IRC nickname:** qmatillat
  - **Phone number:** +33 6 26 27 52 34
  - **Address:** 107 Chemin des bouton d'Ors, 38460 Chamagnieu, France

### Synopsis

Embree has been introduced in blender in commit
[rB2c5531c0](https://projects.blender.org/blender/blender/commit/2c5531c0),
but is still really limited. It can only be used if rendering on CPU,
and it requires to set an optional flag at compilation time (which is
not enabled for pre-built binary).

The goal of this proposal is to make Embree also usable on GPU.

### Benefits

Using the same implementation detail on both CPU and GPU reduce the
amount of code. Moreover using an opensource external project often mean
simpler and less code to maintain, and allows to benefit from external
update made by the community. Using Embree will also lead to faster
rendering, faster motion blur computation, especially on GPU rendering.

### Deliverable

The only change visible for end-user should be at most a checkbox to
enable/disable this feature. No other change will be required.

The deliverable will be :

  - A converter from Embree BVH to Cycles BVH data structures.
  - Improvement to Embree BVH exporter API
  - Updated Cycles kernel BVH traversal
  - Benchmark (performance & memory usage) of the new solution
  - Developer documentation for reference, code maintenance and further
    development.

### Project Details

The goal is to replace the internal BVH builder used by Blender with the
one used by Embree, and use it for both CPU and GPU. Embree already has
CPU BVH traversal algorithm, but before using it on GPU, we need to copy
the BVH data to the data structures used by Cycle. For faster motion
blur the Cycles kernel also needs modified BVH traversal.

#### Implement converter from Embree BVH to Cycles BVH data structures.

Embree's API is only able to run from CPU, but it has the ability to
build a BVH tree with the method \`rtcBuildBVH\`.

Currently, Blender is using his own implementation (see class
[BVH](https://git.blender.org/gitweb/gitweb.cgi/blender.git/tree/HEAD:/intern/cycles/bvh))
that allow multiple implementation to work together. To add a new
implementation, we need to create a new class and add an entry in
\`BVH::create\`. Currently they are 4 implementation (BVH2, 4, 8 and
Embree). But the Embree implementation is using ray-tracing features, so
it can only run on CPU.

The idea is to use Embree to build the BVH, then convert the tree to
Cycles structure, while using the same kernel as before for the
rendering part. Importing the blender's data will be easy as the current
Embree implementation is already doing the conversion.

![../../../images/ConvertProcess.png](../../../images/ConvertProcess.png
"../../../images/ConvertProcess.png")

I submitted a differential
([Diff 14679](https://developer.blender.org/differential/diff/14679/)
that show a really simple implementation an implementation of the
converter. It use \`rtcBuildBVH\` to create the BVH tree, then convert
the tree to the same format as BVH2. It currently work only for basic
scene (no mesh with instance) and don't split meshes (which lead to
really poor performance).

#### Improve Embree's API to export data for motion blur

Currently Embree provide two option : either you provide geometry and
use the ray tracing functions, or you provide bounding box and Embree
build the BVH tree (as in example
[\`bvh\_builder\`](https://github.com/embree/embree/tree/master/tutorials/bvh_builder)).
I used the second option for the
[Diff 14679](https://developer.blender.org/differential/diff/14679/)).
The problem with this solution is that it currently does not support OBB
(oriented bounding box) nor motion blur. While rendering on GPU, we
can't use Embree's functions, so we need to copy all the structure to
GPU memory, but to access this *internal* data, it is currently version
dependent:

`   Please be aware that the internal Embree data structures might change between Embree updates.`  
`   See example `[`bvh_access`](https://github.com/embree/embree/tree/master/tutorials/bvh_access)  
`   `*`Exrtact``   ``from``   ``documentaion:`` 
 `<https://embree.github.io/tutorials.html>*

So we need to update Embree API to either improve the first option
(export BVH for a scene) or to allow the function \`rtcBuildBVH\` to
build OBB and to take in account motion blur.

#### Adjust Cycles kernel BVH traversal for faster motion blur.

Embree has a better support for motion blur support (by using
[STBVH](https://embree.github.io/papers/2017-HPG-msmblur.pdf)). We need
to change the implementation in the kernel that run on GPU, so it can
use this new feature. This require changes in both data structures and
traversal code.

All the change must be done in the file
[kernel/bvh/bvh\_traversal](https://git.blender.org/gitweb/gitweb.cgi/blender.git/blob/HEAD:/intern/cycles/kernel/bvh/bvh_traversal.h).
Currently, GPU only support BVH (BVH2) and QBVH (BVH4). OBVH (BVH8) is
only supported on CPU if AVC2 is available. The idea is to add a new
layout type (STBVH) that will be supported on GPU, that does the
traversal.

#### Test performance and memory usage

Everything in this project is supposed to increase render speed. To
validate that this is positive improvement, we should benchmark the
memory usage and performance of our solution. Two performance criteria
must be distinguished : BVH building and traversal time. The latter will
reflect the quality of the tree.

### Project Schedule

| Week              | Task                                                                                       |
| ----------------- | ------------------------------------------------------------------------------------------ |
| Community bonding | Exploration of Cycles and Embree code base.                                                |
| 1                 | Implement converter Embree's BVH -\> Cycle BVH                                             |
| 2                 |                                                                                            |
| 3                 | Testing converter (including initial benchmark test)                                       |
| 4                 | Update Embree's API so it can export oriented bounding box & motion steps.                 |
| 5                 |                                                                                            |
| 6                 | Buffer week's (to make sure Embree API update is accepted)                                 |
| 7                 | Adjust Cycles kernel BVH traversal                                                         |
| 8                 |                                                                                            |
| 9                 | Test new Cycle kernel                                                                      |
| 10                | Test performance and memory usage to compare to existing BVH, and fix any issues if needed |
| 11                |                                                                                            |
| 12                | Buffer week's (to cope with delay).                                                        |
| 13                | Final testing and debugging. All features must be ready for deployment.                    |

Schedule

### Bio

I'm Matillat Quentin, and I'm a student in fourth year of higher
education in engineering in a school called ESISAR (National School of
Advanced Systems and Networks) in Valence, where I specialize in
Computer Science, networks and cybersecurity.

From previous personal experiment, school projects and internships, I
have a lot of experience in C++ and Python, but I also made small
project using OpenGL and CUDA, so I'm familiar with GPGPU.

I've always been attracted to 3D graphics, especially video games. My
only big project related to CG is a game engine (very basic) that i
built in C++. [*Hosted on
gitlab*](https://gitlab.com/tinou98/TowerDefence). I used blender to
create asset: I created a plugin that add in Blender some UI element to
configure each object before export, and I wrote a custom exporter
(written in python) to create the binary files. [code for the
plugin](https://gitlab.com/tinou98/TowerDefence/tree/master/info)
