## GSoC 2019 - Intel Embree BVH for GPU

Nobody likes waiting for hours whilst blender is busy rendering. Cycle,
one of the render engine of blender, is a heavy user of ray-tracing. The
usage of ray-tracing require to build a BVH (Bounding Volume Hierarchy).
The construction of such a tree is complicated. Currently blender use
it's own BVH Builder.

Currently, Embree can be used only if rendering on CPU, and it requires
that an optional flag is set at compilation time (which is not enabled
for pre-built binary).

The goal of this proposal is to make Embree also usable on GPU.

### [/Proposal/](/Proposal/)
