## GSoC 2019 Embree on GPU

![Example performance gain on a scene with motion
blur](../../../images/RenderTime_deformationBlur.png
"Example performance gain on a scene with motion blur")

This is the final report for the GSoC 2019 project: Embree on GPU.

All of the code can be found in this branch:
[soc-2019-embree-gpu](https://developer.blender.org/diffusion/B/browse/soc-2019-embree-gpu/),
and in this diff [D5522](http://developer.blender.org/D5522). It contain
a patch that summarize all the change to Embree. Those change can also
be found in this repository :
[github.com/tinou98/embree](https://github.com/tinou98/embree).

### Work done

The creation of all the BVH structure are extracted from Embree internal
structure, and support the existing following traversal features :

  - instanced mesh
  - motion blur
  - curves (without motion blur)

Embree also support these new features that have been added to the
traversal code :

  - node that are only valid for a range of time
  - linear interpolation of bounding box at traversal time

### Remaining work

  - Support for motion blur on curves
  - The convert step is currently single threaded, and can benefit from
    multithreading.
  - Improvement to creation of leaf: Creation of leaf still require some
    access to blender object data, and thus is slow.
  - The shrink step can be optimized :

Embree has a BVH4 tree, but GPU expect a BVH2 tree, thus the conversion
step create two inner node : ![Original BVH4
tree](../../../images/BVH4.png "Original BVH4 tree") ![Converted tree
(BVH2)](../../../images/BVH2.png "Converted tree (BVH2)") The
computation of the two bounding box for the inner node, can be optimized
(especially with unaligned node), see
[bvh\_shrink](https://git.blender.org/gitweb/gitweb.cgi/blender.git/blob/refs/heads/soc-2019-embree-gpu:/intern/cycles/bvh/bvh_embree_converter.cpp#l110).

### Try it yourself

  - Clone blender and checkout the
    \`[soc-2019-embree-gpu](https://git.blender.org/gitweb/gitweb.cgi/blender.git/shortlog/refs/heads/soc-2019-embree-gpu)\`.

<!-- end list -->

``` bash
git clone git://git.blender.org/blender.git
git checkout soc-2019-embree-gpu
```

  - Install dependencies, but force to build embree from source :

<!-- end list -->

``` bash
./blender/build_files/build_environment/install_deps.sh --with-embree --build--embree
```

  - Install dependencies, but force to build embree from source :

<!-- end list -->

``` bash
mkdir build; cd build
cmake -DWITH_CYCLES_EMBREE=ON ../blender
make -j <N> install
```

  - Enable Embree converter from <span class="literal">Render
    panel</span> : <span class="literal">Performance</span> »
    <span class="literal">Acceleration Structure</span> »
    <span class="literal">BVH Builder</span> »
    <span class="literal">Embree, then convert</span>
  - Then render

### Benchmark

The converter shows the best performances on the scenes with a
deformation motion blur, but on other scene, it is usually around 10%
slower.

<File:RenderResult> deformation
motion.png|\`test/cycles/deformation\_motion.blend\` <File:RenderTime>
deformationBlur.png|Scene with deformation blur <File:RenderTime>
classroom.png|\`benchmarks/cycles/classroom\_gpu.blend\`
<File:RenderTime> motion blur.png|\`test/cycles/motion\_blur.blend\`
