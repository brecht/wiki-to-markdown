**Related:**

  - <https://wiki.blender.org/wiki/Reference/Glossary>
  - <https://wiki.blender.org/wiki/Source/File_Structure>
  - <https://docs.blender.org/manual/en/dev/glossary/index.html>
  - <https://en.wikibooks.org/wiki/Blender_3D:_Noob_to_Pro/Glossary>
  - <https://en.wikipedia.org/wiki/Glossary_of_computer_graphics>

-----

**Glossary of acronyms and development related terminology**

  - AO  
    Ambient Occlusion
  - astar  
    A\* search algorithm ?
  - BAT  
    Blender Asset Tracker
  - bb  
    BLI\_assert(ELEM(tmp.bb, NULL, me-\>bb));
    bounding box ?
  - bdsm  
    Blender Data Storage Manager (EditDerivedBMesh ?)
  - BKE  
    Blender Kernel
  - BLI  
    (\#include "BLI\_utildefines.h")
    Blender Library
  - BLO  
    Blender Outliner?
  - bmesh  
    Blender Mesh / (non-manifold) boundary representation mesh
  - BMO  
    bmesh operators ?
  - bpy  
    Blender Python (Module ?)
  - BSDF  
    Bidirectional Scattering Distribution Function
  - BVH  
    Bounding Volume Hierarchy
  - CCG  
    Catmull-Clark Grid
  - CD  
    CustomData
  - CDDerivedMesh  
    CDDM
  - CDDM  
    a little mesh library ?
  - ceres  
    ceres solver
  - CLG  
    C Logging Library
  - clog  
    C Logging Library
  - COM  
    Compositor ?
  - COW  
    Copy On Write
  - CSG  
    Constructive Solid Geometry
  - DAG  
    Directed Acyclic Graph
  - DEG  
    depsgraph
  - depsgraph  
    Dependency Graph
  - DM  
    Derived Mesh
  - DNA  
    ?
  - DOF  
    Depth Of Field
  - EBO  
    Element Buffer Object
  - ED  
    editors module
  - EEVEE  
    Extra Easy Virtual Environment Engine
  - elbeem  
    El'Beem, Free Surface Fluid Simulation with the Lattice Boltzmann
    Method
  - ghost  
    Generic Handy Operating System Toolkit
  - ghash  
    general hash ?
  - g11n  
    globalization
  - GI  
    global illumination
  - GLSL  
    openGL Shading Language
  - gpencil  
    Grease Pencil
  - gpd  
    Grease Pencil Data ? (bGPdata)
  - GSOC  
    Google Summer of Code
  - HBAO  
    Horizon Based Ambient Occlusion
  - HDAO  
    High Definition Ambient Occlusion
  - IBO  
    Index Buffer Object
  - i18n  
    internationalization
  - ik  
    Inverse Kinematics
  - IES  
    Illuminating Engineering Society
  - imbuf  
    Image Buffer ?
  - itasc  
    instantaneous Task Specification using Constraints
  - KDL  
    Kernal Data Layer ?
  - kdop  
    k discrete oriented polytope ?
  - kdtree  
    k-dimensional tree
  - KX  
    Ketsji (Blender game engine)
  - l10n  
    localization
  - l12y  
    localizability
  - mat  
    material ?
  - mball  
    metaball
  - metal  
    graphics API for Mac OS
  - MOD  
    modifier
  - NLA  
    Non Linear Animation ?
  - NPR  
    Non Photorealistic Rendering
  - OCIO  
    OpenColorIO
  - OIIO  
    OpenImageIO
  - orco  
    ORthographically projected onto bbox COordinates ?
  - OSD  
    OpenSubDiv
  - OSL  
    Open Shading Language
  - par  
    parameter ?
  - PBR  
    Physically Based Rendering
  - PBVH  
    Paint BVH (bounding volume hierarchy)
  - PCF  
    Percentage Closer Filtering
  - PRT  
    Precomputed Radiance Transfer
  - RNA  
    ?
  - rtl  
    Real Time Library ?
  - SDNA  
    Struct DNA
  - SSAO  
    Screen Space Ambient Occlusion
  - SSS  
    Sub Surface Scattering
  - umb  
    ? metaball?
  - VAO  
    Vertex Array Object
  - VBO  
    vertex buffer object
  - VDB  
    Volumetric Database ?
  - VNT  
    Vertex, Normals, and Texcoords
  - VSE  
    Video Sequence Editor
  - VSM  
    Variance Shadow Mapping
  - VXAO  
    Voxel Ambient Occlusion
  - VXGI  
    Voxel Global Illumination
  - WM  
    Window Manager

  
\----

**other**

  - AHOY (non-acronym) = incoming, soon, on the horizon. e.g.: "release
    AHOY"
  - LGTM = Looks Good To Me
  - NUQ = Non Urgent Question
  - UQ = Urgent Question

  
\----

**3rd party related terms**

  - BEER  
    Blender Extended Expressive Rendering
  - BLAM  
    camera and video projector calibration toolkit
