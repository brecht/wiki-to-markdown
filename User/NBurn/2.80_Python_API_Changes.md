Eventually move to `Reference/Release_Notes/2.80/Python_API` ?

-----

**Update to embedded Python version** (last checked: 2018-12-05)

``` diff
-Python 3.5.3 (May 20 2017)
+Python 3.7.0 (Aug 26 2018)
```

**Renamed lamp type**

  -   
    The `Lamp` type from 2.7x has been replaced by `Light`

<!-- end list -->

``` diff
-bpy.types.Lamp
+bpy.types.Light
```

**Matrix multiplication**

  -   
    mathutils now uses the
    [PEP 465](https://www.python.org/dev/peps/pep-0465/) binary operator
    for multiplying matrices

<!-- end list -->

``` python
import bpy
from mathutils import Matrix
mat_foo = Matrix(( (1, 2), (3, 4) ))
mat_bar = Matrix(( (11, 12), (13, 14) ))
mat_baz = mat_foo @ mat_bar  # result: (37, 40), (85, 92)
```

**BGL module** (Over 700 entries removed, partial sample below)

``` diff
-bgl.glBegin
-bgl.glBitmap
-bgl.glColor
-bgl.glColorMaterial
-bgl.glCopyPixels
-bgl.glDrawPixels
-bgl.glEnd
-bgl.glFog
-bgl.glIndex
-bgl.glLight
-bgl.glRect
-bgl.glRotate
-bgl.glScale
-bgl.glTexCoord
-bgl.glVertex
```

**Other**

``` diff
-scene_update_pre
-scene_update_post
+depsgraph_update_pre
+depsgraph_update_post

-row = column.row(True)
+row = column.row(align=True)

-row.prop(self, "ctrl", "Ctrl", toggle=True)
+row.prop(self, "ctrl", text="Ctrl", toggle=True)

-col.label("Foo")
+col.label(text="Foo")

???
-space_data.use_occlude_geometry
+space_data.shading.show_xray
```

-----

**Additional resources:**

  - [Updating Scripts
    from 2.7x](https://wiki.blender.org/wiki/Process/Addons/Guidelines/UpdatingScripts)
    [(older
    version?)](https://en.blender.org/index.php/Dev:2.8/Source/Python/UpdatingScripts)
  - [Blender 2.8x / Python, Proposed
    Changes](https://developer.blender.org/T47811)
  - [T52599: Python API, changes to type registration
    in 2.8](https://developer.blender.org/T52599)
  - [Blender 2.8 Python Meeting
    Notes](https://blenderartists.org/t/blender-2-8-python-meeting-notes/1116629)
  - [2.8: Addons UI](https://developer.blender.org/T55407)
  - [Blender 2.8 Planning
    Update](https://code.blender.org/2018/08/blender-2-8-planning-update/)

# Blender 2.80 API Changes

This section was backed up from:

<https://en.blender.org/index.php/Dev:2.8/Source/LayersCollections/API-Changes>

``` diff
-context.scene.layers
+# no longer exists
```

``` diff
-context.scene.objects
+context.render_layer.objects
```

``` diff
-context.scene.objects.active
+context.render_layer.objects.active
```

``` diff
-bpy.context.scene.objects.link()
+bpy.context.scene_collection.objects.link()
```

``` diff
-bpy_extras.object_utils.object_data_add(context, obdata, operator=None, use_active_layer=True, name=None)
+bpy_extras.object_utils.object_data_add(context, obdata, operator=None, name=None)
```

``` diff
-bpy.context.object.select
-bpy.context.object.select = True
-bpy.context.object.select = False

+bpy.context.object.select_get()
+bpy.context.object.select_set(action='SELECT')
+bpy.context.object.select_set(action='DESELECT')
```

``` diff
-bpy.context.object.hide
+not bpy.context.object.visible_get()
```

``` diff
-bpy.context.object.hide = False
+# no longer exists, use collection.hide instead
```

``` diff
-AddObjectHelper.layers
```

``` diff
rna_Scene_ray_cast requires a scene_layer argument.
```

## Gotchas

### scene\_collection

If you copy context and override scene\_layer, but not scene\_collection
what's going to be the override scene\_collection?

*It depends\!*

Is the original context.scene\_collection linked (directly, or
indirectly via nesting) into the overridden scene\_layer? Then this is
still the one you will get.

Is it not? In this case you get the active collection of the overridden
scene\_layer.
