<u>Testing...</u>

# Sample Heading 1 bar

## Sample Heading 2 bar

### Sample Sub Heading 3 bar

#### Sample Sub Heading 4 bar

##### Sample Sub Heading 5 bar

-----

  
'single quotes'

*italic text* (2x single quotes)

**bold text** (3x single quotes)

***bold italic text*** (5x single quotes)

'''''bold italic text''''' (above with wiki formatting removed)

[web link (blender.org)](https://www.blender.org/)

`[https://www.blender.org required link title]` external link example
syntax (no title == ref[1](https://www.blender.org))

`[[Main Page|optional link title]]` internal link example

`[[Main Page]]` internal link example without title

`#REDIRECT [[Main Page]]`   (redirect to [Main
Page](../../Main_Page.md))

Bravo **Lima** Echo **November** Delta **Echo** Romeo

H<sub>2</sub>O

``` Python
# python code formatting test
import bpy

def main():
    ob_names = [o.name for o in bpy.context.scene.objects]
    for ob in bpy.context.scene.objects:
        print("obj name:", ob.name)

main()
```

``` C
// C code formatting test
#include "stdio.h"

int main(void) {
    printf("Sample text.");
    return 0;
}
```

**wiki syntax links**

  - [mediawiki
    Help:Formatting](https://www.mediawiki.org/wiki/Help:Formatting)
  - [mediawiki Help:Tables](https://www.mediawiki.org/wiki/Help:Tables)
  - [mediawiki Help:Images](https://www.mediawiki.org/wiki/Help:Images)

  

-----

  

# Article Drafts:

  - [User:NBurn/Setting up a Python Debugger with
    Blender](User:NBurn/Setting_up_a_Python_Debugger_with_Blender)
  - [User:NBurn/Glossary](Glossary.md)
  - [User:NBurn/2.80 Python API
    Changes](2.80_Python_API_Changes.md)

# Blender Wiki Stuff

  - [Old Blender Wiki Archive](https://archive.blender.org), [User
    page](https://archive.blender.org/wiki/index.php/User:NBurn/)
  - [User:NBurn/Desgraph](User:NBurn/Desgraph)
  - <https://developer.blender.org/T48096>
  - <https://developer.blender.org/T56215>
  - <https://developer.blender.org/T48381>
  - [Process/Addons/Guidelines](../../Process/Addons/Guidelines/index.md)
    (needs cleanup / updating)
