# Week 2

Hi everyone\! The second week is gone, and this is my report.

## Objectives

With the shader in place, I could move on to the following (Weeks 4-5):

  - Test shader functionality. If working, release test build.
  - Evaluate received feedback. Start bug fixes.

## What's been done

Admittedly, not much 😢

  - Lukas added a Diffuse override for using the shader with non-hair
    geometry, in `bsdf_principled_hair_setup`. When using it with hair,
    however, Blender didn't render anything *at all*. This was because
    the ID of the shader was outside the range of `CLOSURE_IS_BSDF`. Our
    shader is now treated as BSDF and transmissive
    ([rBc4174a44](https://projects.blender.org/blender/blender/commit/c4174a44dc311d2fd65ca74c4387bbc6f529df95)).
  - With the BSDF in place, I tried to get reference renders. The main
    problem is that Blender's hair primitive is returning `NaN`s in any
    or all of the following:
      - the ray distance `t`
      - the intersection calculation, somewhere deep within
        `cardinal_curve_intersect` or `curve_intersect`
      - the curve distance `h` (this one usually comes from the above)
    Which results in one or more of the following:
      - crash when integrating indirect radiance ("`Non-finite sum in
        path_radiance_clamp_and_sum!`")
      - crash when obtaining `azimuthal_scattering` (I have a scene to
        obtain this exact crash)
      - `assert(fabsf(dot(sd->dPdu, dPdCD)) < 1e-5f);`. I do not know
        (yet) what this one means.
    I ring-fenced the shader from most of those
    ([rB92030f83](https://projects.blender.org/blender/blender/commit/92030f83362fe567f78e519e0acca0700287a933)).
    Brecht agreed to have a look at those weird `NaN`s, since they come
    from outside our shader.
  - [In Phabricator's task for this
    project](https://developer.blender.org/T54796), Ronan Ducluzeau
    could get a reference render without crashing. The shader returns a
    lot of noise and fireflies.

## Next up

Once we figure out why we receive bogus data from the ray intersection
(and fix it), I'll be able to continue revising the shader.

If possible, I'd like to get a dev build out in BlenderArtists for
people to test. I don't expect to get great results yet, but it'd be
great if we can ensure the hair primitive works properly. I don't have
good enough bandwidth (3Mb) to upload a 200MB+ build unless I go to the
uni, so I may ask either Lukas or bzztploink in IRC if they can upload
them.

## Questions

  - In re assertion above - what does it mean?
  - What could be possible sources of noise in our shader? Compared with
    a Diffuse shader (which doesn't use the hair primitive data), it's
    obviously not working properly. I wonder if this and the other
    issues are related.
