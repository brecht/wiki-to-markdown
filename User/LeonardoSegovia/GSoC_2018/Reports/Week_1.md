# Week 1

Hi everyone\! The first week's over, and here's what I've done so far.

## Objectives

  - Review of Pharr’s code. Implement a dummy Cycles node. Verify
    exposure of shader’s parameters and test setters/getters.

Since I had Lukas's work available, I managed to extend this week's work
to the next two objectives:

  - Port Pharr’s shader primitives and statistical tests. Verify they
    work as expected.
  - Port Pharr’s shading function.

## What's been done

You may have noticed my commits were on Monday and Friday-Saturday; this
was because a short postgraduate course, *Topics on Logic and Artificial
Intelligence in Law* by [Dr. Antonino
Rotolo](https://www.unibo.it/sitoweb/antonino.rotolo/en) came up at my
uni during this week's Tue-Wed-Thu mornings. (These were an additional
40 hours towards my degree's quota of 540.)

Anyway...

![Preview of the new Principled Hair shader
node.](../../../../images/ZootopiaShaderNode-week-1.png
"Preview of the new Principled Hair shader node.")

  - The infrastructure for the shader node is now in place 😄.
    ([rB40bc1a5c](https://projects.blender.org/blender/blender/commit/40bc1a5cddf261bdb428a3d07819487b376a6151))
  - Lukas's patch included dummy license info. We (me and Brecht) asked
    him on IRC, and he stated the code is all his own. The license was
    updated accordingly
    ([rB09bb0bca](https://projects.blender.org/blender/blender/commit/09bb0bca0f1f77f61bdc832a36458bb311134b1c))

> `2018-05-18 15:46:02 lukasstockner97_ brecht, regarding your question
> about the code's origin: I didn't copy-paste anything, the code itself
> is written from scratch, but the underlying algorithm is an exaxt
> copy`

> `2018-05-18 15:46:14 lukasstockner97_ So afaik we could license it as
> Apache 2`

> `2018-05-18 15:46:56 lukasstockner97_ oh, and hi @everyone`

> `2018-05-18 15:48:54 brecht lukasstockner97_: yes, I agree then`

  - Reworked some naming conventions to make them follow PBRT and be
    clearer
    ([rB04d40de3](https://projects.blender.org/blender/blender/commit/04d40de354eb8a07f840fa168bbb3d375adcf40d))
  - During the weekend, I managed to figure out that (somehow) Cycles
    creates the shader's closure, but when sampling it, it thinks that
    it's a Diffuse shader instead\! I plugged in a few `printf`s in
    `bsdf_sample`, `bsdf_eval`, and `svm_node_closure_bsdf` that reveal
    the situation. (Ask on IRC if you want the diff)

## Next up

Next week I'll revise the implementation, to figure out where and how
our shader is morphed into a Diffuse one. As I need more info on
Cycles's kernel's code layout to figure this out, I'll ask Brecht and
Lukas if they can give me a walkthrough.

## Questions

This is mostly for the sake of documentation, I'm planning to publish
some time later a sort of "Anatomy of a Shader":

  - What is the meaning of RNA and DNA in Cycles? I understand that it
    integrates the shader's parameters with the UI. I'll be revisiting
    this in the last weeks.
  - What is the registering flow of a shader in Cycles? I should re-read
    [rB40bc1a5c](https://projects.blender.org/blender/blender/commit/40bc1a5cddf261bdb428a3d07819487b376a6151)
    to better understand it.
  - What is the control flow of the sampling process in Cycles? This
    should give me a clue on where and how our closure is being
    Diffuse'd.
