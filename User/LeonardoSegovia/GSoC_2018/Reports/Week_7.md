# Week 7

Hi everyone\! This is my report for the present week.

## Objectives

This week, I dealt with the objectives for Weeks 8, 10, and 11:

  - Repurpose existing regression tests and obtain reference renders.
  - User documentation revision.
  - Developer documentation revision.

Again, bzztploink and Lukas updated the builds on Monday.

## What's been done

  - The usual merging commits from `master`
    ([rB90971242](https://projects.blender.org/blender/blender/commit/909712423b9efb359c84327838bde2ca0873b8ba),
    [rB9b96b009](https://projects.blender.org/blender/blender/commit/9b96b00910148a4342e85c402562aa5b125d0a5a),
    [rB744a806d](https://projects.blender.org/blender/blender/commit/744a806d7f9e5f2f8b86adf05b0c7e2c82ff16df)).
  - Cleaned up the diff between our branch and `master`
    ([rB85bfd5ef](https://projects.blender.org/blender/blender/commit/85bfd5ef008229a00ffc3c007cc58cb539ef6589),
    [rBb4959bb6](https://projects.blender.org/blender/blender/commit/b4959bb62bf791ee923f466ead535e3004305e77)).
  - The existing regression tests in the SVN repo have been ported to
    the new shader ([T55647](https://developer.blender.org/T55647)):
      - `cycles/hair.blend`
      - the files in the `render/ctests/hair/` directory
  - The shader is now self-documented
    ([rB26392505](https://projects.blender.org/blender/blender/commit/263925058ee07e8b5a2cd2dbb53f7910f8741825)).
      - This will be necessary once I start to write the relevant
        chapters of my thesis.
      - The Python and RNA parts are not yet covered\!
  - I've also published the first version of the manual pages
    ([T55648](https://developer.blender.org/T55648)):
      - <https://github.com/amyspark/blender-manual/> is the Git
        repository
      - <https://www.amyspark.me/blender-manual/> is the live version
        (thanks to GitHub Pages)

Brecht managed to obtain permission to use the models from ChrisWillC's
[PBR shader](https://blenderartists.org/t/custom-hair-shader/1116458)
for the reference renders.

## Next up

To wrap up the documentation, we need:

  - Charts detailing the effect of each property
  - Reference renders with the custom shader

I'm still waiting for my desktop to come back from the tech (faulty
power supply), so I can't yet do them. Also, just loading ChrisWillC's
model takes up \~5GB of RAM (both my desktop and MacBook have 8GB), so I
need to figure out if I'll need to reduce it somehow or buy extra RAM 😟.

## Questions

None this time\!
