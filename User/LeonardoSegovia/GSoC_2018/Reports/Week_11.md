# Week 11

Hi everyone\! This is my report for the present week.

## Objectives

Brecht requested that I profile the shader to see if there were any
possible optimizations. Also, if I could find out and fix why the shader
was noisy in certain areas.

## What's been done

  - Brecht uploaded the missing regression tests
    ([rBL62073](https://projects.blender.org/blender/blender-dev-tools/commit/62073)).
  - Apply Filter Glossy to the Roughness value (as detailed in the
    Blender manual), rather than the corresponding logistic's variance
    ([rB52a0e67f](https://projects.blender.org/blender/blender/commit/52a0e67fe0050683a888d6643ec2035bd2b3ea1c)).
  - Enable mode weights. This lets you select which light mode will be
    present in the render. Not exactly useful for physically-based
    renders, but for debugging issues like noise.
    ([rB2ab98cca](https://projects.blender.org/blender/blender/commit/2ab98cca19cb3acd3da61b5f71a61a1139f1713e),
    [rB21aefa09](https://projects.blender.org/blender/blender/commit/21aefa098f4935597a759e199153e9b55279ff8a))

I profiled the shader using nirved's Hair Curls (4 curls, 1000 hairs per
curl, black background, lighted by a HDR sphere) and Xcode's
Instruments. The only edit was to make *all* functions in
`bsdf_hair_principled.h` `ccl_device` (ie. not inlined).

Top ten of functions (full stacktrace as an addendum to the previous
week's report in soc-2018-dev):

    9.11 min   32.6%    9.11 min         ccl::QBVH_bvh_intersect_shadow_all_hair(ccl::KernelGlobals*, ccl::Ray const*, ccl::Intersection*, unsigned int, unsigned int, unsigned int*)
    6.28 min   22.5%    6.28 min         ccl::QBVH_bvh_intersect_hair(ccl::KernelGlobals*, ccl::Ray const*, ccl::Intersection*, unsigned int, unsigned int*, float, float)
    56.99 s    3.4% 56.99 s      ccl::BVHObjectBinning::BVHObjectBinning(ccl::BVHRange const&amp;, ccl::BVHReference*, ccl::BVHUnaligned const*, ccl::Transform const*)
    48.82 s    2.9% 48.82 s      ccl::kernel_branched_path_trace(ccl::KernelGlobals*, float*, int, int, int, int, int)
    36.48 s    2.1% 36.48 s      ccl::camera_sample_perspective(ccl::KernelGlobals*, float, float, float, float, ccl::Ray*)
    29.71 s    1.7% 29.71 s      ccl::kernel_branched_path_integrate(ccl::KernelGlobals*, unsigned int, int, ccl::Ray, float*, ccl::PathRadiance*)
    29.22 s    1.7% 29.22 s      ccl::bsdf_principled_hair_eval(ccl::KernelGlobals*, ccl::ShaderData const*, ccl::ShaderClosure const*, ccl::float3, float*)
    21.94 s    1.3% 21.94 s      ccl::kernel_tex_image_interp(ccl::KernelGlobals*, int, float, float)
    21.39 s    1.2% 21.39 s      ccl::svm_eval_nodes(ccl::KernelGlobals*, ccl::ShaderData*, ccl::PathState*, ccl::ShaderType, int)
    20.40 s    1.2% 20.40 s      ccl::svm_node_mapping(ccl::KernelGlobals*, ccl::ShaderData*, float*, unsigned int, unsigned int, int*)

Even though `bsdf_principled_hair_eval` appears (as expected, since it's
pretty heavy in calculations), the overwhelming majority of CPU time
goes to the BVH intersection calculation for hair, so I deemed this to
be sufficiently optimized.

As regards noise, I could only find out where it happens and in which
illumination conditions, not the actual source (code) of the noise.

![Transparent render of nirved's Hair Curls. From left to right: full
render, only primary specular (R), only transmission (TT), only
secondary specular (TRT), only residual (TRRT+). Noise is most prevalent
at the extrema of the curl (which is away from the
light).](../../../../images/Hair-Curls-noise.png
"Transparent render of nirved's Hair Curls. From left to right: full render, only primary specular (R), only transmission (TT), only secondary specular (TRT), only residual (TRRT+). Noise is most prevalent at the extrema of the curl (which is away from the light).")

  - Noise is most visible in the penumbra of the hair (places away from
    the glint), but separating the light passes (below) shows it affects
    the whole length of the hair.
  - It arises in the Gloss Ind (i.e. indirect glossy) pass of the
    renderer, which AFAIK points to light reflection between hairs.

![Indirect Glossy pass of nirved's Hair Curls. Noise is present
throughout the hair
length.](../../../../images/Hair-Curls-noise.glossind.png
"Indirect Glossy pass of nirved's Hair Curls. Noise is present throughout the hair length.")

  - It only affects the TT mode, which is consistent with the
    observations since it alters the coloring of the render. Rendering
    separate layers for each mode shows the rest are unaffected.

## Next up

  - Nothing yet, I'll wait to hear back from Lukas/Brecht to see if they
    have any more ideas/advice for debugging the noise issue.

## Questions

None this time.
