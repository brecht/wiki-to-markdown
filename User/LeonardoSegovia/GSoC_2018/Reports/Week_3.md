# Week 3

Hi everyone\! Yet another week's now gone. This is my report.

## Objectives

We're still in Weeks 4-5:

  - Test shader functionality. If working, release test build.
  - Evaluate received feedback. Start bug fixes.

## What's been done

This week's been MUCH better\!

  - It turns out that I had `WITH_CYCLES_DEBUG` on, that was the source
    of all the `NaN` noise in my side. I rebuilt Blender from the ground
    up; I still have `RelWithDebInfo` on.
  - Thanks to Lukas, bzztploink and artok, we could get test builds for
    all platforms\! This enabled us to find some issues regarding:
      - Linux compilation
      - Missing initialization code
      - Some stuff I didn't know about Cycles's innards

In terms of commits,

  - Me:
      - Ronan Ducluzeau's noisy renders were caused by an mistake in
        Lukas's optimization of the trimmed logistic distribution. It's
        now fixed
        ([rB2af62e27](https://projects.blender.org/blender/blender/commit/2af62e27e2c894217b5d7daa880434447ea6ed35)).
      - Split off the azimuthal and longitudinal roughness factors to
        make debugging easier when needed
        ([rB4a73f5ce](https://projects.blender.org/blender/blender/commit/4a73f5ce584f49c68b2c64c74ef30188395088de)).
      - Added compatibility with OpenCL. Thanks to nirved for the
        patches and comments\!
        ([rB9d633312](https://projects.blender.org/blender/blender/commit/9d6333127623ebed7a275d382e63da1f87712aae),
        [rB00d0f3c2](https://projects.blender.org/blender/blender/commit/00d0f3c23527b047f3211fd760080fdfcec4037b))
      - Added OSL compatibility. I know I don't allocate the memory for
        the closures correctly, I'll patch it in the weekend.
        ([rB718d730b](https://projects.blender.org/blender/blender/commit/718d730bb409322dd9078273bfc0dc16b956a8d6),
        [rB718d730b](https://projects.blender.org/blender/blender/commit/718d730bb409322dd9078273bfc0dc16b956a8d6),
        [rB4774e906](https://projects.blender.org/blender/blender/commit/4774e906e19816b720c20271b9f9db28bb89debe),
        [rBb7a92fa7](https://projects.blender.org/blender/blender/commit/b7a92fa75e56c2b76968e523892bfb7ee9a6f19e),
        [rBdf0ead4d](https://projects.blender.org/blender/blender/commit/df0ead4dabd0c0f07ff046b59c7d850005d59e60),
        [rBbf5a378c](https://projects.blender.org/blender/blender/commit/bf5a378c3c350384f18ce7daf6624ef0c56ab5ba))
      - Added the final missing feature from Chiang's formulation, the
        R-mode roughness modifier (formally called "Primary Reflection
        Roughness Modifier"). It enables the user to render a shiny coat
        on an otherwise rough patch of fur
        ([rB25677d77](https://projects.blender.org/blender/blender/commit/25677d7738530d8f827e5ac960ccded6b128d897),
        [rBc3048b5f](https://projects.blender.org/blender/blender/commit/c3048b5f53aaeb5b860bf1760a7f1ef51f6c4701),
        [rBb3ebdf64](https://projects.blender.org/blender/blender/commit/b3ebdf64459d7a99e9a848a49f7eaa250b8a07bd)).
          - I had to move the geometry data out of the main
            `ShaderClosure` to make space for this. I didn't know that I
            had to tell Blender how many closures I was using 😅, so all
            Final Renders returned black hair. This was found by YAFU on
            BlenderArtists and fixed by Brecht.
      - Enabled Filter Glossy for the roughness coefficients. Brecht
        reports a \~75% improvement in render quality\!
        ([rB8087cefd](https://projects.blender.org/blender/blender/commit/8087cefd03022759250f5d62eb4776fa6c55be4c),
        [rBdf1e50fa](https://projects.blender.org/blender/blender/commit/df1e50fa5d913d2a577a178d3f26f573795997ad))
      - Finally found out why Blender didn't listen to RNA's default for
        the hair parameterization. I added the correct initialization
        code
        ([rB7780d306](https://projects.blender.org/blender/blender/commit/7780d306a416e0dcc37932698e70595bce58ea5e)).
      - Merged the latest `master` commits. Only a single conflict\!
        (Week 2:
        [rB82e63de2](https://projects.blender.org/blender/blender/commit/82e63de2842c2af8be5ec19a00752b110dbd0c57),
        Week 3:
        [rBfede34a5](https://projects.blender.org/blender/blender/commit/fede34a58a8df630bb927ab9c017ab2cd87478b4))
      - Bonus:
          - Turned off the Diffuse override. It doesn't seem to be
            needed after Brecht's `curve_core_intersect` patch
            ([rB718d730b](https://projects.blender.org/blender/blender/commit/718d730bb409322dd9078273bfc0dc16b956a8d6)).
          - Added our shader to the Glossy closure checker as well
            ([rB25677d77](https://projects.blender.org/blender/blender/commit/25677d7738530d8f827e5ac960ccded6b128d897)).
  - Brecht:
      - Fixed the hair offset calculation by avoiding
        `curve_core_intersect` & pals altogether
        ([rBabfca937](https://projects.blender.org/blender/blender/commit/abfca9375c6ae75320b7538d547545350ad47c85),
        [rBff6883fb](https://projects.blender.org/blender/blender/commit/ff6883fbc330c5ac1333beaf38142d6b1d7728cf),
        [rBbaad410a](https://projects.blender.org/blender/blender/commit/baad410a5c5b15158b06d9b9a9d9c86522261cea)).
      - Added a few comments wrt. hair attenuation terms and offset
        angles
        ([rBfc62d5a3](https://projects.blender.org/blender/blender/commit/fc62d5a3b3622efa69a02377bc15ab6f6f1e389c)).
      - Fixed the range for the R-mode roughness modifier
        ([rB3c56635d](https://projects.blender.org/blender/blender/commit/3c56635da9579a297f3b32ce2cc0b9468d308199)).
      - Fixed the number-of-closures issue that came with the R-mode
        roughness modifier
        ([rB1fac3b7d](https://projects.blender.org/blender/blender/commit/1fac3b7da05ada27d8a41c114283394b5a505ebb)).

Here are some images:

![A R-mode-modified fur ball, also filtered with Filter Glossy. Rendered
with Cuda on Win7 x64 in
\~5min.](../../../../images/hair-ball-glossyfiltered.png
"A R-mode-modified fur ball, also filtered with Filter Glossy. Rendered with Cuda on Win7 x64 in ~5min.")

![The node's current appearance and
sockets.](../../../../images/ZootopiaShaderNode-week-3.png
"The node's current appearance and sockets.")

## Next up

Apart from finally settling the memory allocation flow for the OSL
shader, the original shader is mostly done. I could address some of the
proposed features in [the BlenderArtists
thread](https://blenderartists.org/t/cycless-gsoc-2018-project-new-hair-shader-expectations/1103070?u=amyspark):

  - Define separately the eumelanin/pheomelanin coefficients, *à la*
    Arnold's [Standard
    Hair](https://support.solidangle.com/display/A5AFMUG/Standard+Hair?desktop=true&macroName=center)
  - Perhaps add more features, such as tinting? Again, following
    Arnold's shader features.

If no more bugs arise, I may start with the rest of the objectives too.

## Questions

None this time\!
