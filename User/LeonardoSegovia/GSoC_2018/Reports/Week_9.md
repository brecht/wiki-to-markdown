# Week 9

Hi everyone\! This is my report for the present week.

## Objectives

This week, I dealt with the objectives for Weeks 10 and 11:

  - User documentation revision.
  - Developer documentation revision.

## What's been done

On Monday, Brecht did a revision of the existing code. These were the
comments (summarized from the soc-2018-dev mail):

  - We confused "Primary Reflection Roughness" with "Undercoat
    multiplier" (section 4.3 of Chiang's paper).
      - He suggested to rename the first parameter to just "Coat", make
        it `(1 - primary_reflection_roughness)` and default to `0.0.`
  - We should linearize Melanin concentration into a coefficient in the
    range `0-1` and adjust the absorption factors accordingly.

And the encountered bugs and quirks:

  - Brecht:
      - The OSL shader is using the longitudinal roughness instead of
        the azimuthal roughness for the reflectance to absorption
        mapping.
      - Separate RGB-\>absorption coefficient conversion into its own
        function.
      - The Random socket should use `SOCK_HIDE_VALUE` to make it clear
        the value is ignored if nothing is connected.
      - In the OSL shader, we should use `lower_case_names` for local
        variables, only the parameters should use `CamelCase`.
      - Before merging, any debug code like `printf`s, `curve_center`,
        and other commented out code should be removed.
      - No need to store a random value in the `PrincipledHairBSDF` and
        have it as a parameter in the OSL closure if we are not going to
        use it.
      - As an optimization, variables needed for only one of the 3 color
        parameterizations should be computed in the appropriate
        `if`/`case` instead of before. So `factor_random_color`,
        `pheomelanin`, etc. should only be computed for the Melanin
        parameterization.
      - Update the Direct coloring parametrization to match the default
        color of the rest.
      - Suggest to rename "Color Randomization" to "Random Color",
        sounds a little less technical and to the point. Same for
        "Roughness Randomization".
      - In nodes.h, rename `float melanin_concentration` to `float
        melanin` and `float melanin_redness_ratio` to `float
        melanin_redness`, these should match the socket names.
  - Leo:
      - It turns out that we computed the primary reflection roughness
        *after* it was converted to the logistic distribution's
        parameters, which utterly defeated its purpose.
      - The OSL shader used not only the wrong, but also the
        unrandomized type of roughness for the reflectance to absorption
        mapping.

After these fixes were done (commits below), I turned to designing the
rest of the manual's charts. The initial versions turned out to be bulky
and lacking, so Brecht advised me to use the scenes provided by
[ChrisWillC from
BlenderArtists](https://blenderartists.org/t/custom-hair-shader/1116458)
for better quality; however, he had to adjust them since they OOM'd my
desktop. This is the agreed design for the charts:

  - Covered parameters: Melanin, Melanin Redness, Roughness, Radial
    Roughness, Coat, Random Roughness,Random Color
      - 5 renders per parameter, 256x256
      - 0..1 for everything except (radial) roughness, where it's
        0.1..0.9
      - also would prefer dark flat background so the small isolated
        hairs are visible
      - 16px font size
      - no text since images are not translatable

Of these, I've done all but for the Random ones. I'll also add examples
of Tint and Direct coloring, just for the sake of completion.

As previously, once I've finished rendering the new stuff and after them
being approved by Brecht, I'll upload the sources to the Phabricator
task for merging.

These were the resulting commits in the Blender repository:

  - The usual merging commits from `master`
    ([rB61912874](https://projects.blender.org/blender/blender/commit/61912874353846774bc4ab0f4a14b7c0257e02f1),
    [rBaedb148e](https://projects.blender.org/blender/blender/commit/aedb148ed4dd47d5a8491e3de0a207cdd3701e30)).
      - I'm holding off on the rest of the week's commits until I've
        finished rendering the charts.
  - Adjusted the upper bounds of Roughness
    ([rB2af5f394](https://projects.blender.org/blender/blender/commit/2af5f394886cb6b33a3bf6da4ac7bda2337f8e51)).
  - Fixed OSL's reflectance mapping
    ([rB3279ef6c](https://projects.blender.org/blender/blender/commit/3279ef6cc79dcb5d10ea37d7db46f3469fa1cad8)).
  - Hide Random socket's value if unused
    ([rB665c6f83](https://projects.blender.org/blender/blender/commit/665c6f8333a8b9c30544e8e501f33e5c74cf3182)).
  - Remove random value from SVM and OSL's closures
    ([rBdb694830](https://projects.blender.org/blender/blender/commit/db69483070545f914f9a604ae2a0e3394ff6b280)).
  - Adjusted OSL's variable naming to the specified convention
    ([rBc25a9c19](https://projects.blender.org/blender/blender/commit/c25a9c19f5e835c56a62acb6574b6a1b4b88bf2f)).
  - Renamed the melanin variables as specified
    ([rBc7b3abd4](https://projects.blender.org/blender/blender/commit/c7b3abd470d241cd90d22656069a2b8e69c15b66)).
  - Added the requested brownish default to Direct coloring
    ([rB58d7bc2c](https://projects.blender.org/blender/blender/commit/58d7bc2ce6435fac7b49a0abaf847e4d02e4b1df)).
      - This one was done empirically, I rendered a curl of default
        (Melanin) brownish hair and selected the best (IMHO) RGB color.
  - Renamed X Randomization to Random X
    ([rB83967579](https://projects.blender.org/blender/blender/commit/839675793b09ec47ba6850c974d6ac51d9773f6d)).
  - Reordered variable initialization and usage
    ([rBadcc63fe](https://projects.blender.org/blender/blender/commit/adcc63fe83aec514d44da839e8f96671e81a6506)).
  - Renamed Undercoat Roughness to Coat
    ([rBed27553d](https://projects.blender.org/blender/blender/commit/ed27553dc0e6ed5dc6292d97f12dd3fd383cedc9)).
  - Melanin linearization
    ([rBeb4abf2c](https://projects.blender.org/blender/blender/commit/eb4abf2ca7a0f38a46ebb122cfcd590f07164ed2),
    [rB370d7f92](https://projects.blender.org/blender/blender/commit/370d7f920f3d68d3c959106196ccf4d7c513dc4f),
    [rB04428920](https://projects.blender.org/blender/blender/commit/04428920c6629a3b32eaafe209122bb0cce3f0d8)).
  - Correct Coat calculation to the Roughness value, not the variance
    ([rBd6f39657](https://projects.blender.org/blender/blender/commit/d6f39657a3be8c331a034065543fecc737e7ca2a)).
  - Extra cosmetic cleanup
    ([rB5652abe0](https://projects.blender.org/blender/blender/commit/5652abe07b2dddaf800651ae2ba96082d9da4146)).

In the GitHub repo:

  - Uploaded basic, initial versions of the charts
    ([b3cfbef5a40ef2beb8aa5043a3a42a072f17206f](https://github.com/amyspark/blender-manual/commit/b3cfbef5a40ef2beb8aa5043a3a42a072f17206f),
    [f23a74f482afb050d3ea57c2974647675690df65](https://github.com/amyspark/blender-manual/commit/f23a74f482afb050d3ea57c2974647675690df65)).
  - Updated them to Inkscape-based versions
    ([70299a8a7eb610095fb57ae338f3b4e6b9849e51](https://github.com/amyspark/blender-manual/commit/70299a8a7eb610095fb57ae338f3b4e6b9849e51),
    [3663a7ae2d6488a421162c93ae0699cc80a944d3](https://github.com/amyspark/blender-manual/commit/3663a7ae2d6488a421162c93ae0699cc80a944d3),
    [07f377c1c7021e10fdf56e89f9925436bc6d31f2](https://github.com/amyspark/blender-manual/commit/07f377c1c7021e10fdf56e89f9925436bc6d31f2)).
  - Applied Brecht's requested parameter renamings and adjustments
    (b01c8e1ce3747ab45230029b03e9a519a5389139\](https://github.com/amyspark/blender-manual/commit/b01c8e1ce3747ab45230029b03e9a519a5389139),
    [c6e56391a6ce11ca2d38e7e587d2c0ab26c9eca4](https://github.com/amyspark/blender-manual/commit/c6e56391a6ce11ca2d38e7e587d2c0ab26c9eca4),
    [6951dadb2fac9d5e944e640cb86e16909f130b34](https://github.com/amyspark/blender-manual/commit/6951dadb2fac9d5e944e640cb86e16909f130b34),
    )
  - Uploaded the final version of Melanin Redness
    ([586f3116b8561d0e6372dc429629ffeeedafdfb2](https://github.com/amyspark/blender-manual/commit/586f3116b8561d0e6372dc429629ffeeedafdfb2)),
    Melanin, Coat, Roughness and Radial Roughness
    (bcf453cb7712e1da114616267674cd2ab47b54ed\](https://github.com/amyspark/blender-manual/commit/bcf453cb7712e1da114616267674cd2ab47b54ed)).

## Next up

  - Finish the Randomization, Tint and Color charts

## Questions

None this time\!
