# Implementing a Hair Shader for Cycles

## Report: Community Bonding period

### BlenderArtists community survey

bzztploink on IRC suggested:

> `2018-04-24 16:12:01 bzztploink you could also think about having a
> thread on blenderartists.org so people can follow your progress and
> give feedback`

Thread is
[here](https://blenderartists.org/t/cycless-gsoc-2018-project-new-hair-shader-expectations/1103070/10).

Summary:

  - A substantial number of users asked if the end result would be a
    principled shader (à la [Disney
    über-shader](http://blog.selfshadow.com/publications/s2012-shading-course/%5D)).
      - Brecht says this will be indeed the case:

> `2018-05-07 17:44:29 brecht I didn't mentioned it explicitly, but the
> idea was for the new hair shader to be called "principled hair"`

> `2018-05-07 17:44:41 brecht so that we have principled
> bsdf/hair/volume`

> `2018-05-07 17:44:52 brecht and user will then immediately understand
> it's an "ubershader" type thing`

  - There were a number of complaints about:
      - The lack of documentation on the current shader. There is no
        explanation on how to setup a proper hair material, only the
        physical meaning of the parameters and that’s it.
      - The usability of the current shader. Some artists try to make
        the most of it by using complex nodegroups, but that’s a stopgap
        measure at best.
  - The primary concern was about the shader’s interface:
      - Would the modes (R, TT, TRT) stay separated?
      - Intuitive parameters

### Development environment preparation

I’ve setup MS Visual Studio 2017 on Windows 7 x64. I had CUDA installed
from an earlier postgrad course, so no news on that front.

I tried to follow [the BlenderWiki
page](https://wiki.blender.org/index.php/Dev:Doc/Building_Blender/Windows)
on the subject. The make script finds only the 32-bit toolchain, even if
called from within the 64-bit Build Tools CLI. I ended up following
[Manual CMake
Setup](https://wiki.blender.org/index.php/Dev:Doc/Building_Blender/Windows#Manual_CMake_Setup)
to force a fully x64 build toolchain, using the CMake GUI to finish off
the process.
