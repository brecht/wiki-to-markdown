# Week 4

Hi everyone\! Yet another week's now gone. This is my report.

## Objectives

This week, I think we've finished the Weeks 4-5 objectives:

  - Test shader functionality. If working, release test build.
  - Evaluate received feedback. Start bug fixes.

## What's been done

  - Added some documentation to the node's parameters
    ([rB9973c2c2](https://projects.blender.org/blender/blender/commit/9973c2c26f3554660546dd3452ae1234f447a00b)).
  - Separated each mode's parameters into their own sockets.
      - Melanin:
        [rB31c3476f](https://projects.blender.org/blender/blender/commit/31c3476fd31c2e497a3508e3b2390a3f9c18d945)
      - Direct coloring:
        [rB30369e55](https://projects.blender.org/blender/blender/commit/30369e55c1458832b4057bf94a78a66fee2e15f7)
      - Absorption coefficient:
        [rB85476430](https://projects.blender.org/blender/blender/commit/8547643039a22cc998479b016660d54079851ea8)
  - It seems that d'Eon's melanin mapping works as an absolute quantity,
    not as a ratio, so I lifted the established max melanin.
    ([rB6d8eb3bd](https://projects.blender.org/blender/blender/commit/6d8eb3bdbceb09da8229946461eaff377e395e44))
  - Added PBRT's and Benedikt Bitterli's sane defaults
    ([rBb891aa81](https://projects.blender.org/blender/blender/commit/b891aa81094707c36504b28b40ace7996401be3c),
    [rB257a7701](https://projects.blender.org/blender/blender/commit/257a7701e04db99bd5c9def75729dc07042bcb47))
      - What does this mean? By default, the shader should return in all
        its modes brownish hair.
      - I decided to follow Bitterli's defaults wrt. melanin
        concentration because his reference render (in Tungsten) used
        pheomelanin to achieve the desired colors.
  - Added the ability to dye hair
    ([rB6e8ab6d2](https://projects.blender.org/blender/blender/commit/6e8ab6d2139590c5454f146746dc8d990b4d7e84)).
      - To use it, simply set the melanin to a low quantity and then
        choose the desired color.
      - This ability was inspired in [the new V-Ray hair
        shader](https://www.chaosgroup.com/blog/v-ray-next-the-science-behind-the-new-hair-shader).
      - I don't know if the way I did it is exactly how it should work,
        but it surely looks nice\!
  - Added Melanin and Roughness randomization
    ([rBf1bcaabb](https://projects.blender.org/blender/blender/commit/f1bcaabb8ad141356aa217bd3f0b49b684a23f5b),
    [rBb7d540f2](https://projects.blender.org/blender/blender/commit/b7d540f245aa4adf48ea0481f686a46cbc812ec1),
    [rBc33fbdb9](https://projects.blender.org/blender/blender/commit/c33fbdb9cc68bc7dd8f7e14cba9eece4d1b19e12),
    [rB1eb55b6d](https://projects.blender.org/blender/blender/commit/1eb55b6d12bd5741c866d0406b8e37088db01166),
    [rB332b2d62](https://projects.blender.org/blender/blender/commit/332b2d6206c128bd7ac2474c8b80a55a1fcc64af))
      - This lets you add further realism by varying the properties of
        each strand.
      - To use it, set the "(Parameter) Randomization" factor to how
        much (less) of the desired factor you wish your hairs to have.
        This is implemented as a linear progression, e.g. 0 = no
        randomization at all, 0.5 = hairs can have as much as 50% less
        melanin or roughness, 1 = completely random up to the specified
        value.
      - Do notice that this needs Hair Info-\>Random or other number
        source to work.
      - If you wish to disable this feature, EITHER set the
        Randomization factor to 0, OR unsocket the random source and set
        Random to 0.5.
  - Merged the latest `master` commits
    ([rB9e59a1f7](https://projects.blender.org/blender/blender/commit/9e59a1f7b5c0aca666cda737936d9fb5621423f4)).
      - Two formatting issues were fixed.

The node now looks like this:

![The node's current appearance and sockets for the Direct Coloring
mode.](../../../../images/ZootopiaShaderNode-DirectColoring-week-4.png
"The node's current appearance and sockets for the Direct Coloring mode.")

![The node's current appearance and sockets for the Melanin
Concentration
mode.](../../../../images/ZootopiaShaderNode-MelaninConcentration-week-4.png
"The node's current appearance and sockets for the Melanin Concentration mode.")

![The node's current appearance and sockets for the Absorption
Coefficient
mode.](../../../../images/ZootopiaShaderNode-AbsorptionCoefficient-week-4.png
"The node's current appearance and sockets for the Absorption Coefficient mode.")

nirved on IRC provided me with a reference scene that implements
Bitterli's "Hair Curls". These images were rendered with the following
properties:

  - For all hair curls:
      - Offset: 2.5º
      - Roughness: 0.3 azimuthal and longitudinal
      - Primary Reflection Roughness: 1.0 (i.e. disabled)
      - Randomness: left 0 (i.e. disabled), 1 right (i.e. fully
        randomized)
      - Hair width: 2
      - Hair strands: 1 parent, 1000 children, Simple
      - Random source: Hair Info -\> Random -\> Modulo 0.05 -\> Multiply
        20
          - Reduced the number of possible variations in exchange for
            more distinctiveness
  - For each curl:
      - Dark: Melanin 0, Melanin Redness 8
      - Brown: Melanin 0, Melanin Redness 3
      - Red: Melanin 0, Melanin Redness 1.3
      - Blond: Melanin 0, Melanin Redness 0.35
  - Sampling: Branched path tracing, 784 samples (Square), max 64
    bounces, Clamp Indirect 25, and Denoised (default parameters, but
    enable for all modes)

![nirved's scene rendered with no randomized
parameters.](../../../../images/nirved-HairCurls-norandom.png
"nirved's scene rendered with no randomized parameters.")

![nirved's scene rendered with 10%
randomness.](../../../../images/nirved-HairCurls-10.png
"nirved's scene rendered with 10% randomness.")

![nirved's scene rendered with 25%
randomness.](../../../../images/nirved-HairCurls-25.png
"nirved's scene rendered with 25% randomness.")

![nirved's scene rendered with 50%
randomness.](../../../../images/nirved-HairCurls-50.png
"nirved's scene rendered with 50% randomness.")

![nirved's scene rendered with 100%
randomness.](../../../../images/nirved-HairCurls-100.png
"nirved's scene rendered with 100% randomness.")

## Next up

Depending on user feedback, I may change the existing melanin mapping
from *absolute concentration* to *ratio of eu/pheomelanin* v.
*concentration factor*. I don't know what maximum should we choose or
the relationship between concentration factor and resulting absolute
quantity (linear? logarithmic?). There is no data on this in either
Pharr's or d'Eon's paper.

Please note that the university has asked me to prepare a public class
as part of the examination process for my old TA post (lapsed at the end
of March, prior to GSoC). Therefore, next week I may not be available
immediately for bug fixes and new features.

If no bugs or feature requests arise in the next weeks, I'll begin
designing tests. In the BA thread there have been requests for .blend
hairstyles for this purpose -- it'll be great if you could post them\!

## Questions

  - Do you feel the current features are sufficient and/or correct for
    usage by artists?
      - What would you add, change, or remove, if anything?
