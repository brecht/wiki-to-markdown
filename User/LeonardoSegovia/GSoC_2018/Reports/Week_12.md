# Week 12

Hi everyone\! This is my report for the present week.

## Objectives

In a followup to the report for Week 10, Brecht requested to test the
Russian roulette termination criteria to see if it introduced noise.

## What's been done

  - Apart from increasing the render times, no improvements were found
    (the indirect glossy pass still resulted in noise).
  - The debugging code was removed
    ([rB20b2af9c](https://projects.blender.org/blender/blender/commit/20b2af9cf5a8bb6b15c6652a02665dcd67eec3f6)).
  - I employed the remaining time detailing this work in my MSc thesis.

## Next up

I'll leave the `soc-2018-hair-shader-fixes` branch as is, if its Filter
Glossy commit is needed.

## Questions

None this time.
