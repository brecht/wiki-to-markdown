# Week 10

Hi everyone\! This is my report for the present week.

## Objectives

This week, I finished the objectives for Weeks 10 and 11:

  - User documentation revision.
  - Developer documentation revision.

On Wednesday, Brecht merged our deliverables into Blender\! Yay\!

## What's been done

Leftovers from the past week:

  - Uploaded the remaining charts
    ([e822298052db8d33fade8e9b12cca7d5890ebc7a](https://github.com/amyspark/blender-manual/commit/e822298052db8d33fade8e9b12cca7d5890ebc7a)).
  - Improved node screenshot padding
    ([93ac08022f9adf132916d480e0a8ce74fa5d90ab](https://github.com/amyspark/blender-manual/commit/93ac08022f9adf132916d480e0a8ce74fa5d90ab)).
  - Relocated the charts to beneath their parameters
    ([1892430016fe097d528544cecd683957ed9da642](https://github.com/amyspark/blender-manual/commit/1892430016fe097d528544cecd683957ed9da642)).

On the Blender repo:

  - I performed the final cleanup
    ([rB28dfe13c](https://projects.blender.org/blender/blender/commit/28dfe13cd85bd714fc1cf22dfe5792b8e156816e)).
  - Brecht merged the shader into `master`
    ([rB5078b9d2](https://projects.blender.org/blender/blender/commit/5078b9d2d08a34ae3786100c2301ea960165e7f2),
    [rBM4368](https://projects.blender.org/UNSUPPORTED/blender-manual-translations-svn/commit/4368)).

## Next up

  - The regression test suite has not been uploaded yet to SVN.

## Questions

About the shader itself:

  - It's been asked in the BA thread if I have extra objectives for the
    shaders. Do you feel that more features/controls should be added?
      - I came up with controls for each of the modes (R, TT, TRT,
        TRRT+).
      - It's been also suggested to prune the UI into a "Basic" and
        "Advanced" mode.

About the GSoC project deliverables:

  - If I read the mail correctly, Google wants us students to summarize
    and show off our work in a single place e.g. a blog post. How does
    Blender handle this?
      - My preference would be to add a page (similar to when one
        publishes a paper) in amyspark.me explaining the work we did. We
        could embed the resulting pictures, credits, etc. and the
        demonstration video from Youtube.
      - As for "Get the code", perhaps I could add links to the relevant
        commits?
