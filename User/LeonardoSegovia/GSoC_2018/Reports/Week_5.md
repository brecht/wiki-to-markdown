# Week 5

Hi everyone\! This is my report for the present week.

## Objectives

This week I dealt with Weeks 4-5 objectives:

  - Test shader functionality. If working, release test build.
  - Evaluate received feedback. Start bug fixes.

I released two builds this week:

  - one with the Week 4 features
  - the other almost mid-week after due to a bug (more on this later\!).

Remember that I was partially out on Mon-Tue because of a public class I
had to prepare.

## What's been done

  - In the BA thread, Zanzio found that renders were wildly inconsistent
    between CPU and GPU. This became noticeably if rendering with both
    at the same time.
      - Bug \#1 was a precision issue when normalizing the coordinate
        frame in the presence of low roughness values
        ([rBe2671913](https://projects.blender.org/blender/blender/commit/e267191393fe5dbde91d5b428f4a6adaf6a3fa8f)).
      - Bug \#2, and the true reason for this, was that somehow the GPU
        kernel was reordering the second set of values. I don't know why
        it had such an effect, but it's been fixed. Thanks to nirved
        again for noticing\!
        ([rB2d1eef87](https://projects.blender.org/blender/blender/commit/2d1eef87f6b61a4a2c8ff476d290c9d841661f6b))
  - Did a bit of cleanup: store and retrieve consistently the values
    from the stack
    ([rBdc2827fd](https://projects.blender.org/blender/blender/commit/dc2827fd907d6c3a742ea2292e588521e2be55f9)).
  - Merged the latest `master` commits
    ([rB60a59fba](https://projects.blender.org/blender/blender/commit/60a59fbae6b04f59e91f540eabc52102f354cd0a),
    [rB535e2e4a](https://projects.blender.org/blender/blender/commit/535e2e4ade24801c123017ad5b1fe8e580c1d382),
    [rB9b632add](https://projects.blender.org/blender/blender/commit/9b632addd34758035331399e69b3f619bc6d4b16).
      - Lukas changed the `linear_rgb_to_gray` function signature.
        ([rBbd37b18a](https://projects.blender.org/blender/blender/commit/bd37b18a0d6d33d2be3a2e96d1928356e85a0e2a))

## What I'll do later today, and during the weekend

Today, Brecht, Lukas, nirved and I discussed on IRC the current state of
the shader. This is a summary of the proposed changes:

  - Rename the Roughness parameters to (Longitudinal) "Roughness",
    "Radial Roughness" and "Undercoat Roughness"
  - Remove the "Physical" parameterization
  - Rename and reorder the rest to "Color" ("Direct coloring") "Melanin"
    ("Melanin Concentration"), and "Absorption coefficient"
      - The latter remains for debugging purposes e.g. if someone wants
        to use the coefficients straight from the papers
  - Remap Melanin concentration to represent:
      - Melanin -\> quantity
      - Melanin Redness -\> distribution
      - Brecht asked if Melanin could be done as a percentage of
        saturation (0 = white, 1 = black). I know it can be done that
        way (V-Ray), but there is no public, reliable mapping; for
        example, Tungsten's melanin values have a similar
        representation, but Bitterli still uses d'Eon's mapping, so the
        Melanin value is still an absolute concentration quantity.
  - Redo the randomization to a relative tolerance
      - Formula: `effective_roughness = clamp(roughness * (1.0f +
        (random - 0.5f) * roughness_randomization/100.0f), 0.0f, 1.0f)`
      - i.e. when randomized, the roughness value should be `roughness ±
        roughness_randomization%`-
  - Implement randomization by directly retrieving Hair Info -\> Random
    unless the user sockets a different source.
      - This is easy with OSL (`geom:curve_random`), with SVM... not so
        much. I do have an idea, thanks to Principled Volume.
  - Reorder the sockets as follows:
      - Color
      - Roughness
      - Azimuthal Roughness
      - Undercoat Roughness
      - IOR
      - Offset
      - Randomize Color
      - Randomize Roughness
      - Random

## Next up

If no bugs or feature requests arise in the next weeks, I'll begin
porting the existing hair tests to the new shader. I reiterate the
previous request for .blend hairstyles for this purpose -- it'll be
great if you could post them to the BA thread\!

## Questions

None this week\!
