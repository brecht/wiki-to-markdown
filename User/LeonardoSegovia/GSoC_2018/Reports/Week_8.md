# Week 8

Hi everyone\! This is my report for the present week.

## Objectives

This week, I dealt with the objectives for Weeks 10 and 11:

  - User documentation revision.
  - Developer documentation revision.

I got my desktop back from the tech on Wednesday, and yesterday I had a
French test, so I could only resume work today.

## What's been done

  - The usual merging commits from `master`
    ([rB7551a038](https://projects.blender.org/blender/blender/commit/7551a0389c5929ddf6feaeac3a203987907e1988),
    [rBaedb148e](https://projects.blender.org/blender/blender/commit/aedb148ed4dd47d5a8491e3de0a207cdd3701e30)).
  - Same as above, but for the Blender manual
    ([7a6ae1b6d279cfbd58a05a598c36f6730b3c2bac](https://github.com/amyspark/blender-manual/commit/7a6ae1b6d279cfbd58a05a598c36f6730b3c2bac),
    [a248590db512159febfa1998fcb36bf0c548a0ae](https://github.com/amyspark/blender-manual/commit/a248590db512159febfa1998fcb36bf0c548a0ae))
  - The missing documentation for Python and RNA has been added, along
    with extra cleanup
    ([rB98357a85](https://projects.blender.org/blender/blender/commit/98357a85c8f024fcfc7dca01f43d60ab0f781d02)).
  - The first chart is now up, Melanin Redness
    ([b3cfbef5a40ef2beb8aa5043a3a42a072f17206f](https://github.com/amyspark/blender-manual/commit/b3cfbef5a40ef2beb8aa5043a3a42a072f17206f))
      - This one was based on nirved's port of the Hair Curls scene.
      - I reduced the curls and tweaked the camera a bit to get the
        extra space needed for the text.

## Next up

  - Finish the Roughnesses and Randomization charts
  - Reference renders with the custom shader

## Questions

I'd be grateful if you could review and post your opinions on the
different charts. I'll be uploading them during the weekend, so that on
Monday we can get reasonable reviews.
