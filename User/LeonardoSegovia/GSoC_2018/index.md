# Implementing a Hair Shader for Cycles

Branch:
[soc-2018-hair-shader](https://developer.blender.org/diffusion/B/repository/soc-2018-hair-shader/)

GSoC site:
<https://summerofcode.withgoogle.com/projects/#4664546451521536>

Thread on Blender Artists:
<https://blenderartists.org/t/cycless-gsoc-2018-project-new-hair-shader-expectations/1103070>
([originally
here](https://blenderartists.org/forum/showthread.php?449353-Cycles-s-GSoC-2018-project-new-hair-shader!-Expectations))

## [ Proposal](Proposal.md)

## [ Reports](Reports/index.md)
