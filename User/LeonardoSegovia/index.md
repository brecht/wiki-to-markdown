# Wiki page of L. E. Segovia (amyspark)

## Projects

  - [ Google Summer of Code
    2018](GSoC_2018/index.md)

## About

I'm studying the [MSc in Computer Science](https://cs.uns.edu.ar) at the
[Universidad Nacional del Sur](https://www.uns.edu.ar) in Bahía Blanca,
Argentina.

  - IRC: amyspark
  - Website: [amyspark.me](https://www.amyspark.me)
