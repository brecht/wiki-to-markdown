## ThatAsherGuy's User Page

I'm that Asher guy. I'm a technical writer. My user page is an
uninformative stub full of de-contextualized notes I don't remember
writing and half-baked ideas I should probably abandon.

That'll change. Eventually.

### Pages I Maintain:

Nothing at the moment; still onboarding and setting up my scaffolding.

### Pages I'm Working On:

My current project focuses on migrating, clarifying, and (eventually)
updating Blender's design documentation. I'm currently on the first
step, migrating documents from [the old
wiki](https://archive.blender.org/wiki/index.php/Main_Page/) to this
one.

  - [UI Paradigms Draft](UI_Paradigms.md)
      - [Archive
        Source](https://archive.blender.org/wiki/index.php/Dev:2.5/Source/UI/UIParadigms/)
  - [Sidebar Tab Guidelines
    Draft](Sidebar_Tab_Guidelines.md)
      - [Archive
        Source](https://archive.blender.org/wiki/index.php/Dev:Doc/Projects/UI/Tab_Guidelines/)

### Snippets and Examples:

I've got a thing for prototyping with add-ons. They're rarely useful in
real-world workflows, but they can serve as semi-decent design mockups.

  - [Operator-Based Select Action
    Fallback](https://github.com/M-Asher/blender-dynamic-fallback)
