## Who

My real name’s Bastien Montagne, I was born in 1984, and I live in
France.

I discovered Blender (v2.23…) a few years ago in a magazine, and I’m
using it mainly for little animations, and sometimes playing with fancy
features like fluidsim, softbodies, …

## What

### [Dev](User:Mont29/Dev)

#### Propagate Render Settings py addon

I have created a small python addon to automatically propagate some of
the render settings between different scenes.

See [this
page](https://wiki.blender.org/index.php/Extensions:2.5/Py/Scripts/Render/Copy_Settings).

#### Select Sequence Strips by Type py addon

I have created a small python addon to allow one to select the
sequencer’s strips from their type (audio, scene, video, effect,
etc.).

**Now in trunk, as C code, in a slightly different form\!**

#### WeightVGroup modifier

I have created new modifiers, which affects the weights of a vertex
group.

**Now in trunk\!**

See [this page](User:Mont29/Dev/WeightVGroup).

#### UI Template List Enhancement Proposal

This is a small modification of the Template List C code, to enable
generic inclusion of controls in the list’s items (as it is done,
hard-coded, for e.g. materials, vertex color layers, etc.).

**Now in trunk\!**

See [this
page](User:Mont29/Dev/UI_Template_List_Enhancement).

#### Enhanced pose bones rot/scale parenting

This proposition replaces current Hinge/No Scale options by the ability
to choose any upper bone in the chain as rotation and/or scale parent.

See [this
page](User:Mont29/Dev/Pose_Bone_RotScale_Parenting).

### Wiki

I have translated and/or I’m trying to maintain the following chapters
in french:

  - [Interaction
    in 3D](https://wiki.blender.org/index.php/Doc:FR/Manual/3D_interaction).
  - [Data
    system](https://wiki.blender.org/index.php/Doc:FR/Manual/Data_System).
  - [Modeling](https://wiki.blender.org/index.php/Doc:FR/Manual/Modeling).
  - [Modifiers](https://wiki.blender.org/index.php/Doc:FR/Manual/Modifiers).
  - [Lighting](https://wiki.blender.org/index.php/Doc:FR/Manual/Lighting).
  - [Materials](https://wiki.blender.org/index.php/Doc:FR/Manual/Materials).
  - [Textures](https://wiki.blender.org/index.php/Doc:FR/Manual/Textures).
  - [Worlds &
    Backgrounds](https://wiki.blender.org/index.php/Doc:FR/Manual/World).
  - [Animation
    Basics](https://wiki.blender.org/index.php/Doc:FR/Manual/Animation/Basic).
  - [Character Animation &
    Armatures](https://wiki.blender.org/index.php/Doc:FR/Manual/Animation/Armatures).
  - [Constraints](https://wiki.blender.org/index.php/Doc:FR/Manual/Constraints).
  - [Shape Keys Advanced
    Animation](https://wiki.blender.org/index.php/Doc:FR/Manual/Animation/Advanced/Driven).
  - [Effects & Physical
    Simulation](https://wiki.blender.org/index.php/Doc:FR/Manual/Physics).
  - [Rendering](https://wiki.blender.org/index.php/Doc:FR/Manual/Render).
  - [Nodes
    Compositing](https://wiki.blender.org/index.php/Doc:FR/Manual/Compositing).
  - [VSE](https://wiki.blender.org/index.php/Doc:FR/Manual/Sequencer).
  - [Extending
    Blender](https://wiki.blender.org/index.php/Doc:FR/Manual/Extensions).
  - [Blender Game
    Engine](https://wiki.blender.org/index.php/Doc:FR/Manual/Game_Engine).

I also occasionally correct/write precisions on English pages I
translate, and have done a full rewrite of Lighting, Modeling,
Animation, Rigging and Constraints chapters (some still in review
process…).

I’ve done the refactoring of the
[Lighting](https://wiki.blender.org/index.php/Doc:Manual/Lighting)
section.

The refactoring of the
[Modeling](https://wiki.blender.org/index.php/Doc:Manual/Modeling)/[1](https://wiki.blender.org/index.php/Doc:Manual/3D_interaction/Manipulation_3D_interaction/Manipulation)
sections ([the project
page](User:Mont29/Modelling_Refactor/Manual)).

The refactoring of the
[Animation](https://wiki.blender.org/index.php/Doc:Manual/Animation)/[Rigging](https://wiki.blender.org/index.php/Doc:Manual/Rigging)/[Constraints](https://wiki.blender.org/index.php/Doc:Manual/Constraints)
sections ([the project page](User:Mont29/NewAnimation)).

#### Wiki Internals

I’ve contributed to the new Naiad skin, coding some PHP extensions.

  - A (conditional) cache storing “half-parsed” wiki text and data in
    the cache, with optional conditions to know when to recompute cached
    data.
  - A customized, cachable version of the Tree And Menu extension.

I’ve also worked on the [Glossary
project](https://wiki.blender.org/index.php/User:Mont29/Glossary_Extension).

-----

If you want to contact me, please use my [discussion
page](User_Talk:Mont29).

  -   
    \--[Mont29](index.md) 15:41, 18 September 2011 (CEST)

-----

[:Special:Prefixindex](:Special:Prefixindex)
