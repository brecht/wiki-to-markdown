# Blender Foundation

Everything related to BF work…

[:Special:PrefixIndex/User:Mont29/Foundation/](:Special:PrefixIndex/User:Mont29/Foundation/)

-----

**Simple** projects do not always mean “quick to do”, but rather that
they are well defined and should not reserve much surprises. ;)

**Complex** projects mean work that will take time, but also that is
more challenging (either complex algos, or spreading rather wide
throughout Blender code, etc.).

## Current Work

### Simple

…

### Complex

#### FBX

Current exporter/importer status can be found
[here](Extensions:2.6/Py/Scripts/Import-Export/Autodesk_FBX).

Binary format is documented on
<http://code.blender.org/index.php/2013/08/fbx-binary-file-format-specification/>.

Also working on documenting the [file
structure](User:Mont29/Foundation/FBX_File_Structure)…

#### Split Vertex Normals

Decided to chose this one as “big topic” (also involves tangent space),
after a small talk with Campbell. See
[here](User:Mont29/Foundation/Split_Vertex_Normals).

## TODOs, Ideas…

### Simple

#### Add Renaming in UIList - DONE 2.70

Be able to rename items directly inside UIList. Should not be *that*
difficult, but not trivial either (have to figure out what widget to
use, and how, UILists are a bit tricky).

#### Resize Preview - DONE 2.71

Add the same dragg-resize stuff to material/texture/etc. previews that
we have for UILists or scopes.

### Complex

#### Holes in Polygons

There is some code in BMesh about that, but deactivated currently. Issue
here is that, as far as I know, many tools would need to be updated to
handle such polygons correctly… Would need to talk about it with
Campbell, though.

#### Multi-objects Editing

Another requirement from users (esp. about UVs). Probably even worse
than previous topics, as in Blender we always edit one object at a time.
Do we even want to try to support that?
