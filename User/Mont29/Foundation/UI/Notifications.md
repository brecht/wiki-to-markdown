# Enhanced Notifications

*Disclaimer: this is only rough stub idea so far, real design is fully
TODO\!*

Current system has become too much rigid in several cases, which leads
to either send or catch very generic notifications. This means we often
end updating much more than what is actually needed in UI.

See for example [\#48813](http://developer.blender.org/T48813), and
[Campbell’s suggestion](https://developer.blender.org/T48813#380970) to
address it.
