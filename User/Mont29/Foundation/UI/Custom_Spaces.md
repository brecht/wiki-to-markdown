# Custom Spaces

*Disclaimer: this is only rough stub idea so far, real design is fully
TODO\!*

Idea is to allow creation of custom spaces from scripts.

This would work in similar way to panels or UILists - but for whole
spaces (so you define a Space class, fill it with all needed code,
register it, and it shows up in Space Types menu).

Needless to say this is not a small project, complete space is much more
complex than a simple panel or UI widget.

It should allow some much more advanced customization of Blender UI
(also in relation with the Workflow and Blender 101 ideas), especially
if combined with a new, powerful OpenGL drawing API? Or even with the
'OGL renderers' concept?
