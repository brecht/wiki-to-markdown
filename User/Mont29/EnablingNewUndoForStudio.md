New undo system aims at fixing the main slowness causes of undo in
Object and Pose modes.

To use it, please enable, in your preferences:

  - In the \`Interface\` tab, display panel, the \`Developer Extras\`
    option;
  - Then, in the \`Experimental\` tab that appears, the \`Undo Speedup\`
    option.

Don't forget to save the settings.

Some notes:

  - While there is no known case currently, there is still a (very
    small) risk of file corruption, please be very careful with your
    savings, ideally always check that they reload properly first.
  - Crashes on undo/redo are more likely to happen, so do save regularly
    though.
  - The speed improvements are not yet working in all cases, some
    undo/redo steps will remain about as slow as in the old system (this
    is still being worked on).
  - This new system work on Object and Pose mode undos. Editing and
    sculpting modes use their own code and shall not be affected e.g.
