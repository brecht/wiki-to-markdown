## Project Schedule

Before official begin:

  - Initial OBJ exporter, with support for:
      - Vertices
      - UVs
      - Normals
  - Applying modifiers and triangulating the mesh
  - Moved common code to `common.cpp`
  - Started STL exporter, to evaluate what needed to be made generic

### 1st week (27/05 - 03/06)

  - Added axis remapping
  - Fixed compile bug where `BKE_mesh.h` depends on `BLI_sys_types.h`
    but doesn’t include it directly
  - Added option to disable deduplication object, which might be faster
    but produces larger files

### 2nd week (03/06 - 10/06)

  - Major refactoring to use C++ style iterators (based on
    `boost::iterator_facade`)
  - Working on STL export
  - Lots of bug fixing related to iterators
  - Review of the iterator code and some documentation
  - Bug fixing in STL export
  - Fixed UI for STL, making it separate from OBJ
  - Added binary STL

### 3rd week (10/06 - 17/06)

  - Ran clang-format
  - Bug fixing
  - Merged master (bug I was trying to fix was due to a regression in
    master)
  - Refactoring iterators to not depend on `boost::iterator_facade`
  - Temporary code to instead just copy the data, instead of using
    iterators, to test performance. \~50% hit and higher memory usage

### 4th week (17/06 - 24/06)

  - Testing performance of using C-style output on OBJ. Very
    significant, especially on Windows (32.4s to 19.4s)
  - Fully removed the `boost::iterator_facade` dependency
  - OBJ exporter handles materials and animations
  - Added path mode to materials, which controls if and how image
    textures are copied
  - Added NURBS export

### 5th week (24/06 - 01/07) - 1st evaluation

  - Refactored ExportSettings so settings specific to each exporter are
    stored in another struct, with a pointer in the original
  - Added unit scaling
  - UNRELATED Had to look for an appartment, after being given a short
    notice to leave
  - Writing this schedule
  - Bug fixing in `normal_iter`
  - Refactor to use depsgraph iterator for obtaining objects, per
    Sybren’s suggestion
  - Explore current crashes

### 6th week (01/07 - 08/07)

  - Fix `get_final_mesh`, as it doesn’t seem to apply transforms
  - Add exporting of vertex groups, smooth groups
  - Add option to keep vertex order
  - Add option to sort faces by vertex group, material, etc, as per the
    python OBJ exporter
  - Add operator presets

### 7th week (08/07 - 15/07)

  - Write tests and ensure everything in the OBJ exporter works
  - Understanding previous attempts at writting an OBJ importer
  - Start implementing the OBJ importer

### 8th week (15/07 - 22/07)

  - Continued work on the OBJ importer

### 9th week (22/07 - 29/07) 2nd evaluation

  - Continued work on the OBJ importer

#### Present

### 10th week (29/07 - 05/08)

  - Understand the Blender threading interface
  - Refactor to make each section of exporting write to a temporary
    buffer, enabling concurrency of exporting either between different
    objects or between different properties of each object (vertices,
    normals, uvs). This could be done by representing an export job with
    a type, an iterator, and a resulting buffer, and adding to a queue

### 11th week (05/08 - 12/08)

  - Implement PLY exporter and importer
  - Implement STL importer

### 12th week (12/08 - 19/08)

  - Documentation and bug fixes

### 13th week (19/06 - 26/08) - final week

  - Bug fixing

## Open Questions:

Should this whole project be added as an optional dependecy?

How to best compare similarity of vectors?

What order are the vertices of a face stored in? Clockwise?

Should `object_iter` take into account `G.is_break`?

When creating a matrix for rescaling and remaping the object’s axis,
does the homogenous component need to be set to the scale? According to
the implementation in Alembic, it seems to also scale transforms.

How do I apply a transform to the evaluated object (without modifying
the real object)?

How do I flip an object’s normals?

How do I get the version string describing the Blender build?

How do I properly lock the scene while exporting? `G.is_rendering =
true`? `BKE_main_lock(bmain)`?

When exporting, does -0 need to be converted to 0?

Is T47010 still relevant?

How should an object be evaluated? Using
`DEG_get_evaluated_object(settings->depsgraph, base->object)` seems to
break `mesh_create_eval_final`

Do I need to create a new file type for each exporter? What’s preventing
the proper files (i.e. files ending in “.obj” in the case of OBJ) from
showing in the file selector?

Is it ok to add the version info after a \# in STL and ignore it in
importing? The current exporter just uses the place where the object’s
name goes to add the Blender version information, thus losing the object
name

## Original schedule

1st week: Extend the OBJ exporter to include missing functionality like
Nurbs

2nd week: Extend it further to include materials. Regression testing

3rd week: Implement the OBJ importer. Initial exploration of
parallelization

4th week: Implement the ASCII STL exporter and importer. Evaluation

5th week: Extend the framework to generalize common tasks. Bug fixing

6th week: Implement the ASCII PLY exporter and importer

7th week: If parallelization is worth it, implement it

8th week: Implement the binary variants of PLY and STL exporters and
importers. Evaluation

9th week: Refine the framework and bug fixes

10th week: Final documentation of the framework

11th week: Finalizing regression tests

12th week: Buffer week. Finalization and evaluation
