## About

My name is Satish Goda, been using Blender since 2000 and have
contributed few patches. I like documenting and technical writing, and
so here I am.

\- You can contact me on blender.chat at
<https://blender.chat/direct/iluvblender> - My blog focussed on Blender
can be found at <https://learningblender3dsoftware.blogspot.com>

## WIP Documentation

\-
[User:IluvBlender/Source/Render/EEVEE](IluvBlender/Source/Render/EEVEE.md)
