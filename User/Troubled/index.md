## The person

  - Name  
    Danny McGrath
  - On developer.blender.org  
    [dmcgrath](https://developer.blender.org/p/dmcgrath/)
  - On blender.chat  
    [troubled](https://developer.blender.org/direct/troubled)
  - E-mail  
    dan at blender.org
  - Activities  
    Network and server infrastructure
    DNS contact
