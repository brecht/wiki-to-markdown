# Modeling

  - A new **Set Attribute** operator allows basic control over generic
    attribute values in edit mode
    ([6661342dc5](https://projects.blender.org/blender/blender/commit/6661342dc549a8492)).

![../../videos/File\_example\_WEBM\_640\_1\_4MB.webm](../../videos/File_example_WEBM_640_1_4MB.webm
"../../videos/File_example_WEBM_640_1_4MB.webm")

![../../videos/Mesh\_Edit\_Mode\_Set\_Attribute\_Operator.webm](../../videos/Mesh_Edit_Mode_Set_Attribute_Operator.webm
"../../videos/Mesh_Edit_Mode_Set_Attribute_Operator.webm")

  - Edit mesh conversion to evaluated meshes has been parallelized and
    can be over twice as fast depending on the mesh and CPU
    ([rBebe8f8ce](https://projects.blender.org/blender/blender/commit/ebe8f8ce7197)).
