## Production Ready Many Light Sampling

### Name

Sam Kottler

### Contact

Email: <dev@samkottler.net>  
Github: <https://github.com/samkottler>  
blender.chat: samkottler

### Synopsis

This project will continue development on the Many Light Sampling
project from GSoC 2018. It will add support for importance sampling of
more features and make that project production ready by stabilizing the
existing implementation. The goal of the original Many Light Sampling
project was to use multiple importance sampling to avoid having to
sample every light source directly for every shading point. The
implantation is based on a paper \[1\] and makes efficient use of a BVH
data structure to choose which light source(s) a shading point should
sample.

### Benefits

Using multiple importance sampling to selectively choose which lights to
sample will help make Cycles more efficient at rendering environments
with many light sources. This technique could make it possible to render
scenes with millions of light sources (for example, from a mesh) within
a reasonable amount of time. This will give more freedom to artists who
will not need to worry about performance penalties as a result of
including a large number of lights.

### Deliverables

The minimum deliverable for this project would be a production ready
implementation of the previous Many Light Sampling project, including
volume support and extensive testing. This will mostly be adding support
for the algorithm described in \[1\] to Cycles. It will also include a
few user-facing settings such as a checkbox to enable this feature and a
slider to control the splitting threshold. Finally, this project will
provide end-user documentation giving a high level overview of the
technique.

### Project Details

This project has a number of features that will need to be completed to
make the existing implementation production ready.

  - Test on production scenes and fix issues: The 2018 project was not
    meant to completely finish the implementation of this feature. Thus
    one of the most important things to complete to make this feature
    production ready is to test the algorithm on existing production
    scenes and ensure that it still gives a correct result. In the case
    that there are bugs, they will be fixed.
  - Complete volume rendering support: While some work for volume
    rendering was done, it is still very much a work in progress and
    more work and testing need to be done to make it production ready.
  - Investigate importance sampling: One thing that was left incomplete
    during the 2018 GSoC was balancing importance with background and
    portal lights. Additionally, there may have been other improvements
    to the algorithm made in the last two years. This project will
    complete this investigation and make changes to the implementation
    that will increase performance.
  - Complete GPU rendering support and improve importance estimation for
    textured lights: these are more features that were not completed by
    the 2018 project which can improve performance.

### Project Schedule

June 1 – June 15: Familiarize myself with the implementation from the
2018 project, study the details of \[1\], and research any improvements
to this technique that have been made since then  
June 15 – June 29: Test existing implementation and fix bugs  
June 29 – July 27: Add support for volume rendering, including time for
testing with existing production scenes  
July 27 – August 17: Complete GPU support and improve importance
estimation for textured lights, including time for testing  
August 17 – August 24: Buffer time to allow for unexpected delays. If
this is unneeded, this time will be used to add more features to use for
importance sampling.

### Bio

My name is Sam Kottler and I was a double major in math and computer
science at Colorado College, where I am currently the computer science
paraprofessional. In this position I run problem sessions for courses,
help professors lecture, and have time to work on my own projects. I
will be attending the computer science PhD program at the University of
Wisconsin at Madison starting in the fall. During my studies, I have
worked on multiple 3D rendering and animation projects, including
currently working through the book Physically Based Rendering. My
background in math has made it easier for me pick up rendering
algorithms such as Path Tracing and Metropolis-Hastings sampling. I have
also worked on a few GPU rendering projects in both CUDA and OpenCL,
which can be found on my github linked above.

### References

1.  Alejandro Conty, Christopher Kulla, Importance Sampling of Many
    Lights With Adaptive Tree Splitting,
    <https://sites.google.com/site/ckulla/home/many-lights>
