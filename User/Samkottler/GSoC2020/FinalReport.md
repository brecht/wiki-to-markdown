### Final Report

See [this revision](https://developer.blender.org/D8663/) for the code.
My git branch can be found
[here](https://developer.blender.org/diffusion/B/history/soc-2020-production-ready-light-tree-2/).
Note that there was a problem with git that I noticed towards the end of
the project and when fixing it I had to create a new branch. See
[this](https://developer.blender.org/diffusion/B/history/soc-2020-production-ready-light-tree/)
branch for the commit messages before that point.

#### What got done

My task this summer was to complete the light tree implementation from
the 2018 Google Summer of Code many lights sampling project to a point
it could be added as an experimental feature to blender. This task can
be broken up into a few goals:

  - Fix any bugs in the 2018 code
      - Merge master into the 2018 branch so it can actually compile
      - Pass automated render tests with light tree disabled
      - Pass automated render tests with light tree enabled allowing for
        some failures due to noise differences
      - Create a scene to test if the light tree actually improves
        performance
  - Add volume support for light tree sampling
      - Implement importance calculation
      - Implement splitting for volumes
  - Make sure GPU rendering still works

Of these I completed most of them. It took me almost two weeks to merge
the code because I had never seen the code before. After I did this
though I had a pretty good idea of how the light tree fit into the rest
of the code. Then I started looking for bugs. The bugs that I fixed
included fixing a mistake when picking the last not transparent bounce
and calculating the energy for lights needed for the light tree. I also
made a few tweaks including modifying the light tree GUI and decoupling
splitting from the branched path tracer.

I was able to implement the volume code, although I suspect that bugs
will be found as it tested more.

To get the light tree to work on GPUs I needed to rewrite a few
functions to get rid of recursion. I think these functions can be
improved, but I didn't have time to take a look.

#### What is left

My code is mostly ready to merge and as I mentioned above, I have
submitted my code for review. There are a few things that haven't quite
been done.

1.  The energy calculation only works for lights with constant emission.
    Construction of the light tree depends on knowing the maximum energy
    emitted in a direction not the total amount emitted at all. I didn't
    have time to figure out how that calculation could be done.
2.  There seems to be a subtle bug that causes the fishy cat demo scene
    to render slightly lighter even when the light tree is disabled. I
    was unable to figure out what is causing this.
3.  I wasn't able to create a scene that did a very good job
    demonstrating the benefit of the light tree clearly. I'm not sure if
    this is because its not working correctly or if it is due to my lack
    of artistic ability.
