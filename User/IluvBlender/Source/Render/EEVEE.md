## Goals

After discussing with [User:Jbakker](../../../Jbakker/index.md), the
following points stood out - We should stay high level. - The goal would
be to speed up development by sharing the concepts and code structure
and reasons.

After the draft gets approved, relocate it to
[Source/Render/EEVEE](../../../../Source/Render/EEVEE/index.md)

## Quick Draft

### OpenGL 3.3 and EEVEE

  - With Blender 2.80, the minimum supported OpenGL is 3.3
  - Modern Open GL provides a programmable interface to the graphics
    processing pipeline (vertex, fragment shaders)
  - EEVEE is a GPU powered raster engine that makes use of modern OpenGL
    and deliver amazing visuals.

### EEVEE Rendering Pipeline

EEVEE is one of the drawing engines in Blender. Others being Selection,
workbench, overlay etc.

  - Definition of the engine
  - Registration of engine

<https://developer.blender.org/diffusion/B/browse/master/source/blender/draw/DRW_engine.h>

Ref:
[Source/Render/EEVEE/GPUPipeline](../../../../Source/Render/EEVEE/GPUPipeline.md)

### Learning OpenGL 3.3

  - You can learn the basics of Modern OpenGL at
    <https://www.youtube.com/playlist?list=PLlrATfBNZ98foTJPJ_Ev03o2oq3-GGOS2>
  - <https://www.khronos.org/registry/OpenGL/specs/gl/glspec33.core.pdf>
  - <https://www.khronos.org/registry/OpenGL/specs/gl/glspec33.compatibility.pdf>
  - <http://docs.gl/gl3>
  - <https://www.khronos.org/opengl/wiki/Main_Page>

#### Salient features used in Blender

  - Buffer Objects
      - UBO/VBO
  - Textures
  - Framebuffers
  - GLSL

## References

  - [Source/Render/EEVEE/RenderPasses](Source/Render/EEVEE/RenderPasses)
  - [User:Jbakker/projects/EEVEE\_Compositing](../../../Jbakker/projects/EEVEE_Compositing.md)
  - <https://learningblender3dsoftware.blogspot.com/2020/02/understanding-volumetric-lighting-in.html>
