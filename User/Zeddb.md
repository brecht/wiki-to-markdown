### Oct 31 - Nov 04

More work on the new computer system for the studio

### Oct 24 - Oct 28

More preparations for the Blender conference

Attended Blender conference

### Oct 03 - Oct 21

Working on the new computer system for the studio

Helping with preparations for the Blender Conference

### Aug 15 - Aug 31

Vacation

### Jul 18 - Aug 12

GSoC mentoring.

Further work with IT at the studio

Helped Yiming with further LineArt planning and fixes

More work on libepoxy and headless OpenGL rendering. Patches for this
will be merged to master on Monday (Aug 15)

### Jun 27 - Jul 15

IT work at the studio

Patch review

Testing Wayland backend functionality

Worked on fixing VSE drag and drop

Further GSoC mentoring

### Jun 13 - Jun 24

IT work at studio

Caught up on some of the patch review backlog

More work with Yiming on LineArt

Further GSoC mentoring

### May 30 - Jun 10

Did patch review with Yiming for LineArt patches.

Worked on GSoC by helping out with planning for the coding start.

Started working on the new studio Linux setup.

### May 23 - May 24

(wed, fri day off Thur national holiday)

General IT support for the studio

### May 16 - May 20

More work on getting the LineArt patches reviewed and merged into
master.

Spent quite some time in meetings about our potential move to Gitea.

General IT support.

### May 09 - May 13

Worked on our internal infrastructure to help us more easily setup new
computers.

Reviewed and helped Yiming with more LineArt performance patches.

Spent some time going of the GSoC applications again and deciding on the
final projects with the other mentors

General IT support.

### Apr 11 - May 6

Tested Gitea and reached out to the upstream developers so we are in
touch before the eventual migration

Meetings about VSE projects and workshops.

Gave feedback and tested LineArt performance improvement patches

IT support and planning

### Apr 4 - Apr 8

Finished my VSE drag and drop patch and posted it for review.

More work with Yiming on Lineart

Talked to potential GSoC students about physics simulation projects

IT support and work at the studio

### Mar 29 - Apr 1

Worked on the VSE drag and drop support.

Talked to Yiming about the progress of the Embree Lineart port. Also
looked into different papers and studies on triangle intersections
algorithms.

Looked into and discussed different approaches to making Eevee shader
compilation either controllable or less obtuse to work with in scenes
with complex/heavy shaders.

### Mar 11 - Mar 28

On Vacation

### Feb 28 - Mar 18

IT support

Helped out Yiming with LineArt planning and dev grant proposal feedback.

Continued work on VSE drag and drop improvements.

### Feb 21 - Feb 25

Worked on improving drag and drop support in the VSE.

Helped out the studio with crashing and general stability issues they
discovered.

### Feb 14 - Feb 18

Helped out with updating and testing libraries for Blender 3.2

Had more discussion with Richard how to tackle some of the VSE video
support issues.

IT support

### Feb 7 - Feb 11

Worked on updating our code to support ffmpeg 5.0

Started looking into Variable Refresh Rate video seeking issues.

IT support

### Jan 31 - Feb 4

Provided IT support and general help for the studio

Minor patch review and wrapping up some of my older patches

### Jan 24 - Jan 28

As the last week, a lot of time was spent on apartment related issues
and tasks.

Discussed with Richard (ISS) what the best way would be to solve the
remaining playback/sync issues in the VSE.

IT support

### Jan 17 - Jan 21

Most of this week was spent preparing and moving to a new apartment.

### Jan 10 - Jan 14

Worked on implementing some smaller features/fixes for weight/vertex
paint to bring the default shortcuts more in line with sculpt mode.

Talked and guided a few coding volunteers on what and how to improve
some of the current physics systems in blender. Also discussed surface
and volumetric remeshing that will be needed from some of the
simulations.

Had some back and forth with Yiming about speeding up LineArt with
Embree, the initial results looks promising.

IT support

### Jan 4 - Jan 7

Working through backlog of tasks accumulated during vacation

Looking into potential projects for 2022

IT support

### Dec 6 - Jan 3

On Vacation

### Nov 22 - Dec 3

Wrapping up issues and projects for Blender 3.0

Letting people know that I will go on vacation and planning things with
them so they hopefully will not need me during that time.

IT support.

### Nov 15 - Nov 19

Started creating a presentation of the hurdles and accomplishments from
the development side of things for Sprite Fright.

Worked on figuring out and reporting printer profiling issues/bugs to
upstream.

### Nov 08 - Nov 12

This week nearly all of my time was spent with IT related future
planning and solving issues.

### Nov 01 - Nov 05

Worked on LineArt optimizations. However I hit a major implementation
roadblock. Because of this I spent some time talking to Yiming and other
core developers on how we could change the core algorithm to get more
performance.

In addition to this I also started talking to the BEER (Malt) developer
on how Blender could collaborate with them to bring a dedicated NPR
render to blender. Talks are still ongoing.

Rest of the week was spent on IT support for the office.

### Oct 25 - Oct 29

Was sick for most of the week.

Sprite Fright release party and preparations.

### Oct 18 - Oct 22

Was sick for part of the week.

IT support.

Continued to work on optimization for LineArt.

### Oct 11 - Oct 15

Looked into various critical issues for the studio.

Started working on optimization tasks for LineArt. Managed to make mesh
loading around 7 times faster in heavier scenes.

### Oct 04 - Oct 08

Merged the general transform gizmo:
[D12729](http://developer.blender.org/D12729)

Looked into a few reported VSE issues and discussed some general VSE
roadmap milestones

Started looking into some LineArt threading optimizations.

IT support.

### Sep 27 - Oct 1

Put up my work on the VSE general transform gizmo:
[D12729](http://developer.blender.org/D12729)

Had some discussions about organizing a workshop where we would
restructure/rewrite the current gizmo and transform system.

IT support

### Sep 20 - Sep 24

Attended studio feedback workshop

Reviewed Grease Pencil patches from Yiming

Started working on VSE transform/gizmo polishing/cleanup.

IT support and assembling new computers

### Sep 13 - Sep 17

IT support

Worked with Richard and Francesco on finishing up more VSE changes we
want to get into Blender 3.0

### Aug 30 - Sep 10

Vacation

### Aug 23 - Aug 27

IT support.

Did more research and gave feedback to the Wayland design task.
<https://developer.blender.org/T90676>

Continued work on VSE related cleanup.

Discussed more LineArt optimizations with Yiming.

### Aug 16 - Aug 20

Mon - Fri:

IT support

Merged my VSE patches to master and the blender 2.93 LTS release. See
the log from "Jun 12 - Jul 16" for the list of patches.

Worked on more VSE cleanup: <https://developer.blender.org/D12278>

GSoC feedback for the final week.

LineArt optimization feedback and discussions.

### Aug 09 - Aug 13

Mon - Fri:

Finalized my VSE patches.

GSoC feedback.

IT support.

### Aug 02 - Aug 06

Mon - Fri:

Worked on addressing feedback and issues with my VSE patches.

GSoC feedback.

IT support.

LineArt optimization discussion and planning.

### Jun 26 - Jul 30

Mon - Fri

IT work at studio.

Worked on addressing feedback for my audaspace VSE patch.

Looked into and tested libraries for Wayland support.

More feedback sessions on the rigid body GSoC.

### Jun 19 - Jul 23

Mon - Fri:

IT work for the studio.

Did some minor patch reviews.

Had feedback sessions on the rigid body GSoC

### Jun 12 - Jul 16

Mon - Fri:

IT work at the office.

Submitted my VSE audio/video import, sync, and seeking fixes patch set:

<https://developer.blender.org/D11916>

<https://developer.blender.org/D11917>

<https://developer.blender.org/D11918>

<https://developer.blender.org/D11919>

<https://developer.blender.org/D11920>

<https://developer.blender.org/D11921>

### Jun 05 - Jul 09

Mon - Tue:

Patch review. IT support and hardware inventory planning. Worked on
fixing VSE audio offset issue.

Wed:

Worked on updating the Office Hardware inventory list.

Thu:

IT support

Fri:

Continued work on VSE A/V sync issues.

### Jun 28 - Jul 02

Mon:

LineArt patch review.

Tue:

LineArt patch review.

Looked at Gitea migration options.

Wed:

General IT support.

Helped Paul out with an subtitle export issue in the VSE.

Thu-Fri:

More IT support.

Started looking into a VSE audio offset bug.

### Jun 21 - Jun 25

Mon:

Helped out at the office reorganization

Tue:

IT support

More setup and general work at the office.

Wed:

Office inventory work.

LineArt patch review.

Fri:

LineArt patch review.

### Jun 14 - Jun 18

Mon:

LineArt patch review.

Fixed the last remaining known VSE seeking issue.

Tue-Wed:

LineArt patch review.

Thu-Fri:

Day off.

### Jun 07 - Jun 11

Mon:

Minor VSE talks and cleanups

LineArt patch review.

Provided some guidance to GSoC students

Tue-Fri:

Worked on and fixed most remaining VSE seeking issues.

### May 31 - Jun 4

Mon: More work on fixing VSE issues

Tue: Research for Phabricator migration

Wed-Fri:

More research and discussion about Phabricator migration

Worked with Richard on how to tackle some VSE seeking bugs.

Did some profiling for video playback.

### May 24 - May 28

Mon: National holiday

Tue:

IT support

LineArt patch review

Found a fix for the ffmpeg issue from last week. Fixed an other ffmpeg
issue as well: <https://developer.blender.org/D11390>

Wed-Fri:

Worked on fixing ffmpeg bugs.

LineArt discussions and planning.

Minor GSoC guidance to the students.

### May 17 - May 21

Mon:

Looked into and reported issues upstream for osl, ispc, and usd.

Tue:

GSoC onboarding

IT support

LineArt: Trying to help out to clean up merging issues.

Wed:

LineArt planning and code review.

Thu:

IT support

Started looking into a custom keymap issue (keymap would get mangled
when blender tries to update it)

Fri:

IT support

Minor planning and discussions with GSoC students

The studio discovered a very bad issue with video encoding that would
make exported videos have the wrong FPS. Started looking into this
issue.

### May 10 - May 14

Mon:

Sick Day.

Tue:

Filled out work related paperwork.

Continued work on VSE tests and related changes.

Wed:

IT support.

Fixed a small oversight with the new custom bone shape parameters.

Continued work on VSE tests and related changes.

Thu:

National Holiday

Fri:

Line art discussions and meetings.

IT support.

Fixed a small follow path backward compatibility issue:
[D11263](http://developer.blender.org/D11263)

### May 03 - May 07

Mon:

IT support.

GSoC mentor meeting.

Continued work on my ffmpeg cleanup up patch. Fix a issue where there
would be no sound saved in video files will ffmpeg \>= 4.4

Tue:

IT support.

Continued work on ffmpeg cleanup and writing better tests to to see if
ffmpeg export is properly working.

Merged "Fix text objects being deleted on recursive purge":
[D10983](http://developer.blender.org/D10983)

Wed:

IT support.

Continued work on creating a ffmpeg export/import test.

Thu:

IT support.

Fixed [\#88065](http://developer.blender.org/T88065) Spline IK math
issues.

Continued work and testing of ffmpeg export/import/test.

Fri:

Had some longer discussion if I should merge the ffmpeg refactor into
2.93. We decided to only merge it in to master for now:
[D10338](http://developer.blender.org/D10338)

Continued work on adding functions that would be needed to have
automated ffmpeg import/export tests.

### Apr 26 - Apr 30

Mon:

IT support for the studio and looking into poselib branch issues.

Tue:

Dutch national holiday

Wed:

IT support for the studio and testing out the new build bot
infrastructure.

Tracked down a memory leak in our OSL code.

Thu:

Poked Brecht about the OSL issue I found yesterday and he came up with a
better fix than my initial try. He will commit the fix shortly.

Looked into "Use after free" issues in the test system (related to our
internal CLOG variables)

Animation module meeting.

Fri:

IT support.

Discussed and reviewed GSoC proposals with Sebbas

LineArt meeting.

Commited a fix for the "use after free issues" with the test system
after talking it over with Campbell.

Looked into FFmpeg 4.4 audio encoding issue (no fix yet).

### Apr 19 - Apr 23

Mon:

IT support at the office.

Added more feedback to the Line Art manual page review:
[D10742](http://developer.blender.org/D10742)

Tue:

More IT support.

Investigated and drafted a design task for copy pasting keyframes
between Blender instances: <https://developer.blender.org/T87662>

Wed:

Even more IT support.

Looked into and fixed automated test failures on my system.

Thu-Fri:

LineArt discussions and meetings.

Further looking into automated tests crashing or failing.

### Apr 12 - Apr 16

Mon: Sick day

Tue:

Meetings and tech support

Got my spline ik patch merged:
[D10849](http://developer.blender.org/D10849)

Wed:

Answered and provided blender technical support to the Japanese studios.
Attended meetings at the office.

Thu:

Planned, coded, tested and discussed:
<https://developer.blender.org/D10983>

Fri:

LineArt planning and meeting.

IT support.

Tested and reviewed: [D10974](http://developer.blender.org/D10974)

Started looking into: <https://developer.blender.org/T85050>

### Apr 05 - Apr 09

Mon: National holiday

Tue: Finished my curve \`where\_on\_path\` patch:
<https://developer.blender.org/D10898>

Wed:

IT support.

Looked into an object relationship line refresh issue. However it seems
like it will need a big rewrite to fix. So I gave up for now.

Worked on cleaning up and some of my current patches.

Thu:

Worked on and merged:

\- [D10898](http://developer.blender.org/D10898) (fixed curve path
position evaluation)

\- [D10570](http://developer.blender.org/D10570) (menu list for bone
selection)

Started updating my spline IK patch.

Fri:

LineArt discussions and meetings

Discussed with Sybren on how to best onboard some potential new
animation module contributors.

Updated my the spline IK patch
[D10849](http://developer.blender.org/D10849)

### Mar 29 - Apr 2

Mon:

Got feedback on my patches from Sybren.

Worked the whole day on cleaning up and addressing the feedback.

Tue:

Had some more back and forth with Sybren on how to best adress some of
the patch feedback and solutions.

Looked into and solved some localized build issues on my work computer.

Wed:

Talked and gave guidance to GSoC students.

Looked into a Lineart/Grease Pencil material issue.

Discussed with Jacques and worked on how to best solve a curve path
interpolation issue.

HR meeting

Thu:

Worked more on the animation path interpolation issue.

Helped unload and move desks at the office.

Fri:

Merged my weight paint symmetry option tweaks to master.

Worked more on animation paths.

IT support.

### Mar 22 - Mar 26

Mon:

Some more LineArt discussions and cleanup after some issues were brought
up to me.

General IT support.

Started working about on trying to finish up my animation module
patches.

Tue-Wed:

Looked over and updated some of my patches under review. Need to discuss
more with Sybren before they are merged though.

Looked into some depsgraph API threading issues Yiming discovered.

Thu:

Spent most of the day tweaking and fixing issues with the streaming
setup at the studio.

Fri:

Worked on fixing issues with curve resampling in multiple blender
modifiers.

### Mar 15 - Mar 19

Whole week:

Attended Studio kick-offs and the release targets meeting on monday.

Worked with Yiming to wrap up the code review of LineArt and get it
merged. The rest of the week was spent dealing with LineArt feedback and
discussing and planning with Yiming who he should prioritize bugfixing
and feature development for LineArt.

Got contacted by a few potential GSoC students and gave them a few tips
on what they should work on to have a good proposal.

### Mar 08 - Mar 12

Mon:

Mostly finished the ffmpeg cleanup patch from last week. Needs a bit
more discussion though.

Animation meeting regarding the pose library and baking with Sybren and
the animators from the studio

Tue:

LineArt discussion and review with Yiming.

General IT support

Various meetings and discussions at the office

Wed:

More LineArt testing, planning, and discusisons.

Worked on finishing up my weight paint symmetry patch.

Thu:

Spend some time setting up my new studio email adress.

More LineArt planning and testing.

Fri:

Spent the whole day doing over updated LineArt branch code. Worked
closely together with Yiming to clean up the remaining code and create
roadmap for getting it into master and what to do after it has been
merged.

### Mar 01 - Mar 05

Mon:

Worked on and posted a WIP patch for alt+click select list bones:
[D10570](http://developer.blender.org/D10570)

Tue:

Continued work on the "alt+click list" patch.

Created a small python addon requested by the studio.

Discussed [D9054](http://developer.blender.org/D9054) internally with
Ton and other developers.

Wed:

Made the "alt+click" list also work in bone edit mode. Acted IT support
for the studio.

Thu:

Animation module meeting. Filling out WBSO timesheets.

Fri:

Continued working on the ffmpeg deprecation cleanup:
[D10338](http://developer.blender.org/D10338)

Talked with Yiming about Lineart baking.

### Feb 22 - Feb 26

Mon:

Finally figured out what was the problem with the point scatter node and
pushed a fix.

Tue:

IT work at the studio.

Wed:

Worked on some scripts/addons for the studio. Day got cut short by
sudden sickness.

Thu:

Sick day

Fri:

Started looking into [\#85796](http://developer.blender.org/T85796)

### Feb 15 - Feb 19

Mon:

Talked with Pablo Dobarro about sculpt tools and how we could tackle
performance issues when using demanding tools on high poly sculpts.

Worked on the weight paint symmetry task:
[D10426](http://developer.blender.org/D10426)

Tue:

Discussed back and forth the symmetry patch from yesterday with the
animation module. The proposed solution got change to something much
simpler so people can still use the new sculpt symmetry functionality if
they wish.

Wed:

More tweaks and discussion about the symmetry patch.

Looked into getting the gradient tool working with symmetry.

Thu:

Looked over the GSoC ideas page and posted my own GSoC task.

Attended the animation module meeting to discuss my weight paint
findings.

Started looking into a point scatter node issue the studio found.

Fri:

Discussed and worked a bit on LineArt with Yiming and the GP team.

Continued looking into the point scatter node issue from yesterday.
Seems to be a race condition in the geometry nodes code.

### Feb 08 - Feb 12

Mon:

Continued working on the recording room setup at the office.

Tue:

Minor IT support

Discovered, reported and bisected a mesa driver crashing issue in
Blender: <https://gitlab.freedesktop.org/mesa/mesa/-/issues/4259>

Wed:

Bisected a bone transform bug and discussed potential solutions with
Germano: <https://developer.blender.org/T85471>

Did research on audio/video recording equipment for the recording room
at the studio.

Thu:

Talked to Antonioya about LineArt and booked a meeting about our plan of
attack.

Talked to Clement about some Anti Aliasing issues with GP discovered by
Studio Khara and Studio Q.

Further work on reviewing the LineArt branch. Committed minor cleanup
and pointed out some potential bad code.

Fri:

Tested out some mesa patches from upstream to see if they fixed some of
my reported issues.

Had a meeting with the Grease Pencil team to discuss LineArt and the
plan of attack to get it into master.

Did some more cleaning up and coding on the LineArt branch.

### Feb 01 - Feb 05

Mon:

Geonodes meeting. IT work at the office.

Tue-Thu: Worked on reviewing and updating patches from my backlog.

Fri: Code quality day. Did initial work to remove all deprecated ffmpeg
functions and variables.

### Jan 25 - Jan 29

Mon:

Discussed some Blender related workflows with Studio K and Q. More
discussions about the shearing issues in the Geometry nodes and
potential solutions. Looked into a action editor performance we it has
many keyframes displayed.

Tue:

Discussed with Sybren how to tackle the action editor performance bug
and started working on it. Fixed the shearing issues related to Geometry
nodes from yesterday.

Wed:

Minor tech support. Multi threaded the action editor for 4x speed
increase in certain scenarios. Still need to figure out a caching system
as it is still unusably slow though.

Thu-Fri:

Finishing up the "volume to mesh" and "collection info" node.

### Jan 18 - Jan 22

Mon:

Started implementing the "Collection Info" geometry node

Tue:

Posted initial implementation of the "Collection Info" node
[D10151](http://developer.blender.org/D10151) Started looking into
<https://developer.blender.org/T84867>

Wed-Fri:

Worked more on the "Collection Info" node. Fixed T83867. Started working
on the volume to mesh node.

### Jan 11 - Jan 15

Mon:

Reviewed and discussed two cloth patches from Alexander:
[D10043](http://developer.blender.org/D10043)
[D10064](http://developer.blender.org/D10064)

Geometry nodes meetings

Minor IT support

Responded to feedback on my symmetrize fix
([D9214](http://developer.blender.org/D9214))

Tue-Wed:

IT support for the studio.

Meetings with the Geo nodes module.

Thu:

Tested the automated tests thoroughly after my patches had landed with
various popular optimization flags to make sure the pass in the most
common cases.

Started reviewing the LineArt branch but got hold up by a severe
mesa/llvm greasepencil bug. Still bisecting the llvm/mesa projects so
this can be fixed upstream

Fri:

Bisected the llvm issue from yesterday and submitted a bug report
upstream.

### Jan 04 - Jan 08

Back from vacation

Mon:

Made sure my developer environment was up to date and working.

Worked on disabling OSL cycles tests if OSL support is not enabled.

Updating my modifier test tweaks to the now merged GSoC work.
([D9004](http://developer.blender.org/D9004))

Did some minor IT support.

Tue:

Started work on creating automated tests for the armature symmetrize
operator. (So this fix can be accepted:
[D9214](http://developer.blender.org/D9214))

Wed:

Had a discussion about some potential cloth optimizations and other
physics related features with Alexander Gavrilov. Continued work on the
symmetrize automated tests.

Thu:

Discovered some blender python api bugs when testing the symmetrize
test. Will investigate further tomorrow.

Fri:

Finalized the automated test work and posted them for review. Poked
different people about the python API issues I found while working on
the automated tests. Looked into ASAN misc errors reported while running
blender. Looked through some of my other half finished patches to see if
there were still relevant.

### Nov 30 - Dec 04

Mon:

Long geometry nodes spring planning meeting. Minor IT support. Started
thinking about how to implement the tiling functionality for the point
distributions.

Tue:

Implemented tiling support for point distributions, there are some bugs
though that I need to fix.

Wed:

Spent the whole day on working on IT task at home and at the office.

Thu: Managed to fix the tiling bugs for the point distribution.

Fri: Implemented weight map and "dithering" support for the tiling noise
pattern. Create builds for the branch so it is now available for testing
\`geometry-nodes-distribute-points\`
<https://builder.blender.org/download/branches/>

### Nov 23 - Nov 27

Mon:

Continued work on the point distribution node. Spent most of the day
doing IT stuff for the office.

Tue - Thu:

Some provided some IT support for the office. Worked more on the point
distribution node.

Fri:

Had longer meeting about the current progress and what need to be done
on for the point distribution node. Seems like it was a bigger task to
get working nicely than first anticipated. Will have a followup meeting
with Jeroen on monday on how we should proceed and general optimization
work that we can do.

### Nov 16 - Nov 20

Mon:

Worked Geometry nodes and did some IT helpdesk work.

Tue:

Worked on bringing the distribution node inline with what has been
drafted in the design docs. Some points are still unclear on how it is
expected to work. Will have to do some testing with the artists to get
this done I feel.

Wed:

More design work on the geometry nodes. Had a long meeting with Dalai on
how to the scattering of points should work.

Thu:

Sick day

Fri:

Spend the whole day fixing various issues with my computer I've been
putting off fixing for a while.

### Nov 09 - Nov 13

Mon: Read up on fluid viscosity papers for possible inclusion in
mantaflow.

Tue: Physics meeting. At this meeting we decided that it would probably
be better if I let Sebbas handle the Mantaflow (fluid sim) things and
instead help out the geometry modifier team.

Had a meeting with the geometry modifier team. Started reading up on a
uniform random distributions with weighting.

Wed: Had a longer discussion and about LineArt and what our plan of
attach should be. Current plan is to strip it down even further and only
have it in a non fancy modifier form.

Did work on geometry distribution nodes. However Jacques had already
done a lot of the work already, however this was not clearly
communicated so most of my code could be scrapped.

Thu: Did some smaller discussions and further clarifications about
LineArt.

Onboarding (for me) and meetings with the Geometry Nodes project.

Fri:

More LineArt review work.

Continues work on distribution nodes and more geometry nodes mettings.

### Nov 03 - Nov 06

Mon: Looked into some bullet sim issues

Tue: Continued debugging of the bullet issue from yesterday.

Tried to figure out why some videos uploaded to the wiki becomes
corrupted. It fixed it self after a few hours so I have now clue what
the issue was.

Looked into and fixed a "Copy F-Curve modifier" default setting that
lead to some confusion.

Wed:

Looked into a fluid sim explosion/segfault issue. Ported the blender
code to use doubles to see if the explosion and subsequent segfault was
because of floating point precision issues. It wasn't, but I've shared
my findings and code with Sebbas for future work.

Thu:

Worked on clarifying and discussing some things for LineArt (review
task) Helped some studio artist with IT related things. Started some
research on new office computers.

Fri: Worked on fixing the last of the Note/Warning compilation messages
when building blender on my computer.

### Oct 26 - Oct 30

Mon: Talked to Germano about cloth collision and future plans. Submitted
a fix for the pose layer bug from last week:
<https://developer.blender.org/D9354>

Tue: Finished up and merge my pose layer bug fix and the "unbake
operator"

Wed: Had a water leak in my apartment that I had to attend to.

Thu-Fri: Read up on some cloth collision papers to help Germano with his
upcomming cloth collision changes.

Minor LineArt talks with Yiming.

More work on the water leak in my appartment

Bconf2020

### Oct 19 - Oct 23

Mon:

Continued work on some bugfixes and pull requests in the morning.
Assembled computers for the rest of the day.

Tue:

Fixed a windows test failure with libmv. Helped various people with
development related issues and questions. Worked on porting some of my
older pull requests to the latest master.

Wed-Fri:

Talked to Yiming about more memory optimizations for LineArt.

Worked one fixing an trying to fix a pose mode layer bug:
<https://developer.blender.org/T81844>

Seems like this is a deeper issue with the new undo system. Talked to
Bastien about it and will continue to figure out a solution next week.

### Oct 12 - Oct 16

Mon: Sick

Tue: Did some minor patch review and discussions with other developers.
Still recovering from the monday sickness.

Wed: Talked about LANPR memory usage with Yiming and brainstormed a bit
on how to improve it. Looked into some "Symmetrize armature constraint"
bugs. Did some hardware related work for the studio.

Thu: Talked to Sergey and Jacques about how to tackle a bigger rigidbody
rewrite so it fits with "everything nodes". Animation module meeting.

Fri: Categorized some Physics bugs and started working on fixing a
rigidbody bug.

### Oct 05 - Oct 09

Mon: Help out some contributors by helping them with some questions
about feedback and general code style guidelines. Worked a bit more on
how to tackle the TODOs for the hair brush.

Tue:

Continued work with answering questions and helping out animation module
contributors.

Wed:

Worked more on the hair brush. I came to the conclusion that we need to
rework how it will work as there are some severe limitations with the
current approach.

Thu:

Helped more with on-boarding and discussion in the animation module.
Talked to Dalai about the issues with the current hair brush approach
and started working on drafting how my alternative method would work.

Fri:

Talked to Andy about what he wants out of the hair brush length tool to
try to get a clear view of how it would be used. Spent the rest of the
day packing and cleaning to prepare for moving to an other house.

### Sep 28 - Oct 02

Mon: Worked in implementing some feedback into the automated test
reviews.

Tue: Worked on helping setting up and preparing some computers so that
the users could work from home.

Wed: More tech support tasks.

Thu: Participated in a meeting with the animation team. Started to look
into some hair brushes from Sergey:
<https://developer.blender.org/D9056>

Fri: Worked with Yiming on some LineArt vertex group planning and brain
storming. Looked into some texture paint freezing and out of memory
issues.

### Sep 21 - Sep 25

Mon:

Spent some more time on automated test failures. Committed a follow up
fix of UV edit crashes: <https://developer.blender.org/D8967>

Tue - Fri:

Finished up and created review tasks for my automated tests fixes.

Tested some OpenCL fixes.

### Sep 14 - Sep 18

Mon:

Managed to get the hair sim into a somewhat better state. It needs more
work though.

Worked on some animation system code reviews.

Tue - Wed:

Further work on hair sim and animation system review

Thu:

Look into some bullet crashes. After some talks with Brecht, we decided
that a proper redesign of how the bullet code works in blender is the
best way to solve this.

Provided some more feedback to "Directly select animation curves in the
graph editor" [D8687](http://developer.blender.org/D8687)

Fri:

Spent the day and weekend looking into some automated test failures on
my machine.

### Sep 07 - Sep 12

Mon:

Researched some computer related setups for the office.

Looked into:

[\#80438](http://developer.blender.org/T80438)
[\#80561](http://developer.blender.org/T80561)
[\#79954](http://developer.blender.org/T79954)

Tue:

Did some IT work at the office.

Had a meeting with Brecht about the soft body GSoC

Looked into hair sim issues with curved hair segments.

Wed:

Fixed a crash when converting from mesh to curves:
[\#80596](http://developer.blender.org/T80596)

Did some minor fixes for null pointer references reported by ASAN.

Continued investigation on hair sim issues.

Thu-Fri: More looking into hair sim.

### Aug 31 - Sep 04

Mon:

More work on bullet.

Tue:

Further bullet work. Was in the Animation team and Physics team module
meeting.

Wed:

Merged part 1 of my bullet animation substep rework. Part 2 is just
updating the library to the upstream version.

Thu:

Synced the bullet library with upstream. Fixed a few segfaults related
to bullet and undo.

Fri:

Spent the whole day working on code quality tasks.

### Aug 24 - Aug 28

Mon: Sick day

Tue:

Worked on a cmake building fix: <https://developer.blender.org/D8702>

Looked into how the freeing logic works for softbodies in blender (to
help out the softbody GSoC project)

Worked a bit more on Rigidbody kinematic body logic.

Did some back and forth on the logic regarding
<https://developer.blender.org/D8687> with the patch author.

Wed:

Worked a bit with Sergey to get some bullet objects transforms be
available at the right time in depsgraph. The simulations now seems a
lot more stable with moving objects\!

Thu:

Did more testing and committed my two build systems tweaks/fixes
(Openvdb and Embree).

Worked more on fixing and cleaning bullet depsgraph code.

Fri:

Refactored some bullet code in blender so that we can use the upstream
version.

Started doing some more excessive tests to make sure I didn't break any
existing functionality with my bullet code changes.

### Aug 17 - Aug 21

Mon:

Did some minor reviewing of mano wii's new cloth cleanup/rework patches

Started work on blender bullet fixes/cleanup.

Tue:

More work on bullet physics.

Reviewed: [D8577](http://developer.blender.org/D8577)

Wed:

Worked a bit more on bullet. Started testing and reviewing the soft body
GSoC branch.

Thu - Fri:

Worked more on bullet. Discovered that we might have to do some more
invasive changes to make it work. Will have to discuss this further next
week with Sergey.

### Aug 10 - Aug 14

Mon:

A bit of IT work.

Reviewed and accepted [D8500](http://developer.blender.org/D8500): Fix
T78113: Random explosions of cloth with self collision.

Worked more on rigid body physics testing.

Tue:

Spend the whole day on bullet physics programming. Got it to work
satisfactory for our test case. Will discuss later how to proceed from
here.

Wed:

Did some minor triaging

General office duties/work

Talked to manowii about some cloth collision work he is doing.

Fixed [\#79706](http://developer.blender.org/T79706)

Thu:

IT work.

Reviewed: [D8562](http://developer.blender.org/D8562)

Did more bullet physics tests.

Fri:

Completed the proof of concept bullet physics test program. This showed
that certain simulations that are very tricky do to with the current
bullet implementation in blender is achievable with good results in
vanilla bullet.

### Aug 3 - Aug 7

Mon:

Did IT work most of the day. The rest of it I looked at the "Needs
Developer to reproduce issues" to see if I could reproduce any.

Tue:

Did some physics tests with Andy. Fixed T71488 Flipping Custom Split
Normal Data leads to artifacts.

Wed:

Did some reading up on the bullet physics library.

Bisected T79541: Freestyle render is completely black

Closed: T71283 Rigid body simulation glitching

Thu:

Spent the whole day doing bullet physics library testing.

Fri:

Did some cleanup in the "Find Package" CMake modules:
<https://developer.blender.org/D8495>

Continued working on
\`readability-inconsistent-declaration-parameter-name\`

### Jul 27 - Jul 31

Mon:

Looked into a A/V sync with Sybren:
<https://developer.blender.org/T79231>

Prepared some example animation files for Yiming to test LineArt with.

Tue:

Did some IT work as a few computers needed some maintenance to be able
to build blender with the new GCC version requirements.

Worked more on LineArt feedback and planing.

Wed:

Worked on tracking down a A/V sync issue with Sybren. Worked on testing
and giving more feedback to LineArt

Thu:

Minor IT work. Started gathering data so we can decide of a spec
baseline for the office computers. Looked into and merged rigid body
compound shapes: <https://developer.blender.org/D5797>

Fri:

Helped Sebbas with some computer problems. Did more testing of LineArt
and gave feedback. Sent updated documentation for the "Compound Parent"
changes to the manual.

Next week:

Bugtracker.

### Jul 20 - Jul 24

Mon: Did some more back and forth about
<https://developer.blender.org/D8075> in \#blender-coders

Did some IT work at the office.

Tue:

Worked more on edge sliding. More IT work.

Wed:

Finished up my edge slide patch: <https://developer.blender.org/D8366>

Worked with Yiming on more LineArt planning.

Thu:

Worked on adding some LineArt feature discussion and proposals:
<https://wiki.blender.org/wiki/Line_Art_Desired_Functionality>

Fixed some issue with my edge slide patch.

Fri:

Finished up the edge slide patch. Worked more on LineArt feature
discussion and planning. Worked a bit more on the particle nodes mesh
collision code.

### Jul 13 - Jul 17

Mon:

Discussed some GP features and tweaks with the GP devs. Looked into and
started working on a fix for: <https://developer.blender.org/T78880>
However when doing so I noticed some more issues with UV proportional
editing that I started fixing as well.

Tue:

Committed a bigger fix/rework for the UV edit bug from yesterday:
<https://developer.blender.org/D8289>

Posted a fix for prop edit in curve edit mode:
<https://developer.blender.org/D8293>

Wed:

Had a longer meeting about LineArt (formally LANRP) with Yiming and the
GP team.

Committed my prop edit curve fix from yesterday.

Posted a fix for "move only orgins" not working with GP objects:
<https://developer.blender.org/D8303>

Started looking into some edge slide issues:
<https://developer.blender.org/T69979>

Thu:

Had some long discussions with the Japanese studios about what the want
from LineArt.

Did some IT work at the office.

Continued working on the edge slide issue from yesterday.

Fri:

Got edge slide even thickness working. Though the code needs some
further cleanup.

### Jul 06 - Jul 10

Mon:

Continued working on
\`readability-inconsistent-declaration-parameter-name\`.

Reviewed: <https://developer.blender.org/D8219>
<https://developer.blender.org/D8075>

Looked into the paper:
<https://www.cs.cmu.edu/~kmcrane/Projects/MonteCarloGeometryProcessing/index.html>

Tue:

Went to the office and helped with some IT stuff for the whole day.

Wed:

Spent the morning preparing to for moving my workstation to the office
tomorrow. The rest of the day was spent researching and troubleshooting
printer profiling with Troy

Thu:

Moved my workstation back to the office and set it up. Helped out with
some more general IT things at the office.

Fri:

A bit more IT and printer work at the office.

### Jun 29 - Jul 03

Mon:

Figured out why particle collisions sometimes gave weird results when
sliding down a surface. There might be multiple times where the particle
collides with the same mesh during a timestep. The current methods we
use in blender doesn't necessarily return the absolute first or shortest
collision time.

This is usually not noticeable except for a few corner cases.

Tue:

Spent the day experimenting if the limitation I discovered yesterday
could be easily be remedied. However, most of the solutions that worked
was using brute force logic, so it would be a lot slower even in
scenarios when we don't really need to have this check in place for good
collisions.

I therefore opted to see if I can instead add a sanity check at the end
of the collision detection and work around it this way.

Wed:

Had a bunch of meetings at the office and also did some more IT support
work.

Thu:

More work on particle collisions and did preparations with settings and
tweak to my text editor for tomorrows code quality day.

Fri:

Code quality day. Worked on <https://developer.blender.org/T78535>
Finished \`readability-braces-around-statements\` and started working on
\`readability-inconsistent-declaration-parameter-name\`.

### Jun 22 - Jun 26

Mon:

Discovered and fixed more issues in the particle collision code.

Tue:

Did some testing to see if I could solve some minor annoyances with the
particle system without resorting to query edges and verts individually
instead of just faces. Some minor cleanup of the code.

Wed:

Went back to the office. Took some time to set things up there. Worked a
bit more on cleaning up the particle collision code.

Thu:

Did some more investigation into some weirdness of collisions. Seems
like the collision query sometimes fails to get the actual closest point
when it is close to an edge/vertex. Will have to investigate this more
next week.

Fri:

Spent the whole day sorting out computer and printer issues at the
office.

### Jun 15 - Jun 19

Mon:

Sick day

Tue:

Had a meeting with Brecht and Dalai about my work on the particle
physics and the next steps going forward. In addition, we looked at and
gave feedback on the current LANPR plans.

Helped out Matt a bit with finding existing blender functions he could
reuse for the GSoC project. Talked a bit more with PabloD about some
changes needed for the current state of the cloth brush collision patch:
<https://developer.blender.org/D8019>

Wed:

Reviewed a cloth related patch from Alexander:
<https://developer.blender.org/D8017> Discussed and iterated a bit more
on sculpt cloth brush things with Pablo Dobarro Worked a bit more of
particle collision physics.

Thu:

Worked on particle collisions again. I did some fixes and clean up. Now
it should be ready for general testing I think. Tomorrow I will work on
setting up the UI side of things.

Fri:

Made the particle collisions take the settings from the relevant node
and colliders. Implemented friction:
![../videos/Collider\_friction.mp4](../videos/Collider_friction.mp4
"../videos/Collider_friction.mp4")

Though this lead to some other issues cropping up so I will have to look
at that next week.

### Jun 8 - Jun 12

Mon:

Spent today thinking about how to rework some of the collision response
code to be able to handle "dragging" of particles. I managed to come up
with a solution that seems to work ok:
![../videos/Particle\_drag.mp4](../videos/Particle_drag.mp4
"../videos/Particle_drag.mp4")

However it did introduce some breakage. So I need to look over things.

Looked into a surface deform regression and filed a report here:
<https://developer.blender.org/T77632>

Tue:

Spent today working more on particles. Now the dragging seems to work
quite good. This is with dampening set to 1, so the particles velocity
is set to zero when they collide. Before this would lead to tunneling
happening.

![../videos/Recording\_full\_damp.mp4](../videos/Recording_full_damp.mp4
"../videos/Recording_full_damp.mp4")

Wed:

Worked a bit more on particle physics.

Spent a lot of time discussing how we want the LANPR workflow to work
with YiMing and the GP team.

Thu:

More LANPR discussions. Got back to reviewing
<https://developer.blender.org/D6442>

Fri:

Tested and gave further feedback on
<https://developer.blender.org/rBdeaff945d0b9> \`Mesh: skip conversion
from edit-mesh to mesh in edit-mode\`

Finished reviewing and accepted <https://developer.blender.org/D6442>
(hydrostatic cloth pressure)

Discussed cloth sim issues in sculpt mode with Dobarro:
<https://developer.blender.org/T77634> Did further brainstorming on how
to proceed with certain issues in that task. Also helped him with some
implementation specific stuff.

Did some tech support work.

## Jun 1 - Jun 5

Mon: Dutch holiday

Tue:

Updated the failing \`bmesh\_bevel\` test. It failed because D7772 made
UV calculation more precise leading to slightly different UV coordinates
than before.

Wrote a quick TODO task for particle node collisions:
<https://developer.blender.org/T77266>

Worked more on particle collisions.

Wed:

Discovered an annoying particle teleportation bug while I looked into a
tunneling issue and I am looking into the cause of it.
![../videos/Rec\_popping.mp4](../videos/Rec_popping.mp4
"../videos/Rec_popping.mp4")

Thu:

Hopefully fixed the popping and tunneling. The popping issues was
actually because of a bug in a function from the older particle system.
![../videos/Recording\_prog.mp4](../videos/Recording_prog.mp4
"../videos/Recording_prog.mp4")

Fri:

Further looked into the "Write a script to automatically rename DNA
fields" code quality task. I have proof of concept script that renames
the DNA fields. But it is basically just a glorified client for clangd
using the Language Server Protocol (LSP). So after some discussion about
it in \#blender-coders, we kinda (?) decided to just put this on ice for
the time being.

Next Week: More particle physics\!

## May 25 - May 29

Mon:

Investigated, reported, and have a potential solution for:
<https://developer.blender.org/T77052> (Can't select bones in weight
paint mode (LCS))

Worked more on: <https://developer.blender.org/D7772> (Surface deform
precision issues)

Worked more on getting my A/V sync changes for audaspace accepted
upstream.

Tue:

Finished up my new iteration for the A/V sync patch. Waiting for review
upstream.

Worked on a bigger general cleanup commit for
\`source/blender/blenkernel/intern/fcurve.c\` and
\`source/blender/editors/space\_graph/graph\_edit.c\`. This is in
preparation so <https://developer.blender.org/D6379> (Unbake operator)
can land.

Wed:

Finished up the comment clean up in \`fcurve.c\` and \`graph\_edit.c\`
from yesterday: <https://developer.blender.org/D7850>

Spent the rest of the day looking over my particle physics code and
creating example scenes to showcase the current status and what it is
currently lacking.

Thu:

Helped my GSoC student (new soft body sim) with some general questions
and workflow setup.

Spent some time looking into newer rigging and animation worksflows
like: <https://www.youtube.com/watch?v=XjMKbElVNmg>

Posted my "state of things" report about the particle collision code to
Brecht.

Spend quite a bit of time at the office working on some printer
problems.

Fri:

Had a meeting with Brecht about how I should organize the particle
collision work going forward.

Looked into \`bmesh\_bevel\` test failing because of my patch (D7772)

Attended the Coffee Run premier at the office.

Next Week:

I will create a TODO task for the particle node collisions.

## May 18 - May 22

Mon:

Had a meeting with Antonioya and Yi Ming about LANPR after the dev
meeting. After the meeting, we decided to get the GP team and Clement
together for a bigger meeting to discuss how we should handle the live
preview API wise. Yi Ming wrote a short report of what he will do now:
<https://wiki.blender.org/wiki/User:Yiming/LANPR_GP_New_Solutions>

Posted a solution for the surface deform problem I hunted down last
week: <https://developer.blender.org/D7772>

Merged <https://developer.blender.org/D7733> and updated release notes
and docs.

Working on applying some of my fixes in master to the 2.83 branch.

Tue:

Continued to work on: <https://developer.blender.org/D7772>

Looked into:

1.  <https://developer.blender.org/T75722> (Normal map flips in
    editmode) Relayed what I found to Clement and Jeroen
2.  <https://developer.blender.org/T74577> (world\_to\_camera\_view does
    not take into account x/y camera shift) Fixed\!

Wed:

Did some minor triaging.

Looked into and posted a fix for: <https://developer.blender.org/T76902>
(Sculpt "show face sets" option not working)

Did an initial review of: <https://developer.blender.org/D5797>

Continued to look into: <https://developer.blender.org/T76855> (Normal
map flips in editmode)

Thu:

Dutch national holiday

Fri:

I tried out the git master version of xorg-server yesterday and noticed
that it has some breaking changes when trying to run blender with
xwayland.

Bisected and reported it here:
<https://gitlab.freedesktop.org/xorg/xserver/-/issues/1032>

Hopefully this will be fixed before any Xorg release contains these
breaking changes.

Spent more time reviewing: <https://developer.blender.org/D5797> (Rigid
body compound shapes)

Worked some more on: <https://developer.blender.org/D7772> (Surface
deform floating point issues)

Next week: Fix up the remainder of my submitted patches.

## May 11 - May 15

Mon:

Worked with Jeroen to see if we should narrow down a viewport rending
issue (https://gitlab.freedesktop.org/mesa/mesa/-/issues/2941)

Worked with Sybren to give him a second opinion on a few issues.

Fixed an issue with \`Frame Dropping\`:
<https://developer.blender.org/D7694>

Tue:

Reviewed: <https://developer.blender.org/D7692> (Negative shrink value
doesn't animate)

Continued work on my "Frame dropping" and "Custom normal flipping"
fixes.

Started looking into and posted a potential fix for:
<https://developer.blender.org/T76541>

Wed:

Spent some time discussing UI design feedback issues in
\#blender-coders.

Looked into and fixed: T76717 (Set Rotation Mode Incorrectly
Recalculates Bone Rotation In Pose Mode)

Continued working on my "Custom normal flipping" patch after Bastien
gave me more feedback: <https://developer.blender.org/D7528>

Thu:

Did some minor triaging.

Proposed a minor quick fix for the old group operators (more work need
to be done to figure out the design issues as a whole):
<https://developer.blender.org/T53662> However, this was scrapped after
some talks with Bastien as doing a bigger rework would be better. Added
a new "max ray distance" setting to the baking menu:
<https://developer.blender.org/D7733>

Started looking into a potential surface deform bug (might just be
floating point errors).

Fri:

Filled out my WBSO sheet.

Continued work on the baking patch:
<https://developer.blender.org/D7733>

Figured out the issue with the surface deform modifier from yesterday,
looking into a fix.

Next week:

More bugtracker work\!

## May 04 - May 08

Mon & Wed: More particle physics work. Did some bug fixing and implement
damping and properly hooked up multiple collision per time step. As can
be seen in the video below, there seems to be some randomness going on
that shouldn't be there. I need to figure out why that happens.

![../videos/Particle\_wave.mp4](../videos/Particle_wave.mp4
"../videos/Particle_wave.mp4")

Tue: Dutch holiday

Thu: Continued to work on <https://developer.blender.org/D7528>

Fri: Rewrote the A/V sync patch based on feedback from the audaspace
creator. Will probably need to have some more back and forth before it
is merged upstream.

Helped Andy bisect a freezing issue:
<https://developer.blender.org/T76553>

Next Week: Seems like it will be only tracker work(?)

## Apr 27 - May 01

Mon: Dutch holiday

Tue: Went through and checked that report tickets that I had replied to
previously had not been left hanging. In most cases they had not, so no
further intervention was needed. Worked some more on particle physics.

Wed: Worked on particle physics, it is in a better state than before
now. But there are still some issues left.
![../videos/Particle\_work.mp4](../videos/Particle_work.mp4
"../videos/Particle_work.mp4")

Thu: Talked to Campbell about the new operator search. Worked more on
getting <https://developer.blender.org/D7528> ready for master.

Friday: Talked the Jacques about how to tackle certain issues I ran into
with the particle collision response. I spent the rest of the day
looking into and testing ´clang-rename´ for the "Write a script to
automatically rename DNA fields" code quality task.

Next week:

More work on particle physics and bug tracking

## Apr 20 - Apr 24

Mon:

Wasn't that much of a productive day, spent most of the time chatting
about different topics (LANPR, UI Team, and helping Mike with technical
issues). I have yet to manage to figure out what the exact problem is
with my particle collision code.

Tue:

Helped Hjalti solve some driver issues that made his blender crash
(newer mesa versions are not affected). Got some feedback from the
audaspace dev about my work with A/V sync. Took some time to write down
some thoughts on how to proceed. Spent most of my day chatting with
other people about various topics.

Wed:

Finally some progress with the moving collider response\! Now the
particles do not tunnel through the collider anymore (was because of a
silly math mistake):
![../videos/No\_tunneling\_particle\_coll.mp4](../videos/No_tunneling_particle_coll.mp4
"../videos/No_tunneling_particle_coll.mp4")

However, as you can see the velocity that the particles get from the
surface deflection and collision is to high still.

Thu:

Talked a bit more with the Audaspace dev on how to best implement the
interpolated time reporting mechanism. Fixed an issue with push/relax
pose to breakdown that I missed last time I worked on it:
<https://developer.blender.org/rB35ecfefaec231772d3cd366d21240636b9940be1>
Bisected and talked to Sergey about
[\#73686](http://developer.blender.org/T73686)

Fri:

Did some minor triaging. Helped Pablico setup OBS so he could record
videos. Created a fix for: <https://developer.blender.org/T76025>

Next week:

More work on particle physics and bugtracking

## Apr 14 - Apr 17

Mon: Day off

Tue: Sick

Wed: Still somewhat sick. Fixed
[\#75649](http://developer.blender.org/T75649) and worked further on A/V
sync.

Thu: Now I have something to show with A/V sync\! This is of course a
example that is exaggerated to show case the issue. Old A/V sync with
big audio buffer:

![../videos/Old.mp4](../videos/Old.mp4 "../videos/Old.mp4")

New A/V sync with big audio buffer:

![../videos/New.mp4](../videos/New.mp4 "../videos/New.mp4")

Fri:

Created pull requests for the changes I'ven done on the audaspace github
page. I also worked a bit more on the syncing logic and it is now
buttery smooth. I can no longer tell the difference between A/V sync and
No Sync. ![../videos/AV\_sync\_mk2.mp4](../videos/AV_sync_mk2.mp4
"../videos/AV_sync_mk2.mp4")

Started looking into: <https://developer.blender.org/T75810>

Thoughts about this week:

Because I was sick and didn't feel well at the start of this week, Dalai
and I decided that I should probably wrap up my A/V sync work this week.
So I did that instead of working on the physics system.

Next week:

Will allocate 3 days for particle collisions and 2 days for bug tracker
work.

## Apr 06 - Apr 10

Mon: Fixed up and commited D7248. Had some additional discussion about
A/V sync issues with Sybren.

Tue: Fixed T73598. Merged D6009.

Wed-Thu: Looked more into the A/V sync issues

Fri: Fixed T67232, continued work on A/V sync.

Next week: More work on particle collisions

## Mar 30 - Apr 03

Worked on particle collision detection and collision response the whole
week.

Now collision detection of moving objects seems to work nicely:

<center>

|                                                                             |
| --------------------------------------------------------------------------- |
| ![../videos/Col\_test.mp4](../videos/Col_test.mp4 "../videos/Col_test.mp4") |

style="caption-side: bottom" | Broken particle collision responses with
moving objects.

</center>

However there are still major issues with the collision response:

<center>

|                                                                      |
| -------------------------------------------------------------------- |
| ![../videos/Testx2.mp4](../videos/Testx2.mp4 "../videos/Testx2.mp4") |

style="caption-side: bottom" | Broken particle collision responses with
moving objects.

</center>

But in certain cases it doesn't fail spectacularly at least:

<center>

|                                                                               |
| ----------------------------------------------------------------------------- |
| ![../videos/Recording.mp4](../videos/Recording.mp4 "../videos/Recording.mp4") |

style="caption-side: bottom" | A bit better response, but not great.

</center>

Next week: Tracker work.

## Mar 23 - Mar 27

Mon:

  - Discussed <https://developer.blender.org/D7196> with Philipp. We
    brain stormed a bit about possible solutions.
  - Worked on D6009 (Fix T66751: Symmetrizing armature does not
    symmetrize constraints)
  - Bisected an issue I rand into during pose mode testing and reported
    it as T75036
  - Looked into T74921
  - Did some triaging and IT support.

Tue:

  - Looked into T75047 and started work on setting up WSL for some of
    our web devs.

Wed:

  - DDOS so couldn't do much tracker work. Spent the whole day helping
    with setting up WSL and general IT support

Thu:

  - Did some more general IT support.
  - Committed a fix for T75047: Number input advanced mode is not
    working in JP keyboard.
  - Looked into T74111 and posted a potential fix.

Fri:

  - Small amount of IT support.
  - The rest of the day was spent trying to figure out why particle
    collisions doesn't work as expected.

Next week:

  - More work on particle collisions.

## Mar 16 - Mar 20

Mon:

Setting up to work from home. Got a bit of work done with particle
collisions.

Tue:

Acting as IT support for others to get them setup to work from home.

Rest of the week: On and off acting as IT support and further work on
particle collisions.

Video of broken collisions:

<center>

|                                                                                                                                  |
| -------------------------------------------------------------------------------------------------------------------------------- |
| ![../videos/Broken\_particle\_coll\_test.mp4](../videos/Broken_particle_coll_test.mp4 "../videos/Broken_particle_coll_test.mp4") |

style="caption-side: bottom" | Broken particle collision detections with
moving objects.

</center>

All particles that have a confirmed collision will be teleported to the
middle of the plane. As you can see, the collision detection is not
working that great... (I'm currently trying to figure out why as it
should all work in theory)

Next week: Bugtracker work and one day or particle collision work.

## Mar 09 - Mar 13

Mon:

Fast mass spring experiments

Tue:

Talked to Jacques about planning for particle simulation solvers Looked
our options solver wise on what to implement first Worked on getting
particle \<-\> mesh collisions working

Wed:

More work on getting particle \<-\> mesh collision working

Thu:

Looked into T69223 and talked to Bastien about it (changed to known
issue). Fixed T71961 Soft body behavior is incorrect when CTRL + F12
animation is rendered. Looked into T71588 (might get solver later with
the work Germano is doing on the rigid body modifier)

Fri:

Fixed T74397 Crash after undoing quadriflow remesh on duplicate with
armature deform Looked into: T70546

\- Plan for next week:

Continue working on particle collisions.

## Mar 02 - Mar 06

  - Did general bugfixing work and patch review this week

## Feb 24 - Feb 28

  - Setup some new computers at the office
  - Did some more cloth research/coding

## Feb 17 - Feb 21

  - Was sick this week
  - Did some more cloth research

## Feb 10 - Feb 14

  - Tracker curfew
  - Helped setting up and calibrate monitors at the office

## Feb 3 - Feb 7

  - Tracker curfew
  - Did more fixing of computers and equipment at the office.

## Jan 27 - Jan 31

  - Most of the week went to fixing computers issues in the office.
  - Did some research and test with newer cloth solvers

## Jan 20 - Jan 24

  - Spent the whole week building, setting up, and fixing computers.

## Jan 13 - Jan 17

  - Tracker curfew work
  - Reviewed and merged the hair collision patch by Luca Rood
  - Further work on the krita plugins

## Jan 08 - Jan 10

  - Worked on more hardware dump script
  - Wrote some krita plugins

## Dec 09 - Dec 13

  - Added cloth internal springs to master
  - Worked on assembling new office furniture
  - Worked on setting up some KVM virtual machines

## Dec 02 - Dec 6

  - Added cloth pressure vertex groups
  - Prepared cloth internal spring for merging
  - More polishing on the decimate fcurves feature

## Nov 25 - Nov 29

  - More LANPR review
  - Got cloth pressure merged into master
  - Worked to further improve the decimate fcurves feature

## Nov 18 - Nov 22

  - Worked on reviewing the LANPR patch
  - Merge the new "decimate fcurves" into master
  - Trying to get my cloth patches (pressure/internal springs) ready for
    master

## Nov 11 - Nov 15

Same as last week:

  - Worked on reviewing the LANPR patch
  - More work on the office hardware scripts

## Nov 4 - Nov 8

  - Worked on reviewing the LANPR patch
  - Rewrote the office hardware dump script to dump HW serial numbers if
    available

## Oct 28 - Nov 1

  - Worked on getting stuff setup again after the blender conference.
  - Looked into some more quadriflow debugging features
  - Worked on getting my older patches merged

## Oct 21 - Oct 25

Worked on preparations for the blender conference.

## Oct 14 - Oct 18

Worked on some animation system backlog bugs this week.

  - Bugfixes:
      - Fix autokeyframe not working on bones mirrored bones

<!-- end list -->

  - Waiting for review:
      - D5450 Fix T62631: Cloth vertex mass capped to a very low number
        on scaled scenes.
      - D5173 Fix T66142: Redo panel doesn't work correctly after
        certain transformations.
      - D4841 Add curve decimate in the graph editor
      - D4632 Fix T62932 3D Cursor tool settings is not shared globally
      - D4522 Fix T59065: Blender default keyset is deletable by user\!
      - D4158 Fix T60098: Cycles does not respect the texture alpha mode

<!-- end list -->

  - WIP:
      - D6092 Fix T70805: Pose Bones rotate from wrong pivot when
        translated by Action Constraints.
      - D6009 Fix T66751: Symmetrizing armature does not symmetrize
        constraints
      - D4398 Fix T61814: Metaball basis handling is broken
      - D4040 \[Blender2.8\] Continuation of T58840, refresh result on
        more vpaint operator

## Oct 07 - Oct 11

Looked into more quadriflow issues this week and did some planning with
other developers how to tackle certain issues.

  - Bugfixes:
      - D5892 Fix T67212: No smooth blending in "Push Pose from
        Breakdown" and "Relax Pose to Breakdown"
      - D5877 Fix T70095: Quadriflow crash running on a messy mesh

<!-- end list -->

  - Waiting for review:
      - D5450 Fix T62631: Cloth vertex mass capped to a very low number
        on scaled scenes.
      - D5173 Fix T66142: Redo panel doesn't work correctly after
        certain transformations.
      - D4841 Add curve decimate in the graph editor
      - D4632 Fix T62932 3D Cursor tool settings is not shared globally
      - D4522 Fix T59065: Blender default keyset is deletable by user\!
      - D4158 Fix T60098: Cycles does not respect the texture alpha mode

<!-- end list -->

  - WIP:
      - D6009 Fix T66751: Symmetrizing armature does not symmetrize
        constraints
      - D4398 Fix T61814: Metaball basis handling is broken
      - D4040 \[Blender2.8\] Continuation of T58840, refresh result on
        more vpaint operator

## Sep 30 - Oct 04

Did some smaller bug fixes, but most of my week was spent sick. Didn't
get that much done.

  - Bugfixes:
      - D5892 Fix T67212: No smooth blending in "Push Pose from
        Breakdown" and "Relax Pose to Breakdown"
      - D5877 Fix T70095: Quadriflow crash running on a messy mesh

<!-- end list -->

  - Waiting for review:
      - D5450 Fix T62631: Cloth vertex mass capped to a very low number
        on scaled scenes.
      - D5173 Fix T66142: Redo panel doesn't work correctly after
        certain transformations.
      - D4841 Add curve decimate in the graph editor
      - D4632 Fix T62932 3D Cursor tool settings is not shared globally
      - D4522 Fix T59065: Blender default keyset is deletable by user\!
      - D4158 Fix T60098: Cycles does not respect the texture alpha mode

<!-- end list -->

  - WIP:
      - D4398 Fix T61814: Metaball basis handling is broken
      - D4040 \[Blender2.8\] Continuation of T58840, refresh result on
        more vpaint operator

## Sep 23 - Sep 27

Most of the week went into looking into quadriflow. The majority of the
issues was because of bad input meshes. So a patch in under review to
remedy this.

  - Bugfixes:
      - Fix T69542: Corrective Smooth modifier breaks when drivers are
        involved
      - Removed auto generated config.h file from quadriflow

<!-- end list -->

  - Waiting for review:
      - D5892 Fix T67212: No smooth blending in "Push Pose from
        Breakdown" and "Relax Pose to Breakdown"
      - D5877 Fix T70095: Quadriflow crash running on a messy mesh
      - D5450 Fix T62631: Cloth vertex mass capped to a very low number
        on scaled scenes.
      - D5173 Fix T66142: Redo panel doesn't work correctly after
        certain transformations.
      - D4841 Add curve decimate in the graph editor
      - D4632 Fix T62932 3D Cursor tool settings is not shared globally
      - D4522 Fix T59065: Blender default keyset is deletable by user\!
      - D4158 Fix T60098: Cycles does not respect the texture alpha mode

<!-- end list -->

  - WIP:
      - D4398 Fix T61814: Metaball basis handling is broken
      - D4040 \[Blender2.8\] Continuation of T58840, refresh result on
        more vpaint operator

## Sep 16 - Sep 20

Worked on getting a hardware dumping script up and running for the
studio. The rest of the week was spend fixing and building computers for
our new arrivals.

  - Waiting for review:
      - D5450 Fix T62631: Cloth vertex mass capped to a very low number
        on scaled scenes.
      - D5173 Fix T66142: Redo panel doesn't work correctly after
        certain transformations.
      - D4841 Add curve decimate in the graph editor
      - D4632 Fix T62932 3D Cursor tool settings is not shared globally
      - D4522 Fix T59065: Blender default keyset is deletable by user\!
      - D4158 Fix T60098: Cycles does not respect the texture alpha mode

<!-- end list -->

  - WIP:
      - D4398 Fix T61814: Metaball basis handling is broken
      - D4040 \[Blender2.8\] Continuation of T58840, refresh result on
        more vpaint operator

## Sep 09 - Sep 13

Got Quadriflow commited to master and worked on finishing up my cloth
patches.

  - Waiting for review:
      - D5450 Fix T62631: Cloth vertex mass capped to a very low number
        on scaled scenes.
      - D5173 Fix T66142: Redo panel doesn't work correctly after
        certain transformations.
      - D4841 Add curve decimate in the graph editor
      - D4632 Fix T62932 3D Cursor tool settings is not shared globally
      - D4522 Fix T59065: Blender default keyset is deletable by user\!
      - D4158 Fix T60098: Cycles does not respect the texture alpha mode

<!-- end list -->

  - WIP:
      - D5425 Fix potential issues when loading files with missing
        libraries
      - D4398 Fix T61814: Metaball basis handling is broken
      - D4040 \[Blender2.8\] Continuation of T58840, refresh result on
        more vpaint operator

## Sep 02 - Sep 06

Continued work on Quadriflow integration this week.

  - Waiting for review:
      - D5450 Fix T62631: Cloth vertex mass capped to a very low number
        on scaled scenes.
      - D5173 Fix T66142: Redo panel doesn't work correctly after
        certain transformations.
      - D4841 Add curve decimate in the graph editor
      - D4632 Fix T62932 3D Cursor tool settings is not shared globally
      - D4522 Fix T59065: Blender default keyset is deletable by user\!
      - D4158 Fix T60098: Cycles does not respect the texture alpha mode

<!-- end list -->

  - WIP:
      - D5425 Fix potential issues when loading files with missing
        libraries
      - D4398 Fix T61814: Metaball basis handling is broken
      - D4040 \[Blender2.8\] Continuation of T58840, refresh result on
        more vpaint operator

## Aug 26 - Aug 30

More Quadriflow works this week. I've added some basic edge border
preservation.

  - Waiting for review:
      - D5450 Fix T62631: Cloth vertex mass capped to a very low number
        on scaled scenes.
      - D5173 Fix T66142: Redo panel doesn't work correctly after
        certain transformations.
      - D4841 Add curve decimate in the graph editor
      - D4632 Fix T62932 3D Cursor tool settings is not shared globally
      - D4522 Fix T59065: Blender default keyset is deletable by user\!
      - D4158 Fix T60098: Cycles does not respect the texture alpha mode

<!-- end list -->

  - WIP:
      - D5425 Fix potential issues when loading files with missing
        libraries
      - D4398 Fix T61814: Metaball basis handling is broken
      - D4040 \[Blender2.8\] Continuation of T58840, refresh result on
        more vpaint operator

## Aug 19 - Aug 23

My time this week as dedicated to porting Quadriflow to Blender.

  - Waiting for review:
      - D5450 Fix T62631: Cloth vertex mass capped to a very low number
        on scaled scenes.
      - D5173 Fix T66142: Redo panel doesn't work correctly after
        certain transformations.
      - D4841 Add curve decimate in the graph editor
      - D4632 Fix T62932 3D Cursor tool settings is not shared globally
      - D4522 Fix T59065: Blender default keyset is deletable by user\!
      - D4158 Fix T60098: Cycles does not respect the texture alpha mode

<!-- end list -->

  - WIP:
      - D5425 Fix potential issues when loading files with missing
        libraries
      - D4398 Fix T61814: Metaball basis handling is broken
      - D4040 \[Blender2.8\] Continuation of T58840, refresh result on
        more vpaint operator

## Aug 12 - Aug 16

All of this week was used to fix computers at the office and work on
cloth physics improvements.

  - Waiting for review:
      - D5450 Fix T62631: Cloth vertex mass capped to a very low number
        on scaled scenes.
      - D5173 Fix T66142: Redo panel doesn't work correctly after
        certain transformations.
      - D4841 Add curve decimate in the graph editor
      - D4632 Fix T62932 3D Cursor tool settings is not shared globally
      - D4522 Fix T59065: Blender default keyset is deletable by user\!
      - D4158 Fix T60098: Cycles does not respect the texture alpha mode

<!-- end list -->

  - WIP:
      - D5425 Fix potential issues when loading files with missing
        libraries
      - D4398 Fix T61814: Metaball basis handling is broken
      - D4040 \[Blender2.8\] Continuation of T58840, refresh result on
        more vpaint operator

## Aug 05 - Aug 09

Most of this week was spent in meeting and fixing computer issues in the
office.

  - Bugfixes:
      - Fix crash when opening files with missing armature libaries
      - D5416 Fix T67665 "Affect Alpha" in Texture Paint mode doesn't
        work as expected
      - Fix T67821: Snap to Symmetry not updating
  - Waiting for review:
      - D5450 Fix T62631: Cloth vertex mass capped to a very low number
        on scaled scenes.
      - D5173 Fix T66142: Redo panel doesn't work correctly after
        certain transformations.
      - D4841 Add curve decimate in the graph editor
      - D4632 Fix T62932 3D Cursor tool settings is not shared globally
      - D4522 Fix T59065: Blender default keyset is deletable by user\!
      - D4158 Fix T60098: Cycles does not respect the texture alpha mode

<!-- end list -->

  - WIP:
      - D5425 Fix potential issues when loading files with missing
        libraries
      - D4398 Fix T61814: Metaball basis handling is broken
      - D4040 \[Blender2.8\] Continuation of T58840, refresh result on
        more vpaint operator

<!-- end list -->

  - Triaged 8 reports.
  - Assigned 10 reports.
  - Closed 6 reports.
  - Merged 5 reports

## Jul 29 - Aug 02

  - Bugfixes:
      - D5401 Fix T68073: Wacom Intuos 5S no pen pressure on Wayland
      - D4665 Fix T63429: Random deselect function lost
      - D5380 Have CMake be more strict when optional x11 libraries are
        missing but enabled
      - D5361 Fix T67460: Vertex painting: Sampling color opens empty
        options window in viewport
      - D5365 Fix T67274: Graph Editor Normalization broken by Keyframe
        that uses Easing or Dynamic interpolation
      - D5128 Fix T63921: Unable to use confirm on release for keyboard
        shortcuts
      - D5132 Fix T65837: "Zoom Axis" is not working on the node editor

<!-- end list -->

  - Waiting for review:
      - D5173 Fix T66142: Redo panel doesn't work correctly after
        certain transformations.
      - D4841 Add curve decimate in the graph editor
      - D4632 Fix T62932 3D Cursor tool settings is not shared globally
      - D4522 Fix T59065: Blender default keyset is deletable by user\!
      - D4158 Fix T60098: Cycles does not respect the texture alpha mode

<!-- end list -->

  - WIP:
      - D4398 Fix T61814: Metaball basis handling is broken
      - D4040 \[Blender2.8\] Continuation of T58840, refresh result on
        more vpaint operator

<!-- end list -->

  - Triaged 48 reports.
  - Assigned 19 reports.
  - Closed 46 reports.
  - Merged 9 reports

## Jul 21 - Jul 24

  - Bugfixes:
      - Fix T67117: Font thumbnails crash to desktop
  - Waiting for review:
      - D5173 Fix T66142: Redo panel doesn't work correctly after
        certain transformations.
      - D5128 Fix T63921: Unable to use confirm on release for keyboard
        shortcuts
      - D4841 Add curve decimate in the graph editor
      - D4632 Fix T62932 3D Cursor tool settings is not shared globally
      - D4522 Fix T59065: Blender default keyset is deletable by user\!
      - D4158 Fix T60098: Cycles does not respect the texture alpha mode

<!-- end list -->

  - WIP:
      - D5128 Fix T63921: Unable to use confirm on release for keyboard
        shortcuts
      - D5132 Fix T65837: "Zoom Axis" is not working on the node editor
      - D4398 Fix T61814: Metaball basis handling is broken
      - D4040 \[Blender2.8\] Continuation of T58840, refresh result on
        more vpaint operator

<!-- end list -->

  - Triaged 31 reports.
  - Assigned 18 reports.
  - Closed 23 reports.
  - Merged 3 reports

## Jul 15 - Jul 19

  - Bugfixes:
      - Fix T67062: Movie Clip Editor does not update Editor Type when
        changing Editing Context
      - Fix T66999: Blender creates new cache for particles with step 10
        instead 1
      - Fix T66835: Dynamic Paint weight group isn't updated unless
        weight has been assigned
      - Fix T66870: AutoIK-Len stops working after releasing the mouse
      - Fix T66587: Can't bake second dynamic paint canvas to image
        sequence
  - Waiting for review:
      - D5173 Fix T66142: Redo panel doesn't work correctly after
        certain transformations.
      - D5128 Fix T63921: Unable to use confirm on release for keyboard
        shortcuts
      - D4841 Add curve decimate in the graph editor
      - D4632 Fix T62932 3D Cursor tool settings is not shared globally
      - D4522 Fix T59065: Blender default keyset is deletable by user\!
      - D4158 Fix T60098: Cycles does not respect the texture alpha mode

<!-- end list -->

  - WIP:
      - D5128 Fix T63921: Unable to use confirm on release for keyboard
        shortcuts
      - D5132 Fix T65837: "Zoom Axis" is not working on the node editor
      - D4398 Fix T61814: Metaball basis handling is broken
      - D4040 \[Blender2.8\] Continuation of T58840, refresh result on
        more vpaint operator

<!-- end list -->

  - Triaged 47 reports.
  - Assigned 24 reports.
  - Closed 36 reports.
  - Merged 5 reports

## Jul 8 - Jul 12

Noticed that the phabricator default reporting style had changed. So the
prior weeks reported bug tracker numbers have been wrong. The numbers
for this week should be correct.

  - Bugfixes
      - Fix T66560 Sequencer: Shortcut keys no showing in menu for
        'Move' and 'Refresh All'
      - D5208 Fix T64149: Texture paint can act as a canvas and brush at
        the same time
  - Waiting for review:
      - D5173 Fix T66142: Redo panel doesn't work correctly after
        certain transformations.
      - D5128 Fix T63921: Unable to use confirm on release for keyboard
        shortcuts
      - D4841 Add curve decimate in the graph editor
      - D4632 Fix T62932 3D Cursor tool settings is not shared globally
      - D4522 Fix T59065: Blender default keyset is deletable by user\!
      - D4158 Fix T60098: Cycles does not respect the texture alpha mode

<!-- end list -->

  - WIP:
      - D5128 Fix T63921: Unable to use confirm on release for keyboard
        shortcuts
      - D5132 Fix T65837: "Zoom Axis" is not working on the node editor
      - D4398 Fix T61814: Metaball basis handling is broken
      - D4040 \[Blender2.8\] Continuation of T58840, refresh result on
        more vpaint operator

<!-- end list -->

  - Triaged 52 reports.
  - Assigned 29 reports.
  - Closed 7 reports.
  - Merged 14 reports

## Jul 1 - Jul 5

  - Bugfixes
      - D5183 T66266: Grease Pencil Simplify Adaptive is not behaving
        correctly
      - D5151 Fix T66165: RGB Curve node generates too bright color
  - Waiting for review:
      - D5173 Fix T66142: Redo panel doesn't work correctly after
        certain transformations.
      - D5128 Fix T63921: Unable to use confirm on release for keyboard
        shortcuts
      - D4841 Add curve decimate in the graph editor
      - D4632 Fix T62932 3D Cursor tool settings is not shared globally
      - D4522 Fix T59065: Blender default keyset is deletable by user\!
      - D4158 Fix T60098: Cycles does not respect the texture alpha mode

<!-- end list -->

  - WIP:
      - D5128 Fix T63921: Unable to use confirm on release for keyboard
        shortcuts
      - D5132 Fix T65837: "Zoom Axis" is not working on the node editor
      - D4398 Fix T61814: Metaball basis handling is broken
      - D4040 \[Blender2.8\] Continuation of T58840, refresh result on
        more vpaint operator

<!-- end list -->

  - Triaged 6 reports.
  - Assigned 9 reports.
  - Closed 7 reports.
  - Merged 4 reports

## Jun 22 - Jun 26

Had more computer issues this week. Not much got done...

  - Bugfixes

<!-- end list -->

  - Waiting for review:
      - D5128 Fix T63921: Unable to use confirm on release for keyboard
        shortcuts
      - D4841 Add curve decimate in the graph editor
      - D4632 Fix T62932 3D Cursor tool settings is not shared globally
      - D4522 Fix T59065: Blender default keyset is deletable by user\!
      - D4158 Fix T60098: Cycles does not respect the texture alpha mode

<!-- end list -->

  - WIP:
      - D5128 Fix T63921: Unable to use confirm on release for keyboard
        shortcuts
      - D5132 Fix T65837: "Zoom Axis" is not working on the node editor
      - D4398 Fix T61814: Metaball basis handling is broken
      - D4040 \[Blender2.8\] Continuation of T58840, refresh result on
        more vpaint operator

<!-- end list -->

  - Triaged 4 reports.
  - Assigned 17 reports.
  - Closed 5 reports.
  - Merged 3 reports

## Jun 17 - Jun 21

  - Bugfixes
      - Fix T65802: F-curves modifiers in nodes doesn't updates properly
      - Fix T65775: UV projection is dependant of the object position
      - D4138 Fix T59915: Skin modifier produces inverted normals on
        end-cap faces when vertically aligned edge is assigned root

<!-- end list -->

  - Waiting for review:
      - D4841 Add curve decimate in the graph editor
      - D4632 Fix T62932 3D Cursor tool settings is not shared globally
      - D4522 Fix T59065: Blender default keyset is deletable by user\!
      - D4158 Fix T60098: Cycles does not respect the texture alpha mode

<!-- end list -->

  - WIP:
      - D4398 Fix T61814: Metaball basis handling is broken
      - D4040 \[Blender2.8\] Continuation of T58840, refresh result on
        more vpaint operator

<!-- end list -->

  - Triaged 24 reports.
  - Assigned 36 reports.
  - Closed 10 reports.
  - Merged 6 reports

## Jun 10 - Jun 14

  - Bugfixes
      - Fix T65632: Connected Proportional Editing is affected by Curve
        Object's Local Scale
      - Fix T64930: FFmpeg Output- no color mode by default
      - D5033 Fix T64533: Using "X-Axis Mirror" while posing with auto
        keyframe on does not keyframe the mirrored bone
  - Waiting for review:
      - D4841 Add curve decimate in the graph editor
      - D4632 Fix T62932 3D Cursor tool settings is not shared globally
      - D4522 Fix T59065: Blender default keyset is deletable by user\!
      - D4158 Fix T60098: Cycles does not respect the texture alpha mode
      - D4138 Fix T59915: Skin modifier produces inverted normals on
        end-cap faces when vertically aligned edge is assigned root
  - WIP:
      - D4398 Fix T61814: Metaball basis handling is broken
      - D4040 \[Blender2.8\] Continuation of T58840, refresh result on
        more vpaint operator
  - Triaged 78 reports.
  - Assigned 58 reports.
  - Closed 45 reports.
  - Merged 9 reports

## May 6 - May 10

  - Bugfixes
      - D4808 Add ability to create a keyboard shortcut for more mode
        settings.
      - D4832 Move out pose edit options into the pose data
  - Waiting for review:
      - D4841 Add curve decimate in the graph editor
      - D4632 Fix T62932 3D Cursor tool settings is not shared globally
      - D4522 Fix T59065: Blender default keyset is deletable by user\!
      - D4158 Fix T60098: Cycles does not respect the texture alpha mode
      - D4138 Fix T59915: Skin modifier produces inverted normals on
        end-cap faces when vertically aligned edge is assigned root
  - WIP:
      - D4398 Fix T61814: Metaball basis handling is broken
      - D4040 \[Blender2.8\] Continuation of T58840, refresh result on
        more vpaint operator
  - Triaged 57 reports.
  - Assigned 51 reports.
  - Closed 25 reports.
  - Merged 11 reports

## April 29 - May 3

  - Bugfixes
      - Implement mirroring in pose mode (absolute and relative).
  - Waiting for review:
      - D4632 Fix T62932 3D Cursor tool settings is not shared globally
      - D4522 Fix T59065: Blender default keyset is deletable by user\!
      - D4158 Fix T60098: Cycles does not respect the texture alpha mode
      - D4138 Fix T59915: Skin modifier produces inverted normals on
        end-cap faces when vertically aligned edge is assigned root
  - WIP:
      - D4398 Fix T61814: Metaball basis handling is broken
      - D4040 \[Blender2.8\] Continuation of T58840, refresh result on
        more vpaint operator
  - Triaged 20 reports.
  - Assigned 27 reports.
  - Closed 19 reports.
  - Merged 6 reports

## April 23 - April 26

First week of my trip to BI in Amsterdam. Took a while to get everything
setup, so a bit slower week than usual.

  - Bugfixes
  - Waiting for review:
      - D4632 Fix T62932 3D Cursor tool settings is not shared globally
      - D4522 Fix T59065: Blender default keyset is deletable by user\!
      - D4158 Fix T60098: Cycles does not respect the texture alpha mode
      - D4138 Fix T59915: Skin modifier produces inverted normals on
        end-cap faces when vertically aligned edge is assigned root
  - WIP:
      - D4398 Fix T61814: Metaball basis handling is broken
      - D4040 \[Blender2.8\] Continuation of T58840, refresh result on
        more vpaint operator
  - Triaged 16 reports.
  - Assigned 45 reports.
  - Closed 14 reports.
  - Merged 6 reports

## April 15 - April 18

  - Bugfixes
      - D4665 Fix T63429: Random deselect function lost
      - D4646 Fix T63247: edbm\_rip\_invoke\_\_edge rips unselected
        verts on mesh borders
      - Fix T63663: Object mode proportional editing affects objects
        which are disabled…
  - Waiting for review:
      - D4632 Fix T62932 3D Cursor tool settings is not shared globally
      - D4522 Fix T59065: Blender default keyset is deletable by user\!
      - D4158 Fix T60098: Cycles does not respect the texture alpha mode
      - D4138 Fix T59915: Skin modifier produces inverted normals on
        end-cap faces when vertically aligned edge is assigned root
  - WIP:
      - D4398 Fix T61814: Metaball basis handling is broken
      - D4040 \[Blender2.8\] Continuation of T58840, refresh result on
        more vpaint operator
  - Triaged 32 reports.
  - Assigned 47 reports.
  - Closed 18 reports.
  - Merged 12 reports

## April 8 - April 12

  - Bugfixes
      - Fix T63341: Xray mode makes some shading popover options grayed
        out despite them working
      - Fix T63467: Edge/vertex selection isnt working properly with
        X-ray set to 1
      - D4504 Fix T62114: Wireframe mode selection: selects backside
        objects when clicking frontside object in 3d-window
  - Waiting for review:
      - D4665 Fix T63429: Random deselect function lost
      - D4646 Fix T63247: edbm\_rip\_invoke\_\_edge rips unselected
        verts on mesh borders
      - D4632 Fix T62932 3D Cursor tool settings is not shared globally
      - D4522 Fix T59065: Blender default keyset is deletable by user\!
      - D4158 Fix T60098: Cycles does not respect the texture alpha mode
      - D4138 Fix T59915: Skin modifier produces inverted normals on
        end-cap faces when vertically aligned edge is assigned root
  - WIP:
      - D4398 Fix T61814: Metaball basis handling is broken
      - D4040 \[Blender2.8\] Continuation of T58840, refresh result on
        more vpaint operator
  - Triaged 34 reports.
  - Assigned 62 reports.
  - Closed 28 reports.
  - Merged 12 reports

## April 1 - April 5

  - Bugfixes
      - D4653 Fix T63281: Drivers inside nodegroups inside nodegroups
        don't show up in Driver Editor
      - Fix T63120 Select random in particle edit mode is broke
  - Waiting for review:
      - D4646 Fix T63247: edbm\_rip\_invoke\_\_edge rips unselected
        verts on mesh borders
      - D4632 Fix T62932 3D Cursor tool settings is not shared globally
      - D4522 Fix T59065: Blender default keyset is deletable by user\!
      - D4504 Fix T62114: Wireframe mode selection: selects backside
        objects when clicking frontside object in 3d-window
      - D4158 Fix T60098: Cycles does not respect the texture alpha mode
      - D4138 Fix T59915: Skin modifier produces inverted normals on
        end-cap faces when vertically aligned edge is assigned root
  - WIP:
      - D4398 Fix T61814: Metaball basis handling is broken
      - D4040 \[Blender2.8\] Continuation of T58840, refresh result on
        more vpaint operator
  - Triaged 32 reports.
  - Assigned 55 reports.
  - Closed 17 reports.
  - Merged 8 reports

## March 25 - March 29

I had some computer issues this week, so it was a little slower as I had
to migrate to an other computer.

  - Bugfixes
      - D4616 Fix T63086: Navigation problem with Background/Reference
        Images
      - D4590 Fix T62887: When searching for fonts, thumbnail dispaly
        mode will crash Blender
      - D4514 Fix T53997: \[bug\] island\_margin in
        bpy.ops.uv.smart\_project seems to have no effect
      - D4194 Fix T60421: Bone heads are hidden when non-connected
        parent is hidden
  - Waiting for review:
      - D4522 Fix T59065: Blender default keyset is deletable by user\!
      - D4504 Fix T62114: Wireframe mode selection: selects backside
        objects when clicking frontside object in 3d-window
      - D4158 Fix T60098: Cycles does not respect the texture alpha mode
      - D4138 Fix T59915: Skin modifier produces inverted normals on
        end-cap faces when vertically aligned edge is assigned root
  - WIP:
      - D4398 Fix T61814: Metaball basis handling is broken
      - D4040 \[Blender2.8\] Continuation of T58840, refresh result on
        more vpaint operator
  - Triaged 25 reports.
  - Assigned 45 reports.
  - Closed 14 reports.
  - Merged 4 reports

## March 18 - March 22

  - Bugfixes

<!-- end list -->

  - Waiting for review:
      - D4522 Fix T59065: Blender default keyset is deletable by user\!
      - D4504 Fix T62114: Wireframe mode selection: selects backside
        objects when clicking frontside object in 3d-window
      - D4194 Fix T60421: Bone heads are hidden when non-connected
        parent is hidden
      - D4158 Fix T60098: Cycles does not respect the texture alpha mode
      - D4138 Fix T59915: Skin modifier produces inverted normals on
        end-cap faces when vertically aligned edge is assigned root
  - WIP:
      - D4514 Fix T53997: \[bug\] island\_margin in
        bpy.ops.uv.smart\_project seems to have no effect
      - D4398 Fix T61814: Metaball basis handling is broken
      - D4040 \[Blender2.8\] Continuation of T58840, refresh result on
        more vpaint operator
  - Triaged 46 reports.
  - Assigned 60 reports.
  - Closed 13 reports.
  - Merged 6 reports

## March 11 - March 15

  - Bugfixes

<!-- end list -->

  - Waiting for review:
      - D4522 Fix T59065: Blender default keyset is deletable by user\!
      - D4504 Fix T62114: Wireframe mode selection: selects backside
        objects when clicking frontside object in 3d-window
      - D4194 Fix T60421: Bone heads are hidden when non-connected
        parent is hidden
      - D4158 Fix T60098: Cycles does not respect the texture alpha mode
      - D4138 Fix T59915: Skin modifier produces inverted normals on
        end-cap faces when vertically aligned edge is assigned root
  - WIP:
      - D4514 Fix T53997: \[bug\] island\_margin in
        bpy.ops.uv.smart\_project seems to have no effect
      - D4398 Fix T61814: Metaball basis handling is broken
      - D4040 \[Blender2.8\] Continuation of T58840, refresh result on
        more vpaint operator
  - Triaged 29 reports.
  - Assigned 56 reports.
  - Closed 21 reports.
  - Merged 11 reports

## March 4 - March 8

  - Bugfixes
      - Fix T62113: Color Management Curves CRGB buttons missing.
      - D4493 Fix T62125: snapping not working if invisible object is
        between you and the target
      - D4439 Fix T62104: VertexGroup.add() doesn't push depsgraph
        update for the mesh.
      - D4386 Fix T61737: Gizmo refresh issue
      - D4469 Fix T62162: Bones set to unselectable can't be fully
        selected through the viewport in edit mode.
  - Waiting for review:
      - D4194 Fix T60421: Bone heads are hidden when non-connected
        parent is hidden
      - D4158 Fix T60098: Cycles does not respect the texture alpha mode
      - D4138 Fix T59915: Skin modifier produces inverted normals on
        end-cap faces when vertically aligned edge is assigned root
  - WIP:
      - D4398 Fix T61814: Metaball basis handling is broken
      - D4040 \[Blender2.8\] Continuation of T58840, refresh result on
        more vpaint operator
  - Triaged 56 reports.
  - Assigned 54 reports.
  - Closed 35 reports.
  - Merged 21 reports

## February 25 - March 1

  - Bugfixes
      - D4417 Fix T61945: Scaling UV-sphere while using snap to vertex
        scales to incredible size.
  - Waiting for review:
      - D4439 Fix T62104: VertexGroup.add() doesn't push depsgraph
        update for the mesh.
      - D4398 Fix T61814: Metaball basis handling is broken
      - D4386 Fix T61737: Gizmo refresh issue
      - D4194 Fix T60421: Bone heads are hidden when non-connected
        parent is hidden
      - D4158 Fix T60098: Cycles does not respect the texture alpha mode
      - D4138 Fix T59915: Skin modifier produces inverted normals on
        end-cap faces when vertically aligned edge is assigned root
  - WIP:
      - D4040 \[Blender2.8\] Continuation of T58840, refresh result on
        more vpaint operator
  - Triaged 50 reports.
  - Assigned 55 reports.
  - Closed 24 reports.
  - Merged 17 reports

## February 18 - 22

Were sick the whole week so triaging was a bit harder.

  - Bugfixes
      - D4358 Fix T61563: INFO panel incorrectly printing operators
        using constraint\_matrix
      - D4338 Node headers should not respect theme alpha values
      - D4373 Fix T61690: Hidden curve vertices are drawn in edit-mode
  - Waiting for review:
      - D4398 Fix T61814: Metaball basis handling is broken
      - D4386 Fix T61737: Gizmo refresh issue
      - D4194 Fix T60421: Bone heads are hidden when non-connected
        parent is hidden
      - D4158 Fix T60098: Cycles does not respect the texture alpha mode
      - D4138 Fix T59915: Skin modifier produces inverted normals on
        end-cap faces when vertically aligned edge is assigned root
  - WIP:
      - D4040 \[Blender2.8\] Continuation of T58840, refresh result on
        more vpaint operator
  - Triaged 38 reports.
  - Assigned 41 reports.
  - Closed 24 reports.
  - Merged 9 reports

## February 11 - 15

  - Bugfixes
      - D4336 Fix [\#61376](http://developer.blender.org/T61376) Group
        Node Node Editor theme property is missing alpha channel
      - rB4f920371580b: Fix T61536: can't snap vertex to another vertex
        in edit mode using curves.
  - Waiting for review:
      - D4358 Fix T61563: INFO panel incorrectly printing operators
        using constraint\_matrix
      - D4338 Node headers should not respect theme alpha values
      - D4158 Fix T60098: Cycles does not respect the texture alpha mode
      - D4138 Fix T59915: Skin modifier produces inverted normals on
        end-cap faces when vertically aligned edge is assigned root
  - WIP:
      - D4040 \[Blender2.8\] Continuation of T58840, refresh result on
        more vpaint operator
  - Triaged 53 reports.
  - Assigned 54 reports.
  - Closed 31 reports.
  - Merged 11 reports

## February 4 - 8

  - Bugfixes waiting for review:
      - D4158 Fix T60098: Cycles does not respect the texture alpha mode
      - D4138 Fix T59915: Skin modifier produces inverted normals on
        end-cap faces when vertically aligned edge is assigned root
  - WIP:
      - D4040 \[Blender2.8\] Continuation of T58840, refresh result on
        more vpaint operator
  - Triaged 57 reports.
  - Assigned 59 reports.
  - Closed 37 reports.
  - Merged 9 reports.

## January 28 - Feb 1

  - Bugfixes waiting for review:
      - D4158 Fix T60098: Cycles does not respect the texture alpha mode
      - D4138 Fix T59915: Skin modifier produces inverted normals on
        end-cap faces when vertically aligned edge is assigned root
  - WIP:
      - D4040 \[Blender2.8\] Continuation of T58840, refresh result on
        more vpaint operator
  - Triaged 88 reports.
  - Assigned 63 reports.
  - Closed 39 reports.
  - Merged 12 reports.

## January 23 - 25

  - Bugfixes:
      - Fix T60602: Pose bones' selection doesn't update in the viewport
        if it was changed outside of the viewport.
  - Still waiting for review:
      - D4158 Fix T60098: Cycles does not respect the texture alpha mode
      - D4138 Fix T59915: Skin modifier produces inverted normals on
        end-cap faces when vertically aligned edge is assigned root
      - D4040 \[Blender2.8\] Continuation of T58840, refresh result on
        more vpaint operator
  - Triaged 67 reports.
  - Assigned 43 reports.
  - Closed 18 reports.
  - Merged 9 reports.

## December 31 - Jan 4

  - Bugfixes:
      - Fix T59946: Multiple quit dialogs possible
      - Fix T60068: Viewport shows old cached object names
      - Fix T59944: Python template operator\_mesh\_uv.py fails with an
        error
  - Still waiting for review:
      - D4158 Fix T60098: Cycles does not respect the texture alpha mode
      - D4138 Fix T59915: Skin modifier produces inverted normals on
        end-cap faces when vertically aligned edge is assigned root
      - D4089 Fix T59450: Texture Painting - Brush Panel - brush colors
        flip is inactive
      - D4040 \[Blender2.8\] Continuation of T58840, refresh result on
        more vpaint operator
  - Triaged 60 reports.
  - Assigned 39 reports.
  - Closed 28 reports.
  - Merged 12 reports.

## December 24 - 28

  - Bugfixes:
      - Fix T59865 UV editor: vertex snap, active does not make any
        sense.
      - Fix T59864: UV editor insufficient snap options
  - Still waiting for review:
      - D4147 Fix T59946: Multiple quit dialogs possible
      - D4146 Fix T59944: Python template operator\_mesh\_uv.py fails
        with an error
      - D4138 Fix T59915: Skin modifier produces inverted normals on
        end-cap faces when vertically aligned edge is assigned root
      - D4040 \[Blender2.8\] Continuation of T58840, refresh result on
        more vpaint operator
      - D4089 Fix T59450: Texture Painting - Brush Panel - brush colors
        flip is inactive
  - WIP:
      - Fix T59633: Impossible to show only x/y axis in ortho front/side
        view
  - Triaged 23 reports.
  - Assigned 14 reports.
  - Closed 7 reports.
  - Merged 12 reports.

## December 17 - 21

  - Bugfixes:
      - Fix T59083: Normal transform orientation ignores absolute grid
        snap
      - Fix T59424: Color wheel snaps to center: loosing hue if light
        value is down to zero.
      - Fix T59387: Simple Crash
      - Fix T59391: 2.80 Crash on selecting an entire collection and
        snappping cursor to active.
  - Still waiting for review:
      - D4040 \[Blender2.8\] Continuation of T58840, refresh result on
        more vpaint operator
      - D4089 Fix T59450: Texture Painting - Brush Panel - brush colors
        flip is inactive
  - WIP:
      - Fix T59633: Impossible to show only x/y axis in ortho front/side
        view
  - Triaged 58 reports.
  - Assigned 44 reports.
  - Closed 17 reports.
  - Merged 17 reports.

## December 10 - 14

  - Bugfixes:
      - Fix T59358: Wrong status bar keymap for release confirms and
        right click select.
  - Still waiting for review:
      - D4040 \[Blender2.8\] Continuation of T58840, refresh result on
        more vpaint operator
      - D4069 Fix T59083: Normal transform orientation ignores absolute
        grid snap
  - Triaged 58 reports.
  - Assigned 55 reports.
  - Closed 19 reports.
  - Merged 12 reports.

## December 5 - 7

  - Bugfixes:
      - Fix T58750: missing fallof power tooltip.
      - Fix T58659: absolute grid snapping wrong with custom grid scale.
  - Triaged about 60 reports.
