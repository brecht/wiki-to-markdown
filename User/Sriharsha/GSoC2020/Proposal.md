**Google Summer of Code 2020**

# Liquid Simulation Display Options - Proposal

### Name

Sriharsha Kotcharlakot

### Contact

  - Email: k.venkatsriharsha@gmail.com
  - Sriharsha @ blender.chat,
    [devtalk](https://devtalk.blender.org/u/sriharsha) and
    [developer.blender.org](https://developer.blender.org/p/Sriharsha/)
  - GitHub: <https://github.com/srihk>

### Synopsis

The goal of this project is to make the viewport more informative by
adding options to visualize liquid simulation grids and improving
existing fluid display options in Workbench.

### Benefits

Currently, Blender’s viewport visualization options for liquid
simulation grids are not present. If the velocities and external forces
are visualized as vectors within the simulation grids, the viewport
could be more informative and useful for producing desired liquid
simulations.

### Deliverables

  - Improved liquid simulation visualization within Blender’s viewport
    with added support for visualization of velocity vectors and
    external force vectors within dynamic liquid simulation grids.
  - A panel under liquid domain to control the possible options for
    viewport display.
  - End-user documentation.

### Project Details

Vector properties like velocity and external forces can be visualised
using vectors in the liquid simulation grids on a per-cell basis. All
the particles present within a cell of the grid contribute to the
properties represented by that cell. Average of the velocities of all
the particles present in a cell can be taken as the velocity represented
by that cell. Instead of average, other filters like maximum or minimum
can be used.  
The generated data finally gets visualized in the form of vectors or
colours within the simulation grid.  
The level-set grid for liquids can be visualized by colourizing the grid
with respect to the data residing within it.  
The implementation of visualization of liquid simulation grids should
mostly resemble that of visualization of gas simulation grids in
Blender.

### Project Schedule

#### May:

  - Community bonding, imbibing mentor’s insights, getting familiar with
    the codebase.

#### June:

  - Make a UI panel under liquid domain to interact with the desired
    options for liquid simulation visualization in viewport.
  - Analyse the implementation of gas simulation grids visualization
    options.

#### July:

  - Implement dynamic grid selection and detection for liquid
    simulation.
  - Implement display of velocity vectors on a per cell basis.
  - Implement display of external force vectors.

#### August:

  - Clean the code and handle bugs if any.
  - Add options to colorize the grid according to the strengths of the
    vectors.
  - Create end-user documentation.

#### If time permits:

  - Visualizing grids between frames.
  - Implement visualization for density of liquid using color mapping.

I may have semester-end examinations in late July or early August. The
schedule of my examinations may vary due to ongoing lockdown in India
due to Coronavirus.

### Biography

My name is Sriharsha Kotcharlakot. Currently, I’m a junior undergraduate
student majoring in computer science & engineering from MLR Institute of
Technology, India. I have always been keenly interested in the working
of 3D computer graphics due to the gravity of its marvelous applications
such as simulations of complex real world phenomena, games, VFX and
animated movies.  
I have been using Blender for over 5 years. I’m an amateur competitive
programmer. I’m proficient with C, C++ and Python. I have experience in
[game development](https://sriharsha2000.github.io/RoughDrift/) and app
development. I made a few [blender
tutorials](https://www.youtube.com/playlist?list=PLIWe6J-BVVA6SkHL_ELmO4bmzGUGSmVMC)
demonstrating some basic uses of physics tools like fluid simulation,
cloth simulation and rigidbody physics in Blender.
