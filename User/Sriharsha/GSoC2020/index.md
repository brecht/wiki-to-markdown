**Google Summer of Code 2020**

# Liquid Simulation Display Options

  - **[Proposal](Proposal.md)**
  - **[Weekly
    Reports](https://devtalk.blender.org/t/gsoc-2020-liquid-simulation-display-options-weekly-reports/13524)**
  - **[Final Report](Final_Report.md)**
