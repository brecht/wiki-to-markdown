\_\_NOTOC\_\_

# Developer Documentation

Here you can find information about Blender's development process,
design and architecture, as well as instructions for Building Blender.

<div class="card-deck">

<div class="card bg-light mb-3">

<div class="card-body">

<h4 class="card-title">

[New Developer Introduction](../../Developer_Intro/index.md)

</h4>

<h6 class="card-subtitle mb-2 text-muted">

Welcome\! Advice on how to get started.

</h6>

</div>

</div>

<div class="card bg-light mb-3">

<div class="card-body">

<h4 class="card-title">

[Communication](Contact)

</h4>

<h6 class="card-subtitle mb-2 text-muted">

The most important thing.

</h6>

</div>

</div>

<div class="card bg-light mb-3">

<div class="card-body">

<h4 class="card-title">

[Code Documentation](../../Source/index.md)

</h4>

<h6 class="card-subtitle mb-2 text-muted">

Technical documentation about the code.

</h6>

</div>

</div>

</div>

<div class="card-deck">

<div class="card bg-light mb-3">

<div class="card-body">

<h4 class="card-title">

[Building Blender](../../Building_Blender/index.md)

</h4>

<h6 class="card-subtitle mb-2 text-muted">

Instructions for compiling Blender locally.

</h6>

</div>

</div>

<div class="card bg-light mb-3">

<div class="card-body">

<h4 class="card-title">

[Module Owners](Process/Module_Owners)

</h4>

<h6 class="card-subtitle mb-2 text-muted">

Blender Components and their maintainers

</h6>

</div>

</div>

<div class="card bg-light mb-3">

<div class="card-body">

<h4 class="card-title">

[Style Guide](../../Style_Guide/index.md)

</h4>

<h6 class="card-subtitle mb-2 text-muted">

Coding Guidelines and Committer Etiquette.

</h6>

</div>

</div>

</div>

<div class="card-deck">

<div class="card bg-light mb-3">

<div class="card-body">

<h4 class="card-title">

[Tools](../../Tools/index.md)

</h4>

<h6 class="card-subtitle mb-2 text-muted">

Setup your development environment.

</h6>

</div>

</div>

<div class="card bg-light mb-3">

<div class="card-body">

<h4 class="card-title">

[Process](../../Process/index.md)

</h4>

<h6 class="card-subtitle mb-2 text-muted">

Release cycle, BugTracker, Code Reviews and Testing.

</h6>

</div>

</div>

<div class="card bg-light mb-3">

<div class="card-body">

<h4 class="card-title">

[Release Notes](../../Release_Notes/index.md)

</h4>

<h6 class="card-subtitle mb-2 text-muted">

What changed on each Blender version.

</h6>

</div>

</div>

</div>

<div class="card-deck">

<div class="card bg-light mb-3">

<div class="card-body">

<h4 class="card-title">

[Google Summer of Code](../../GSoC/index.md)

</h4>

<h6 class="card-subtitle mb-2 text-muted">

A program that introduces students to open source software development.

</h6>

</div>

</div>

<div class="card bg-light mb-3">

<div class="card-body">

<h4 class="card-title">

[Python](../../Python.md)

</h4>

<h6 class="card-subtitle mb-2 text-muted">

Learn about scripting and Add-ons.

</h6>

</div>

</div>

<div class="card bg-light mb-3">

<div class="card-body">

<h4 class="card-title">

[Translation](../../Translation.md)

</h4>

<h6 class="card-subtitle mb-2 text-muted">

Blender UI internationalization

</h6>

</div>

</div>

</div>

<div class="card-deck">

<div class="card bg-light mb-3">

<div class="card-body">

<h4 class="card-title">

[Infrastructure](../../Infrastructure/index.md)

</h4>

<h6 class="card-subtitle mb-2 text-muted">

Details about the online ecosystem that supports Blender development.

</h6>

</div>

</div>

<div class="card bg-light mb-3">

<div class="card-body">

<h4 class="card-title">

[FAQ](../../FAQ.md)

</h4>

<h6 class="card-subtitle mb-2 text-muted">

Common questions about the development process.

</h6>

</div>

</div>

</div>

## [Building Blender](../../Building_Blender/index.md)

#### [Linux](../../Building_Blender/Linux/index.md)

[:Special:Prefixindex/Building
Blender/Linux/](:Special:Prefixindex/Building_Blender/Linux/)

#### [macOS](../../Building_Blender/Mac.md)

#### [Windows](../../Building_Blender/Windows.md)

## [Developer Introduction](../../Developer_Intro/index.md)

<div class="mw-prefixindex-body">

  - [Overview](../../Developer_Intro/Overview.md)
  - [General Advice](../../Developer_Intro/Advice.md)
  - [Development Environment](../../Developer_Intro/Environment/index.md)
  - [Info for Committers](../../Developer_Intro/Committer.md)
  - [Contact Developers](Contact)

</div>

## [Source Code & Architecture](../../Source/index.md)

<div class="mw-prefixindex-body">

  - [Code Style](Source/Code_Style)
  - [Cycles](../../Source/Render/Cycles/index.md)
  - [Depsgraph](../../Source/Depsgraph.md)
  - [File Structure](../../Source/File_Structure.md)
  - [Internationalization](../../Source/Interface/Internationalization.md)

</div>

## [Tools](../../Tools/index.md)

[:Special:Prefixindex/Tools/](:Special:Prefixindex/Tools/)

## [Process](../../Process/index.md)

[:Special:Prefixindex/Process/](:Special:Prefixindex/Process/)

## [Reference](../../Reference/index.md)

<div class="mw-prefixindex-body">

  - [FAQ](../../FAQ.md)
  - [Google Summer of Code](../../GSoC/index.md)
  - [Project Policy](../../Reference/ProjectPolicy.md)
  - [Release Notes](../../Release_Notes/index.md)

</div>
