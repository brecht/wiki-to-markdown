## Alembic Procedural

To align with more production render engines, Cycles now support reading
data from Alembic directly, using an Alembic procedural. This procedural
will create objects in the Cycles scene from an Alembic archive,
skipping the export process from Blender. When using the procedural,
data from the Alembic archive is not read in Blender.

During viewport renders, the procedural can load the data from the
entire animation in memory to accelerate data lookups and updates on
frame change. It is not yet available for final renders.

Caching the animation allows for fast playback in the viewport,
potentially real-time with the limiting factors being the compute power,
scene complexity, and features used (subdivided meshes, and dynamic
meshes coming from physics simulations will increase the cost of
updating the scene).

Since data is loaded directly from Cycles instead of going through
Blender, we are able to overcome some of Blender's limitations. For
example, Blender does not generate subdivision from Alembic SubD meshes,
it merely builds a mesh from the data and any subdivision control is
lost. Arbitrary attributes can also be trivially read.

To enable the Cycles procedural, first switch on the experimental
feature set for Cycles, and enable the procedural in the Mesh Sequence
Cache by toggling "Use Render Engine Procedural". This setting is global
for the CacheFile, so every object in the Scene imported from the
CacheFile will use the Cycles procedural for rendering. Those objects
will be displayed using bounding boxes in the viewport.

If the Alembic object pointed to by the path is a subdivision object,
Cycles will only perform the subdivision if there is a Subdivision
Surface modifier after the Mesh Sequence Cache modifier.

### Features

  - Support polygonal and subdivision meshes
  - Limited support for Curve objects
  - Instancing
  - Arbitrary attributes (along with UVs, vertex colors)
  - Basic cache controls (it is possible to load the entire animation
    with some memory limit, if the memory limit is reached, rendering is
    aborted)
  - Layers : extra files to use for property override

### Known Limitations

  - Object level attributes are not supported
  - Motion blur (transformation or vector based) is not supported
  - N.U.R.B.S. and Point clouds cannot be rendered at the moment (Cycles
    cannot render point clouds at the moment)
  - Curves are only expected to be acyclic
  - Attributes are not read for Curves
  - Cameras are ignored
  - No support for file sequences
  - No support for detecting invisible objects

### Possible future settings

This is a list of possible features for the Cycles Alembic Procedural
and the Blender Alembic Importer. This list excludes the limitations
above, as it would be redundant to mention them again.

  - FaceSet name scope, to decide what face sets should be considered
    for shader assignments
  - Material attribute, to decide which attribute instead of face set
    should be considered for shader assignments
  - Exclude transform, to not concatenate child transformations matrices
    to their parent's one
  - Ignore velocity, to ignore computing motion blur data even if a
    velocity attribute has been detected
  - Velocity attribute, to specify which attribute to use for computing
    motion blur data
  - Velocity unit, to specify the unit of time used to differentiate
    velocity vectors
  - Radius attribute, to specify which attribute to use for setting
    curves or points radiuses
  - Expand hidden, to ignore the visibility attribute in the Alembic
    archive
  - Frame step
  - Replay speed, to control the speed of the playback (scales the FPS)
  - Animation boundary conditions : to specify what should happen before
    or after the animation begins or ends
      - none : nothing is displayed, objects will appear suddenly when
        the animation starts, and disappear when it ends
      - freeze : simply hold the first or last frame
      - loop : repeat the animation from the beginning
      - ping pong : plays back and forth the animation (first foward,
        then backward, then forward again, etc.)
  - Make instances, detect geometries with the same topology and make
    instances out of them, even if they are not set as instances in the
    archive
