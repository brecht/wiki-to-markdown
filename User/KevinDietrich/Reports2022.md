# Weekly Reports 2021

## Week 1 3 - 7 January 2022

This week was mostly spent doing bug triaging and fixing, mostly
following the GPU subdivision merge. There are still some issues to fix
regarding performances regressions. I also updated my work on the
subdivision settings as part of the mesh datablock
[\#68891](http://developer.blender.org/T68891).

  - Features:
      - Cycles: support rendering PointCloud motion blur from attribute
        ([rB4c3f52e7](https://projects.blender.org/blender/blender/commit/4c3f52e7dc86))

<!-- end list -->

  - Fixes:
      - Fix [\#94713](http://developer.blender.org/T94713): Alembic
        crash with empty frames and velocities
        ([rB45bc4e32](https://projects.blender.org/blender/blender/commit/45bc4e320970))
      - Fix [\#94672](http://developer.blender.org/T94672): incorrect
        Workbench shadows with GPU subdivision
        ([rB0c6b29ee](https://projects.blender.org/blender/blender/commit/0c6b29ee4378))
      - GPU subdiv: fix wrong data sizes used for lines adjacency IBO
        ([rBc2089ae5](https://projects.blender.org/blender/blender/commit/c2089ae53ca3))
      - Fix [\#94674](http://developer.blender.org/T94674): crash
        reading ORCOs from an Alembic animation.
        ([rB88e15ff1](https://projects.blender.org/blender/blender/commit/88e15ff1e686))

<!-- end list -->

  - Active patches:
      - [D13603](http://developer.blender.org/D13603): Alembic: add
        support for reading override layers.
      - [D13730](http://developer.blender.org/D13730): Fix
        [\#93179](http://developer.blender.org/T93179): geonodes UVs and
        Vertex colors do not work in EEVEE.
      - [D10145](http://developer.blender.org/D10145): Subdivision: add
        support for vertex creasing.

## Week 2 10 - 14 January 2022

This week was also mostly spent doing bug triaging and fixes for the GPU
subdivision code. Some bugs are a bit tricky to solve, and will continue
next week. One tough bug, which is present in multiple reports already,
is caused by the fact that the GPU subdivision code does not support
meshes will loose geometry only (such as meshes used for custom bone
shape). The code presumes that we will always at least one polygon.
There seem to be more cases of AMD only bugs, I ordered an AMD card so
that I can try to fix those. I also did some maintenance in the Alembic
code.

  - Features:
      - Alembic: add support for reading override layers
        ([rB45bc4e32](https://projects.blender.org/blender/blender/commit/45bc4e320970))

<!-- end list -->

  - Bug fixes:
      - Fix [\#94865](http://developer.blender.org/T94865): GPU subdiv
        crash switching to texpaint area
        ([rBbc66cd98](https://projects.blender.org/blender/blender/commit/bc66cd986874))
      - Fix [\#94918](http://developer.blender.org/T94918): GPU
        subdivision uses viewport levels for final render
        ([rB9664cc91](https://projects.blender.org/blender/blender/commit/9664cc91f338))
      - Fix [\#94671](http://developer.blender.org/T94671): performance
        regression with subsurf modifier
        ([rBf134341e](https://projects.blender.org/blender/blender/commit/f134341e03b5))

<!-- end list -->

  - Active patches
      - [D13730](http://developer.blender.org/D13730): Fix
        [\#93179](http://developer.blender.org/T93179): geonodes UVs and
        Vertex colors do not work in EEVEE.
      - [D10145](http://developer.blender.org/D10145): Subdivision: add
        support for vertex creasing.

## Weeks 3-6 17 January - 11 Februart 2022

TODO: forgot to them, need to gather data.

## Week 7 14 - 18 February 2022

This week was spent doing bug fixing for the GPU subdivision
integration, and starting implementing the edit mode for the new Curves
object.

  - Features
      - Curves: add initial edit mode support
        ([rB5f16e24c](https://projects.blender.org/blender/blender/commit/5f16e24cc9ac))

<!-- end list -->

  - Code reviews
      - [D14117](http://developer.blender.org/D14117): Curves: Initial
        brush system integration for curves sculpt mode.

<!-- end list -->

  - Bug Fixes
      - Fix [\#94881](http://developer.blender.org/T94881): GPU
        subdivision fails with high polycount coarse meshes
        ([rBc5dcfb63](https://projects.blender.org/blender/blender/commit/c5dcfb63d9e4))
      - Fix [\#95827](http://developer.blender.org/T95827): vertex
        groups do not display correctly with GPU subdivision
        ([rBbe3047c5](https://projects.blender.org/blender/blender/commit/be3047c500be))
      - Fix [\#95806](http://developer.blender.org/T95806): subdivision
        missing in Cycles when using autosmooth
        ([rB53fe4f62](https://projects.blender.org/blender/blender/commit/53fe4f62feae))
      - (unreported) GPU subdiv: fix custom data interpolation for
        N-gons
        ([rB430ced76](https://projects.blender.org/blender/blender/commit/430ced76d559))
      - Fix [\#94479](http://developer.blender.org/T94479): GPU
        Subdivision surface modifier does not apply to Cycles renders.
        ([rB56407432](https://projects.blender.org/blender/blender/commit/56407432a6aa))
      - Fix [\#95320](http://developer.blender.org/T95320): CacheFile
        templates crash when used through Python
        ([rB0999a01b](https://projects.blender.org/blender/blender/commit/0999a01b03d4))
      - Fix [\#95177](http://developer.blender.org/T95177): GPU subdiv
        crashes mirror modifier in edit-mode
        ([rB993839ce](https://projects.blender.org/blender/blender/commit/993839ce8513))
      - Fix [\#95697](http://developer.blender.org/T95697): GPU
        subdivision ignores custom normals
        ([rB48b26d9c](https://projects.blender.org/blender/blender/commit/48b26d9c2e0f))

<!-- end list -->

  - Active Patches
      - [D14038](http://developer.blender.org/D14038): Geometry
        Component: add access to vertex creases.

## Week 8 21 - 25 February 2022

This week was mostly spent doing bug fixing. A bit of time was spent
continuing work on the Curves edit mode overlay drawing.

  - Notable commits
      - [rBc8b4e0c0](https://projects.blender.org/blender/blender/commit/c8b4e0c0b5f8):
        Disable GPU subdivision if autosmooth or split normals are used

<!-- end list -->

  - Bug Fixes
      - Fix [\#93123](http://developer.blender.org/T93123): viewport
        lags with custom attributes
        ([rBa911f075](https://projects.blender.org/blender/blender/commit/a911f075d7aa))
      - Fix [\#95242](http://developer.blender.org/T95242),
        [\#94919](http://developer.blender.org/T94919): Fix different
        shading between CPU and GPU subdivision
        ([rB118a219e](https://projects.blender.org/blender/blender/commit/118a219e9dce))
      - Fix [\#95976](http://developer.blender.org/T95976): on cage GPU
        subdivision breaks X-ray selection
        ([rBb7a95421](https://projects.blender.org/blender/blender/commit/b7a954218b0d))
      - Fix [\#95959](http://developer.blender.org/T95959): crash when
        exporting subdivision to Alembic.
        ([rBba274d20](https://projects.blender.org/blender/blender/commit/ba274d20b4f1))

<!-- end list -->

  - Open Patches
      - [D14199](http://developer.blender.org/D14199): Subdivision node:
        add input for vertex creases.
      - [D14171](http://developer.blender.org/D14171): Fix
        [\#94729](http://developer.blender.org/T94729): GPU subdivision
        does not support meshes without polygons.

## Week 9 - 28 February - 4 March

  - New or active patches
      - [D14199](http://developer.blender.org/D14199): Subdivision node:
        add input for vertex creases

<!-- end list -->

  - Bug Fixes
      - Fix [\#94729](http://developer.blender.org/T94729): GPU
        subdivision does not support meshes without polygons.
        ([6883c47bb5](https://projects.blender.org/blender/blender/commit/6883c47bb593))
      - Fix [\#94952](http://developer.blender.org/T94952): normals maps
        don't render correctly with GPU subdivision.
        ([4932269ec3](https://projects.blender.org/blender/blender/commit/4932269ec3fa))

## Week 10 - 7 - 11 March

Work on curves edit mode, GPU subdivision fixes, and bug triaging.

  - Bug fixes
      - Fix [\#96224](http://developer.blender.org/T96224): GPU subdiv
        crash with smooth normals and tangents
        ([d5e4cab76c](https://projects.blender.org/blender/blender/commit/d5e4cab76c62))

<!-- end list -->

  - Commits/maintenance
      - GPU: disable compute shader for problematic drivers
        ([3a672fe6fb](https://projects.blender.org/blender/blender/commit/3a672fe6fbdc))
      - Fix wrong edge crease validity check in the Cycles Alembic
        procedural
        ([64a5fd7a1d](https://projects.blender.org/blender/blender/commit/64a5fd7a1dce))

<!-- end list -->

  - New or active patches
      - [D14262](http://developer.blender.org/D14262): Curves edit mode:
        show dots for points.

## Week 11 - 14 - 18 March

Work on curves edit mode, GPU subdivision fixes, and bug triaging.

  - Commits/maintenance
      - Fix wrong documentation/UI text for NodeLink
        ([89ab3cdc56](https://projects.blender.org/blender/blender/commit/89ab3cdc56b6))
      - Cleanup: unused class members.
        ([8407c0b64e](https://projects.blender.org/blender/blender/commit/8407c0b64eb5))
      - Fix typo in smooth brush tooltip
        ([a67c7693a7](https://projects.blender.org/blender/blender/commit/a67c7693a76b))
      - Disable GPU subdivision if the maximum number of SSBO binding is
        reached
        ([3bb4597b2d](https://projects.blender.org/blender/blender/commit/3bb4597b2d23))
      - GPU capabilities: detect max SSBO bindings
        ([45b637e1e7](https://projects.blender.org/blender/blender/commit/45b637e1e79f))

<!-- end list -->

  - New or active patches
      - [D14335](http://developer.blender.org/D14335): Curves edit mode:
        support point selection.
      - [D14262](http://developer.blender.org/D14262): Curves edit mode:
        show dots for points.
      - [D14330](http://developer.blender.org/D14330): Curves: add an
        EditCurves structure.
      - [D14336](http://developer.blender.org/D14336): ViewLayer AOVs:
        add "remove" and "clear" to Python API.

## Week 12 - 21 - 25 March

Work on curves edit mode locally, GPU subdivision fixes, bug triaging,
some maintenance and finishing up the Alembic arbitrary attribute import
patch.

  - Bug Fixes
      - Fix [\#96372](http://developer.blender.org/T96372): UV editor
        selection display wrong with GPU subdivision.
        ([612ec0ecdf](https://projects.blender.org/blender/blender/commit/612ec0ecdf04))

<!-- end list -->

  - Maintenance
      - Cleanup: extra property update for ShaderFxGlow.opacity
        ([ead9ed7e16](https://projects.blender.org/blender/blender/commit/ead9ed7e16e3))
      - Cleanup/fix wrong enum items for object.gpencil\_modifier\_add
        ([ead2c71a90](https://projects.blender.org/blender/blender/commit/ead2c71a90e0))
      - Fix compile error on GCC
        ([d84b4becd3](https://projects.blender.org/blender/blender/commit/d84b4becd3bc))
      - Cleanup: use nullptr
        ([03a7747aa6](https://projects.blender.org/blender/blender/commit/03a7747aa6e6))
      - Cleanup: add const qualifier
        ([945dfd200b](https://projects.blender.org/blender/blender/commit/945dfd200b1f))
      - Cleanup: move Mesh Sequence Cache modifier to C++
        ([d40d5e8d0f](https://projects.blender.org/blender/blender/commit/d40d5e8d0f56))

## Week 13 - 28 March - April 1st

Further work on GPU subdivision bug fixes and regressions, as usual.

Refactored the Alembic attribute import patch based on code review.
Geometry set support for Alembic and USD was also sent to code review.

Worked on edit curves:

  - refactor detection of edit mode
  - reworked the base patch for selection, still needs to add the
    StaticAnonymousAttribute as suggested by Hans.

While working on finishing up the Alembic/USD geometry set support,
dusted off an old patch to use PointCloud as input type for gas
simulations. This patch was made a few months ago as some sort of proof
of concept, as PointCloud could now come from other software, which
include simulation caches. This requires to move files to C++, and one
case seems to be tricky: storing previous vertices. This cannot use a
\`blender::Vector\` as it is declared in DNA, it might need to add a
runtime structure which is only known to the C++ code. It would be nice
to also directly use the velocity attribute if present instead of
computing one.

  - Commits:
      - Cycles: enable Alembic procedural for final renders
        ([e81d7bfcc9](https://projects.blender.org/blender/blender/commit/e81d7bfcc9f4))

<!-- end list -->

  - Bug Fixes:
      - Fix [\#96915](http://developer.blender.org/T96915): GPU
        subdivision modifier: Smooth Shade doesn't work
        ([dc73c71b3b](https://projects.blender.org/blender/blender/commit/dc73c71b3b9b))
      - Fix [\#96344](http://developer.blender.org/T96344): edit mode
        GPU subdivision crashes with X-ray mode and modifiers
        ([ba28c10199](https://projects.blender.org/blender/blender/commit/ba28c10199b3))
      - Fix [\#96356](http://developer.blender.org/T96356): artefacts
        with GPU subdivision and vertex paint mask.
        ([2aa49107a2](https://projects.blender.org/blender/blender/commit/2aa49107a2fb))
      - Fix [\#76746](http://developer.blender.org/T76746): Alembic,
        wrong result importing back exported
        ([4c1393c202](https://projects.blender.org/blender/blender/commit/4c1393c202d4))
      - Fix [\#96915](http://developer.blender.org/T96915): GPU
        subdivision modifier: Smooth Shade doesn't work.
        ([dc73c71b3b](https://projects.blender.org/blender/blender/commit/dc73c71b3b9b))

<!-- end list -->

  - New or Active Patches
      - [D14484](http://developer.blender.org/D14484): Cleanup: Alembic,
        use a structure to pass parameters
      - [D11591](http://developer.blender.org/D11591): Alembic: import
        arbitrary attributes
      - [D11592](http://developer.blender.org/D11592): Alembic/USD: use
        Geometry Sets to import data
      - [D13855](http://developer.blender.org/D13855): Cleanup:
        CacheFile, use double precision for time: Alembic, Pipeline,
        Assets & I/O.
      - [D11156](http://developer.blender.org/D11156): Alembic: operator
        to set the Scene frame range from the Archive time information
      - [D12411](http://developer.blender.org/D12411): Cleanup: move ABC
        sequence length computation to Cachefile.
      - [D14491](http://developer.blender.org/D14491): Fix
        [\#96338](http://developer.blender.org/T96338): crash with GPU
        subdivision switching to UV editing tab.
      - [D14489](http://developer.blender.org/D14489): Fix
        [\#96327](http://developer.blender.org/T96327): data transfer
        crash with GPU subdivision.
      - [D14488](http://developer.blender.org/D14488): Fix
        [\#96283](http://developer.blender.org/T96283): last disabled
        subsurf is used for GPU subdivision.
      - [D14330](http://developer.blender.org/D14330): Curves: use a
        flag to detect edit mode
      - [D14262](http://developer.blender.org/D14262): Curves edit mode:
        show dots for points.

## Week 14 4 - 7 April 2022

Some bug triaging. Replied to various code reviews. Some of the patches
for curves edit mode were finally accepeted and merged after some
redesigns so work on more featurs here will resume. Looked into (without
a fix yet) [\#96829](http://developer.blender.org/T96829): Cycles:
modifying properties through the Python console does not trigger
updates. This seems to be a general problem for addons, not just Cycles.

  - Fixes (unreported)
      - Curves: fix edit mode detection
        ([fc8bcd26c0](https://projects.blender.org/blender/blender/commit/fc8bcd26c0be))
      - Curves: fix missing updates switching to sculpt mode
        ([a3e122b9ae](https://projects.blender.org/blender/blender/commit/a3e122b9aec5))
      - Alembic: fix clamping of frame offset during exports
        ([c0a9ec222f](https://projects.blender.org/blender/blender/commit/c0a9ec222f3e))

<!-- end list -->

  - Code reviews
      - Quick review of
        [:Template:PhaDiff](:Template:PhaDiff): Shader nodes:
        Cubemap projection extension for the environment texture node.
        This was mostly a code style review, the actual feature is much
        more complicated to implement than the patch.

<!-- end list -->

  - Active/updated patches
      - [D11156](http://developer.blender.org/D11156): Alembic: operator
        to set the Scene frame range from the Archive time information.
      - [D11592](http://developer.blender.org/D11592): Alembic/USD: use
        geometry sets to import data
      - [D14605](http://developer.blender.org/D14605): BMesh Python API:
        add access to vertex crease layer

## Week 15 11 - 15 April 2022

Some bug triaging again, and updates for ongoing code reviews. Spent a
bit of time finishing a patch to support volume motion in Cycles, which
still needs revision. Also spent some time updating Tangent's work on
modernizing volume rendering. Curves edit mode work was not resumed.

For a personal project, I worked on adding support to create
CurveMapping throught the Python API (via \`bpy.props.CurveMapping\`).
It would be nice to also add support for CurveProfile and ColorRamp, as
those are highly requested by the addon developers. This might remain a
project done outside of Fondation time, but may be done during at least
for code review. There was also some work on improving the node API,
which will also at some point cross with Blender Fondation time.

  - Active patches
      - [D11592](http://developer.blender.org/D11592): Alembic/USD: use
        geometry sets to import data
      - [D11591](http://developer.blender.org/D11591): Alembic: import
        arbitraty attributes. (there were some unnoticed regressions
        which probably prevented its acceptance...)
      - [D14629](http://developer.blender.org/D14629): Cycles: add
        support for volume motion blur
      - [D14656](http://developer.blender.org/D14656): Cycles: ratio
        tracking for volume rendering. This is a draft, and work on it
        happen based on available time, most likely very low priority.

<!-- end list -->

  - Commits
      - BMesh Python API: add access to vertex crease layer
        ([b2143da253](https://projects.blender.org/blender/blender/commit/b2143da253ed))

<!-- end list -->

  - Bug Fixes
      - Fix [\#95700](http://developer.blender.org/T95700): Oject Info
        node does not work with GPU subdivided meshes
        ([a9b94e5f81](https://projects.blender.org/blender/blender/commit/a9b94e5f81ce))
      - Fix [\#96563](http://developer.blender.org/T96563): tangents
        artifacts with GPU subdivision
        ([f84f9eb8ed](https://projects.blender.org/blender/blender/commit/f84f9eb8ed43))
      - Fix part of [\#96596](http://developer.blender.org/T96596):
        wrong generated coordinates with GPU subdivision
        ([3e25561d51](https://projects.blender.org/blender/blender/commit/3e25561d5184))
      - Fix [\#97053](http://developer.blender.org/T97053): on cage GPU
        subdivision shows subdivided edges as actual edges
        ([3f305b9ab3](https://projects.blender.org/blender/blender/commit/3f305b9ab3cb))

## Week 16 18 - 22 April 2022

As the report was not done readily, I don't remember a lot. Although, I
really needed to upgrade my workstation, as I kept postponing it,
especially since some tools like Clang Format were outdated on my
platform (and thus producing wrong outputs that I needed to manually
fix), this went bad and cost me a couple of days to reset.

Commits:

  - Cycles: add support for volume motion blur
    ([2890c11cd7](https://projects.blender.org/blender/blender/commit/2890c11cd7b0))

Updated Patches:

  - [D11591](http://developer.blender.org/D11591): Alembic: import
    arbitrary attributes.

## Week 17 25 - 29 April 2022

Some bug triaging. Investigated
[\#96338](http://developer.blender.org/T96338). It took a while to
figure out the cause of the crash, but could not find a proper fix for
it. Essentially the UV stretch angles buffer is requested but not the
position buffer, which is needed in the subdivision case and leads to a
crash. Trying to generate the buffer using the coarse with interpolation
on the GPU did not work. Probably a simpler fix is to temporarily
compute the positions in the function.

Tried to implement attribute rendering in EEVEE for the new curves
objects based on some of the refactors I did for the curves edit mode,
without any luck so far. I am not sure if deeper refactors are
necessary, or if there is a simpler solution.

  - Patches
      - [D14773](http://developer.blender.org/D14773): Fix
        [\#96836](http://developer.blender.org/T96836): UV points
        missing with some modifiers
      - [D14199](http://developer.blender.org/D14199): Subdivision node:
        add input for vertex creases.
      - [D14775](http://developer.blender.org/D14775): BPY types: add
        default Geometry Node poll function. (fix for
        GeometryNodeCustomGroup registration)

<!-- end list -->

  - Bug fixes
      - Fix [\#96434](http://developer.blender.org/T96434): bad
        performance with viewport statistics and GPU
        ([163f6a17e4](https://projects.blender.org/blender/blender/commit/163f6a17e4fe))
      - Fix [\#96327](http://developer.blender.org/T96327): data
        transfer crash with GPU subdivision
        ([08731d70bf](https://projects.blender.org/blender/blender/commit/08731d70bf66))
      - Fix [\#96283](http://developer.blender.org/T96283): last
        disabled subsurf is used for GPU subdivision
        ([68ca12a7fc](https://projects.blender.org/blender/blender/commit/68ca12a7fc0e))

## Week 18 2 - 6 May 2022

Some bug triaging and fixing. Partial work on curves attributes
rendering in EEVEE.

  - Bug Fixes
      - Fix [\#96845](http://developer.blender.org/T96845): artifacts
        with GPU subdivision and mirror modifier
        ([fc0f6d19ad](https://projects.blender.org/blender/blender/commit/fc0f6d19adae))
      - Fix [\#97827](http://developer.blender.org/T97827): material
        preview not displaying textures
        ([d86d7c935e](https://projects.blender.org/blender/blender/commit/d86d7c935ef3))
      - Fix [\#93179](http://developer.blender.org/T93179): geonodes UVs
        and Vertex colors do not work in EEVEE
        ([281bcc1c1d](https://projects.blender.org/blender/blender/commit/281bcc1c1dd6))
      - Fix [\#96338](http://developer.blender.org/T96338): GPU subdiv
        crash switching to UV editing
        ([947f8ba300](https://projects.blender.org/blender/blender/commit/947f8ba30009))

<!-- end list -->

  - Features
      - Subdivision node: add input for vertex creases
        ([1a98bec40e](https://projects.blender.org/blender/blender/commit/1a98bec40ec2))
      - BPY types: add default Geometry Node poll function
        ([2062116924](https://projects.blender.org/blender/blender/commit/20621169244d))

## Week 19 9 - 13 May 2022

A fair amount of time was spent reworking (and simplifying) curves
attribute rendering support in EEVEE, following a discussion with the
module members. Got a working implementation for curves attribute
rendering in EEVEE, although I forgot to implement point attribute
subdivision before sending to code review, that will need to be fixed,
but luckily is/should be straightforward as it can reuse the
interpolation routines used for the positions.

Investigated some of the remaining GPU subdivision bugs. Those are the
complicated ones, but could not really understand/try to fix them. Some
smaller bugs do exist and will be fixed next week.

  - Bug Fixes
      - Fix [\#97330](http://developer.blender.org/T97330): UV points
        missing with some modifiers
        ([fa9e878e79](https://projects.blender.org/blender/blender/commit/fa9e878e7935))

## Week 20 16 - 20 May 2022

Finalized implementation of curves attribute rendering with proper
support for point attribute refinement. Also some updates to patches
currently in code review (merged conflicts, compile errors, etc.).

  - Commits:
      - GPU subdiv: smoothly interpolate orco layer
        ([9d9f2f1a03](https://projects.blender.org/blender/blender/commit/9d9f2f1a0356))

<!-- end list -->

  - Active or updated patches:
      - [D11591](http://developer.blender.org/D11591): Alembic: import
        arbitrary attributes.
      - [D11592](http://developer.blender.org/D11592): Alembic/USD: use
        geometry sets to import data.
      - [D14916](http://developer.blender.org/D14916): EEVEE/Workbench:
        support Curves attributes rendering.
      - [D14335](http://developer.blender.org/D14335): Curves edit mode:
        support point selection.

<!-- end list -->

  - Code reviews:
      - [D14969](http://developer.blender.org/D14969): Fix
        [\#98052](http://developer.blender.org/T98052): Eevee /
        Workbench background render crash with GPU subdivision.
      - [D14958](http://developer.blender.org/D14958): Cleanup:
        Deduplicate Alembic procedural bounding box mesh creation.

## Week 21 23 - 27 May 2022

Finalized curves attribute rendering based on last code reviews, and
merged the code. Fixed a few GPU subdivision bugs. There are two big
ones that remain but seem a bit tricky.

[\#97086](http://developer.blender.org/T97086): corrupted UVs display
with multiple objects in edit mode. Since it works fine in debug builds,
but not release builds, it looks like this is a synchronization issue,
where we read data before the compute shaders have done executing. It
could be that in debug builds, as execution is a bit slower, the data
arrives in time. As far as I can tell, the coarse UV data is properly
setup inside of the OpenSubDiv evaluator, but after running the compute
the refined UVs have no data or unitialized data. This does not happen
for the first mesh though, only the others. I tried scattering memory
barriers and calls to \`glFinish\` but to no avail. Either it is a weird
synchronization problem, or there is something else.

[\#97877](http://developer.blender.org/T97877): Broken shadow in solid
mode with GPU Subdiv. At first it looked like some edge case with the
shadowing technique, but in the end it looks like that there several
levels of floating precision issues due to compression of UV coordinates
used for patch interpolation happening both in Blender and in OpenSubDiv
which result in the mesh not being exactly watertight. In Blender, UVs
are stored in 16-bits to reduce data transfer. Removing this improves
the situation but does not fix it. OpenSubDiv stores the UVs for the
first corner of each patch as 10-bit integers, which is then used to
offset the patch UVs that we use for evaluation. It could be that this
may lead to further precision issues. Since the OpenSubDiv code for CPU
and GPU are slightly different here, it could be that the GPU is less
robust to this kind of floating point compression. I would need to port
over the GLSL code from OpenSubDiv and modify it to try and see if it
fixes the issue.

  - Bug Fixes:
      - Fix [\#98385](http://developer.blender.org/T98385): Color
        attributes not working with GPU subdivision
        ([bf53956914](https://projects.blender.org/blender/blender/commit/bf5395691473))
      - Fix [\#98392](http://developer.blender.org/T98392): GPU
        subdivision crash with knife tools
        ([55e3930b25](https://projects.blender.org/blender/blender/commit/55e3930b253e))
      - Fix [\#96080](http://developer.blender.org/T96080): hiding
        elements does not work with GPU subdiv
        ([98b66dc040](https://projects.blender.org/blender/blender/commit/98b66dc04026))

<!-- end list -->

  - Commits
      - EEVEE: support Curves attributes rendering
        ([cd968a3273](https://projects.blender.org/blender/blender/commit/cd968a327395))

## Week 22 30 may - 3 June 2022

Fixed the last remaining high priority bugs for GPU subdivision.

  - Bug Fixes
      - Fix [\#98526](http://developer.blender.org/T98526): broken
        corner attributes with GPU subdivision
        ([54d076b20d](https://projects.blender.org/blender/blender/commit/54d076b20d39))
      - Fix [\#98571](http://developer.blender.org/T98571): corrupted
        face selection drawing with GPU subdivision
        ([7f7ed8e098](https://projects.blender.org/blender/blender/commit/7f7ed8e0989d))
      - Fix [\#97877](http://developer.blender.org/T97877): broken
        shadows with GPU subdivision
        ([a5dcae0c64](https://projects.blender.org/blender/blender/commit/a5dcae0c6416))
      - Fix [\#97086](http://developer.blender.org/T97086): corrupted
        UVs with GPU subdivision.
        ([344a8fb3d4](https://projects.blender.org/blender/blender/commit/344a8fb3d4ec))

<!-- end list -->

  - Active Patches
      - [D11591](http://developer.blender.org/D11591): Alembic: import
        arbitrary attributes.

## Week 23 - 24 7 - 17 June 2022

Worked on fixing some bugs and shaky logic in the Alembic arbitrary
attributes patch. This patch introduces some complexity to the Alembic
importer in order to support files created by as many external sofware
as possible (it would be helpful if people would just abide by the
standard...). Creating test files to test the remapping logic was a bit
tedious though. This was based on another (not shared yet) patch to
implement exporting arbitrary attributes to Alembic and required to
modify the exported data types to only output floats instead of float3,
color, etc. This other patch has some issue where default custom data
layers like positions and normals are also exported (so they are
exported twice: once as geometric data, and once as attributes),
probably there needs to be some mechanism to detect them, if there is
none already.

For curves edit mode, one issue that was found is that the curves that
are drawn in edit mode are the final curves with modifiers applied,
which is wrong. I tried some simple things to go around that, but I feel
a proper refactor to separate final and edit data in the draw code has
to happen first. Since there is some active development happening in
this area for other reasons, it should be done in a way that avoid too
many merge conflicts for everyone involved.

As this is nearing the end of the contract, I also spent some time
factoring out some patches from personal branches that I think could be
useful to the community. Those patches relate to the node editor API,
and should help addon developer have more control, especially for custom
node groups. Python defined node groups can be powerful, but some
features are missing to really unleash their power. For example, switch
or mix nodes with infinite input sockets could be coded, which would
help avoid manually managing a hierarchy of switch/mix/etc. nodes, but
some events are missing from the node tree API to really implement that.

  - Bug Fixes
      - Fix [\#98866](http://developer.blender.org/T98866): GPU subdiv
        crash in edit mode with loose geometry
        [67f5596f19](https://projects.blender.org/blender/blender/commit/67f5596f194c)
      - Fix [\#98735](http://developer.blender.org/T98735): GPU subdiv
        displays normals for all elements
        [16f5d51109](https://projects.blender.org/blender/blender/commit/16f5d51109bc)

<!-- end list -->

  - Active Patches
      - [D11591](http://developer.blender.org/D11591): Alembic: import
        arbitrary attributes.

<!-- end list -->

  - Code reviews
      - [D15205](http://developer.blender.org/D15205): Helped Hans solve
        an issue, actual review will be done when the patch is ready

## Week 25 20 - 24 June 2022

Revised some of the Alembic patches, and investigated some potential
issues based on code review (the knots array will not be read anymore,
this might cause some discrepancy between applications, although there
are currently a lot of issues with curves in Blender so they are not
used a lot with Alembic, something that the new Curves is fixing, so I
would not worry a lot about potential issues here.)

Started to send OpenSubDiv fixes to Pixar, as some GPU subdivision bugs
come from issues in the library, the compute evaluator code was copied
to Blender, it would nice to send the fixes upstream.

  - Bug Fixes
      - Fix [\#98773](http://developer.blender.org/T98773): GPU
        Subdivision breaks auto selection in UV edit mode.
        [6c3965c027](https://projects.blender.org/blender/blender/commit/6c3965c0271c)
      - GPU subdiv: fix hidden faces in paint mode when hidden in edit
        mode
        [697363545f](https://projects.blender.org/blender/blender/commit/697363545f37)
      - Fix [\#99016](http://developer.blender.org/T99016): GPU subdiv
        artifacts in weight paint with smooth shading
        [d7fbc5708a](https://projects.blender.org/blender/blender/commit/d7fbc5708ae2)
      - Fix artefacts with GPU subdiv and weight paint face selection
        [72a5bb8ba9](https://projects.blender.org/blender/blender/commit/72a5bb8ba98a)
      - Fix [\#98913](http://developer.blender.org/T98913): GPU
        Subdivision: "Show Wire" overlay glitch
        [b73a52302e](https://projects.blender.org/blender/blender/commit/b73a52302edc)
