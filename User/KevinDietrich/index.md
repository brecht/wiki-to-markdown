## Who

My name is Kévin Dietrich, I was born in 1991 and I live in France. I
discovered Blender around 2009-2010 (v2.49, when v2.5 was still in
development) as I was interested in film making and visual effects. I am
mostly interested in physics simulations (mainly fluids) and volume
rendering. As I wanted to improve Blender a bit, I taught myself
programming and computer graphics, and became a Blender contributor.

## What

Some of my contributions include work on using OpenVDB for caching gas
simulations, Alembic import and export (based on a patch from Dwarf
Labs), as well as improvements to volume rendering in Cycles (empty
space optimization), as well as various little improvements here and
there.

-----

[:Special:Prefixindex](:Special:Prefixindex)
