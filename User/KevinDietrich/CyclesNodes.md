## Cycles Nodes

This page references the various nodes which can be used in Cycles.
There are two categories of Nodes, the "top level" ones, which define
the content of the Scene, and the "shader graph" ones, which describe a
Shader network.

### Top Level Nodes

Those are the nodes that make up a Cycles Scene.

#### Alembic

  - Sockets
      - Filename
          - filepath *string*
      - Frame
          - frame *float*
      - Start Frame
          - start\_frame *float*
      - End Frame
          - end\_frame *float*
      - Frame Rate
          - frame\_rate *float*
      - Frame Offset
          - frame\_offset *float*
      - Default Radius
          - default\_radius *float*
      - Scale
          - scale *float*
      - Objects
          - objects *array\_node*

#### Alembic Object

  - Sockets
      - Alembic Path
          - path *string*
      - Used Shaders
          - used\_shaders *array\_node*
      - Max Subdivision Level
          - subd\_max\_level *int*
      - Subdivision Dicing Rate
          - subd\_dicing\_rate *float*
      - Radius Scale
          - radius\_scale *float*

#### Background

  - Sockets
      - AO Factor
          - ao\_factor *float*
      - AO Distance
          - ao\_distance *float*
      - Use Shader
          - use\_shader *boolean*
      - Use AO
          - use\_ao *boolean*
      - Visibility
          - visibility *uint*
      - Transparent
          - transparent *boolean*
      - Transparent Glass
          - transparent\_glass *boolean*
      - Transparent Roughness Threshold
          - transparent\_roughness\_threshold *float*
      - Volume Step Size
          - volume\_step\_size *float*
      - Shader
          - shader *node*

#### Camera

  - Sockets
      - Shutter Time
          - shuttertime *float*
      - Motion Position
          - motion\_position *enum*
      - Rolling Shutter Type
          - rolling\_shutter\_type *enum*
      - Rolling Shutter Duration
          - rolling\_shutter\_duration *float*
      - Shutter Curve
          - shutter\_curve *array\_float*
      - Aperture Size
          - aperturesize *float*
      - Focal Distance
          - focaldistance *float*
      - Blades
          - blades *uint*
      - Blades Rotation
          - bladesrotation *float*
      - Matrix
          - matrix *transform*
      - Motion
          - motion *array\_transform*
      - Aperture Ratio
          - aperture\_ratio *float*
      - Type
          - camera\_type *enum*
      - Panorama Type
          - panorama\_type *enum*
      - Fisheye FOV
          - fisheye\_fov *float*
      - Fisheye Lens
          - fisheye\_lens *float*
      - Latitude Min
          - latitude\_min *float*
      - Latitude Max
          - latitude\_max *float*
      - Longitude Min
          - longitude\_min *float*
      - Longitude Max
          - longitude\_max *float*
      - FOV
          - fov *float*
      - FOV Pre
          - fov\_pre *float*
      - FOV Post
          - fov\_post *float*
      - Stereo Eye
          - stereo\_eye *enum*
      - Use Spherical Stereo
          - use\_spherical\_stereo *boolean*
      - Interocular Distance
          - interocular\_distance *float*
      - Convergence Distance
          - convergence\_distance *float*
      - Use Pole Merge
          - use\_pole\_merge *boolean*
      - Pole Merge Angle From
          - pole\_merge\_angle\_from *float*
      - Pole Merge Angle To
          - pole\_merge\_angle\_to *float*
      - Sensor Width
          - sensorwidth *float*
      - Sensor Height
          - sensorheight *float*
      - Near Clip
          - nearclip *float*
      - Far Clip
          - farclip *float*
      - Viewplane Left
          - viewplane.left *float*
      - Viewplane Right
          - viewplane.right *float*
      - Viewplane Bottom
          - viewplane.bottom *float*
      - Viewplane Top
          - viewplane.top *float*
      - Border Left
          - border.left *float*
      - Border Right
          - border.right *float*
      - Border Bottom
          - border.bottom *float*
      - Border Top
          - border.top *float*
      - Viewport Border Left
          - viewport\_camera\_border.left *float*
      - Viewport Border Right
          - viewport\_camera\_border.right *float*
      - Viewport Border Bottom
          - viewport\_camera\_border.bottom *float*
      - Viewport Border Top
          - viewport\_camera\_border.top *float*
      - Offscreen Dicing Scale
          - offscreen\_dicing\_scale *float*
      - Full Width
          - full\_width *int*
      - Full Height
          - full\_height *int*
      - Use Perspective Motion
          - use\_perspective\_motion *boolean*

#### Film

  - Sockets
      - Exposure
          - exposure *float*
      - Pass Alpha Threshold
          - pass\_alpha\_threshold *float*
      - Filter Type
          - filter\_type *enum*
      - Filter Width
          - filter\_width *float*
      - Mist Start
          - mist\_start *float*
      - Mist Depth
          - mist\_depth *float*
      - Mist Falloff
          - mist\_falloff *float*
      - Generate Denoising Data Pass
          - denoising\_data\_pass *boolean*
      - Generate Denoising Clean Pass
          - denoising\_clean\_pass *boolean*
      - Generate Denoising Prefiltered Pass
          - denoising\_prefiltered\_pass *boolean*
      - Denoising Flags
          - denoising\_flags *int*
      - Use Adaptive Sampling
          - use\_adaptive\_sampling *boolean*
      - Use Light Visibility
          - use\_light\_visibility *boolean*
      - Display Pass
          - display\_pass *enum*
      - Cryptomatte Passes
          - cryptomatte\_passes *enum*
      - Cryptomatte Depth
          - cryptomatte\_depth *int*

#### Geometry Base

  - Sockets
      - Motion Steps
          - motion\_steps *uint*
      - Use Motion Blur
          - use\_motion\_blur *boolean*
      - Shaders
          - used\_shaders *array\_node*

#### Hair

  - Base : geometry\_base
  - Sockets
      - Motion Steps
          - motion\_steps *uint*
      - Use Motion Blur
          - use\_motion\_blur *boolean*
      - Shaders
          - used\_shaders *array\_node*
      - Curve Keys
          - curve\_keys *array\_point*
      - Curve Radius
          - curve\_radius *array\_float*
      - Curve First Key
          - curve\_first\_key *array\_int*
      - Curve Shader
          - curve\_shader *array\_int*

#### Integrator

  - Sockets
      - Min Bounce
          - min\_bounce *int*
      - Max Bounce
          - max\_bounce *int*
      - Max Diffuse Bounce
          - max\_diffuse\_bounce *int*
      - Max Glossy Bounce
          - max\_glossy\_bounce *int*
      - Max Transmission Bounce
          - max\_transmission\_bounce *int*
      - Max Volume Bounce
          - max\_volume\_bounce *int*
      - Transparent Min Bounce
          - transparent\_min\_bounce *int*
      - Transparent Max Bounce
          - transparent\_max\_bounce *int*
      - AO Bounces
          - ao\_bounces *int*
      - Volume Max Steps
          - volume\_max\_steps *int*
      - Volume Step Rate
          - volume\_step\_rate *float*
      - Reflective Caustics
          - caustics\_reflective *boolean*
      - Refractive Caustics
          - caustics\_refractive *boolean*
      - Filter Glossy
          - filter\_glossy *float*
      - Seed
          - seed *int*
      - Sample Clamp Direct
          - sample\_clamp\_direct *float*
      - Sample Clamp Indirect
          - sample\_clamp\_indirect *float*
      - Motion Blur
          - motion\_blur *boolean*
      - AA Samples
          - aa\_samples *int*
      - Diffuse Samples
          - diffuse\_samples *int*
      - Glossy Samples
          - glossy\_samples *int*
      - Transmission Samples
          - transmission\_samples *int*
      - AO Samples
          - ao\_samples *int*
      - Mesh Light Samples
          - mesh\_light\_samples *int*
      - Subsurface Samples
          - subsurface\_samples *int*
      - Volume Samples
          - volume\_samples *int*
      - Start Sample
          - start\_sample *int*
      - Adaptive Threshold
          - adaptive\_threshold *float*
      - Adaptive Min Samples
          - adaptive\_min\_samples *int*
      - Sample All Lights Direct
          - sample\_all\_lights\_direct *boolean*
      - Sample All Lights Indirect
          - sample\_all\_lights\_indirect *boolean*
      - Light Sampling Threshold
          - light\_sampling\_threshold *float*
      - Method
          - method *enum*
      - Sampling Pattern
          - sampling\_pattern *enum*

#### Light

  - Sockets
      - Type
          - light\_type *enum*
      - Strength
          - strength *color*
      - Co
          - co *point*
      - Dir
          - dir *vector*
      - Size
          - size *float*
      - Angle
          - angle *float*
      - Axis U
          - axisu *vector*
      - Size U
          - sizeu *float*
      - Axis V
          - axisv *vector*
      - Size V
          - sizev *float*
      - Round
          - round *boolean*
      - Spread
          - spread *float*
      - Map Resolution
          - map\_resolution *int*
      - Spot Angle
          - spot\_angle *float*
      - Spot Smooth
          - spot\_smooth *float*
      - Transform
          - tfm *transform*
      - Cast Shadow
          - cast\_shadow *boolean*
      - Use Mis
          - use\_mis *boolean*
      - Use Diffuse
          - use\_diffuse *boolean*
      - Use Glossy
          - use\_glossy *boolean*
      - Use Transmission
          - use\_transmission *boolean*
      - Use Scatter
          - use\_scatter *boolean*
      - Samples
          - samples *int*
      - Max Bounces
          - max\_bounces *int*
      - Random ID
          - random\_id *uint*
      - Is Portal
          - is\_portal *boolean*
      - Is Enabled
          - is\_enabled *boolean*
      - Shader
          - shader *node*

#### Mesh

  - Base : geometry\_base
  - Sockets
      - Motion Steps
          - motion\_steps *uint*
      - Use Motion Blur
          - use\_motion\_blur *boolean*
      - Shaders
          - used\_shaders *array\_node*
      - Triangles
          - triangles *array\_int*
      - Vertices
          - verts *array\_point*
      - Shader
          - shader *array\_int*
      - Smooth
          - smooth *array\_boolean*
      - Triangle Patch
          - triangle\_patch *array\_int*
      - Patch UVs
          - vert\_patch\_uv *array\_point2*
      - Subdivision Type
          - subdivision\_type *enum*
      - Subdivision Crease Edges
          - subd\_creases\_edge *array\_int*
      - Subdivision Crease Weights
          - subd\_creases\_weight *array\_float*
      - Subdivision Face Corners
          - subd\_face\_corners *array\_int*
      - Subdivision Face Start Corner
          - subd\_start\_corner *array\_int*
      - Subdivision Face Corner Count
          - subd\_num\_corners *array\_int*
      - Subdivision Face Shader
          - subd\_shader *array\_int*
      - Subdivision Face Smooth
          - subd\_smooth *array\_boolean*
      - Subdivision Face PTex Offset
          - subd\_ptex\_offset *array\_int*
      - NGons Number
          - num\_ngons *int*
      - Subdivision Dicing Rate
          - subd\_dicing\_rate *float*
      - Subdivision Dicing Rate
          - subd\_max\_level *int*
      - Subdivision Object Transform
          - subd\_objecttoworld *transform*

#### Object

  - Sockets
      - Geometry
          - geometry *node*
      - Transform
          - tfm *transform*
      - Visibility
          - visibility *uint*
      - Color
          - color *color*
      - Random ID
          - random\_id *uint*
      - Pass ID
          - pass\_id *int*
      - Use Holdout
          - use\_holdout *boolean*
      - Hide on Missing Motion
          - hide\_on\_missing\_motion *boolean*
      - Dupli Generated
          - dupli\_generated *point*
      - Dupli UV
          - dupli\_uv *point2*
      - Motion
          - motion *array\_transform*
      - Terminator Offset
          - shadow\_terminator\_offset *float*
      - Asset Name
          - asset\_name *string*
      - Shadow Catcher
          - is\_shadow\_catcher *boolean*
      - Particle System
          - particle\_system *node*
      - Particle Index
          - particle\_index *int*

#### Particle System

  - Sockets

#### Pass

  - Sockets
      - Type
          - type *enum*
      - Name
          - name *string*

#### Shader

  - Sockets
      - Use MIS
          - use\_mis *boolean*
      - Use Transparent Shadow
          - use\_transparent\_shadow *boolean*
      - Heterogeneous Volume
          - heterogeneous\_volume *boolean*
      - Volume Sampling Method
          - volume\_sampling\_method *enum*
      - Volume Interpolation Method
          - volume\_interpolation\_method *enum*
      - Volume Step Rate
          - volume\_step\_rate *float*
      - Displacement Method
          - displacement\_method *enum*
      - Pass ID
          - pass\_id *int*

#### Volume

  - Base : mesh
  - Sockets
      - Motion Steps
          - motion\_steps *uint*
      - Use Motion Blur
          - use\_motion\_blur *boolean*
      - Shaders
          - used\_shaders *array\_node*
      - Triangles
          - triangles *array\_int*
      - Vertices
          - verts *array\_point*
      - Shader
          - shader *array\_int*
      - Smooth
          - smooth *array\_boolean*
      - Triangle Patch
          - triangle\_patch *array\_int*
      - Patch UVs
          - vert\_patch\_uv *array\_point2*
      - Subdivision Type
          - subdivision\_type *enum*
      - Subdivision Crease Edges
          - subd\_creases\_edge *array\_int*
      - Subdivision Crease Weights
          - subd\_creases\_weight *array\_float*
      - Subdivision Face Corners
          - subd\_face\_corners *array\_int*
      - Subdivision Face Start Corner
          - subd\_start\_corner *array\_int*
      - Subdivision Face Corner Count
          - subd\_num\_corners *array\_int*
      - Subdivision Face Shader
          - subd\_shader *array\_int*
      - Subdivision Face Smooth
          - subd\_smooth *array\_boolean*
      - Subdivision Face PTex Offset
          - subd\_ptex\_offset *array\_int*
      - NGons Number
          - num\_ngons *int*
      - Subdivision Dicing Rate
          - subd\_dicing\_rate *float*
      - Subdivision Dicing Rate
          - subd\_max\_level *int*
      - Subdivision Object Transform
          - subd\_objecttoworld *transform*
      - Clipping
          - clipping *float*
      - Step Size
          - step\_size *float*
      - Object Space
          - object\_space *boolean*

### Shader Graph Nodes

Those are the nodes that make up a Cycles Shader graph.

#### Absorption Volume

  - Input Sockets
      - Color
          - color *color*
      - Density
          - density *float*
      - VolumeMixWeight
          - volume\_mix\_weight *float*
  - Output Sockets
      - Volume
          - volume *closure*

#### Add Closure

  - Input Sockets
      - Closure1
          - closure1 *closure*
      - Closure2
          - closure2 *closure*
  - Output Sockets
      - Closure
          - closure *closure*

#### Ambient Occlusion

  - Input Sockets
      - Samples
          - samples *int*
      - Color
          - color *color*
      - Distance
          - distance *float*
      - Normal
          - normal *normal*
      - Inside
          - inside *boolean*
      - Only Local
          - only\_local *boolean*
  - Output Sockets
      - Color
          - color *color*
      - AO
          - ao *float*

#### Anisotropic Bsdf

  - Input Sockets
      - Color
          - color *color*
      - Normal
          - normal *normal*
      - SurfaceMixWeight
          - surface\_mix\_weight *float*
      - Distribution
          - distribution *enum*
      - Tangent
          - tangent *vector*
      - Roughness
          - roughness *float*
      - Anisotropy
          - anisotropy *float*
      - Rotation
          - rotation *float*
  - Output Sockets
      - BSDF
          - BSDF *closure*

#### Aov Output

  - Input Sockets
      - Color
          - color *color*
      - Value
          - value *float*
      - AOV Name
          - name *string*

#### Attribute

  - Input Sockets
      - Attribute
          - attribute *string*
  - Output Sockets
      - Color
          - color *color*
      - Vector
          - vector *vector*
      - Fac
          - fac *float*
      - Alpha
          - alpha *float*

#### Background Shader

  - Input Sockets
      - Color
          - color *color*
      - Strength
          - strength *float*
      - SurfaceMixWeight
          - surface\_mix\_weight *float*
  - Output Sockets
      - Background
          - background *closure*

#### Bevel

  - Input Sockets
      - Samples
          - samples *int*
      - Radius
          - radius *float*
      - Normal
          - normal *normal*
  - Output Sockets
      - Normal
          - bevel *normal*

#### Blackbody

  - Input Sockets
      - Temperature
          - temperature *float*
  - Output Sockets
      - Color
          - color *color*

#### Brick Texture

  - Input Sockets
      - Translation
          - tex\_mapping.translation *point*
      - Rotation
          - tex\_mapping.rotation *vector*
      - Scale
          - tex\_mapping.scale *vector*
      - Min
          - tex\_mapping.min *vector*
      - Max
          - tex\_mapping.max *vector*
      - Use Min Max
          - tex\_mapping.use\_minmax *boolean*
      - x\_mapping
          - tex\_mapping.x\_mapping *enum*
      - y\_mapping
          - tex\_mapping.y\_mapping *enum*
      - z\_mapping
          - tex\_mapping.z\_mapping *enum*
      - Type
          - tex\_mapping.type *enum*
      - Projection
          - tex\_mapping.projection *enum*
      - Offset
          - offset *float*
      - Offset Frequency
          - offset\_frequency *int*
      - Squash
          - squash *float*
      - Squash Frequency
          - squash\_frequency *int*
      - Vector
          - vector *point*
      - Color1
          - color1 *color*
      - Color2
          - color2 *color*
      - Mortar
          - mortar *color*
      - Scale
          - scale *float*
      - Mortar Size
          - mortar\_size *float*
      - Mortar Smooth
          - mortar\_smooth *float*
      - Bias
          - bias *float*
      - Brick Width
          - brick\_width *float*
      - Row Height
          - row\_height *float*
  - Output Sockets
      - Color
          - color *color*
      - Fac
          - fac *float*

#### Brightness Contrast

  - Input Sockets
      - Color
          - color *color*
      - Bright
          - bright *float*
      - Contrast
          - contrast *float*
  - Output Sockets
      - Color
          - color *color*

#### Bump

  - Input Sockets
      - Invert
          - invert *boolean*
      - UseObjectSpace
          - use\_object\_space *boolean*
      - Height
          - height *float*
      - SampleCenter
          - sample\_center *float*
      - SampleX
          - sample\_x *float*
      - SampleY
          - sample\_y *float*
      - Normal
          - normal *normal*
      - Strength
          - strength *float*
      - Distance
          - distance *float*
  - Output Sockets
      - Normal
          - normal *normal*

#### Camera Info

  - Output Sockets
      - View Vector
          - view\_vector *vector*
      - View Z Depth
          - view\_z\_depth *float*
      - View Distance
          - view\_distance *float*

#### Checker Texture

  - Input Sockets
      - Translation
          - tex\_mapping.translation *point*
      - Rotation
          - tex\_mapping.rotation *vector*
      - Scale
          - tex\_mapping.scale *vector*
      - Min
          - tex\_mapping.min *vector*
      - Max
          - tex\_mapping.max *vector*
      - Use Min Max
          - tex\_mapping.use\_minmax *boolean*
      - x\_mapping
          - tex\_mapping.x\_mapping *enum*
      - y\_mapping
          - tex\_mapping.y\_mapping *enum*
      - z\_mapping
          - tex\_mapping.z\_mapping *enum*
      - Type
          - tex\_mapping.type *enum*
      - Projection
          - tex\_mapping.projection *enum*
      - Vector
          - vector *point*
      - Color1
          - color1 *color*
      - Color2
          - color2 *color*
      - Scale
          - scale *float*
  - Output Sockets
      - Color
          - color *color*
      - Fac
          - fac *float*

#### Clamp

  - Input Sockets
      - Type
          - clamp\_type *enum*
      - Value
          - value *float*
      - Min
          - min *float*
      - Max
          - max *float*
  - Output Sockets
      - Result
          - result *float*

#### Color

  - Input Sockets
      - Value
          - value *color*
  - Output Sockets
      - Color
          - color *color*

#### Combine Hsv

  - Input Sockets
      - H
          - h *float*
      - S
          - s *float*
      - V
          - v *float*
  - Output Sockets
      - Color
          - color *color*

#### Combine Rgb

  - Input Sockets
      - R
          - r *float*
      - G
          - g *float*
      - B
          - b *float*
  - Output Sockets
      - Image
          - image *color*

#### Combine Xyz

  - Input Sockets
      - X
          - x *float*
      - Y
          - y *float*
      - Z
          - z *float*
  - Output Sockets
      - Vector
          - vector *vector*

#### Convert Closure To Closure

  - Input Sockets
      - value\_closure
          - value\_closure *closure*
  - Output Sockets
      - value\_closure
          - value\_closure *closure*

#### Convert Closure To Color

  - Input Sockets
      - value\_closure
          - value\_closure *closure*
  - Output Sockets
      - value\_color
          - value\_color *color*

#### Convert Closure To Float

  - Input Sockets
      - value\_closure
          - value\_closure *closure*
  - Output Sockets
      - value\_float
          - value\_float *float*

#### Convert Closure To Int

  - Input Sockets
      - value\_closure
          - value\_closure *closure*
  - Output Sockets
      - value\_int
          - value\_int *int*

#### Convert Closure To Normal

  - Input Sockets
      - value\_closure
          - value\_closure *closure*
  - Output Sockets
      - value\_normal
          - value\_normal *normal*

#### Convert Closure To Point

  - Input Sockets
      - value\_closure
          - value\_closure *closure*
  - Output Sockets
      - value\_point
          - value\_point *point*

#### Convert Closure To String

  - Input Sockets
      - value\_closure
          - value\_closure *closure*
  - Output Sockets
      - value\_string
          - value\_string *string*

#### Convert Closure To Vector

  - Input Sockets
      - value\_closure
          - value\_closure *closure*
  - Output Sockets
      - value\_vector
          - value\_vector *vector*

#### Convert Color To Closure

  - Input Sockets
      - value\_color
          - value\_color *color*
  - Output Sockets
      - value\_closure
          - value\_closure *closure*

#### Convert Color To Color

  - Input Sockets
      - value\_color
          - value\_color *color*
  - Output Sockets
      - value\_color
          - value\_color *color*

#### Convert Color To Float

  - Input Sockets
      - value\_color
          - value\_color *color*
  - Output Sockets
      - value\_float
          - value\_float *float*

#### Convert Color To Int

  - Input Sockets
      - value\_color
          - value\_color *color*
  - Output Sockets
      - value\_int
          - value\_int *int*

#### Convert Color To Normal

  - Input Sockets
      - value\_color
          - value\_color *color*
  - Output Sockets
      - value\_normal
          - value\_normal *normal*

#### Convert Color To Point

  - Input Sockets
      - value\_color
          - value\_color *color*
  - Output Sockets
      - value\_point
          - value\_point *point*

#### Convert Color To String

  - Input Sockets
      - value\_color
          - value\_color *color*
  - Output Sockets
      - value\_string
          - value\_string *string*

#### Convert Color To Vector

  - Input Sockets
      - value\_color
          - value\_color *color*
  - Output Sockets
      - value\_vector
          - value\_vector *vector*

#### Convert Float To Closure

  - Input Sockets
      - value\_float
          - value\_float *float*
  - Output Sockets
      - value\_closure
          - value\_closure *closure*

#### Convert Float To Color

  - Input Sockets
      - value\_float
          - value\_float *float*
  - Output Sockets
      - value\_color
          - value\_color *color*

#### Convert Float To Float

  - Input Sockets
      - value\_float
          - value\_float *float*
  - Output Sockets
      - value\_float
          - value\_float *float*

#### Convert Float To Int

  - Input Sockets
      - value\_float
          - value\_float *float*
  - Output Sockets
      - value\_int
          - value\_int *int*

#### Convert Float To Normal

  - Input Sockets
      - value\_float
          - value\_float *float*
  - Output Sockets
      - value\_normal
          - value\_normal *normal*

#### Convert Float To Point

  - Input Sockets
      - value\_float
          - value\_float *float*
  - Output Sockets
      - value\_point
          - value\_point *point*

#### Convert Float To String

  - Input Sockets
      - value\_float
          - value\_float *float*
  - Output Sockets
      - value\_string
          - value\_string *string*

#### Convert Float To Vector

  - Input Sockets
      - value\_float
          - value\_float *float*
  - Output Sockets
      - value\_vector
          - value\_vector *vector*

#### Convert Int To Closure

  - Input Sockets
      - value\_int
          - value\_int *int*
  - Output Sockets
      - value\_closure
          - value\_closure *closure*

#### Convert Int To Color

  - Input Sockets
      - value\_int
          - value\_int *int*
  - Output Sockets
      - value\_color
          - value\_color *color*

#### Convert Int To Float

  - Input Sockets
      - value\_int
          - value\_int *int*
  - Output Sockets
      - value\_float
          - value\_float *float*

#### Convert Int To Int

  - Input Sockets
      - value\_int
          - value\_int *int*
  - Output Sockets
      - value\_int
          - value\_int *int*

#### Convert Int To Normal

  - Input Sockets
      - value\_int
          - value\_int *int*
  - Output Sockets
      - value\_normal
          - value\_normal *normal*

#### Convert Int To Point

  - Input Sockets
      - value\_int
          - value\_int *int*
  - Output Sockets
      - value\_point
          - value\_point *point*

#### Convert Int To String

  - Input Sockets
      - value\_int
          - value\_int *int*
  - Output Sockets
      - value\_string
          - value\_string *string*

#### Convert Int To Vector

  - Input Sockets
      - value\_int
          - value\_int *int*
  - Output Sockets
      - value\_vector
          - value\_vector *vector*

#### Convert Normal To Closure

  - Input Sockets
      - value\_normal
          - value\_normal *normal*
  - Output Sockets
      - value\_closure
          - value\_closure *closure*

#### Convert Normal To Color

  - Input Sockets
      - value\_normal
          - value\_normal *normal*
  - Output Sockets
      - value\_color
          - value\_color *color*

#### Convert Normal To Float

  - Input Sockets
      - value\_normal
          - value\_normal *normal*
  - Output Sockets
      - value\_float
          - value\_float *float*

#### Convert Normal To Int

  - Input Sockets
      - value\_normal
          - value\_normal *normal*
  - Output Sockets
      - value\_int
          - value\_int *int*

#### Convert Normal To Normal

  - Input Sockets
      - value\_normal
          - value\_normal *normal*
  - Output Sockets
      - value\_normal
          - value\_normal *normal*

#### Convert Normal To Point

  - Input Sockets
      - value\_normal
          - value\_normal *normal*
  - Output Sockets
      - value\_point
          - value\_point *point*

#### Convert Normal To String

  - Input Sockets
      - value\_normal
          - value\_normal *normal*
  - Output Sockets
      - value\_string
          - value\_string *string*

#### Convert Normal To Vector

  - Input Sockets
      - value\_normal
          - value\_normal *normal*
  - Output Sockets
      - value\_vector
          - value\_vector *vector*

#### Convert Point To Closure

  - Input Sockets
      - value\_point
          - value\_point *point*
  - Output Sockets
      - value\_closure
          - value\_closure *closure*

#### Convert Point To Color

  - Input Sockets
      - value\_point
          - value\_point *point*
  - Output Sockets
      - value\_color
          - value\_color *color*

#### Convert Point To Float

  - Input Sockets
      - value\_point
          - value\_point *point*
  - Output Sockets
      - value\_float
          - value\_float *float*

#### Convert Point To Int

  - Input Sockets
      - value\_point
          - value\_point *point*
  - Output Sockets
      - value\_int
          - value\_int *int*

#### Convert Point To Normal

  - Input Sockets
      - value\_point
          - value\_point *point*
  - Output Sockets
      - value\_normal
          - value\_normal *normal*

#### Convert Point To Point

  - Input Sockets
      - value\_point
          - value\_point *point*
  - Output Sockets
      - value\_point
          - value\_point *point*

#### Convert Point To String

  - Input Sockets
      - value\_point
          - value\_point *point*
  - Output Sockets
      - value\_string
          - value\_string *string*

#### Convert Point To Vector

  - Input Sockets
      - value\_point
          - value\_point *point*
  - Output Sockets
      - value\_vector
          - value\_vector *vector*

#### Convert String To Closure

  - Input Sockets
      - value\_string
          - value\_string *string*
  - Output Sockets
      - value\_closure
          - value\_closure *closure*

#### Convert String To Color

  - Input Sockets
      - value\_string
          - value\_string *string*
  - Output Sockets
      - value\_color
          - value\_color *color*

#### Convert String To Float

  - Input Sockets
      - value\_string
          - value\_string *string*
  - Output Sockets
      - value\_float
          - value\_float *float*

#### Convert String To Int

  - Input Sockets
      - value\_string
          - value\_string *string*
  - Output Sockets
      - value\_int
          - value\_int *int*

#### Convert String To Normal

  - Input Sockets
      - value\_string
          - value\_string *string*
  - Output Sockets
      - value\_normal
          - value\_normal *normal*

#### Convert String To Point

  - Input Sockets
      - value\_string
          - value\_string *string*
  - Output Sockets
      - value\_point
          - value\_point *point*

#### Convert String To String

  - Input Sockets
      - value\_string
          - value\_string *string*
  - Output Sockets
      - value\_string
          - value\_string *string*

#### Convert String To Vector

  - Input Sockets
      - value\_string
          - value\_string *string*
  - Output Sockets
      - value\_vector
          - value\_vector *vector*

#### Convert Vector To Closure

  - Input Sockets
      - value\_vector
          - value\_vector *vector*
  - Output Sockets
      - value\_closure
          - value\_closure *closure*

#### Convert Vector To Color

  - Input Sockets
      - value\_vector
          - value\_vector *vector*
  - Output Sockets
      - value\_color
          - value\_color *color*

#### Convert Vector To Float

  - Input Sockets
      - value\_vector
          - value\_vector *vector*
  - Output Sockets
      - value\_float
          - value\_float *float*

#### Convert Vector To Int

  - Input Sockets
      - value\_vector
          - value\_vector *vector*
  - Output Sockets
      - value\_int
          - value\_int *int*

#### Convert Vector To Normal

  - Input Sockets
      - value\_vector
          - value\_vector *vector*
  - Output Sockets
      - value\_normal
          - value\_normal *normal*

#### Convert Vector To Point

  - Input Sockets
      - value\_vector
          - value\_vector *vector*
  - Output Sockets
      - value\_point
          - value\_point *point*

#### Convert Vector To String

  - Input Sockets
      - value\_vector
          - value\_vector *vector*
  - Output Sockets
      - value\_string
          - value\_string *string*

#### Convert Vector To Vector

  - Input Sockets
      - value\_vector
          - value\_vector *vector*
  - Output Sockets
      - value\_vector
          - value\_vector *vector*

#### Diffuse Bsdf

  - Input Sockets
      - Color
          - color *color*
      - Normal
          - normal *normal*
      - SurfaceMixWeight
          - surface\_mix\_weight *float*
      - Roughness
          - roughness *float*
  - Output Sockets
      - BSDF
          - BSDF *closure*

#### Displacement

  - Input Sockets
      - Space
          - space *enum*
      - Height
          - height *float*
      - Midlevel
          - midlevel *float*
      - Scale
          - scale *float*
      - Normal
          - normal *normal*
  - Output Sockets
      - Displacement
          - displacement *vector*

#### Emission

  - Input Sockets
      - Color
          - color *color*
      - Strength
          - strength *float*
      - SurfaceMixWeight
          - surface\_mix\_weight *float*
  - Output Sockets
      - Emission
          - emission *closure*

#### Environment Texture

  - Input Sockets
      - Translation
          - tex\_mapping.translation *point*
      - Rotation
          - tex\_mapping.rotation *vector*
      - Scale
          - tex\_mapping.scale *vector*
      - Min
          - tex\_mapping.min *vector*
      - Max
          - tex\_mapping.max *vector*
      - Use Min Max
          - tex\_mapping.use\_minmax *boolean*
      - x\_mapping
          - tex\_mapping.x\_mapping *enum*
      - y\_mapping
          - tex\_mapping.y\_mapping *enum*
      - z\_mapping
          - tex\_mapping.z\_mapping *enum*
      - Type
          - tex\_mapping.type *enum*
      - Projection
          - tex\_mapping.projection *enum*
      - Filename
          - filename *string*
      - Colorspace
          - colorspace *string*
      - Alpha Type
          - alpha\_type *enum*
      - Interpolation
          - interpolation *enum*
      - Projection
          - projection *enum*
      - Animated
          - animated *boolean*
      - Vector
          - vector *point*
  - Output Sockets
      - Color
          - color *color*
      - Alpha
          - alpha *float*

#### Fresnel

  - Input Sockets
      - Normal
          - normal *normal*
      - IOR
          - IOR *float*
  - Output Sockets
      - Fac
          - fac *float*

#### Gamma

  - Input Sockets
      - Color
          - color *color*
      - Gamma
          - gamma *float*
  - Output Sockets
      - Color
          - color *color*

#### Geometry

  - Input Sockets
      - NormalIn
          - normal\_osl *normal*
  - Output Sockets
      - Position
          - position *point*
      - Normal
          - normal *normal*
      - Tangent
          - tangent *normal*
      - True Normal
          - true\_normal *normal*
      - Incoming
          - incoming *vector*
      - Parametric
          - parametric *point*
      - Backfacing
          - backfacing *float*
      - Pointiness
          - pointiness *float*
      - Random Per Island
          - random\_per\_island *float*

#### Glass Bsdf

  - Input Sockets
      - Color
          - color *color*
      - Normal
          - normal *normal*
      - SurfaceMixWeight
          - surface\_mix\_weight *float*
      - Distribution
          - distribution *enum*
      - Roughness
          - roughness *float*
      - IOR
          - IOR *float*
  - Output Sockets
      - BSDF
          - BSDF *closure*

#### Glossy Bsdf

  - Input Sockets
      - Color
          - color *color*
      - Normal
          - normal *normal*
      - SurfaceMixWeight
          - surface\_mix\_weight *float*
      - Distribution
          - distribution *enum*
      - Roughness
          - roughness *float*
  - Output Sockets
      - BSDF
          - BSDF *closure*

#### Gradient Texture

  - Input Sockets
      - Translation
          - tex\_mapping.translation *point*
      - Rotation
          - tex\_mapping.rotation *vector*
      - Scale
          - tex\_mapping.scale *vector*
      - Min
          - tex\_mapping.min *vector*
      - Max
          - tex\_mapping.max *vector*
      - Use Min Max
          - tex\_mapping.use\_minmax *boolean*
      - x\_mapping
          - tex\_mapping.x\_mapping *enum*
      - y\_mapping
          - tex\_mapping.y\_mapping *enum*
      - z\_mapping
          - tex\_mapping.z\_mapping *enum*
      - Type
          - tex\_mapping.type *enum*
      - Projection
          - tex\_mapping.projection *enum*
      - Type
          - gradient\_type *enum*
      - Vector
          - vector *point*
  - Output Sockets
      - Color
          - color *color*
      - Fac
          - fac *float*

#### Hair Bsdf

  - Input Sockets
      - Color
          - color *color*
      - Normal
          - normal *normal*
      - SurfaceMixWeight
          - surface\_mix\_weight *float*
      - Component
          - component *enum*
      - Offset
          - offset *float*
      - RoughnessU
          - roughness\_u *float*
      - RoughnessV
          - roughness\_v *float*
      - Tangent
          - tangent *vector*
  - Output Sockets
      - BSDF
          - BSDF *closure*

#### Hair Info

  - Output Sockets
      - Is Strand
          - is\_strand *float*
      - Intercept
          - intercept *float*
      - Thickness
          - thickness *float*
      - Tangent Normal
          - tangent\_normal *normal*
      - Random
          - index *float*

#### Holdout

  - Input Sockets
      - SurfaceMixWeight
          - surface\_mix\_weight *float*
      - VolumeMixWeight
          - volume\_mix\_weight *float*
  - Output Sockets
      - Holdout
          - holdout *closure*

#### Hsv

  - Input Sockets
      - Hue
          - hue *float*
      - Saturation
          - saturation *float*
      - Value
          - value *float*
      - Fac
          - fac *float*
      - Color
          - color *color*
  - Output Sockets
      - Color
          - color *color*

#### Ies Light

  - Input Sockets
      - Translation
          - tex\_mapping.translation *point*
      - Rotation
          - tex\_mapping.rotation *vector*
      - Scale
          - tex\_mapping.scale *vector*
      - Min
          - tex\_mapping.min *vector*
      - Max
          - tex\_mapping.max *vector*
      - Use Min Max
          - tex\_mapping.use\_minmax *boolean*
      - x\_mapping
          - tex\_mapping.x\_mapping *enum*
      - y\_mapping
          - tex\_mapping.y\_mapping *enum*
      - z\_mapping
          - tex\_mapping.z\_mapping *enum*
      - Type
          - tex\_mapping.type *enum*
      - Projection
          - tex\_mapping.projection *enum*
      - IES
          - ies *string*
      - File Name
          - filename *string*
      - Strength
          - strength *float*
      - Vector
          - vector *point*
  - Output Sockets
      - Fac
          - fac *float*

#### Image Texture

  - Input Sockets
      - Translation
          - tex\_mapping.translation *point*
      - Rotation
          - tex\_mapping.rotation *vector*
      - Scale
          - tex\_mapping.scale *vector*
      - Min
          - tex\_mapping.min *vector*
      - Max
          - tex\_mapping.max *vector*
      - Use Min Max
          - tex\_mapping.use\_minmax *boolean*
      - x\_mapping
          - tex\_mapping.x\_mapping *enum*
      - y\_mapping
          - tex\_mapping.y\_mapping *enum*
      - z\_mapping
          - tex\_mapping.z\_mapping *enum*
      - Type
          - tex\_mapping.type *enum*
      - Projection
          - tex\_mapping.projection *enum*
      - Filename
          - filename *string*
      - Colorspace
          - colorspace *string*
      - Alpha Type
          - alpha\_type *enum*
      - Interpolation
          - interpolation *enum*
      - Extension
          - extension *enum*
      - Projection
          - projection *enum*
      - Projection Blend
          - projection\_blend *float*
      - Tiles
          - tiles *array\_int*
      - Animated
          - animated *boolean*
      - Vector
          - vector *point*
  - Output Sockets
      - Color
          - color *color*
      - Alpha
          - alpha *float*

#### Invert

  - Input Sockets
      - Fac
          - fac *float*
      - Color
          - color *color*
  - Output Sockets
      - Color
          - color *color*

#### Layer Weight

  - Input Sockets
      - Normal
          - normal *normal*
      - Blend
          - blend *float*
  - Output Sockets
      - Fresnel
          - fresnel *float*
      - Facing
          - facing *float*

#### Light Falloff

  - Input Sockets
      - Strength
          - strength *float*
      - Smooth
          - smooth *float*
  - Output Sockets
      - Quadratic
          - quadratic *float*
      - Linear
          - linear *float*
      - Constant
          - constant *float*

#### Light Path

  - Output Sockets
      - Is Camera Ray
          - is\_camera\_ray *float*
      - Is Shadow Ray
          - is\_shadow\_ray *float*
      - Is Diffuse Ray
          - is\_diffuse\_ray *float*
      - Is Glossy Ray
          - is\_glossy\_ray *float*
      - Is Singular Ray
          - is\_singular\_ray *float*
      - Is Reflection Ray
          - is\_reflection\_ray *float*
      - Is Transmission Ray
          - is\_transmission\_ray *float*
      - Is Volume Scatter Ray
          - is\_volume\_scatter\_ray *float*
      - Ray Length
          - ray\_length *float*
      - Ray Depth
          - ray\_depth *float*
      - Diffuse Depth
          - diffuse\_depth *float*
      - Glossy Depth
          - glossy\_depth *float*
      - Transparent Depth
          - transparent\_depth *float*
      - Transmission Depth
          - transmission\_depth *float*

#### Magic Texture

  - Input Sockets
      - Translation
          - tex\_mapping.translation *point*
      - Rotation
          - tex\_mapping.rotation *vector*
      - Scale
          - tex\_mapping.scale *vector*
      - Min
          - tex\_mapping.min *vector*
      - Max
          - tex\_mapping.max *vector*
      - Use Min Max
          - tex\_mapping.use\_minmax *boolean*
      - x\_mapping
          - tex\_mapping.x\_mapping *enum*
      - y\_mapping
          - tex\_mapping.y\_mapping *enum*
      - z\_mapping
          - tex\_mapping.z\_mapping *enum*
      - Type
          - tex\_mapping.type *enum*
      - Projection
          - tex\_mapping.projection *enum*
      - Depth
          - depth *int*
      - Vector
          - vector *point*
      - Scale
          - scale *float*
      - Distortion
          - distortion *float*
  - Output Sockets
      - Color
          - color *color*
      - Fac
          - fac *float*

#### Map Range

  - Input Sockets
      - Type
          - range\_type *enum*
      - Value
          - value *float*
      - From Min
          - from\_min *float*
      - From Max
          - from\_max *float*
      - To Min
          - to\_min *float*
      - To Max
          - to\_max *float*
      - Steps
          - steps *float*
      - Clamp
          - clamp *boolean*
  - Output Sockets
      - Result
          - result *float*

#### Mapping

  - Input Sockets
      - Type
          - mapping\_type *enum*
      - Vector
          - vector *point*
      - Location
          - location *point*
      - Rotation
          - rotation *point*
      - Scale
          - scale *point*
  - Output Sockets
      - Vector
          - vector *point*

#### Math

  - Input Sockets
      - Type
          - math\_type *enum*
      - Use Clamp
          - use\_clamp *boolean*
      - Value1
          - value1 *float*
      - Value2
          - value2 *float*
      - Value3
          - value3 *float*
  - Output Sockets
      - Value
          - value *float*

#### Mix

  - Input Sockets
      - Type
          - mix\_type *enum*
      - Use Clamp
          - use\_clamp *boolean*
      - Fac
          - fac *float*
      - Color1
          - color1 *color*
      - Color2
          - color2 *color*
  - Output Sockets
      - Color
          - color *color*

#### Mix Closure

  - Input Sockets
      - Fac
          - fac *float*
      - Closure1
          - closure1 *closure*
      - Closure2
          - closure2 *closure*
  - Output Sockets
      - Closure
          - closure *closure*

#### Mix Closure Weight

  - Input Sockets
      - Weight
          - weight *float*
      - Fac
          - fac *float*
  - Output Sockets
      - Weight1
          - weight1 *float*
      - Weight2
          - weight2 *float*

#### Musgrave Texture

  - Input Sockets
      - Translation
          - tex\_mapping.translation *point*
      - Rotation
          - tex\_mapping.rotation *vector*
      - Scale
          - tex\_mapping.scale *vector*
      - Min
          - tex\_mapping.min *vector*
      - Max
          - tex\_mapping.max *vector*
      - Use Min Max
          - tex\_mapping.use\_minmax *boolean*
      - x\_mapping
          - tex\_mapping.x\_mapping *enum*
      - y\_mapping
          - tex\_mapping.y\_mapping *enum*
      - z\_mapping
          - tex\_mapping.z\_mapping *enum*
      - Type
          - tex\_mapping.type *enum*
      - Projection
          - tex\_mapping.projection *enum*
      - Dimensions
          - dimensions *enum*
      - Type
          - musgrave\_type *enum*
      - Vector
          - vector *point*
      - W
          - w *float*
      - Scale
          - scale *float*
      - Detail
          - detail *float*
      - Dimension
          - dimension *float*
      - Lacunarity
          - lacunarity *float*
      - Offset
          - offset *float*
      - Gain
          - gain *float*
  - Output Sockets
      - Fac
          - fac *float*

#### Noise Texture

  - Input Sockets
      - Translation
          - tex\_mapping.translation *point*
      - Rotation
          - tex\_mapping.rotation *vector*
      - Scale
          - tex\_mapping.scale *vector*
      - Min
          - tex\_mapping.min *vector*
      - Max
          - tex\_mapping.max *vector*
      - Use Min Max
          - tex\_mapping.use\_minmax *boolean*
      - x\_mapping
          - tex\_mapping.x\_mapping *enum*
      - y\_mapping
          - tex\_mapping.y\_mapping *enum*
      - z\_mapping
          - tex\_mapping.z\_mapping *enum*
      - Type
          - tex\_mapping.type *enum*
      - Projection
          - tex\_mapping.projection *enum*
      - Dimensions
          - dimensions *enum*
      - Vector
          - vector *point*
      - W
          - w *float*
      - Scale
          - scale *float*
      - Detail
          - detail *float*
      - Roughness
          - roughness *float*
      - Distortion
          - distortion *float*
  - Output Sockets
      - Fac
          - fac *float*
      - Color
          - color *color*

#### Normal

  - Input Sockets
      - direction
          - direction *vector*
      - Normal
          - normal *normal*
  - Output Sockets
      - Normal
          - normal *normal*
      - Dot
          - dot *float*

#### Normal Map

  - Input Sockets
      - Space
          - space *enum*
      - Attribute
          - attribute *string*
      - NormalIn
          - normal\_osl *normal*
      - Strength
          - strength *float*
      - Color
          - color *color*
  - Output Sockets
      - Normal
          - normal *normal*

#### Object Info

  - Output Sockets
      - Location
          - location *vector*
      - Color
          - color *color*
      - Object Index
          - object\_index *float*
      - Material Index
          - material\_index *float*
      - Random
          - random *float*

#### Output

  - Input Sockets
      - Surface
          - surface *closure*
      - Volume
          - volume *closure*
      - Displacement
          - displacement *vector*
      - Normal
          - normal *normal*

#### Particle Info

  - Output Sockets
      - Index
          - index *float*
      - Random
          - random *float*
      - Age
          - age *float*
      - Lifetime
          - lifetime *float*
      - Location
          - location *point*
      - Size
          - size *float*
      - Velocity
          - velocity *vector*
      - Angular Velocity
          - angular\_velocity *vector*

#### Point Density Texture

  - Input Sockets
      - Filename
          - filename *string*
      - Space
          - space *enum*
      - Interpolation
          - interpolation *enum*
      - Transform
          - tfm *transform*
      - Vector
          - vector *point*
  - Output Sockets
      - Density
          - density *float*
      - Color
          - color *color*

#### Principled Bsdf

  - Input Sockets
      - Distribution
          - distribution *enum*
      - Subsurface Method
          - subsurface\_method *enum*
      - Base Color
          - base\_color *color*
      - Subsurface Color
          - subsurface\_color *color*
      - Metallic
          - metallic *float*
      - Subsurface
          - subsurface *float*
      - Subsurface Radius
          - subsurface\_radius *vector*
      - Specular
          - specular *float*
      - Roughness
          - roughness *float*
      - Specular Tint
          - specular\_tint *float*
      - Anisotropic
          - anisotropic *float*
      - Sheen
          - sheen *float*
      - Sheen Tint
          - sheen\_tint *float*
      - Clearcoat
          - clearcoat *float*
      - Clearcoat Roughness
          - clearcoat\_roughness *float*
      - IOR
          - ior *float*
      - Transmission
          - transmission *float*
      - Transmission Roughness
          - transmission\_roughness *float*
      - Anisotropic Rotation
          - anisotropic\_rotation *float*
      - Emission
          - emission *color*
      - Emission Strength
          - emission\_strength *float*
      - Alpha
          - alpha *float*
      - Normal
          - normal *normal*
      - Clearcoat Normal
          - clearcoat\_normal *normal*
      - Tangent
          - tangent *normal*
      - SurfaceMixWeight
          - surface\_mix\_weight *float*
  - Output Sockets
      - BSDF
          - BSDF *closure*

#### Principled Hair Bsdf

  - Input Sockets
      - Parametrization
          - parametrization *enum*
      - Color
          - color *color*
      - Melanin
          - melanin *float*
      - Melanin Redness
          - melanin\_redness *float*
      - Tint
          - tint *color*
      - Absorption Coefficient
          - absorption\_coefficient *vector*
      - Offset
          - offset *float*
      - Roughness
          - roughness *float*
      - Radial Roughness
          - radial\_roughness *float*
      - Coat
          - coat *float*
      - IOR
          - ior *float*
      - Random Roughness
          - random\_roughness *float*
      - Random Color
          - random\_color *float*
      - Random
          - random *float*
      - Normal
          - normal *normal*
      - SurfaceMixWeight
          - surface\_mix\_weight *float*
  - Output Sockets
      - BSDF
          - BSDF *closure*

#### Principled Volume

  - Input Sockets
      - Density Attribute
          - density\_attribute *string*
      - Color Attribute
          - color\_attribute *string*
      - Temperature Attribute
          - temperature\_attribute *string*
      - Color
          - color *color*
      - Density
          - density *float*
      - Anisotropy
          - anisotropy *float*
      - Absorption Color
          - absorption\_color *color*
      - Emission Strength
          - emission\_strength *float*
      - Emission Color
          - emission\_color *color*
      - Blackbody Intensity
          - blackbody\_intensity *float*
      - Blackbody Tint
          - blackbody\_tint *color*
      - Temperature
          - temperature *float*
      - VolumeMixWeight
          - volume\_mix\_weight *float*
  - Output Sockets
      - Volume
          - volume *closure*

#### Refraction Bsdf

  - Input Sockets
      - Color
          - color *color*
      - Normal
          - normal *normal*
      - SurfaceMixWeight
          - surface\_mix\_weight *float*
      - Distribution
          - distribution *enum*
      - Roughness
          - roughness *float*
      - IOR
          - IOR *float*
  - Output Sockets
      - BSDF
          - BSDF *closure*

#### Rgb Curves

  - Input Sockets
      - Curves
          - curves *array\_color*
      - Min X
          - min\_x *float*
      - Max X
          - max\_x *float*
      - Fac
          - fac *float*
      - Color
          - value *color*
  - Output Sockets
      - Color
          - value *color*

#### Rgb Ramp

  - Input Sockets
      - Ramp
          - ramp *array\_color*
      - Ramp Alpha
          - ramp\_alpha *array\_float*
      - Interpolate
          - interpolate *boolean*
      - Fac
          - fac *float*
  - Output Sockets
      - Color
          - color *color*
      - Alpha
          - alpha *float*

#### Rgb To Bw

  - Input Sockets
      - Color
          - color *color*
  - Output Sockets
      - Val
          - val *float*

#### Scatter Volume

  - Input Sockets
      - Color
          - color *color*
      - Density
          - density *float*
      - Anisotropy
          - anisotropy *float*
      - VolumeMixWeight
          - volume\_mix\_weight *float*
  - Output Sockets
      - Volume
          - volume *closure*

#### Separate Hsv

  - Input Sockets
      - Color
          - color *color*
  - Output Sockets
      - H
          - h *float*
      - S
          - s *float*
      - V
          - v *float*

#### Separate Rgb

  - Input Sockets
      - Image
          - color *color*
  - Output Sockets
      - R
          - r *float*
      - G
          - g *float*
      - B
          - b *float*

#### Separate Xyz

  - Input Sockets
      - Vector
          - vector *color*
  - Output Sockets
      - X
          - x *float*
      - Y
          - y *float*
      - Z
          - z *float*

#### Set Normal

  - Input Sockets
      - Direction
          - direction *vector*
  - Output Sockets
      - Normal
          - normal *normal*

#### Sky Texture

  - Input Sockets
      - Translation
          - tex\_mapping.translation *point*
      - Rotation
          - tex\_mapping.rotation *vector*
      - Scale
          - tex\_mapping.scale *vector*
      - Min
          - tex\_mapping.min *vector*
      - Max
          - tex\_mapping.max *vector*
      - Use Min Max
          - tex\_mapping.use\_minmax *boolean*
      - x\_mapping
          - tex\_mapping.x\_mapping *enum*
      - y\_mapping
          - tex\_mapping.y\_mapping *enum*
      - z\_mapping
          - tex\_mapping.z\_mapping *enum*
      - Type
          - tex\_mapping.type *enum*
      - Projection
          - tex\_mapping.projection *enum*
      - Type
          - sky\_type *enum*
      - Sun Direction
          - sun\_direction *vector*
      - Turbidity
          - turbidity *float*
      - Ground Albedo
          - ground\_albedo *float*
      - Sun Disc
          - sun\_disc *boolean*
      - Sun Size
          - sun\_size *float*
      - Sun Intensity
          - sun\_intensity *float*
      - Sun Elevation
          - sun\_elevation *float*
      - Sun Rotation
          - sun\_rotation *float*
      - Altitude
          - altitude *float*
      - Air
          - air\_density *float*
      - Dust
          - dust\_density *float*
      - Ozone
          - ozone\_density *float*
      - Vector
          - vector *point*
  - Output Sockets
      - Color
          - color *color*

#### Subsurface Scattering

  - Input Sockets
      - Color
          - color *color*
      - Normal
          - normal *normal*
      - SurfaceMixWeight
          - surface\_mix\_weight *float*
      - Falloff
          - falloff *enum*
      - Scale
          - scale *float*
      - Radius
          - radius *vector*
      - Sharpness
          - sharpness *float*
      - Texture Blur
          - texture\_blur *float*
  - Output Sockets
      - BSSRDF
          - BSSRDF *closure*

#### Tangent

  - Input Sockets
      - Direction Type
          - direction\_type *enum*
      - Axis
          - axis *enum*
      - Attribute
          - attribute *string*
      - NormalIn
          - normal\_osl *normal*
  - Output Sockets
      - Tangent
          - tangent *normal*

#### Texture Coordinate

  - Input Sockets
      - From Dupli
          - from\_dupli *boolean*
      - Use Transform
          - use\_transform *boolean*
      - Object Transform
          - ob\_tfm *transform*
      - NormalIn
          - normal\_osl *normal*
  - Output Sockets
      - Generated
          - generated *point*
      - Normal
          - normal *normal*
      - UV
          - UV *point*
      - Object
          - object *point*
      - Camera
          - camera *point*
      - Window
          - window *point*
      - Reflection
          - reflection *normal*

#### Toon Bsdf

  - Input Sockets
      - Color
          - color *color*
      - Normal
          - normal *normal*
      - SurfaceMixWeight
          - surface\_mix\_weight *float*
      - Component
          - component *enum*
      - Size
          - size *float*
      - Smooth
          - smooth *float*
  - Output Sockets
      - BSDF
          - BSDF *closure*

#### Translucent Bsdf

  - Input Sockets
      - Color
          - color *color*
      - Normal
          - normal *normal*
      - SurfaceMixWeight
          - surface\_mix\_weight *float*
  - Output Sockets
      - BSDF
          - BSDF *closure*

#### Transparent Bsdf

  - Input Sockets
      - Color
          - color *color*
      - SurfaceMixWeight
          - surface\_mix\_weight *float*
  - Output Sockets
      - BSDF
          - BSDF *closure*

#### Uvmap

  - Input Sockets
      - attribute
          - attribute *string*
      - from dupli
          - from\_dupli *boolean*
  - Output Sockets
      - UV
          - UV *point*

#### Value

  - Input Sockets
      - Value
          - value *float*
  - Output Sockets
      - Value
          - value *float*

#### Vector Curves

  - Input Sockets
      - Curves
          - curves *array\_vector*
      - Min X
          - min\_x *float*
      - Max X
          - max\_x *float*
      - Fac
          - fac *float*
      - Vector
          - value *vector*
  - Output Sockets
      - Vector
          - value *vector*

#### Vector Displacement

  - Input Sockets
      - Space
          - space *enum*
      - Attribute
          - attribute *string*
      - Vector
          - vector *color*
      - Midlevel
          - midlevel *float*
      - Scale
          - scale *float*
  - Output Sockets
      - Displacement
          - displacement *vector*

#### Vector Math

  - Input Sockets
      - Type
          - math\_type *enum*
      - Vector1
          - vector1 *vector*
      - Vector2
          - vector2 *vector*
      - Vector3
          - vector3 *vector*
      - Scale
          - scale *float*
  - Output Sockets
      - Value
          - value *float*
      - Vector
          - vector *vector*

#### Vector Rotate

  - Input Sockets
      - Type
          - rotate\_type *enum*
      - Invert
          - invert *boolean*
      - Vector
          - vector *vector*
      - Rotation
          - rotation *point*
      - Center
          - center *point*
      - Axis
          - axis *vector*
      - Angle
          - angle *float*
  - Output Sockets
      - Vector
          - vector *vector*

#### Vector Transform

  - Input Sockets
      - Type
          - transform\_type *enum*
      - Convert From
          - convert\_from *enum*
      - Convert To
          - convert\_to *enum*
      - Vector
          - vector *vector*
  - Output Sockets
      - Vector
          - vector *vector*

#### Velvet Bsdf

  - Input Sockets
      - Color
          - color *color*
      - Normal
          - normal *normal*
      - SurfaceMixWeight
          - surface\_mix\_weight *float*
      - Sigma
          - sigma *float*
  - Output Sockets
      - BSDF
          - BSDF *closure*

#### Vertex Color

  - Input Sockets
      - Layer Name
          - layer\_name *string*
  - Output Sockets
      - Color
          - color *color*
      - Alpha
          - alpha *float*

#### Volume Info

  - Output Sockets
      - Color
          - color *color*
      - Density
          - density *float*
      - Flame
          - flame *float*
      - Temperature
          - temperature *float*

#### Voronoi Texture

  - Input Sockets
      - Translation
          - tex\_mapping.translation *point*
      - Rotation
          - tex\_mapping.rotation *vector*
      - Scale
          - tex\_mapping.scale *vector*
      - Min
          - tex\_mapping.min *vector*
      - Max
          - tex\_mapping.max *vector*
      - Use Min Max
          - tex\_mapping.use\_minmax *boolean*
      - x\_mapping
          - tex\_mapping.x\_mapping *enum*
      - y\_mapping
          - tex\_mapping.y\_mapping *enum*
      - z\_mapping
          - tex\_mapping.z\_mapping *enum*
      - Type
          - tex\_mapping.type *enum*
      - Projection
          - tex\_mapping.projection *enum*
      - Dimensions
          - dimensions *enum*
      - Distance Metric
          - metric *enum*
      - Feature
          - feature *enum*
      - Vector
          - vector *point*
      - W
          - w *float*
      - Scale
          - scale *float*
      - Smoothness
          - smoothness *float*
      - Exponent
          - exponent *float*
      - Randomness
          - randomness *float*
  - Output Sockets
      - Distance
          - distance *float*
      - Color
          - color *color*
      - Position
          - position *point*
      - W
          - w *float*
      - Radius
          - radius *float*

#### Wave Texture

  - Input Sockets
      - Translation
          - tex\_mapping.translation *point*
      - Rotation
          - tex\_mapping.rotation *vector*
      - Scale
          - tex\_mapping.scale *vector*
      - Min
          - tex\_mapping.min *vector*
      - Max
          - tex\_mapping.max *vector*
      - Use Min Max
          - tex\_mapping.use\_minmax *boolean*
      - x\_mapping
          - tex\_mapping.x\_mapping *enum*
      - y\_mapping
          - tex\_mapping.y\_mapping *enum*
      - z\_mapping
          - tex\_mapping.z\_mapping *enum*
      - Type
          - tex\_mapping.type *enum*
      - Projection
          - tex\_mapping.projection *enum*
      - Type
          - wave\_type *enum*
      - Bands Direction
          - bands\_direction *enum*
      - Rings Direction
          - rings\_direction *enum*
      - Profile
          - profile *enum*
      - Vector
          - vector *point*
      - Scale
          - scale *float*
      - Distortion
          - distortion *float*
      - Detail
          - detail *float*
      - Detail Scale
          - detail\_scale *float*
      - Detail Roughness
          - detail\_roughness *float*
      - Phase Offset
          - phase *float*
  - Output Sockets
      - Color
          - color *color*
      - Fac
          - fac *float*

#### Wavelength

  - Input Sockets
      - Wavelength
          - wavelength *float*
  - Output Sockets
      - Color
          - color *color*

#### White Noise Texture

  - Input Sockets
      - Dimensions
          - dimensions *enum*
      - Vector
          - vector *point*
      - W
          - w *float*
  - Output Sockets
      - Value
          - value *float*
      - Color
          - color *color*

#### Wireframe

  - Input Sockets
      - Use Pixel Size
          - use\_pixel\_size *boolean*
      - Size
          - size *float*
  - Output Sockets
      - Fac
          - fac *float*
