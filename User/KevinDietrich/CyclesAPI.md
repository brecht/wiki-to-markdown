Cycles node socket API refactor

In an effort to improve the Cycles API and to better detect how a Node
has changed, Node socket properties are now private and should be
queried or modified through specific methods. For each Node socket, the
following methods are defined:

  - get\_x
  - set\_x
  - x\_is\_modified
  - tag\_x\_modified

Where x is the name of the socket. This name, for scalar or array
sockets, is the same as the current class member, so Mesh::verts will
give Mesh::get\_verts, Mesh::set\_verts, etc. For class members that are
structures, like Camera::border (of BoundBox2D type), the name is the
concatenation of the names of the property and its related member using
snake case, so camera.border.x becomes camera.get\_border\_x(), etc.

The set\_x methods will check if the value is different than the one
previously set, and if so, tag the socket as modified, which can be
checked using the x\_is\_modified() method. Additionnaly,
tag\_x\_modified() can be used to manually tag a socket as modified.

Some other class members were removed from public access, while other
were transformed into sockets where it made sense. Below is an
exhaustive list of changes for specific Nodes:

Background

-----

  - Background::need\_update was removed, Background::is\_modified()
    should be used instead.
  - Background::modified(Background) was removed,
    Background::is\_modified() should be used instead.

Camera

-----

  - Camera::type was renamed to Camera::camera\_type
  - Camera::width, Camera::height, and Camera::resolution have been
    removed from public access, and are not accessible through sockets.
    Camera::set\_screen\_size\_and\_resolution can be used to modify
    those properties.
  - Camera::modified(Camera) and Camera::motion\_modified(Camera) were
    removed, checking for this should be done via the related sockets'
    update flag status.
  - Camera::tag\_update() was removed, Camera::tag\_modified() should be
    used instead, if the intent is to tag every socket as dirty.
  - Camera::need\_update was removed, Camera::is\_modified() should be
    used instead.
  - new sockets are available for the following properties:
    viewport\_camera\_border, full\_width, full\_height,
    use\_perspective\_motion

ClampNode

-----

  - ClampNode::type was renamed to ClampNode::clamp\_type

EnvironmentTextureNode

-----

  - added a socket for the animated property

Film

-----

  - Film::need\_update was removed, Film::is\_modified() should be used
    instead.
  - Film::modified(Film) was removed, Film::is\_modified() should be
    used instead.
  - Film::tag\_update(Scene) was removed, Film::tag\_modified().
  - new sockets are available for the following properties:
    use\_light\_visibility, display\_pass, cryptomatte\_passes,
    cryptomatte\_depth

Geometry

-----

  - Geometry::type was renamed to Geometry::geometry\_type
  - Geometry::need\_update was removed, Geometry::is\_modified() should
    be used instead.
  - Geometry::used\_shaders is now an array\<Node \*\> (used to be
    vector\<Shader \*\>)

GradientTextureNode

-----

  - GradientTextureNode::type was renamed to
    GradientTextureNode::gradient\_type

ImageTextureNode

-----

  - new sockets are available for the following properties: tiles,
    animated

Integrator

-----

  - Integrator::need\_update was removed, Integrator::is\_modified()
    should be used instead.
  - Integrator::modified(Integrator) was removed,
    Integrator::is\_modified() should be used instead.

Light

-----

  - Light::type was renamed to Light::light\_type

MappingNode

-----

  - MappingNode::type was renamed to MappingNode::mapping\_type

MapRangeNode

-----

  - MapRangeNode::type was renamed to MapRangeNode::range\_type

Mesh

-----

  - Mesh::SubdParams was removed from public access, and is replaced by
    the new sockets: subd\_dicing\_rate, subd\_max\_level, and
    subd\_objecttoworld.
  - Mesh::SubdEgdeCrease was removed from public access, and is replaced
    by the new sockets: subd\_creases\_edge, subd\_creases\_weight
  - Mesh::SubdFaces was removed from public access, and is replaced by
    the new sockets: subd\_start\_corner, subd\_num\_corners,
    subd\_shader, subd\_smooth, subd\_ptex\_offset
  - additionnal sockets are available for the following properties:
    triangle\_patch, vert\_patch\_uv, and num\_ngons

MixNode

-----

  - MixNode::type was renamed to MixNode::mix\_type

MathNode

-----

  - MathNode::type was renamed to MathNode::math\_type

Object

-----

  - added sockets for the following properties: asset\_name,
    particle\_system, particle\_index

Shader

-----

  - added a socket for the pass\_id property

SkyTextureNode

-----

  - SkyTextureNode::type was renamed to SkyTextureNode::sky\_type

VectorMathNode

-----

  - VectorMathNode::type was renamed to VectorMathNode::math\_type

VectorRotateNode

-----

  - VectorRotateNode::type was renamed to VectorRotateNode::rotate\_type

VectorTransformNode

-----

  - VectorTransformNode::type was renamed to
    VectorTransformNode::transform\_type

WaveTextureNode

-----

  - WaveTextureNode::type was renamed to WaveTextureNode::wave\_type
