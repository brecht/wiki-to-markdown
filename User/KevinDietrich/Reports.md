# Weekly Reports 2020

## Week 1 - 6-10 July 2020

Week mostly spent getting acquainted to the new Blender code base, as
the project was not really defined yet.

  - Patches
      - Worked on updating the patch to render deformation motion blur
        in Cycles from Alembic caches
        ([D2388](http://developer.blender.org/D2388))

<!-- end list -->

  - Bug Tracker/Fixes
      - Started working on a fix for the artifacts showing in volume
        rendering in Cycles
        ([\#77882](http://developer.blender.org/T77882)).

## Week 2 - 13-17 July 2020

Had a meeting with the stakeholder from the project, which defined some
goals and key areas to look at. Started mocking some APIs and did some
exploratory work in Cycles to better understand how scene and objects
updates work.

  - Patches
      - [D2388](http://developer.blender.org/D2388), replied to reviews
        from Brecht and Sybren.
      - Updated [D3089](http://developer.blender.org/D3089) to current
        master in a private branch to explore what a Cycles Procedural
        API would require.

<!-- end list -->

  - Bug Tracker/Fixes
      - Fix [\#78883](http://developer.blender.org/T78883), unitialized
        memory when manually adding a curve point
        ([D8283](http://developer.blender.org/D8283), committed by
        Campbell)
      - Fix (unreported) memory leak in the fluid bake operators
        happening when path validation fails.
        ([rB2e8a78f4](https://projects.blender.org/blender/blender/commit/2e8a78f4d105))
      - Closed [\#77854](http://developer.blender.org/T77854) as already
        resolved

## Week 3 - 20-24 July 2020

Worked on proposals for changes in Cycles according to the project
requirements. Made some experiments for changes in the API, and some
others to better understand how Cycles work.

  - Proposals
      - [\#79131](http://developer.blender.org/T79131) Cycles/Blender
        API redesign
      - [\#79174](http://developer.blender.org/T79174) Cycles Procedural
        API and faster scene update

<!-- end list -->

  - Patches
      - [D2388](http://developer.blender.org/D2388), final changes from
        Sybren's review, could go in master this week, did not have time
        to push for 2.90 as documentation is needed

## Week 4 - 27-31 July 2020

Work was mostly exploratory, had some software/development environment
issues trying to install CUDA. Also worked on a patch to compress
normals in Cycles (not shared as it needs proper testing, and is not
really part of the project).

  - Patches
      - Fix volume artifacts in Cycles
        [D8401](http://developer.blender.org/D8401)

## Week 5 - 3-7 August 2020

  - Patches
      - improving the bug fix for volume renders and final patch
        [D8401](http://developer.blender.org/D8401)

<!-- end list -->

  - Committed patches
      - rBb5dcf746369e: Cycles: add support for rendering deformation
        motion blur from Alembic caches.

## Week 6 - 8-14 August 2020

Improvements to the Alembic procedural node and API refactor to fix
infinite loops during viewport render.

  - Patches
      - Node ownership system
        [D8540](http://developer.blender.org/D8540)
      - Scene/Session refactor
        [D8590](http://developer.blender.org/D8590)
      - make Volume a Node [D8538](http://developer.blender.org/D8538)
      - make Pass a Node [D8591](http://developer.blender.org/D8591)

<!-- end list -->

  - Bug Fixes :
      - [\#77882](http://developer.blender.org/T77882): artifacts
        rendering OpenVDB volumes with multiple grids in
        Cycles.[rB9280fb19](https://projects.blender.org/blender/blender/commit/9280fb19e4ff)

## Week 7 - 17-21 August 2020

Modified Alembic procedural according to stakeholder's wants.

  - Patches
      - add update flags to Node and SocketType
        [D8644](http://developer.blender.org/D8644)
      - make Image a Node [D8649](http://developer.blender.org/D8649)
      - work on Alembic procedural
        [D3089](http://developer.blender.org/D3089)
      - (review update) Node ownership system
        [D8540](http://developer.blender.org/D8540)

<!-- end list -->

  - Committed patches
      - make Pass a Node
        ([rB1da06850](https://projects.blender.org/blender/blender/commit/1da0685076bb))
      - make Volume a Node
        ([rBaa1e4baa](https://projects.blender.org/blender/blender/commit/aa1e4baa22b3))

## Week 8 24-28 August 2020

  - Patches
      - make DeviceInfo a node
        [D8750](http://developer.blender.org/D8750)
      - make SessionParams a node
        [D8751](http://developer.blender.org/D8751)
      - work on profiling the scene update
        [D8596](http://developer.blender.org/D8596)
      - (review update) update flags
        [D8644](http://developer.blender.org/D8644)

<!-- end list -->

  - committed patches :
      - Node Ownership system for protecting nodes against unwanted
        deletions
        ([rB429afe0c](https://projects.blender.org/blender/blender/commit/429afe0c626))

<!-- end list -->

  - Bug fixes :
      - [\#80076](http://developer.blender.org/T80076): Cycles Alembic
        Motion Blur Problem
        ([rB53035093](https://projects.blender.org/blender/blender/commit/53035093547))

## Week 9 30 August - 4 September 2020

(Forgot to do the report, I do not remember everything.)

Investigated some bug report about motion blur issues with Alembic
caches.

## Week 10 7-11 September 2020

(Forgot to do the report, I do not remember everything.)

  - General
      - Work on the Alembic procedural, and using the new socket API to
        detect issues

## Week 11 14-18 September 2020

(Forgot to do the report, I do not remember everything.)

  - General
      - Cleaned up the socket API patch
        ([D8544](http://developer.blender.org/D8544))
      - Profiled the Alembic procedural against master
      - Work on the Alembic procedural, some optimizations

<!-- end list -->

  - Patches
      - Updated [:Template:PhadDiff](:Template:PhadDiff)
        Cycles: make DeviceInfo a Node.

## Week 12 21-25 September 2020

(Forgot to do the report, I do not remember everything.)

  - Patches
      - Socket API refactor
        ([D8544](http://developer.blender.org/D8544)), made changes
        according to code review
      - Update statistics ([D8596](http://developer.blender.org/D8596)),
        made changes according to code review

<!-- end list -->

  - Commits
      - Cycles: add update flags to Node and SocketType
        ([rB62620168](https://projects.blender.org/blender/blender/commit/626201683ec0))

## Week 13 28 September - 2 October 2020

(Forgot to do the report, I do not remember everything.)

  - General
      - modified Alembic importer to optionnally add a modifier to every
        objects, so we can use the Cycles procedural for everything,
        improves synchronisation speed
      - start addressing concerns for the socket API review, fixed a few
        bugs found in the test suite

<!-- end list -->

  - Commits
      - Cycles: add time statistics to scene update
        [rBedd11645](https://projects.blender.org/blender/blender/commit/edd1164575fe)

## Week 14 5 - 9 October 2020

  - General
      - shared Blender files for Alembic testing with project's
        stakeholder
      - experimented with rebuilding the BVH only every N frame,
        refitting it otherwise
      - experimental refactor of the device memory update to take socket
        flags into consideration, to avoid copying and sending to the
        device unchanged data

<!-- end list -->

  - Patches
      - Socket API refactor
        ([D8544](http://developer.blender.org/D8544)), made changes
        according to code review

<!-- end list -->

  - Fixes :
      - type issues in the Cycles introspection code

## Week 15 12 - 16 October 2020

(Forgot to do the report, I do not remember everything.)

  - General
      - continue device update refactor
      - work on the RenderDevice node
      - work on OptiX BVH refit

<!-- end list -->

  - Socket API refactor ([D8544](http://developer.blender.org/D8544))
      - Fix remaining issues with the patch

<!-- end list -->

  - Fixes
      - missing ShaderNode ownership in render\_graph\_finalize\_test
        [rBc82d167d](https://projects.blender.org/blender/blender/commit/c82d167d814f)

## Week 16 19 - 23 October 2020

Had some personal stuff to do, so lost quite a bit of time.

  - Socket API refactor ([D8544](http://developer.blender.org/D8544))
      - Work on some developer/implementer level documentation for the
        socket API refactor [D8544](http://developer.blender.org/D8544)
      - Fix some regressions regarding the detection of tesselation

<!-- end list -->

  - Cycles API
      - Encapsulated most of the objects used throughout the Blender
        exporter

<!-- end list -->

  - Patches
      - Cycles: add support for BVH refit in OptiX.
        [D9353](http://developer.blender.org/D9353)

## Week 17 26 - 30 October 2020

  - General
      - Fixed bugs in Socket API patch after bad merge
        ([D8544](http://developer.blender.org/D8544))

<!-- end list -->

  - Patches
      - [D9353](http://developer.blender.org/D9353) Reply to code review
        for Cycles OptiX BVH refit patch

<!-- end list -->

  - Bug Fixes
      - Fix [\#82129](http://developer.blender.org/T82129): Cycles
        "Persistent Images" incorrectly retains scene data
        ([rB8c3d42bd](https://projects.blender.org/blender/blender/commit/8c3d42bd0f01))

## Week 18 2 - 6 November 2020

  - General :
      - continued working on optimizing Cycles scene updates

<!-- end list -->

  - Commits
      - Cycles: add support for BVH refit in OptiX
        ([rB57d1aea6](https://projects.blender.org/blender/blender/commit/57d1aea64f0c))
      - Cycles API: encapsulate Node socket members
        ([rB31a620b9](https://projects.blender.org/blender/blender/commit/31a620b9420c))

<!-- end list -->

  - Fixes :
      - Fix [\#82428](http://developer.blender.org/T82428): Cycles
        crashes when building volume meshes
        ([rB6a2a6fa5](https://projects.blender.org/blender/blender/commit/6a2a6fa5143e))
      - Fix race conditions following API changes
        ([rBc937f9c4](https://projects.blender.org/blender/blender/commit/c937f9c4e76c),
        [rB17ccda4f](https://projects.blender.org/blender/blender/commit/17ccda4fe1c4))

## Week 19 9 - 13 November 2020

  - General
      - worked on finalizing and stabilizing scene updates
        optimizations.

<!-- end list -->

  - Fixes
      - Fix [\#82617](http://developer.blender.org/T82617): artifacts in
        Cycles viewport when changing subdivision attributes
        ([rB88bb29de](https://projects.blender.org/blender/blender/commit/88bb29dea668))
      - Fix [\#82561](http://developer.blender.org/T82561): shader
        compilation crashes in OSL
        ([rBa284e559](https://projects.blender.org/blender/blender/commit/a284e559b90e))
      - Cycles: fix compilation of OSL shaders following API change
        ([rBb980cd16](https://projects.blender.org/blender/blender/commit/b980cd163a9d))
      - Cycles: fix Node::tag\_modified not setting modified flag's
        upper bits
        ([rB021c4016](https://projects.blender.org/blender/blender/commit/021c40167dea))

<!-- end list -->

  - Patches
      - [D9555](http://developer.blender.org/D9555): Cycles: optimize
        device updates.

## Week 20 16 - 20 November 2020

Continued work on Alembic procedural and scene updates. Some bug
triaging. Started reviewing patches in the tracker.

  - Fixes
      - Fix [\#81983](http://developer.blender.org/T81983): volume tiles
        missing in Cycles renders
        ([rB13ec512f](https://projects.blender.org/blender/blender/commit/13ec512f4b9a))

<!-- end list -->

  - Patches
      - [D9597](http://developer.blender.org/D9597): Cycles: add a
        parameter to control the maximum number of BVH refits.

<!-- end list -->

  - Reviews:
      - [D8896](http://developer.blender.org/D8896): Fix T75539: Cycles.
        Update geometry when switching displacements.

## Week 21 23 - 27 November

Continued work on Alembic procedural and scene updates. Some bug
triaging.

## Week 22 30 November - 4 December

  - Fixes :
      - Fix [\#83300](http://developer.blender.org/T83300): constant
        scene refreshing in Cycles with empty volumes
        ([rB84451f89](https://projects.blender.org/blender/blender/commit/84451f89f5c1))

## Week 23 7 December - 11 December

  - Patches :
      - [D8750](http://developer.blender.org/D8750): Cycles: make
        DeviceInfo a Node.
      - [D3089](http://developer.blender.org/D3089): Cycles: add
        experimental support for an Alembic procedural (first version
        for final review)

<!-- end list -->

  - Fixes :
      - Fix [\#82852](http://developer.blender.org/T82852): Cycles
        crashes when editing a mesh with adaptive subdivision
        ([rB548e9624](https://projects.blender.org/blender/blender/commit/548e9624d0fe))

<!-- end list -->

  - Reviews:
      - [D9718](http://developer.blender.org/D9718) Cycles: Add CPU+GPU
        rendering support with OptiX.
      - [D8896](http://developer.blender.org/D8896): Fix T75539: Cycles.
        Update geometry when switching displacements.

## Week 24 14 December - 18 December

  - Reviews:
      - [D9887](http://developer.blender.org/D9887) Cycles: pointcloud
        geometry type.

<!-- end list -->

  - Patches:
      - [D9555](http://developer.blender.org/D9555) Refactor device
        update according to code review
      - [D3089](http://developer.blender.org/D3089) Updated patch with
        improvements and fixes

<!-- end list -->

  - Commits:
      - Fix [\#75539](http://developer.blender.org/T75539): Cycles
        missing geometry update when switching displacement method
        ([rB68d5ad99](https://projects.blender.org/blender/blender/commit/68d5ad998393))
        (on behalf of Joan Bonet Orantos)

## Week 25 21 December - 25 December

Slow week due to Christmas. Fixes in the device update refactor patch
([D9555](http://developer.blender.org/D9555)), also tried to figure out
a complicated newly found bug in this patch. Some bug
triaging/investigating.

## Week 26 28 December 2020 - 1 January 2021

Vacancy.
