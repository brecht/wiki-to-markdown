# Weekly Reports 2023

Unless specific mention, all my work is related to Grease Pencil 3
(GPv3).

## August 21 - August 25 (week 9)

Last week at Blender HQ. I finished the patch for soft eraser. Many
patches are pending for review until Falk comes back from his holidays.

Merged :

`- Display layer groups in grease pencil dopesheet (`[`111015`](https://projects.blender.org/blender/blender/pulls/111015)`).`  
`- Fix: Stroke Eraser does not work with one-point strokes (`[`111387`](https://projects.blender.org/blender/blender/pulls/111387)`).`  
`- Refactor code for the hard eraser (`[`111390`](https://projects.blender.org/blender/blender/pulls/111390)`).`

In review:

`- Soft Eraser tool (`[`110310`](https://projects.blender.org/blender/blender/pulls/110310)`).`  
`- Duplicate keyframes (`[`111051`](https://projects.blender.org/blender/blender/pulls/111051)`).`  
`- Frame All/Selected keyframes (`[`111480`](https://projects.blender.org/blender/blender/pulls/111480)`).`  
`- Jump to Selected/Previous/Next keyframe for grease pencil frames (`[`111476`](https://projects.blender.org/blender/blender/pulls/111476)`).`  
`- Set keyframe type (`[`111472`](https://projects.blender.org/blender/blender/pulls/111472)`).`  
`- Snap selected grease pencil frames (`[`111507`](https://projects.blender.org/blender/blender/pulls/111507)`). `  
`- Mirror grease pencil frames (`[`111511`](https://projects.blender.org/blender/blender/pulls/111511)`). `  
`- Frame selected channels for grease pencil (`[`111512`](https://projects.blender.org/blender/blender/pulls/111512)`).`

## August 14 - August 18 (week 8)

This week I worked on an improved version of the soft eraser, which is
still in review, but significantly changed. Plus, I addressed the
changes requested on 111015 and 111051 which should be merged soon.

Merged :

`- Fix layer renaming synchronization from the dopesheet (`[`111038`](https://projects.blender.org/blender/blender/pulls/111038)`) `

In review:

`- Soft Eraser tool (`[`110310`](https://projects.blender.org/blender/blender/pulls/110310)`).`  
`- Display layer groups in grease pencil dopesheet (`[`111015`](https://projects.blender.org/blender/blender/pulls/111015)`).`  
`- Duplicate keyframes (`[`111051`](https://projects.blender.org/blender/blender/pulls/111051)`).`

## August 7 - August 11 (week 7)

Merged :

`- Fix floating-point error on the hard eraser (`[`110801`](https://projects.blender.org/blender/blender/pulls/110801)`).`  
`- Transform action for grease pencil frames (`[`110743`](https://projects.blender.org/blender/blender/pulls/110743)`).`  
`- Display layer properties in animation channels (`[`110991`](https://projects.blender.org/blender/blender/pulls/110991)`)`  
`- Include summary and datablock keyframes in the grease pencil dopesheet (`[`110962`](https://projects.blender.org/blender/blender/pulls/110962)`)`

In review:

`- Soft Eraser tool (`[`110310`](https://projects.blender.org/blender/blender/pulls/110310)`).`  
`- Display layer groups in grease pencil dopesheet (`[`111015`](https://projects.blender.org/blender/blender/pulls/111015)`).`  
`- Duplicate keyframes (`[`111051`](https://projects.blender.org/blender/blender/pulls/111051)`).`

## July 31 - August 4 (week 6)

Merged :

`- Insert Keyframes (`[`110649`](https://projects.blender.org/blender/blender/pulls/110649)`).`  
`- Delete Keyframes (`[`110746`](https://projects.blender.org/blender/blender/pulls/110746)`).`

In review:

`- Soft Eraser tool (`[`110310`](https://projects.blender.org/blender/blender/pulls/110310)`).`  
`- Fix floating-point error on the hard eraser (`[`110801`](https://projects.blender.org/blender/blender/pulls/110801)`).`  
`- Transform action for grease pencil frames(`[`110743`](https://projects.blender.org/blender/blender/pulls/110743)`).`

## July 25 - July 28 (week 5)

I've created a todo list for the dopesheet support :
[110056](https://projects.blender.org/blender/blender/issues/110056).

Some of them are now community tasks :

`- Select box (`[`110521`](https://projects.blender.org/blender/blender/issues/110521)`),`  
`- Select circle/lasso (`[`110522`](https://projects.blender.org/blender/blender/issues/110522)`).`

Merged :

`- Stroke Eraser tool (`[`110304`](https://projects.blender.org/blender/blender/pulls/110304)`).`  
`- Click selection for frames (`[`110492`](https://projects.blender.org/blender/blender/pulls/110492)`).`  
`- Column-based selection in the dopesheet (`[`110523`](https://projects.blender.org/blender/blender/pulls/110523)`).`  
`- Filtering grease pencil channels in the dopesheet (`[`110484`](https://projects.blender.org/blender/blender/pulls/110484)`).`  
`- Some smaller commits for cleanup and/or refactor (`[`110525`](https://projects.blender.org/blender/blender/pulls/110525)`, `[`110460`](https://projects.blender.org/blender/blender/pulls/110460)`, and `[`110486`](https://projects.blender.org/blender/blender/pulls/110486)`)`

In review:

`- Soft Eraser tool (`[`110310`](https://projects.blender.org/blender/blender/pulls/110310)`).`

## July 17 - July 21 (week 4)

Merged :

`- Smoothing algorithm (`[`109635`](https://projects.blender.org/blender/blender/pulls/109635)`).`  
`- Hard Eraser tool (`[`110063`](https://projects.blender.org/blender/blender/pulls/110063)`).`

In review:

`- Stroke Eraser tool (`[`110304`](https://projects.blender.org/blender/blender/pulls/110304)`).`  
`- Soft Eraser tool (`[`110310`](https://projects.blender.org/blender/blender/pulls/110310)`).`

## July 10 - July 13 (week 3)

Merged :

`- Initial Dopesheet Support (`[`108807`](https://projects.blender.org/blender/blender/pulls/108807)`).`  
`- Accessors for opacity and radius (`[`109733`](https://projects.blender.org/blender/blender/pulls/109733)`).`

In review :

`- Smoothing algorithm (`[`109635`](https://projects.blender.org/blender/blender/pulls/109635)`).`  
`- Hard Eraser tool (`[`110063`](https://projects.blender.org/blender/blender/pulls/110063)`).`

## July 3 - July 7 (week 2)

Initial Dopesheet Support
([108807](https://projects.blender.org/blender/blender/pulls/108807))
done, pending for review.

The smoothing algorithm
([109635](https://projects.blender.org/blender/blender/pulls/109635))
works, but may need some additions in the curves API
([109733](https://projects.blender.org/blender/blender/pulls/109733)).
Both PR are pending for review.

Started working on the eraser tool, which was not as easy as one may
think. Still WIP.

## June 28 - June 30 (week 1)

First days at Blender.

Almost done with the Initial Dopesheet Support
[\#108807](https://projects.blender.org/blender/blender/pulls/108807).

Started working on the smoothing algorithm.
