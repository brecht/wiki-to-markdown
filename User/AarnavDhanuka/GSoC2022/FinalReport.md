## Summary

In this project, significant progress was achieved in the development of
a dedicated soft body solver in Blender. As part of the project a
volumetric remesher was implemented in the Blender code-base. The
tetrahedralization also gives the option for addition of internal points
to allow for more uniform tetrahedralization and to represent the object
in 3 dimensions more accurately which can be controlled using \`Internal
Resolution\` parameter. Additionally a basic flow for soft body
simulations using Extended Position Based Dynamics (XPBD) was
implemented including distance and volume constraint. The stiffness of
the object can be controlled using compliance which is implemented as
inverse of stiffness.

While there are still a few advancements required to deploy the solver
to the main branch, this project sets the first stone to a dedicated
soft body solver using XPBD.

## Volumetric Remeshing

Before simulations can be performed, the surface meshes need to be
converted to volumetric meshes, in particular tetrahedral mesh. To
perform this conversion, a modified version of Incremental Delaunay
Tetrahedralization was implemented. Since Blender does not have data
structures to store and work with volumetric meshes, the tetrahedron
information was stored as quadrilateral planes. This assumption works
well since all vertices of a tetrahedron are connected, thus eliminating
the requirement to store edge information for each tetrahedron
primitive.

Delaunay method works by adding vertices incrementally and briefly
follows the steps:  
1\) Find tetrahedron containing the point  
2\) Find all tetrahedrons that violate Delaunay Condition  
3\) Delete violating tetrahedrons and make new tetrahedrons centered at
the new point  
4\) Update the neighbors  
We start by defining the Delaunay condition for a tetrahedron and an
external point. The Delaunay condition is satisfied for this pair if the
point lies outside the circum-sphere of the tetrahedron.

For the first step to find the tetrahedron that contains the new point
to be added, a ray is cast from center of the current tetrahedron to the
point. The next tetrahedron to be tested is the neighbor of current
tetrahedron that shares the face intersected by the cast ray.

Once the containing tetrahedron is found, we recursively check neighbors
of all violating tetrahedrons whether they satisfy the Delaunay
Condition and add the ones that don’t to the stack to subsequently check
their neighbors. In addition, all tetrahedrons that are tested for
satisfying the condition are marked.

Once all the violating tetrahedrons are found, we need to take all
boundary faces - outermost faces or more technically, faces that either
don’t have a neighboring tetrahedron or that are common to violating and
non-violating tetrahedrons. Finally new tetrahedra are constructed by
joining boundary faces with the new point.

Finally the neighbors for all the new tetrahedrons are updated. This can
be divided into 2 parts, updating information around boundary faces and
updating information for internal faces. For boundary, the non-violating
tetrahedron’s neighbor is set to the new tetrahedron formed using the
boundary face. Same is done for the violating tetrahedron. For the
internal faces, we make use of the fact that each face is shared by only
2 tetrahedrons and can be uniquely represented using it’s 3 vertices.

Since the algorithm makes tetrahedrons using existing vertices, to allow
for more consistent and uniform tetrahedralization, the option of adding
points inside the structure has also been added in the form of interior
resolution. Additionally to visualize the tetrahedralization, the option
to display all faces individually was also given where each tetrahedron
is scaled down so that it can be comfortably analyzed for correctness.

![Suzanne tetrahedralized and represented with individual tetrahedrons
scaled down to a factor of
0.8](../../../images/Suzanne_tetrahedralized.jpg
"Suzanne tetrahedralized and represented with individual tetrahedrons scaled down to a factor of 0.8")

## Extended Position Based Dynamics (XPBD)

To perform the actual simulations, Extended Position Based
Dynamics(XPBD) was implemented. As opposed to force based methods that
calculate the amount of force on vertices and determine the motion of
individual vertices, position based methods move the vertices in the
direction of their current velocity and then enforce hard constraints as
necessary.

Since the current soft-body solver was just a cloth simulation solver
with high surface tension, the data structures had to be changed
completely to store tetrahedron information. New data-structures
\`BodyTet\` and \`BodyEdge\` were defined while the existing
data-structures \`SoftBody\` and \`BodyPoint\` were completely
rewritten. This required subsequent changes to be made DNA and RNA code
for soft body.

The various intrinsic properties of an object are implemented in the
form of constraint functions in XPBD. Keeping that in mind, basic
constraints like distance conservation constraint (distance between
connected vertices is preserved) and volume conservation constraint
(volume of individual tetrahedrons must be preserved) were implemented.
As a general rule these constraints must ensure that the linear momentum
and angular momentum of the object is conserved.

To ensure that the center of mass of the object is in the middle of the
object and the density is uniform in the body, the mass of vertices has
been set as sum of inverse of volume of tetrahedrons that they are a
part of. Because of this, the mass of vertices is dependent on the
resolution of tetrahedralization and eventual volume of individual
tetrahedrons.

The stiffness of the object can be controlled using the edge and volume
compliance values which is inversely proportional to the stiffness. Thus
a compliance value of 0 gives the object infinite stiffness causing the
object to behave like a rigid body. An important thing to note here is
how the masses for vertices have been set. The compliance values for
specific softness can vary in magnitude of order from object to object
or for that matter even same object with different internal resolutions.
The reason being that the compliance values are closely tied to the mass
of vertices. ![Tetrahedralized cube bouncing on imaginzary x-y
plane](../../../images/Screencast-12-08-22-092938-am-ist_KrOxvmSo.gif
"Tetrahedralized cube bouncing on imaginzary x-y plane")

## Future Work

1\) **Make a dedicated modifier for tetrahedralization**: The current
code for tetrahedralization was added to a random modifier file
(\`MOD\_weld.cc\`) since it was supposed to be a part of simulator and
this allowed for easier debugging but it turned out to be a request by
many and generally since the code is in place, a separate modifier for
the same only helps

2\) **Shift tetrahedralization code** : Shift tetrahedralization to part
where mesh is converted to soft-body data-structures. While this is
contradicting to the first point, this will add the layer of abstraction
where the users don’t see the individual tetrahedrons stored as quads
which distort the view and don’t allow for practical usage of the
simulator since there’s not proper way to go back to the original object
once the modifier has been applied. The first point will simply allow
users to make use of the tetrahedralization whenever required outside of
simulations.

3\) **Implement self collisions and collision** : This is fairly self
explanatory, a part of the project scope and undoubtedly one of the most
important parts of any simulation engines is collision and self
collision that could not be completed due to various challenges.

4\) **Vertex groups** : Add vertex group options that can be used for
multiple purposes including pinning vertices and changing mass of
certain regions of the soft-body

5\) **Cloth simulation** : Make option for performing cloth simulation
using the current solver. This would require additional bending
constraint to be implemented. The bending constraint would require the
initial angle between neighboring faces to be saved which is not a part
of the current data structures defined. An easy fix to this would be to
store the initial normal vector of all planes and use them to calculate
the initial angle in subsequent steps.

6\) **Refactor soft body code** : Define new flow for soft-body
data-structure. Currently the soft-body data-structure is present inside
the \`object\` data-structure. This makes the code less readable and a
little more convoluted to navigate around. Would prefer structuring the
data like it’s structured for cloth right now.

## Acknowledgements

Thank you so much Sebastian Parborg for being my rubber duck. You were
always extremely helpful and patient and I really look forward to
working a lot more with you. I would also like to thank Matthias Müller
whose work have been implemented throughout the project.
