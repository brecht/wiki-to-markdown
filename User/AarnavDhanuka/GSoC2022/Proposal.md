## Project Title

Soft Bodies Simulation using Extended Position-Based Dynamics (XPBD)

## Name

Aarnav Dhanuka

## Contact

Email : arcticblazer.headshot@gmail.com

LinkedIn : [Aarnav Dhanuka |
LinkedIn](https://www.linkedin.com/in/aarnav-dhanuka-663a62148/)

Blender.chat : arcticblazer

Github : [Aarnav Dhanuka | Github](https://github.com/arcticblazer)

## Synopsis

We are surrounded by deformable objects that can't be accurately
simulated in Blender yet. It is proposed in this project to build a new
solver that would take as input a surface mesh and simulate the object
as a soft body. The goal is to build a modular pipeline that will
convert a surface mesh into a volumetric tetrahedral mesh using the
"Incremental Delaunay Method" that will be passed to the "Extended
Position-Based Dynamics (XPBD)" solver to simulate soft bodies.

## Benefits

While for artists, this project will give the ability to simulate
various deformable objects like muscles quickly and accurately, for
developers, the project will introduce a remesher to convert surface
meshes into tetrahedral volumetric meshes. In addition, it will add a
new customizable solver system that will be used to simulate soft bodies
and can also be used to simulate cloths in a significantly faster
manner. The project will also allow for future development in physics
simulations to write specific constraint functions for different cases.

## Deliverables

The final product would be a general solver pipeline to perform
soft-body simulations. This pipeline starts with a remesher to convert
the input surface mesh to a volumetric tetrahedral mesh. This is
followed by a general solver that can be invoked with various constraint
functions specific to the object to be simulated. Constraint functions
required to simulate cloths and softbodies, including distance, bending
and volume constraints and collision control, would be implemented.

## Project Details

Currently, soft bodies are simulated in Blender using cloth simulation,
which is, in turn, implemented using various springs. To accurately
simulate a cloth, the internal forces of the cloth are simulated using
different types of springs, while the soft bodies are simulated by
making stiff surface springs. This makes the old method complex and slow
for cloth simulation since all forces need to be recalculated at each
step, while the simulations are inaccurate for soft bodies.

The method proposed to be implemented, “XPBD: Position-Based Simulation
of Compliant Constrained Dynamics” by Miles Macklin, Matthias M ̈uller
and Nuttapong Chentanez\[1\], can act as a replacement to the current
cloth solver and introduce a robust and fast Soft Body Solver.

As opposed to the force-based simulation, XPBD enforces hard constraints
like length conservation between adjacent vertices (distance constraint)
and tetrahedral volume conservation (volume constraint). These
constraints are enforced by simply moving the vertices in the direction
of the constraint solution. The method is very fast since this is done
using first-order Euler integration. To quantify the model's accuracy,
the authors simulated a chain with 20 particles falling under gravity
and compared it to the analytic solution. They found that the error was
under 6%, 2%, and 0.5% for 50, 100, and 1000 iterations
respectively\[1\].

![Fig - A time-lapse view of a hanging chain of 20 particles falling
under gravity. Left: Reference Newton solver. Right: XPBD with 50
iterations. \[1](../../../images/Proposal_image1.png
"Fig - A time-lapse view of a hanging chain of 20 particles falling under gravity. Left: Reference Newton solver. Right: XPBD with 50 iterations. [1")\]

However, it must be noted that while the method is very accurate in a
lot of cases, it is not as precise as the more complex methods like the
Finite Element Method (FEM). However, the simulation speed is very fast,
while the simulations are visually indistinguishable from the analytic
solution. The authors displayed the method's speed by simulating various
complex structures in real-time on a browser using just Javascript.

![Fig - A still from the author's demo, which shows a bar consisting of
18,620 tetrahedral stretched to 3x the initial length with only 6%
volume gain and interactive
speed\[2](../../../images/Proposal_image2.png
"Fig - A still from the author's demo, which shows a bar consisting of 18,620 tetrahedral stretched to 3x the initial length with only 6% volume gain and interactive speed[2")\]

Soft body simulations require a volumetric tetrahedral. Thus, the
surface mesh would need remeshing. A new internal data type would be
created to store the vertices that make up each tetrahedral to store the
volumetric information. To solve the task of remeshing, Incremental
Delaunay Method\[3\] would be used, an iterative method that works by
adding vertices iteratively to existing structures and dividing them.

XPBD is a constraint-based solver for which various constraints can be
experimented with. For the scope of the project, the following
constraints will be implemented:

`   1) Distance constraint (both)`  
`   2) Volume constraint (soft body)`  
`   3) Bending constraint (cloth)`  
`   4) Collision handling (both)`

As a stretch goal, add UI controls to allow users to invoke the solver
from the GUI.

## Project Schedule

|                                                     |                |
| --------------------------------------------------- | -------------- |
| Define new data types and write code to populate it | 2 weeks        |
| Remeshing Algorithm                                 | 3 weeks        |
| XPBD Constraints:                                   |                |
| Distance Constraint and Bending Constraint          | 1 week         |
| Solid Volume Preservation                           | 3 days         |
| Collision Handling                                  | 2 weeks 2 days |
| Testing of all constraints together                 | 1 week         |
| Stress testing                                      | 1 week         |
| Buffer                                              | 1 week         |

Schedule Conflicts - I will be graduating this summer, and I might have
to start working at my new job in mid-August. To account for this, the
majority of coding for the project has been scheduled for the initial
weeks, while the later weeks have been kept for final testing.

## Bio

I am a final semester student pursuing a double major in Physics and
Computer Science from Birla Institute of Technology & Science, Pilani,
India.

My fascination with Computer Graphics started when I first took it as an
elective. This was followed by a project in medical imaging -
"Segmentation of Retinal Vasculature of OCTA", to explore the domain in
the context of medical imaging.

As part of the exploration for the project, I successfully implemented
PBD for cloth simulation with distance constraints in Blender by
tweaking the existing cloth data types\[4\].

## References

1\. <https://matthias-research.github.io/pages/publications/XPBD.pdf>

2\. <https://www.youtube.com/watch?v=uCaHXkS2cUg>

3\. <https://www.cs.jhu.edu/~misha/Spring20/Shewchuk98.pdf>

4\. <https://github.com/arcticblazer/blender>
