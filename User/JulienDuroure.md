# Hello

I am Julien. I was born on 1982. I live in Toulouse, France.

# glTF2.0

I am the current maintainer of glTF2.0 importer/exporter of Blender

This project is manage mostly upstream, on
<https://github.com/KhronosGroup/glTF-Blender-IO>

[Official Documentation is
here](https://docs.blender.org/manual/en/2.83/addons/import_export/scene_gltf2.html)
