### Project Name

Integrating Many Lights Sampling into Cycles X

### Name

Jeffrey Liu

### Contact

  - Email: jliu179@illinois.edu
  - Blender Chat: Jebbly
  - GitHub: <https://github.com/Jebbly>

### Synopsis

The Many Lights Sampling project has been a long-standing project over
the course of Blender’s development. It is meant to improve Cycles
render times in scenes with many lights, so that artists may be more
flexible when it comes to creating and lighting environments. Although a
lot of work has already been completed in regards to the Many Lights
Sampling algorithm, the goal of this project is to merge this work into
the main branch after sufficient testing and user feedback.

### Benefits

Past GSoC projects have shown that Many Lights Sampling can
significantly improve Cycles efficiency for scenes with large numbers of
lights. If it is finally merged into official releases, artists will
experience drastically decreased render times. Furthermore, the project
should also provide a foundation for developers to implement other
algorithms that are based on Many Lights Sampling.

### Deliverables

The minimum deliverable would be to merge the Many Lights Sampling
project into the master branch, and thus official releases. This would
include optimizing the algorithm (with specific benchmark numbers),
testing the stability on a variety of scenes, and getting user feedback
to improve the implementation. End-user documentation should also
provide a clear explanation of how any UI options affect the algorithm.
Time should be dedicated to making the UI as simple and intuitive as
possible.

### Project Details

A major part of this project requires merging previous GSoC Many Lights
Sampling work to Cycles X. Since Cycles X introduced a rewrite of a
large portion of the old Cycles code, much of the original logic must be
updated to fit into the current code. This would also include adding
support for the Embree implementation.

The next step of the project would be to gather and/or create more
scenes to test the stability and performance of the implementation.
Furthermore, more feedback on the user interface is needed in order to
finalize a set of options and designs.

If time allows, optimizing and stabilizing the algorithm would take
priority rather than extending the feature set. Some potential work may
include investigating a bottom-up construction of the tree, finding a
better method to determine the splitting threshold, or improving any of
the current heuristics used. For example, the current splitting cost
function doesn’t take into consideration the centroids of the bounding
boxes, which may actually have a large impact on the accuracy of
resulting orientation bounds. More accurate heuristics should produce a
more effective tree.

### Project Schedule

Since a lot of work has already been done on the Many Lights Sampling
project, the scope of this project will likely be a medium project
(\~175 hours). I can guarantee approximately 25-30 hours per week to
dedicate to this project.

  - May 20 - June 12: Read through the notes from the 2018 and 2022
    projects, research any improvements that have been made to the
    original implementation, and find scenes that can be used to test
    the stability/performance of the algorithm. Also, read more about
    light sampling and debug how Cycles’ kernels work in detail.

<!-- end list -->

  - June 13 - June 27: Finish up the initial implementation of the
    algorithm for the CPU, including support for Embree. Also, create a
    basic set of options for end-users to interface with, and reach out
    to developers and users to get feedback on the UI.

<!-- end list -->

  - June 27 - July 11: Apply any user feedback for UI, and begin
    integration testing + benchmarking scenes. Include time to fix any
    bugs in the implementation.

<!-- end list -->

  - July 11 - July 25: If the algorithm is stable enough on the CPU,
    start working on the GPU implementations. Also, catch up on writing
    any end-user documentation.

<!-- end list -->

  - July 25 - August 8: Complete GPU implementations, including time for
    testing and benchmarking. Start investigating potential
    optimizations, including a bottom-up light tree construction and
    different split heuristics.

<!-- end list -->

  - August 8 - August 22: I applied to be a SIGGRAPH student volunteer,
    so if I get accepted, I will likely not be available between August
    8-11. In the remaining time, test and benchmark the potential
    optimizations used. Also, implement any requested or suggested
    changes.

<!-- end list -->

  - August 22 - September 5: I will likely be unavailable for a few days
    commuting to college. Otherwise, clean up the code so that the work
    can be merged into main.

<!-- end list -->

  - September 5 - 12: Buffer week for any remaining work. Any extra time
    can be spent working on polishing the end-user documentation.

Note that this was just the proposed schedule. The actual schedule is
likely to change depending on the progress made and any obstacles
encountered.

### Bio

My name is Jeffrey Liu and I’m currently an undergraduate student at the
University of Illinois at Urbana-Champaign. My major is Mathematics +
Computer Science, and I’m also planning to pursue a minor in Physics as
well.

I have been using Blender since middle school for renders,
visualizations, and game development; its open-source software and
community have nurtured my passion for computer graphics. As a result,
my computer science journey has largely been oriented around graphics.
I’ve done learning projects with OpenGL, Vulkan, and CUDA, and I’ve
also implemented a variety of rendering techniques in my GitHub
projects. Currently, I’ve been reading through PBRT to learn about
photorealistic raytracing. Furthermore, I have experience contributing
to large-scale C/C++ projects: I participated in Google Code-in 2019
with BRL-CAD where I began integrating the appleseed renderer with
BRL-CAD’s geometry.

I’ve already gone through the “Importance Sampling of Many Lights With
Adaptive Tree Splitting” paper that covers the original implementation,
and I have completed some preliminary work on this patch:
<https://developer.blender.org/D14664>. Outside of this work, I’ve also
been stepping through Cycles code to understand how it works, and trying
out the testing/benchmarking suite myself.
