## Final Report

### Summary

This is my final report detailing the work I've done for the "Many
Lights Sampling in Cycles X" project as part of GSoC 2022. There are two
main ways that others can access my work:

1.  I've uploaded patches on the developer portal as differential
    [D14664](https://developer.blender.org/D14664). This also contains
    some discussion on the subject.
2.  All my work is committed to the branch
    [soc-2022-many-lights-sampling](https://git.blender.org/gitweb/gitweb.cgi/blender.git/shortlog/refs/heads/soc-2022-many-lights-sampling)
    with relatively detailed commit messages.

At a higher level, the following links may also be of interest:

1.  A [list of weekly
    reports](https://devtalk.blender.org/t/gsoc-2022-many-lights-sampling-in-cycles-x-weekly-report/24732)
    detailing the progress I made that week.
2.  A [feedback
    thread](https://devtalk.blender.org/t/gsoc-2022-many-lights-sampling-in-cycles-x-feedback-thread/24773)
    where other people interested in the project could ask questions and
    report issues. For the sake of posterity, I tried to respond to the
    bug reports with a description of how I resolved it, or if it was
    still an ongoing issue.
3.  A [set of technical posts](https://jebbly.github.io/) describing the
    motivation of the algorithm, as well as why certain decisions were
    made. Some parts still need to be updated and/or edited, but it may
    be a useful resource for beginners to get started contributing to
    Cycles.

### Progress

The rewritten [Cycles
X](https://docs.google.com/presentation/d/13Zo2gafKyIFEyZCBucEfFHrrrwz9RBQoIImC6nBCgEE/)
meant that [past GSoC
work](https://wiki.blender.org/wiki/User:Samkottler/GSoC2020/FinalReport)
on the Many Lights Sampling project became outdated and unable to merge
with the master branch. As such, the purpose of this project was to
update, or reimplement, the code so that it could work with Cycles X.
The original goal was to gradually support sampling for the following
lights in both surfaces and volumes:

  - Point Lights
  - Spot Lights
  - Area Lights
  - Emissive Triangles (and later, instanced lights)
  - Background Lights

Originally, my plan was to use the original work as a reference,
complete the initial implementation, and then debug from there. However,
Brecht and Lukas suggested implementing support for features one at a
time. I actually liked this method a lot more because it forced me to
understand the paper at a deeper level, and in retrospect, it probably
saved me a lot of extra debugging time.

These features were first implemented without adaptive splitting in mind
because the original idea was not immediately compatible with Cycles'
architecture. However, as more bugs were reported, it became apparent
that some sort of adaptive splitting would be useful (e.g. when the
importance heuristic wasn't accurate enough). Thus, the latter half of
my project was related to implementing our version of adaptive
splitting. Brecht first suggested the use of resampled importance
sampling, which I then implemented using weighted reservoir sampling to
save on memory usage. This seems to be effective in the problematic
cases mentioned.

So far, the implementation seems to be effective for certain scenes,
such as this [sample attic scene provided by
NVIDIA](https://developer.nvidia.com/usd#sample), slightly adjusted for
Blender by Alaska.

After 2 minutes of render time, these are the results:

![NVIDIA Attic (Original - 2
Minutes)](../../../images/NVIDIA_Attic_\(Original_2_Mins\).png
"NVIDIA Attic (Original - 2 Minutes)") ![NVIDIA Attic (MLS - 2
Minutes)](../../../images/NVIDIA_Attic_Sample_\(MLS_2_Minutes\).png
"NVIDIA Attic (MLS - 2 Minutes)")

After 30 minutes of render time, these are the results:

![NVIDIA Attic (Original - 30
Minutes)](../../../images/NVIDIA_Attic_\(Original_30_Mins\).png
"NVIDIA Attic (Original - 30 Minutes)") ![NVIDIA Attic (MLS - 30
Minutes)](../../../images/NVIDIA_Attic_Sample_\(MLS_30_Minutes\).png
"NVIDIA Attic (MLS - 30 Minutes)")

There are still some scenes where the original implementation performs
better. However, I generally observed that the MLS implementation would
produce better results sample-wise, but the samples themselves were
taking too long to complete. I suspect that the MLS implementation will
perform much better once the algorithm has been optimized. Furthermore,
these implementations should all be GPU compatible, so optimizations
should be the only work needed on that end as well.

### Future Tasks

Unfortunately, some of the debugging took me longer than expected (and
there were also new issues to resolve), so the project is not fully
ready to merge into master yet. There are also a few outstanding issues
that need some further discussion in order to be resolved. Many of the
to-do tasks are listed [here](https://developer.blender.org/T77889), but
the main ones are the following:

  - Support for volumes
  - Better heuristics for textured lights
  - More debugging and benchmarking

Although I won't be able to dedicate much time to this project during my
fall/spring semesters, I plan to continue working on these tasks in the
winter.
