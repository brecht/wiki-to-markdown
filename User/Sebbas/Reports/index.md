# Reports

## Google Summer of Code

  - [2016](https://archive.blender.org/wiki/index.php/User:Sebbas/GSoC_2016/Reports/)
  - [2017](https://archive.blender.org/wiki/index.php/User:Sebbas/GSoC_2017/Reports/)

## Work for Blender Foundation

  - [2019](2019.md)
  - [2020](2020.md)
  - [2021](2021.md)
