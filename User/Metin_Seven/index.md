\_\_TOC\_\_

## Personal Info

  -   
    **Name:** Metin Seven
    **Location:** Netherlands
    **Occupation:** Freelance visualizer, pixel artist, illustrator, 3D
    designer
    **Homepage:** [metinseven.nl](https://metinseven.nl)
    **Mastodon:** [@metin](https://mastodon.art/@metin)
    **Twitter:** [@metinse7en](https://twitter.com/metinse7en)

  
:**⌛ Former…**

  -   
    🙋 Blender Artists, Devtalk & Wiki moderator
    🧑‍🎨 Blender Foundation Technical Artist
    📰 News cartoonist
    💬 Comic strip creator
    📺 TV show animator
    📝 Magazine editor
    🕹️ 16-bit game developer (Amiga, CD32, MS-DOS, …)
