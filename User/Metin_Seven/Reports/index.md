\_\_TOC\_\_

# Reports 2021

## May 03 - May 10

  - Added multiple new Geometry Nodes demo scenes to the Blender.org
    file server and [Demo
    Files](https://www.blender.org/download/demo-files/) download page,
    including a parametric building generator (temporarily deactivated
    until the Blender 2.93 release).
  - Updated three 2.92 Blender demo scenes that didn't work correctly in
    2.93: 'Spherical eye', 'Cubic whirlpool' and 'Text morph'.

## April 26 - May 02

  - Daily checking, moderation, participation and answering at Blender
    Developer Talk and Blender.chat.
  - Added a new Geometry Nodes demo scene to the Blender.org file server
    and [Demo Files](https://www.blender.org/download/demo-files/)
    download page, titled *Ripple Dreams* (temporarily deactivated until
    the Blender 2.93 release).
  - Reported a possible Cycles-X bug to Brecht at
    [Blender.chat](https://blender.chat/channel/render-cycles-module?msg=gkLrpZSd7BzRM5tvc),
    and initiated and moderated a [Cycles-X feedback
    thread](https://devtalk.blender.org/t/cycles-x-feedback/18598) at
    Devtalk.
  - Made three Blender builds for the [Experimental branch download
    page](https://builder.blender.org/download/branches/):

<!-- end list -->

  -   
    ─ A new version of the Sculpt Dev branch after the 3.0 Master build
    was merged on April 22nd.
    ─ The BMesh Multires branch, so there's a download for some first
    public testing.
    ─ Update of the Cycles-X branch.
    ─ Posted notifications of the availability of these branch updates
    at the Blender Artists and Devtalk communities, to stimulate
    testing.

<!-- end list -->

  - Discovered a reproducible crash in the new Sculpt Dev build. Sent a
    download link to the crash scene and info to Pablo Dobarro. The
    issue turned out to be caused by the Dyntopo version that was used
    in the Sculpt Dev branch, and the issue does not occur in the BMesh
    Multires branch I initiated today, featuring a newer version of
    Dyntopo. So the issue has been solved.
  - Noticed an issue in the Sculpt Dev build regarding a selection
    highlight color remaining visible in a separate camera view.
    Reported it to Pablo Dobarro in the [Sculpt-Paint-Texture-Module at
    Blender.chat](https://blender.chat/channel/sculpt-paint-texture-module?msg=rTorCxFvbbiiPf4RR).
  - While testing the Temp Bmesh Multires build I encountered a sudden
    slowdown issue, which I reported to Joseph Eagar at the
    [Sculpt-Paint-Texture-Module channel at
    Blender.chat](https://blender.chat/channel/sculpt-paint-texture-module),
    and sent a download link to the problem scene. The bug was
    discovered and fixed.
  - Initiated a discussion in the Staff section of
    [Devtalk](https://devtalk.blender.org/) and the BF Committers
    mailing list, about the (current lack of) inclusion of a tag that
    would unify the different types of meeting notes, so it would be
    easier for Blender users to follow all meeting notes.
  - Approached the creator of a scene featuring parametric buildings
    generated using Geometry Nodes, asking for submission of the scene
    as a Blender demo. The creator responded positively.

## April 19 - April 25

  - Daily checking, moderation and participation at Blender Developer
    Talk and Blender.chat.
  - Continued creating and improving assets for the [Blender Assets
    Bundle](https://developer.blender.org/diffusion/BL/browse/trunk/lib/assets/).
  - The Blender Asset Bundle task
    ([T55239](http://developer.blender.org/T55239)) was reassigned to
    me. Started indexing and managing the Asset Bundle, including
    contributions from other Blender artists.

<!-- end list -->

  -   
    ─ Approached Vilém Duha from Blenderkit to ask if he would like to
    contribute materials to the Asset Bundle. He responded positively.

<!-- end list -->

  - Processed several procedural materials and textures by Simon Thommes
    into the material assets Blend file, including renaming, adding
    Asset Manager descriptions, keywords, committing to the
    [repository](https://developer.blender.org/diffusion/BL/browse/trunk/lib/assets/),
    and updating the Asset Bundle task
    ([T55239](http://developer.blender.org/T55239)).
  - Added a new Geometry Nodes demo scene to the Blender.org file server
    and [Demo Files](https://www.blender.org/download/demo-files/)
    download page, titled *Abstract Monkey* (temporarily deactivated
    until the Blender 2.93 release).
  - Established a new [Blender Asset Bundle
    info](../../../Release_Notes/Blender_Asset_Bundle.md) Wiki
    page, reorganizing content from a design note by Julian Eisel and
    combining it with the Asset Bundle task
    ([T55239](http://developer.blender.org/T55239)) intro text.
  - Simplified the Asset Bundle task
    ([T55239](http://developer.blender.org/T55239)) intro text,
    referring to the new Blender Asset Bundle Wiki page for more info.

## April 12 - April 18

  - Daily checking, moderation and participation at Blender Developer
    Talk and Blender.chat.
  - Continued creating assets for the [Blender Assets
    Bundle](https://developer.blender.org/diffusion/BL/browse/trunk/lib/assets/)
    (details: [T55239](http://developer.blender.org/T55239)):

<!-- end list -->

  -   
    ─ Car paint material with automatic angular discoloration and
    procedural flakes.

<!-- end list -->

  - Approached two Blender artists to ask if they would like to share
    their interesting Geometry Nodes demo scenes via the [Blender.org
    Demo Files](https://www.blender.org/download/demo-files/) download
    page.
  - Checked and added two new Blender demo scenes to the Blender file
    server and the Blender Demo Files page CMS, in deactivated state.
  - Updated the [Blender feature demo scene
    info](../../../Release_Notes/Blender_Demos.md) Wiki page.
  - Checked and added a new Geometry Nodes demo scene by James Redmond
    to the Blender file server and the Blender Demo Files page CMS, in
    deactivated state.

## April 05 - April 11

  - Daily checking, moderation and participation at Blender Developer
    Talk and Blender.chat.
  - Finished studio lighting setups for the Built-in Assets project
    ([T55239](http://developer.blender.org/T55239)).
  - Requested a repository folder for the Built-in Assets, so they can
    easily be connected to the Blender releases.
  - Set up SVN in order to be able to commit asset updates to the
    Phabricator.
  - Committed the first version of the [Blender Asset
    Bundle](https://developer.blender.org/diffusion/BL/browse/trunk/lib/assets/)
    blend file ([rBL62609](http://developer.blender.org/rBL62609)).
  - Created various metal materials for the Blender Assets Bundle
    ([rBL62611](http://developer.blender.org/rBL62611)), featuring:

<!-- end list -->

  -   
    ─ correct Fresnel conversion of IOR to Specular value, as [described
    in the Blender
    documentation](https://docs.blender.org/manual/en/dev/render/shader_nodes/shader/principled.html#inputs);
    ─ node groups for procedural scratches and brushed surface textures.

<!-- end list -->

  - Split previous assets Blend file into Materials.asset.blend and
    StudioLights.asset.blend , and extended and improved the studio
    lights ([rBL62612](http://developer.blender.org/rBL62612)).

## March 29 - April 4

  - Daily checking, moderation and participation at Blender Developer
    Talk and Blender.chat.
  - Performed some language corrections in the [Blender community sites
    page](https://www.blender.org/community/).
  - Added the 'meeting' tag to all meeting threads in Devtalk, so
    visitors can search across different types of meetings (general,
    rendering, UI) using the 'meeting' tag.
  - Built a new Sculpt Dev branch build for the [Blender Experimental
    Builds](https://builder.blender.org/download/branches/).
  - Continued creating assets for the Built-in Assets project
    ([T55239](http://developer.blender.org/T55239)):

<!-- end list -->

  -   
    ─ More lighting setups with softbox mesh lights.  
    ─ Lighting setups with fine control over light falloff (Cycles).

## March 22 - March 28

  - Daily checking, moderation and participation at Blender Developer
    Talk and Blender.chat.
  - Checked and prepared Blender 2.93 demo scenes from the [Blender
    feature demo scene
    submissions](../../../Release_Notes/Blender_Demos.md) list,
    including addition of 'README' info texts.
  - Uploaded the demo scenes to the Blender file server, and added
    temporarily deactivated entries to the [Blender.org Demo
    Files](https://www.blender.org/download/demo-files/) download page.
  - Approached Blender demo scene creator James Redmond regarding a
    rendering issue in one of his Geometry Nodes demo scenes. We
    subsequently agreed that he will submit four new 2.93 Geometry Nodes
    demo scenes in time before the 2.93 release.
  - Approached Blender demo scene creator Grigoriy Ignatyev because a
    number of elements turned out to be invisible in the March 23 Alpha
    of 2.93. He will take a look at it. It's probably caused by changes
    in Geo Nodes.
  - Added an observation to my
    [T84513](http://developer.blender.org/T84513) bug submission.
  - Approached Blender artist Erindale Woodford regarding a Geometry
    Nodes scene that would be eligible for the Blender Demo Files page
    and possibly also the 2.93 splash screen. He prepared the scene.
  - Updated the Blender feature demo scene submissions list, including
    addition of a dual screenshot to show how multiple info text blocks
    can be included in a demo scene.

## March 15 - March 21

  - Daily checking, moderation and participation at Blender Developer
    Talk and Blender.chat.
  - Received the previously mentioned Blend file containing two new
    Geometry Nodes demo scenes for the 2.93 release. Updated the
    [Blender demo file
    contributions](https://hackmd.io/15Ef8CmwTPaMI6acdCoRiw?view) note,
    and will check and prepare all 2.93 demo scenes soon.
  - Confirmed an apparent bug in the Grease Pencil Time Offset modifier:
    the Reverse option repeats the animation once (so it is played
    twice), then stops. Checked this [by request of
    Frogstomp](https://blender.chat/channel/blender-coders?msg=CgX9YKw4JbhfLJcPL)
    in the \#blender-coders channel on Blender.chat.
  - Approached a Blender artist on Twitter who shared a nice potential
    2.93 demo scene, but the scene turned out to contain commercial 3D
    models.
  - Transferred the [Blender demo file
    contributions](https://hackmd.io/15Ef8CmwTPaMI6acdCoRiw?view) note
    to a [new Blender Wiki
    page](../../../Release_Notes/Blender_Demos.md), including
    conversion of Markdown tags to Wiki markup tags.

## March 08 - March 14

  - Daily checking, moderation and participation at Blender Developer
    Talk and Blender.chat.
  - Continued creation of a demo scene to showcase the improved Random
    Walk SSS algorithm in Blender 2.93.
  - Finalized the Random Walk SSS demo scene, and created two
    renderings, one in 2.92 and one in 2.93, for a comparison in the
    [2.93 Cycles Release
    Notes](../../../Release_Notes/2.93/Cycles.md#Subsurface_Scattering).
  - Added the Random Walk SSS demo scene to the Blender.org [Demo
    Files](https://www.blender.org/download/demo-files/) download page.
  - Removed all 2.92 requirement indications from the Blender Demo Files
    page now that 2.92 has been officially released.
  - Updated the [Blender demo file
    contributions](https://hackmd.io/15Ef8CmwTPaMI6acdCoRiw?view) note
    with new Blender demo scenes, to be processed into the Demo Files
    download page.
  - Added a 'Demo scene submission guidelines' section to the Blender
    demo file contributions note.
  - Edited 'README' text, renamed file, rendered and added the 'Taco
    rice' 2.92 Geometry Nodes demo scene to the Blender Demo Files page.
  - Approached a Blender user via Twitter, asking if he would be
    interested to submit his 2.93 Geometry Nodes demo scene(s) for
    inclusion in the Blender Demo Files page. He turned out to be
    interested. The scene(s) will follow. Added entry in Blender demo
    file contributions note.

## March 01 - March 07

  - Daily checking, moderation and participation at Blender Developer
    Talk and Blender.chat.
  - Checked a report of the APIC vs FLIP solver fluid simulation demo
    scene inexplicably not rendering in F12 production rendering mode
    (it only worked in the Eevee viewport). Fixed the issue by appending
    everything into a fresh Blender scene, and updated the [Demo
    Files](https://www.blender.org/download/demo-files/) page download
    of the scene.
  - Prepared and added an APIC solver fluid simulation lava demo scene
    to the Blender Demo Files page, after approval by Dalai Felinto and
    fluid sim developer Sebastián Barschkis.
  - Posted a reminder at developer.blender.org that my
    [T83623](http://developer.blender.org/T83623) bug report hasn't been
    picked up yet. The issue is still there at the time of writing.
  - Updated Glen Melenhorst's Mr. Elephant demo scene, which had become
    very slow due to Eevee having changed. Changed the Volumetric
    Shadows to a value of 1 for a significant speed-up. Will update the
    Demo Files download around the 2.93 release, so the current version
    will remain compatible with current Blender versions.
  - Updated the [Blender demo file
    contributions](https://hackmd.io/15Ef8CmwTPaMI6acdCoRiw?view) note
    to reflect the above.
  - Submitted Sculpt Mode UI / UX feedback to
    [Blender.chat](https://blender.chat/channel/user-interface-module?msg=WTRnFM6DiFYGLogMm)
    and
    [Devtalk](https://devtalk.blender.org/t/sculpt-mode-feedback/7420/318).
  - Submitted bug report [T86331](http://developer.blender.org/T86331)
    (Preferences window save menu issue).
  - Continued creation of a demo scene to showcase the improved Random
    Walk SSS algorithm in Blender 2.93.

## February 22 - February 28

  - Daily checking, moderation and participation at Blender Developer
    Talk.
  - Added a 2.92 splash screen download entry to the [Demo
    Files](https://www.blender.org/download/demo-files/) page, including
    credits, links and the right Creative Commons license type.
  - Created a marked-up [HackMD
    note](https://hackmd.io/15Ef8CmwTPaMI6acdCoRiw) containing Blender
    demo file submissions, authors and related information, which I will
    keep up to date.
  - Prepared and added a fluid simulation solver demo scene by Sebastián
    Barschkis to the 2.92 Demo Files page.
  - Inspected the Geometry Nodes demo scenes from the Demo Files page,
    which turned out to look correct in the viewport, but incorrect in
    the production rendering. Approached the creator, who fixed the
    rendering issues. Added credits for newly embedded HDRI files to the
    'README' text editor data blocks, and reuploaded the scenes to the
    Demo Files page.
  - Unlocked cameras in the Geometry Nodes files, to avoid user
    confusion, and updated the Demo Files page downloads.
  - Approached a concept artist to ask for permission to translate a
    drawing of his to a 3D demo scene to showcase the improved Random
    Walk SSS algorithm in Blender 2.93.
  - Started creating a demo scene to showcase the improved Random Walk
    SSS algorithm in Blender 2.93.

## February 15 - February 21

  - Daily checking, moderation and participation at Blender Developer
    Talk.
  - Created variations of the Blender 2.92 splash screen in several
    sizes and two image file formats with the appropriate filenames for
    the Steam release, according to the [Steam image submission
    guidelines](../../../Process/Release_On_Steam.md).
  - Established a GIMP XCF template with all splash variations as
    layers, including fitting logos as separate layers for each size, to
    ease future creation of the Steam splash screen variations.
  - Final management of Blender 2.92 demo content submissions, including
    discussing with the participating artists, asking for adjustments,
    presenting a list of final demo scene submission download links,
    author names and website links to the Blender staff on Blender.chat.
  - Prepared the selected demo scene submissions, combining some Blend
    files into one Blend file with multiple scenes, adding credits and
    instructions in a 'README' text datablock, and making sure they're
    saved from Blender 2.92.
  - Uploaded the demo scenes to blender.org, created thumbnails, added
    new sections and demo scene download entries with credits to the
    [Demo Files](https://www.blender.org/download/demo-files/) page.

## February 08 - February 14

  - Daily checking, moderation and participation at Blender Developer
    Talk.
  - Continued management of Blender 2.92 demo content submissions,
    including discussing the content with the participating artists,
    asking for adjustments, planning delivery and posting updates on
    Blender.chat. Reported the first final demo scene submissions to the
    Blender staff.
  - Continued creating the first assets for the Built-In Assets project
    ([T55239](http://developer.blender.org/T55239)): studio lighting
    setups, including creation of limbos and pedestals, adding every
    item to the Asset Library and tagging everything.

## February 01 - February 07

  - Daily checking, moderation and participation at Blender Developer
    Talk.
  - Continued management of Blender 2.92 demo content submissions,
    including discussing the content with the participating artists,
    planning delivery and posting updates on Blender.chat.
  - Started creating the first assets for the Built-In Assets project
    ([T55239](http://developer.blender.org/T55239)): studio lighting
    setups.
  - Discovered and submitted issue
    [T85454](http://developer.blender.org/T85454). This turned out to be
    a known limitation though.

## January 25 - January 31

  - Daily checking, moderation and participation at Blender Developer
    Talk. Among the activities is closing these topics: [An open letter
    to the Blender
    Foundation](https://devtalk.blender.org/t/an-open-letter-to-the-blender-foundation/14801/332)
    and [When will real caustics be
    implemented](https://devtalk.blender.org/t/when-will-real-caustics-be-implemented/17197/16?u=metinseven).
  - Blender Wiki maintenance: checked links in
    [Addons](../../../Process/Addons/index.md) section, fixed broken Blender
    Add-ons link, added a list entry with advice to subscribe to the
    Add-ons (Community) project, and some more minor updates of that
    page.
  - Posted to \#Blender-coders on Blender.chat that the Submit Add-on
    and Add-ons (Community) pages on developer.blender.org both
    reference the, no longer used, Addon Catalog and should be updated.
  - Submitted a proposal for an Apply Scale option with automatically
    corrected modifier values to
    [Devtalk](https://devtalk.blender.org/t/modifiers-improvements/9788/21)
    and
    [Blender.chat](https://blender.chat/channel/user-interface-module?msg=ivr5Mwp4Y5rTZbHf3).
  - Posted a request for Blender 2.92 demo content on Blender community
    platforms and social media:
    [BlenderNation](https://www.blendernation.com/2021/01/27/call-for-blender-2-92-demo-content/),
    [Blender
    Artists](https://blenderartists.org/t/call-for-blender-2-92-content/1281096)
    and
    [Twitter](https://twitter.com/metinse7en/status/1354397212869070849).
  - Corresponded with the Blender users who responded to the request for
    2.92 demo content.
  - Submitted issue [T85224](http://developer.blender.org/T85224),
    regarding Sculpt Mode Auto-Masking. This turned out to be an unclear
    difference between local and global settings.

## January 18 - January 24

  - Daily checking, moderation and participation at Blender Developer
    Talk.
  - Created and rendered a Blender demo animation scene to showcase the
    fluid system's Viscosity feature.
  - Added a compilation of rendered frames from the Viscosity demo scene
    to the [Blender 2.92 Release
    Notes](../../../Release_Notes/2.92/Physics.md).
  - Initiated a discussion with Julian Eisel about the Built-in Assets
    project ([T55239](http://developer.blender.org/T55239)), as a
    preamble to working on content for the Built-in Assets.
  - Tested if bug [T84049](http://developer.blender.org/T84049) I
    reported still causes a crash in the latest Blender 2.92 Beta. The
    crash still occurred, indicated that.
  - Submitted a proposal for Subdivision Surface modifier improvement to
    Devtalk and Blender.chat. The proposal has been initiated in patch
    [D10187](http://developer.blender.org/D10187).

## January 11 - January 17

  - Daily checking, moderation and participation at Blender Developer
    Talk.
  - Researched potential artists for the Blender 2.92 splash image, and
    communicated 11 portfolios and related contact info to Blender staff
    members.
  - Wrote two iterations of a Copyright Rules concept text for the
    Blender Wiki.
  - Finalized the [Copyright Rules](Contact/CopyrightRules)
    text and implemented it in the Blender Wiki, including an
    introduction text in the [Communication](Contact)
    section.

## January 04 - January 10

  - Researched a number of things, such as tasks to perform on a regular
    basis, who is responsible for which Blender tasks, where and how to
    submit my reports, and more.
  - Added issue report [T84513](http://developer.blender.org/T84513)
  - Asked Ray Molenkamp (Lazy Dodo) to inform me of his Devtalk
    moderation activities, so I can start relieving him from those
    tasks.
  - Performed various moderation tasks at Devtalk, including moving
    threads to appropriate sections, answering questions and
    participating in discussions.
  - Asked Bastien Montagne to create a Blender Wiki account for me.
  - Created my personal Blender Wiki profile and pages.
  - Approached two potential Blender developers.
