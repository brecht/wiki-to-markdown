## August 28 - September 1, 2023

  - DevTalk: [User Interface module meeting
    notes](https://devtalk.blender.org/t/2023-08-29-user-interface/30895)
  - ToDo
    \`[\#111746](https://projects.blender.org/blender/blender/issues/111746)
    UI: Unify Add menu in Node Editors\`
  - Design
    \`[\#111736](https://projects.blender.org/blender/blender/issues/111736)
    Grease Pencil: Add eyedropper to palette popup\`
  - Design \`[PR
    \#111756](https://projects.blender.org/blender/blender/pulls/111756)
    UI: Add canvas picker to paint modes in 3D Viewport\`
  - Merged \`[PR
    \#111798](https://projects.blender.org/blender/blender/pulls/111798)
    Shading Nodes: Refactor Add menu \#111798\`
  - Merged \`[PR
    \#111481](https://projects.blender.org/blender/blender/pulls/111481)
    Compositor: Re-organize Add menu\`

## August 21-25, 2023

  - Reviewed \`[PR
    \#111446](https://projects.blender.org/blender/blender/pulls/111446)
    Nodes: Change rotation socket color\`
  - Design
    \`[\#111436](https://projects.blender.org/blender/blender/issues/111436)
    UI: Placeholder in Text fields\`
  - Design
    \`[\#111538](https://projects.blender.org/blender/blender/issues/111538)
    Modifiers UI Overhaul\`

## August 14-18, 2023

  - DevTalk: [User Interface module meeting
    notes](https://devtalk.blender.org/t/2023-08-15-user-interface/30710)
  - Pushed
    \`[rBeef2b61e](https://projects.blender.org/blender/blender/commit/eef2b61e91)
    Theme: Set 3D Viewport background to single color\`
  - Pushed
    \`[26dcc961](https://projects.blender.org/blender/blender/commit/26dcc961)
    Markers: Use double-click to rename\`
  - Reviewed \`[PR
    \#111153](https://projects.blender.org/blender/blender/pulls/111153)
    UI: Increase Color Picker Size\`
  - Reviewed \`[PR
    \#111017](https://projects.blender.org/blender/blender/pulls/111017)
    Fix \#111017: Gap between color square and frame of box in the color
    ramp\`
  - Reviewed \`[PR
    \#111194](https://projects.blender.org/blender/blender/pulls/111194)
    UI: Make View3D Header Overlapped\`
  - Reviewed \`[PR
    \#111261](https://projects.blender.org/blender/blender/pulls/111261)
    UI: Add panel for Node Editor in Preferences\`
  - Feedback \`[PR
    \#110065](https://projects.blender.org/blender/blender/pulls/110065)
    Node previews in the shader editor\`
  - Design \`[PR
    \#111272](https://projects.blender.org/blender/blender/pulls/111272)
    UI: Replace list of workspace add-ons with a UIList\`
