## Sebastian Sille (NRGSille)

### About

  -   
    **Location:** Oldenburg, Germany
    **Resume:** Event technician, DJ, lighting designer, 3D artist and
    hobby programmer
    **Website:** [NRGSille](https://www.nrgsille.com)

Blender user since 2009 starting with Blender version 2.49

### Maintaining

I am maintaining the following add-on:

  - [3ds
    Import/Export](https://docs.blender.org/manual/en/3.6/addons/import_export/scene_3ds.html)
