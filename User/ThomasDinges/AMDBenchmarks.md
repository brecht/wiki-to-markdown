## GPU Rendering Benchmarks for AMD

  - Windows 10, AMD Ryzen 3700X, AMD Radeon 5500 XT (8GB of memory)
  - Blender (from buildbot): Blender 2.93.5, Blender 3.0 Beta
    (12.11.2021)
  - Scene files were modified locally, to reduce rendertime for the
    tests and ensure settings are equal between versions.
  - In Blender 2.93, all files were rendered with Path Tracing and a
    Tile Size of 128x128. Rendertime for 2.93 is excluding kernel
    compile time, as the HIP kernel in 3.0 is precompiled.
  - Each scene was rendered twice per version, and the average time was
    taken.

| Scene                | Samples | 2.93 | 3.0  |
| -------------------- | ------- | ---- | ---- |
| barbershop\_interior | 200     | 7:04 | 3:12 |
| bmw27                | 1000    | 1:54 | 1:07 |
| classroom            | 300     | 4:51 | 2:37 |
| fishy\_cat           | 1000    | 3:55 | 2:37 |
| junk\_shop           | 240     | 3:49 | 2:21 |
| koro                 | 500     | 4:33 | 2:15 |
| monster              | 256     | 2:16 | 1:07 |
| pabellon             | 500     | 4:41 | 2:56 |
| sponza               | 1000    | 3:29 | 1:59 |
