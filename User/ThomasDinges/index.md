## Thomas Dinges (DingTo)

#### Contact

  -   
    **blender.chat:**
    [ThomasDinges](https://blender.chat/direct/ThomasDinges)
    **devtalk.blender.org:**
    [ThomasDinges](https://devtalk.blender.org/u/thomasdinges/)
    **projects.blender.org:**
    [ThomasDinges](https://projects.blender.org/ThomasDinges)
    **Twitter:** [@dingto](https://twitter.com/dingto)
    **Weekly Open Chat:** [Devtalk
    Forum](https://devtalk.blender.org/t/developer-community-coordinator-weekly-open-chat/21578)

#### About me

I started using Blender in 2007, Blender 2.44 was the first version I
downloaded. Two years later I joined the 2.5x project and worked on the
User Interface (mostly UI layouts). Later I joined the Cycles renderer
team and worked on features like shading, performance and OSL. I got a
Google Summer of Code grant three times (2013, 2014 and 2016). You can
find more information about that in the [old
wiki](https://archive.blender.org/wiki/index.php/User:DingTo/). Today I
work as development coordinator for Blender.

### Development Coordinator

[Weekly Reports](WeeklyReports/index.md)

[Onboarding & Modules
report](OnboardingModulesReport.md)

[Improving Patch
Review](ImprovingPatchReview.md)

[Roadmap planning for the Blender 3.x
series](RoadmapPlanning.md)

[Module improvements](ModuleImprovements.md)

### Development

[AMD Benchmarks](AMDBenchmarks.md)
