## Pull Request workflow

This guide explains how to create a pull request (PR) on
projects.blender.org. This is the new prefered way of submitting
patches.

### Fork the repository

  - Go to the repository you want to create a pull request for,
    <https://projects.blender.org/blender/blender> for Blender.
  - Click on the **Fork** button in the upper right.

![../../images/Gitea\_PR\_CreateFork.jpg](../../images/Gitea_PR_CreateFork.jpg
"../../images/Gitea_PR_CreateFork.jpg")

  - Confirm the fork by clicking on the green **Fork Repository**
    button. (XXX Private / Non Private? Guess private should be avoided
    here and won't work for PRs later?)

![../../images/Gitea\_PR\_CreateForkConfirmation.jpg](../../images/Gitea_PR_CreateForkConfirmation.jpg
"../../images/Gitea_PR_CreateForkConfirmation.jpg")

### Checkout & Development

  - The fork has been creeated now and can be checked out as usual via
    the address shown. (See [How to setup git and commit
    access](../../Tools/Git.md#Commit_Access))

![../../images/Gitea\_PR\_ForkCheckout.jpg](../../images/Gitea_PR_ForkCheckout.jpg
"../../images/Gitea_PR_ForkCheckout.jpg")

### Create a new pull request

  - Once a change has been commited and pushed to a branch on your fork,
    a new pull request can be created by switching to the branch and
    then clicking on the PR button next to it.

![../../images/Gitea\_PR\_CreatePR.jpg](../../images/Gitea_PR_CreatePR.jpg
"../../images/Gitea_PR_CreatePR.jpg")

  - You can see all the changes that are ahead of master now and can
    click on **New Pull Request**. Enter a title and a description,
    attach additional files if needed and submit it.

## Alternative text

### One Time Setup

This assumes you have the blender/blender repository already checked out
on your computer, following the build instructions.

  - Go to <https://projects.blender.org/blender/blender> and click the
    **Fork** button.
  - Confirm the fork with the default settings.
  - Now you will have to add your personal fork as a remote in your
    local git repository. Click **SSH** to see the correct URL, and then
    add it like this:

<!-- end list -->

``` bash
git remote add me git@projects.blender.org:<USERNAME>/blender.git
```

### Create a Pull Request

Now in your git local git repository, make your changes in a branch:

``` bash
git checkout -b my-feature master
git commit file1 file2
```

When you are done, push this branch to your fork:

``` bash
git push me my-feature
```

If all goes well, a message will be printed to the console with a link
to create a PR:

``` bash
TODO: get example of message
```

Alternatively, you can go to the page of your fork on
projects.blender.org/<USERNAME>/blender. There you can select the
branch, and click the icon next to it create a PR from the branch.

### Update a Pull Request

You can update the code by simply pushing the same branch, and the
changes will be reflected in the pull request.

``` bash
git checkout my-feature
git commit --amend file3 file4
git push -f me my-feature
```

You can also add additional commits instead of amending an existing one,
if changes are more clear when separated into multiple commits.
Reviewers are able to squash everything into a single commit if needed.

### Checkout a Pull Request

For a given pull request \`\#123\`, checkout it out into a local branch
\`their-feature\` like this:

    git fetch origin pull/123/head:their-feature

### Merge a Pull Request

Blender developers with commit access to the main repository can merge
pull requests. There are two strategies:

  - **Rebase and fast-forward**: push all the commits in the PR, with
    commit messages unchanged except \`Pull Request \#123\` being added
    at the end of the last commit.
  - **Squash**: squash everything into one commit, using the PR title
    and description as the commit message. You will have the opportunity
    to edit the title and description in the user interface before the
    push goes through.
