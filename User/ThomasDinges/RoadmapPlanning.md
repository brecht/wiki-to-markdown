## Roadmap planning for Blender 3.x

This wiki page is used to collect information and schedule meetings
regarding the Blender 3.x series roadmap.

Questions and infos for module teams:

  - Are there plans / roadmaps for the 3.x series already?
  - Ton is proposing a series of chats in September with the modules to
    give them a chance to present their plans, and to give the community
    an opportunity to be involved in the Blender 3 plans.
  - Please note available times during regular European workday,
    preferably not later than 16/17:00 CEST so we can schedule the
    chats.

### Animation & Rigging

Plans / Roadmaps:

  - Improve bone selection/interaction, potentially via face maps (i.e.
    interacting with subsets of the mesh, rather than selecting bones
    and interacting with those).
  - Hierarchical bone interaction, to make it easier to switch between
    coarse/intermediate/fine controls.
  - Improve evaluation speed of rigging & animation system.
  - Stretch goal: Rigging via nodes. Needs more design & planning
    discussions to decide between generative nodes (node system to
    generate current-style rig) or to have constraints, and maybe bones
    themselves and their parent/child relations, expressed & evaluated
    via nodes.

Available times: Mo, Tu, Th, Fr 11:00 - 18:00 CEST. Earlier than 11:00
also possible, but not that comfortable on a regular basis (already used
by weekly kickoffs, daily standups, etc).

### Core

Plans / Roadmaps:

No official roadmap, lots of ideas and known TODOs (including some
fairly big ones).

[The project
workboard](https://developer.blender.org/project/board/127/), in
particular the version-tagged columns and the 'Long-Term - Official'
tasks.

Available times:

Regular Amsterdam office hours.

### Grease Pencil

Plans / Roadmaps: -

No official roadmap, but main two topics will be asset manager
integration and new Eevee materials for grease pencil.

Also we have a list of ideas for LineArt and other improvements

Available times: Usually \>= 16:00 Amsterdam time, but could get some
time during the morning if we arrange the meeting before.

### Modeling

Plans / Roadmaps: -

Available times: -

### Nodes & Physics

Plans / Roadmaps:

  - Geometry Nodes fields and other workflow improvements
    ([blog](https://code.blender.org/2021/08/attributes-and-fields/)).
  - Support for curves and volumes in geometry nodes.
  - Integrate simulations into node based workflow
    ([blog](https://code.blender.org/2021/07/nodes-workshop-june-2021/)).

Available times: Usually 10:00 - 18:00.

### Pipeline, Assets, I/O

Plans / Roadmaps:

Main two projects will be:

  - [Assets](https://developer.blender.org/T73366) (stabilization and
    completing features).
  - [USD](https://developer.blender.org/T73363): The
    [importer](https://developer.blender.org/T81257) will be in 3.0.
    Plans for further developments are unclear, besides stabilizing. One
    possibility would be to work on the [Collection
    IO](https://developer.blender.org/T68933) project in relation with
    Core module.

Smaller projects:

  - The [Faster I/O for OBJ, PLY,
    STL](https://developer.blender.org/T68936) status is currently
    unclear, though unlikely to make it into 3.0 now (except maybe as an
    experimental option).
  - [Unifying I/O importing and drag and
    drop](https://developer.blender.org/T68935) could also be an
    interesting project.

\[The project workboard
<https://developer.blender.org/project/board/97/>\], in particular the
version-tagged columns and the 'Long-Term - Official' tasks.

Available times:

Regular Amsterdam office hours.

### Platforms, Builds, Tests & Devices

Plans / Roadmaps:

I'd consider the platform module a service module to the others, there's
no roadmap it's essentially being driven by the requirements of the
other modules, only plan is to codify the operation of the module.

Available times:

Ray: MST(GMT-7) - 6.30-15.30

### Python & Addons

Plans / Roadmaps:

  - \`gpu\` module (replace the \`bpy\` module completely).
  - Python 3.10 support (mostly complete).

Available times: -

### Rendering

Plans / Roadmaps:

  - Eevee & Viewport
      - [Eevee 2.0](https://code.blender.org/2021/06/eevees-future/)
      - [Vulkan](https://developer.blender.org/T68990)
  - Render & Cycles
      - Stabilize Cycles X
      - [Cycles sampling
        improvements](https://developer.blender.org/T87839)
      - [Cycles light linking](https://developer.blender.org/T68915)
      - [Color management
        improvements](https://developer.blender.org/T68926)

Available times:

Brecht: Amsterdam local office hours

### Virtual Reality

Plans / Roadmaps:

  - [Roadmap](https://developer.blender.org/T91627)

Available times:

Peter: 9:00 - 13:00 CEST.

### Sculpt, Paint & Texture

Plans / Roadmaps: -

Available times: -

### Triaging

Plans / Roadmaps:

Get untriaged reports under 100 again and keep it stable there. Take on
more first round Diff review (perhaps limit this to bugfixing patches).
(both of the above are constrained by limited resources atm, without
more staff improvements are unlikely)

Available times:

Philipp: 8:30 - 17:00 CET

### User Interface

Plans / Roadmaps: -

Available times: -

### VFX & Video

Plans / Roadmaps:

No official roadmap: been busy with Cycles X. There are smaller
projects/improvements happening in VSE and Compositor.

Compositor:

  - Full frame compositor.

Available times:

  - Sergey: Amsterdam local office hours
  - Jeroen: Amsterdam local office times except Thursday.
  - Richard: No fixed schedule, If not avalable in morning, then
    afternoon.
