# Onboarding & Modules report

## Introduction

I wanted to find out how the onboarding process is working, see how the
modules are doing and where help is needed.

Therefore I sent a small survey to all module owners, to get a better
understanding of the current situation.

Onboarding questions:

  - How do you see the current onboarding process? What can be improved?
  - Do you feel our documentation is fine for new developers (Getting
    started, Compilation…) or are there things missing or need
    improvement?
  - How do you handle onboarding in your module? Anything special,
    something that is / isn’t working well?

Module questions:

  - How is your module doing? What do you want to improve, what are you
    happy with?
  - Do you need help with roadmaps, more developers, artist member
    involvement…?

This report summarizes the answers given, and proposes improvements to
the onboarding process.

## Onboarding

The first few steps in the onboarding process, like getting Blender to
compile, submit a patch or report a bug is described as working well.
Afterwards two main issues are mentioned with wide consensus: Long
response times for code review and lack of documentation.

### Issues in the current onboarding process

#### Response time

Waiting times for code review are too long. Both the initial time to the
first response from a developer as well as follow ups often take weeks
or even months. This is a huge problem and can result in new
contributors losing their interest and giving up. Two reasons were
mentioned:

  - Lack of time / not enough (experienced) developers who could do the
    code review. Some modules are split thin, in maintenance mode or
    people are busy with other projects.
  - No open communication of expectations. When a contributor is not
    experienced enough for the task, the feature is low priority or
    results in additional work and is therefore not a good use of time,
    this isn’t communicated openly (probably to not step on peoples
    feet) and so some developers rather say nothing.

#### Quality expectation

There are lots of low quality contributions:

  - UI guidelines and conventions are ignored
  - No backwards compatibility
  - Implemented quickly / hacky, where a bigger refactor would be needed

It is time consuming to bring patches and new developers up to speed,
especially if they don’t stick around long term. This is also an issue
with possible follow ups: Bug fixes, additional work like docs or
release logs are often done by others, as we often fail to keep people
involved there after a patch is merged. In the end it is often faster to
implement the feature/fix ourselves.

#### Documentation

We need more (experienced) developers, but also better documentation.
The documentation for compiling Blender and getting started is described
as good, but documentation for the architecture and overall design is
lacking. Especially for the core design and architecture of Blender,
better docs would be very helpful, to get a better understanding how
areas communicate to each other. Even experienced developers often spend
a lot of time talking to the original author or reading a lot of code,
before they can work in a new area. Things that could be explained in a
simple diagram end up taking several hours of reverse engineering. New
contributors are not likely to spend their limited time on this and give
up. The availability of docs varies a lot with the age of the code.
Newer features and areas usually have better documentation and are
easier to get started with than legacy ones. We also have to
differentiate between experienced developers (e.g. from corporations)
and hobbyist / enthusiasts. Our docs seem to be written more for the
second group.

### Possible improvements in the onboarding process

Many suggestions were made, how to improve things.

#### Response time & Quality expectation

In order to keep contributors happy we have to decrease our response
time. At the same time we need to ensure patches meet a certain quality
level so the actual reviewer can focus on the feature/fix, rather than
spending lots of time on trivial things like explaining code style. This
can be achieved by automating things or have more people help with the
review process. Some ideas:

  - Automate code style checks for submitted patches and have buildbot
    check if patches compile
  - Have a checklist on the patch submission form for things like UI
    guidelines, backwards compatibility, tests passing and code style.
  - Move to a better system overall like gitlab (pitfalls of arc have
    been mentioned several times).
  - Have junior reviewers do a first round of review: Tagging the
    correct project, check code style, testing functionality, spot
    obvious code errors. A code review playbook could be helpful here,
    similar to the bug triaging playbock we have [Triaging
    Playbook](https://wiki.blender.org/wiki/Process/Bug_Reports/Triaging_Playbook)
  - Encourage new contributors to reach out to developers on the forum
    or chat, and discuss a planned feature/fix and its design before
    working on the patch. This may avoid the issue of having to reject a
    patch because of hacky implementation / design.

There are also other ideas, like having a pre approved list of features,
that can be picked up by new contributors. But letting them work on
their own projects is often more fun, but still could be good to
maintain such a low hanging fruit list actively.

#### Documentation

Obviously, time needs to be spend on writing documentation, especially
for older areas. In general, anything that a developer can write about
his/her area (philosophy, structure and conventions) would be helpful\!
Other possible improvements are to have (additional) forms of
documentation like videos for topics like compilation and patch
submission. Documentation for processes and tools can also be a bit
overwhelming for new contributors, maybe too much and counter
productive.

#### Onboarding in the modules

I asked if modules do anything special in their onboarding and checked
what is working well and what isn’t: Most modules have a dedicated chat
room on blender.chat, answer questions there and are available to help
new contributors. This seems very important and helps in the
communication process. Some have (bi)weekly meetings to further foster
planning and communication. The Render & Cycles Module has lots of
corporate contributions that go via other channels or have NDAs
involved, but they try to move things into the regular development
process as much as possible.

## Modules

### Status of modules and where help is needed

#### Grease Pencil

Core members are volunteers but very passionate and the module is
running fine. Regular and smooth meetings. They are already working
together for five years, so it’s a very well balanced team. Passionate
artists like Daniel and Matias help a lot with great ideas. Help is
needed with EEVEE materials for Grease Pencil. Clément is on it, but
could use an update on status.

#### Render & Cycles

Development is pretty active, but incoming patches could be handled more
efficiently. Better regression testing, performance and GPU testing is
needed, and that is being worked on. There is especially need for
contributors to do maintenance and bug fixing. Artists members are
involved, getting help with images for release notes and feedback is
fine, but could use more help with docs, writing release logs…

#### Modeling

The module is doing okay but suffers from not having more dedicated
developers, most people involved spend time on different projects too.
So there is not enough time to focus on larger projects and there are no
regular meetings. Artists involvement is okay. Modeling is in some sense
in maintenance mode. Mostly smaller projects are done. Campbell would
like to plan more sprints / projects, 6-8 week tasks, like this
optimization work:

  - Blog:
    <https://code.blender.org/2021/05/mesh-editing-optimization-initial-steps/>
  - Progress: <https://developer.blender.org/T88550>

More developers are needed who learn and understand the modeling code.
Also it would help to keep track of feature requests on Right Click
Select and put the promising ones into a roadmap.

#### Bug triaging

They are happy with response time, but some reports still slip through.
More time to review fix patches (instead of feature patches) wished.
Issues with certain GPUs are still an issue. An updated list with
hardware used by / available to active developers would be good. They
can always need more triagers. Some sort of rewards/badges might
motivate people to do more.

#### Compositor & Render/Viewport

Compositor is doing ok, Render/Viewport could use more experienced
developers, especially with big projects like EEVVE2, Viewport
compositing and Vulkan.

#### Sculpt / Paint

They are redesigning the whole workflow for asset creation right now.
The legacy status of the code and the lack of development slows down
progress too much. They need more people involved full time and a more
flexible and faster way of decision making. They think about outsourcing
development (such as mesh processing algorithms), which don’t need to
happen within the Blender project.

#### VFX & Video

Not moving forward as much as one would expect. Some small development
is going on. Bigger projects are planned and have partial designs but
there is no experienced developer to really push them forward. Having
more people with experience will make it easier and more exciting.
Sergey reports that side projects like Cycles X make it difficult to
focus on module owner work. More developers with good design skills and
developing experience would be a very nice addition to the module.

#### Nodes & Physics

The module is doing quite well, at least the geometry-nodes part of it.
The rest of the Nodes & Physics module maybe not so much. Maybe because
no one feels fully responsible for the Nodes & Physics module, because
it contains so many unrelated things. Artist involvement was great with
several people joining the meetings, but that happens much less
regularly nowadays.

#### User Interface

The UI module activity is largely on hold. Developers are busy with
other projects and demotivated to work on smaller things due to many
controversies since 2.8. There's mostly a lack of quality design work
and general direction. Just adding more developers will not solve this.
Technical debt is a big issue too. They were working on Human Interface
Guidelines, which is an important project. They need workshops to define
a direction/vision and designers. Communication to other modules and the
public is seen as an issue. Somebody could help communicating the what,
how and why of UI development. HCI specialists to help professionalizing
the process would be helpful too. Later on they may want to look for
more UI developers.

#### EEVEE & Viewport

Bugs are stacking and there are often obscure driver dependent bugs.
Communication could be improved (bug tracker, review, roadmap). Clear
plans for large projects (GPU selection, Vulkan port) need to be made,
then additional developers could help.

## Improving the onboarding process

Talking to module owners and getting up to speed with everything was
very important and interesting this week. It was also great to connect
with everyone again, lot’s of familiar faces\! Unfortunately, long
standing issues in the onboarding process, like response time for patch
review and documentation is nothing that can be fixed quickly, but there
is definitely room for improvement.

### Improving patch review

As described in the previous chapters, response time and code quality
are the biggest issues here. Here are some ideas that can improve the
process and reduce the frustration for both new contributors and
reviewers.

#### Encourage communication before starting to code

Encourage new contributors to get in touch with us, before starting to
code can reduce the risk of work being wasted and not ending up in
Blender. When people reach out to developers, for example in
blender.chat or on devtalk, developers can point to relevant
documentation, check if the proposed change fits into Blenders design
and inform the contributor about problems to watch out for. This avoids
frustration, increases the chances of success and also brings everyone
up to speed with the project from the beginning. It can reduce the
response time as well, as both developer and contributor are connected
from the beginning and might feel more motivated to see the process
through. Another way to have an indirect communication is to maintain a
list of [good first
issues](https://developer.blender.org/maniphest/project/34/query/open/).
Of course it is more fun for new contributors to work on their own
projects, but having a list of pre approved and easy tasks can make the
first contact with Blender development and patch review less
frustrating, hence improve the chances of contributors staying on board
longer and trying more complicated things. Automate parts of the patch
review process As suggested already by developers in the survey, the
patch review process could be automated more, increasing code quality
and spotting errors before a first review is even done.

  - Have more information on the patch submission form, or requiring to
    check some boxes to confirm contributors checked code style,
    backward compatibility, UI guidelines…
  - Automatically check the submitted code for code style, have buildbot
    compile it…

#### Strengthen the Triaging module

According to the module description, they are already responsible for
“First round of Differential review “. We can try to get more people
involved to help with this. Benefits:

  - Have a quick first response. Contributors see that something is
    happening and that people are looking at the patch. We have patches
    that haven't had a (first) response in months.
  - First round of review reduces the work for the actual reviewer.
  - Things that can be checked during first round of review
      - Correct tagging of modules, reviewers…
      - Check on code style, compilation, see if UI guidelines are
        followed, test the feature, spot obvious code errors
      - This gives the contributor a chance to fix these things and have
        a more clean patch for the actual reviewer who needs to do less
        work and can focus on the actual feature and architecture of the
        patch.

### Documentation

Improving documentation, especially for core modules in Blender is very
important. I don’t know yet how this can be fixed efficiently, but
experienced developers should spend a bit of time on this. Maybe one
hour every week, that should be doable without making it to tedious. In
a first step, we should identify the most pressing issues, by asking
contributors and active developers which areas are most complex and in
need for better documentation. We should also check where the biggest
impact can be made with relative small amount of work.

### Project platform

Some developers mentioned a possible move to gitlab and added that
phabricator has its limitations and pitfalls (arc). This is more a
longterm project, but a better project platform could increase
efficiency, reduce work and make the contribution process easier. For
example the bug reporting process could be improved. Instead of trusting
people to fill out the template given, we could require information like
operating system, Blender version, an example .blend file… Or promote
the inbuilt functionality more: Help → Report a bug, which already fills
out some of the information.
