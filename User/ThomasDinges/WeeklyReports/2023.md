# 2023

## Calendar Week 1

Jan 2 - Jan 8

Mostly time off.

''' General '''

  - Posting the weekly notes.
  - Bcon2 for Blender 3.5 is delayed due to pending patches, will be
    checked on Monday 9th.

## Calendar Week 2

Jan 9 - Jan 15

This week I visited the Blender HQ in Amsterdam and had a lot of talks
and catching up with everyone. Thanks for having me.

It was also good to evaluate the past year and confirm the goals and
responsibilities of my role: Onboarding, release management, module
coordination and GSoC. To better summarize this, my formal role is
**Development Coordinator** from now on.

''' Development Coordination '''

  - Helped people, answered questions and moderated on our platforms
    (devtalk, developer, chat...)
  - Posted the weekly notes.

''' Release Management '''

  - Moved to Bcon2 on Tuesday, it was delayed a bit to give more time to
    Hans and Martijn to merge their patches.
  - Posted an [updated
    policy](https://lists.blender.org/pipermail/bf-committers/2023-January/051463.html)
    on handling high priority reports.
  - Checked on all existing high priority reports and asked developers
    for updates on them.

''' Development '''

  - [D16976](http://developer.blender.org/D16976): Cycles: Add AVX512
    kernel. (WIP)

## Calendar Week 3

Jan 16 - Jan 22

''' Development Coordination '''

  - Helped people, answered questions and moderated on our platforms
    (devtalk, developer, chat...)
  - Finished and posted the weekly dev notes.
  - Hosted the online kick-off meetings on Monday.

''' Release Management '''

  - LTS Updates (3.3.3 and 2.93.14) on Tuesday, tested builds, tagged
    git, updated website...
  - Fixed LTS script generating bad URLs
    ([rBc1d360f7](https://projects.blender.org/blender/blender/commit/c1d360f7fb88))

''' Module Coordination '''

  - Joined the [Sculpt / Paint / Texture module
    meeting](https://devtalk.blender.org/t/2023-01-17-sculpt-texture-paint-module-meeting/27316)
    on Tuesday.

''' Development '''

  - Cleanup: Remove unused light\_sample\_is\_light() function.
    [rBf31f7e3e](https://projects.blender.org/blender/blender/commit/f31f7e3ef0bd)

''' Triaging '''

  - Closed more than 40 reports that were pending in status "Needs
    Information from User", most of them since December.

## Calendar Week 4

Jan 23 - Jan 29

''' Development Coordination '''

  - Helped people, answered questions and moderated on our platforms
    (devtalk, developer, chat...)
  - Finished and posted the weekly dev notes.
  - Hosted the online kick-off meetings on Monday.

''' Module Coordination '''

To be more visible and approachable, I will regularly join module
meetings from now on. Also helps to better stay in the loop of ongoing
projects and development.

  - Joined the [Eevee/Viewport
    meeting](https://devtalk.blender.org/t/2023-01-23-eevee-viewport-module-meeting/27366)
    on Monday.
  - Joined the [Grease Pencil
    meeting](https://devtalk.blender.org/t/2023-01-23-grease-pencil-module-meeting/27368)
    on Monday.
  - Joined the [Geometry Nodes
    meeting](https://devtalk.blender.org/t/2023-01-24-geometry-nodes-sub-module-meeting/27367)
    on Tuesday.
  - Joined the Render & Cycles meeting on Tuesday.

''' GSoC '''

  - Checked on and updated our wiki docs and ideas page, will submit the
    organization submission next week.

''' Gitea migration '''

  - Send mail to
    [bf-commiters](https://lists.blender.org/pipermail/bf-committers/2023-January/051469.html)
    about the migration and also the 3.5 release delay.
  - Tested the new site and discussed issues and workflows with others.

## Calendar Week 5

Jan 30 - Feb 5

''' Development Coordination '''

  - Helped people, answered questions and moderated on our platforms
    (devtalk, developer, chat...)
  - Finished and posted the weekly dev notes.
  - Hosted the online kick-off meetings on Monday.

''' GSoC '''

  - Submitted the organization application to Google for GSoC 2023.

''' Gitea '''

  - Tested the new Gitea platform, reported issues, worked on docs...

## Calendar Week 6

Feb 6 - Feb 12

''' Development Coordination '''

  - Helped people, answered questions and moderated on our platforms
    (devtalk, developer, chat...)
  - Finished and posted the weekly dev notes.
  - Hosted the online kick-off meetings on Monday.

''' Gitea '''

  - Spent most week on Gitea, testing, adding back devs to the teams,
    help on the chat, docs...

## Calendar Week 7

Feb 13 - Feb 19

''' Development Coordination '''

  - Helped people, answered questions and moderated on our platforms
    (devtalk, developer, chat...)
  - Finished and posted the weekly dev notes.
  - Hosted the online kick-off meetings on Monday.

''' Meetings '''

  - Admin meeting on Tuesday

''' Gitea '''

  - Spent some time again on account support, answering questions about
    workflows etc.

''' Release Management '''

  - [Move to Bcon3 on
    Wednesday](https://lists.blender.org/pipermail/bf-committers/2023-February/051482.html):
    Created the Blender 3.5 release branch, version bumps, splash and
    updated websites / docs.

## Calendar Week 8

Feb 20 - Feb 26

''' Development Coordination '''

  - Helped people, answered questions and moderated on our platforms
    (devtalk, developer, chat...)
  - Finished and posted the weekly dev notes.
  - Hosted the online kick-off meetings on Monday.

''' Onboarding '''

  - Offered the [Open
    Chat](https://devtalk.blender.org/t/developer-community-coordinator-weekly-open-chat/21578)
    again, this week on Thursday.

''' Meetings '''

  - Cycles module meeting on Tuesday

''' Release Management '''

  - Little bit of work on the 2.93.15 and 3.3.4 LTS updates on Tuesday,
    version bump and updating milestones.
  - Updated text on the [release
    cycle](../../../Process/Release_Cycle.md) wiki page and the 4.0
    milestone.

''' Google Summer of Code '''

  - Blender has been accepted again to participate in this years GSoC.
    Sent announcement mail to
    [bf-commiters](https://lists.blender.org/pipermail/bf-committers/2023-February/051489.html),
    updated the [wiki page](../../../GSoC/index.md) and posted a thread with
    infos on
    [devtalk](https://devtalk.blender.org/t/blender-has-been-accepted-for-gsoc-2023/27861).

## Calendar Week 9

Feb 27 - Mar 5

''' Development Coordination '''

  - Helped people, answered questions and moderated on our platforms
    (devtalk, developer, chat...)
  - Hosted the online kick-off meetings on Monday.
  - Finished and posted the weekly dev notes.

''' Onboarding '''

  - Offered the [Open
    Chat](https://devtalk.blender.org/t/developer-community-coordinator-weekly-open-chat/21578)
    again, this week on Tuesday.
  - Onboarding call with Harley.

''' Module Coordination '''

  - Joined the [EEVEE & Viewport
    meeting](https://devtalk.blender.org/t/2023-02-27-eevee-viewport-module-meeting/27962)
    on Monday.

''' Triaging '''

  - Did some bug triaging.

## Calendar Week 10

Mar 6 - Mar 12

''' Development Coordination '''

  - Helped people, answered questions and moderated on our platforms
    (devtalk, developer, chat...)
  - Hosted the online kick-off meetings on Monday.
  - Finished and posted the weekly dev notes.

''' Module Coordination '''

  - Joined the Animation & Rigging module meeting on Thursday.

''' Release Management '''

  - Added a task to get a better overview of all [planned breaking
    changes for
    Blender 4.0](https://projects.blender.org/blender/blender/issues/105523).

''' Onboarding '''

  - Offered the [Open
    Chat](https://devtalk.blender.org/t/developer-community-coordinator-weekly-open-chat/21578)
    again, this week on Wednesday. Many people joined this week,
    especially interested in contributing to GSoC.

''' Triaging '''

  - Did some bug triaging again.

## Calendar Week 11

Mar 13 - Mar 19

''' Development Coordination '''

  - Helped people, answered questions and moderated on our platforms
    (devtalk, developer, chat...)
  - Hosted the online kick-off meetings on Monday.
  - Finished and posted the weekly dev notes.

''' Meetings '''

  - Admin meeting on Tuesday

''' Release Management '''

  - Went over all high priority reports and asked for updates.
  - Set up Linux environment again to work on the THIRD-PARTY-LICENSES
    update for 3.5.

''' Onboarding '''

  - Offered the [Open
    Chat](https://devtalk.blender.org/t/developer-community-coordinator-weekly-open-chat/21578)
    again, this week on Thursday.

''' Module Coordination '''

  - Joined the [EEVEE &
    Viewport](https://devtalk.blender.org/t/2023-03-13-eevee-viewport-module-meeting/28276)
    module meeting on Monday.

## Calendar Week 12

Mar 20 - Mar 26

''' Development Coordination '''

  - Helped people, answered questions and moderated on our platforms
    (devtalk, developer, chat...)
  - Hosted the online kick-off meetings on Monday.
  - Finished and posted the weekly dev notes.

''' GSoC '''

  - Contributors Sign-up is open since Monday. Answered questions on the
    chat, invited more mentors to help with reviewing proposals etc.

''' Release management '''

  - Helped a bit with the LTS updates on Tuesday (version char bump,
    tagging git)
  - Updated the THIRD-PARTY-LICENSES.txt for 3.5.
    [dd0d572935](https://projects.blender.org/blender/blender/commit/dd0d5729351919e1a6075a23318a9f8283394829)
  - Updated the freedesktop file for 3.5.
    [6cf19b2f79](https://projects.blender.org/blender/blender/commit/6cf19b2f7931b1dc7d76b983e823970592b9929f)
  - Blender 3.5 moved to Bcon4 on wednesday, updated websites and [sent
    out a mail to the
    ML.](https://lists.blender.org/pipermail/bf-committers/2023-March/051500.html)

''' Onboarding '''

  - Offered the [Open
    Chat](https://devtalk.blender.org/t/developer-community-coordinator-weekly-open-chat/21578)
    again, this week on Friday.

''' Module Coordination '''

  - Joined the [Render &
    Cycles](https://devtalk.blender.org/t/2023-03-21-render-cycles-meeting/28459)
    module meeting on Tuesday.
  - Joined the [Pipeline, Assets & I/O
    Meeting](https://devtalk.blender.org/t/2023-03-23-pipeline-assets-i-o-meeting/28212)
    module meeting on Thursday.

## Calendar Week 13

Mar 27 - April 2

''' Development Coordination '''

  - Helped people, answered questions and moderated on our platforms
    (devtalk, developer, chat...)
  - Hosted the online kick-off meetings on Monday.
  - Finished and posted the weekly dev notes.

''' GSoC '''

  - Answered questions again on the chat, invited more mentors to help
    with reviewing proposals etc.

''' Release management '''

  - Blender 3.5 release on wednesday. Moved release cycle to
    [bcon5](https://lists.blender.org/pipermail/bf-committers/2023-March/051504.html),
    tested builds, uploded builds to the stores, updated website/chat,
    release credits etc.

## Calendar Week 14

April 3 - April 9

''' Development Coordination '''

  - Helped people, answered questions and moderated on our platforms
    (devtalk, developer, chat...)
  - Hosted the online kick-off meetings on Monday.
  - Finished and posted the weekly dev notes.

''' GSoC '''

  - Started reviewing proposals with mentors. Updated website etc with
    new deadline.

''' Release management '''

  - Checked on incoming reports and issues, 3.5.1 is not urgent and can
    be released after Easter.

''' Meetings '''

  - Joined the [Render &
    Cycles](https://devtalk.blender.org/t/2023-04-04-render-cycles-meeting/28766)
    module meeting on Tuesday.
  - Meeting with Arnd, Danny and Aaron to discuss git-lfs and moving the
    manual to git.

## Calendar Week 15

April 10 - April 16

''' Development Coordination '''

  - Helped people, answered questions and moderated on our platforms
    (devtalk, developer, chat...)
  - Hosted the online kick-off meetings on Monday.
  - Finished and posted the weekly dev notes.

''' Meetings '''

  - Admin meeting on Tuesday.

''' Devtalk '''

  - Worked further on the devtalk reorganization proposal, has been
    accepted by admins and module owners and will be implemented now.

Shorter week due to Easter.

## Calendar Week 16

April 17 - April 23

''' Development Coordination '''

  - Helped people, answered questions and moderated on our platforms
    (devtalk, developer, chat...)
  - Hosted the online kick-off meetings on Monday.
  - Finished and posted the weekly dev notes.

''' Google Summer of code '''

  - Further coordination to make sure all proposals are reviewied in
    time for the deadline next week.

''' Release Management '''

  - [Backported
    fixes](https://projects.blender.org/blender/blender/issues/106258)
    to the 3.5 branch for the upcoming 3.5.1 bugfix release.

''' Onboarding '''

  - Offered the [Open
    Chat](https://devtalk.blender.org/t/developer-community-coordinator-weekly-open-chat/21578)
    again, this week on Tuesday.

''' Meetings '''

  - Joined the [Grease
    Pencil](https://devtalk.blender.org/t/2023-04-17-grease-pencil-module-meeting/28996)
    module meeting on Monday.
  - Joined the Render & Cycles module meeting on Tuesday.

## Calendar Week 17

April 24 - April 30

''' Development Coordination '''

  - Helped people, answered questions and moderated on our platforms
    (devtalk, developer, chat...)
  - Hosted the online kick-off meetings on Monday.
  - Finished and posted the weekly dev notes.

''' Release Management '''

  - Released Blender 3.5.1 on Tuesday with more then 59 bugfixes
    compared to 3.5.0. Updated websites, helped with store deployment,
    git tagging...

''' Devtalk re-categorization '''

  - Implemented the new Devtalk 2.0 proposal, with new categories. See
    [the thread on
    devtalk](https://devtalk.blender.org/t/devtalk-category-reorganization/29152)
    for more information.

''' Google Summer of code '''

  - Finalized the submission of accepted projects. Google will announce
    them on May 3rd (publicly on May 4th).

## Calendar Week 18

May 1 - May 7

''' Development Coordination '''

  - Helped people, answered questions and moderated on our platforms
    (devtalk, developer, chat...)
  - Hosted the online kick-off meetings on Monday.
  - Finished and posted the weekly dev notes.
  - Helped with the Annual Report 2022 (developer descriptions and
    collecting images)

''' Meetings '''

  - Joined the Nodes & Physics meeting on Tuesday.
  - Joined the Render & Cycles meeting on Tuesday.

''' Google Summer of Code '''

  - Accepted projects have been announced on May 4, published a [blog
    post](https://code.blender.org/2023/05/google-summer-of-code-2023/)
    about them.

''' Development '''

  - Fix compilation on Windows.
    [8775cf804e](https://projects.blender.org/blender/blender/commit/8775cf804ec369820e4afba5230fe9cb122eac0d)

## Calendar Week 19

May 8 - May 14

''' Development Coordination '''

  - Helped people, answered questions and moderated on our platforms
    (devtalk, developer, chat...)
  - Hosted the online kick-off meetings on Monday.
  - Finished and posted the weekly dev notes.

''' Onboarding '''

  - Offered the [Open
    Chat](https://devtalk.blender.org/t/developer-community-coordinator-weekly-open-chat/21578)
    again, this week on Friday.

''' Meetings '''

  - Joined the Admin meeting on Tuesday

''' Google Summer of Code '''

  - Onboarded the contributors, sent them a mail with infos about the
    chat, devtalk and where to post their thread for weekly reports etc.

## Calendar Week 20

May 15 - May 21

''' Development Coordination '''

  - Helped people, answered questions and moderated on our platforms
    (devtalk, developer, chat...)
  - Hosted the online kick-off meetings on Monday.
  - Finished and posted the weekly dev notes.

''' Meetings '''

  - Joined the Render & Cycles meeting on Tuesday

''' Onboarding '''

  - Offered the [Open
    Chat](https://devtalk.blender.org/t/developer-community-coordinator-weekly-open-chat/21578)
    again, this week on Friday.

''' Release Management '''

  - Created the blender-v3.6-release branch for Bcon3, commited version
    bumps and splash. Updated websites, release notes, sent out
    announcements etc

## Calendar Week 21

May 22 - May 28

''' Development Coordination '''

  - Helped people, answered questions and moderated on our platforms
    (devtalk, developer, chat...)
  - Hosted the online kick-off meetings on Monday.
  - Finished and posted the weekly dev notes.

''' Onboarding '''

  - Offered the [Open
    Chat](https://devtalk.blender.org/t/developer-community-coordinator-weekly-open-chat/21578)
    again, this week on Thursday.

''' Release Management '''

  - Helped a bit with LTS releases on Tuesday (3.3 and 2.93). This will
    be likely the last 2.93 update.

''' Google Summer of Code '''

  - Onboarding meeting with students and mentors on Wednesday. It was a
    good opoortunity to get to know each other and answer open
    questions. The coding period starts the coming week.

## Calendar Week 22

May 29 - June 4

Time off.

## Calendar Week 23

June 5 - June 11

Time off.

## Calendar Week 24

June 12 - June 18

''' General '''

  - Catching up on mails and messages.

''' Development Coordination '''

  - Helped people, answered questions and moderated on our platforms
    (devtalk, developer, chat...)
  - Hosted the online kick-off meetings on Monday.
  - Finished and posted the weekly dev notes.

''' Release Management '''

  - Checking on high priority bug reports, prepared updates to the
    THIRD-PARTY-LICENSES.txt file.

''' Meetings '''

  - Joined the Render & Cycles meeting on Tuesday.

## Calendar Week 25

June 19 - June 25

''' Development Coordination '''

  - Helped people, answered questions and moderated on our platforms
    (devtalk, developer, chat...)
  - Hosted the online kick-off meetings on Monday.
  - Finished and posted the weekly dev notes.

''' Release Management '''

  - Helped with the 3.3.8 LTS release (testing on stores mainly).
  - Blender 3.6 entered BCon4.
      - Sent infos to the [mailing
        list](https://lists.blender.org/pipermail/bf-committers/2023-June/051526.html)
      - Updated the third party lib license doc.
        [4b0dc9eb29](https://projects.blender.org/blender/blender/commit/4b0dc9eb2956b7257bc88f6fffce7ac533525c8b)
      - Updated freedesktop file.
        [80e518bdd4](https://projects.blender.org/blender/blender/commit/80e518bdd49170b10a68f7febb7f8e590fbf4fde)

''' Onboarding '''

  - Offered the [Open
    Chat](https://devtalk.blender.org/t/developer-community-coordinator-weekly-open-chat/21578)
    again, this week on Thursday.

## Calendar Week 26

June 26 - July 2

''' Development Coordination '''

  - Helped people, answered questions and moderated on our platforms
    (devtalk, developer, chat...)
  - Hosted the online kick-off meetings on Monday.
  - Finished and posted the weekly dev notes.

''' Meetings '''

  - Joined the [EEVEE & Viewport
    meeting](https://devtalk.blender.org/t/2023-06-26-eevee-viewport-module-meeting/30031)
    on Monday.

''' Release Management '''

  - Blender 3.6 LTS was released on Tuesday
  - Final git commits
      - [fad70313d9](https://projects.blender.org/blender/blender/commit/fad70313d9f8f55a7446daa1460879b32d874525)
        Release: Bump to 3.6.0 - release.
      - [08ffb77279](https://projects.blender.org/blender/blender/commit/08ffb77279b8d91dee773f586d0876b3a5c7d7dd)
        Release: Updates for freedesktop file.
      - [91ca4f581b](https://projects.blender.org/blender/blender/commit/91ca4f581bcdd1f502d6363e2142a82744205d11)
        Release: Update license file for OpenSSL 3.0.9.
  - Updated websites (credits, release notes on the wiki, chat, projects
    landing page...)
  - Git tagging, testing and deployment to the stores (Microsoft, Steam,
    Snap)

''' Onboarding '''

  - Offered the [Open
    Chat](https://devtalk.blender.org/t/developer-community-coordinator-weekly-open-chat/21578)
    again, this week on Friday.

## Calendar Week 27

July 3 - July 9

''' Development Coordination '''

  - Helped people, answered questions and moderated on our platforms
    (devtalk, developer, chat...)
  - Hosted the online kick-off meetings on Monday.
  - Finished and posted the weekly dev notes.

''' Google Summer of Code '''

  - The midterm evaluation is coming up, talked with mentors and checked
    on contributors status.

''' Meetings '''

  - Joined the [VFX & Video
    meeting](https://devtalk.blender.org/t/2023-07-04-vfx-video/30164)
    on Tuesday.

''' Development '''

  - Read about the Gitea API, I want to improve the current High
    Priority reporting script.

## Calendar Week 28

July 10 - July 16

''' Development Coordination '''

  - Helped people, answered questions and moderated on our platforms
    (devtalk, developer, chat...)
  - Hosted the online kick-off meetings on Monday.
  - Finished and posted the weekly dev notes.

''' Onboarding '''

  - Offered the [Open
    Chat](https://devtalk.blender.org/t/developer-community-coordinator-weekly-open-chat/21578)
    again, this week on Thursday.

''' Meetings '''

  - Joined the [EEVEE & Viewport
    meeting](https://devtalk.blender.org/t/2023-07-10-eevee-viewport-module-meeting/30237)
    on Monday.

''' Google Summer of Code '''

  - Made sure all midterm evaluations were submitted.

''' Gitea '''

  - Started to move inactive contributor accounts to "All-Time" teams,
    for example
    [All-Time-Developers](https://projects.blender.org/org/blender/teams/all-time-developers)

## Calendar Week 29

July 17 - July 23

''' Development Coordination '''

  - Helped people, answered questions and moderated on our platforms
    (devtalk, developer, chat...)
  - Hosted the online kick-off meetings on Monday.
  - Finished and posted the weekly dev notes.

''' Release Management '''

  - Helped with the LTS updates (3.3.9 and 3.6.1) on tuesday.

''' Gitea '''

  - Finished moving inactive accounts to "All-Time-" teamsm and [posted
    an
    announcement](https://devtalk.blender.org/t/inactive-contributor-accounts-on-projects-blender-org/30397)
    on devtalk.

## Calendar Week 30

July 24 - July 30

''' Development Coordination '''

  - Helped people, answered questions and moderated on our platforms
    (devtalk, developer, chat...)
  - Hosted the online kick-off meetings on Monday.
  - Finished and posted the weekly dev notes.

''' Onboarding '''

  - Offered the [Open
    Chat](https://devtalk.blender.org/t/developer-community-coordinator-weekly-open-chat/21578)
    again, this week on Thursday.

''' Documentation '''

  - Made the wiki page on [using the stabilizing
    branch](../../../Process/Using_Stabilizing_Branch.md) clearer,
    especially what to do after the initial main release.
  - Removed remaining references to the sunsetted mailing lists on the
    wiki and the module pages on projects.blender.org

''' Release Planing '''

  - Checked with all modules if there is a need to delay Blender 4.0 by
    about a month (release end of December then). Will be further
    discussed with admins.

## Calendar Week 31

July 31 - Aug 6

''' Development Coordination '''

  - Helped people, answered questions and moderated on our platforms
    (devtalk, developer, chat...)
  - Hosted the online kick-off meetings on Monday.
  - Finished and posted the weekly dev notes.

*' Meeting*'

  - Joined the admin meeting on Tuesday. We agreed to stick with the
    original planning for 4.0 and rather delay some targets to 4.1.

## Calendar Week 32

Aug 7 - Aug 13

''' Development Coordination '''

  - Helped people, answered questions and moderated on our platforms
    (devtalk, developer, chat...)
  - Finished and posted the weekly dev notes.

''' Onboarding '''

  - Offered the [Open
    Chat](https://devtalk.blender.org/t/developer-community-coordinator-weekly-open-chat/21578)
    again, this week on Tuesday.

''' Triaging '''

  - Closed various old bug reports or asked for updates.

''' Meetings '''

  - Various internal meetings.

## Calendar Week 33

Aug 14 - Aug 20

''' Development Coordination '''

  - Helped people, answered questions and moderated on our platforms
    (devtalk, developer, chat...)
  - Hosted the online kick-off meetings on Monday.
  - Finished and posted the weekly dev notes.

''' Meetings '''

  - Joined the [User Interface
    meeting](https://devtalk.blender.org/t/2023-08-15-user-interface/30710)
    on tuesday.

''' Release management '''

  - The LTS releases (3.3.10 and 3.6.2) were postponed from Tuesday to
    Thursday due to a missing Embree library update. Helped with release
    tests etc.

## Calendar Week 34

Aug 21 - Aug 27

Time off.

## Calendar Week 35

Aug 28 - Sep 3

Time off.

## Calendar Week 36

Sep 4 - Sep 10

''' General '''

  - Catching up with e-mails, messages and development that happened in
    the last two weeks.

''' Development Coordination '''

  - Helped people, answered questions and moderated on our platforms
    (devtalk, developer, chat...)
  - Hosted the online kick-off meetings on Monday.
  - Finished and posted the weekly dev notes.

''' Meetings '''

  - Joined the [Render &
    Cycles](https://devtalk.blender.org/t/2023-09-05-render-cycles-meeting/30692)
    meeting on Tuesday.

''' Modules '''

  - Checked on high priority bug reports and reminded / asked people to
    fix them.

## Calendar Week 37

Sep 11 - Sep 17

''' Development Coordination '''

  - Helped people, answered questions and moderated on our platforms
    (devtalk, developer, chat...)
  - Hosted the online kick-off meetings on Monday.
  - Finished and posted the weekly dev notes.

''' Meetings '''

  - Joined the admin meeting on Tuesday.

''' Modules '''

  - Checked on high priority bug reports and reminded / asked people to
    fix them.

## Calendar Week 38

Sep 18 - Sep 24

''' Development Coordination '''

  - Helped people, answered questions and moderated on our platforms
    (devtalk, developer, chat...)
  - Hosted the online kick-off meetings on Monday.
  - Finished and posted the weekly dev notes.

''' Onboarding '''

  - Offered the [Open
    Chat](https://devtalk.blender.org/t/developer-community-coordinator-weekly-open-chat/21578)
    again, this week on Wednesday.

''' Documentation '''

  - Updated the [add-on page](../../../Process/Addons/index.md), was still
    mentioning submodules and old git urls.

''' Release Management '''

  - Helped with the LTS Updates (3.6.3 and 3.3.11) on Thursday.
      - 3.6.4 is already scheduled for next week Tuesday, due to two
        regressions.

''' Meetings '''

  - Joined the [Grease
    Pencil](https://devtalk.blender.org/t/2023-09-18-grease-pencil-module-meeting/31203)
    meeting on Monday
  - Joined the Render & Cycles meeting on Tuesday

## Calendar Week 39

Sep 25 - Oct 1

''' Development Coordination '''

  - Helped people, answered questions and moderated on our platforms
    (devtalk, developer, chat...)
  - Hosted the online kick-off meetings on Monday.
  - Finished and posted the weekly dev notes.

''' Meetings '''

  - Joined the [UI
    meeting](https://devtalk.blender.org/t/2023-09-26-user-interface-meeting/31112)
    on Tuesday.

''' Release Management '''

  - Hotfix release for 3.6.4 on Tuesday, helped with platform testing.
  - Blender 4.0 went to Bcon3 on Wednesday.
      - Updated the websites, gitea, chat, wiki and posted an
        announcement to
        [devtalk](https://devtalk.blender.org/t/release-cycle-now-at-4-0-bcon3-and-4-1-bcon1/31315).
      - Created a new gitea milestone for 4.2, and release notes for 4.1
      - Created the 4.0 release branch, commited version bumps etc

## Calendar Week 40

Oct 2 - Oct 8

''' Development Coordination '''

  - Helped people, answered questions and moderated on our platforms
    (devtalk, developer, chat...)
  - Hosted the online kick-off meetings on Monday.
  - Finished and posted the weekly dev notes.

''' Triaging '''

  - Checked on my Gitea notifications, updated reports, closed old ones
    etc.

''' Meetings '''

  - Joined the Grease Pencil meeting on Thursday.
  - Joined the [Animation &
    Rigging](https://devtalk.blender.org/t/2023-10-05-animation-rigging-module-meeting/31420)
    meeting on Thursday.

## Calendar Week 41

Oct 9 - Oct 15

''' Development Coordination '''

  - Helped people, answered questions and moderated on our platforms
    (devtalk, developer, chat...)
  - Hosted the online kick-off meetings on Monday.
  - Finished and posted the weekly dev notes.

''' Meetings '''

  - [Admin
    meeting](https://devtalk.blender.org/t/2023-10-10-blender-admins-meeting/31543)
    on Tuesday

''' Release management '''

  - Commited new 4.0 splash screen.

## Calendar Week 42

Oct 16 - Oct 22

''' Development Coordination '''

  - Helped people, answered questions and moderated on our platforms
    (devtalk, developer, chat...)
  - Hosted the online kick-off meetings on Monday.
  - Finished and posted the weekly dev notes.

''' Release Management '''

  - Helped with the LTS updates on Thursday.

## Calendar Week 43

Oct 23 - Oct 29

''' Development Coordination '''

  - Helped people, answered questions and moderated on our platforms
    (devtalk, developer, chat...)
  - Hosted the online kick-off meetings on Monday.
  - Finished and posted the weekly dev notes.

''' Blender Conference '''

  - Coordinated the Lightning Talks for the conference.
  - Travel to Amsterdam and conference\!

## Calendar Week 44

Oct 30 - Nov 5

''' Development Coordination '''

  - Helped people, answered questions and moderated on our platforms
    (devtalk, developer, chat...)
  - Hosted the online kick-off meetings on Monday.
  - Finished and posted the weekly dev notes.

''' Blender Conference '''

  - Spent Monday and Tuesday in the office in Amsterdam to catch up with
    everyone.
  - Travel back to Germany.

''' Release Management '''

  - Checked on high priority reports again and asked developers to fix
    remaining showstoppers for 4.0.
  - [Extended the 4.0 release cycle by one
    week](https://devtalk.blender.org/t/blender-4-0-relese-cycle-extended-by-one-week/31880)
    to give people more time for fixes and docs.

## Calendar Week 45

Nov 6 - Nov 12

''' Development Coordination '''

  - Helped people, answered questions and moderated on our platforms
    (devtalk, developer, chat...)
  - Hosted the online kick-off meetings on Monday.
  - Finished and posted the weekly dev notes.

''' Documentation '''

  - Updated [minimum system
    requirements](https://www.blender.org/download/requirements/) for
    Blender to Intel Browadwell GPUs and CPUs with SSE4.2.
  - Investigated how rna\_manual\_reference.py works and checked on the
    updater script (rna\_manual\_reference\_updater.py). Talked with
    Aaron about keeping this updated more frequently.

''' Release Management '''

  - Checked on high priority reports and informed developers about them
    / asked for updates.
  - Updated the licenses document and freedesktop file:
      - [176df4bead](https://projects.blender.org/blender/blender/commit/176df4bead130e5421a3043c8ac54a9bf40008a7)
        Release: Update licenses doc and freedesktop file for 4.0
      - [574b0aa4dc](https://projects.blender.org/blender/oss-attribution-builder/commit/574b0aa4dc2bdc683e84f42853ecc5e2bbefd85e)
        Blender 4.0 database dump.

## Calendar Week 46

Nov 13 - Nov 19

Release week, spent most time on getting 4.0 into the wild\!

''' Development Coordination '''

  - Helped people, answered questions and moderated on our platforms
    (devtalk, developer, chat...)
  - Hosted the online kick-off meetings on Monday.
  - Finished and posted the weekly dev notes.

''' Documentation '''

  - Discussed ideas to improve the
    [requirements](https://www.blender.org/download/requirements/) page.
    Created a google doc with minimum and recommended values for all
    platforms. Idea is to have three tabs (one per OS), and make the
    page much clearer and easier to scan.

''' Release Management '''

  - 4.0 release on Tuesday. Checked on builds, uploaded to stores,
    updated credits page, wiki, gitea, chat...
  - 4.0.1 hotfix release on Friday. I deployed to stores, created
    release notes, git tagging etc...

''' Triaging '''

  - Kept a close eye on the bugtracker this week. Especially crash
    issues and issues releated to the new bumped hardware requirements.

## Calendar Week 47

Nov 20 - Nov 26

''' Development Coordination '''

  - Helped people, answered questions and moderated on our platforms
    (devtalk, developer, chat...)
  - Hosted the online kick-off meetings on Monday.
  - Finished and posted the weekly dev notes.

''' Release Management '''

  - Helped a bit with LTS updates on Tuesday, but they were then
    postponed by one week due to the cyberattack.

## Calendar Week 48

Nov 27 - Dec 3

''' Development Coordination '''

  - Helped people, answered questions and moderated on our platforms
    (devtalk, developer, chat...)
  - Hosted the online kick-off meetings on Monday.
  - Finished and posted the weekly dev notes.

''' Release Management '''

  - Backported all [remaining commits from the
    list](https://projects.blender.org/blender/blender/issues/114706)
    for 4.0.2.

## Calendar Week 49

Dec 4 - Dec 10

''' Development Coordination '''

  - Helped people, answered questions and moderated on our platforms
    (devtalk, developer, chat...)
  - Hosted the online kick-off meetings on Monday.
  - Finished and posted the weekly dev notes.

''' Release Management '''

  - Released 4.0.2 on Tuesday, deployed to stores, wrote release notes,
    tagged git ...

''' Campaign '''

  - Helped with testing and PR of the 30th anniversary campaign

## Calendar Week 50

Dec 11 - Dec 17
