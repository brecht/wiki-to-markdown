## Week 1: May 27 - 30

  - Added more operations to the *Vector Math* node and implemented
    dynamic inputs.
    [https://developer.blender.org/rBa52006c2fe879099b675c983d29b3373af016e8a
    Ba52006](https://developer.blender.org/rBa52006c2fe879099b675c983d29b3373af016e8a_Ba52006)
  - Implemented dynamic inputs to the *Math* node.
    [https://developer.blender.org/rBfac13b10a256ad194d069f585dcb2aa251c7f81a
    Bfac13b](https://developer.blender.org/rBfac13b10a256ad194d069f585dcb2aa251c7f81a_Bfac13b)
  - Changed the default *Vector Socket* drawing method. Introduced
    *Compact Sockets*.
    [https://developer.blender.org/rB18be4b8866d33b753a0fb9d6971249f1fe5e262d
    B18be4b](https://developer.blender.org/rB18be4b8866d33b753a0fb9d6971249f1fe5e262d_B18be4b)
