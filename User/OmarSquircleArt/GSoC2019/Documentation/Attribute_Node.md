This document proposes an improved *Attribute Node* that provide wider
access of Blender's data to the render targets.

## Current State

Currently, the attribute node takes a string representing the identifier
of an attribute as an input. The possible identifiers are very few, in
particular, the attributes that are not possible to get through other
nodes are as follows:

  - \`density\`, \`flame\`, \`temperature\`, and \`color\` of the smoke
    of a domain object.
  - Vertex color.

If a vertex color map have the same name as a UV map, the UV map is
returned. No warnings or errors are displayed if the attribute doesn't
exist. The node returns three outputs, only one of them should be used,
the other two are implicitly derived from it.

## The Problem

The attribute node suffer from the following problems:

  - It is not intuitive to use, especially for artists. Users have to
    memorize and write identifiers, and a misspelled identifier can
    easily go unnoticed.
  - The available attributes are very few and limited.

## Proposal

The following points outlines my proposal for the attribute node:

  - Specific attributes can be moved to dedicated nodes. For instance,
    \`density\`, \`flame\`, \`temperature\`, and \`color\` attributes
    can have their own *Volume Info* node. Moreover, vertex colors can
    have their own *Vertex Color* node. This will improve the user
    experience and remove the ambiguity when similar names exist.
  - The attribute node will have an enum (menu) that lets the user
    choose the source of the attribute, which can be an ID or a Struct.
    The possible sources are those than can be associated with the
    shader. For instance, the menu will contain:
      - Object
      - Material
      - Mesh
      - Vertices
      - Polygons
      - Loops
      - ...
  - If possible, a secondary enum (menu) should list the available
    attributes. Those are any boolean, scalar, or vector property,
    including custom properties. (I am not sure if this is possible, so
    this point is subject to change)
