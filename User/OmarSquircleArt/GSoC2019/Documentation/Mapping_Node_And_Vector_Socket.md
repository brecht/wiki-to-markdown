# Mapping Node And Vector Socket

Users have asked for a more dynamic *Mapping* node, that is, the node
should have explicit sockets for its transformation data. This allows
variable transformations of texture coordinates to perform deformations
and the like. While the idea is simple, it have certain implications on
the user experience. In this document, I shall mention some of those
implications and propose solutions to them.

## Vector Socket

Currently, when a vector socket is not linked, the vector socket is
drawn using a component menu. This means the user have to click on a
button to open a menu that allows the vector to be edited. This
behaviour is bad for certain nodes that require a high level of
interactivity, such as the *Mapping* node. This extra click (or two
clicks) required to edit the vector hinders the user experience. And
this problem will be apparent when the *Mapping* node gets sockets.

A solution would be to draw the vector socket using a column layout like
most of the Blender interface. However, a column layout would take
significantly more space. And some sockets may not need that space
because they are not edited interactively or they are linked almost all
the time, this includes the *Input Vector* of the *Mapping* node. So,
another solution would be to introduce a \`SOCK\_COMPACT\` socket flag,
which if set, a more compact representation of the socket is drawn if
available. This could be used for any socket and not just vector
sockets.

## Mapping Node Interface

Introducing sockets would mean the node will get slimmer and taller.
Consequently, it is best that the *Vector Type* be converted to a
drop-down enum.

Moreover, the Min and Max vectors could be removed altogether from the
node, where their job is provided by one or two *Vector Math* nodes.
Those options never belonged to the node and were nearly never used as
most users relied on the extrapolation option in the *Image* node to
perform standard clipping.
