# Procedural Textures

This document covers the possible improvements to procedural textures in
Cycles/EEVEE. User input is of essence here, so this document also serve
as a guideline for discussing the topic and eventually deciding on the
final set of features and improvements that we shall implement.

The discussion is taking place in the following [devtalk
thread](https://devtalk.blender.org/t/improve-cycles-eevee-for-procedural-content-creation/7186).
Feel free to engage in the discussion and weight in with your opinions.

## Higher Dimensional Textures

Textures like *Noise*, *Voronoi*, and *Musgrave* can be evaluates in a
3D space. Often, it is useful to evaluate those textures in a 4D space,
the fourth dimension typically being a function of time. A 4D evaluation
of the textures can also be utilized to create a loopable 2D animation
by evaluating the the textures on a particular 4D path. So extending
those textures to a higher dimension can be rewarding for artists and
TDs.

Extending those textures to a higher dimension, however, is not without
its complications. In particular, the computational cost and user
experience.

### User Experience

Evaluating a 4D texture means the user have to supply a 4D vector, which
tends to be rather confusing for artists. So, typically, we split the 4D
vector into a 3D vector and a scalar. This scalar is usually labeled
*Time* or *Evolution*. We are most likely going to use this same
structure to implement our nodes as it is more artist friendly.

### Computational Cost

It is obvious that a higher dimensional texture will be more
computationally demanding and expensive. While there is no way around
this, it makes no sense to add that overhead for all users if only a
small subset of users will utilize those higher dimensions. In other
words, most users will keep the higher dimensions constant.

A potential fix for that is to implement multiple versions of the
textures for 2D, 3D, and 4D dimensions, either as separate
implementations or as a generic algorithm if possible. Giving the user
the ability to choose the target dimension as an option.

Moreover, we could change the textures computation methods to methods
that are more suited for higher dimensions, something like *Open
Simplex* noise as an alternative to *Perlin Noise* for instance.

### Conclusion

Our point of discussion should be around those questions:

  - Which textures should support 4D evaluations, if any?
  - How should 4D evaluations happen? UX/UI considerations.
  - Should we have separate implementation for different dimensions? Or
    should there just be a single implementation of the highest
    dimension?
  - Should we look into alternative methods like *Open Simplex*?
  - …

## More Flexible Textures

Users have asked for more noise types and more flexibility for existing
types. But there isn’t an official list of the features that will be
implemented yet. This list will be formalized after the discussion in
the [devtalk
thread](https://devtalk.blender.org/t/improve-cycles-eevee-for-procedural-content-creation/7186),
so please engage in the discussion if you have any proposals.

### Guidlines

We should try to follow the following guidelines during the discussion:

  - Requested textures should be those that are otherwise hard or
    impossible to create using other textures.
  - Requested textures should be general and not specific to a certain
    situation.
  - Avoid requests like *“A texture that look like this”*. Provide as
    much information as you can.

### Conclusion

Our point of discussion should be around those questions:

  - What new textures should be implemented?
  - What lack of flexibility should be handled?
  - What textures are not behaving as expected?
  - …
