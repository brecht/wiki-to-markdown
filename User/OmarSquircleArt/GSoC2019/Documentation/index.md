## Documentation

### Discussion Documents:

##### [/Procedural Textures/](/Procedural_Textures/)

##### [/Mapping Node And Vector Socket/](/Mapping_Node_And_Vector_Socket/)

##### [/Code Repetition: Performance Readability And Portability/](/Code_Repetition:_Performance_Readability_And_Portability/)

##### [/Smooth Voronoi/](/Smooth_Voronoi/)

##### [/Attribute Node/](/Attribute_Node/)

### Developer Documentation:

##### [/Adding A New Shading Node/](/Adding_A_New_Shading_Node/)
