## GSoC 2019: Improve Cycles/EEVEE For Procedural Content Creation

The project hopes to refactor and add Cycles/EEVEE nodes to improve the
workflow of technical/shading artists. This is done by providing; more
streamline nodes for vector math and numerical computations, more noise
types, and wider access to Blender’s data like object properties and
spline info.

### [Weekly Reports](https://devtalk.blender.org/t/cycles-eevee-improvements-weekly-reports/7697)

### [/Final User Report/](/Final_User_Report/)

### [/Proposal/](/Proposal/)

### [/Documentation/](/Documentation/)
