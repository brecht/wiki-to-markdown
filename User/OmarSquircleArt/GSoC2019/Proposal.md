## Improve Cycles/EEVEE For Procedural Content Creation

### Name

Omar Ahmad

### Contact

  - Email:
    [omar.squircleart@gmail.com](http://mailto:omar.squircleart@gmail.com)
  - Github: <https://github.com/OmarSquircleArt>
  - Twitter: <https://twitter.com/OmarSquircleArt>
  - Facebook: <https://www.facebook.com/OmarSquircleArt>
  - StackExchange:
    <https://blender.stackexchange.com/users/5794/omar-ahmad>

### Synopsis

The project hopes to refactor and add Cycles/EEVEE nodes to improve the
workflow of technical/shading artists. This is done by providing; more
streamline nodes for vector math and numerical computations, more noise
types, and wider access to Blender’s data like object properties and
spline info.

### Benefits

Faster workflow and less workarounds for procedural content creators.

### Deliverables

The projects hopes to achieve the following:

  - Implement dynamic socket hiding for nodes that may benefit from it.
  - Allow variable inputs for the Mapping node. Take care of UI
    considerations.
  - Provide more vector math operations. Entrywise multiplication, snap,
    reflect, etc.
  - Provide a random generator, often referred to as White Noise.
  - Provide a Clamp node as an alternative to the Clamp option in the
    math node.
  - Provide a Map Range node similar to that of the Compositor’s.
  - Provide more basic noise functions based on feedback from the
    community.
  - Investigate accessing custom properties through the Attribute node.
  - Expose information about splines in a Spline Info node.

### Project Details

#### Dynamic Socket Hiding

Nodes sometimes have sockets that are undefined or unused, this can be
confusing for new users and doesn’t look good. A simple solution is to
dynamically hide sockets that are not defined or unused. For instance,
the sine and cosine functions in the math node don’t use the second
input socket and it can be hidden. The same goes for the Vector Math
node where the value and vector outputs may not always be defined as in
the Dot Product type.

#### Variable Mapping Inputs

We currently have a Mapping node capable of performing linear
transformations. However, this node doesn’t expose the transformation
parameters which often leads to the user having to implement custom
transformation node groups. We shall allow dynamic inputs for the node
to avoid this. Relevant [RCS
post](https://blender.community/c/rightclickselect/wTcbbc/).

#### Vector Math Operations

We shall add more vector math operations to avoid frequent separation
and recombination of vectors in order to do the simplest of operations.
The new operations include:

  - Entrywise Multiplication and Division.
  - Projection.
  - Reflection.
  - Entrywise Absolute.
  - Entrywise Snap.
  - Vector Length.

Relevant [RCS
post](https://blender.community/c/rightclickselect/qKbbbc/).

#### Random Generator

We currently have a Noise node, but often we just need a random value
based on the input vector. We shall add a random generator node that
returns a random number between 0 and 1 based on the input vector. A
seed value will be provided. And the output shall be constant for the
same seed value.

#### Clamp Node

Math nodes have a clamp option that clamps the output to the 0-1 range.
This option—in my experience—is nearly never used and is limited to that
range. The other option is to use two maths nodes with a maximum-minimum
configuration. We shall add a Clamp node as an alternative that lets the
user define the range.

#### Map Range Node

The compositor already have a node to perform linear value remapping. We
shall implement a similar node for the shading system.

#### More Noise Types

We shall implement more basic organic noise types. The plan is to ask
feedback from the community and implement the noise types they think are
essential for their creations. The project shall only implements the
noise types that are hard or impossible to create through other means.
Relevant [RCS
post](https://blender.community/c/rightclickselect/V8bbbc/).

#### Object Properties

We shall allow access to any primitive object property (Numbers,
Vectors, and Colors) through the Attribute node. For instance, color
would return the color of the object and scale would return the scale of
the object. Relevant [RCS
post](https://blender.community/c/rightclickselect/Wvbbbc/).

#### Spline Info

We shall add a Spline Info node that exposes some information about
splines. I explained this in details in my proposal [Spline
Info](https://blender.community/c/rightclickselect/xWbbbc/).

#### Project Schedule

I shall finish my final exams on the first of June. At this time, I will
start studying the prerequisites of the project, mainly OSL and C++ OOP
concepts. The coding timeline would be:

1.  Setup a development environment and compile Blender.
2.  Get familiar with the code base. Learn concepts like RNA and common
    structures.
3.  Study the backends SVM, OSL, and the process of adding new
    Cycles/EEVEE nodes.
4.  Implement dynamic socket hiding.
5.  Add the extra Vector Math Operators.
6.  Add Clamp, Random Generator, and Map Range nodes.
7.  Add dynamic sockets to the Mapping node.
8.  Add Spline Info node.
9.  Add more noise types.
10. Implement Object Properties access through the Attribute node.
11. Document the development process for future students and developers.
12. Add documentation.

#### Biography

I am a 19 years old student in the Faculty of Computers and Information
at Helwan University. I have been using Blender for over 6 years. And I
worked as a CG Generalist and a Technical Artist for over 3 years. My
favourite thing about Blender is Animation Nodes, I love procedural
content creation and scientific computation/visualization using
Animation Nodes and Cycles. You might call me a “noder” because the node
editor is my second home, hence my choice of the project. I also love
technical writing and cats.

I gained most of my programming experience by contributing to the
Animation Nodes project using Python and Cython. I also created multiple
addons for Blender, but I am probably the only one who uses them. So I
am experienced in Python and know my way around the API.

I never contributed to an open source project that uses C/C++. But I
coded a multitude of small programs using C. I created those programs to
practise C, OpenGL, GLSL, X11, and event driven programming in general.
You can see some of those on my Github. While I never used C++ before, I
am motivated and confident that I will get up to speed with it by the
time the project starts. I implemented one of my goals as a
demonstration, you can check the patch at
[D4625](https://developer.blender.org/D4625).

I spend a lot of my time doing technical writing. I am one of the top
users on Blender’s StackExchange network and I wrote the Animation Nodes
documentation. So I can write good and efficient documentations.
