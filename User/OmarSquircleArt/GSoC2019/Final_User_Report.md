This page provides an overview and examples of the features that have
been implemented in the GSoC project.

# Vector Socket

The Vector Socket is now drawn expanded by default:

![../../../images/GSoC\_2019\_Report\_Vector\_Socket.png](../../../images/GSoC_2019_Report_Vector_Socket.png
"../../../images/GSoC_2019_Report_Vector_Socket.png")

# Vector Math

Added Multiply, Divide, Project, Reflect, Distance, Length, Scale, Snap,
Floor, Ceil, Modulo, Fraction, Absolute, Minimum, and Maximum operators
to the Vector Math node. The Value output has been removed from
operators whose output is a vector, and the other way around.

Snap, for instance, can be used to easily create cell noise:

![../../../images/GSoC\_2019\_Report\_Cell\_Noise.png](../../../images/GSoC_2019_Report_Cell_Noise.png
"../../../images/GSoC_2019_Report_Cell_Noise.png")

Or it can be used to easily resample images through quantization of
space:

![../../../images/GSoC\_2019\_Report\_Resampling.png](../../../images/GSoC_2019_Report_Resampling.png
"../../../images/GSoC_2019_Report_Resampling.png")

Modulo can be used to repeat patterns, for instance, creating a dot
pattern:

![../../../images/GSoC\_2019\_Report\_Dots.png](../../../images/GSoC_2019_Report_Dots.png
"../../../images/GSoC_2019_Report_Dots.png")

Modulo and Snap, together, can be used to create a stippling pattern:

![../../../images/GSoC\_2019\_Report\_Stippling.png](../../../images/GSoC_2019_Report_Stippling.png
"../../../images/GSoC_2019_Report_Stippling.png")

# Mapping Node

The Mapping node is now dynamic, taking variable inputs:

![../../../images/GSoC\_2019\_Report\_Mapping.png](../../../images/GSoC_2019_Report_Mapping.png
"../../../images/GSoC_2019_Report_Mapping.png")

For instance, given the following radial pattern:

![../../../images/GSoC\_2019\_Report\_Radial.png](../../../images/GSoC_2019_Report_Radial.png
"../../../images/GSoC_2019_Report_Radial.png")

We can deform the pattern by rotating the space based on the distance to
the origin as follows:

![../../../images/GSoC\_2019\_Report\_Radial\_Deformed.png](../../../images/GSoC_2019_Report_Radial_Deformed.png
"../../../images/GSoC_2019_Report_Radial_Deformed.png")

Or we can evaluate a noise at this space to get a vortex texture:

![../../../images/GSoC\_2019\_Report\_Deformed\_Noise.png](../../../images/GSoC_2019_Report_Deformed_Noise.png
"../../../images/GSoC_2019_Report_Deformed_Noise.png")

We can also achieve shearing through variable translation as follows:

![../../../images/GSoC\_2019\_Report\_Shear.png](../../../images/GSoC_2019_Report_Shear.png
"../../../images/GSoC_2019_Report_Shear.png")

Or we can shift the previously created dots in an alternating pattern as
follows:

![../../../images/GSoC\_2019\_Report\_Shifted\_Dots.png](../../../images/GSoC_2019_Report_Shifted_Dots.png
"../../../images/GSoC_2019_Report_Shifted_Dots.png")

The max and min options have been moved outside of the node to the max
and min operations of the *Vector Math* node:

![../../../images/GSoC\_2019\_Report\_Clip\_Image.png](../../../images/GSoC_2019_Report_Clip_Image.png
"../../../images/GSoC_2019_Report_Clip_Image.png")

# Map Range

A map range node similar to that of the compositor’s was added:

![../../../images/GSoC\_2019\_Report\_Map\_Range.png](../../../images/GSoC_2019_Report_Map_Range.png
"../../../images/GSoC_2019_Report_Map_Range.png")

# Clamp Node

A clamp node was added:

![../../../images/GSoC\_2019\_Report\_Clamp.png](../../../images/GSoC_2019_Report_Clamp.png
"../../../images/GSoC_2019_Report_Clamp.png")

# White Noise

A new White Noise node was added. The node can operate in 1D, 2D, 3D, or
4D space.

![../../../images/GSoC\_2019\_Report\_White\_Noise.png](../../../images/GSoC_2019_Report_White_Noise.png
"../../../images/GSoC_2019_Report_White_Noise.png")

# Voronoi Texture

The voronoi node was rewritten to support much more modes of operation.
Each of the following sections describe one of those modes of
operations.

## Dimensions

The node can now operate in 1D, 2D, 3D, or 4D space.

### 1D

One dimensional voronoi can be used, for instance, to create a brick
texture with **random width**:

![../../../images/GSoC\_2019\_Report\_1D\_Voronoi.png](../../../images/GSoC_2019_Report_1D_Voronoi.png
"../../../images/GSoC_2019_Report_1D_Voronoi.png")

### 2D

Two dimensional voronoi can be used to create a somewhat accurate
2-distances to voronoi cells. Something that was otherwise
hard/impossible to get with 3D voronoi. This can be used to create
cracks with uniform width:

![../../../images/GSoC\_2019\_Report\_2D\_Voronoi.png](../../../images/GSoC_2019_Report_2D_Voronoi.png
"../../../images/GSoC_2019_Report_2D_Voronoi.png")

### 3D

We already had 3D voronoi before, so nothing was added here.

### 4D

Four dimensional voronoi can be used to animate voronoi in 3D space. In
other words, the fourth dimension can be a function of time:

![../../../images/GSoC\_2019\_Report\_4D\_Voronoi.gif](../../../images/GSoC_2019_Report_4D_Voronoi.gif
"../../../images/GSoC_2019_Report_4D_Voronoi.gif")

Or it can be used create a **loopable** animated voronoi in 2D space.
This gif loops\!

![../../../images/GSoC\_2019\_Report\_4D\_Voronoi\_Loop.gif](../../../images/GSoC_2019_Report_4D_Voronoi_Loop.gif
"../../../images/GSoC_2019_Report_4D_Voronoi_Loop.gif")

## Jitter

The new jitter input controls the uniformity of the voronoi cells:

![../../../images/GSoC\_2019\_Report\_Voronoi\_Jitter.gif](../../../images/GSoC_2019_Report_Voronoi_Jitter.gif
"../../../images/GSoC_2019_Report_Voronoi_Jitter.gif")

## Features

In general, we fixed some symmetry issues and improved precision. The
node lets you select what voronoi feature you want to compute. The
following features are supported.

### F1

This is the standard F1 feature we had before. However, we now provide
the position of the voronoi cell as an output:

![../../../images/GSoC\_2019\_Report\_Voronoi\_Cell\_Position.png](../../../images/GSoC_2019_Report_Voronoi_Cell_Position.png
"../../../images/GSoC_2019_Report_Voronoi_Cell_Position.png")

The position can be used to compute local texture coordinates in voronoi
cells as follows:

![../../../images/GSoC\_2019\_Report\_Voronoi\_Cell\_Coordinates.png](../../../images/GSoC_2019_Report_Voronoi_Cell_Coordinates.png
"../../../images/GSoC_2019_Report_Voronoi_Cell_Coordinates.png")

The significance of this local texture coordinates will become apparent
later.

### Smooth F1

Voronoi can't be used in organic textures due to the hard
discontinuities at the cell edges. For that reason, we introduced a
smoothed voronoi textures. A comparison between normal F1 distance and
smoothed F1 distance is shown below:

![../../../images/GSoC\_2019\_Report\_Smooth\_Voronoi.png](../../../images/GSoC_2019_Report_Smooth_Voronoi.png
"../../../images/GSoC_2019_Report_Smooth_Voronoi.png")

The smoothness is more apparent if the node was used as a displacement
texture. Both of the following textures are smooth with different
smoothness values:

![../../../images/GSoC\_2019\_Report\_Smooth\_Voronoi\_Shaded.png](../../../images/GSoC_2019_Report_Smooth_Voronoi_Shaded.png
"../../../images/GSoC_2019_Report_Smooth_Voronoi_Shaded.png")

The ID color of the voronoi cells is also smoothed, introducing a new
type of noise. Different smoothness values are shown below:

![../../../images/GSoC\_2019\_Report\_Smooth\_Voronoi\_ID.png](../../../images/GSoC_2019_Report_Smooth_Voronoi_ID.png
"../../../images/GSoC_2019_Report_Smooth_Voronoi_ID.png")

In fact, it is just a Value Noise:

![../../../images/GSoC\_2019\_Report\_Smooth\_Voronoi\_Value.gif](../../../images/GSoC_2019_Report_Smooth_Voronoi_Value.gif
"../../../images/GSoC_2019_Report_Smooth_Voronoi_Value.gif")

Yes, we just got Value Noise free of charge. The cell position is also
smoothed:

![../../../images/GSoC\_2019\_Report\_Smooth\_Voronoi\_Position.png](../../../images/GSoC_2019_Report_Smooth_Voronoi_Position.png
"../../../images/GSoC_2019_Report_Smooth_Voronoi_Position.png")

Such smoothness can be utilized to create all sorts of patterns. The
difference between smoothed F1 and F1, for instance, can be used to
create a beveled voronoi cell pattern:

![../../../images/GSoC\_2019\_Report\_Smooth\_Voronoi\_Beveled\_Cells.png](../../../images/GSoC_2019_Report_Smooth_Voronoi_Beveled_Cells.png
"../../../images/GSoC_2019_Report_Smooth_Voronoi_Beveled_Cells.png")

A high smoothness value with color based masking can produce a water
droplets texture:

![../../../images/GSoC\_2019\_Report\_Smooth\_Voronoi\_Water\_Droplets.png](../../../images/GSoC_2019_Report_Smooth_Voronoi_Water_Droplets.png
"../../../images/GSoC_2019_Report_Smooth_Voronoi_Water_Droplets.png")

The isolines of the smoothed distance is meta-ball like:

![../../../images/GSoC\_2019\_Report\_Smooth\_Voronoi\_Isolines.gif](../../../images/GSoC_2019_Report_Smooth_Voronoi_Isolines.gif
"../../../images/GSoC_2019_Report_Smooth_Voronoi_Isolines.gif")

### F2

This is just the standard F2 we had before. The primary purpose of F2 is
to compute the well known F2-F1 pattern for non-euclidean distance
voronoi:

![../../../images/GSoC\_2019\_Report\_Voronoi\_Difference.png](../../../images/GSoC_2019_Report_Voronoi_Difference.png
"../../../images/GSoC_2019_Report_Voronoi_Difference.png")

### F3 and F4

F3 and F4 were deprecated.

### Distance To Edge

This is mode of operation computes the distance to the voronoi cells
edges. This is a much more accurate version of the cracks feature we had
before, which was an F2-F1 algorithm.

![../../../images/GSoC\_2019\_Report\_Voronoi\_Distance\_To\_Edge.png](../../../images/GSoC_2019_Report_Voronoi_Distance_To_Edge.png
"../../../images/GSoC_2019_Report_Voronoi_Distance_To_Edge.png")

![../../../images/GSoC\_2019\_Report\_2D\_Voronoi.png](../../../images/GSoC_2019_Report_2D_Voronoi.png
"../../../images/GSoC_2019_Report_2D_Voronoi.png")

Only euclidean distance is supported for now. Manhatten and Chebyshev
distances may be supported in the future while Minkowski will not be
considered.

### N-Sphere Radius

This mode of operation computes the radius of the n-sphere inscribed in
the voronoi cells. In other words, it is half the distance between the
closest point and the point closest to it. Consequently, it is the max
radius of the bounding n-sphere that each cell is allowed to have to
avoid intersection.

The simplest example would be as follows. Notice how the n-spheres never
intersect:

![../../../images/GSoC\_2019\_Report\_Voronoi\_NSphere\_Radius.png](../../../images/GSoC_2019_Report_Voronoi_NSphere_Radius.png
"../../../images/GSoC_2019_Report_Voronoi_NSphere_Radius.png")

A more practical example would be:

![../../../images/GSoC\_2019\_Report\_Voronoi\_NSphere\_Radius\_Extra.png](../../../images/GSoC_2019_Report_Voronoi_NSphere_Radius_Extra.png
"../../../images/GSoC_2019_Report_Voronoi_NSphere_Radius_Extra.png")

We can use this setup to create a very robust and highly efficient
scatter node, which I shall show now. We are going to use the local
coordinates we previously computed:

![../../../images/GSoC\_2019\_Report\_Voronoi\_Cell\_Coordinates.png](../../../images/GSoC_2019_Report_Voronoi_Cell_Coordinates.png
"../../../images/GSoC_2019_Report_Voronoi_Cell_Coordinates.png")

And we are going to scale the local coordinates based on the n-sphere
radius and rotate it based on the cell color along the z axis. Finally,
we are going to evaluate the required texture to scatter at this texture
coordinates:

![../../../images/GSoC\_2019\_Report\_Voronoi\_Distribute.png](../../../images/GSoC_2019_Report_Voronoi_Distribute.png
"../../../images/GSoC_2019_Report_Voronoi_Distribute.png")

And voila, we just created a scatter node\! Here is 3D sphere packing:

![../../../images/GSoC\_2019\_Report\_Voronoi\_Packing.png](../../../images/GSoC_2019_Report_Voronoi_Packing.png
"../../../images/GSoC_2019_Report_Voronoi_Packing.png")

# Noise Texture

1D, 2D, and 4D versions of perlin noise were added.

## 1D

1D noise can be used as a general noise function when only a single axis
is varying:

![../../../images/GSoC\_2019\_Report\_Noise\_1D.gif](../../../images/GSoC_2019_Report_Noise_1D.gif
"../../../images/GSoC_2019_Report_Noise_1D.gif")

This axis can be time, in which case, the noise act as a 1D wiggle:

![../../../images/GSoC\_2019\_Report\_Noise\_1D\_Wiggle.gif](../../../images/GSoC_2019_Report_Noise_1D_Wiggle.gif
"../../../images/GSoC_2019_Report_Noise_1D_Wiggle.gif")

More specifically, it can be used, for instance, to create a brushed
metal texture:

![../../../images/GSoC\_2019\_Report\_Noise\_1D\_Brushed\_Metal.png](../../../images/GSoC_2019_Report_Noise_1D_Brushed_Metal.png
"../../../images/GSoC_2019_Report_Noise_1D_Brushed_Metal.png")

## 2D

2D perlin is almost two times faster than 3D perlin, so in cases where
3D perlin may not be needed, like terrain generation, using 2D perlin
should noticeably speed up your scene.

Additionally, 2D perlin can be used to animate 1D perlin:

![../../../images/GSoC\_2019\_Report\_Noise\_2D.gif](../../../images/GSoC_2019_Report_Noise_2D.gif
"../../../images/GSoC_2019_Report_Noise_2D.gif")

Or to make 1D perlin C¹ continuous at `i / scale` for any integer `i`:

![../../../images/GSoC\_2019\_Report\_Noise\_1D\_Continuous.gif](../../../images/GSoC_2019_Report_Noise_1D_Continuous.gif
"../../../images/GSoC_2019_Report_Noise_1D_Continuous.gif")

## 4D

Primarily, 4D perlin can be used to animate 3D perlin:

![../../../images/GSoC\_2019\_Report\_Noise\_4D.gif](../../../images/GSoC_2019_Report_Noise_4D.gif
"../../../images/GSoC_2019_Report_Noise_4D.gif")

Or loop a 2D perlin animation:

![../../../images/GSoC\_2019\_Report\_Noise\_4D\_Loop.gif](../../../images/GSoC_2019_Report_Noise_4D_Loop.gif
"../../../images/GSoC_2019_Report_Noise_4D_Loop.gif")

# Musgrave Noise

Musgrave noise was also extended to 1D, 2D, and 4D dimensions. Since it
is very similar to perlin, the aforementioned examples also apply and I
shall not showcase it further.

# Volume Info Node

A *Volume Info* node was added. The node provides the color, density,
flame, and temperature of smoke domains:

![../../../images/GSoC\_2019\_Report\_Volume\_Info.png](../../../images/GSoC_2019_Report_Volume_Info.png
"../../../images/GSoC_2019_Report_Volume_Info.png")

# Vertex Color Node

A *Vertex Color* node was added. The node provide a nice interface to
select the target Vertex Color:

![../../../images/GSoC\_2019\_Report\_Vertex\_Color.png](../../../images/GSoC_2019_Report_Vertex_Color.png
"../../../images/GSoC_2019_Report_Vertex_Color.png")

# Vertex Color Alpha

The alpha value of the vertex colors is now exposed:

![../../../images/GSoC\_2019\_Report\_Vertex\_Color\_Alpha.gif](../../../images/GSoC_2019_Report_Vertex_Color_Alpha.gif
"../../../images/GSoC_2019_Report_Vertex_Color_Alpha.gif")

# Object Color

The object color property is now exposed through the *Object Info* node.
This is particularly useful when using a tool like Animation Nodes,
where colors are computed in Animation Nodes and needs to be passed to
the render target. For instance:

![../../../images/GSoC\_2019\_Report\_Object\_Color.gif](../../../images/GSoC_2019_Report_Object_Color.gif
"../../../images/GSoC_2019_Report_Object_Color.gif")
