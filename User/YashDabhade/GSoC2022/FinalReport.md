# Text Usability Improvements - Final Report

For the Google Summer of Code'22 project, I worked on improving the
Blender's Text editor and also worked on adding few new operators to
simplify the formatting of text.

## Summary

This summer I started the project with discussing the deliverables with
my mentor, which were organized in order of their priority. The project
was divided into two main parts:

  - Toggle the state of Selected text.
  - Select regions of Text with Mouse.

A part of both the parts was mentioned in the proposal but most of it
was improvised in response to the mentor's feedback.

### Toggle the State of Selected Text

The feature extends the functionality of the palette portraying the
state of text i.e Bold, Italics, Underline or Smallcaps. Two new
improvements have been made.

  - The Buttons in the Font menu accurately portray the whether the
    selected text is bold, Italics, Underline or Smallcaps.

![../../../images/ToggleState.png](../../../images/ToggleState.png
"../../../images/ToggleState.png")

  - The Buttons can be further used to toggle the state of selected
    text.

![../../../images/Smallvid2.gif](../../../images/Smallvid2.gif
"../../../images/Smallvid2.gif")

The earlier functionality of choosing the state of the next character on
clicking the button is kept intact.

### Select regions of Text with Mouse

Two new selection features have been implemented with mouse drag and
mouse double click respectively.

  - The cursor is placed in the location corresponding to the character
    pointed by the mouse pointer. This in-turn is used to select regions
    of text by mapping the mouse pointer movement with the cursor
    location and extending the selected part of the text.

![../../../images/Smallvid3.gif](../../../images/Smallvid3.gif
"../../../images/Smallvid3.gif")

  - A new feature to select a word by double click is implemented by
    selecting the word double clicked by the user.

![../../../images/Selectword.gif](../../../images/Selectword.gif
"../../../images/Selectword.gif")

The above mentioned features are devised for selection of text in 3D
space so that the orientation of text is also taken into consideration
while mapping the mouse pointer coordinates with the nearest character.
The selection of text can further be used to format parts of text i.e
convert to bold, italics, underline, small-caps, change color etc.

### Future Work

  - Many new features which can be used to simplify editing of text will
    be added.
  - Code cleanup for the already submitted code is left.
  - Minor issue fixes will be worked upon.

### Acknowledgements

I will always be grateful to Campbell for his constant support and
motivation without which it was impossible for me to even think of being
a part of the organization and I am eagerly looking forward to work with
him.
