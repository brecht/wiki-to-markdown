# Aligorith - Dr Joshua Leung

**About Me**:

  - Blender Developer since 2006, User since 2004
  - Responsible for Animation System/Tools and Grease Pencil
  - Based in Christchurch, New Zealand
  - PhD in Computer Science

**This Page**: Version 2.0 of my old Blender Wiki userpage, with content
ported over from the old wiki

## Weekly Reports (for BF Work)

  - [User:Aligorith/Foundation/2018](Aligorith/Foundation/2018.md)

... TO BE PORTED BACK FROM OLD WIKI ...

## Resources for Other Devs

**Blender Repository Setup**:

  - **[ `brepo_init.sh`
    ](Aligorith/Scripts/brepo_init_sh.md)** - Shell script
    for checking out and setting up new checkouts of the Blender
    sources, ready to use for development. (Created during Code Quest
    '18, as I was constantly setting up new repos after having to set up
    my machine twice in as many days)
  - **[ `brepo_init.bat`
    ](Aligorith/Scripts/brepo_init_bat.md)** - Windows port
    of the `brepo_init.sh` script.

**Blender Default Settings**:

  - **[
    configure\_blender.py](Aligorith/Scripts/my_settings.md)**
    - bpy script to reapply all my custom settings to a new instance of
    Blender created from factory settings. (Created post Code Quest, due
    to frequent compatability-breaking changes landing in the 2.8
    branch, requiring from-scratch recreations of local settings)
