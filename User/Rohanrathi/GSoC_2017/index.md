# Google Summer of Code 2017: Normal Editing Tools

A little about me:

<https://wiki.blender.org/index.php/User:Rohanrathi/About>

Branch:

Normal Editing Tools - soc-2017-normal-tools

Link:
<https://developer.blender.org/diffusion/B/browse/soc-2017-normal-tools/>

Project listing on Google Summer of Code site:

<https://summerofcode.withgoogle.com/projects/#5878855406452736>

## [Proposal](Proposal.md)
