# User Documentation

## Edit Single Normal

Split normals of vertices can be edited by activating Face selection
with either Edge or Vertex selection mode. First selecting a vertex then
any face of this vertex will edit the split normal associated with this
Face.

![../../../images/Normal\_Rotation.gif](../../../images/Normal_Rotation.gif
"../../../images/Normal_Rotation.gif")

## Normal Rotation

A rotate functionality similar to standard rotation. Pressing R for
rotation and then N invokes normal rotation, which supports numeric
inputs, snapping, UI based inputs and constraining to any axis.

![../../../images/Normal\_Rotation\_2.gif](../../../images/Normal_Rotation_2.gif
"../../../images/Normal_Rotation_2.gif")

## Split/Merge

With custom normals using sharp and smooth tags destroys the custom
normal integrity. Merge takes average of custom normal data and merges
all split normals at this average. Split splits the normals of the face
to align with the face normal.

![../../../images/split\_normal.gif](../../../images/split_normal.gif
"../../../images/split_normal.gif")

## Average

Average provides similar functionality to the modifier with face area
and corner angle modes. The custom normal mode calculates the average of
split normals of each selected vertex and set the direction of the split
normals to this average.

![../../../images/Average.gif](../../../images/Average.gif
"../../../images/Average.gif")

## Point Normals to Target

Press ALT-L to reveal a number of modes to point normals to varied
locations, with the ability to point towards or away from the specified
target.

Spherize Normals: Generates a normal vector radiating from a point to
all the vertices and adds that vector to the custom normals in a given
proportion.

  - L Key to point to pivot: The L Key uses the following pivot points
    as target: 1) Active Element 2) Median Point 3) 3D Cursor 4)
    Bounding Box center.
  - M Key to point towards mouse: The vertex normals will follow the
    cursor when this is activated. This method also poses the ability to
    align normals in a specific direction.
  - O Key to point to origin: Selected normals will point towards object
    origin.
  - LMB Click: Set the selected normals to point towards newly placed
    cursor location.
  - RMB Click: Set the selected normals to point towards new active
    element of mesh.

![../../../images/Point\_normals\_1.gif](../../../images/Point_normals_1.gif
"../../../images/Point_normals_1.gif")

## Normal Interface Tools

The tools interacting with the XYZ interface are:

1.  Add: Add normal vector to selected normal
2.  Multiply: Multiply scalar with X, Y, Z vector
3.  Copy: Copy selected normals to buffer
4.  Paste: Paste buffer value to selected normals
5.  Reset: Resets the buffer and any custom normals that may be selected

![../../../images/Copy.gif](../../../images/Copy.gif
"../../../images/Copy.gif")

## Smoothen Normals

Smoothen custom normal based on adjacent vertex normal. Brings the
custom normals closer to the adjacent vertex normals. A factor value
specifies strength of smoothened vs origin normal.

![../../../images/Smoothen.gif](../../../images/Smoothen.gif
"../../../images/Smoothen.gif")

## Keep Custom Normal

The ability to keep custom normals unchanged as they were before any
transform op.

![../../../images/Keep\_CN.gif](../../../images/Keep_CN.gif
"../../../images/Keep_CN.gif")

## Set Normals from Faces

Fixed set normals from faces to respected sharp edges on the
'unselected' side of the faces.

![../../../images/Set\_from\_face.gif](../../../images/Set_from_face.gif
"../../../images/Set_from_face.gif")

## Weighted Normal Modifier

The modifier sets normals according to 3 presets:

  - Face Area: Weighted according to face area. Larger area implies more
    weight.
  - Corner Angle: Weighted according to angle each face makes incident
    on the vertex. This method is default weighting mode of blender.
  - Face Area - Corner Angle: Weights are a product of face area and
    corner angle.

Weight: This determines additional weights that the faces can hold. A
value of 10 means all faces are weighted equally. More than 10 means
faces with higher weights are given even more weight. Less than 10 means
faces with high weights are given lesser weights.

Threshold: This parameter specifies which all faces to be weighted
equally based on their weight value. Eg: Consider 2 faces with area of 5
units and 5.15 units. This threshold decides whether these 2 faces will
hold same weight or one will have more weight than other.

Vertex Groups: Support for vgroups to only apply modifier to specific
parts of mesh.

Keep Sharp Edges: This option tells the modifier to respect sharp edges
with similar working to sharp edges with Auto Smooth. All faces on one
side of a sharp edge are still weighted.

![ 640px | 640px](../../../images/Weighted_normal_mod.png
" 640px | 640px")
