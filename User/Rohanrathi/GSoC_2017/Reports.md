# Reports - GSoC 2017 - Normal Editing Tools

Welcome\! Venture with me through my journey as I make custom loop
normals all the more editable\! This is a log of all my weekly reports.
You'll find here all my accomplishments and difficulties encountered
throughout my journey in GSoC.

## Week 1

What I did this week:

Fairly rudimentary work. Cached the custom loop normal spaces for the
mesh and added a mechanic to detect when the loop normal spaces are
dirty and rebuild the loop normal space array.

Other than that I developed a much stronger understanding of BMesh Ops.
All bmesh ops that are executed should now either flag the loops of the
modified geometry or invalidate the entire loop normal space array of
the mesh.

What I plan on doing next week:

Now that this is cleared, I think I can focus on writing the actual
tools that will manipulate the custom normal. My first goal was to add
the ability to rotate normals. I've studied modal rotation already and
most likely will have added this ability by the end of next week.

Although all BMesh Ops that modify geometry are taken care of, there are
other tools that manipulate geometry like modal transform. So I need to
figure out a way to invalidate the loop normal spaces in this case. This
will be my prior task along with normal rotation.

## Week 2

What I did this week:

Added generic structures and functions that will be used to initialize
the loop normals data required for editing tools. Other than that I've
added normal rotation. I'm also close to finishing my second feature,
point normals to target.

Normal Rotation: Works by pressing R, then N. requires autosmooth to be
active. Can be constrained to either of three axes, can work by
accepting num inputs or accepting inputs from UI. In short, works
exactly as standard rotation. Though of course, the rotation center is
local to each vertex.

When I tested it, it did crash once, so definitely needs some bug
testing.

What I plan on doing next week:

Finish point normals to target. Its a modal op, may require workflow
changes. Then begin work on Aligning normals. This is a big part, but if
possible I'd like to look back as well and think about how to make lnor
invalidation more cohesive with BMOps and practical.

## Week 3

What I did this week:

Added ability to point and align normals to location. Other than that
I've worked on an op to have selected custom normals point to mouse
location or align them. Haven't been able to get it to work right yet.

Point Normal: Press ALT-L to activate it. Press L again to have normals
point to selected pivot point (all supported except individual origins).
RMB click on the mesh to have the normals point to the newly selected
active object (vert, edge, face) center. LMB click to relocate 3D cursor
and then have the normals point to it. O Key to have normals point to
object origin.

After execution, there are options available to point towards or away
from target. Along with it, the target location can also be changed from
UI. One thing I would like to add is to have some sort of a visual aid
to show the target location when the target location changes within the
UI.

What I plan on doing next week: I'll hopefully finish point/align
normals with mouse. Then I'll work to incorporate lnor invalidation with
transform ops and then split/ merge custom loop normals.

## Week 4

What I did this week:

Did a lot of work this week. Completed the operator to point and align
normals by having clnors point to or align with mouse location. With
this, it finishes the first three parts of my proposal.

lnor spaces are now invalidated with transform ops as well, works the
same way as before ie invalidates all the spaces of the faces that share
the selected vertices.

There is also a checkbox to keep the pre-transform clnors of the mesh
after the transform has completed. This was a suggestion in BA,
specifically for negative scaling and the mirror tool. Unfortunately
hasn't fixed either as in negative scaling the the lnors already stay
the same, you have to invert them instead and mirror tool has weird
interaction with custom normals. I'll address this by the end of next
week I think.

Whenever the Loop Normal spaces are rebuilt, an auxiliary function is
now called which again invalidates and then rebuilds ALL the Loop Normal
Spaces of the mesh, so if any of the Loop Normal spaces change, this
means that the mesh editing function prior to this which was meant to
invalidate lnor spaces did not correctly mark all the lnor spaces as
invalid.

Ability to edit individual clnors: Select a vertex then face, then
perform any standard edit op, only those clnors will be edited which are
common to the selected verts and faces. The workflow may not be as fast
as I had imagined, but it still is a better and faster technique than
all other methods I could think of.

What I plan on doing next week: Wrote trivial code for merging normals,
doesn't work. Atleast, now I know what not to do with this operator. I
reckon I may need more than just a couple of days for this operator.
Split and Merge are complementary ops and they will take most of the
week.

## Week 5

First month in GSoC has ended, I'll post a video soon describing all the
functionality I've added so far. Along with discussing the plans for the
future.

What I did this week:

Was away for a few days so not a whole lot. After much, much
deliberation, I've added splitting merging clnors as suggested by
Bastien. I wanted to be able to merge each individual clnor
independently but after careful thought, it just isn't possible. What
I've implemented so far is a brushed up technique of smoothing
faces/edges with added support on how to maintain the merged/splitted
resultant normals. With this method, merging the loop normals of just a
single vertex is just not possible. I would have liked to provide more
control but this idea requires too much tough work. It may not even be
much more practical.

What I plan to do next week:

I'll take time out to clean, revise and improve on the code written so
far. Add proper documentation and get the presentation going. My mentor
provided some useful inputs. After this I'll work on different averaging
techniques.

## Week 6

What I did this week:

Caught and fixed bugs, did a lot of cleanup and fixed a few mistakes.
Looking back at the code I have some ideas in mind.There are other
issues like Split/Merge don't actually perform very well with large
number of loops. A loop index table would be nice to have. Other than
this I've worked out how to keep the clnors same with the other
transform ops which were glitching.

What I plan to do next week:

Next week I'll begin work either on copying normals or weighted normals.
Weighted normals is a critical feature and I feel needs more discussion.

## Week 7

What I did this week:

Fixed all loops in an lnor space not not updating (had to explicitly
invalidate them) which was causing glitching. Along with other minor
fixes. Fixed the performance issues with Split, by generating a
temporary loop index table. Pretty handy imo. Main work I did was on
copy normals, I've finally settled on the ability to copy face normals
and only singular loop normals, nothing else. Feels a little too
stringent implementation to me, I guess I need feedback. Lastly, I've
begun work on weighting modes. The main and most important part of the
project yet. Finally discussed the method to follow and I'm reading code
about modifiers to get a better outlook on how to work on it.

What I plan to do next week:

I'll be spending more time than ever on the work to get weighted normals
go smoothly. I'll be taking feedback as well and make changes to the
existing ops based on suggestions by artists. Moulding the tools by
artists needs will be my prior task, I'll be working on weighting modes
alongside this.

## Week 8

What I did this week:

First of all I made a couple changes to the UI/workflow in some of tools
I've already implemented based on user feedback. Then I've worked on the
weighted normal modifier. After looking into how to implement it, I've
coded it in such a way that it'll provide some decent control to the
user. I'll be providing a release soon to get feedback on it, with what
artists would want different. Though I don't feel any different approach
would be more suitable than this.

What I plan to do next week:

Haven't thought of what I'll do after the modifier. I did have adding
weights to split/merge and average on mind, so I'll be working on that.
I may also work on finding a way to keep custom normals same with bmesh
ops, mostly ops not adding or removing geometry.

## Week 9

What I did this week:

Weighted normals modifier got off to a good start, got some decent
feedback. Also, got ideas to some neat improvements for the modifier.
Something which I guess will be taking most of next week. Added
weighting modes to both average and merge. Didn't consider sharp edges
in it. Most likely will be tackling this alongside with sharp edges in
the modifier, though the structs used are very different. Did some other
fixes.

What I plan on doing next week:

A lot of additions requested for the modifier. So its my primary focus.
Then I'll add sharp edges support with merge and average and make
split/merge and average more coherent. Lastly, I'll focus on custom
normal fixes.

## Week 10

What I did this week: I've added some functionality to the weighted
normals modifier. It had some serious bugs and glitches so I've spent
the majority of my time to refine and work on the features I've added.
Most likely, the modifier will work completely fine now.

Added the following: 1) Modifier now respects sharp edges similar to
sharp edges with smooth shading. 2) Added vertex group support. Modifier
will only affect vertex groups and leave all other loop data unchanged.
3) Add smooth/sharp flags as boolean weights. A check as well whether to
use this function.

What I plan on doing next week: With just over 2 weeks left for the
project. I feel adding more tools is not a good option, not that I have
anything important left to add. I'll go on to maintaining and
documenting the code from next week onwards and prepare for final eval.
There are some bugs and tasks that need attention so I'll put on focus
on those.

## Week 11

What I did this week:

Fixed the problems occurring with sharp edges on the modifier. Then I've
worked on changes to the tools eg respecting sharp edges with average
and fixing the split functionality. College is pushing hard so couldn't
go all the way with these. Will complete in a few days.

What I plan on doing next week:

After the work on the tools and bugs are done. I'll start with the user
documentation and the wiki page to link the work done in the project.

## Week 12

What I did this week:

Added weighting modes and sharp edges to average so that it performs
similar to the modifier. Though custom normal mode needs a bit more work
because of its different behaviour than the rest.

Changed the working of split and merge and transferred their
functionality to average. With custom normals using smooth/sharp tags
loses the custom normal data, giving random results. So, merge takes an
average of clnors and set all loops to a single space, giving consistent
results. Similarly, split explicitly sets clnors to respective face
normals.

Then I've added copy, paste, add, multiply and reset to utilise a normal
vector from UI. I've also added the ability to smoothen clnors based on
adjacent normals. Finally, I've added the ability to 'spherize' in point
normals which adds a vector radiating from a point to all custom
normals.

Other than this also fixed some bugs.

What I plan on doing next week:

With final evaluation about to start, I'll prepare code documentation
and do more rigorous testing of tools to check out errors. I have added
the User Documentation but it is not yet final.

## Final Report

<https://wiki.blender.org/index.php/User:RohanRathi/GSoC_2017/Final_Report>
