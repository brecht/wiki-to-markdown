# Final Report

With over 12 weeks of continuous coding, Blender now has a working
normal editor. This page showcases the final report of my work done
during GSoC '17. In my 12 weeks of coding, I have established a base for
loop normal invalidation, rebuilding custom loop normals using lazy
initialization and have written a large set of tools to manipulate
custom normals after taking input from the community.

## What all was done

I implemented the functionality proposed in my proposal with varying
degrees of modification. The links below provide documentation for all
the features added. My weekly reports add to the timing and chronology
of work done. The following differ from original planning:

1.  Implemented Align as an option for point normals, which itself
    houses a wide array of tools.
2.  Weighted Normals has been implemented as a modifier.
3.  Copy/Paste and few other tools are housed under what I call 'Normal
    Interface Tools' which interact with an XYZ input in the UI.
4.  Preset to harden and soften has been reworked to not smoothen on the
    basis of face normal average, but as an average of adjacent vertex
    normals and is named as Smoothen in the UI.

### To Do

There are some things left to do, for example adding proper support for
3 level weights in Weighted Normal modifier. I had put split tool in my
Todo, but cleared it beforehand.

1.  Adding Support for 3 level weights in the modifier. (DONE)
2.  Fixing custom normal data broken when triangulation or the modifier
    is applied. This is much trickier to do. I've an idea to possibly to
    fix up all lnor space data to be remapped when loop indices change
    but I'm not sure if it is even possible. Even if it is, it will
    require a lot of work. Though this can potentially fix all problems
    with keeping custom normals, its just speculation for now. If not I
    can certainly work out a hackish way to preserve data for
    triangulation.

## Links

GSoC branch containing all my work done:
<https://developer.blender.org/diffusion/B/browse/soc-2017-normal-tools/>

Proposal:
<https://wiki.blender.org/index.php/User:RohanRathi/GSoC_2017/Proposal>

Weekly Reports:
<https://wiki.blender.org/index.php/User:RohanRathi/GSoC_2017/Reports>

My Blender Artists thread:
<https://blenderartists.org/forum/showthread.php?427746-GSoC-2017-Normal-Editing-Tools>

User Documentation:
<https://wiki.blender.org/index.php/User:RohanRathi/GSoC_2017/User_Documentation>
