# GSoC 2018: Bevel Modifier Improvements

## Name

Rohan Rathi

### Email / IRC

  - IRC: RohanRathi

### Synopsis

The bevel modifier has a proposed list of changes and improvements
planned. A non destructive alternative to the bevel tool, the modifier
allows rounded corners for the geometry, and smooths out edges and
corners. However the current modifier does not ensure correct normals
and has various listed bugs which slows down artists workflow. Having
written my own Weighted Normals modifier in last years Google Summer of
Code. I am familiar with the modifier stack and its implementation and
can work to provide good quality functionality on the modifier. I plan
on adding critical discussed functionality that interact with the
normals in the bevel modifier. I intend to keep the workflow as smooth
and user friendly.

### Benefits

The bevel modifier is widely used in modelling to chamfer edges and
smoothen them out. Bevels are used to correct the shading of mesh, so as
to make the edges blunt to give a solid and realistic look as opposed to
non beveled objects.

I will work on adding a robust set of tools that will help improve the
workflow of artists working with the bevel modifier so that many more
artists will be able to utilise the modifier to complete their workflow.
I plan on documenting the code related to my work on the modifier and
work in close communication with the artists to best develop the set of
functionality requested by them.

### Project Details

#### Improve normal editing integration with Modifier

Proper support for custom normals is new found with work done last year.
I propose to add much higher level support to the bevel modifier and
modifiers in general. Given how much computation normals calculation
requires it will be best to redefine structures and cache frequently
used values to provide a base for any further edits and improvements to
the bevel modifier. There are other requests for normals control in the
modifier so I will work on adding them as well.

#### Harden Normals Option

Currently, after a bevel, the way vertex normals are calculated in
Blender by default gives tilted vertex normals at the boundaries between
the new polygons made along the beveled edges, and the original faces
adjacent to those edges. By using smooth shading on the model, when hard
body modeling, an artist would rather like that the original faces of
something like a cube to be shaded flat after a bevel - the smooth
shading would only affect the beveled edges and corners. However,
currently we get shading artifacts due to the bent vertex normals and
the way the faces are triangulated before shading.

With proper support for custom split normals it is now possible to
adjust the vertex normals with specified algorithms. However this issue
cannot be solved by the current weighted normals modifier, as with multi
segment bevels the face area decreases requiring a different weighting
mechanism to be used based on the segments of the bevel.

An advantage of normal support in bevel is to assign the newly created
faces a face strength via the modifier itself. This would enable to keep
the perfectly accurate normals which would otherwise be done through
applying the modifier. This would also allow the bevels to be
nondestructive so the sizes or the underlying geometry can be easily
changed. Which would in turn improve the workflow and fasten asset
creation.

#### Fix normal shading continuity

With specific meshes the shading continuity breaks giving non smooth
shading. A possible solution us to have custom normal-editing option to
bevel to fix up such continuity errors by changing the loop normals.
This is a much requested feature for uniform shading in abstract models.
However, the given fix would have to be fairly complex.

### Project Schedule

I will use the community bonding period to discuss bugs and feature
requests for the modifier in detail. I will then understand the codebase
of the modifier to realise how I will rework it to support the changes
requested. I will then devise a more comprehensive plan on how to
provide a base for support of normals editing to bevel and plan to
resolve the bugs listed. When coding begins, I will work to add the
harden normals option to the modifier and implement various approaches
discussed to figure out the best workflow and functionality. Before
working on the options I will have to add structures to define how to
support normal editing in the modifier with aim for future additions and
better integration. After the bevel integration is finished I will
prepare for first evaluation and revise and review the code. After
evaluation I will finish with the functionality for hardened normals and
discuss and review the changes to be incorporated to the work done so
far. After this I will work on fixing the normal shading discontinuity
with different meshes. I will finish the fix and polish the work I have
done since then. I will also attempt to resolve the remaining bugs. I
will then prepare for second evaluation. My college will start early
August and I will wrap up most of the work by the last week of July. I
will then spend the last few weeks to clean up and maintain the branch
along with documenting the code.

### Bio

I am a 20 years old student enrolled in Delhi Technological University
studying Computer Science and Engineering, currently in my 3rd year of
college. Having taken part in last years GSoC I have a good
understanding of blenders codebase and the modifier workflow and
implementation. I have been a regular contributor and have followed up
on my work even after GSoC ended. I have been a blender user for several
years, and plan on maintaining and revising the code for the bevel tool
and my normal editing branch even after the program ends.
