# Final Report - Bevel Modifier Improvements

Blender Now supports a wide array of improvements to the existing Bevel
Tool and Modifier present.

This page showcases the final report of my work done during GSoC '18.

The functionality available through bevel is shared both by the tool and
modifier.

`I have added new functionality, made improvements and fixed critical bugs that can hamper artists workflow.`

## What all was done

The following functionality was added:

1\) Added Harden Normals option in Bevel.

2\) Fixed generation of edge data (seams/sharp edges).

3\) Fixed bevel of vertex in vertex only in-plane bevels.

4\) Added ability to mark face strength of new polygons for use in
Weighted Normal Modifier.

5\) Fixed normal shading continuity in bevel with non cubic edges.

6\) Added ability to bevel curves.

7\) The soc-2018-bevel branch contains work from my previous GSoC branch
as well

### To Do

There are some things left to do, for example adding custom profile
curve in Bevel.

1.  Add support for custom profile curve.
2.  Add merge near vertex to fix geometry by merging vertices within a
    threshold

## Links

GSoC branch containing all my work done:
<https://developer.blender.org/diffusion/B/history/soc-2018-bevel/>

Proposal:
<https://wiki.blender.org/wiki//User:Rohanrathi/GSoC_2018/Proposal>

Discussion thread:
<https://devtalk.blender.org/t/gsoc-2018-bevel-improvements/440/48>
