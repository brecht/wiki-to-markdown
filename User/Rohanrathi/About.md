  - IRC: RohanRathi

Hi there\! I'm a university student born on 23 August 1997, currently
living in New Delhi, India. I've been a blender user since 2012 (v2.63 I
think) and have been selected to contribute towards Blender Foundation
in Google Summer of Code 2017.

You can find out more about my proposal in my user pages along with what
poly, vertex, loop normals are and how they can be and are used in 3D
Graphics. I've had interest in 3D graphics for many years and am
acquainted with a fair number of software (Eg SolidWorks, 3DS Max,
Unreal Engine) along with a fair share of experience with trivial
software that do picture and video editing.

Right now, my focus is to add a toolset to manipulate custom loop
normals which can boost the capability for game development in Blender.
Be sure to provide any feedback in the Blender Artists thread\!

Link: [Normal Editing
Tools](https://blenderartists.org/forum/showthread.php?427746-GSoC-2017-Normal-Editing-Tools)

Cheers,

Rohan Rathi
