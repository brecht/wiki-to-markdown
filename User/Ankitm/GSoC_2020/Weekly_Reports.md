### Week 12 August 17 - 21

  - Fix overlapping nodes in the nodetree.
    [rB0c7801fa](https://projects.blender.org/blender/blender/commit/0c7801fa2b6)
  - Vertex normals:
      - Parse vertex normals: vn lines.
        [rBc583afd6](https://projects.blender.org/blender/blender/commit/c583afd60b3)
      - Decouple getting smooth groups from export parameters
        [rB0479dc41](https://projects.blender.org/blender/blender/commit/0479dc410fd)
      - Export vertex normals if smooth shading is enabled.
        [rB4923087d](https://projects.blender.org/blender/blender/commit/4923087d253)
  - Refactor:
      - Move file opening errors to constructors.
        [rB491bd8b8](https://projects.blender.org/blender/blender/commit/491bd8b8bbc)
      - Add a public member function in MaterialWrap.
        [rBc815ba0a](https://projects.blender.org/blender/blender/commit/c815ba0a3c0)
      - Move object creation code from constructor.
        [rB4777a6a5](https://projects.blender.org/blender/blender/commit/4777a6a5)
      - Remove unused uv vertex index offset.
        [rBedd5307e](https://projects.blender.org/blender/blender/commit/edd5307e)

<!-- end list -->

  - For polygons with holes:
      - Initial commit for ngon\_tessellate: it is untested and will
        very likely crash.
        [rBefab0bc7](https://projects.blender.org/blender/blender/commit/efab0bc7)
      - Fix tessellate crash due to wrong Vector size.
        [rB5b0cb5bb](https://projects.blender.org/blender/blender/commit/5b0cb5bb)
      - Add newly creates faces to imported faces and dissolve edges. As
        of this commit, ngons are tessellated but the holes are also
        filled up since it keeps falling back to triangle-fan method.
        Need to debug and fix it to use scan-fill.
        [rBe76ab124](https://projects.blender.org/blender/blender/commit/e76ab124)
      - Edit FaceElem list outside polygon creation code.
        [rB10e3f23b](https://projects.blender.org/blender/blender/commit/10e3f23b)

<!-- end list -->

  - Parsing
      - Use \`blender::StringRef\` instead of \`std::string\_view\`.
        [rB73454388](https://projects.blender.org/blender/blender/commit/73454388)
      - Use remove\_prefix for easy transition to StringRef.
        [rBae122533](https://projects.blender.org/blender/blender/commit/ae122533)
      - Read multiple lines with "\\\\n" line breaks.
        [rBa662ddd7](https://projects.blender.org/blender/blender/commit/a662ddd7)
  - Silence normal being zero warnings in mesh validate's verbose mode.
    [rB3f8a8d17](https://projects.blender.org/blender/blender/commit/3f8a8d1757a)
  - Silence mesh\_validate verbose warnings.
    [rB32fe2869](https://projects.blender.org/blender/blender/commit/32fe2869)

### Week 11 August 10 - 15

  - Catch out of range exceptions for stoi/stof.
    [rBbb2eca07](https://projects.blender.org/blender/blender/commit/bb2eca07a68).
  - Add vertex offset due to Curves' vertices also.
    [rBb378f84f](https://projects.blender.org/blender/blender/commit/b378f84f7c5).
  - Use const& instead of pointers in Mesh class.
    [rB82eff7a0](https://projects.blender.org/blender/blender/commit/82eff7a0255).
  - Accept 3D UV vertex coordinates
    [rB6ee696e5](https://projects.blender.org/blender/blender/commit/6ee696e5bfb).
  - Fix loose edge export & import, and offsets.
    [rBc010cf58](https://projects.blender.org/blender/blender/commit/c010cf58147).
  - Fix vertex offsets for MEdges and MLoops.
    [rBe0c0b8ff](https://projects.blender.org/blender/blender/commit/e0c0b8ffd17).
  - Fix several material import bugs, remove wrong assert.
    [rB606b0d7d](https://projects.blender.org/blender/blender/commit/606b0d7da34).
  - Fix usemtl line not being written.
    [rB69f70829](https://projects.blender.org/blender/blender/commit/69f70829d2c)
  - Use function pointer; remove redundant variables.
    [rBba0d376e](https://projects.blender.org/blender/blender/commit/ba0d376e260)
  - Export packed image file names as if they're in current directory.
    [rBd9cdfba2](https://projects.blender.org/blender/blender/commit/d9cdfba21e5)
  - Use const; move constructors to implementation file.
    [rBa725b6f7](https://projects.blender.org/blender/blender/commit/a725b6f7ac6)
  - Fix several material import bugs, remove wrong assert.
    [rB606b0d7d](https://projects.blender.org/blender/blender/commit/606b0d7da34)
  - Apply axes transform to exportable Nurbs.
    [rBd882a63e](https://projects.blender.org/blender/blender/commit/d882a63e981)
  - Move MTL writer code near OBJ writer code.
    [rBae1c5f16](https://projects.blender.org/blender/blender/commit/ae1c5f16cb7)
  - Skip parsing extra texture map options, move MTLWriter from Objects
    code.
    [rB99ededd9](https://projects.blender.org/blender/blender/commit/99ededd9472)
  - Clean up: use const, header cleanup.
    [rBa732abd9](https://projects.blender.org/blender/blender/commit/a732abd99b6)
  - Move file open errors to constructors.
    [rB491bd8b8](https://projects.blender.org/blender/blender/commit/491bd8b8bbc)
  - Add a public member function in MaterialWrap.
    [rBc815ba0a](https://projects.blender.org/blender/blender/commit/c815ba0a3c0)
  - Cleanup : clang format; cout \> cerr
    [rBc5e0e82f](https://projects.blender.org/blender/blender/commit/c5e0e82f68c)

### Week 10 August 4 - 7

Committed material creation prototype & fixed several issues later on.

  - Initial material creation support.
    [rBc401d8a0](https://projects.blender.org/blender/blender/commit/c401d8a0ae3).
      - Add created material to the object.
        [rB9b8f2042](https://projects.blender.org/blender/blender/commit/9b8f2042b0d)
      - Use Map's lookup instead of direct pointer to MTLMaterial
        [rB64ff38a7](https://projects.blender.org/blender/blender/commit/64ff38a7a7c).
      - Fix bsdf\_ being null; asserts for SOCK\_RGBA
        [rBd5ad01ed](https://projects.blender.org/blender/blender/commit/d5ad01edf3e).
      - Don't add null Image\* to the p-BSDF node.
        [rB2c20b379](https://projects.blender.org/blender/blender/commit/2c20b379f90).
      - Load image and add it to image texture node.
        [rBf9348be8](https://projects.blender.org/blender/blender/commit/f9348be84f2)
  - Cleanup: Renaming, comments.
    [rBb8e4d4c6](https://projects.blender.org/blender/blender/commit/b8e4d4c6dad)
    [rBd6604558](https://projects.blender.org/blender/blender/commit/d6604558823)
    [rB06336941](https://projects.blender.org/blender/blender/commit/06336941b30)
    [rBd081bf97](https://projects.blender.org/blender/blender/commit/d081bf97df9).
  - Use pragma once as per style guide.
    [rB0a339bb5](https://projects.blender.org/blender/blender/commit/0a339bb5e7b),
    [rB6e419e95](https://projects.blender.org/blender/blender/commit/6e419e95e77).
  - Refactors:
      - Replace enum and its functions with Map for maps
        [rB928736b1](https://projects.blender.org/blender/blender/commit/928736b173b)
      - Move index offset from parsing to Object creation
        [rB7f289384](https://projects.blender.org/blender/blender/commit/7f2893848ec)
      - Use std::string\_view for string splitting. This gave 33%
        speedup.
        [rB9616e2ef](https://projects.blender.org/blender/blender/commit/9616e2ef78f)
        ![OBJ Import using \`string\_view\` with the file in Week 7 &
        6.](../../../images/OBJ_import_using_string_view.png
        "OBJ Import using \`string_view\` with the file in Week 7 & 6.")
      - Create new Geometry instance at the start.
        [rB9b37f943](https://projects.blender.org/blender/blender/commit/9b37f943246)

<!-- end list -->

  - Accept 3D UV vertex coordinates
    [rB6ee696e5](https://projects.blender.org/blender/blender/commit/6ee696e5bfb)
  - Fix wrong material exporter assert added recently.
    [rB85989931](https://projects.blender.org/blender/blender/commit/85989931570)

### Week 9 July 27 - 31

  - Updated design doc:
    <https://docs.google.com/document/d/17Uzl47OljjoKgaMbukiLHUVGQP220lPTmPS-atb65mw/edit>
  - Add fallback value in string to number conversions.
    [rBc5bd1631](https://projects.blender.org/blender/blender/commit/c5bd1631f60)
  - Add MTL parser.
    [rB19145856](https://projects.blender.org/blender/blender/commit/19145856ba7)
  - Cleanup: Use MutableSpan, add asserts & make exporter's material
    code similar to importer's.
    [48d3582196](https://projects.blender.org/blender/blender/commit/48d3582196f2)
  - Initial material creation support.
    [rBc401d8a0](https://projects.blender.org/blender/blender/commit/c401d8a0ae3b)

I could not work for the whole week productively.

  - Off-topic (Master) Enable header auto-complete suggestions in Xcode.
    [rB27d50d6f](https://projects.blender.org/blender/blender/commit/27d50d6f67a)
    This turned into a standardised way of dealing with Xcode's include
    file paths.
  - Off-topic: Respond to a new developer on [a bug
    fix](https://developer.blender.org/D8342) and the [devtalk
    thread](https://devtalk.blender.org/t/bug-fix-help-object-visibility-out-of-sync-with-the-outliner/14388/16)
  - Off-topic: Tested
    [ClangBuildAnalyzer](https://github.com/aras-p/ClangBuildAnalyzer)
    on Blender (lite build).

### Week 8: July 20 - 25

  - Move vertex (and UV vertex) list outside \`OBJRawObject\` so that
    objects that are defined (\`o curvename\`) after the list of
    vertices is written can access them using relative indices.
    [71eadb4b62](https://projects.blender.org/blender/blender/commit/71eadb4b628)
  - Support NURBS curve import
    [6e21f8c20d](https://projects.blender.org/blender/blender/commit/6e21f8c20da).
    Got help from Howard Trickey for fixing a visibility bug.
    [af278ce58b](https://projects.blender.org/blender/blender/commit/af278ce58bc)
  - Fix build errors: avoid \`uint\` with Vector's \`size()\` function.
    [7bd38c2776](https://projects.blender.org/blender/blender/commit/7bd38c27761).
    Style guide was updated to discourage \`uint\` usage and \`{}\`
    initialisation doesn't allow narrowing conversions. There's still a
    lot of code in OBJ exporter that uses \`uint\`, that's for a Friday.
    This commit was only for removing \`uint\` with \`Vector.size()\`.
  - Worked on Refactor: Conform to style guide, add documentation.
    [97aa9d44fa](https://projects.blender.org/blender/blender/commit/97aa9d44fa0).
  - Move Object creation code in Mesh creation class.
    [2bb56c83fa](https://projects.blender.org/blender/blender/commit/2bb56c83faa).
    This helps adding deform groups and materials to the \`Object\` in
    one place and away from the code that adds that \`Object\` to a
    Collection.
  - Rename \`exportable\_meshes\` to \`exportable\_as\_mesh\`
    [4716660591](https://projects.blender.org/blender/blender/commit/47166605915).
    (This was long due.)
  - Support importing vertex deform groups.
    [25526821a4](https://projects.blender.org/blender/blender/commit/25526821a4b).
  - Use VectorSet instead of vector for def\_group.
    [b33a4592a3](https://projects.blender.org/blender/blender/commit/b33a4592a38).
    The commit
    [37467ec5e9](https://projects.blender.org/blender/blender/commit/37467ec5e9a)
    used \`std::vector<std::string>\` which is slow due to linear
    searching. Jacques Lucke suggested using \`VectorSet\` for faster
    lookups. Also \`StringRef\` is used to avoid copying.
  - Cleanup: silence clang-tidy warnings; comments.
    [ffaa1df439](https://projects.blender.org/blender/blender/commit/ffaa1df4397).
  - Wrote an intial draft of importer's design document.
    [T68936\#982751](https://developer.blender.org/T68936#982751). Still
    need to incorporate mentors' feedback on the document. [Google doc
    for Importer
    design](http://docs.google.com/document/d/17Uzl47OljjoKgaMbukiLHUVGQP220lPTmPS-atb65mw/edit?usp=sharing)

#### Next Week plans

  - Fix design doc
  - MTL and material grouping.
  - Port a portion of mesh editing tools written in python to C++:
    \`bpy\_utils.mesh.ngon\_tessellate\` (in
    \`release/scripts/modules/bpy\_extras/mesh\_utils.py\`
  - Add error handling for bad meshes or fix them.

### Week 7: July 13 - 17

  - Break code into mesh, objects, and reader files.
    [rB7582bbd5](https://projects.blender.org/blender/blender/commit/7582bbd5740)
  - Directly create Mesh without using intermediate BMesh
    [ec04edfe5c](https://projects.blender.org/blender/blender/commit/ec04edfe5c3).
    It lowered the peak memory usage by half. Also made mesh creation
    faster.
  - Use \`stoi\` and \`stof\` instead of \`\>\>\` for faster parsing.
    [aacb1f4756](https://projects.blender.org/blender/blender/commit/aacb1f4756a).
    Time taken in string to float conversion is less now as compared to
    \`\>\>\` operator. But the cost is in splitting the string and
    allocations made for several substrings. Overall this is faster. ![
    OBJ import the same file as last week using \`stoi\`. cube subd 8;
    23.3MB file.](../../../images/OBJ_file_import_using_stoi.png
    " OBJ import the same file as last week using \`stoi\`. cube subd 8; 23.3MB file.")
  - Fix UV crashes and redundant loops in v1//vn1 case.
    [rB92be92be](https://projects.blender.org/blender/blender/commit/92be92befea)
  - Support meshes with edges with or without polygons.
    [5a9b983263](https://projects.blender.org/blender/blender/commit/5a9b983263c)
  - Importer: Support smooth shading for polygons.
    [rB031c4732](https://projects.blender.org/blender/blender/commit/031c4732f45)
  - Importer: Fix misaligned UV vertices of a mesh
    [rBfe4c5350](https://projects.blender.org/blender/blender/commit/fe4c5350c46)
  - Exporter: fix vertex normals not being exported when smooth shading
    is enabled. Calculating smooth groups before exporting normals was
    the solution.
    [rB501ead4b](https://projects.blender.org/blender/blender/commit/501ead4bbc8)

#### Week 8 planned tasks

  - Due to curves, the parser needs some changes in how it stores the
    vertices. There can be vertices that belong to no object. And then
    later on an object refers to them with relative indices. So there
    needs to be another copy of all vertices which can be referred to by
    curves.
  - Grouping
  - MTL

### Week6: July 6 - 10

  - Setup clang tidy with Ninja. On macOS, had to build llvm + clang 11
    + tidy to get the executable. Also setup a build with clang 11 +
    leak sanitiser since Xcode's asan doesn't provide the functionality.
  - Import a mesh successfully in the viewport. It took a long time,
    trying to avoid Mesh, BMesh asserts, seg-faults and memory leaks due
    to un-freed meshes.
      - The mesh has vertex data only, so far. Texture coordinates are
        to be added.
      - The parser takes a long time. \~70% of the import process.
        Making a Blender object from the parsed data is the rest \~30%.
        So need to optimise it.
      - The importer is still 3-4 times faster than the python one.

Side note: an interesting diff is under review: Use mmap() IO for
reading uncompressed .blends [D8246](http://developer.blender.org/D8246)

#### Week 7 planned tasks

  - Texture coordinates, normals and curves.
  - Vertex Grouping, object grouping.
  - Material library and grouping.

### Week 5: June 29 - July 4

  - Support smooth groups and bit flags, per face
    [rBd85c2620](https://projects.blender.org/blender/blender/commit/d85c2620)
    [rBfe3a359f](https://projects.blender.org/blender/blender/commit/fe3a359fb1fff0226e)
  - Support vertex groups per face
    [rB012175f8](https://projects.blender.org/blender/blender/commit/012175f8437)
  - Support modifier's viewport and render properties
    [639d512369](https://projects.blender.org/blender/blender/commit/639d512369dc9c0a61)
  - Review update to adhere to style guide & comments update.
    [rBc2eb16f6](https://projects.blender.org/blender/blender/commit/c2eb16f66249)
  - Null Checks: material
    [rB35dc587f](https://projects.blender.org/blender/blender/commit/35dc587f),
    image
    [rB46939815](https://projects.blender.org/blender/blender/commit/46939815),
    UV map
    [rB02fa1f0b](https://projects.blender.org/blender/blender/commit/02fa1f0b),
    deform vert
    [5a1ecb1702](https://projects.blender.org/blender/blender/commit/5a1ecb170293d468).

Ticked exporter checkbox in the status tracker task.
[\#68936](http://developer.blender.org/T68936)

### Week 4: June 22-27

  - Review update and cleanup in the OOP
    refactor([rBf2a1a66b](https://projects.blender.org/blender/blender/commit/f2a1a66b8c90)):
    [rB0f383a3d](https://projects.blender.org/blender/blender/commit/0f383a3d7c9a).
  - Support NURBS being exported in parameter form, optionally. So far,
    they were exported as meshes.
    [rB31d48017](https://projects.blender.org/blender/blender/commit/31d480174a8caebeeacac14902b02a).
      - Support NURBS surfaces to be exported as a regular mesh. (same
        as above
        [rB31d48017](https://projects.blender.org/blender/blender/commit/31d480174a8c))
  - Broke down files to create new ones for \`OBJMesh\` and
    \`OBJNurbs\`.
    [rB6c98925d](https://projects.blender.org/blender/blender/commit/6c98925d5ad8)
    Later on, \`MTLWriter\` will also have its own implementation file.
  - Support material library export. We decided against using the
    existing python writer, since there were no benefits of using it
    except saving some time now. To extend support, one would have to
    edit [Node Shader
    Utils](https://docs.blender.org/manual/en/latest/addons/import_export/node_shaders_info.html)
    file first. Also debugging it would be a trouble. Since nodes are
    already in C/C++, we have more control there, instead of in the API.
      - Export of single material per object:
        [rB827869a4](https://projects.blender.org/blender/blender/commit/827869a45bcc).
        Support for multiple materials in one object:
        [rBfaa11ec0](https://projects.blender.org/blender/blender/commit/faa11ec04a4d)
  - Changes in the UI:
      - Object groups:
        [rB90869897](https://projects.blender.org/blender/blender/commit/90869897442a)
      - Material groups:
        [rBbab0fce9](https://projects.blender.org/blender/blender/commit/bab0fce9141b)
      - Export selected objects only:
        [rBd724bf37](https://projects.blender.org/blender/blender/commit/d724bf37972a).
  - Bug fixes:
      - Duplicate texture coordinates:
        [rBef0eff0b](https://projects.blender.org/blender/blender/commit/ef0eff0b8a42)
      - Show red colored warning if overriding an existing file while
        exporting (it was a wrong flag)
        [rBc39128ca](https://projects.blender.org/blender/blender/commit/c39128ca97bf)
      - Fix socket vector being read as socket RGBA
        [rB7685d9e4](https://projects.blender.org/blender/blender/commit/7685d9e4509c)

#### Remaining tasks

  - Smooth groups (and bitflags) & Vertex groups
  - Start with importer. I'm already lagging here
    [User:Ankitm/GSoC\_2020\_Proposal\_IO\_Perf\#Project\_Schedule](User:Ankitm/GSoC_2020_Proposal_IO_Perf#Project_Schedule).
    But since the refactor I had planned for week 7 (along with
    profiling) won't take long, I feel that I have a good buffer to work
    in.

### Week 3: June 15-19

  - Animation: Allow all frames to be exported, instead of 0-1000. Also
    filenames are also edited with frame number only if there are
    multiple frames.
    [rB45461012](https://projects.blender.org/blender/blender/commit/454610123787a7b)
  - Worked on exposing more settings in the UI:
      - triangulating a mesh,
        [rBc3f8e955](https://projects.blender.org/blender/blender/commit/c3f8e9550c1ea54a9e16e56fd2945e0c)
      - export UV vertices, export normals
        [rB41f47cd8](https://projects.blender.org/blender/blender/commit/41f47cd8d6a4699bafa4f)
      - axes transform (up, forward axes).
        [rB1d75ece6](https://projects.blender.org/blender/blender/commit/1d75ece6add7ad7f2661)
        Scaling factor
        [rBd47318fb](https://projects.blender.org/blender/blender/commit/d47318fb4537277e301)
  - Add support for curves to be exported after converting them to
    meshes.
  - Refactor the code upto now to object-oriented style. (commit
    \[todo\], in review [D8089](http://developer.blender.org/D8089))  
    This allowed us to have minimal memory overhead with no timing cost.
    UV vertices, however, have to be allocated on heap and thus show a
    peak while exporting.

Unfortunately, I couldn't finish some goals at time: Material library,
NURBS and grouping settings. So the goal for next week is to finish them
as soon as possible and start with importer.

### Week 2: June 8-13

  - The OBJ exporter now exports Texture Coordinates.
    [rBf01a7d50](https://projects.blender.org/blender/blender/commit/f01a7d508d0d).
    Not the MTL file yet. It's a goal of Week 3.  
    Here's a table of comparison of a single object to one file which
    includes UV coordinates too.

<table>
<thead>
<tr class="header">
<th><p>Object</p></th>
<th><p>File Size (There's a little difference due to the extra info like MTL in python )<br />
(C++/ Python + MTL)</p></th>
<th><p>Export Time (`fstream`/`fprintf`)<br />
(s)</p></th>
<th><p>Export time Python<br />
(s)</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Default cube + 8 subdivision surface levels</p></td>
<td><p>61.6 MB / 63 MB</p></td>
<td><p>3.1 / 1.9 s</p></td>
<td><p>20 s</p></td>
</tr>
<tr class="even">
<td><p>Default cube + 9 subdivision surface levels</p></td>
<td><p>257.3 MB / 262 MB + 229 bytes MTL</p></td>
<td><p>10.7 / 6.7 s</p></td>
<td><p>82 s</p></td>
</tr>
<tr class="odd">
<td><p>Ico-sphere + 8 subdivision surface levels</p></td>
<td><p>663.1 MB / 674 MB + 124 bytes MTL</p></td>
<td><p>27.7 / 19 s</p></td>
<td><p>210 s</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

  - Since fstream is not adding any benefits and is slower too, it was
    removed.
  - It also exports multiple objects in the same file. But so far only
    \`OB\_MESH\` object types are supported.
    [rB40736795](https://projects.blender.org/blender/blender/commit/40736795ec4e).
  - Multiple frames can also be exported now, in individual files.
    [rB6d088bdd](https://projects.blender.org/blender/blender/commit/6d088bdde46a).
  - Progress is now shown on per-file basis in Terminal and timing in
    the end. It should be improved to act on per-object basis when there
    is a single frame.
  - The windows build to test is up:
    <https://blender.community/c/graphicall/Pmbbbc/>

#### Planned for Week 3

  - Curves, Axes transform, Scale transform.
  - Object Grouping, Indices (absolute is already there, will discuss
    about relative.)
  - Materials, Triangulation.

### Week 1: June 1-6

  - Received a lot of input from the mentors regarding coding style,
    comments, documentation, and file structures. Spent quite some time
    on cleaning the hastily committed
    [rB485cc433](https://projects.blender.org/blender/blender/commit/485cc433).
    See current version at
    [Diffusion](https://developer.blender.org/diffusion/B/browse/soc-2020-io-performance/source/blender/io/wavefront_obj/;02c7cf5322dc)
  - Currently the exporter exports vertices, face normals, and face
    indices. In world coordinates.
  - It's lacking in (1) multiple objects, (2) texture coordinates (3)
    Axes transformation. If you compare obj txt files written with
    python & C++ , you'll see differences in vertex order, and some
    coordinates with opposite sign. But the shape is correct.
  - Did a comparison between the old & new exporters. Release
    configuration, lite build + opensubdiv, default cube with 9 subsurf
    modifiers. See images:
    [F8586987](https://developer.blender.org/F8586987) &
    [F8586984](https://developer.blender.org/F8586984). We're checking
    the time taken by \`wm\_handler\_fileselect\_do\` function.

<table>
<thead>
<tr class="header">
<th><p>Object</p></th>
<th><p>File Size (note that `vt` are not there in C++ yet so C++ file size is smaller.)<br />
(C++/ Python + MTL)</p></th>
<th><p>Export Time (`fstream`/`fprintf`)<br />
(s)</p></th>
<th><p>Export time Python<br />
(s)</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Default cube + 8 subdivision surface levels</p></td>
<td><p>48 MB / 63 MB</p></td>
<td><p>2.4 / 1.47 s</p></td>
<td><p>20 s</p></td>
</tr>
<tr class="even">
<td><p>Default cube + 9 subdivision surface levels</p></td>
<td><p>186 MB / 262 MB + 229 bytes MTL</p></td>
<td><p>7.6 / 4.6 s</p></td>
<td><p>82 s</p></td>
</tr>
<tr class="odd">
<td><p>Ico-sphere + 8 subdivision surface levels</p></td>
<td><p>478 MB / 674 MB + 124 bytes MTL</p></td>
<td><p>22 / 13 s</p></td>
<td><p>210 s</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

#### Planned for Week 2

  - Textures, multiple objects in the same file.
  - Modifiers, Curves, Multiple frames , Axes transform, Progress
    logging

### Week 0: before June 1

  - Updated the goals to move OBJ in the beginning & PLY in the end. See
    timeline:
  - Set up the branch \`soc-2020-io-performance\` : [See on
    diffusion](https://developer.blender.org/diffusion/B/history/soc-2020-io-performance/)
  - Project status tracker and design discussion task on Phab:
    [\#68936](http://developer.blender.org/T68936)
  - Get familiar with gperftools, see [ gperftools installation and use
    ](../gperftools_installation_on_mac)
  - Set up UI and file-handler API for both exporter & importer (OBJ).

#### Planned for Week 1

  - Export vertex coordinates, vertex normals, faces & texture
    coordinates.
