## Improving IO performance for big files - Final report

I wholeheartedly thank my mentors, Dr. Sybren Stüvel and Dr. Howard
Trickey, for their patience, code review, and constant guidance. I also
thank Jacques Lucke for reviewing the code throughout the project and
extending functionalities in the master branch needed for the project.

### Introduction

The project aimed at improving import/export time of 3D file formats:
PLY and STL by rewriting them in C++. The goals were adjusted at the
beginning of the program to prioritize Wavefront OBJ since it supports
more features.

### Status

Wavefront OBJ (and material library, MTL) exporter and importer work
like a replacement of the older one. I could not work on PLY and STL,
unfortunately. I will work on them in the coming months.

  - Proposal: [Proposal IO
    Perf](Proposal_IO_Perf.md)
  - Differential for exporter + shared code:
    [D8754](http://developer.blender.org/D8754)
  - Differential for importer:
    [D8753](http://developer.blender.org/D8753)
  - Branch:
    [soc-2020-io-performance](https://developer.blender.org/diffusion/B/browse/soc-2020-io-performance/source/blender/io/wavefront_obj/)
  - Design Documents: [T68936](http://developer.blender.org/T68936)
  - Community discussion:
    [devtalk](https://devtalk.blender.org/t/gsoc-2020-faster-io-for-obj-stl-ply-feedback/13528/)

### Code structure

Detailed information can be found in the [design
docs](http://developer.blender.org/T68936), so here's a broad overview
of the code structure.  
\`source/blender/editors/io\_obj.c\` has the operator definition and
file browser UI drawing code. The C-interface is in
\`source/blender/io/wavefront/IO\_wavefront\_obj.h\`. The implementation
is in the \`source/blender/io/wavefront/intern\` directory.

[File:Wavefront\_importer\_code\_structure.png|Importer](File:Wavefront_importer_code_structure.png%7CImporter)
[File:Wavefront\_obj\_exporter\_main\_code\_structure.png|Exporter](File:Wavefront_obj_exporter_main_code_structure.png%7CExporter)

### Comments

These are a few comments I thought are worth mentioning.

Significant speed gains came directly by virtue of language and making
fewer memory allocations. However, some modifications that gave speedup
were:

  - Use \`std::fprintf\` instead of \`std::ofstream\` to write the
    files. The former was consistently about 40-50% faster for writing
    the same file. [reference: week
    2](Weekly_Reports.md#Week_2:_June_8-13)
  - Fetching data at the time of writing, instead of storing it in lists
    (and duplicating the scene in memory). My inital concern was that
    adding more tasks in the way of writing could be slow. But profiling
    showed that the time was actually saved by removing the large memory
    allocations. [reference: week
    3](Weekly_Reports.md#Week_3:_June_15-19)
  - Use \`std::stof\`/ \`std::stoi\` for converting strings to numbers
    instead of \`\>\>\`. While the latter is convenient and safe, it's
    slow due to the same reasons. [reference: week
    7](Weekly_Reports.md#Week_7:_July_13_-_17)
  - Avoided \`BMesh\`. It's slightly easier to create vertices using a
    \`BMesh\`, but the downsides were: more memory usage, slow creation
    & slow conversion to \`Mesh\`. [reference: week
    7](Weekly_Reports.md#Week_7:_July_13_-_17)
  - Minimise string allocations and operations by using
    \`blender::StringRef\` instead of creating new strings (for e.g. in
    splitting a line into components). [reference: week
    10](Weekly_Reports.md#Week_10_August_4_-_7).

For profiling,
[Instruments.app](https://en.wikipedia.org/wiki/Instruments_\(software\))
was used. It comes bundled with Xcode and is easy to use. Other tools
that I tried are:
[gperftools](gperftools_installation_on_mac.md),
[dtrace](https://github.com/opendtrace/toolkit) with
[Flamegraph](https://github.com/brendangregg/FlameGraph) (script:
[P1161](https://developer.blender.org/P1611)),
[py-spy](https://github.com/benfred/py-spy) for python scripts.

### Advice I'd give to my past self

  - Try to make reasonable time estimates for milestones. At first,
    plans may go wrong, and one may find oneself not meeting deadlines.
    But with time, as one gets more familiar with the codebase,
    estimates improve.
  - Discuss the design before implementing it. Refactoring will be a
    time sink later on.

### Comparisons

The following tables note the time taken by
\`wm\_fileselect\_handler\_do\` which is the last common function for
both the old and new implementations before the code path diverges. The
time taken by the user to select the file is not added to this
function.  
Enabled export settings: write materials, write normals, write UV
coordinates.

<table>
<thead>
<tr class="header">
<th><p>Default cube + 9 subdivision<br />
surface levels. (triangulated faces)</p></th>
<th><p>File Size (MB)</p></th>
<th><p>Time Old (s)</p></th>
<th><p>Time New (s)</p></th>
<th><p>Speed-up</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Import</p></td>
<td><p>365</p></td>
<td><p>101</p></td>
<td><p>15</p></td>
<td><p>6.7X</p></td>
</tr>
<tr class="even">
<td><p>Export</p></td>
<td><p>365</p></td>
<td><p>132</p></td>
<td><p>14</p></td>
<td><p>9.4X</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

<table>
<thead>
<tr class="header">
<th><p>Default Cylinder: 2038 copies.<br />
(non-triangulated faces)</p></th>
<th><p>File Size (MB)</p></th>
<th><p>Time Old (s)</p></th>
<th><p>Time New (s)</p></th>
<th><p>Speed-up</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Import</p></td>
<td><p>20</p></td>
<td><p>84.6</p></td>
<td><p>1.5</p></td>
<td><p>56x</p></td>
</tr>
<tr class="even">
<td><p>Export</p></td>
<td><p>20</p></td>
<td><p>12.3</p></td>
<td><p>.9</p></td>
<td><p>13x</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### Future work

  - As mentioned above, PLY and STL are pending.
  - Currently the MTL exporter and importers have their classes for
    nodetree operations. The importer uses \`ShaderNodetreeWrap\` class
    to create a nodetree from materials in MTL file. The exporter uses
    \`MaterialWrap\` class to traverse an \`Object\`'s nodetree and
    extract the data for MTL file. It would be good to extend them for
    any other writer/reader that operates on material data. On the
    python side, \`node\_shader\_utils.py\` provides such an utility.
  - All the three file formats have shared functionality that would be
    good to extract in \`source/blender/io/common\` to reduce code
    duplication. For e.g: exporters need to obtain object names, vertex
    coordinates, total number of elements etc. Similarly for the
    importer, if coordinates are stored in flat lists, mesh creation
    code would be the same.
  - \`ngon\_tessellate\`, which was blatantly ported from
    \`mesh\_utils.py\` to \`mesh\_utils.cc\`, can be improved further to
    be generic. Currently, the way it accepts arguments and returns
    values is tightly governed by the OBJ importer.
  - Support more options for NURBS curves and surfaces. This is hindered
    in part by the lenient file format specification, e.g., some
    implementations use \`vp\` instead of \`v\` for vertex coordinates,
    \`curv2\` instead of \`curv\` etc.
