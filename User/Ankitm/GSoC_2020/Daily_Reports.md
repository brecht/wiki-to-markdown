## Daily Reports

### Week 12

Weekly Summary
[User:Ankitm/GSoC\_2020\_Weekly\_Reports\#Week\_12\_August\_17\_-\_21](User:Ankitm/GSoC_2020_Weekly_Reports#Week_12_August_17_-_21)

#### August 23

  - Read multiple lines with "\\\\n" line breaks.
    [rBa662ddd7](https://projects.blender.org/blender/blender/commit/a662ddd7)
  - Cleanup: Mark unused variables.
    [rB8c6a06e7](https://projects.blender.org/blender/blender/commit/8c6a06e7)
  - Silence mesh\_validate verbose warnings.
    [rB32fe2869](https://projects.blender.org/blender/blender/commit/32fe2869)

#### August 22

  - Use \`blender::StringRef\` instead of \`std::string\_view\`.
    [rB73454388](https://projects.blender.org/blender/blender/commit/73454388)
  - Use remove\_prefix for easy transition to StringRef.
    [rBae122533](https://projects.blender.org/blender/blender/commit/ae122533)

#### August 21

  - For polygons with holes:
      - Commit as it is ngon-tessellate.
        [rBefab0bc7](https://projects.blender.org/blender/blender/commit/efab0bc7)
      - Fix tessellate crash due to wrong Vector size.
        [rB5b0cb5bb](https://projects.blender.org/blender/blender/commit/5b0cb5bb)
      - Add newly creates faces to imported faces and dissolve edges.
        [rBe76ab124](https://projects.blender.org/blender/blender/commit/e76ab124)
      - Edit FaceElem list outside polygon creation code.
        [rB10e3f23b](https://projects.blender.org/blender/blender/commit/10e3f23b)
  - Move object creation code from constructor.
    [rB4777a6a5](https://projects.blender.org/blender/blender/commit/4777a6a5)
  - Remove unused uv vertex index offset.
    [rBedd5307e](https://projects.blender.org/blender/blender/commit/edd5307e)

#### August 19-20

  - Kept working on \`ngon\_tessellate\` and adding the new polygons to
    the mesh.

#### August 18

  - Fix overlapping nodes in the nodetree.
    [rB0c7801fa](https://projects.blender.org/blender/blender/commit/0c7801fa2b6)
  - Silence normal being zero warnings in mesh validate's verbose mode.
    [rB3f8a8d17](https://projects.blender.org/blender/blender/commit/3f8a8d1757a)
  - Cleanup: use const, remove single use variables.
    [rB69270c78](https://projects.blender.org/blender/blender/commit/69270c7800c)

Working on writing \`ngon\_tessellate\` and \`tessellate\_polygon\` in
C++ for ngon polygons with holes in them.

#### August 17

  - Parse vertex normals: vn lines.
    [rBc583afd6](https://projects.blender.org/blender/blender/commit/c583afd60b3)
  - Decouple getting smooth groups from export parameters
    [rB0479dc41](https://projects.blender.org/blender/blender/commit/0479dc410fd)
  - Export vertex normals if smooth shading is enabled.
    [rB4923087d](https://projects.blender.org/blender/blender/commit/4923087d253)
  - Move file open errors to constructors.
    [rB491bd8b8](https://projects.blender.org/blender/blender/commit/491bd8b8bbc)
  - Add a public member function in MaterialWrap.
    [rBc815ba0a](https://projects.blender.org/blender/blender/commit/c815ba0a3c0)

### Week 11

Weekly summary:
User:Ankitm/GSoC\_2020\_Weekly\_Reports\#Week\_11\_August\_10\_-\_15

#### 14 August

  - Apply axes transform to exportable Nurbs.
    [rBd882a63e](https://projects.blender.org/blender/blender/commit/d882a63e981)
  - Work on separating material data collection and MTL writers. Fix
    bugs in MTL import too.

#### 13 August

  - Fix usemtl line not being written.
    [rB69f70829](https://projects.blender.org/blender/blender/commit/69f70829d2c)
  - Use function pointer; remove redundant variables.
    [rBba0d376e](https://projects.blender.org/blender/blender/commit/ba0d376e260)
  - Export packed image file names as if they're in current directory.
    [rBd9cdfba2](https://projects.blender.org/blender/blender/commit/d9cdfba21e5)
  - Use const; move constructors to implementation file.
    [rBa725b6f7](https://projects.blender.org/blender/blender/commit/a725b6f7ac6)
  - Fix several material import bugs, remove wrong assert.
    [rB606b0d7d](https://projects.blender.org/blender/blender/commit/606b0d7da34)

#### 12 August

  - Fix several material import bugs, remove wrong assert.
    [rB606b0d7d](https://projects.blender.org/blender/blender/commit/606b0d7da34).

#### 11 August

  - Fix loose edge export & import, and offsets.
    [rBc010cf58](https://projects.blender.org/blender/blender/commit/c010cf58147).
  - Fix vertex offsets for MEdges and MLoops.
    [rBe0c0b8ff](https://projects.blender.org/blender/blender/commit/e0c0b8ffd17).

#### 10 August

  - Catch out of range exceptions for stoi/stof.
    [rBbb2eca07](https://projects.blender.org/blender/blender/commit/bb2eca07a68).
  - Add vertex offset due to Curves' vertices also.
    [rBb378f84f](https://projects.blender.org/blender/blender/commit/b378f84f7c5).
  - Fix new Geometry creation with empty initial object.
    [rB009b3771](https://projects.blender.org/blender/blender/commit/009b37719dd).
  - Use const& instead of pointers in Mesh class.
    [rB82eff7a0](https://projects.blender.org/blender/blender/commit/82eff7a0255).
  - Correct geometry creation code with Curves.
    [rBd062c712](https://projects.blender.org/blender/blender/commit/d062c712e20).
  - Accept 3D UV vertex coordinates
    [rB6ee696e5](https://projects.blender.org/blender/blender/commit/6ee696e5bfb).
  - Create new Geometry instance at the start.
    [rB9b37f943](https://projects.blender.org/blender/blender/commit/9b37f943246).

### Week 10

Weekly Summary
[User:Ankitm/GSoC\_2020\_Weekly\_Reports\#Week\_10\_August\_4\_-\_7](User:Ankitm/GSoC_2020_Weekly_Reports#Week_10_August_4_-_7)

#### August 7

Minor cleanups

  - Remove deleted header from CMakeLists.txt.
    [rB6e419e95](https://projects.blender.org/blender/blender/commit/6e419e95e77).
  - Use pragma once as per style guide.
    [rB0a339bb5](https://projects.blender.org/blender/blender/commit/0a339bb5e7b).
  - Fix crash due to OBJ files with no \`o name\` line.
    [rB3cff0de4](https://projects.blender.org/blender/blender/commit/3cff0de4359).

#### August 6

  - Don't add null Image\* to the p-BSDF node.
    [rB2c20b379](https://projects.blender.org/blender/blender/commit/2c20b379f90).
  - Avoid .data() while creating a std::string from StringRef.
    [rBbefe950f](https://projects.blender.org/blender/blender/commit/befe950f186).
  - Replace std::string\_view with StringRef.
    [rB8339dd66](https://projects.blender.org/blender/blender/commit/8339dd66477).
  - Remove OBJ prefix from containers.
    [rB1b1727ea](https://projects.blender.org/blender/blender/commit/1b1727ea6a7).
  - Cleanup: Renaming, comments.
    [rBd6604558](https://projects.blender.org/blender/blender/commit/d6604558823)
    [rB06336941](https://projects.blender.org/blender/blender/commit/06336941b30)
    [rBd081bf97](https://projects.blender.org/blender/blender/commit/d081bf97df9).

#### August 5

  - Cleanup: clang-tidy warnings
    [rBb8e4d4c6](https://projects.blender.org/blender/blender/commit/b8e4d4c6dad)
  - Load image and add it to image texture node.
    [rBf9348be8](https://projects.blender.org/blender/blender/commit/f9348be84f2)
  - Add created material to the object.
    [rB9b8f2042](https://projects.blender.org/blender/blender/commit/9b8f2042b0d)
  - Replace enum and its functions with Map for maps
    [rB928736b1](https://projects.blender.org/blender/blender/commit/928736b173b)

#### August 4

Committed material creation prototype & fixed several issues later on.

  - Cleanup: Use MutableSpan, add asserts
    [rB48d35821](https://projects.blender.org/blender/blender/commit/48d3582196f).
  - Initial material creation support.
    [rBc401d8a0](https://projects.blender.org/blender/blender/commit/c401d8a0ae3).
  - MTL: release shader\_output\_ node instead of bsdf\_.
    [rB8e58bd09](https://projects.blender.org/blender/blender/commit/8e58bd0996d).
  - MTL: use mtllib for importing MTL files.
    [rB722a793b](https://projects.blender.org/blender/blender/commit/722a793b748).
  - Use Map's lookup instead of direct pointer to MTLMaterial
    [rB64ff38a7](https://projects.blender.org/blender/blender/commit/64ff38a7a7c).
  - Fix bsdf\_ being null; asserts for SOCK\_RGBA
    [rBd5ad01ed](https://projects.blender.org/blender/blender/commit/d5ad01edf3e).

#### August 3

Holiday

### Week 9

Weekly Summary:
[User:Ankitm/GSoC\_2020\_Weekly\_Reports\#Week\_9\_July\_27\_-\_31](User:Ankitm/GSoC_2020_Weekly_Reports#Week_9_July_27_-_31)

#### July 30

  - Add MTL parser.
    [rB19145856](https://projects.blender.org/blender/blender/commit/19145856ba7)
  - Off-topic (Master) Enable header auto-complete suggestions in Xcode.
    [rB27d50d6f](https://projects.blender.org/blender/blender/commit/27d50d6f67a)
    This turned into a standardised way of dealing with Xcode's include
    file paths.
  - Off-topic Respond to a new developer on [a bug
    fix](https://developer.blender.org/D8342) and the [devtalk
    thread](https://devtalk.blender.org/t/bug-fix-help-object-visibility-out-of-sync-with-the-outliner/14388/16)

#### July 29

  - Add fallback value in string to number conversions.
    [rBc5bd1631](https://projects.blender.org/blender/blender/commit/c5bd1631f60)
  - Tested some Xcode attributes used for header search paths with a
    final goal of enabling auto-complete suggestions,

#### July 28

  - Updated design doc:
    <https://docs.google.com/document/d/17Uzl47OljjoKgaMbukiLHUVGQP220lPTmPS-atb65mw/edit>

#### July 27

  - Discussed naming conventions with mentors.
  - Worked on MTL parser.
  - Off-topic: checked out
    [ClangBuildAnalyzer](https://github.com/aras-p/ClangBuildAnalyzer)
    on Blender (lite build).

### Week 8

Weekly Summary:
[User:Ankitm/GSoC\_2020\_Weekly\_Reports\#Week\_8:\_July\_20\_-\_25](User:Ankitm/GSoC_2020_Weekly_Reports#Week_8:_July_20_-_25)

#### July 24

  - Support importing vertex deform groups.
    [rB25526821](https://projects.blender.org/blender/blender/commit/25526821a4b).
  - Keep vertex deform group names and def\_nr in sync
    [rB37467ec5](https://projects.blender.org/blender/blender/commit/37467ec5e9a).
    To handle the same deform groups appearing in the file multiple
    times, a deduplication is required and order of occurrence of deform
    groups is also important. So use \`std::vector<std::string>\`
    instead of \`Map\<std::string, int\>\`.
  - Use VectorSet instead of vector for def\_group.
    [rBb33a4592](https://projects.blender.org/blender/blender/commit/b33a4592a38).
    The commit
    [rB37467ec5](https://projects.blender.org/blender/blender/commit/37467ec5e9a)
    used \`std::vector<std::string>\` which is slow due to linear
    searching. Jacques Lucke suggested using \`VectorSet\` for faster
    lookups. Also \`StringRef\` is used to avoid copying.
  - Fix curve not being visible in Object mode.
    [rBaf278ce5](https://projects.blender.org/blender/blender/commit/af278ce58bc).
    Thanks to Howard Trickey for debugging this\!
  - Cleanup: silence clang-tidy warnings; comments.
    [rBffaa1df4](https://projects.blender.org/blender/blender/commit/ffaa1df4397).

#### July 23

  - Move Object creation code in Mesh creation class.
    [rB2bb56c83](https://projects.blender.org/blender/blender/commit/2bb56c83faa).
    This helps adding deform groups and materials to the \`Object\` in
    one place and away from the code that adds that \`Object\` to a
    Collection.
  - Rename exportable\_meshes to exportable\_as\_mesh
    [rB47166605](https://projects.blender.org/blender/blender/commit/47166605915).
    (This was long due.)
  - Refactor: Conform to style guide, add documentation.
    [rB97aa9d44](https://projects.blender.org/blender/blender/commit/97aa9d44fa0).

#### July 22

  - Worked on Refactor: Conform to style guide, add documentation, which
    got committed on July 23rd.
    [97aa9d44fa](https://projects.blender.org/blender/blender/commit/97aa9d44fa0).
  - Set active NURBS of a curve; set object group.
    [cab598f2b2](https://projects.blender.org/blender/blender/commit/cab598f2b2c).
  - The NURBS bug mentioned yesterday is still there.

#### July 21

  - Support NURBS curve import
    [rB6e21f8c2](https://projects.blender.org/blender/blender/commit/6e21f8c20da).
    BUT the vertices are visible in edit mode, but the black spline is
    not visible in edit mode. Committed the rest of the code since it
    was working to some extent. Will try to fix it next day.
  - Fix build errors: avoid \`uint\` with Vector's \`size()\` function.
    [rB7bd38c27](https://projects.blender.org/blender/blender/commit/7bd38c27761).
    Style guide was updated to discourage \`uint\` usage and \`{}\`
    initialisation doesn't allow narrowing conversions. There's still a
    lot of code in OBJ exporter that uses \`uint\`, that's for a Friday.
    This commit was only for removing \`uint\` with \`Vector.size()\`.

#### July 20

  - Move vertex (and UV vertex) list outside \`OBJRawObject\` so that
    objects that are defined (\`o curvename\`) after the list of
    vertices is written can access them using relative indices.
    [rB71eadb4b](https://projects.blender.org/blender/blender/commit/71eadb4b628)
  - Kept working on importing NURBS curve.

### Week 7

Weekly Summary:
[User:Ankitm/GSoC\_2020\_Weekly\_Reports\#Week\_7:\_July\_13\_-\_17](User:Ankitm/GSoC_2020_Weekly_Reports#Week_7:_July_13_-_17)

#### July 17

  - Use MutableSpan instead of raw pointers in string to float converter
    [65fd3be1fa](https://projects.blender.org/blender/blender/commit/65fd3be1fa2)
  - Support meshes with edges with or without polygons.
    [5a9b983263](https://projects.blender.org/blender/blender/commit/5a9b983263c)
  - Cleanup: Rename variables, use {} initialisation.
    [37d59dbc8c](https://projects.blender.org/blender/blender/commit/37d59dbc8cf)

#### July 16

  - Importer: Support smooth shading for polygons.
    [rB031c4732](https://projects.blender.org/blender/blender/commit/031c4732f45)
  - Importer: Fix misaligned UV vertices of a mesh
    [rBfe4c5350](https://projects.blender.org/blender/blender/commit/fe4c5350c46)
  - Move stoi into a function; Add exception handling
    [rBef5941f3](https://projects.blender.org/blender/blender/commit/ef5941f31aa)
  - Fix UV crashes and redundant loops in v1//vn1 case.
    [rB92be92be](https://projects.blender.org/blender/blender/commit/92be92befea)
  - Exporter: fix vertex normals not being exported when smooth shading
    is enabled. Calculating smooth groups before exporting normals was
    the solution.
    [rB501ead4b](https://projects.blender.org/blender/blender/commit/501ead4bbc8)
  - Exporter: Fix freeing invalid pointer in \~OBJMesh
    [rBc275b278](https://projects.blender.org/blender/blender/commit/c275b2784b7)
  - Exporter: Fix UV indices being edited by the writer.
    [rB151e8825](https://projects.blender.org/blender/blender/commit/151e8825122)

#### July 15

  - Add UV vertices to the mesh.
    [a4a1184ece](https://projects.blender.org/blender/blender/commit/a4a1184ecea).
    This needs some corrections though: the coordinates are correct, but
    UV edges are not matched to edges in the mesh. Also spent some time
    trying to find the problem of the assert: \`(uniform), function
    GPU\_batch\_uniform\_4fv,\` it was present in
    [rBc8a62e4f](https://projects.blender.org/blender/blender/commit/c8a62e4f50290f560bc09160)
    has been fixed before
    [rB065a00ee](https://projects.blender.org/blender/blender/commit/065a00ee3e6).
  - Exporter: Fix UV vertex indices off by one.
    [d68899e99a](https://projects.blender.org/blender/blender/commit/d68899e99a0)
  - Cleanup single use variable, comments, formatting
    [aa9e4b23e5](https://projects.blender.org/blender/blender/commit/aa9e4b23e5a)
  - Responded to some build error and a bug fix help post on Devtalk.

#### July 14

  - Use stoi and stof instead of \>\> for faster parsing.
    [aacb1f4756](https://projects.blender.org/blender/blender/commit/aacb1f4756a).
    Time taken in string to float conversion is less now as compared to
    \`\>\>\` operator. But the cost is in splitting the string and
    allocations made for several substrings. Overall this is faster. ![
    OBJ import the same file as yesterday using \`stoi\`. cube subd 8;
    23.3MB file.](../../../images/OBJ_file_import_using_stoi.png
    " OBJ import the same file as yesterday using \`stoi\`. cube subd 8; 23.3MB file.")
  - Parse UV coordinates and normals.
    [5e9196ed11](https://projects.blender.org/blender/blender/commit/5e9196ed11e)
  - Directly create Mesh without using intermediate BMesh
    [ec04edfe5c](https://projects.blender.org/blender/blender/commit/ec04edfe5c3).
    It lowered the peak memory usage by half. Also made mesh creation
    faster.
  - Refactor: Keep mesh\_from\_raw\_object in a class
    [3cfcf37cea](https://projects.blender.org/blender/blender/commit/3cfcf37ceaf)

#### July 13

  - Refactor: Keep mesh\_from\_raw\_object in a class
    [rB3cfcf37c](https://projects.blender.org/blender/blender/commit/3cfcf37ceaf)
  - Rename files: exporter -\> ex, importer -\> im.
    [rB582bf439](https://projects.blender.org/blender/blender/commit/582bf4397cf).
    It will be reverted and files moved into folders though.
  - Break code into mesh, objects, and reader files.
    [rB7582bbd5](https://projects.blender.org/blender/blender/commit/7582bbd5740)

### Week 6

Weekly Summary:
[User:Ankitm/GSoC\_2020\_Weekly\_Reports\#Week6:\_July\_6\_-\_10](User:Ankitm/GSoC_2020_Weekly_Reports#Week6:_July_6_-_10)

#### July 8, 9, 10

  - Parsed and stored the file contents in a temporary, OBJ-local
    \`OBJRawObject\` structure. It is not the Blender object, rather an
    OBJ object with the data from the OBJ file.
  - Successfully created a mesh in the viewport and added it to an
    Blender object. Also added the new Blender object to a collection in
    viewport.
      - So far the mesh has vertex data only. Texture coordinates and
        normals are not there yet.
  - Initial profiling shows \~70% time being spent in parsing and
    storing the data in \`OBJRawObject\`. Rest \~30% is spent in making
    a Blender object from a Raw one. So there's a huge room for
    optimization. See attached image.

![Imported a level 8 subdivided cube back using new C++ importer. OBJ
File size: 23.3 MB. Corresponds to the commit
[rBf8d64b39](https://projects.blender.org/blender/blender/commit/f8d64b396dff)](../../../images/OBJ_C%2B%2B_Importer_preliminary_import_profile._.png
"Imported a level 8 subdivided cube back using new C++ importer. OBJ File size: 23.3 MB. Corresponds to the commit rBf8d64b39")

  - Also set up clang-11 and its leak detector since clang-10 in Xcode
    and in `/Library/Developer/Command Line Tools` doesn't have the lsan
    library. Needed to edit and enable @rpath.

#### July 7

  - Started working on the importer. UI is already there, worked on file
    IO and looked at Jacques Lucke's code for the same.
    <https://github.com/JacquesLucke/blender/compare/master>...obj-import-experiments

#### July 6

  - Set up clang-tidy on macOS. Had to build llvm from source, and
    remove extra executables from PATH to avoid conflicts with other
    compilers.
  - Got carried away a little bit and experimented a little bit to get
    clang-tidy to work with Xcode, using a compilation database (JSON)
    with clang-tidy etc. Finally settled with using Ninja as the
    generator in a new folder, solely for checking clang-tidy warnings.

### Week 5

Weekly Summary:
[User:Ankitm/GSoC\_2020\_Weekly\_Reports\#Week\_5:\_June\_29\_-\_July\_4](User:Ankitm/GSoC_2020_Weekly_Reports#Week_5:_June_29_-_July_4)

#### July 3

  - Lots of tiny changes:
    [rBc2eb16f6](https://projects.blender.org/blender/blender/commit/c2eb16f66249)
      - Adhering to recently updated style guide
        [Style\_Guide/C\_Cpp\#C.2B.2B\_Namespaces](../../../Style_Guide/C_Cpp.md#C.2B.2B_Namespaces).
        Specifically, \`\_\` in the end of private class members,
        combined namespace, reorder class members, use \`const\`
        wherever suitable.
      - Remove documentation comments from headers and keep only at
        implementation. Reduces duplication, and reading a class easier.
      - Break functions doing different things into smaller ones.
      - Better comments.

(It was like a code quality day for me.)

#### July 2

  - Fix [\#78533](http://developer.blender.org/T78533): Skip writing
    image texture in MTL (\`map\_Kd\` ) if image filepath is empty.
  - Support exporting smooth groups for smooth shaded meshes
    [rBd85c2620](https://projects.blender.org/blender/blender/commit/d85c2620b81).
  - Worked on feedback received by mentors for evaluation. [Google
    Doc](https://docs.google.com/document/d/1rWd1nfEuiSRS_8ELvRT4ENtIBXqs2DhxSbW_OYwJRzk/edit#).
    Since the comments were on commits not posted for review, and I
    don't receive comments on my branch commits, the doc was made.

#### July 1

  - Support vertex group to which a face element belongs.
    [rB012175f8](https://projects.blender.org/blender/blender/commit/012175f8437).
    It took some followup commits to make it working properly.

#### June 30

  - Build error: Replace nodes\_with\_idname with nodes\_by\_type
    [71c6d384c1](https://projects.blender.org/blender/blender/commit/71c6d384c1d),
    namespace BKE \> bke
    [e3e5ae5da8](https://projects.blender.org/blender/blender/commit/e3e5ae5da8f)
  - Cleanup: simplified conditionals, remove warnings on linux
    [2585882973](https://projects.blender.org/blender/blender/commit/2585882973d)
  - Use unique\_ptr to avoid early destruction of new meshes.
    [9279377f4b](https://projects.blender.org/blender/blender/commit/9279377f4b2)
  - Remove axis transform defines and use existing enums
    [c2e43ca99a](https://projects.blender.org/blender/blender/commit/c2e43ca99a2)
  - Working on exporting vertex groups.

#### June 29

  - Null checks
      - Check if there is any material to write.
        [rB35dc587f](https://projects.blender.org/blender/blender/commit/35dc587fd6f)
      - no image is present in the texture node
        [rB46939815](https://projects.blender.org/blender/blender/commit/469398155d4)
      - uv vertices with no UV map.
        [rB02fa1f0b](https://projects.blender.org/blender/blender/commit/02fa1f0bba2)

### Week 4

Weekly Summary:
[User:Ankitm/GSoC\_2020\_Weekly\_Reports\#Week\_4:\_June\_22-27](User:Ankitm/GSoC_2020_Weekly_Reports#Week_4:_June_22-27)

#### June 25

  - Refactored to separate \`OBJMesh\` and \`OBJNurbs\` classes in
    separate files.
    [rB6c98925d](https://projects.blender.org/blender/blender/commit/6c98925d5ad82238c8cd28d20f3)
  - Trying to keep MTL writer in C++ itself: wrote object's material
    name and material library filename in OBJ file, create MTL file.
      - Need to extract required properties from the Principled-BSDF
        node. If it is not present, default hardcoded values are being
        written.

#### June 24

  - Clean up OBJNurbs to be consistent with OBJMesh. Also, Nurbs
    surfaces are exported after being converted to mesh. Nurbs curves
    are exported in form of control points if "export curves as nurbs"
    is checked.
    [rB31d48017](https://projects.blender.org/blender/blender/commit/31d480174a8caebeeacac14902b02a)
  - Fixed a bug: duplicated UV vertices and indices.
    [rBef0eff0b](https://projects.blender.org/blender/blender/commit/ef0eff0b8a42bac8515b)
  - To reuse the MTL writer in
    [export\_obj.py](https://developer.blender.org/diffusion/BA/browse/master/io_scene_obj/export_obj.py$49)
    : Testing with registering an operator in Python and calling it from
    C++, and also sending some strings to be operator upon.

#### June 23

  - Update [D8089](http://developer.blender.org/D8089) in response to
    the review. Goal was to move more variables to private access in
    OBJMesh & OBJWriter and expose functions only.
  - Finished Nurbs support. Now just need to refactor it like OBJMesh.

#### June 22

  - Cleaned up and put [D8089](http://developer.blender.org/D8089) up
    for review.
  - Worked on exporting curves as nurbs.
  - Replaced \`std::vector\` with \`blender::Vector\` proposed in
    [D7931](http://developer.blender.org/D7931)

### Week 3

Weekly Summary:
[User:Ankitm/GSoC\_2020\_Weekly\_Reports\#Week\_3:\_June\_15-19](User:Ankitm/GSoC_2020_Weekly_Reports#Week_3:_June_15-19)

#### June 19

  - Worked on exporting curves as meshes.
  - Started working on refactor to reduce memory allocation and copying
    the mesh data into it. Rather at the time of writing, relevant info
    should be obtained on-the-fly.

#### June 18

  - Committed mesh triangulation feature.
    [rBc3f8e955](https://projects.blender.org/blender/blender/commit/c3f8e9550c1e)
  - Renamed occurrences of \`objects\` with \`ob\_mesh\` or similar to
    indicate that mesh objects are being handled. It is necessary to
    differentiate it from curve objects.
  - Made changes in response to the review in
    [D7959](http://developer.blender.org/D7959)

#### June 17

  - Made writing UV coordinates optional (default true). By extension,
    the faces are now written as \`f v1//vn1 ...\`
  - Face normals are optional too (default true). Faces are written as
    \`f v1/vt1 ...\`
    [rB41f47cd8](https://projects.blender.org/blender/blender/commit/41f47cd8d6a4699bafa4f6a)
  - Added scaling factor. (default 1.000)
    [rBd47318fb](https://projects.blender.org/blender/blender/commit/d47318fb4537277e3013b362f8)
  - Worked on triangulation of meshes. Need to fix a memory leak though.

#### June 16

  - Slow day today. Worked on axes transform (up/forward) and exposing
    it in the exporter preferences UI.

#### June 15

  - Allowed all frames to be exported, instead of 0-1000. Added
    animation checkbox to make single frame export intuitive.
  - Destination filename is edited only when multiple frames are being
    exported.
    [rB45461012](https://projects.blender.org/blender/blender/commit/454610123787a7b)
  - Found and installed
    [XcodeClangFormat](https://github.com/mapbox/XcodeClangFormat) for
    easy formatting inside the IDE itself. (Off-topic: Plugins were
    deprecated in Xcode 8 and extensions are now the way to go.)

### Week 2

Weekly Summary:
[User:Ankitm/GSoC\_2020\_Weekly\_Reports\#Week\_2:\_June\_8-13](User:Ankitm/GSoC_2020_Weekly_Reports#Week_2:_June_8-13)

#### June 12

  - Finished work on multiple objects to be exported in the same file.
  - Need to test its accuracy. Will commit tomorrow.

#### June 11

  - Not a lot of work today unfortunately.
  - Tested [{fmt::fprintf}](https://fmt.dev) library as compared to
    \`fprintf\`. Saw only 10-15% speed up.
  - Pushed work upto UV coordinates.
    [rBf01a7d50](https://projects.blender.org/blender/blender/commit/f01a7d508d).
  - Fixed a out of bounds crash on windows in
    [rBc2fbfb42](https://projects.blender.org/blender/blender/commit/c2fbfb421ba).

#### June 10

  - Finished exporting non-duplicate and exhaustive list of UV
    coordinates and also stored their correct indices in \`Polygon\`s.
  - Tested it by exporting with C++ exporter and importing it back with
    python importer and comparing the texture map with that of original
    object.
  - Minor review update in [D7959](http://developer.blender.org/D7959).

Todo

  - Export multiple objects in the same file, multiple frames (different
    files).
  - Axes transform.

#### June 9

  - My initial attempt at UV coords was wrong. The list was incomplete,
    since it was not iterating over the whole \`me\_eval-\>mloopuv\`
    list; there were duplicates since a mesh vertex can be in multiple
    places in the texture map.
  - Asked about it in chat and found
    \`BKE\_mesh\_uv\_vert\_map\_create\`. Took me a lot of time to adapt
    it to my requirements.
  - Ended the day at merely listing non-duplicated list of vertices.

Todo Next day

  - Now that we have a list of all UV vertex coordinates, we need to
    match every 3D vertex of all polygons with that list. So todo is to
    add correct indices, \`vt1\`, \`vt2\`.. and store it in every
    \`Polygon\` member to export it like:

\`vt1 u v\`  
\`vt2 u v\`  
\`f \*/vt1/\* \*/vt2/\*\`  

#### June 8

  - Updated soc-2020-io-performance to improve readability inside loops
    using iteration variables. Minor namespace cleanup.
    [7af1ec516f](https://projects.blender.org/blender/blender/commit/7af1ec516f94)
    and
    [86845ea907](https://projects.blender.org/blender/blender/commit/86845ea907f8)
  - Merged those changes in review branch & created
    [D7959](http://developer.blender.org/D7959).
  - Searched for averaging function, to replace
    \`face\_normal\_axis\_component\`, but only found some functions in
    cycles utilities. However calculating face normals using vertex
    position can be done using cross product of two edges. That function
    is there. Downside is wasted memory in
    \`data\_to\_export-\>mvert\[i\].no\`.
  - Worked on UV coordinates. Testing for correctness at the end of the
    day.

### Week 1

Weekly summary:
[User:Ankitm/GSoC\_2020\_Weekly\_Reports\#Week\_1:\_June\_1\_to\_June\_6](User:Ankitm/GSoC_2020_Weekly_Reports#Week_1:_June_1_to_June_6)

#### June 5

  - Correctly transformed vertex normals according to object's
    transformation matrix. Still need to incorporate axes transformation
    matrix.
  - Also calculated face normal from its vertices' normals inlined while
    writing them to the file. It's a dirty averaging function.
      - This removed the need of \`BLI::Vector<uint>
        face\_normal\_index;\`. Since \`vn\` and \`f\` lines are written
        by looping over the same \`polygon\`s, their indices would be
        the same.
      - This also ensures that the memory allocated in \`MVert \*mvert\`
        is used better. We were discarding all
        \`data\_to\_export-\>mvert\[i\].no\` arrays. Now we don't.
        They're used to calculate face normals on the fly.
      - It'll be more clear when I'll send it for review.
  - Added object's name in the file.
  - Encountered non-freed memory while testing both of these exporters^.
    Since that has been fixed in master, most probably with the commits
    between 25th May & 28th May, I'll merge master once.
  - Did a comparison between the old & new exporters. Release
    configuration, lite build + opensubdiv, default cube with 6+3
    subsurf. See images:
    [F8586987](https://developer.blender.org/F8586987)
    [F8586984](https://developer.blender.org/F8586984)

In the table below, we're checking the time taken by
\`wm\_handler\_fileselect\_do\` function. Release build.

<table>
<thead>
<tr class="header">
<th><p>Object</p></th>
<th><p>File Size (note that `vt` are not there in C++ yet so C++ file size is smaller.)<br />
(C++/ Python + MTL)</p></th>
<th><p>Export Time (`fstream`/`fprintf`)<br />
(s)</p></th>
<th><p>Export time Python<br />
(s)</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Default cube + 8 subdivision surface levels</p></td>
<td><p>48 MB / 63 MB</p></td>
<td><p>2.4 / 1.47 s</p></td>
<td><p>20 s</p></td>
</tr>
<tr class="even">
<td><p>Default cube + 9 subdivision surface levels</p></td>
<td><p>186 MB / 262 MB + 229 bytes MTL</p></td>
<td><p>7.6 / 4.6 s</p></td>
<td><p>82 s</p></td>
</tr>
<tr class="odd">
<td><p>Ico-sphere + 8 subdivision surface levels</p></td>
<td><p>478 MB / 674 MB + 124 bytes MTL</p></td>
<td><p>22 / 13 s</p></td>
<td><p>210 s</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

Todo on Weekend:

  - Texture coordinates.
  - Try to add multiple objects. An object can be a made a class
    instance with \`data\_to\_export\` its struct. But we'll discuss.

#### June 4

  - Review update on [D7918](http://developer.blender.org/D7918) and
    [D7917](http://developer.blender.org/D7917):
      - Renamed obj to wavefront\_obj in filenames to remove confusion
        with blender's data types. Also renamed C++ headers to \`.hh\`.
        moved the code to \`wavefront\_obj/intern/\` leaving only
        \`wavefront\_obj/IO\_wavefront\_obj.h\` externally.
      - Improved comments in structs & functions.
      - arc diff wasn't the best tool for getting two sequential commits
        reviewed. So there was some branch juggling, diff files creation
        & resets. Now onwards, I'll stick to getting one change reviewed
        at a time → Merge it to soc branch & repeat.
  - Got familiar with [Xcode's Address
    sanitizer](https://help.apple.com/xcode/mac/current/#/devcef23c572).
    (also fixed a small heap buffer overflow).
  - Tomorrow: I'll be working on transforming vertex normals,
    \_obtaining\_ face normals from vertex normals, write object name to
    the file. \_Writing\_ face normals to file is a on halt waiting for
    [D7931](http://developer.blender.org/D7931)

#### June 3

  - Finished major changes in \`io/obj/\*.cc\` files.
  - Uploaded [D7918](http://developer.blender.org/D7918) and
    [D7917](http://developer.blender.org/D7917) which will be committed
    to the soc-2020-io-performance branch after review.
  - Experimenting with "multiplying obmat and mvert.co while writing to
    the file" vs "multiplying earlier & keeping its copies" as per
    Howard's comment
    [D7917\#191581](https://developer.blender.org/D7917#191581). But
    first I need to find appropriate method to transform normals. After
    data structure for polygons is finalised, I'd be able to fix its
    issue with wrong vertex\_index.
  - Also updated [project
    plan](User:Ankitm/GSoC_2020_Proposal_IO_Perf#Project_Schedule)
    to give more time and spread to such refactoring and bug fixing.
    Squeezing PLY and STL.

#### June 2

  - Received review from mentors on the commit
    [rB485cc433](https://projects.blender.org/blender/blender/commit/485cc4330aff5f38c8af7874).
  - Worked on resolving the straightforward bits, renaming files (.h →
    .hh), writing comments & proper naming.
  - Will finish the changes on the next day for review before committing
    it. That involves finding how to transform face normals according to
    global transform, using \`BLI\_\*\` instead of STL, breaking up
    functions into smaller, specific ones.

#### June 1

  - Cleaned and prepared a commit for the work done in the past 4 days.
    See
    [rB485cc433](https://projects.blender.org/blender/blender/commit/485cc4330aff5f38c8af7874).
  - The UI blocks and dummy files were added in [week
    0](User:Ankitm/GSoC_2020_Weekly_Reports). After that, OBJ
    Exporter now exports a single, selected object's, vertices, vertex
    normals, and faces. Need to verify it with complex geometries. The
    axes transformation (\`Z\_up\`, \`Y\_forward\`) is not there yet.
  - Tried to get ASan to work on macOS, but failed. It's not supported,
    see [the
    docs](http://clang.llvm.org/docs/AddressSanitizer.html#supported-platforms).
    gperftools' heap-profiler & heap-checker work on simple standalone
    scripts, not in Blender. Instruments.app's [leak
    detector](https://help.apple.com/instruments/mac/current/#/dev022f987b)
    crashes after detecting a leak that happens without even doing
    anything.
