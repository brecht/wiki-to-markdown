### gperftools installation and basic usage

Broader todo task for profiling Blender on Phabricator:
[\#70016](http://developer.blender.org/T70016)

[Instruments.app](https://help.apple.com/instruments/) is bundled with
Xcode & provides easy CPU profiling. But it isn't available for other
platform. \`perf\` based tools (for e.g. Hotspot, flamegraph, see [see
usage](https://wiki.blender.org/wiki/User:Jbakker/projects/FasterAnimationPlayback/Spring_Scene_Analysis#Metrics))
will work only on Linux. This won't work on windows but at least other
two platforms can reproduce consistent results. Plus highly localised
functions can be tested using gperftools.

<hr>

#### 27th May

gperftools finally worked. Here's the recipe: \[1\]

  - `brew install gpertools`

<!-- end list -->

  - `brew install graphviz`

<!-- end list -->

  - Insert this line in the platform specific file among [these
    files](https://developer.blender.org/diffusion/B/browse/master/build_files/cmake/platform/)
    on diffusion. I used platform\_apple.cmake.  
    Use `pkg-config --libs libprofiler` to find out the path to the
    library & the flag.
    ``` cmake
    set(PLATFORM_LINKFLAGS "${PLATFORM_LINKFLAGS} -L/usr/local/Cellar/gperftools/2.7/lib -lprofiler")
    ```

<!-- end list -->

  - Use this in a C/C++ file around the function to profile.

<!-- end list -->

``` cpp
#include </usr/local/include/gperftools/profiler.h>  
ProfilerStart("/tmp/data.prof");
int a = 47;
for (long int i = 0; i < __INT32_MAX__; i++) {
 a = a*a;
 a = a%178948;
} 
ProfilerStop();
```

  - Build, run, execute the function. Verify that the console has
    similar in the output:  
        PROFILE: interrupts/evictions/bytes = 3532/1448/174440 

<!-- end list -->

  - Then run this to see an svg in your web browser. For text, instead
    of `--web`, use `--text`.  
    `pprof --web /path/to/Blender.app/Contents/MacOS/Blender
    /tmp/data.prof`

#### \[1\]:

You can build it from source too. Download the source code from
<https://github.com/gperftools/gperftools/releases>

`cd gperftools`

`./configure` OR `./configure --prefix=/path/to/destination`

`make`

`make install`

Then separately install graphviz. I cannot comment on that since I
didn't build it.

#### References:

  - <https://gperftools.github.io/gperftools/cpuprofile.html>
  - <https://github.com/google/pprof>
  - <http://www.graphviz.org>
  - <https://wiki.geany.org/howtos/profiling/gperftools>
  - <http://engineering.appfolio.com/appfolio-engineering/2016/6/22/using-gperftools-on-the-mri-ruby-interpreter-in-mac-os-x>
  - <https://developer.ridgerun.com/wiki/index.php?title=Profiling_with_GPerfTools>
  - <https://stackoverflow.com/questions/10874308/how-to-use-google-perf-tools>
