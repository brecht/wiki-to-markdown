  - [ Proposal](/Proposal_IO_Perf)
  - [ Daily](/Daily_Reports) Reports, [
    Weekly](/Weekly_Reports) Reports, Project Status Tracker
    & design discussions: [\#68936](http://developer.blender.org/T68936)
  - IO Benchmarks
      - [ gperftools installation and
        use](/gperftools_installation_on_mac)
      - [Comparisons](/Final_report#Comparisons): Currently
        links to final report's comparisons section. Older timings can
        be found in Weekly Reports too.
  - [Devtalk
    thread](https://devtalk.blender.org/t/gsoc-2020-faster-io-for-obj-stl-ply/13528)for
    community feedback & testing
  - Branch on diffusion:
    [soc-2020-io-performance](https://developer.blender.org/diffusion/B/browse/soc-2020-io-performance/source/blender/io/wavefront_obj/)
  - [Final Report](/Final_report)
