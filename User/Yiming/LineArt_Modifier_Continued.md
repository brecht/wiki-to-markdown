# Line Art Modifier Continued

Development log for Line Art modifier...

### 0329 - 0404

  - Updated fading control and fixed curve ineffective bug in GPencil
    opacity/thickness modifiers.

### 0405 - 0411

  - Separate memories for line art run time structure and result data.
      - <s>Use \`PointCache\` structure to do line art caching control
        for each frame.</s> Disregarded, \`PointCache\` was not intended
        to work that way.

I made the boeing 727 file for demonstrating line art capability over
the span of this week. Now on [Blender
Cloud](https://cloud.blender.org/p/gallery/608da4565f9fe397b1944ba2).

### 0412 - 0418

  - New threaded loading code.
      - Better load spreading on loading code so threads handles similar
        amount of geometries.
      - Performance benchmark shows \~50% performance increase when
        loading the geometry.
  - Line Art now accepts multiple edge types for one edge.
  - Better tiling strategy for ortho and perspective camera. Different
    treatment for different camera types.
      - \~5-10% performance increase overall.
  - UI fixes for master.
  - Manual updates according to those changes.

### 0419 - 0425

  - Use array instead of list (as suggested by Falk David) for tile
    linked triangle list.
      - \~40% Performance increase in tested file in Release build.
        Depending on the distribution of the geometry, performance may
        vary but will be substantially faster.
  - Cache for modifier stack. Now line art runs only once for each
    modifier stack.
  - Manual updates.
  - Tried threading current intersection detection method but failed :/.

### 0426 - 0502

  - Added a \`use\_cache\` switch so line art is more flexible in
    situations like filtering different vertex groups, using different
    chaining settings and so on.
      - \[TODO\] Should use a better code path where the ones that
        "doesn't use cache" will calculate less because there will be no
        source outside specified be shown.
  - Stroke offset function, this allows line art to be used with
    hand-drawn gpencil objects and produce correct occlusion using
    viewport depth instead of shown in front.
  - Fading patch in master now.
  - Still manual updates and various other tweaks.
  - Made some icons [here](https://developer.blender.org/T87814).

Bug Fixes:

  - Fix incorrect fov when sensor fit is not Auto. (in master)

I went back to Chengdu for 3 days during Labour Day holiday for some
family matters.

### 0503 - 0509

  - Occlusion effectiveness function: Specify materials to have 0-3
    levels of equivalent occlusion. Suitable for complex structures
    where you want to manually control the density of lines.
  - Floating edge type can be selected separately.
      - <s>Clipping has wrong behavior.</s> Fixed.
  - Added a anti-jaggy algorithm post chaining, so this allows "angle
    split" function to work much better because there will now be no zig
    zag chaining paths.
  - Basic intersection filtering.

### 0510 - 0516

Mainly small fixes this week. Preparing patches for merging into new
version of Blender.

  - Better chaining handling for floating edges where those edges were
    mainly for rendering small details that you might not want chain
    them together.
      - This and duplicated edge type can be toggled now.
  - Use array also for tile linked edges. With some performance boost.
  - Can now use face mark to filter edges.

### 0517 - 0523

Mainly preparing the patches this week.

  - Experimented with BVH for line art intersection calculation, current
    performance on par with legacy algorithm, but can be easily
    threaded. So there will be more improvements.

Bug Fixes:

  - Fix T88464: Incorrect baking with camera markers.

### 0524 - 0530

(In progress)

  - Naming conventions are fixed and merged.
  - List optimization merged.
  - \`use\_lights\` option in adding GPencil object.

### 0531 - 0606

  - Allow custom camera to be selected as line art rendering source.
    This allows baking multiple camera views at once.
      - In stroke composition, an option to move strokes towards active
        camera or the selected one.
  - Better tolerance for faces that are perpendicular to view.
  - Fixed edge clipping index error.
  - Camera overscan (fixed value).
      - Discard edges that are out of frame. Need a frustum check to be
        even faster (don't even load objects that are outside view).

### 0607 - 0613

  - Adjustable camera overscan.
      - Also fixed an old bug where shifting would cause line art
        internal FOV to change. Now shifting works properly also with
        overscan.
  - Move BMesh conversion into loader worker thread so the loading is
    even faster.
  - Frustum culling now working.
  - <s>Added custom vector (3f) in \`bGPDspoint\`, allows storage of
    line art normal in GPencil points for later manipulation.</s> We
    then decided to go the custom data route so this is discarded.
  - Light based contour.

### 0614 - 0620

  - Cache patch and UI adjustment (now in master).
  - Made all remaining patches that relies on
    Cache/Threaded-loading/Line-type up to date with latest master here:
    <https://developer.blender.org/T87739>
  - BVH based triangle intersection test is running correctly now,
    benchmarked to be a little bit faster in my test files that my
    legacy algorithm.
      - The math portion could be further sped up as my algo has a
        "fuzzier" tolerance than the one included in \`BLI\_math.h\` and
        I return early in a lot of cases. So this needs some testing.
  - Fixed override limits to only include modifiers that's using cache
    instead of setting the value "globally".
  - Cleaned up remaining local branches.

### 0621 - 0627

  - While waiting for next bunch of reviews, trying to get Sebastian's
    smooth contour mesh modifier working...
      - Tried, \`BMesh\` seem to produce some assertion errors upon some
        normals that has "not correct length". And when it doesn't
        produce any errors, it doesn't produce calculated result :(
  - More patches went into master so the backlog is getting smaller.
  - Fixed edge type overlapping issue where sometimes edge mark doesn't
    show up with crease priority being too high and errors in early
    return.
  - Mesh smooth shading and mark sharp support.
  - Fix light contour default behaviour.
  - Hide collection tab when scene master collection is active.
  - Length modifier extension now follows curvature (curvature sample
    method is still being discussed).

### 0628 - 0704

  - Fix UI stuff.
      - Also made intersection/material mask bits into 8 bits for more
        flexibility and simplicity in flag transferring.
      - Change occlusion effectiveness into "material mask" name for
        better clarity.
      - Rename "floating"-\>"loose".
  - Merged intersection filtering code into master.
  - Back face culling option now available, automatically turned on when
    max occlusion level == 0 (only visible faces).
  - Threaded intersection calculation, preliminary \~50% increase of
    speed in intersection stage.

### 0705 - 0711

  - Fix culled triangle material mask assignment.
  - Various UI fixes.
  - Fix ineffective modifier apply caused by wrong flag in cache.
  - Branch \`lineart-shadow\`:
      - Started coding shadow support. Basic code path is laid out.
  - GPencil:
      - Started working on dash modifier.
        (https://developer.blender.org/D11876)

### 0712 - 0718

  - Discussed about line art UI clean ups, some back and forth an it's
    committed now. (https://developer.blender.org/D11839)
  - Branch \`lineart-shadow\`:
      - Able to run basic shadow now, parallel and perspective light
        projection working correctly in terms of geometry.
      - Edge cutting flag fixes, now projection cuts correctly in terms
        of visual result. May have some zero-length duplicated cut
        somewhere uncaught, fixed chaining algorithm to discard zero
        length segments.
  - GPencil:
      - Dash modifier back and forth. Mainly changed it to list instead
        of a weird looking table. Some bug fixes.

### 0719 - 0725

  - Branch \`lineart-shadow\`:
      - Div by 0 error caught, now no flying lines is visible. (Need to
        check if projection onto intersected triangles produce correct
        result)
      - Because the shadow is projected **on to** triangles so the
        traditional occlusion algorithm needs to know if it's on the
        front or the back face of said triangle or it will not calculate
        correct occlusion result from it. Because lack of edge-triangle
        pair reference across two runs of line art, it's gonna take some
        time to do this properly. Now the code for determining
        front/back is added, only need to have that pair reference info
        to be available.
      - Shadow branch has higher numeric stability in terms of
        triangle-edge relationship algorithm, maybe I'll modify the
        legacy code and make line art own occlusion stage more robust as
        well.
  - GPencil:
      - Dash modifier animation problem solved. The patch is Almost
        okay, waiting for approval.
        (https://developer.blender.org/D11876)
      - Further discussion about length modifier curve behaviour with
        Sebastian on the transformation method and end-point drifting
        problem, added inverted curve and curvature fading option.
        (https://developer.blender.org/D11668)
      - Researching a probable curve modifier for gpencil.

### 0726 - 0801

  - Line Art main stuff:
      - Occlusion accuracy fix for a rare condition where the edge
        cutting function missed the starting point by a small epsilon
        ([675d8a9c](https://projects.blender.org/blender/blender/commit/675d8a9c)).
      - Support bounding box check in shifted camera lens. Overscan
        function now in master
        ([8e9d06f5](https://projects.blender.org/blender/blender/commit/8e9d06f5)).
      - Fixed material flag occlusion level error.
      - Child object visibility respect parent collection.
      - Mesh auto smooth supported to detect crease lines. Crease value
        priority is object crease \> auto smooth crease \> line art
        default crease.

<!-- end list -->

  - Branch \`lineart-shadow\`:
      - Support detection of occlusion when shadow is projected on one
        side of a single sided plane, which means the plane will choose
        to occlude the edge projected on them dynamically based on which
        side the camera is viewing at.
      - Shadow edge matching support for check edges between two runs of
        line art. Note: not exact match, in theory there might be
        errors, but a more prefect solution may need to rethink the
        2-run strategy and data structure.

### 0802 - 0808

  - Line Art:
      - Prevent depsgraph warning when camera is \`NULL\`.
      - Trimming edges at image boundary so it makes it easier to stitch
        multiple shots together.
      - Researching methods for accelerated triangle processing method,
        for putting them into subdiv tiles and intersection stage. (With
        Sebastian, wip patch and discussion on performance can be seen
        here: <https://developer.blender.org/D12082>)

<!-- end list -->

  - Branch \`lineart-shadow\`:
      - UI updates for clarity.

<!-- end list -->

  - GPencil:
      - Dash modifier icon added (proposal):
        <https://developer.blender.org/D11876>

### 0809 - 0815

  - Line Art:
      - Sort/merge method for triangle tile splitting is done, now
        implementing ways to make use of this structure for accelerated
        intersection calculation. Problems including the potential use
        of various locks for generating triangle pair lists for
        intersection stage.

I went back home to Chengdu to stay with my parents for a few weeks.

### 0816 - 0822

  - Line Art:
      - Used tolerance value in \`point\_on\_edge()\` function to make
        occlusion cutting function more robust
        ([a12bc92d](https://projects.blender.org/blender/blender/commit/a12bc92d)).
        Together with
        [675d8a9c](https://projects.blender.org/blender/blender/commit/675d8a9c),
        it should give much more stable result in situations like edge
        split and shadow-on-surface.
      - Fix T90695: Lower tile splitting limit for lineart
        ([f41beca9](https://projects.blender.org/blender/blender/commit/f41beca9)).

<!-- end list -->

  - GPencil:
      - Fixing length curvature modifier, much thanks to the help from
        Henrik Dick (weasel). Now the curvature is correct and screw is
        working nicely. Needs testing.

### 0823 - 0829

  - Line Art:
      - Fix unexpected crashes when loading objects (Fix mesh freeing
        call for obi-\>original\_me so the address is correct).
      - Better stroke offset behaviour including clamping at the camera
        near plane and reversed offset being possible.

<!-- end list -->

  - GPencil:
      - More grinding on the length curvature modifier and dash
        modifier. Modified my implementation to include a skew on/off
        option (But eventually not used)

### 0830 - 0905

  - Line Art:
      - Again working on threaded intersection stage, now put triangle
        pair building in threads, still not very much improvement, the
        reason could be in this method the triangle list is iterated
        twice and repeatedly being queried, so it's not as efficient in
        the original method where there's only 1 time iteration and no
        more seeking. Performance and descriptions are recorded here:
        <https://developer.blender.org/D12082> (Talked with Sebastian
        and we shall look at the performance problem later on). Line art
        branch itself still works with spatial locking method and it's
        currently the fastest.
      - Restored in-front on by default, which gives much better look
        when using them on most cases.

<!-- end list -->

  - GPencil:
      - Try fix the new algorithm for curvature length modifier to
        handle the situation when there are only 2 points in the stroke
        (Weasel handled the final fix later because I'm not quite
        familiar with that algorithm).
      - Cleaning up dash modifier.

### 0906 - 0912

  - Line Art:
      - Better smooth tolerance algorithm for handling ending "hook"
        case where a few segments goes backwards and form a small hook,
        which leads to problems with chain continuity in some cases
        (D12050).
      - Clean up various patches including overscan trimming, stroke
        offset, custom camera, auto smooth crease and gp curvature stuff
        for review.

<!-- end list -->

  - Branch \`lineart-shadow\`:
      - Use the vector-math variant of intersection function instead of
        point-and-slope for some steps in the occlusion stage, fixed
        some problems in ortho camera but it's still having some
        problems (e.g. when camera is perspective but light is ortho/sun
        and vise versa when they don't really align).
      - Auto back face culling fixed when shadow is on.

I'm back in school with my normal working machine again :D

### 0913 - 0919

  - Line Art:
      - Show stroke offset when adding line art object for clarity so it
        should be no problem with in-front on by default.

<!-- end list -->

  - GPencil:
      - Fix T91421: Length modifier bake missing influence check.
      - Dot dash modifier is in master now.
      - Fix dash modifier reading error caused by the change of segment
        list into array.
      - Some more back and forth involving modifiers......

<!-- end list -->

  - Misc:
      - Make Sebastian's smooth contour modifier working with latest
        master again (D12562), ui needs more work(although it doesn't
        seem to need much more or even different options for the mesh?),
        smooth options on by default.

### 0920 - 0926

  - Line Art:
      - Again lean up patches for trimming, stroke offset and custom
        camera (Basically awaits approval).

<!-- end list -->

  - GPencil:
      - Curvature length modifier is in master now (phew\~).
