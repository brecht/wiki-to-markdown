# User document for LANPR

This is for the 2019 version of LANPR. The user interface as well as the
functionality has changed a lot since last year’s GSoC. Please refer to
this document for clarity. LANPR document will reasonably be included
into Blender’s main document as well, if it got merged. This page serves
a draft befor the review and merge.

## Basic concepts

LANPR is a feature line render engine that is useful for producing
artistic or industrial styled line renderings of a 3D scene.

LANPR can be used as a standalone engine, which renders the scene
directly, or as a GPencil feature line generator for editing/rendering
in other engines as well.

Feature line rendering is useful for making comics, stylized animations,
doing structural visualizations, and many other production fields as
well. LANPR is much faster than Freestyle, and it’s more accurate and
versatile than extracting edges using image filtering methods. The SVG
workflow in LANPR and GPencil is another addition to potential external
usages.

## LANPR components

Currently LANPR can be treated as several major components:

  - **Computation**: This is for generating feature lines and their
    occlusion info.
  - **Rendering**: This is for rendering the line results from
    computation.
  - **“Feature Line” modifier**: This is for customizing objects and
    line types that are included into LANPR computation.
  - **GPencil converter**: This is for generating GPencil Strokes from
    LANPR to be later used in other engines.
  - **SVG converter**: Directly export SVG path from LANPR cache.

Although there are many modules, they are mostly for achieving different
targets, and a basic set up won’t require too much effort.

As we talked about, LANPR can be used as an standalone engine, so let’s
begin with this one.

## LANPR as an engine

This part covers the basics of how to use LANPR engine.

  - [Introduction](engine.md)
  - [CPU and GPU
    modes](modes.md)
  - [Line layer](layer.md)
  - [Parameters](parameters.md)
  - [Effects](effects.md)

## QUICK SETUP for rendering your scene using LANPR

Using LANPR engine:

1.  Select "LANPR" engine.
2.  In LANPR panel, Click "Add line layer" to add 1 default line layer.
    Or you can click "Default" to add 3 default layers for rendering
    inner structures.
3.  In the viewport, Choose rendering preview mode, lines should pop up.
4.  F12 to render using LANPR engine.

Using LANPR as Grease Pencil generator.

1.  In any other engines, enable "LANPR" checkbox.
2.  Add a grease pencil object as the stroke holder.
3.  Select the collection you want to be rendered (in the outline).
4.  Go to Collection LANPR tab, enable the LANPR configuration, choose
    target grease pencil object. Enable all line types.
5.  Manually update in LANPR panel or use auto-update to generate
    strokes. Auto-update now only operate when frame number changes.

## Advanced use of LANPR

  - [GPencil and
    LANPR](gp.md)
  - [Including/excluding content from being
    rendered](selection.md)
  - [SVG exporting](User:Yiming/GSoC2019/LANPR_Document/svg)

LANPR has its own strengths and limitations as well. Please refer to
this document for detailed info:

  - [Other considerations when using
    LANPR](User:Yiming/GSoC2019/LANPR_Document/considerations)
