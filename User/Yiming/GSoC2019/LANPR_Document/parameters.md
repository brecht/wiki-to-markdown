# Parameters

## Main panel

**Crease Threshold** is the global value for detecting crease lines.
This value can be overwritten by per-object and per-material values (yet
to be implemented).

**Crease Fade** is exclusively for GPU mode. It will allow crease lines
to fade within the range of two crease values (Threshold and Fade),
allows better visual effect, especially for sculpted model or any model
who doesn’t have distinctive sharp geometric features.

## Options panel

**Intersection** if enabled, LANPR will calculate intersection lines in
both CPU and GPU mode.

**Chain Lines** if enabled, discrete edges will be chainned into a long
line to allow line taper and other line effects. LANPR will need to
allocate some extra time during calculation. If you only need solid
lines without any fancy styles, this can be disabled.

## Chaining panel

**Geometry Threshold** is for setting the 3D distance below which two
line tips can be chained into one longer line.

**Image Space Threshold** is for setting the projected 2D distance. This
could result in a connection between two tips that are actually far away
in z-depth. Sometimes this is used to smooth bumpy surfaces.

(Note: Geometery-sapce chaining stage take place before image-space
chaining. Setting values to zero disables corresponding stage, but if
both value is set to zero and chaining option is enabled, Image space
chaining will be internally set to a default value of 0.01. Try
different values to check if it is the effect you want.)
