# GPencil and LANPR

## Another way to use LANPR

As you may know already, LANPR can be used with other engines as well.
Currently it works by using GPencil to draw its result.

When LANPR engine is not selected, LANPR will automatically switch
itself into GPencil mode. You will see a checkbox on top of LANPR’s main
panel. When disabled, It doesnt affect your scene at all. When enabled,
you are then able to utilize LANPR results and making it update
automatically as well.

When using LANPR like this, only CPU mode computation is available. This
time, LANPR to GPencil process doesn’t require GPU. So you are able to
batch process this on a headless system.

## How

First, you need to check LANPR checkbox.

To convert LANPR render cache into GPencil strokes, you will need to set
GPencil targets. There are two places for you do do so: One is in the
collection LANPR tab, and another one is in object feature line modifier
configurations (also in a separate tab just under the collection tab).

Let’s start with a simpler way.

## Collection target

First, you need to add a GPencil object as your LANPR container. You may
want to put it at the origin of the world without any transformations.

Then make sure the collection you want to render is active, you can
check in the outliner.

Go to **Collection LANPR** tab, you will see a target object selector.
This sets the target for the active collection. All the lines within
this collection will then be sent into this target. Select the GPencil
object you just created.

More options will pop up. They are quite similar to those configurations
in LANPR line layer. The only difference is that when using GPencil as
LANPR output, the line style is then decided by GPencil material. You
are able to choose which material to use when generating strokes.

When everything is set, go back to LANPR panel and click Update, you
will then able to see the result generated inside the viewport.

## Object target

You can also assign GPencil target for individual mesh objects. In this
way different objects can have different line styles. You can also leave
collection target empty, to only render lines from configured objects.

To make the Object follow your custom configuration, you need to add a
feature line modifier for your object.

Go to **Object LANPR** panel, you can see the detailed configurations
for this feature line modifier (It’s meant to be a special modifier,
lust like physics settings, they are in a separate panel).

In there, you can set target GPencil object and material to be used,
just like in collection.

(Please note that you *can* share one target object across different
sources.)

Go back to LANPR main panel and update. The result will appear.

Currently there can only be one feature line modifier. In the future you
will be able to do multiple.
