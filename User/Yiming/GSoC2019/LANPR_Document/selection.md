# Selecting render content

## Why

In some scenarios, you may want to only render some parts of your scene
as lines. In this case, only selecting line types won’t necessarily
work, and configuring object one by one won’t be very practical.

An example of this is that many animations draw lines on the character
or moving parts of the scene, and the background is typically left with
no lines. Another example of this is in a complex structural
representation, you can select different groups to be rendered one at a
time.

## How

LANPR offers a flag for quick settings of whether include or exclude
collection/objects from being rendered.In both collection and object
LANPR tab, there’s a usage selector there.

For Object LANPR tab, it will be visible only when you select LANPR as
render engine, or enable LANPR to generate Grease Pencil lines.

Three options are available:

  - **Include**: Collection is included into LANPR calculation.
  - **Occlusion Only**: Collection is included into the calculation, but
    no lines will be produced, this collection only serves a role of an
    “occlusion mask”.
  - **Exclude**: Collection is excluded from the calculation, as if it
    doesn’t exist at all.

If not set to " Inherent", object usage flag will override parent
collection’s.

When selecting “Occlusion Only” and “Exclude” in the collection, there’s
an extra “Force” option. By default, when you exclude a collection from
LANPR, the object inside who has a feature line modifier will still be
included (as if they were specially treated). This feature makes it
convenient to toggle a few special object within a large collection of
hidden ones. To ignore this, check the “Force” checkbox in the
collection.

The usage flag is effective in both CPU and GPU modes in LANPR, and is
also effective when using LANPR as a GPencil stroke generator.
