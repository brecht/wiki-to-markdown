# Line layer

## Modifying line layer

When using LANPR as a render engine, you are able to configure multiple
line layers for rendering. Typically different line layers consists of
lines with different occlusion levels, also other customizable
properties.

Line layer configuration is initially empty. At least one line layer is
needed to produce a visible render result. The simplest way is adding
one line layer by clicking the `plus` button, or adding default line
layers by clicking the `default` button.

Using `up/down` button to adjust the overlay order. This is particularly
useful when you have thick lines in multiple levels.

## Occlusion levels

By setting different value in the `level` entry, you can select lines
with different occlusion levels up to 127. Zero means selecting visible
lines. Typically, for structural visualizations, this value won’t exceed
beyond 3 levels (which also matches ISO standard). For usages that
doesn’t require invisible lines, only one layer with level zero is
enough.

When you want to include multiple levels in one layer, Enable `multiple
levels` option.

## Line types

LANPR can extract different line types from your mesh, in the future
this functionality may be extended. Currently it supports:

  - Contour lines
  - Crease lines
  - Material separate lines
  - LANPR edge mark (compatible with Freestyle data, this function is
    preserved)
  - Intersection lines
  - Modifier deformed original edges (inoperative yet)

You are able to toggle the checkbox beside each line types your line
layer. Intersection line is only available when intersection is enabled
in LANPR options.

## Line style

LANPR engine offers a very simple line style system. You are able to
configure line color and thickness.

The `Use Same Style` option is for simplicity, this is enabled by
default. When disabled, you can configure individual styles for
different line types. Please note that the thickness of line types are
relative to the master thickness.
