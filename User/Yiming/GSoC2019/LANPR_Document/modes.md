# Modes in LANPR

There are two modes in LANPR: CPU and GPU modes.

## CPU mode

LANPR computes everything on your CPU but then draws the results using
OpenGL. CPU can compute complex occlusion information and intersection
lines.

CPU mode is partly threaded, it should be substantially faster than
Freestyle. Also, LANPR should also deliver a relatively stable
performance in scenarios with unevenly distributed geometries and camera
configurations.

## GPU mode

LANPR does line transformations and feature line extraction using OpenGL
shaders in a computational way (Hence it doesn’t matter if you enable
CUDA/OpenCL or not, it is still OpenGL). Then the result is again drawn
using OpenGL.

(Note: When GPU mode is selected, LANPR still needs to calculate
intersection lines on CPU. In the future, GPU based intersection
calculation may be implemented.)

LANPR can render feature lines in real-time with GPU mode, although with
heavy scenes it might be slower. However, GPU mode can not achieve
accurate occlusion, when this is a need, you may want to choose CPU
mode.

Your machine needs to have video card installed in order to render any
results out of LANPR engine itself, because either way it requires
OpenGL. Intel core display is acceptable too, but may encounter some
problems with LANPR GPU mode (newer models does seem to handle things
much better).
