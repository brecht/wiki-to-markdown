# Using LANPR engine

Select LANPR as the render engine, and basically it’s done. This page is
an overview of the UI and a general description of what those UI
elements are for.

## LANPR panel in the render tab

You can access the most frequently used parameters and operations from
LANPR’s main panel in Render tab.

There are some configurations in sub panels too.

**Options**: Including global options for LANPR, including chaining and
intersection switches.

**Effects**: This panel contains line taper and depth controlled style
settings. Availability depends on the master mode of LANPR.

**Normal Based Line Weight**: This is another line effect which takes
the *surface* normal into account, this could be used to generate fake
lighting and some interesting thickness effects.

## Other two tabs dedicated to LANPR

When you select LANPR as the render engine (or enable LANPR in other
engines), there will be two new tabs available in the properties area.
They both work for LANPR, and looks similar in content. One of them is
for collection-specific settings in LANPR, and another one is for the
object.

In these two tabs, you can set usage flags for active collection or
object, and selecting line types, styles or GPencil targets as well. For
how this works, please refer to [selecting render content in
LANPR](selection.md)
