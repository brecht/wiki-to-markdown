# Week 9

## Things have done

1.  UI is settled, including LANPR main panel and modifier/separate tab
    (Feature line modifier configurations now in a separate tab, if
    LANPR engine is selected or enabled in other engines, it will
    appear). UI should only include minor changes in this stage.
2.  Fixed alpha problem during F12 render.
3.  Utilize the new GPencil modifier callback (with frame argument in
    `deformStroke()`) to make MultiStroke modifier more tolerant to
    multiple deform/generation modifier in the stack.
4.  Various code style fixes.

## Next week

1.  Code fixes, eliminate warnings.
2.  Re-write GPU mode cache code path. (I'm not going to trace the
    memory issue in the legacy code, it's too messy...)
3.  Make SVG export function into a menu entry. (LANPR only for the
    moment)
4.  Check camera projection of GreasePencil, find a way for it to export
    view-space (flattened) stroke too.
5.  Basic documents based on the current UI workflow.
