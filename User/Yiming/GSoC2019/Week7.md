# Week 7

## Things have done

1.  Make operations in GPU and CPU modes more consistent, including
    object/collection usage flags and modifier selections.
2.  Option for using world background color.
3.  Rendering with background alpha now blends properly.
4.  CPU mode in viewport camera mode now aligned with actual scene, able
    to zoom and move around now.
5.  Initial SVG export investigation.
6.  Safer chaining value settings for CPU mode.

It's been a tough week for me the last few days. Thank god things
finally cleared up on Monday :/ Next week should have a very free time
frame.

## Next week

1.  **To Complete** SVG export functions.
2.  **To Complete** Code style migrations, including file structure,
    naming conventions and internal function usages. Prepare the code to
    reserve time for the review.
3.  Should not have drawing errors and shader memory leaks by the end of
    the week.
4.  LANPR in GPencil mode will not automatically update, should fix
    this.
5.  GPencil Sample modifier is still problematic, fix it using another
    pre-process algorithm.
6.  Report says object/collection selection will not work properly in
    duplicated object/collections. This should not work in current code,
    will sure investigate this.
7.  Try remove Freestyle code base from Blender.
8.  Remove image filter mode from LANPR temporally. (It doesn't seem to
    get many love from the users or me :') )

## If you want to use the crappy SVG exporter now...

Follow these steps:

1.  You should have a GPencil object to be exported.
2.  Select(Activate) the object, in object mode.
3.  Search(F3/Space) for "Export to SVG".
4.  Go to one of the text editors in Blender, pull the text file from
    the drop down selector, each corresponding to a GPencil layer.
5.  Use the text editor to save the content as \*.svg file.

The file will suffer from ratio differences, way-off stroke width,
filled regions, and uniform stroke pressure.
