# Week 11

Finally into code review\!

## Things have done

1.  Fixed all memory problems within LANPR. (Turns out to be some
    batches are not freed upon quit, not drawing updates)
2.  Changed LANPR background to using world background and film
    transparency.
3.  Normal controlled line style now comes with default style.
4.  Fixed normal value errors in chained lines.
5.  F12 now shows rendering progress/status. Viewport rendering status
    not implemented yet.
6.  Various UI and code style fixes, including some doxygen styled
    comments, also slightly improved stability by solving some hidden
    access problems. Fewer crashes were observed.
7.  Split LANPR into a separate local branch, and cleaned up for code
    reviewing.

## Next week

All about code reviewing. I’ll try to attend the 10:00 CEST meeting and
see what developers are thinking.

I will also include patches for other accessory modules for LANPR,
including GPencil modifiers, smooth contour modifier, SVG functions and
UI fixes. These patches will likely be generated upon LANPR for
continuity.

LANPR’s GPencil modifier doesn’t have vertex weight support yet, there’s
only a stub on the UI. Should implement that soon, likely after LANPR is
done reviewing.

-----

Yiming
