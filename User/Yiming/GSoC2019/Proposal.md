## LANPR 2019 Proposal

This passage is for LANPR's (probable) GSOC2019 project.

### What is this

LANPR is a 3D NPR feature line rendering engine developed by YimingWu,
it is aimed to replace legacy Freestyle. Last year, YimingWu has already
made everything in LANPR work with Blender, and it can take on some
basic production on NPR renderings. Learn more about [LANPR's
GSOC2018](../Proposal_2018/index.md)

Updates will be indexed [in this
page](Updates.md).

### Main Tasks

  - Achieve **LANPR to GPencil/SVG** format (Co-operation with
    Darkdefender guy). If GPencil converting is OK then we can implement
    SVG export from generic GPencil data. Variable width information
    should be stored in Inkscape identifiable format (such as brush
    strokes). Layers should also be included.
  - **Make a more intuitive UI**. Current UI is too developer-oriented,
    not really good for art producto work with. At least one present
    will be included, making LANPR ready to render in default style the
    moment you switch to the engine. Some parameters should be combined
    with Blender's internal ones to avoid strange value settings.
  - **Optimize composition of line and solid rendering results**.
    Currently, render results are composed by a composition scene who
    directly links to the original one, there should be a better way.
    Composition method that Freestyle uses might bring some additional
    problems to LANPR's rendering process, additional considerations
    requires.
  - **Fix bugs in triangle-clipping algorithms**.
  - **Camera shift should be supported**. After supporting this
    function, LANPR can then implement non-linear perspective feature.
    (Must finish the previous task)
  - **Optimize shaders for drawing lines**, and try to provide some
    simple line patterns. It might be possible that LANPR inherent
    Freestyle's Line modifier stuff, we'll see on the go.
  - CPU/GPU calculation modes are fast enough now, so LANPR plans to
    **deprecate image space edge detection mode (Snake mode)**. For
    circumstances who needs such function, one can easily done this in
    composition nodes.

### Concerns on the plans for this GSoC

  - **School schedule**. I'm doing school project review this semester.
    So I will have to spend less time on GSoC (compared with last year).
    Under this condition, I choose to implement the most crucial and
    most required functions only.
  - **More focus on getting into master branch**. Relatively lighter
    tasks would enable more time to be spent on code review.

This is not the final list, If you are users who use/will use LANPR for
production, please contact me to state your opinion. Thank you\!

### Benifits

  - Fast and stable output of LANPR will become the fundation of many
    production fields like Manga/Animations.
  - LANPR produce accurate result for any model, which means it can also
    be used to display complex structures.
  - The intuitive UI should lower the bar for the ones who wants to
    learn LANPR. The design should be simpler than Freestyle for the
    users.

### Planned time schedule

| | Development for                              | | Estimated time         | | Remarks          |
| ---------------------------------------------- | ------------------------ | ------------------ |
| GPencil/SVG exporting                          | 1 Week                   |                    |
| UI and composition workflow                    | 1 Week                   |                    |
| Clipping bug fixes and camera shift capability | 1-2 Weeks                |                    |
| Shader optimization                            | 1 Week                   |                    |
| Code review and modifications                  | For the rest of the time |                    |
| Freestyle line modifier adapatation            | 2 Weeks                  | Need investigation |

### About me

My name is Yiming Wu (吴 奕茗). I live in China.

Currently studying at **Industrial Design Department at Xi'an Jiaotong
University**.

Blender User experience since 2011. Participated GSoC 2018 to implement
LANPR engine in Blender.

  - Twitter: @ChengduLittleA
  - Frequent E-Mail: xp8110@outlook.com
  - Blender.chat: @ChengduLittleA
  - Freenode IRC: YimingWuDell
  - QQ(China): 1094469528
  - Weibo(China): @成都小A
