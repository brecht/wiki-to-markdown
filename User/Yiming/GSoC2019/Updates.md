# LANPR Development for GSoC 2019

## Notes

  - [Thoughts on User
    Interface](UiThoughts.md)
  - [Thoughts on GPencil modifiers that are used to create stylized
    lines from LANPR](ModifierThoughts.md)
  - [General SVG exporter notes](SVG.md)

## Updates

  - [Week 1 Report](Week1.md)
  - [Week 2 Report](Week2.md)
  - [Week 3 Report](Week3.md)
  - [Week 4 Report](Week4.md)
  - [Week 5 Report](Week5.md)
  - [Week 6 Report](Week6.md)
  - [Week 7 Report](Week7.md)
  - [Week 8: What's
    Reamining](Week8WhatsRemaining.md)
  - [Week 8 Report](Week8.md)
  - [Week 9 Report](Week9.md)
  - [Week 10 Report](Week10.md)
  - [Week 11 Report](Week11.md)
  - [Week 12 Report](Week12.md)

## LANPR Document Draft

[Document Main Page](LANPR_Document/index.md)

## Summary

[GSoC 2019
Summary](User:Yiming/GSoC2019/LANPR_GSoC_2019_Summary)
