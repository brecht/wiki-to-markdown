# Week 4 Report

## This week

![Object LANPR Modifier|300px|thumb](../../../images/LANPR_Week4_0.jpg
"Object LANPR Modifier|300px|thumb")

1.  Moved stroke generation modifier to Object.
2.  UI modifications for easier use. Now named "CPU/GPU" modes for
    clarity.
3.  Default values now loaded with blender startup.
4.  Image-space chain reduce now have a threshold value to allow
    precision adjustments.
5.  Added "Include/Exclude/Occlusion Only" settings in collection.

I had three tests during the week and finally over all of them :D. Time
to clean stuff up.

## Things from the last week

1.  About intersection algorithm: Intersection lines \*\*do\*\* need to
    be clipped since visible segments could also penetrate near clipping
    plane. Very rare condition, but needed.
2.  Image-space chaining bug is solved from point 4 above.

## Still to come

1.  Actual stroke generation functions and automation.
2.  Split chains based on their types.
3.  Check for potential segment type errors.
4.  Look for the problems in GPU mode. Now producing wrong
    transformations on Intel Graphics, could be a problem with DRW.
