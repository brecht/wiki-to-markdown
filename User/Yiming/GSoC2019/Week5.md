# Week 5 Report

![GPencil Selection|300px|thumb](../../../images/LANPR_Week5_6.jpg
"GPencil Selection|300px|thumb")

## This week

![Feature Line modifier|300px|thumb](../../../images/LANPR_Week5_3.jpg
"Feature Line modifier|300px|thumb")

### Major stuff

1.  **Fixed** LANPR now uses master "color" and "thickness" instead of
    following contour values.
2.  **Done** Stroke modifier now fully functional (includes target
    layer/material and line type selecting).
3.  **Done** Added LANPR settings for collection, now fully functional.
4.  **Done** Now you can set "Include/Exclude/OcclusionOnly" flags to
    objects and collections, to control what lines are needed. See below
    for how it works.
5.  **Fixed** LANPR now uses two separate threshold values for
    image-space and geometry space chaining. See below for explanation.
6.  **Fixed** Stroke sample modifier now re-written to accept bone
    weights in GPencil strokes. Which is also useful without LANPR.
7.  **Temporary** There's a "bake" button for baking frames in an
    animated scene into GPencil frames.

### Minor Fixes

1.  \`\`\`if()\`\`\` statements now all use \`\`\`{}\`\`\` brackets to
    prevent potential merge errors.
2.  Comments all changed into \`\`\`/\* \*/\`\`\` style to follow C
    standard.
3.  UI fixes, hide/deactivate items when needed to make it more logical
    to operate on.

## From the Last Week

1.  **Done** Stroke generation from object modifier.
2.  **Fixed** Chaining occlusion errors.
3.  **Fixed** Chain now only includes one type of lines.

## Next week

1.  Chain optimization functions, including:
      - discard of very short segments
      - small loop preventing
      - overlapping reduction.
2.  GP Modifiers:
      - pattern
      - random
      - backbone stretch improvements
      - "sketch modifier"???
      - see [this
        document](ModifierThoughts.md) for
        a more clear idea what this is.
3.  LANPR and GPencil frame data should be automatically updated by
    Depsgraph.
4.  Better have a button that only re-create strokes for current object
    (by the Feature Line modifier). GP strokes are not deleted when
    LANPR is updating them, this could lead to overlapping.

-----

## About Object/Collection Usage Flag

![Collection Flag|300px|thumb](../../../images/LANPR_Week5_4.jpg
"Collection Flag|300px|thumb") ![Object
Flag|300px|thumb](../../../images/LANPR_Week5_5.jpg
"Object Flag|300px|thumb")

This is how the usage flag works.

By default, Collection is set to "Include", which means this collection
will be included into LANPR's feature line calculation. For object, the
default is "Inherent", which means it follows its parent collection's
settings.

If you set object's usage to "Occlusion Only", the object will still
take part in LANPR's calculation, however, no lines will be produced by
this object, but it can still occlude other object in the calculation.
If the flag is "Exclude", LANPR will ignore this object completely, as
if it didn't exist at all.

For collection, you can also set it to "Occlusion Only" and "Exclude",
but if you only set the flag, the object within this collection who also
happens to have a Feature Line modifier will still be included into
calculation. To prevent this from happen, check the "force" option
below. Then the children objects and those recursive children
collections are all forced to follow the rule.

You need to set target GPencil object for strokes to appear, which
applies to both objects and collections.

**How to generate strokes:** As of now, you need to manually click
"Update" in LANPR, then click "Update" in any one of your Feature Line
modifiers, and tab your GPencil object to trigger re-draw.

## About Stroke Chaining in LANPR

![Chaining Thresholds|300px|thumb](../../../images/LANPR_Week5_2.jpg
"Chaining Thresholds|300px|thumb")

LANPR produces accurate occlusion info, yet in 3D space, those lines
might be very jaggy and not good for applying a texture. Thus we need a
good chaining algorithm. Currently it performs ok, LANPR uses two step
chaining method now, geometry-space and image-space chaining, they can
produces smooth lines in most cases, yet some cases it's hard to say
which way is better.

![Image-space Chaining|300px|thumb](../../../images/LANPR_Week5_1.jpg
"Image-space Chaining|300px|thumb")

For bumpy surfaces, one might rely more on image-space chaining to erase
those non-continuous bumps along a curve, however for many hard-surface
scenarios, it is more important to keep the geometric information
accurate. So now LANPR incorporates two threshold values for users to
choose which way they want. Set thresholds to zero will disable the
corresponding chaining step.

The image to the right shows a result of an image-space prioritized
chaining. In which you can see the spatial result is incorrect, but it
gives a smoother approximate in image space projection, which is useful
in many cases, especially in rendering animated characters. Thus the
two-threshold solution is necessary.

-----

YimingWu
