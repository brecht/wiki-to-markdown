## LANPR User Interface Thoughts

### Current State

In UI aspect, currently LANPR has some major draw backs:

![drawbacks](../../../images/Lanpr_2019_ui_notes_1.png "drawbacks")

  - Render is too complicated to set up, even for a very simple scene.
  - Lack of proper arrangement for so many options.
  - No presents.
  - Poorly integrated with Blender's internal UI structure. Need to
    switch back and forth to get parameters right.
  - Only have a very primitive composition support.
  - ...

### Detailed Discussion

#### Composition

![composition](../../../images/Lanpr_2019_ui_notes_5.png "composition")

LANPR should come with composition by default. Here propose a viable
way: When user enables LANPR line render in a EEVEE or Cycles scene, the
scene is automatically link-duplicated into another scene in the
background. User don't adjust the values in this scene directly, this
linked scene is only for line rendering and composition. to the user,
this process should be hidden or not of much interest, and they don't
have to switch between engines to adjust values.

#### Line Set/Selection logic

The line style configuration has been confusing since Freestyle's time.
LANPR uses similar principle for the same task, so the same problems
inherited, such as QI range, inclusive/exclusive logic, select within
collection... We do need these functions to achieve many desired
effects, so we won't be removing them. However, to make stuff easier to
use, understand and customize, the UI needs to be revisited in a
different way.

![line logic](../../../images/Lanpr_2019_ui_notes_3.png "line logic")

Now the proposal:

For simple and straight forward situations, provide a default present.
UI should include very few most frequently used properties, and hide
everything else. For example, only color and line thickness is shown.
Just like the Freestyle checkbox under render tab. (Consider re-use
this)

When things get a little advanced, e.g. the user wants to adjust
individual styles for different line types, then the interface should
only show the entries which are being adjusted, untouched entries should
be in sync with the master style. Current design is too stupid that
sometimes you even need to set the color five times to get a consistent
look.

As for object/material/collection based line selection, Here propose a
way to simplify this process.

In Freestyle, Line selection is achieved under "Line Sets" panel, and
there's a lot of things going on there. The interface is distracting and
hard to understand (This is partly because Blender 2.7x has a
layer/group system, in 2.8x it is replaced with unified collection).
LANPR will remove this line selector section, instead it will be put
under object/material/collection panels. Users will be able to adjust
specific line set just like they are dealing with materials of an
object, and the "inclusive/exclusive" selection logic will also become
easier to understand within such context.

#### Feature Line Specific Properties

Crease angle, camera clipping, distance influence, fake lighting
direction and many other stuff like these can be treated as global
properties within LANPR's feature line extraction algorithm. However,
taking one step further would be awesome.

![distributed](../../../images/Lanpr_2019_ui_notes_2.png "distributed")

![material controlled ](../../../images/Lanpr_2019_ui_notes_4.png
"material controlled ")

For example, instead being set for every scene, crease angle can be
treated as an material-specific value. Artists can specify different
crease angle to different parts of an object to further convey smooth or
complex rigid surfaces without making multiple line sets. Enabling
different distance influence to selected objects in a scene can make
them pop or fade out too.

The fake lighting thing can also be integrated with lamp materials, but
this needs some more considerations.

### GPencil

Proposed design already separated the major parts of LANPR into other
parts of the interface which makes the configuration more logically
relatable, which is good. We still have some things that may be included
in LANPR's own panel that has not been added yet. One of the most
important part is the bridge to GPencil.

A great news recently is the support of GPencil Texture, which when
combined with LANPR stroke output, and the smooth contour modifier by
DarkDefender has the potential of replacing Freestyle's stroke style
system. GPencil already got its effect system and a bunch of modifiers.
So this can be considered of greater potential than Freestyle's own
stuff.

How LANPR handles GPencil conversion is not clear yet. But the principle
would be to use some sort of rule to tell LANPR when and how to convert
the strokes into GPencil object for next step rendering, because we need
to make use of GPencil's modifier, effects and texture (almost
everything in it), so there has to be some sort of a pre-defined
automation going on which handles GPencil conversion and rendering,
especially with animation usages. This need some more research.
