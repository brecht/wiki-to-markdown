# LANPR GSoC 2019 Summary

GSoC 2019 has ended. LANPR was fortunate enough to have get a lot of
improvements from the last year’s development. A huge thanks for
everyone that checked in and helped me in the development process\! I am
positive that LANPR could make it into Blender versoin 2.81, at least
the core functionalities.

## What has been achieved

  - Stability and performance improvements for both modes in LANPR.
  - Feature line chaining algorithm improvements for better animation
    quality.
  - Grease Pencil output capability for LANPR.
  - A new selection of rendering components.
  - New interface to fully match Blender 2.8x design.
  - Better code structure to match Blender’s source conventions.
  - Many small features for workflow integrations.

## Where can I find the work

### Code and compiled branch

Downloading a pre-compiled binary for use? Go to
[GraphicAll](https://blender.community/c/graphicall/) and look for NPR
for your operating system. Those binary packages are built by the
awesome people form Blender Community.

You can get source code from Blender’s official repository as shown
below.

    git clone https://git.blender.org/blender.git

Use `soc-2019-npr` branch to get a complete working example of all the
functionalities LANPR provides. Currently `temp-lanpr-staging` branch
can be accessed to see temporary code for merging. Will update this page
when the new code branch is available for upcoming developments.

To see the complete code patch and review changes, go to [LANPR patch on
Blender Developer](https://developer.blender.org/D5442). Subscribe this
patch to get notifications when there are updates.

### User document draft

Document is available on [this wiki
page](https://wiki.blender.org/wiki/User:Yiming/GSoC2019/LANPR_Document).
We are going through the process of code reviewing and workflow
polishing (and creating a lot of new problems in the same time) with the
developers, so this version of the document only applies to current
development version of LANPR, and may suffer many changes as the
development goes.

### Development logs

Development logs together with some notes are all available here on the
[updates
page](https://wiki.blender.org/wiki/User:Yiming/GSoC2019/Updates).

You can also find updates and user feedbacks from [this Blender Artists
thread](https://blenderartists.org/t/gsoc-2019-lanpr-updates-here/1159633)
and [this DevTalk
thread](https://devtalk.blender.org/t/gsoc-2019-lanpr-development-and-feedbacks/).

## Other information

[LANPR main page on Blender
Wiki](https://wiki.blender.org/wiki/User:Yiming). The last year’s wiki
is archived as the wiki migrates, you can also easily [find those
previous pages there](https://wiki.blender.org/wiki/GSoC).

You can access [LANPR main page on YimingWu’s personal
website](http://www.wellobserve.com/index.php?page=NPR/index.md), this
include all the previous development logs and related information that
doesn’t belong particularly to GSoC. Please note that the majority of
the content except GSoC related is in Chinese.
