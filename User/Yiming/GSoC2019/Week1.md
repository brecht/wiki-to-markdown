## Week 1 Report

![GPencil Comp|300px|thumb](../../../images/LANPR_Week1_0.jpg
"GPencil Comp|300px|thumb")

### Things have done

In this week, there are mainly following things to address:

1.  Naming for most parts of the code are now in Blender's style.
2.  Included a GPencil stroke generator. Capable of generating 2D chains
    from smooth contour modifier result (Sebastian's work) and from 3D
    chains computed by LANPR code(My part). Included an option for
    toggling drawing GPencil on overlay or with depth.
3.  GPencil stroke generator now support animation rendering from LANPR
    data, which execute stroke calculation directly in render, no need
    to use custom script.
4.  Cleaned up the memory allocation in LANPR's software mode. Now
    everything uses memory pool in software mode. The data is cached
    after calculation and freed at the beginning of the next frame or
    when the engine destroys.
5.  Other code cleanups.

### The following week

1.  \`\`\`engine\_free()\`\`\` from LANPR engine is not called when
    blender quits. Resulting in memory leaks. Maybe missing some
    registration parameters.
2.  Ctrl-Z will cause the render buffer to reset. Maybe should move the
    whole render buffer into \`\`\`g\_data\`\`\` ?
3.  3D chaining vertex attribute has a offest of 1, cause determined,
    suspect incorrect or duplicated vertex when generating GPencil
    stroke.
4.  New UI for composition, should support material/object/collection
    selector and the newly integrated GPencil functionality.
5.  LANPR engine should be able to toggle GPencil or internal render
    mode depending on the modifier and active engine selection.

-----

Yiming
