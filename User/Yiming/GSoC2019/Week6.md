# Week 6 Report

## This week

### Major stuff

1.  **Fixed** LANPR/GP Targets auto update when frame changes.
2.  **Implemented** GPencil multiply modifier. Capable of splitting
    strokes at a threshold angle and duplicate strokes around the
    original ones, fading effects included.
3.  **Implemented** High performance chaining algorithm, capable of
    handing very large scenes.
4.  **Fixed** Bugs in GPencil stretch modifier (now length modifier).
    Now you can stretch or shrink GPencil strokes, with both absolute
    and relative length.
5.  **Fixed** A bug in near-plane clipping code, now works flawlessly.

### Minor stuff

1.  Usability improvements, including various interface improvements,
    automation and convenience functions.
2.  Modified some default values to reduce effort of adjusting them
    every time.
3.  Performance and stability improvements in CPU and GPU mode.
4.  Edge split modifier can be disabled using a global switch in LANPR,
    enabled by default, reduce the problem of feature line rendering.
5.  Various small bugs.

Tasks from the last week are all done except the remaining optimization
of the Chaining algorithm.

## Next week

1.  UI should still be more informative. For example, when there's no GP
    targets, there should be a message beside the update button.
2.  CPU mode preview improvements, including camera view and 3D view
    (3D: How?)
3.  GPU memory leak thing.
4.  Edge detection mode fix.
5.  Should use world's background color.
6.  Vertex group and material selection thing.
7.  I'll add when things occurs to me.

-----

## Cache update: how it works

LANPR doesn't automatically update it's data cache by default, because
it's a computation-heavy task. For lighter scenes, you can enable auto
update by clicking the "Auto Update" button under LANPR's mode selector,
or you can manually update everything by clicking "Update" button on the
right. In GPU mode, LANPR will only do Intersection cache updates to
save time. If auto update is enabled, you should be able to view the
result directly in your viewport when the frame is changed.

When not in LANPR engine (which means you are using GPencil objects as
the result target), there's another option "Overwrite", by default it is
disabled, so you can safely edit the strokes after updating. If you
messed up a frame, you can re-genereate this frame by enabling
"overwrite" and clicking the update button.

-----

Check the [original task-list of this week
here](http://www.wellobserve.com/index.php?page=NPR/2019/logs/20190628_Week06.md&translation=disabled).
It's very lengthy...
