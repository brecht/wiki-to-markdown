# Week 10

## Things have done

1.  Fixed most of compiler warnings in LANPR module, one or two left for
    better solution.
2.  Fixed shader and matrix assertions for deprecated shader. Thank to
    Sebastian’s test.
3.  Camera projection function is updated by Antonioya, so I’ll make svg
    output using the new one.
4.  Basic user document based on the new interface. Check back later for
    the link.
5.  Made many comment styles into oxygen standard. Not completely done
    yet.

## Next week

1.  Fix warnings in SVG and GPencil module as well.
2.  Re-write GPU mode cache. (STILL\!?\!?)
