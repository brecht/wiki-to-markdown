## Week 3 Report

### Things from the last week

1.  **Fixed** engine\_free() callback, now no memory leaks on LANPR
    side.
2.  **Fixed** LANPR shader free problems.
3.  Software mode now support camera lens shift. Should be usable with
    any camera configurations now. (Only fisheye not implemented)
4.  Implemented Image-space chain connection to reduce jaggy lines.
5.  After some back and forth, finally settled down on a more solid
    design for Bridging LANPR with Blender and how to use GPencil
    modifiers. More information to come.
6.  Minor bug fixes.

### Still to come

1.  Intersection lines needs near-far clipping before perspective
    division and coordinate shift. Need to prove this isn't needed.
2.  Perspective correction for occlusion points on the chains or use
    world-space split points.
3.  Still have some VBO utility structures not cleared correctly when
    LANPR exits.
4.  Move stroke generation modifier into Object modifiers as part of the
    design.
5.  Implement the new LANPR to GP path, including selections and
    generation.
6.  Image-space chaining reported to have some bugs in it. Look into it
    during the weekend.

I typically do more things in the weekends because I'm still enrolling
in school. So expect updates before Monday. Happy weekend every one\! :)

-----

YimingWu
