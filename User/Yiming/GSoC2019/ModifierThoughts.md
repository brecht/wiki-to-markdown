## A more detailed modifier proposal for GPencil and LANPR

### What is this all about?

Grease Pencil has received a lot of great improvements since Blender2.8.
LANPR used to render all the strokes by its own. Now GPencil already
have some style and texture options that we are able to use, so It is
also reasonable for LANPR to utilize this part of the functions. At the
current stage, GPencil's modifiers and stroke material are of the most
interests. Since Freestyle is well known for its ability to generate
feature lines with artistic shapes and textures, and people loved it, so
it is necessary for us to implement such kind of functions into LANPR.

Since we can already share data between LANPR and GPencil, we then need
more stroke modifiers to help us achieve the target.This short proposal
is dedicated to extending GPencil modifier development for better NPR
rendering usages.

### What are we lacking?

How do we get LANPR data into Grease Pencil eventually? Now it is done
by manually calculate the feature lines, and then create GPencil
modifiers. There's a need to automate the process, or at least to notify
the user about LANPR data (Thanks for Antonioya for mentioning this).
This process should be something like this:

  - Use stroke generation modifier to automatically set LANPR a default
    present. User can adjust those arguments later.
  - Use a operator similar to "Quick Smoke", the "Quick LANPR" should
    create the GPencil object and set up everything you needed.

To me the first solution may be better, since one can use the stroke
generator with their existing GPencil objects, and later could utilize
LANPR's line select functions to give different GPencil objects
different strokes and effects. Both methods doesn't necessarily have to
be executed in LANPR engine though, because GPencil is used to draw.
Hence I also put it in this modifier proposal.

There are some requirements (which isn't of great importance if the only
thing GPencil does is drawing) to meet specific needs for stylized
rendering: Please give feedback on what you think is also necessary too.

  - **Sketchy stroke effect**, Which may include effects like:
      - random curves along the stroke
      - not accurately placed stroke tips
      - multiple strokes in similar directions
      - simplified stroke to represent a collection of similar strokes.
  - **Graph with industrial characteristics**, To be used in e.g.
    structural representations:
      - dot/dash/doubled line styles
      - adjustable line tips, e.g. caps, arrows or something else
      - regulated geometric patterns along the stroke e.g. sinus wave.

Current GPencil has some basic modifiers. But to make better use of
them, which means to better adapt them to algorithm-generated strokes,
few existing modifiers may need some re-works:

[thumb|200px](file:LANPR_Modifier_0.gif)
[thumb|200px](file:LANPR_Modifier_1.gif)

  - **Simplify**: When in adaptive mode doesn't preserve geometry
    features very well. It will lose turning points. See image for
    demonstration.
  - **Noise**: Only does "visually predictable" noise patterns, and it
    is only in 2D (appears to be only perpendicular to the stroke normal
    direction?). A noise back-end like something that we use in the
    animation curve editor would be much better in terms of randomness.
    See image for demonstration.
  - **Thickness**: Can adjust thickness factor, but not override the
    original, also not able to adjust influence from by the distance
    from two end points.

Combining those points can give us a general considerations for what
functions that those modifiers should have:

1.  Able to control the density and distribution of the points in a
    generated stroke.
2.  Able to manipulate the geometric pattern of a stroke more freely.
3.  Able to simplify single or multiple strokes at once, in a
    shape-preserve matter.
4.  Able to sometimes deconstruct one stroke into multiple strokes to
    represent one shape.
5.  Able to apply textures to strokes.
6.  **And most importantly**, most factors that goes into operations
    above should be able to be randomized or regulated according to the
    desired outcome.

But also keep in mind, the modifier shouldn't be too general that
setting up an effect could use too many of them, that is not how it
works on the artists side. For example, a "Sketch modifier" is much more
convenient than "Split, Multiply, Random, Offset". It is better to bare
a mind that put user's not programmer's thought first, but I don't know
how far can I go to be honest.

### OK, but where are the proposed modifiers?

Propose to add such modifiers:

**Stroke Generation modifier**

  - Should automatically set up and call LANPR.
  - Should add options to be able to cache the frame result for GPencil.

**Sampling modifier**

  - To regulate/randomize the distance between adjacent points in a
    stroke, also control the density.
  - Able to place points using a length-based pattern to create e.g. dot
    and dash stuff.

**Extend/Shrink modifier**

  - Able to extend the end points, better with a curvature that can be
    controlled to follow the original stroke.
  - Able to shrink the stroke by/to a specific length.

**Multiplication modifier**

  - Able to duplicate segments around the original stroke, with optional
    randomness.
  - **At the same time**, it should also be able to split stroke into
    multiple separate strokes. Should add options to co-operate with
    Sampling modifier.

**Simplify modifier \[MODIFICATIONS\]**

  - Make it more shape-aware.
  - **Should be able to merge multiple similar strokes into one**.

**Smooth modifier \[MODIFICATIONS\]**

  - Add another algorithm to generate more controlled curve lines like
    Freestyle's Bezier modifier.

### More stuff that is worth considering

There are some things that aren't specifically related to
LANPR-\>GPencil modifier development. But they can also be relevant,
opinions are welcomed.

One interesting comparison of LANPR now would be Pencil+. On technical
side (and let's just ignore the cartoon highlight stuff it has), two
things still needs improvements on LANPR's side. 1) Time-coherence of
patterned/textured stroke. 2) perspective disortions. Will take some
time to achieve, the algorithm itself isn't too hard. However, with
GPencil, I can already see some different stylization approaches, such
as time-based warping and even geometry re-generation from strokes.

There are another use that I've been mentioned to is rotoscoping. I
don't quite grasp how this is going to work because we can already use
stuff like cyptomatte for rendered passes. But could be something to
look forward too. This will require further refines to LANPR chain
algorithms, also the help from Smooth Contour modifier.

-----

YimingWu
