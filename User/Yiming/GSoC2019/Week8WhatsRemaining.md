# LANPR: What's remaining?

## Why are we discussing this

LANPR is a versatile module that could potentially have much greater
extensive use in the future, its development goal grows along with the
project itself now. It's not very realistic to do everything in a short
period of time. So we need to distinguish what needs to be down in this
stage (GSoC and shortly after), and what is reserved for the future.
Here in this passage I'll propose my plan.

### This stage

Propose to include these parts into the first merge, which is supposed
to happen within the GSoC time frame:

  - LANPR Engine as it is now (Remove edge detection mode for it's lack
    of use).
  - LANPR to GP support.
  - Feature line modifier.
  - Extra GP modifiers from LANPR for supporting line styles.
  - Basic LANPR to SVG exporter (Which uses LANPR data directly).

Lower priority, if not able to then put into the next stage:

  - GP/General curve to SVG exporter (Because this may require camera
    transform and various other operations, like stroke solidify and so
    on).

### Tasks to finish before we can reach the goal：

<s>Striken out</s> lines are done.

  - LANPR Engine
      - Slight memory bugs in GPU mode.
      - <s>Buffer auto update outside the engine (When using
        GPencil).</s>
      - Object/Collection selection for duplicated items isn't working
        properly.
  - GPencil
      - <s>Sample modifier bugs.</s>
      - <s>Stroke thickness ineffective in duplication modifier.</s>
      - <s>Length modifier has some bugs.</s>
      - `generateStrokes()` and `deformeStroke()` are in separate steps.
  - LANPR-\>SVG
      - Adapt stroke thickness and viewbox size.
      - Modal file dialog.

Actually, there's not much there left to do. I'm gonna spend most of my
time making sure that those functions work in a stable state, in the
mean time, I'll continue clean up the code to better meet BF's standard.

### Current problems:

For LANPR auto update, it's only available for post frame change. When
using GPencil as output, because the lack of drawing callback, auto
update is ineffective. Currently I'm working on a workaround, a more
"blender standard" way will be implemented in the future, and any help
is welcomed. (It seems that the current method have some access conflict
risk as it is probably running in another thread by depsgraph)

Thickness problem is solved. (Yeah\!)

Stroke sample function bug is fixed.

## Future plans

  - Vertex group selection in LANPR calculation.
  - Material selection in LANPR.
  - A more versatile SVG exporter for both LANPR and GPencil objects.
  - ...

We could update this part as things go along.
