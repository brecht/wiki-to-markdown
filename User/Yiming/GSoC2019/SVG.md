# SVG stuff

## Why

Workflow reasons. Easy connect different software.

## SVG format

Here discusses the format that the proposed SVG exporter follows:

[Reference from w3.org](https://www.w3.org/TR/SVG/paths.html)

<?xml version="1.0" standalone="no"?>

<svg width="4cm" height="4cm" viewBox="0 0 400 400"
        xmlns="http://www.w3.org/2000/svg" version="1.1">

<title>

Example triangle01- simple example of a 'path'

</title>

`   `<desc>`A path that draws a triangle`</desc>  
`   `<rect x="1" y="1" width="398" height="398"
            fill="none" stroke="blue" />  
`   `<path d="M 100 100
            L 300 100
            L 200 300
            z"
            fill="#FFFFFF" stroke="#FFFFFF" stroke-width="3" />  
`   `

</svg>

The main thing here is the **<path>** tag. **d** describes the points.
The leading characters in the **d** string represent following meanings:
(The table only includes the commands that are used in our exporter)

| Command | Arguments | Meaning                                          |
| ------- | --------- | ------------------------------------------------ |
| L/l     | X Y       | Absolute/Relative line to (Actually draws stuff) |
| M/m     | X Y       | Absolute/Relative move to                        |
| Z/z     | None      | Close path                                       |

**How to fill the `d`:**

An example of this can be like the following, please note that you can
insert new lines as space for visual clarity.

    <path d="M 123 123 L 123 123 L 123 123 Z" />

To minimize the file size, the space between a command and a value can
be omitted. For example, this is perfectly ok:

    <path d="M123 123L123 123L123 123Z" />

Please note that the space between argument values can not be omitted.

For the **fill** and **stroke** part, they both describes color. If the
tag is not present, then the path won't have such property.

In Blender, Grease pencil width value is measured 1000 as 1 Blender
Unit, which is the size of a default viewport floor grid.

In SVG, if the unit is not specified, then values are all treated as
**px**. We should follow this standard throughout the exporting process.
In order to achieve DPI scale, we can specify **width** and **height**
in the SVG header alone with viewbox property. If you have a 1000px wide
viewbox, with a **width** set to 5 inch, then tis SVG file will be
interpreted as a 200 dpi file (in width direction).
