# Week 12

## Things have done

1.  Made double variation of the math functions into a separate patch
    for review.
2.  LANPR now use BLI's math functions completely to eliminate the need
    of its own math library. Performance is the same as it used to be.
3.  Made LANPR software mode update into a separate thread to let the
    viewport draw progressively. However there are still problems within
    it. Probably a copied scene being freed before calculation finished.
4.  Some other code style fixes.

## Next week (Post-GSoC)

1.  Stabilize progressive drawing in the viewport.
2.  Fix as many code style problems as I can.

I'm gonna continue refactoring the code and keeping the reviewing
process going. Thanks for the support everyone\! :)
