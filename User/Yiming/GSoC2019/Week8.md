# Week 8

## Things have done

1.  Fixed segment counting errors in GPencil sample functions.
    Integrated the modifier with simplify modifier (became another mode
    in it).
2.  Multiplication modifier now renamed MultiStroke. Fixed previously
    not working stroke thickness variation function.
3.  Auto update GP strokes when not in LANPR engine.
4.  Moved all the non-drawing related code into editor/lanpr module,
    refactored naming and code structure to better align with the coding
    preference of blender. Also added few double version of vector math
    functions inside BLI math module.
5.  Normalization checkbox in GPencil thickness modifier now allows the
    use of curve as well.
6.  Fixed erroneous length extending modifier.
7.  Removed freestyle compile brackets regarding to edge/face mark
    operators. Renamed those into LANPR to keep them. Also added them
    back into the UI when LANPR is in use.
8.  Slight changes with GPU shader and APIs.
9.  Decided which functions are needed to merge in the first stage. The
    development won't focus on other stuff outside this stage's target.
    Good ideas will be added after this stage is completed solid. To see
    what we are planning to merge, [check this
    document](Week8WhatsRemaining.md). We
    are getting so close\!
10. The GP team and me are working on a more workflow-oriented UI, which
    is of better consistency with current Blender UI. Will be
    implemented soon.

## Next week

1.  UI should be mostly done with maybe only few slight changes in near
    the future.
2.  Vertex group support for LANPR oriented GP modifiers.
3.  Fix the stroke deform/generation order problem that one of my GP
    modifier induced. Should be quick as Antonioya already added frame
    argument in the deform callback.
4.  Still unable to fix that slight GPU memory leak somewhere in my
    code. Needs investigation.
5.  A Usable SVG API.
