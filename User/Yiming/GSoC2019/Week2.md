# Week 2 Report

(Reported on Friday, still some weekend works to do, and I have two
tests this week :( )

## Solved problems from last week

1.  **Fixed** GPencil stroke vertex attribute error in chaining.
2.  Split stroke chains based on their occlusion levels.
3.  **Fixed** overlay method for GPencil strokes, now use Viewport
    Display-\>In Front.
4.  **Fixed** Triangle culling bug in LANPR geometry pre-processor.
    (during the weekend)
5.  **Fixed** DPIX wrong object position error.

## And something else

  - GPencil modifier works:
      - A proposal on some new modifiers and some modifications to the
        existing ones, please [check
        here](ModifierThoughts.md).
      - Added a (experimental) sampling modifier and a end-point
        extending modifier, which proved it is possible to do such edits
        in the modifier.

## Still unsolved problems

1.  engine\_free() still not working for LANPR.
2.  When GPencil stroke generator is present, LANPR should automatically
    disable the line drawing output from itself and let GPencil handle
    the rest.

## This weekend and the next week

1.  I'm pretty sure that I can grab my hand on the interface.
2.  Implement **all** proposed modifier, and integrate the suggestions
    from users and artists.
3.  According to the original plan, the near-far plane triangle
    intersection calculation should work flawlessly by the end of the
    week. (Done)
4.  If still have time, fix the camera lens "shift" support for software
    mode.
5.  Remove "snake mode" from being accessed?
6.  DPIX is broken under new DRW\_view APIs. (Done)

**Update**

After discussing with my mentors, I found out that It is better for me
to fix most of the bugs, and make sure all the internal features working
properly, then it makes more sense to implement the interface features.
Which means this will be the priority for the next week.

-----

YimingWu
