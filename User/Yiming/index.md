# This is Yiming's Blender Wiki Content Index

## 2023

[ Generic Weekly Reports ](Reports_2023.md)

## 2022

[ Line Art Further Improvements
(2022/03)](LineArt_Further_Improvements.md)

## 2021

[Line Art As GPencil
Modifier(2021/03)](LineArt_As_GPencil_Modifier.md)

[Line Art Modifier Continued(2021/04 -
2021/09)](LineArt_Modifier_Continued.md)

## 2020

[GP Integration, bugs, tasks and
updates](LANPR_GP_New_Solutions.md)

[Line Art development continued
(2020/09)](LineArt_Development_Log_2020_09.md)

[Line Art development continued
(2020/10)](LineArt_Development_Log_2020_10.md)

[Line Art development continued
(2020/11)](LineArt_Development_Log_2020_11.md)

[Additional Line Art functionality
discussions](Line_Art_Desired_Functionality)

### Depsgraph Improvements?

[Deferred Updates For
Depsgraph](Deferred_Updates_For_Depsgraph.md)

## GSoC 2019

[Proposal:Continued Development of LANPR
Engine](GSoC2019/Proposal.md)

[Updates are here\!](GSoC2019/Updates.md)

[GSoC 2019
Summary](User:Yiming/GSoC2019/LANPR_GSoC_2019_Summary)

## GSoC 2018

[Proposal: Blender NPR Line Rendering
Improvement](Proposal_2018/index.md)

[Conclusion: Blender NPR Line Rendering
Improvement](Conclusion_2018.md)

[LANPR: Next Steps](NPR_next_steps.md)
