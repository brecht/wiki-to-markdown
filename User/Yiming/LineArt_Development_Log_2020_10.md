# LineArt Development Log Continued (2020/10)

[Previous Log](LineArt_Development_Log_2020_09.md)

### 1001

*1001-1008 is National day holiday :D*

Fixed code for GPencil modifier API changes.

### 1002

![../../images/Lineart\_vertex\_group.jpg](../../images/Lineart_vertex_group.jpg
"../../images/Lineart_vertex_group.jpg")

Added a function that allows using mesh vertex group to control result
GPencil lines, so that different part of mesh can be controlled
differently by using multiple modifiers and even after baking strokes
(because weight data is transferred to GPencil).

(After some further discussions with Sebastian we eventually came up
with a better UI concept of doing this, so below are just for
referencing the mechanism of this functionality.)

Here's what you are gonna do:

1.  Do whatever vertex group in your mesh, this acts like a source.
2.  Add one or multiple output vertex group in the GPencil object that's
    gonna have the line art modifier.
3.  In line art modifier, enter the vertex group name. If you have
    multiple groups you want to use, then enter the common prefix of
    those names.
4.  Select an output vertex group, then all selected source vertex
    groups will go into that.
5.  \[Done 1003\] If check "Match output", then all selected vertex
    groups will go into the ones with the same name in that GPencil
    object.
6.  In later modifiers, select whatever GPencil vertex group you want to
    use and adjust effect correspondingly.

### 1003

Match output option for vertex group data transfer.

Also: can enable/disable smooth weight transfer. When disabled, all
non-zero weight in source will be registered as weight=1 in output
GPencil vertex group.

### 1008

Crease value changes to degrees. (Finally?\!)

### 1015

Line cut data reduction, initial test show 100MB memory saving on a 5M
face scene. If you have a lot occlusion going on this might save a lot
more memory.

Added some memory debug prints.

### 1016

Camera scaling matrix is applied before rendering, so that you can have
a perspective camera that is of any scale but producing the correct
result.

  - <s>TODO</s>: Check ortho camera behavior\!

Fixed crease line lost bug when having a clipping plane cutting on them.

### 1019

  - \[Done\] <s>Ortho camera scaling behaviour check</s>
      - Scaling is correct, but clipping range is wrong, should be z -1
        to 1 range in NDC.

![../../images/LineArt\_remove\_double\_option.png](../../images/LineArt_remove_double_option.png
"../../images/LineArt_remove_double_option.png")

Added an option for removing doubles during mesh loading. This
simplifies steps for a largely triangulated model that probably came
from some exports from parametric software.

Optimized vertex weight transferring interface. Now leaving name filter
blank will select all vertex groups when "Match names" is checked.

  - TODO: Maybe we need to disable that entirely when that is not
    checked? (unselect output does the same though).

Chaining now perform better\!

  - Fixed a problem within the fuzzy chaining logic, now working
    properly.
  - Optimized for near-bounding area look up so there should be less
    "unreasonable breakups" of chains.

Also updated the patch for depth controlled fading in GPencil alone,
[Here is the Diff](https://developer.blender.org/D9091) now with better
controllability for start-end values.

### 1021

A change that allows only feature lines to be loaded, so there will only
be very few lines that go into the calculation stage, thus memory
savings\!

![../../images/LineArt\_memory\_statistics\_1021.jpg](../../images/LineArt_memory_statistics_1021.jpg
"../../images/LineArt_memory_statistics_1021.jpg")

Also some memory optimizations here and there, removed adjacent links
from triangle (\`rt-\>rl\[3\]\`), this typically gives some extra 200MBs
back. On the right is a diagram that compares memory usage before
today's update and the versions before.

  - \[Finally done in 1023\] Culling function is now buggy due to new
    adjacent info storage, needs investigation.

### 1022

![../../images/LineArt\_memory\_statistics\_1022.png](../../images/LineArt_memory_statistics_1022.png
"../../images/LineArt_memory_statistics_1022.png")

More optimizations on memory footprint, mainly on the intersection data.
The same test shows better statistics on the right.

Partially fixed culling.

### 1023

Culling now allocate its own memory area so it's freed after the stage
is done. Adjacent links are good now. Tested on large models and it's
working perfectly.

Fixed some bugs.

### 1024

Moved calculation progress string to appropriate position.

### 1027

Unchecking "Line Art" master switch will cause hang, because it's
waiting for the calculation to be done instead of stop right away. Now
fixed with proper cancelling.

### 1028

All operator code moved to \`lineart\_ops.c\`.

"Extra" options are now in a separate panel under line art.

I'm doing fixes to format and comments.

Blender now refuse to start :( it says following and crashed:

`Fatal Python error: initfsencoding: Unable to get the locale encoding`
`ModuleNotFoundError: No module named 'encodings'`

### 1031

I'm now preparing the patch for review. Fixed a bunch of coding style
stuff from the comments, including typo, comment styles, UI clean-ups.

[Next Log](LineArt_Development_Log_2020_11.md)
