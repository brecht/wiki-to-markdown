## GSoC2018 LANPR Development: Explained

Hi everyone\! LANPR is progressing relatively fluent these days, and
some people have already tested some pre-alpha release of this LANPR
branch. But the tasks seems a little unclear since I haven't quite
explained how the development steps are, this includes what shall be
implemented and how much time will be spent on each of them. Guess it is
time for me to put up this development task list here on blender wiki.
In this page You will see the planned steps, substeps, references as
well as development progress.

### What are the main tasks?

this GSoC project is focused on giving Blender2.8 the ability to
genderate NPR line render just like Freestyle did in the 2.7x. This
time, two options are proposed: GPU realtime render and CPU offline
render.

To better adapt the development target of Blender2.8 as well as feedback
from the users, GPU option is developed first, and most of its main
functions are basically implemented now (optimization maybe later).

The next step is to achieve the CPU side which is more stable, accurate,
and different styles can be adjusted than on GPU. But to achieve this,
some support data structures will have to be done first. Below are the
detailed steps. (and what has been done now)

#### Main Algorithms

  - **\[OK\]** CPU: Edge detector CPU trace and vectorization drawing.
      - **\[need attention\]** Traced line smoothing undone.
      - **\[need attention\]** Sobel result thinning does not quite good
        in some conditions.
  - **\[OK\]** DPIX algorithm implementation.
      - **\[need attention\]** Buffer size is currently hard-coded, will
        have to be dynamic.
      - **\[need attention\]** DPIX vector occlution test not
        implemented yet (has performance impact. Current results are
        good enough for viewport depth test, if there's further need, it
        will then be implemented).
  - **\[ \]** CPU computation implementation.
      - **\[WORKING\]** Intersection line computation. (the result can
        be used at both GPU and CPU.)
      - **\[ \]** Feature line computation. (this can use DPIX result,
        save a lot of time).
      - **\[ \]** Accurate feature line extraction and preventing jaggy
        / incorrect results.
      - **\[ \]** Occlution test. (multithread)
  - **\[ \]** Line layer and style system.
      - **\[OK\]** Depth controlled line style.
      - **\[ \]** Light controlled line style: will have to wait until
        the new line layer design come out.
      - **\[ \]** Material, Group, Collection controlled line layer
        selection.
      - **\[ \]** Layer selection and composition.

#### Support Structures

  - **\[OK\]** GPU viewport drawing code, adapt the shaders.
  - **\[WORKING\]** Render to image support.
  - **\[OK\]** UI basic controls.
  - **\[WORKING\]** struct LANPR\_SoftwareRender design.
  - **\[WORKING\]** struct LANPR\_LineLayer design. (To match blender
    2.8's new collection structure.)
      - **\[ \]** UI for line layer composition.
  - **\[ \]** Multithread.
  - **\[ \]** Support for modifier, shape keys, armatures and even
    particle system(maybe later).

### FAQ

Is intersection calculation hard?

Well, it certainlly isn't easy. When calculate the intersections using a
geometric approach, it's really a heavy task for CPU, just imagine you
bool-intersect every objects in the scene with each other.
Multithreading is quite hard for this stage also, but if you have enough
memory, there's certainlly some solution.

At least it should work.

### Performance / Hardware requirements?

DPIX algorithm use large texture buffers as its data inputs and outputs,
and perform complicated calculation in opengl shaders, then when your
scene is large, it is expected that you should have plenty of GPU memory
on your card. One large scene with test proformed on my side showed that
you need some where near 3GB of VRAM available to run a 30milion verts
scene. But small scenes should not use as much VRAM as you may not
notice that. If you have a fast enough card, you can run it more
fluently.

For CPU computation, the more threads you have, the more memory you
have, the faster it can get. Freestyle will eat up a lot of memery in
your machine, LANPR wouldn't decrease this amount very much (20% to 30%
is possible though), but with multithreading, it will be a lot faster
than Freestyle.

### Why sometimes developing progress are slow?

When implementing lower level data structure, it could take some time
and there's no immediate visible results, but it is important.

I'm still in school and some matters I will have to attend to, I will
keep you posted when something unexpected happens.
