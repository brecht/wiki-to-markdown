## GSoC Week5 Status

YimingWu

( I just saw this new wiki, will migrate the old pages of mine here
soon:) )

### Things have done

Last week's plan is almost exactly finished, except some are put off,
will then implement them when the line style layering system is designed
and implemented.

1.  Last week's dpix shader has some weird behavior with non-uniform
    object scale, Fixed now.
2.  Multisample display achieved. Seems no need for user selected
    multisample because all our tests seems to produce fluent framerate
    at sample==8. will add this feature if there's extensive
    requirements.
3.  Camera distance controlled line style implemented. Light direction
    contolled one need further design. (e.g. select light object or
    input a vector or something...)
4.  Material seperate lines and edge marks ok.
5.  Made render\_to\_image work, but don't produce result. probably have
    something to do with multisample.

#### Screenshots\!

![../../../images/LANPR\_Week5\_1.png](../../../images/LANPR_Week5_1.png
"../../../images/LANPR_Week5_1.png")

### Next week

1.  I have gathered some suggestions from various places. I will list
    the major requests from the users, and see what can be implemented
    first.
2.  For coding, Many people have asked for intersection line rendering,
    and it is also one major feature in LANPR. I think I will then adapt
    the data structure to achive intersection computation. (and because
    future works also dependes on it)

<!-- end list -->

  -   - One thing about accurate intersection calculation is that it
        should be pre-calculated on cpu side, and then we can send the
        cached intersection lines to GPU. so when scene have objet/mesh
        updates then the intersection cache will be re-calculated. This
        is a time consuming work.
      - But I still remember there's a paper on GPU intersection
        calculation but can't find it anywhere. If there's anyone
        happened to be familiar with it please let me know. Thanks

<!-- end list -->

1.  Document some code usages.
2.  It seems that dot-dash/ wiggle line style are not quite the interest
    of the user, so I think I will just focus on LineStyle layer deisgn
    and adapting intersection code next week.

### Questions

I have listed questions (and some answers) here in a seperate wiki page.
\[Oh, no wait\!.....\]
