# Weekly Reports 2023

## 0501-0507

First week of the work period\! :D Mostly just triaging stuff.

Triaged:
[\#107514](https://projects.blender.org/blender/blender/issues/107514),
[\#107494](https://projects.blender.org/blender/blender/issues/107494),
[\#107489](https://projects.blender.org/blender/blender/issues/107489),
[\#107474](https://projects.blender.org/blender/blender/issues/107474),
and helped a few other ones to confirm the behaviour.

Merged fixes: [PR
\#107529](https://projects.blender.org/blender/blender/pulls/107529),
[PR
\#107575](https://projects.blender.org/blender/blender/pulls/107575).

Suggested features:
[\#107493](https://projects.blender.org/blender/blender/issues/107493)

## 0508-0514

Fixed:
[\#107717](https://projects.blender.org/blender/blender/issues/107717),
[PR
\#107864](https://projects.blender.org/blender/blender/pulls/107864),
with a few pending approval:
[\#107789](https://projects.blender.org/blender/blender/issues/107789),
[\#107768](https://projects.blender.org/blender/blender/issues/107768),
<s>[PR
\#107865](https://projects.blender.org/blender/blender/pulls/107865)</s>
(This does not seem needed as it's a file problem)

Triaged:
[\#107810](https://projects.blender.org/blender/blender/issues/107810),
[\#107870](https://projects.blender.org/blender/blender/issues/107870)

Additionally, tried to make some generic node to get familiar with the
node API to be able to port line art to it. Seems pretty doable at this
stage, although expectedly there will be a lot of sockets.

## 0515-0521

Pending: [PR
\#107937](https://projects.blender.org/blender/blender/pulls/107937) [PR
\#108004](https://projects.blender.org/blender/blender/pulls/108004)

Triaged/Involved:
[\#108034](https://projects.blender.org/blender/blender/issues/108034)
[\#108036](https://projects.blender.org/blender/blender/issues/108036)
[\#108033](https://projects.blender.org/blender/blender/issues/108033)
[\#107988](https://projects.blender.org/blender/blender/issues/107988)
[\#107856](https://projects.blender.org/blender/blender/issues/107856)
[\#107969](https://projects.blender.org/blender/blender/issues/107969)
[\#107996](https://projects.blender.org/blender/blender/issues/107996)
[\#108040](https://projects.blender.org/blender/blender/issues/108040)
[\#108005](https://projects.blender.org/blender/blender/issues/108005)

Reviewed: [PR
\#104450](https://projects.blender.org/blender/blender/pulls/104450)

<s>Needs follow up:
[\#107925](https://projects.blender.org/blender/blender/issues/107925)</s>

Was in the process of moving from school to home from 19th... Still a
few things left need to take care but I think I can get it sorted out
soon.

## 0522-0528

Pending: [PR
\#108257](https://projects.blender.org/blender/blender/pulls/108257) [PR
\#108217](https://projects.blender.org/blender/blender/pulls/108217)

Triaged/Involved:
[\#108229](https://projects.blender.org/blender/blender/issues/108229)
[\#108098](https://projects.blender.org/blender/blender/issues/108098)
[\#108161](https://projects.blender.org/blender/blender/issues/108161)
[\#107956](https://projects.blender.org/blender/blender/issues/107956)

Merged fixes: [PR
\#108172](https://projects.blender.org/blender/blender/pulls/108172)

[\#108113](https://projects.blender.org/blender/blender/issues/108113)
is about weird behavior with Wacom Cintiq 27 Pro (a very new piece of
hardware atm).

[\#108162](https://projects.blender.org/blender/blender/issues/108162)
indicates that using \`WiredXDisplay from Splashtop\` to use iPad as a
display would cause display issue.

## 0529-0604

Merged Fixes: [PR
\#108217](https://projects.blender.org/blender/blender/pulls/108217)

Triaged/Involved:
[\#108229](https://projects.blender.org/blender/blender/issues/108229)
[\#108244](https://projects.blender.org/blender/blender/issues/108244)
[\#108267](https://projects.blender.org/blender/blender/issues/108267)
[\#108385](https://projects.blender.org/blender/blender/issues/108385)
[\#108409](https://projects.blender.org/blender/blender/issues/108409)
[\#108536](https://projects.blender.org/blender/blender/issues/108536)
[\#108553](https://projects.blender.org/blender/blender/issues/108553)

Others stuff:

Merged a PR for migrating the monkey from old GP to new GP [PR
\#108503](https://projects.blender.org/blender/blender/pulls/108503)

## 0605-0611

Triaged/Involved:
[\#108684](https://projects.blender.org/blender/blender/issues/108684)
[\#108732](https://projects.blender.org/blender/blender/issues/108732)
[\#108736](https://projects.blender.org/blender/blender/issues/108736)
[\#108737](https://projects.blender.org/blender/blender/issues/108737)
[\#\#108756](https://projects.blender.org/blender/blender/issues/#108756)
[\#\#108783](https://projects.blender.org/blender/blender/issues/#108783)
[\#\#108788](https://projects.blender.org/blender/blender/issues/#108788)
[\#108833](https://projects.blender.org/blender/blender/issues/108833)
[\#108845](https://projects.blender.org/blender/blender/issues/108845)
[\#108855](https://projects.blender.org/blender/blender/issues/108855)
[\#108866](https://projects.blender.org/blender/blender/issues/108866)
[\#108875](https://projects.blender.org/blender/blender/issues/108875)

Merged Fixes: [PR
\#107803](https://projects.blender.org/blender/blender/pulls/107803) [PR
\#108257](https://projects.blender.org/blender/blender/pulls/108257) [PR
\#108592](https://projects.blender.org/blender/blender/pulls/108592)

## 0612-0618

This week have been a bit slower for some life stuff.

Triaged/Involved:

[\#109004](https://projects.blender.org/blender/blender/issues/109004)
[\#109011](https://projects.blender.org/blender/blender/issues/109011):
some interface weirdness needs follow up.

Closed but better have a follow up:
[\#108860](https://projects.blender.org/blender/blender/issues/108860)
Subdiv "Create n-gon" option may need a better description.

[\#109111](https://projects.blender.org/blender/blender/issues/109111)
Gizmo drawing bug needs a follow up.

Also
[\#109105](https://projects.blender.org/blender/blender/issues/109105)
[\#109100](https://projects.blender.org/blender/blender/issues/109100)
[\#109081](https://projects.blender.org/blender/blender/issues/109081)
[\#109004](https://projects.blender.org/blender/blender/issues/109004)
[\#108866](https://projects.blender.org/blender/blender/issues/108866)
[\#109074](https://projects.blender.org/blender/blender/issues/109074)
[\#109066](https://projects.blender.org/blender/blender/issues/109066)

## 0619-0625

Triaged:
[\#109115](https://projects.blender.org/blender/blender/issues/109115)
[\#109114](https://projects.blender.org/blender/blender/issues/109114)
[\#109175](https://projects.blender.org/blender/blender/issues/109175)
[\#109194](https://projects.blender.org/blender/blender/issues/109194)
[\#109344](https://projects.blender.org/blender/blender/issues/109344)

Notes:

[\#109127](https://projects.blender.org/blender/blender/issues/109127) &
[\#109168](https://projects.blender.org/blender/blender/issues/109168)
For frame change handler instability.

[\#109178](https://projects.blender.org/blender/blender/issues/109178)
Selection count -1 problem in pose mode.

[\#109275](https://projects.blender.org/blender/blender/issues/109275)
Bone symmetry constraint value going haywire (needs follow up, probably
with matrix).

[\#109313](https://projects.blender.org/blender/blender/issues/109313) &
[\#109327](https://projects.blender.org/blender/blender/issues/109327)
Preservation of shape key and \`bpy.data.meshes.new\_from\_object()\`
inconsistency.

## 0626-0702

Made two meta-task for listing bugs of similar cause for ease of looking
up:

  - HIP related bugs:
    [\#109431](https://projects.blender.org/blender/blender/issues/109431)
  - Retopo Overlay & Z-Bias:
    [\#109545](https://projects.blender.org/blender/blender/issues/109545)

Not sure:
[\#109372](https://projects.blender.org/blender/blender/issues/109372)
Selecting camera cause scene with line art to crash (why?)

[\#109484](https://projects.blender.org/blender/blender/issues/109484)
Alt click conflict with emulate 3 btn.

[\#109524](https://projects.blender.org/blender/blender/issues/109524)
\`pip\` bpy module vse doesn't render sound. (This is due to bpy module
has sound disabled)

[\#109506](https://projects.blender.org/blender/blender/issues/109506)
linux virtual input device middle click issue. (Needs to send from
keyboard device/switch to another device)

[\#109551](https://projects.blender.org/blender/blender/issues/109551)
calling dll function from python needs to have lib version exactly
match.

Others:
[\#109346](https://projects.blender.org/blender/blender/issues/109346)
[\#109349](https://projects.blender.org/blender/blender/issues/109349)
[\#109350](https://projects.blender.org/blender/blender/issues/109350)
[\#109457](https://projects.blender.org/blender/blender/issues/109457)
[\#109506](https://projects.blender.org/blender/blender/issues/109506)
[\#109538](https://projects.blender.org/blender/blender/issues/109538)

Note: Since 3.6 release, a few weird graphics issues under linux have
been reported, <s>and I suspect they have something to do with
\`flatpak\`.</s> undetermined cause.

## 0703-0709

Issues worth noting:

[\#109676](https://projects.blender.org/blender/blender/issues/109676)
Frame update handler crash.

[\#109679](https://projects.blender.org/blender/blender/issues/109679)
Normal map strength scaling issue.

[\#109720](https://projects.blender.org/blender/blender/issues/109720)
Driver property initialization problem (\`@persistent\` callback).

[\#109765](https://projects.blender.org/blender/blender/issues/109765)
Dissolve edge removing vertices that are not previously selected.

[\#109780](https://projects.blender.org/blender/blender/issues/109780)
Seam/Mark edge overlay not showing on some intel graphics.

[\#109837](https://projects.blender.org/blender/blender/issues/109837)
In-place expression evaluation ignoring separated unit strings.

Other involved:

[\#109561](https://projects.blender.org/blender/blender/issues/109561)
[\#109560](https://projects.blender.org/blender/blender/issues/109560)
[\#109650](https://projects.blender.org/blender/blender/issues/109650)
[\#109651](https://projects.blender.org/blender/blender/issues/109651)
[\#109673](https://projects.blender.org/blender/blender/issues/109673)
[\#109749](https://projects.blender.org/blender/blender/issues/109749)
[\#109802](https://projects.blender.org/blender/blender/issues/109802)

## 0710-0716

I went to Beijing at 12th so a bit less work for this week... However we
had a meet up with Pablo Vazquez and it was super cool\!

Triaged/Involved:

[\#109932](https://projects.blender.org/blender/blender/issues/109932)
[\#109962](https://projects.blender.org/blender/blender/issues/109962)
[\#109985](https://projects.blender.org/blender/blender/issues/109985)
[\#109992](https://projects.blender.org/blender/blender/issues/109992)
[\#110031](https://projects.blender.org/blender/blender/issues/110031)

[\#109995](https://projects.blender.org/blender/blender/issues/109995)
Manually subscribe to changes (unresolved, not quite familiar with bpy).

[\#110129](https://projects.blender.org/blender/blender/issues/110129)
Undo node changes in edit mode.

UNRESOLVED:
[\#110038](https://projects.blender.org/blender/blender/issues/110038)
Regression: Crash when Sculpting on a mesh from a GP Lineart Modifier.

## 0717-0723

I have two merged fixes this week\! [PR
\#110188](https://projects.blender.org/blender/blender/pulls/110188) [PR
\#110191](https://projects.blender.org/blender/blender/pulls/110191)

Merge pending: [PR
\#110287](https://projects.blender.org/blender/blender/pulls/110287)
Sequencer camera override for line art.

Needs follow-up:

  - [\#110160](https://projects.blender.org/blender/blender/issues/110160)
    Simulation should reset when fps changes.
  - [\#110167](https://projects.blender.org/blender/blender/issues/110167)
    3D cursor doesn't precisely follow mouse pointer when Transform
    Pivot Point is \`Active Element\`.
  - [\#110303](https://projects.blender.org/blender/blender/issues/110303)
    [\#110228](https://projects.blender.org/blender/blender/issues/110228)
    Huion tablet focus and pressure issue.

Triaged/Fixed:
[\#110128](https://projects.blender.org/blender/blender/issues/110128)
[\#110155](https://projects.blender.org/blender/blender/issues/110155)
[\#110157](https://projects.blender.org/blender/blender/issues/110157)
[\#110161](https://projects.blender.org/blender/blender/issues/110161)
[\#110176](https://projects.blender.org/blender/blender/issues/110176)
[\#110207](https://projects.blender.org/blender/blender/issues/110207)
[\#110208](https://projects.blender.org/blender/blender/issues/110208)
[\#110232](https://projects.blender.org/blender/blender/issues/110232)(User
Remap)
[\#110253](https://projects.blender.org/blender/blender/issues/110253)
[\#110261](https://projects.blender.org/blender/blender/issues/110261)
[\#110278](https://projects.blender.org/blender/blender/issues/110278)
[\#110281](https://projects.blender.org/blender/blender/issues/110281)(De-spilling)
[\#110328](https://projects.blender.org/blender/blender/issues/110328)
[\#110331](https://projects.blender.org/blender/blender/issues/110331)

## 0724-0730

Needs follow up:

  - [\#110452](https://projects.blender.org/blender/blender/issues/110452)
    Add cylinder panel should show scale if added through viewport
    gizmo.
  - <s>[\#110524](https://projects.blender.org/blender/blender/issues/110524)
    Support very large uv image export, (With pending patches [PR
    \#110630](https://projects.blender.org/blender/blender/pulls/110630)
    and [104800 in uv
    export](https://projects.blender.org/blender/blender-addons/pulls/104800))
    might need a different approach, by simply using \`size\_t\`, and
    let user deal with their GPU masx texture size.</s> In hindsight
    this might not be a good idea, since very few video cards support
    massive texture sizes, and svg export option is also available.
  - [\#110530](https://projects.blender.org/blender/blender/issues/110530)
    Confirm button on remote access \_and\_ reversed L/R mouse button
    settings.
  - Might need to update the patch for
    [\#110616](https://projects.blender.org/blender/blender/issues/110616)
    sculpting pivot.

Involved:
[\#110388](https://projects.blender.org/blender/blender/issues/110388)
[\#110390](https://projects.blender.org/blender/blender/issues/110390)
[\#110411](https://projects.blender.org/blender/blender/issues/110411)
[\#110419](https://projects.blender.org/blender/blender/issues/110419)
[\#110454](https://projects.blender.org/blender/blender/issues/110454)
[\#110464](https://projects.blender.org/blender/blender/issues/110464)
[\#110466](https://projects.blender.org/blender/blender/issues/110466)
[\#110494](https://projects.blender.org/blender/blender/issues/110494)
[\#110503](https://projects.blender.org/blender/blender/issues/110503)
[\#110516](https://projects.blender.org/blender/blender/issues/110516)
[\#110601](https://projects.blender.org/blender/blender/issues/110601):
Make line tool
[\#110632](https://projects.blender.org/blender/blender/issues/110632)

## 0731-0806

Needs follow-ups:

  - [\#110635](https://projects.blender.org/blender/blender/issues/110635)
    VSE strip color "multiply" will also multiply alpha. (If the
    original reporter doesn't provide a patch then we could maybe make
    one)
  - [\#82006](https://projects.blender.org/blender/blender/issues/82006)
    A bunch of trackpad support patches that are made by Yevgeny Makarov
    that haven't yet merged, needs to create new PR for gitea.
  - [\#110794](https://projects.blender.org/blender/blender/issues/110794)
    Clay strip brush delay on stroke start, need to pin point which
    commit.

I tried the new \`/tools/triage/weekly\_report.py\` this time and see if
it can spit out something interesting:

**Involved in 41 reports:**

  - Confirmed: 5
  - Closed as Resolved: 0
  - Closed as Archived: 4
  - Closed as Duplicate: 0
  - Needs Info from User: 6
  - Needs Info from Developers: 2
  - Actions total: 48

**Review: 0**

**Created pulls: 2**

  - [PR
    \#110844](https://projects.blender.org/blender/blender/pulls/110844):
    Fix \#110834: UI: Prevent flickering when scrolling popovers.
  - [PR
    \#110780](https://projects.blender.org/blender/blender/pulls/110780):
    Fix \#110766: Prevent panning at limit with zoom-to-cursor

## 0807-0813

This weekly report generator thingy is pretty convenient :D

**Involved in 42 reports:**

  - Confirmed: 15
  - Closed as Resolved: 1
  - Closed as Archived: 7
  - Closed as Duplicate: 0
  - Needs Info from User: 12
  - Needs Info from Developers: 2
  - Actions total: 51

**Review: 0**

**Created pulls: 5**

  - [PR
    \#110930](https://projects.blender.org/blender/blender/pulls/110930):
    Fix \#110848: Trackpad: Allow changing the speed while fly
    navigating
  - [PR
    \#110940](https://projects.blender.org/blender/blender/pulls/110940):
    Fix \#110917: Multiply \`UI\_SCALE\_FAC\` to view scroll operators
  - [PR
    \#110927](https://projects.blender.org/blender/blender/pulls/110927):
    Fix \#110833: Trackpad: Text Editor scrolling improvement
  - [PR
    \#104832](https://projects.blender.org/blender/blender-addons/pulls/104832):
    Fix \#104723: Freestyle SVG output ensure path exists
  - [PR
    \#110928](https://projects.blender.org/blender/blender/pulls/110928):
    Fix \#110853: Trackpad: support for
    'ui\_colorpicker\_small\_wheel\_cb'

**Commits:**

  - Fix
    [\#104723](https://projects.blender.org/blender/blender-addons/issues/104723):
    Freestyle SVG output ensure path exists
    ([35824c38e3](https://projects.blender.org/blender/blender-addons/commit/35824c38e3))
  - Fix
    [\#110917](https://projects.blender.org/blender/blender/issues/110917):
    Multiply \`UI\_SCALE\_FAC\` to view scroll operators
    ([337c47275c](https://projects.blender.org/blender/blender/commit/337c47275c))

## 0814-0820

**Involved in 61 reports:**

  - Confirmed: 9
  - Closed as Resolved: 0
  - Closed as Archived: 8
  - Closed as Duplicate: 0
  - Needs Info from User: 9
  - Needs Info from Developers: 9
  - Actions total: 71

**Review: 3**

  - [PR
    \#111157](https://projects.blender.org/blender/blender/pulls/111157):
    Fix \#111156: Principled transmission not rendering in OSL
  - [PR
    \#111168](https://projects.blender.org/blender/blender/pulls/111168):
    Fix \#111017: Gap between color square and frame of box in the color
    ramp
  - [PR
    \#108399](https://projects.blender.org/blender/blender/pulls/108399):
    Fix \#101877, rigidbodies & constraints causing frequent crashes.

**Created pulls: 1**

  - [PR
    \#111141](https://projects.blender.org/blender/blender/pulls/111141):
    Fix \#89906: Consistent track pad zoom directions

## 0814-0827

**Involved in 60 reports:**

  - Confirmed: 6
  - Closed as Resolved: 0
  - Closed as Archived: 14
  - Closed as Duplicate: 2
  - Needs Info from User: 12
  - Needs Info from Developers: 2
  - Actions total: 76

**Review: 3**

  - [PR
    \#111359](https://projects.blender.org/blender/blender/pulls/111359):
    Fix \#111295: Add Missing Win32 Platform-Specific Window functions
  - [PR
    \#110605](https://projects.blender.org/blender/blender/pulls/110605):
    Fix \#110161: Crash on dragging a Speaker NLA strip
  - [PR
    \#111168](https://projects.blender.org/blender/blender/pulls/111168):
    Fix \#111017: Gap between color square and frame of box in the color
    ramp

**Created pulls: 1**

  - [PR
    \#111424](https://projects.blender.org/blender/blender/pulls/111424):
    WIP: Disabled OneAPI for testing HD4400/4600 on linux.

## 0828-0903

**Involved in 38 reports:**

  - Confirmed: 3
  - Closed as Resolved: 2
  - Closed as Archived: 9
  - Closed as Duplicate: 0
  - Needs Info from User: 11
  - Needs Info from Developers: 3
  - Actions total: 46

**Review: 1**

  - [PR
    \#111725](https://projects.blender.org/blender/blender/pulls/111725):
    Outliner: listen to collection change in the header region

**Created pulls: 2**

  - [PR
    \#111602](https://projects.blender.org/blender/blender/pulls/111602):
    Fix \#111601: VSE strip modifier copy ensure unique name
  - [PR
    \#111615](https://projects.blender.org/blender/blender/pulls/111615):
    Fix \#111607: Do not unregister internal nodes

**Commits:**

  - Fix
    [\#111601](https://projects.blender.org/blender/blender/issues/111601):
    VSE strip modifier copy ensure unique name
    ([02406e17a0](https://projects.blender.org/blender/blender/commit/02406e17a0))
  - Fix
    [\#111607](https://projects.blender.org/blender/blender/issues/111607):
    Do not unregister internal nodes
    ([0798aae01c](https://projects.blender.org/blender/blender/commit/0798aae01c))
  - PyAPI: Better error msg when re-using internal node id
    ([f662d8acb0](https://projects.blender.org/blender/blender/commit/f662d8acb0))

## 0904-0910

**Involved in 55 reports:**

  - Confirmed: 16
  - Closed as Resolved: 1
  - Closed as Archived: 9
  - Closed as Duplicate: 1
  - Needs Info from User: 12
  - Needs Info from Developers: 11
  - Actions total: 64

**Review: 1**

  - [PR
    \#112004](https://projects.blender.org/blender/blender/pulls/112004):
    Fix: Inconsistent input drag editing for factor properties

**Created pulls: 2**

  - [PR
    \#112121](https://projects.blender.org/blender/blender/pulls/112121):
    Fix \#112083: Loop cut requires GL only in interactive mode
  - [PR
    \#111915](https://projects.blender.org/blender/blender/pulls/111915):
    Fix \#111820: Missing type casting in XrGraphicsBinding.cc

**Commits:**

  - Fix
    [\#111820](https://projects.blender.org/blender/blender/issues/111820):
    Missing type casting in XrGraphicsBinding.cc
    ([bf8d3c157a](https://projects.blender.org/blender/blender/commit/bf8d3c157a))

## 0911-0917

*Involved in 22 reports:*'

  - Confirmed: 8
  - Closed as Resolved: 1
  - Closed as Archived: 3
  - Closed as Duplicate: 0
  - Needs Info from User: 4
  - Needs Info from Developers: 3
  - Actions total: 26

**Review: 0**

**Created pulls: 1**

  - [PR
    \#112244](https://projects.blender.org/blender/blender/pulls/112244):
    Fix \#112218: Do not require screen in \`object\_join\_poll\`

**Commits:**

  - Fix
    [\#112218](https://projects.blender.org/blender/blender/issues/112218):
    Do not require screen in \`object\_join\_poll\`
    ([44e245f4f0](https://projects.blender.org/blender/blender/commit/44e245f4f0))

## 0918-0924

**Involved in 44 reports:**

  - Confirmed: 9
  - Closed as Resolved: 3
  - Closed as Archived: 9
  - Closed as Duplicate: 0
  - Needs Info from User: 9
  - Needs Info from Developers: 3
  - Actions total: 51

**Review: 1**

  - [PR
    \#112652](https://projects.blender.org/blender/blender/pulls/112652):
    Fix \#112630: Skip grave quotation when selecting

**Created pulls: 2**

  - [PR
    \#112609](https://projects.blender.org/blender/blender/pulls/112609):
    Fix \#112604: Add "tag" to the tool tip of asset search
  - [PR
    \#112626](https://projects.blender.org/blender/blender/pulls/112626):
    Fix \#112622: Give default color for sockets without
    \`draw\_color\_simple\`

**Commits:**

  - Fix
    [\#112083](https://projects.blender.org/blender/blender/issues/112083):
    Loop cut requires GL only in interactive mode
    ([34877ec38b](https://projects.blender.org/blender/blender/commit/34877ec38b))
  - Fix
    [\#100596](https://projects.blender.org/blender/blender/issues/100596):
    Use sequencer override camera for line art in render
    ([954ae19b2b](https://projects.blender.org/blender/blender/commit/954ae19b2b))
  - Fix
    [\#112604](https://projects.blender.org/blender/blender/issues/112604):
    Add "tag" to the tool tip of asset search
    ([4720fda1a7](https://projects.blender.org/blender/blender/commit/4720fda1a7))
  - Cleanup: Use \`intern/render\_types.h\` for lineart\_cpu.c
    ([f6082a5042](https://projects.blender.org/blender/blender/commit/f6082a5042))

## 0924-1008

Typical work stuff until 0928, 0929-1007 is the mid-autumn festival and
Chinese national day holiday for day off.

## 1009-1015

**Involved in 24 reports:**

  - Confirmed: 0
  - Closed as Resolved: 0
  - Closed as Archived: 0
  - Closed as Duplicate: 0
  - Needs Info from User: 0
  - Needs Info from Developers: 0
  - Actions total: 28

**Review: 0**

**Created pulls: 0**

**Commits:**

Debug: Activities from 2023-10-09T00:42:28.859038 to
2023-10-15T00:42:28.859038:

Pulls Created: Pulls Reviewed: Issues Confirmed: Issues Closed as
Resolved: Issues Closed as Archived:
<https://projects.blender.org/blender/blender/issues/113616>
<https://projects.blender.org/blender/blender/issues/112323>
<https://projects.blender.org/blender/blender/issues/113436>
<https://projects.blender.org/blender/blender/issues/113435> Issues
Closed as Duplicate: Issues Needing Info from User: Issues Needing Info
from Developers:
