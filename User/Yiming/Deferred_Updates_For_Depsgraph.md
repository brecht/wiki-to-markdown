# Deferred Updates For Depsgraph

This is mainly to accomplish background evaluation of nodes that is slow
to run. A simple first goal would be the evaluation of certain modifiers
in the background thread.

In this page I'll just lay out some notes while I going through the
depsgraph code.

-----

\`deg\_evaluate\_on\_refresh\` is the entry point. It's a
\`work\_and\_wait\` kind of update. For background evaluation we just
need to put some of the nodes into yet another thread, when it's done we
call for a refresh. We can have current evaluation APIs behave as they
used to be (which is not background), and give one or two new functions
that allow non-blocking calls.

Modifier evaluation is eventually linked to depsgraph through
\`build\_object\_data\_geometry()\`, which adds an operation node for
later calling \`BKE\_object\_eval\_uber\_data()\` on a cow object: (so
done with cpp pointer)

<code>

` void DepsgraphNodeBuilder::build_object_data_geometry(Object *object, bool is_object_visible)`  
` {`  
` ...`  
` /* Geometry evaluation. */`  
` op_node = add_operation_node(`  
`     &object->id,`  
`     NodeType::GEOMETRY,`  
`     OperationCode::GEOMETRY_EVAL,`  
`     function_bind(BKE_object_eval_uber_data, _1, scene_cow, object_cow));`  
` ...`  
` }`

</code>

Modifier is currently evaluated in \`BKE\_object\_eval\_uber\_data()\`.
In order to let the modifier have access to \`Job\` or anything like
that, we need a separate *type* of node that is somehow calculated
outside the main loop, but all the nodes who depends on this background
node will also be waiting until this node is done, at which point the
rest would continue being evaluated.
