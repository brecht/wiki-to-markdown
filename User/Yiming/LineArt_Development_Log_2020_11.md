# LineArt Development Log Continued (2020/11)

[Previous Log](LineArt_Development_Log_2020_10.md)

### 1102

Support edit mode line preview. Just use the \`edimesh\`.

### 1104

Further code format clean-ups, including using \`LISTBASE\_FOREACH\`
stuff for iterations.

Fixed compiler warnings for unused variables and incompatible types and
stuff.

### 1105

Chain occlusion info was missing for the last segment of the line,
result in erroneous chaining result in later stages, now fixed.

Do not restrict GPencil object selectable property, user can
enable/disable select on their own demand.

Added "Scene Line Art" option, which the modifier will select everything
in the scene.

### 1107

Vertex weight transfer appears to be problematic, this is due to the use
of global index instead of local index, now fixed, but added extra index
fields in \`LineartRenderLine\`, so it will use a little bit extra
memory.

### 1109

Transparency mask is lost during angle splitting process, now fixed.

Other UI optimizations.

### 1110

Reduced default chaining thresholds to 0.001, this eliminates erroneous
chaining behaviour, in which lines could jump across a long distance to
connect to other ones.

### 1111

**It seems that current evaluation path for line art isn't quite
preferable in master. To achieve background modifier evaluation, We need
some other changes in depsgraph, so if we want to get line art into
master, the best way right now is do blocking updates for each line art
modifier, which is slow, but that way we don't have architectural
problems.**

<s>I'm creating a separate design task for this, anyone interested could
check it out: <https://developer.blender.org/T82605></s> (Doesn't really
align with blender's architectural philosophy either, which means we
will probably require all stuff except render to be near real-time.)

So what I'm gonna do is that I should make line art fully self-contained
in the following day. By doing this, we will:

1.  lose the ability to cancel the calculation,
2.  the interface will completely lock-up during line art calculation,
3.  and there will likely be multiple line art evaluations if you have
    multiple line art modifiers which is again very slow.
4.  and we are probably gonna lose the ability to bake everything in one
    go.

But this will be the way that best align with Blender's current
infrastructure. We can start doing optimizations from there.

It's likely that I'll do the change in another branch. More info to
come.2

### 1112

<s>Moving code</s> Done. Now the "Pure modifier" code is in
\`temp-lineart-contained\` branch. \`lineart-under-gp\` branch is still
merging with latest master.

I'm now cleaning up the redundant code for the next review. Todo:

  - \[Done\] <s>\`lineart\_share\` needs to go into
    \`LineArtGpencilModifierData\`.</s>
  - function naming format changes as we are not in \`editors/\`(?)

### 1113

  - There are some now-modifier properties whose UI should be made
    visible.
