## LANPR: Next Steps

Bugs, pending features and improvements are mostly listed in GSoC2018
Conclusion developer reference page.

### Production Ready?

So we should be able to use viewport composition anytime now, if that is
the case, then the composition part for production is ready. Now we can
just render the scene with cy/eevee to get the color pass, then use
LANPR to produce line images, and finally composite the two together.
This composition process still contains too many steps for an simple
output if there's no composition nodes already present in every project.

Currently, the graphical user interface of LANPR itself isn't quite
efficient for a simple render, it's just too many steps. Actually, LANPR
is designed in a way that user won't have to recalculate everything
every time they change a tiny argument. So this kind of design helps
when this is the case. but in many cases we just want to click LANPR and
render a simple black line image out of it. So in the future there is
going to be a more Freestyle-like way of triggering the whole render.
And, we will need some presents available in LANPR, because the default
style indeed isn't very good looking in most cases...

However, image quality and calculation speed optimization is of higher
priority. LANPR is now much quicker than Freestyle in most of the cases,
to achieve such a efficiency, LANPR internally adapted some algorithms
that are not quite optimized for accuracy, and in extreme cases some
errors are expected. Although most of the algorithms are already
optimized to allow better result within floating point limitations, but
there's still work to do, and I believe it will be quick. Do images
produced by LANPR meet the A/V production standard? Sometimes they do,
sometimes they don't. For example, chaining algorithm will produce sharp
points around some turns, and clipping isn't accurate... things like
that will all be fixed one by one during future developments. Compared
to implementing big "features", these kind of "bugs" are generally
easier to fix.

LANPR's DPIX mode shows some potentials that it can be used in fields
like real-time structural preview, or games. However currently DPIX mode
is limited in Blender's viewport, and relies heavily on Blender's
internal data structures(easy to peel off though...), now with BGE
fading, to use this feature outside Blender is not easy. However, I plan
to release the code for my standalone program, including the whole UI
system and LANPR itself on to Internet some time later this year, then
it can be checked out as a reference.

### Some More Thoughts

Snake mode is in a awkward position now. It isn't fast enough to beat
DPIX, and not qualitied enough to beat the other two modes. The only
advantage it has is that inter-frame continuum can be easily implemented
to this mode later on (which somehow doesn't seem to be a requirement,
and it is progressive). So in many cases, snake doesn't have advantage
over any other modes in LANPR. Thus, I plan to make this mode into
something that deal with fills rather than lines, for example: cartoon
styled highlights, hatching, halftone, or even watercolor. Because Snake
mode uses an image-space calculation, it is easy to expand, shrink or
modify the shape of areas that is going to be shaded, this can not be
achieved using only material nodes.

So for a long time the NPR community is requiring SVG output (strangely
enough, there's just no such requirement in Chinese community……). The
reason for it hasn't been achieved are two: The first is before
implementing the Chaining algorithm, everything that LANPR ever produce
is multiple segments of short lines. The second is because there haven't
been a good way to represent line style in SVG. For example, should we
use line stroke or quad strip? Or how to represent a gradient color
along a curve? Or even how to treat lines from different objects? From
the information I've known, there are different treatments on the SVG,
some apply them with noise, some actually transform the outlines.
Exactly how to write this SVG file still need some considerations.

There might be a customizable shader or node tree system for LANPR in
the future.

![Pencil+'s Demonstration](../../images/PerspectiveDeform_2.jpg
"Pencil+'s Demonstration")

Another very interesting topic is non-linear perspective distortions
like Pencil+ has, or in another way of saying, is multiple z-frustum
composition. Just like this image from PSOFT's website. The simplest way
(and already achievable just as easy) to do this is stack some three to
five cameras in different FOVs and align the clipping distance. LANPR
currently does not handle clipped lines differently for this kind of
composition, but to implement such function is fairly easy, and it will
not involve too many changes (at least in software mode). In DPIX mode,
this is also achievable, but multiple refreshes in one frame is very
slow in real-time applications.

Click here to view Pencil+'s demostration video of this.
<https://www.youtube.com/watch?v=-9oQAMsg7w8>

Another thing to mention is developer Sebastian Parborg's awesome smooth
contour modifier code in Blender. This modifier adjust the model's micro
topology before render begins, so that there won't be jaggy/messy lines
produced. This also makes chaining a lot easier. The modifier can be
used in 3D to 2D manga creation, which requires good smooth line
quality. However, the code doesn't run that fast (although it is already
a great lot faster than the original implementation), so optimization is
still needed, but we can postpone this for other more important tasks
considering this is not urgent.

### A little more things...

LANPR uses a whole lot different approach than Freestyle's, the design
of which enables multithreading and acceleration structures, and made
LANPR not sensitive to spatial structure of a specific scene. I will
post some papers about the algorithm behind LANPR to give a detailed
description for ones who is interested.

I'm in school so which means I may have time to do bug fixes, but it is
unlikely that I can actually find time to do the big "features".

### Will I be participating GSoC2019?

Well, at this time next year I should be attending my postgraduate
lessons (or maybe driving a bus in Chengdu), It's perfect for doing all
these things in GSoC, but actually I will write some code for these bug
fixes during holidays and vacations, so very likely by then I would have
solved many of these things listed above. So when the time comes then we
will see what can be done.

And finally thanks all LANPR users, NPR enthusiastic and Blender
developer for the focus and kind helps\!
