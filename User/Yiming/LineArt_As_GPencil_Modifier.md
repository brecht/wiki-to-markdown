# Line Art As GPencil Modifier FAQ

**Updated 2021/03/17**

This document discusses the current limitations and workarounds based on
the evaluation process of Line Art under the management of Grease Pencil
Modifier.

## Problems

### Nothing shows up when I add a line art object\!

There needs to be an active camera in scene. Line results are generated
from the view of that camera.

### It's too slow compared to past experiences\!

Due to lack of global cache at the moment, each line art modifier will
run the entire occlusion calculation for itself, so if you have multiple
line art modifiers to select different parts of the scene (to apply
different styles etc.), the process is going to cost you multiple
evaluation time.

Solution is being considered and developed at the moment, at the mean
time you can choose to use \`lanpr-under-gp\` branch, which is the
cached line art branch, the UI won't lock up when you adjust things, but
the cache not done in a "standard" way. You can use this branch to speed
up your process, but there are slight differences between this and
master code. This branch will become obsolete after we incorporated a
proper caching mechanism in master.

Also, a multithread loading and intersection calculation is under
development and will be available shortly.

### Intersection lines will appear outside source collection\!

Due to the way line art calculates intersection lines, they can not be
distinguished on a object/collection basis. There will be a temporary
solution coming real soon, which is to eliminate intersection
calculation outside selected geometry source. We are also developing a
new intersection selection method that can be efficiently integrated in
line art that doesn't slow down the calculation yet provide enough
flexibility.

### There are phantom connections across geometries\!

You have set the chaining threshold too big. Lower that value so lines
that are too far apart from each other won't connect.

### The line does not select when a edge mark is on a contour line.

This is currently a limitation of line art and will be addressed in the
future.
