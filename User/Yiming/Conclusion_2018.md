## GSoC Part

### Tasks

During GSoC time, these tasks are accomplished as planned:

\- Create a LANPR engine in Blender (2.8 branch).

\- Implement GPU side DPIX and Snake edge extraction algorithm.

\- Implement CPU side software edge extraction algorithm.

\- Include a smooth contour mesh modifier code by Sebastian Parborg,
Many thanks\! (note: I didn't write the modifier code, but we both are
looking forward to make this algorithm into Blender so we did cooperated
on this one), the algorithm can be found
[here](http://www.labri.fr/perso/pbenard/publications/contours/).

\- Design a basic graphical user interface for LANPR. The UI is going
through some major changes during Blender2.8's development, so this part
is predicted to have many changes afterwards.

\- Make the render pipeline fully functional in viewport, Single image
rendering and sequence rendering.

The development report from every week, [can be accessed
here](https://wiki.blender.org/wiki/User:Yiming/Proposal_2018).

### LANPR Outputs

[Click here to view LANPR Showcase
\>\>\>](Proposal_2018/Showcase.md)

### User Documents

[Click here to view documents
\>\>\>](Proposal_2018/UserDocument/index.md)

I've written a basic user manual for LANPR engine. Currently not in
master document trunk, because it's mainly for Blender 2.79 not 2.8, so
here I put a copy of the document here, This should be enough for
evaluation and later usages. The patch (D3572) for going into the master
document [can be seen here](https://developer.blender.org/D3572).

### Code Repository

[LANPR patch for Blender 2.8 is linked here
\>\>\>](https://developer.blender.org/D3505), this also including brief
explanations. For detailed git commit list (if you are curious...), My
branch is over [here on Blender's
gitweb](https://git.blender.org/gitweb/gitweb.cgi/blender.git/shortlog/refs/heads/soc-2018-npr).

Please note that at the time this passage is written, LANPR branch still
haven't been merged into master branch of Blender 2.8

The code for LANPR project can be accessed from Blender's main
repository. Want to compile yourself? Please check Blender Wiki for
instructions. <https://wiki.blender.org/wiki/Building_Blender>

Switch to soc-2018-npr branch to see my code result.

The smooth contour modifier by Sebastian Parborg which is been merged
into LANPR patch, its task page can be seen
[here](https://developer.blender.org/T48644).

Additionally, you will need OpenSubDiv 3.3.0 (and above) pre-built
libraries to compile my code (because the smooth contour algorithm uses
functions from it). OpenSubDiv part in Blender is also changing at the
time, so make sure you check about it if it gives compile errors.

Compile issues on Windows (MSVC) and Linux (GCC) is tested under default
compiler settings and works well. There might be some minor issue
related with compiler strict flags, tweak your compiler settings if you
keep encounter weird problems.

## Existing Problems and Future Developments

There are many small issues that are not solved quite well currently. I
actually have plans to stay around for some time and fix those small
problems when I had time. But I'm not as experienced as full time
professional developers, so LANPR could use a little help if you are
interested in it.

Please refer to [Development Reference
Page](Proposal_2018/DevRef.md) for detailed info.

(To view this document on Yiming's own website [please click
here](http://www.wellobserve.com/#!NPR/2018Summer/Conclusion.md))
