## GSoC Week1 Status

YimingWu

### Things have done

1\. Modified some cmake files and added external sourcefiles.

2\. Reviewed Sebastian Parborg's new code for smooth line solution, link
here <http://www.labri.fr/perso/pbenard/publications/contours/> May
probably not include the algorithm now as I am trying to merge exiting
code.

I finished most of the classes last Thursday then I started to work on
the project. Sorry for the late respond, I was at lectures this weekend.

### Next week

1\. Make the whole thing work in blender's infrastructure, most works
shall go into adapt the memory allocation stuff and adjust the
(NPR)intermal mesh structure.

2\. The most basic viewport post processing stuff will hopefully be
added. Will also adapt the BGL APIs.

3\. If time permits, will add RNA stuff into blender and prepare for ui
parameter access.

### Quesions

1\. I currently added offline NPR rendering stuff as a
"source/blender/bf\_line\_render" project, or shall I put it under
extern?

2\. Is it okay to mess around realtime NPR stuff in eevee's code or
something or shall I create a seperate project?

3\. Does blender's internal memory management (the BLI\_ ones)have any
limitations on using them? e.g. during different running state of the
program.

Another not-so-relavent one question: When debug using RelWithDebInfo
build, sometimes hit render will trigger a breakpoint and VS gives a
warning. But blender is able to continue running seemingly without any
problem. Is it normal? I'm using VS2015 with update 3 on Win10, this
problem appears since maybe two or three years ago.

That's it so far. I'll be on IRC as YimingWu during the day on my side,
and If there's anything I should do for the code review please send me
mail or IRC messages.

Regards,

YimingWu
