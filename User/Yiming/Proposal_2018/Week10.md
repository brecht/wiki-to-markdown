## GSoC Week10 Status

Hello everyone, here's my week 10 report\!

### Things have done

1\. Implemented chaining and detail reducing for software
rendering.(Detail reducing algorithm have bugs...)

2\. Fixed invalid normal after triangulation. Now the jaggy line cause
by this will never happen.

3\. Fixed File R/W errors. now LANPR config can be saved into blend
files and read out without any problems.

The screenshot below shows the chaining effect, which enables the
possibility to adjust line styles along the curves. Occlusion is turned
off for inspecting wether the result is right or not.

![Chaining effect without occlusion](../../../images/LANPR_Week10_1.png
"Chaining effect without occlusion")

### Next week

1\. Main task is to fix naming convensions and code style.

2\. Fix detail reducing bugs.

3\. Adjust UI to meet Blender's design guidlines, also make some tweaks
to enable some newer arguments in chaining algorithm.

### Questions

1\. About rendering: F12 still not working. I have had this problem for
several weeks but still don't know the cause of the problem. No Idea
why, but all I get is blank with only glClearColor on the framebuffer.

Thanks\! Have a great weekend\!

YimingWu
