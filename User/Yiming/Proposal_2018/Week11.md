## GSoC Week9 Status

Hello everyone, here's my week 9 report\!

### Things have done

1\. Suprisingly with Darkdefender's Smooth Contour implementation (Here:
<http://www.labri.fr/perso/pbenard/publications/contours/>), the result
become a lot more better because the algorithm solves the front/back
facing inconsistency problem using a geometry approach, it uses a
modifier to achieve such function, the only problem is that geometry
process is slower than expected, and it require subdivision. We are
trying to optimize this in the future.

2\. Plan to use Ramer Doglas Prucker algorithm to simplify the vector
curve. Here's a simple discription of this algorithm:
<http://karthaus.nl/rdp/> As for chainning, The only faster method is to
use the same acceleration structure for occlusion test and intersection
calculation, but even more memory is required in this method, as it will
store more point linkers.

`  - SVG export can then be achieved after this vector data is present.`  
`  - User will be able to adjust how much simplification they want for the curve.`  
`  - Vector programs such as Inkscape can reduce the amount of points and create bezier curve from them.`

3\. Fixing the places that got pointed out during the code review.

4\. Fixed depth offset algorithm, but sometimes still look weird in
viewport.

Here's a GIF demonstrating the effect of the crappy but now pretty
stable rendered animation: The frame is in 1920X1080, monkeys have 2X
subdivide, rendered using DPIX mode in LANPR, with intersection lines
turned off, and render time for each frame is less than 1 sec.
(including the time to send everything to GPU, larger scenes will
significantly increase the time of CPU-GPU data exchange)

![Monkey Animation](../../../images/LANPR_Week11_1.gif
"Monkey Animation")

![Rendering window](../../../images/LANPR_Week11_2.png
"Rendering window")

### Next week

1\. Continue fixing errors in code review.

2\. Implememt vector chainning and reducing.

3\. user should be able to use reduced vector result to produce line
effects like jittering, extending and dot dash.

4\. Implement svg export. (Hopefully)

Questions

Seems there are not much questions now. If there's any I will ask in IRC
when I'm coding.

YimingWu
