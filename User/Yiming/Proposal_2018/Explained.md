## GSoC2018 LANPR Development Explained

Hi everyone\! LANPR is progressing relatively fluent these days, and
some people have already tested some pre-alpha release of this LANPR
branch. But the tasks seems a little unclear since I haven't quite
explained how the development steps are, this includes what shall be
implemented and how much time will be spent on each of them. Guess it is
time for me to put up this development task list here on blender wiki.
In this page You will see the planned steps, substeps, references as
well as development progress.

### What are the main tasks?

this GSoC project is focused on giving Blender2.8 the ability to
genderate NPR line render just like Freestyle did in the 2.7x. This
time, two options are proposed: GPU realtime render and CPU offline
render.

To better adapt the development target of Blender2.8 as well as feedback
from the users, GPU option is developed first, and most of its main
functions are basically implemented now (optimization maybe later).

The next step is to achieve the CPU side which is more stable, accurate,
and different styles can be adjusted than on GPU. But to achieve this,
some support data structures will have to be done first. Below are the
detailed steps. (and what has been done now)

After main algorithms, there are several utility functions that can be
implemented. Such as vector result exporting, line style modifiers.
These will be implemented based one the actual demand of the users. In
Freestyle, there are so many line style modifers, but accroding to many
artists, they don't usually use most of them, although they tend to
demand that all of the styles to be available, So my plan is to just
implement some basic ones, for complicated ones which involve more
calculation on CPU side, leaving them for vector programs if vector
exporting is implemented.

#### Main Algorithms

`   - [OK] CPU: Edge detector CPU trace and vectorization drawing.`  
`     - [need attention] Traced line smoothing undone.`  
`     - [need attention] Sobel result thinning does not quite good in some conditions.`  
`   - [OK] DPIX algorithm implementation.`  
`     - [need attention] Buffer size is currently hard-coded, will have to be dynamic.`  
`     - [need attention] DPIX vector occlution test not implemented yet (has performance impact. Current results are good enough for viewport depth test, if there's further need, it will then be implemented).`  
`   - [OK] CPU computation implementation.`  
`     - [OK] Intersection line computation. (the result can be used at both GPU and CPU.)`  
`     - [OK] Feature line computation. (this can use DPIX result, save a lot of time).`  
`     - [OK] Accurate feature line extraction and preventing jaggy / incorrect results.`  
`     - [OK] Occlution test. (multithread)`  
`   - [     ] Line layer and style system.`  
`     - [OK] Depth controlled line style.`  
`     - [     ] Light controlled line style.`  
`     - [OK] Material, Group, Collection controlled line layer selection.`  
`     - [OK] Layer selection and composition.`

#### Support Structures

`   - [OK] GPU viewport drawing code, adapt the shaders.`  
`   - [STILL WORKING???????] Render to image support.`  
`   - [OK] UI basic controls.`  
`   - [OK] struct LANPR_SoftwareRender design.`  
`   - [OK] struct LANPR_LineLayer design. (To match blender 2.8's new collection structure.)`  
`     - [ALMOST OK] UI for line layer composition.`  
`   - [OK] Multithread.`  
`   - [     ] Support for modifier, shape keys, armatures and even particle system(maybe later).`  
`   - [     ] Line chainning for style modification and vector exporting (this need a better performance algorithm than Freestyle's)`

### FAQ

#### LANPR's Aim?

LANPR is originally developed to speed up feature line extraction and
occlusion test, and produce more stable result than Freestyle. Line
rendering is good for structural illustration and manga rednering, but
Freestyle tend to have so many bugs that the result usually needs fix.
LANPR's standalone program now already achieves very stable line result
output. As blender2.8's dropping Freestyle, LANPR take the chance.

#### Are they similar in algorithms?

Yes they follow the same principle, but LANPR uses different internal
structure than freestyle, which allows easier multithreading and more
accurate result. Freestyle is more sensitive to geometry topology, LANPR
behaves more stable in different topologies.

#### Why currently LANPR can only achieve limited line style?

Different from Freestyle, LANPR uses OpenGL to render all lines, no
matter the vector result is calculated on CPU or GPU side. OpenGL now
produce more trustworthy result than years before, and it is considered
to be suitable for outputing final image in most cases. The drawback of
this is that line styles are now fully achieved on shader side, which
means most of the old style functions are not immediately avaliable. But
there's also an advantage. Shaders are easier than coding in blender's
internal structure, so In the future line style can be added more
easily.

#### Can LANPR replace all functions in Freestyle?

No, accroding to many artists, most functions related to line
stylization is left unused, the most used are curving, random jitter and
dot-dash effect. So instead implementing so many effects that are hardly
used, LANPR choose to do the most used ones, and leave the slow, complex
ones to 2d vector programs that can better handle these problems.

#### What advantage does LANPR's Line Component have over Freestyle's "Select By"

LANPR can select lines with multiple rules together. You can specify one
to multiple object, material, collections to select lines from. In
Freestyle, the equivalent one is "Select By Group", putting this with
visibility and edge type together is a little hard to understand because
they aren't exactly the same level of concept.

LANPR also have the feature that can select "the rest of" the lines, so
when you adjust some specific lines that you wanted to pop out in the
scene, you can easily select rest of the lines that haven't been
displayed yet. This frees user from using "including/excluding" and
"and/or" logic thing.

#### Is intersection calculation hard?

Well, it certainlly isn't easy. When calculate the intersections using a
geometric approach, it's really a heavy task for CPU, just imagine you
bool-intersect every objects in the scene with each other.
Multithreading is quite hard for this stage also, but if you have enough
memory, there's certainlly some solution.

At least it should work.

#### Performance / Hardware requirements?

DPIX algorithm use large texture buffers as its data inputs and outputs,
and perform complicated calculation in opengl shaders, then when your
scene is large, it is expected that you should have plenty of GPU memory
on your card. One large scene with test proformed on my side showed that
you need some where near 3GB of VRAM available to run a 30milion verts
scene. But small scenes should not use as much VRAM as you may not
notice that. If you have a fast enough card, you can run it more
fluently.

For CPU computation, the more threads you have, the more memory you
have, the faster it can get. Freestyle will eat up a lot of memery in
your machine, LANPR wouldn't decrease this amount very much (20% to 30%
is possible though), but with multithreading, it will be a lot faster
than Freestyle.

#### Why sometimes developing progress are slow?

When implementing lower level data structure, it could take some time
and there's no immediate visible results, but it is important.

I'm still in school and some matters I will have to attend to, I will
keep you posted when something unexpected happens.

#### Will YimingWu continue support LANPR?

As far as I'm concerned, LANPR project will continue grow when I had
time. After this GSoC, the function which should be added into LANPR
will also be programmed if I had time, and many enthusiastics are also
focusing on this project. IRC user darkdefender's smooth contour code
also helped a lot in this project.
