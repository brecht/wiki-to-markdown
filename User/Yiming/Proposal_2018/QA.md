# Quesions and answers

## On GPU/DRW/GWN

1\. What's the difference between ShaderGroup/Pass/Framebuffer?

`  - ShaderGroup is used to warp a shader program (as in OpenGL).`  
`  - Pass is used to group Shader and drawing calls. It does not hold actual buffer object.`  
`  - Framebuffer is used to warp a FBO.`

2\. What's the difference between GPU/DRW/GWN APIs?

`  - GPU is for warpping OpenGL objects and functions.`  
`  - DRW is for excuting drawing-related command.`  
`  - GWN is a batch tool to generate calls that can be passed to DRW.`

3\. Can we use customized batch to draw primitives onto framebuffers?

`  - Yes, the DRW calls resets drawing states to its required state every time. So as long as your modification is covered by DRW, you are free to use your code.`

4\. When matrix is not set, what coordinate is used when doing vertex
shading?

`  - The default full screen rect shader doesn't have vertex transform in its vertex shader, and the coordinates is in the NDC coordinate.(X from -1 to 1, Y from -1 to 1)`

5\. Can I set my own matrix using my own drawing codes?

`  - Yes, the matrix is reset when DRW needs it. So no worries for destroying drawing state.`

6\. Does GPU/DRW support multisample textures? Also if it does support,
can transform\_to\_screen() display it correctly? If not, how do we
achieve multisample? By rendering it multiple times or something? Any
APIs for doing it?

`  - Yes, then in shader use your Sampler2DMS texture refernce. Haven't tested if it displays correctly when using transform to screen function.`

7\. Uniform array assign.

`  - Seems a little tricky. Float array doesn't seem to work on vec4, and you should take care when using vec4 functions, the array number should be 1 instead of 4, otherwise it will be treated like a mat4 (which is a 4*4 matrix).`

8\. Why F12 isn't working while rendering in viewport works?

`  - This is because in rendering, another GL Context is created and vertex buffers is not shared. So you need to manually create batch when you are in another render context.`

## On Mesh Operations

1\. If I'm to implement DPIX in blender I need to know how to 1) access
mesh's global-space vertex position and global-space normal (original
data prefered). It seems that object drawing code is warpped by batch
functions, then do we have functions for accesing these data directly?

`  - I have solved this using BMesh because I needed face/edge adjacent data. See the next question.`

2\. BMesh creation, access and delete.

`  -     Object* ob = some_mesh_object;`  
`        Mesh* me = ob->data;`  
`        `  
`        /* create bmesh holder */`  
`        BMesh* bm = BM_mesh_create(&allocsize, &((struct BMeshCreateParams){.use_toolflags = true,}));`  
`        `  
`        /* fill bmesh data */`  
`        BM_mesh_bm_from_me(bm, me, &((struct BMeshFromMeshParams){.calc_face_normal = true,}));`  
`        `  
`        /* this create the lookup table for bmesh. */`  
`        /* if you want to access bmesh elements by index, then the table for correspoding type must be available */`  
`        BM_mesh_elem_table_ensure(bm, BM_VERT|BM_EDGE|BM_FACE); /* creating one or two of them is also okay */`  
`        `  
`        /* access at index */`  
`        BMEdge* e = BM_edge_at_index(bm,i/*uint32 index*/);`  
`        `  
`        /* destroy bmesh when you are done */`  
`        BM_mesh_free(bm);`

1\. Triangulate BMesh?

`  -     BM_mesh_triangulate(bm, MOD_TRIANGULATE_QUAD_BEAUTY, MOD_TRIANGULATE_NGON_EARCLIP, false, NULL, NULL, NULL);`  
`  - Do remember to recalculate normals before you actually use the mesh data, BMesh operation functions does not automatically calculate them.`

2\. Triangulated mesh how to access the material data?

`  - There's a mat_nr field where the material is.`

## Other Things

1\. RNA/data Out of sync?

`  - New DepsGraph uses copy on write on many places. Some sync functions need to be manually added when modifying data. See T54810 for detail info.`

2\. notifier from another thread?

`  - I will need this later....`

3\. DPIX produce transformed vertex data on gpu side, we can use that to
speed up the calculation. Is that acceptable? (or using a toggle switch
for GPU-assisted mode and FULL CPU mode)

`  - Need more discussion with developers and users.`

4\. Win32 CRITICAL\_SECTION equivalent in blender?

`  - Just use the SpinLock. Check BLI_threads.h for usages. (I's very simple)`

5\. How to iterate all objects in a scene?

`  -     DEG_OBJECT_ITER_BEGIN(`  
`               depsgraph, o,`  
`               DEG_ITER_OBJECT_FLAG_LINKED_DIRECTLY |`  
`               DEG_ITER_OBJECT_FLAG_VISIBLE |`  
`               DEG_ITER_OBJECT_FLAG_DUPLI |`  
`               DEG_ITER_OBJECT_FLAG_LINKED_VIA_SET)`  
`           {`  
`               lanpr_MakeRenderGeometryBuffersObject(o, view, proj, rb);`  
`           }`  
`           DEG_OBJECT_ITER_END;`  
`  - Use Depsgraph *depsgraph = CTX_data_depsgraph(C);  to get depsgraph.`

6\. Collection RNA reading returns invalid pointer?

`  - See readfile.c/writefile.c, Blender's file reading pointer matching process is done manually not automatically, so in readfile you should use writestruct() to write your own struct, and in writefile use linklist() and newaddrptr() to rematch the pointers.`
