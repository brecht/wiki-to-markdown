## Why We Have This Section

LANPR is a new engine, just like EEVEE used to be. So there are sure
many things that haven't been achieved by one person in such a short
time (GSoC 2018) .

### How the code is generally organized

LANPR engine is very simple in structure. Currently, the engine's source
codes are all in Blender's draw/engines/lanpr folder. Other source codes
includes corresponding DNA/RNA structure definitions and python user
interface codes. Below are the main files:

| File                            | Description                                                                                                                                                                                                                |
| ------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| lanpr\_all.h                    | This is the file which contains all lanpr\_internal structures.                                                                                                                                                            |
| lanpr\_util.c and lanpr\_util.h | This is all the utility functions that LANPR used. this is originally from LANPR's standalone application, so there are some adaptor functions in it. Also, LANPR's 64bit float math and matrix function are defined here. |
| lanpr\_engine.c                 | LANPR's render engine definition.                                                                                                                                                                                          |
| lanpr\_snake.c                  | Snake mode specific code.                                                                                                                                                                                                  |
| lanpr\_dpix.c                   | DPIX mode specific code.                                                                                                                                                                                                   |
| lanpr\_ops.c                    | Software mode specific code and operator codes.                                                                                                                                                                            |

I have written \[a brief document\](Questions.md) on blender's
DRW/GPU/GWN and some other modules, because in Blender 2.8 these things
are new and poorly documented, I hope this document can help a little
when any new developer are coding with these modules.

## Functions That We Hope To Achieve

### High Priority

  - Software multithread \*\*seems to\*\* have some access conflicts.
    See lanpr\_ops.c line 2088 for the exact position.
  - Double precision functions in lanpr\_util.c haven't been modified
    yet. This part should use internal math functions but many of them
    don't have double support. So I'm just leaving it that way for now.
  - Software triangle culling have problems with near clipping plane.
  - DPIX clipping also have similar problem.
  - Snake image filter should have better GLSL.

### Normal Priority

  - GLSL line style module.
  - Select lines based on objects, materials, collections when chaining.
  - Limit intersection calculation in specific group/collection of
    objects.
  - Even faster chaining and different chaining modes.
  - DPIX Atlas occlusion test.
  - DPIX Chaining? (maybe not possible in real time)
  - Adjustable DPIX "polygon offset".
  - Snake chaining better algorithm, also need smoothing.
  - Snake automatic adapt edge detector coefficients.

## Known Bugs

  - DPIX and Software modes doesn't support ortho camera quite well.
  - Software mode camera FOV doesn't match with blender's.
