## GSoC Week3 Status

YimingWu

### Things have done

The coding seems not going as fast as expected. I had three class
reports this week, these are things I've done during the week for NPR
project:

1\. Improved line rendering performance by moving all line segments in
to one drawing batch. 2. Added several switches to control rendering
method. Currently the engine can choose to render edge detection filter
result, thinning result and vectorization result. 3. Moved my DPIX code
into the engine, but still modifying code to adapt blender's data
structure. (and can be used very soon) I used \#ifdef blocks to prevent
it from compiling now. DPIX shaders have problems too. 4. Some other
minor changes in struct SceneLANPR includes more parameters for
controlling vector line extending and curve sensitivity, which is used
to mimic hand-sketched images. (Still implementing these features
currently)

Screenshots shows the new rendering parameters.

![Snake UI Modes](../../../images/LANPR_Week3_1.png "Snake UI Modes")
![Snake Effects UI Panel](../../../images/LANPR_Week3_2.png
"Snake Effects UI Panel")

### Next week

1\. Turns out that DPIX algorithm needs a lot more work than I was
expecting, I've decided to focus more on DPIX implementation next week
and make sure it produce correct result by next friday. 2. Currently,
the Snake algorithm produces a lot of jaggy lines, and frequently misses
short feature lines. Will look into it to see if there's anything I can
do to improve the line rendering quality.

### Questions

I have listed questions (and some answers) here in a seperate wiki page.
Check here\>\>\>
