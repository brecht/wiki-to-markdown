### GSoC Week6 Status

YimingWu

I can't seem to find my page I created for week5 report on the new wiki
(My account still works though). Maybe there have been an issue with it,
so for now I just linked the report to my own website. If there's
anything I'm missing please let me know.

The link to this report:
<http://www.wellobserve.com/#!NPR/2018Summer/logs/Week6.md>

### Things have done

1\. As planned, I spent most of time moving all calculation code for
software rendering into lanpr (and made it compile). This also include
code for object intersection and multithread occlusion test. Basically
there're only display/drawing code left which I will rewite based on
line layer system.

2\. Changed Line composition ui for the new line layer system. LANPR's
line layer design has some difference with freestyle.

3\. Disabled freestyle compile option but preserved freestyle edge/face
mark data and it's api so we can still mark things and LANPR could also
use the information. We don't need to re-write the whole thing. (the
naming of "LineStyle"/"LineLayer" is a bit messy, I'll probably fix it
this weekend.)

4\. Used "uncrustify" to clean up the .c and .h files, now the content
look a lot better. Thanks ideasman42 for suggesting this.

5\. Updated my coding Q\&A page with some stupid questions. Check it
here.

### See these:

\- 12315f4 for UI fix.

\- All the way to ofb1e8f for Complete offline calculation code. (code
style cleaned up)

### Next week

1\. Make offline render produce correct result and make it display.
(This is also a lot of work so...)

2\. Realtime render doesn't produce result onto the final framebuffer,
need investigation. Should solve this problem.

3\. Upload a patch for code review (maybe I can do it this weekend).
