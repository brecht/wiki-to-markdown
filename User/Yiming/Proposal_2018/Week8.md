## GSoC Week8 Status

Hello everyone, here's my week 8 report\!

### Things have done

1\. Multithread line occlusion test is OK. Now software calculation runs
even faster than before. See 59d878a.

2\. Found out that overlapping edges will result in strange occlusion
result, thus removing edge split modifier works better. For some models
that already have split the edges, an option can be added to enable some
pre-process the mesh before all the calculations start. (not added yet)
See ca4144a.

3\. Object/material/collection-based line selection functions are added
as planed, now user can select desired line types based on these three
qualifiers, and preview the result in both DPIX and Software modes. This
is a little bit like the one in Freestyle, but there are some difference
in design to make it easier to use and to understand, it also includes
some features that Freestyle don't have. See 54172dc.

4\. Improved user interface, using the new grouping API (or something
like that...), different arguments are now categorized and reused in
different algorithm choices. Also improved intersection calculation
display, allowing user to cleary understand the state. See 07b7fae.

5\. Try to fix F12 but no luck. (:'/ why..)

6\. Reported that gcc tool chain have some problems compiling the code,
already fixed.

### Next week

1\. Figure out a way to export vector data as svg. Users have feedback
that shows the need of vector data for other composition software to
work with.

`  - This includes line chainning and image space reducing, which can then be exported as a fluent curve (or multi-segment straight lines). `

2\. Make DPIX's data texture size into a auto-decide or user selectable
one, now it is hard coded 2048\*2048.

3\. DPIX preview's depth offset need some redo, now only uses simple
constant depth offset, but z-buffer is non-linear so in line result
preview, something like glPolygonOffset() should be implemented.

4\. Still try to solve F12 problem.

5\. I'll spend more time documenting LANPR, and explain how to use it.

6\. Snake algorithm shows some bugs that some feature lines will
disappear in certain angles.

7\. Match the code with blender's naming conventions. (some code from my
own program is in a different style)

### Questions

Seems there are not much questions now. If there's any I will ask in IRC
when I'm coding.

YimingWu
