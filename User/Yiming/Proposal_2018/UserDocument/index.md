## What is LANPR?

LANPR is a new NPR line render engine developed for blender2.8, to
replace legacy Freestyle. LANPR is originally developed to speed up
feature line extraction and occlusion test, and produce more stable
result than Freestyle. Line rendering is good for structural
illustration and manga rednering, but Freestyle tend to have many bugs
that the result usually needs fix. LANPR's line extraction method is
much more stable. As Blender2.8's dropping Freestyle, LANPR take the
chance.

## Why use LANPR?

  - It's Fast. LANPR features three rendering modes, two modes of them
    can produce realtime line extraction result, even in very big
    scenes. For non-rea ltime rendering mode, the results are
    predictable and rendering time is greatly reduced than Freestyle's.
  - It's More Stable. LANPR uses different line extraction algorithm
    than Freestyle's, and it's promised to produce accurate,
    non-flickering continuous line result, with very rare chance of a
    occlusion error.
  - It Produces Intersection Lines. LANPR can caluclate intersection
    lines between geometries. Which Freestyle is incapable of.
    Intersection line calculation is limited to visible region and it
    uses acceleration methods as well to speed up the process.

Just like Freestyle, the principle of feature line calculation in LANPR
is basically the same, so there won't be so much difficulties
understanding the usage of LANPR, since Freestyle has already been there
for a while.

But unlike Freestyle, LANPR's infrastrcuture is very different than
Freestyle's, which enables functions like multithread occlusion test,
intersection lines, stable chaining and fast GLSL rendering. At this
viewpoint, LANPR have more potential to extend its functions in future
development.

## To Begin With

Select any chapter you want:

  - [Modes in
    LANPR](Modes/index.md)
      - [How to use: Software
        Mode](Modes/Software.md)
      - [How to use: DPIX
        Mode](Modes/DPIX.md)
      - [How to use: Snake
        Mode](Modes/Snake.md)
  - [Performance
    Issues](Performance.md)

## Bugs and Future Development Reference

See the GSoC conclusion document.
