LANPR engine has three rendering modes: Software, DPIX and Snake. Each
of them is designed for different usages. Users can choose between the
three and see what is more suitable for their demand.

## Software Mode

Software mode is a lot comparable to Freestyle. Here are the main
features:

  - It allows pre-compute feature lines. User can adjust line layers,
    components, styles in realtime preview without re-render the whole
    scene.
  - Different line types can be selected, including contour, crease,
    intersection, edge mark, material separation, just like Freestyle.
  - User can choose to enable/disable intersection calculation and
    chaining, to save calculation time when needed.

Currently there are some major drawbacks that users should know:

  - The memory usage isn't significantly less than Freestyle.
  - Chaining algorithm is slow for the moment.
  - Only very limited line styles are present at this moment.

## DPIX Mode

DPIX mode extract lines in real time. It uses advanced GLSL trick to do
the vector line extraction, which is very fast compared to CPU. Here are
the main features:

  - Line types used in software mode is also effective in DPIX mode.
  - If intersection line is calculated for current frame, then DPIX can
    also draw the intersection lines in real time preview.
  - Depth controlled line style such as thickness reduction and fog
    effect can be easily achieved.

DPIX mode in LANPR also have some drawbacks:

  - In the original DPIX paper, occlusion test is achieved along edges
    using depth sample, which can then give a rough vector result. In
    LANPR's implementation, only line extraction is implemented,
    occlusion test is done directly over a depth test. Then there's no
    vector information about occlusion. But this saves VRam, and we can
    always use software mode instead, since it's not that slow. Future
    development on this is also welcomed.
  - DPIX line segments are in principle hard to chain in real time.

## Snake Mode

Unlike those two modes above, the snake method traces feature lines in
image-space, which is less affected by discrete geometry topology. Here
are the main features:

  - Can enable or disable vectorization, image filter arguments are
    adjustable.
  - Image filter result can be output as masks for later composition.
  - Vectorized line style is adjustable.
  - Has a fluent level-of-detail transition.

Currently there are following drawbacks:

  - Vector chaining needs a better algorithm, current algorithm is a bit
    jaggy.
  - image filter shader needs improvement to enable more accurate
    feature extraction.
