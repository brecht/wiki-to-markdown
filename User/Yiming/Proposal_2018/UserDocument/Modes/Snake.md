## Edge Detection

![Snake edge detection panel](../../../../../images/LANPR_Doc_10.png
"Snake edge detection panel")

LANPR applies Sobel edge detector on depth and normal pass to extract
feature lines.

There are four values to adjust the effect of edge detection. By
default, viewport will display edge detection result. Adjust different
value, then when the extraction meet your desire, you can proceed to
further post-processing steps.

### Thinning

To trace feature pixels, edge detection result is processed with
thinning algorithm, to ensure lines are in exact one pixel width. You
can preview thinning result by selecting the display mode to Thinning.

note: It is recommended that you should adjust the values to achieve the
effect that no large white regions are shown in viewport, this prevents
messy vectorization result. Because different scene have different depth
clipping planes, so the depth control values will have to be adjusted to
meet this standard. In the future, automatic adaptation may be
developed.

### Vectorization

![vectorization config](../../../../../images/LANPR_Doc_11.png
"vectorization config")

Image space edge detection and thinning results are sent to
vectorization stage, if you enable Vectorization option. LANPR then
trace the pixel features into a complete chain in CPU, and finally CPU
send the result to GPU for rendering.

When vectorization is enabled, Line Styles can then be adjusted. Due to
lack of edge type information, Snake mode can not adjust different line
type individually. Line style can only be set uniformly.

## Taper

Because the traced result is a long chain, Snake mode naturally have the
ability to apply 1D styles on it. You can set taper strength and
distance of influence. You can preview the effect realtime in the
viewport.

note: Taper distance is not in absolute length currently. Different
scene and camera configurations may give different result on this style.

## Other Issues

As the other two modes have the advantage of speed and quality, Snake
mode become less used in most of the conditions. The quality isn't the
best but it is usable in nearly all scenes if properly adjusted. So
Snake mode also has potential. Future development should also consider
extending its feature.
