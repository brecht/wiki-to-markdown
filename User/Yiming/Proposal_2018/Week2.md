## GSoC Week2 Status

YimingWu

### Things have done

This week I mainly focused on realtime NPR line part, and implement the
snake algorithm in blender.

![Robot Hand](../../../images/LANPR_Week2_1.png "Robot Hand") ![Human
Body Model](../../../images/LANPR_Week2_2.png "Human Body Model") ![Rail
Gun](../../../images/LANPR_Week2_3.png "Rail Gun")
![Engine](../../../images/LANPR_Week2_4.png "Engine")

1\. Snake method algorithm migration almost done. Latest commit has some
memory pool issue my be solved this afternoon if I had time. Memory pool
issue already solved.

2\. Made a simple UI in the properties panel (under Scene page), can be
used to adjust render parameters. I've been told that RNA/Depsgraph is
experiencing some modifications and properties can be un-synced but le
us see if it got fixed next week.

3\. I actually learned about the DRW/GWN/GPU apis and the convenient
design makes things a lot easier. (Good job to whoever wrote these
codes\!) Also, when I had spare time I will try to manage a
documentation (or newbie instruction) for these apis. I categoried the
questions in a seperate page Here\>\>\>, it might be useful for new
developers to grab a hand on.

These are the screenshots I captured. it demonstrated the vector line
thickness variation effect. Currently only background color and line
color can be adjusted uniformly. Will make more parameters adjustable in
the next week. This may look as good as expected, and I'm still
adjusting arguments to find a better combination.

### Next week

1\. Make more properties in Snake render avaliable, which are mostly
hard-coded currently.

2\. Implement an adjustable line-smooth thing.

3\. Migrate DPIX algorithm.

4\. Also, I may need to figure out a way to export vector data, as many
people are expecting.
