## GSoC Week4 Status

YimingWu

### Things have done

1\. Basic DPIX algorithm implemented. Occlsion test is done directly
through depth buffer and polygonoffset(to avoid z-fighting). Depth
sampling method seems not quite necessary for generating normal lines.
2. Seperated sorce code into several new files for better organization.
3. Fixed several bugs caused by optimization, including depth lost and
taper option not working. 4. Used GL\_LINE\_SMOOTH and
GL\_POLYGON\_SMOOTH to achieve basic anti-alias. (later I was informed
that Multisample texture is supported by blender's api.)

Screenshots and videos\! (many people have been following the project's
status on twitter, you might have seen most of the images.)

(Not uploaded yet)

### Next week

1\. I'm currently making some ui concepts for NPR rendering control
during this weekend. 2. Must fix normal matrix problem in DPIX,
currently not-one scale factor will cause strange behavior.... 3. Redo
the multisample shader. should be very quick. (also, this feature can be
enable/disabled by user, and the choosing of multisample quality should
also be implemented) 4. camera distance, light controlled line thickness
and transparency should be implemented. 5. Material seperate lines and
edge marks should be implemented. 6. Rendering should accept
post-modifier mesh. Morphing animation should be supported by then. 7.
Dash line style or textured line style need further research, expecially
when DPIX algorithm doesn't natually produce continuous line.

### Questions

I have listed questions (and some answers) here in a seperate wiki page.
