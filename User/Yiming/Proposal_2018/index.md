# Proposal: Blender NPR Line Rendering Improvement

note: this page isn't complete yet after the wiki migration.

(Updated on 18/7/20)

note: GSoC2018 has concluded, please refer to
[Conclusion\_2018](../Conclusion_2018.md) for latest
info

To developers and artists who is looking forward to have a more
production-ready NPR line render, I propose some improvements to
blender's freestyle (or not using freestyle), enables much faster (real
time preferred) line rendering with much fewer errors. Many artists who
I once worked with have suggested that this feature should go into
blender's code base to ensure a fluent workflow. As blender's 2.8 hasn't
decided freestyle's fate and it will very likely became obsolete if no
one cares about it anymore, therefore I present my developement here as
a proposal to this year's GSoC project, and hope it can demonstrate its
ability.

## Targets

This proposal gives two options on major targets.

1\. When the major focus is the Real Time solutions, the target should
be:

Achieve viewport real time NPR line drawing. (and corresponding UI)

Line texture/style support.(and corresponding UI)

2\. When the major focus is Offline Rendering, here are the targets:

Overcome occlusion computation errors.

Making Freestyle running on multi-thread.

Implement intersection lines.

Implement a more user friendly line-set structure.

detailed description for this solution please CLICK HERE\>\>\>.

LOOK HERE\>\>\> for another comparison I did for realtime solution, a
hybrid solution for future is also proposed, which I consider valuable
for future devs.

Many people have been giving me suggestions and asking questions, so
Here\>\>\> I present my opinion to these feedbacks.

## Benefits

Freestyle will become production ready for it will involve nearly zero
post-render hand trimming. This is a huge benefit for artists creating
stylized images especially in comic/manga style.

People who use Freestyle to generate engineer figure or animation will
get accurate result with less time.

Future real time preview feature can co-operate with eevee, give very
promising cartoon render effect.

## Project Schedule

Development Log Here\!

  - [Q\&A](QA.md)
  - [Week1](Week1.md)
  - [Week2](Week2.md)
  - [Week3](Week3.md)
  - [Week4](Week4.md)
  - [NPR Dev, Explained
    \>\>\>](Explained.md)
  - [Week5](Week5.md)
  - [Week6](Week6.md)
  - [Week7](Week7.md)
  - [Week8](Week8.md)
  - [Week9](Week9.md)
  - [Week10](Week10.md)
  - [Week11](Week11.md)
  - [Week12](Week12.md)

(due to change of solutions this section become more variable...)

\- OPTION ONE To implement two new real time solutions: estimated 5
Weeks

\- DPIX implementation: about 1-2 weeks, Blender should already have
many GL api wrappings, they will make things easier.

\- Snake implementation: about 3 weeks, including basic functions, line
style support, ui and so on.

\- DPIX with GPU intersection line will spend more time.

\- OPTION TWO To implement Freestyle speedup: estimated 6 Weeks

\- Infrastructure adaptation: about 1-2 Weeks

\- Algorithm integration: about 2-3 Weeks

\- Realtime line style preview: about 1 Week

\- Freestyle user interface reconstruction: about 1 Week

Because nearly all algorithm thing are implemented and tested, it should
be not very hard to integrate it into Blender.

\- My school classes will finish at around May 20th and since then I'm
free to work on this project full time until August 30th.

\- Some semester presentation is required at around June 10th, it may
take two or three days.

## About me

My name is Yiming Wu (吴 奕茗). I live in China.

Currently studying at Industrial Design Department at Xi'an Jiaotong
University.

Blender User experience since 2011. Using it as a hobby, good at
hard-surface and mechanical structure modeling. Not quite good at
sculpting. I have a portfolio about some of my blender usage at
Artstation.

### Programming experience

(Envidential matter can be provided)

\- Internship at 3DPunk.com (a Chinese website similar to Sketchfab),
worked on PBR and NPR shaders, three.js drawing core codes, and a
server-side image generation for patent agent client. Approved by devs.

\- Coding our own studio 3d test platform (c library), including Win32,
OpenGL (api boundle and making shaders), user interface kernel, scene
management, file io, node graph, animation and so on. Under continuous
development. Tested and used at Beijing PIGO studio, one of BlenderCN
community's major studio.

\- images used to demostrate line render result in this proposal are
also captured in this platform.

\- Mobile app user interface design for a Chinese website myouth.cn.
Pending approval.

\- Wrote several Blender RNA Architecture Analysis documents for Chinese
developers.

I haven't submitted code to d.b.o. But I can certainly do some small
hacks to show that I'm able of solving problems in Blender.

### Contact

E-Mail: xp8110@outlook.com

freenode IRC username: YimingWu

Twitter: @ChengduLittleA

Weibo: @成都-小A-nicksbest

Personal Website: www.wellobserve.com (not mainly for developing stuff)

QQ: 1094469528

## I'd like to thank...

All feedback I get from mailing list, irc, discord, facebook currently.
Wasili has been very active as a NPR enthusiasm and give me some new
viewpoint. Developer KAJIYAMA and many others have been focusing on this
too. Thanks for Christian Michelin's feedback on animation usages.
BlenderCN's support is also great. Thank you all for suggestive info\!
