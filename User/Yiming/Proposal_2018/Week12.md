## GSoC Week12 Status

Hello everyone, here's my week 12 report\! Sorry for the delay, I've
been busy with school project since August. (but I hope time difference
will offset it back xD)

### Things have done

1\. A basic user document is finished, see D3572 for the patch (patch
only includes text).

2\. Fixed software rendering animation sequence data cache, now sequence
can be rendered without a problem.

3\. Fixed software mode triangle culling errors. Triangle that cross the
near clipping plane used to be a problem, but now everything works
great. (yeah\!)

4\. Fixed render-while-drawing-viewport conflict.

5\. Fixed some user interface panel missing problems, but haven't fixed
them all.

6\. Thanks to Joshua, GCC compiler now working.

7\. Also fixed some minor memory leaks, so now LANPR renders everything
without building up a large trunk.

![Dynamic intersection lines](../../../images/LANPR_Week12_1.gif
"Dynamic intersection lines")

![Monkey animation slow motion](../../../images/LANPR_Week12_2.gif
"Monkey animation slow motion")

### My plans on future developments

I plan to stay around (a lot) longer for LANPR project, because there
are indeed many people looking forward to see this in master, if I
continue improve the code, we don't need to halt the project.

Still helps are also needed as I'm not that familiar with modules in
blender, so when anyone sees anything that he/she can improve, feel free
to do so.

It's been very exciting to participate in GSoC project. Also, I'd like
to thank every person in blender's community who helped me make this
project possible.

If anyone could check those first...

Currently there are these problems which should be solved first:

1\. In lanpr\_ops.c line 2088, there's a very strange situation related
to threading. See comment for info.

2\. When using DPIX mode to render images, after F12 , then viewport
will display wrong results. (But F12 is always correct.) I'm suspecting
the state for half-pixel texture sample in OpenGL is not reset after
drawing, because DPIX uses every single pixels on data texture to
produce results.

YimingWu
