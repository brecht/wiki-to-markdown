# How to write a bug report

## 1\. Introduction

The right place to report bugs is [Blender's bug
tracker](https://developer.blender.org/). Before you start writing your
ticket, please read the following post. It will help you write a good
bug report that allows developers to identify the source of the problem
and thus fix the issue as soon as possible.

## 2\. Preparation

The first step is to ensure that the issue is actually a bug in Blender
and that you can provide enough information for others to reproduce it
on their system.

### 2.1. Minimum requirements

In order to run Blender properly, you have to ensure that your hardware
fulfills the [minimum
requirements](https://www.blender.org/download/requirements/). The
supported graphics cards are particularly important. If your hardware is
not supported, you may experience graphics glitches, crashes or may not
be able to start Blender at all. The developers do not provide support
on the bug tracker for hardware that doesn't meet the minimum
requirements.

Please note that the [requirements for GPU
rendering](https://docs.blender.org/manual/en/latest/render/cycles/gpu_rendering.html#supported-hardware)
are higher than the minimum requirements for running Blender. In case
you don't see any GPUs listed as render device in the preferences ("No
compatible GPUs found for path tracing"), then this is most likely the
reason why. It could also happen that your graphics driver is out-dated
as explained in the next section.

If you're able to start Blender, you can get all the system information
through *Help \> Save System Info*.

## 2.2. Update the graphics driver

Crashes of Blender or even the entire operating system can be caused by
bugs in the graphics driver. Updating the graphics driver to the most
recent version should minimize the likelihood that this is the cause of
your problem. You may have to update the driver for both your integrated
GPU and dedicated GPU.

[Blender's
manual](https://docs.blender.org/manual/en/dev/troubleshooting/gpu/index.html)
provides additional information for troubleshooting on all supported
operating systems.

### 2.2.1. Windows

The most recent driver for
[Nvidia](https://www.nvidia.com/Download/index.aspx?lang=en),
[AMD](https://www.amd.com/en/support) and
[Intel](https://downloadcenter.intel.com/) graphics cards can be
downloaded from their respective websites.

### 2.2.2. macOS

On macOS the graphics card driver is tied to the operating system
version, therefore it may be necessary to update the OS.

At the time of writing the GPU rendering support for Cycles is limited.
GPU rendering with OpenCL is only available in versions prior to 2.80
because Apple has deprecated their OpenCL compiler. GPU rendering with
CUDA is only possible in versions before 2.9x because Nvidia
discontinued the development of the CUDA Toolkit for macOS. For a more
detailed explanation see [this answer on Blender's Stack
Exchange](https://blender.stackexchange.com/a/198660/74827). As a
consequence you may not find your GPUs listed as supported render device
in the user preferences. This is not a bug.

### 2.2.3. Linux

Please note that OpenCL GPU rendering with AMD graphics cards on Linux
requires the installation of the AMDGPU-PRO Driver. Consult your
distribution's documentation to install and update the driver.

## 2.3. Reproducible steps and minimal project

Try to identify and write down a precise list of steps that result in
the issue that you're experiencing. Ideally these instructions should
allow to reproduce the problem beginning from the default startup file.
If the problem only occurs in a specific project, try to create a
minimal version of it. Remove anything that's not relevant to the issue.

The instructions should be detailed enough that anyone who is proficient
in Blender is able to produce the same result that you're getting.
Please use standard nomenclature in your description. Should you be
unsure how certain parts of the user interface are called, consult the
[manual](https://docs.blender.org/manual/en/latest/glossary/index.html).

## 2.4. Test different Blender versions

In order to identify when an issue started to occur in Blender or if it
is already fixed in a more recent one, you should check if your problem
also occurs in other Blender versions.

### 2.4.1. Test the latest versions

The issue may already be fixed in a more recent version of Blender. Test
with both the [latest stable release](https://www.blender.org/download/)
and the [most recent daily
build](https://builder.blender.org/download/).

If you need to test a specific project file, create a backup copy of it.
This is meant as a precaution, since files saved in a newer version of
Blender may not open in the older versions of Blender that you're
usually using. When you're trying to check if the issue still occurs in
the most recent version of Blender, use *File \> Open* to load the
project file. Don't open the project file from your operating system's
file browser as this may open a different version of Blender that is
associated with the file extension.

In case the issue doesn't occur in the latest stable release, you should
continue to use that version. If the issue is fixed in the daily build,
you should continue to use your current version and update as soon as a
new stable release is published.

### 2.4.2. Test previous versions (optional)

If you are able to, check if you can find the *most recent* [previous
releases of Blender](https://download.blender.org/release/) that does
*not* have the issue. This can help the developers to narrow down when
the bug was introduced and what change has caused it. The portable
releases (\`\*.zip\` and \`\*.tar.xz\`) don't require any installation,
they just have to be extracted and thus allow for easy testing. You
should *not* test further back than Blender 2.79b.

### 2.5. Check if it's a (new) bug

If you're unsure whether or not the issue is a bug, or if it could be a
misunderstanding how Blender works, post a question on one of our
[community websites](https://www.blender.org/community/). For example:

  - [Blender's chat support
    channel](https://blender.chat/channel/support)
  - [Blender's StackExchange](https://blender.stackexchange.com/)
  - [blenderartists.org](https://blenderartists.org/c/support/)

In case your issue requires an enhancement or new feature in Blender,
[it's not a bug](../../Reference/Not_a_bug.md). Feature requests can
be posted on [right-click
select](https://blender.community/c/rightclickselect).

Should you be fairly certain that it's a bug, [verify that it hasn't
already been reported](https://developer.blender.org/maniphest/). If you
find an existing ticket that matches your issue exactly *and* others had
trouble reproducing it, please add your step by step instructions in a
comment.

## 2.6. Create a debug log

Blender can output detailed debug information when one of the [debug CLI
flags](https://docs.blender.org/manual/en/latest/advanced/command_line/arguments.html#debug-options)
are used. If you're either experiencing a crash or Blender displays an
error message, you should create a debug log file that can be attached
to your bug report.

Additional log files with different CLI flags may be requested by the
developers, but should't be provided up front.

### 2.6.1. Windows

Open Blender's installation directory and double-click on the
\`blender\_debug\_log.cmd\`. This will start Blender in debug mode and
create log files. Try to reproduce the error while Blender is open in
debug mode. Close it once the error occured. The Windows Explorer should
open automatically and show you two files. Copy them to a location that
you know and attach them to your bug report in the following chapter.

In case Blender 2.9x or later crashes, it will create a crash log that
contains valuable information about where the error occurred. This file
should also be attached to your report. The location is determined by
the environment variable \`TEMP\`. If you haven't modified this
environment variable, it will point to \`C:\\Users\\\[your
username\]\\AppData\\Local\\Temp\\\` (replace \`\[your username\]\` with
the actual username on your system). In order to locate the crash log,
open the Windows Explorer (<kbd>Win</kbd>+<kbd>E</kbd>) and type
\`%TEMP%\` into the path bar. The name of the crash log will be the same
as your project file, but with the file extension \`.crash.txt\`. If you
haven't previously saved the project before, it will be
\`blender.crash.txt\`.

### 2.6.2. Linux

Open Blender and save the system information through *Help \> Save
System Info*. Open a terminal in Blender's installation directory and
start Blender with the \`--debug\` and \`--debug-cycles\` flag. Redirect
the output for both stdout and stderr to a file. The example command
below places the log file in your home directory.

    ./blender --debug --debug-cycles > ~/blender_debug_output.txt 2>&1

Attach the system information and log file to your bug report in the
following chapter.

In case Blender 2.9x or later crashes, it will also create a crash log
that contains a stack trace. The file can be found in the \`/tmp\`
directory. It will be named like your project file, but with the file
extension \`.crash.txt\`. If you haven't previously saved the project
before, it will be saved as \`blender.crash.txt\`. This file should also
be attached to your report.

### 2.6.3. macOS

Open Blender and save the system information through *Help \> Save
System Info*. Open a terminal and start Blender with the \`--debug\` and
\`--debug-cycles\` flag. Redirect the output for both stdout and stderr
to a file. The example command below places the log file in your home
directory.

    /Applications/Blender.app/Contents/MacOS/Blender --debug --debug-cycles > ~/blender_debug_output.txt 2>&1

Attach the system information and log file to your bug report in the
following chapter.

In case Blender crashes the macOS Crash Reporter will open and show you
a detailed report with a stack trace. You should copy the text, save it
in a text file and also attach it to your report.

## 3\. Create the report

Create an account on
[developer.blender.org](https://developer.blender.org/).

There are two different templates for reporting bugs, one for add-ons
and one for Blender itself.

**If you want to report a bug in an add-on:**

1.  Open Blender.
2.  Open the preferences and navigate to the add-ons (*Edit \>
    Preferences \> Add-ons*).
3.  Find the add-on you would like to report a bug for.
4.  Check that it's an official add-on that is shipped with Blender. If
    it isn't, contact the developer directly. The bug tracker is only
    for add-ons that are distributed with Blender.
5.  Open the details section of the add-on and click *Report a Bug*.

**If you want to report a bug in Blender:**

1.  Open Blender.
2.  Use *Help \> Report a Bug*.
3.  Add which versions of Blender do not work.
4.  Add which version of Blender works.

**If you cannot open Blender and want to report a bug:**

1.  Use the [bug report
    template](https://developer.blender.org/maniphest/task/edit/form/1/).
2.  Add your operating system and graphics card information.
3.  Add which versions of Blender do not work.
4.  Add which version of Blender works.

Once you've logged in you should see the submission template. Anything
in square brackets should be replaced with the information about your
problem. Should you have trouble writing in English, then please write
the report in your native language and include a machine translated text
alongside.

1.  Give your ticket a descriptive title. It should ideally explain
    what's broken and where.
2.  Add a short description of your problem to *"Short description of
    the error"*. You may include a screenshot if the problem is hard to
    describe.
3.  Add the detailed steps to reproduce the issue from section *"2.3
    Reproducible steps and minimal project"* to *"Exact steps for others
    to reproduce the error"*.
4.  Attach your system information file, debug log and crash log to the
    bug report by pressing the upload button or dragging and dropping
    the file into the browser window.
5.  Attach your .blend project file (only if the bug can't be reproduced
    with the default startup file).
6.  Click "Create New Task" to create the bug report.

## 3.1. Example of a good bug report

Title: Crash using color drag and drop in the image editor

    **System Information**
    Operating system: Windows-10-10.0.18362 64 Bits
    Graphics card: GeForce GTX 1080 Ti/PCIe/SSE2 NVIDIA Corporation 4.5.0 NVIDIA 436.30
        
    **Blender Version**
    Broken: 
     - version: 2.82 (sub 7), branch: master, commit date: 2020-03-12 05:06, hash: `rB375c7dc4caf4`
     - version: 2.83 (sub 12), branch: master, commit date: 2020-04-06 15:48, hash: `rB71a52bbe2a2e`
    Worked: 2.80
        
    **Short description of error**
    Blender crashes when dragging and dropping a color onto the texture in the image editor.
        
    **Exact steps for others to reproduce the error**
    1. Open the Texture Paint workspace.
    2. Create a new texture in the image editor.
    3. Put the mouse cursor over the current color in the header and drag it onto the texture.

## 4\. Triage and bug fix

Once your bug report is submitted, volunteers and developers will try to
reproduce the issue. They may have additional questions, so check once
in a while if somebody has left a comment on your ticket. If the status
of your ticket is "Needs information from user" and you do not reply
within a week, the ticket will be closed.

  - **Do not** assign the task to anyone (unless they've told you to).
  - **Do not** change the status.
  - **Do not** change the priority.
  - **Do not** change the edit policy.
  - **Do not** edit parent or subtasks.

The ticket will then go through the ["Life of a
Bug"](../../Process/A_Bugs_Life.md). Once the bug is confirmed the
developers will try to fix this as soon as possible. Please be aware
that the developers get many bug reports and the more serious ones are
being prioritized. It may take a while until the issue is fixed.

*Please be kind*, many of the people helping on the bug tracker are
volunteers and our developers are working as hard as they can.
