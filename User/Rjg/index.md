## Personal Info

  -   
    **Name:**  
    :Robert Gützkow

<!-- end list -->

  -   
    **On developer.blender.org:**  
    :[rjg](https://developer.blender.org/p/rjg/)

<!-- end list -->

  -   
    **On blender.chat:**  
    :[rjg](https://blender.chat/direct/rjg/)

<!-- end list -->

  -   
    **On Blender's StackExchange:**  
    :[Robert
    Gützkow](https://blender.stackexchange.com/users/74827/robert-g%C3%BCtzkow)

<!-- end list -->

  -   
    **On Twitter:**
      -   
        [@robertguetzkow](https://twitter.com/robertguetzkow)

## Subpages

  - [Weekly Reports](Reports/index.md)
  - [How to write a bug report](HowToBugReport.md)
  - [Template texts for bug triaging](TemplateTexts.md)

## Drafts

  - [How to triage bugs](HowToHelpTriagingBugs.md)
  - [Analyzing a Windows Memory Dump with
    WinDbg](WindowsMemoryDump.md)
