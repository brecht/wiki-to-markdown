# Template texts for bug triaging

## Incomplete or invalid bug report

### Incomplete bug report

    This report does not contain the requested information, which are required for us to investigate the issue. Please read [[ https://wiki.blender.org/wiki/User:Rjg/HowToBugReport | this guide ]], it may help you write a bug report that allows us to fix your problem.
    
    In case you believe that you've found a bug, please create a new bug report through //Help > Report a Bug// in Blender. We expect all bug reports to follow the [[ https://developer.blender.org/maniphest/task/edit/form/1/ | submission template ]]. Please provide **system information** (operating system, GPU, driver version), **affected Blender version**, previous **Blender version that worked properly**, a **short and precise description of the issue**, step by step **instructions to reproduce it** and, if relevant, **a minimal .blend file that showcases the problem**. 

### Incomplete bug report and request for support

    This report does not contain the requested information, which are required for us to investigate the issue. Please read [[ https://wiki.blender.org/wiki/User:Rjg/HowToBugReport | this guide ]], it may help you write a bug report that allows us to fix your problem.
    
    If you need help learning or using Blender, please post a question on one of our [[ https://www.blender.org/community/ | community websites ]], for instance [[ https://blender.stackexchange.com/ | Blender's StackExchange ]], [[ https://blenderartists.org/ | blenderartists.org ]] or the [[ https://blender.chat/channel/support | #support channel on Blender chat ]]. The bug tracker is only for bug reports.
    
    In case you believe that you've found a bug, please create a new bug report through //Help > Report a Bug// in Blender. We expect all bug reports to follow the [[ https://developer.blender.org/maniphest/task/edit/form/1/ | submission template ]]. Please provide **system information** (operating system, GPU, driver version), **affected Blender version**, previous **Blender version that worked properly**, a **short and precise description of the issue**, step by step **instructions to reproduce it** and, if relevant, **a minimal .blend file that showcases the problem**.

### Performance problem

    While we do continue to work on improving performance in general, potential performance improvements are not handled as bug reports. If you have found a significant performance regression compared to a recent, previous release of Blender, then this would be a bug.
    
    In case you believe that you've found a bug, please create a new bug report through //Help > Report a Bug// in Blender. We expect all bug reports to follow the [[ https://developer.blender.org/maniphest/task/edit/form/1/ | submission template ]]. Please provide **system information** (operating system, GPU, driver version), **affected Blender version**, previous **Blender version that worked properly**, a **short and precise description of the issue**, step by step **instructions to reproduce it** and, if relevant, **a minimal .blend file that showcases the problem**. 

## Unsupported system

### Unsupported Nvidia GeForce GPU - Running Blender

    The [[ https://en.wikipedia.org/wiki/List_of_Nvidia_graphics_processing_units | GeForce _ series ]] predates the [[ https://en.wikipedia.org/wiki/GeForce_400_series | Nvidia GeForce 400 series ]] which is the [[ https://www.blender.org/download/requirements/ | minimum requirement ]] for running Blender 2.9x with a GeForce GPU.
    
    > **Supported Graphics Cards**
    >   - **NVIDIA**: **GeForce 400 and newer**, Quadro Tesla GPU architecture and newer, including RTX-based cards, with NVIDIA drivers (list of all [[ https://en.wikipedia.org/wiki/GeForce#GeForce_400_series_and_500_series | GeForce ]] and [[ https://en.wikipedia.org/wiki/Nvidia_Quadro#Quadro_FX_(with_CUDA_and_OpenCL,_but_no_Vulkan) | Quadro ]] GPUs)
    >   - **AMD**: GCN 1st gen and newer ([[ https://en.wikipedia.org/wiki/List_of_AMD_graphics_processing_units | list of all AMD GPUs ]])
    >   - **Intel**: Haswell and newer ([[ https://en.wikipedia.org/wiki/List_of_Intel_graphics_processing_units | list of all Intel GPUs ]])
    >   - **macOS**: version 10.13 or newer with supported hardware
    
    Unfortunately, this means that your GPU is below the minimum requirements and we don't provide support for it. Graphics glitches and even crashes due to unpatched bugs in the graphics driver can occur on unsupported hardware. You may have to use a [[ https://download.blender.org/release/ | previous release of Blender ]] with lower requirements.
    
    Please create a new report in case the issue is also happening on a system that fulfills the minimum requirements and uses a current graphics driver.

### Unsupported AMD GPU - Running Blender

    The AMD _ is from the [[ https://en.wikipedia.org/wiki/List_of_AMD_graphics_processing_units | AMD _ series ]] and has the old TeraScale 2 micro-architecture. The [[ https://www.blender.org/download/requirements/ | minimum requirements ]] for Blender 2.8x and 2.9x state that [[ https://en.wikipedia.org/wiki/List_of_AMD_graphics_processing_units#Features_Overview | GCN first generation or later ]] is required for running Blender.
    
    > **Supported Graphics Cards**
    >   - **NVIDIA**: GeForce 400 and newer, Quadro Tesla GPU architecture and newer, including RTX-based cards, with NVIDIA drivers (list of all [[ https://en.wikipedia.org/wiki/GeForce#GeForce_400_series_and_500_series | GeForce ]] and [[ https://en.wikipedia.org/wiki/Nvidia_Quadro#Quadro_FX_(with_CUDA_and_OpenCL,_but_no_Vulkan) | Quadro ]] GPUs)
    >   - **AMD**: **GCN 1st gen and newer** ([[ https://en.wikipedia.org/wiki/List_of_AMD_graphics_processing_units | list of all AMD GPUs ]])
    >   - **Intel**: Haswell and newer ([[ https://en.wikipedia.org/wiki/List_of_Intel_graphics_processing_units | list of all Intel GPUs ]])
    >   - **macOS**: version 10.13 or newer with supported hardware
    
    Unfortunately, this means that your GPU is below the minimum requirements and we don't provide support for it. Graphics glitches and even crashes due to unpatched bugs in the graphics driver can occur on unsupported hardware. You may have to use a [[ https://download.blender.org/release/ | previous release of Blender ]] with lower requirements.
    
    Please create a new report in case the issue is also happening on a system that fulfills the minimum requirements and uses a current graphics driver.

### Unsupported Nvidia GPU - Rendering

    GPU rendering with Cycles requires a compute capability of 3.0 and higher for Nvidia graphics cards. The Nvidia _ is from the [[ https://en.wikipedia.org/wiki/List_of_Nvidia_graphics_processing_units | GeForce _ series ]] and has a CUDA compute capability of _. Therefore, it is not supported as GPU rendering device.
    The requirements for GPU rendering are documented in [[ https://docs.blender.org/manual/en/latest/render/cycles/gpu_rendering.html#supported-hardware | Blender's manual ]]. Please note that they are higher than the [[ https://www.blender.org/download/requirements/ | minimum requirements ]] for running Blender.
    
    > **CUDA**
    > CUDA requires graphics cards with compute capability 3.0 and higher. To make sure your GPU is supported, see the [[ https://developer.nvidia.com/cuda-gpus#compute | list of Nvidia graphics cards ]] with the compute capabilities and supported graphics cards.
    
    > **OptiX**
    > OptiX requires graphics cards with compute capability 5.0 and higher. To make sure your GPU is supported, see the [[ https://developer.nvidia.com/cuda-gpus#compute | list of Nvidia graphics cards ]]. OptiX works best on RTX graphics cards with hardware ray tracing support (e.g. Turing and above). OptiX support is still experimental and does not yet support all features, see below for details.
    >
    > OptiX requires Geforce or Quadro RTX graphics card with recent Nvidia drivers.

### Unsupported AMD GPU - Rendering

    GPU rendering with Cycles requires a micro-architecture of GCN second generation or later for AMD graphics cards. The AMD _ is from the [[ https://en.wikipedia.org/wiki/List_of_Nvidia_graphics_processing_units | AMD _ series ]] and has the _ micro-architecture. Therefore, it is not supported as GPU rendering device.
    The requirements for GPU rendering are documented in [[ https://docs.blender.org/manual/en/latest/render/cycles/gpu_rendering.html#supported-hardware | Blender's manual ]]. Please note that they are higher than the [[ https://www.blender.org/download/requirements/ | minimum requirements ]] for running Blender.
    
    > **AMD**
    > OpenCL is supported for GPU rendering with AMD graphics cards. Blender supports graphics cards with GCN generation 2 and above. To make sure your GPU is supported, see the [[ https://en.wikipedia.org/wiki/Graphics_Core_Next#Generations | list of GCN generations ]] with the GCN generation and supported graphics cards.
    >
    > On Windows and Linux, the latest Pro drivers should be installed from the [[ https://www.amd.com/en/support | AMD website ]].

### macOS GPU rendering - Blender 2.9x

    Unfortunately it is currently not possible to use Cycles with GPU rendering on macOS in Blender 2.9x.
    
    Apple has deprecated their OpenCL compiler, which is why Cycles doesn't support GPU rendering on AMD GPUs in Blender 2.80 and later. This was originally [[ https://lists.blender.org/pipermail/bf-committers/2018-December/049695.html | announced on Blender's bf-committers mailing list ]]. This limitation is also documented in [[ https://docs.blender.org/manual/en/latest/render/cycles/gpu_rendering.html#amd | Blender's manual ]] and the [[ https://wiki.blender.org/wiki/Reference/Release_Notes/2.80/Cycles#GPU_rendering | release notes for Blender 2.80 ]].
    
    > **AMD**
    > OpenCL is supported for GPU rendering with AMD graphics cards. Blender supports graphics cards with GCN generation 2 and above. To make sure your GPU is supported, see the [[ https://en.wikipedia.org/wiki/Graphics_Core_Next#Iterations | list of GCN generations ]] with the GCN generation and supported graphics cards.
    > 
    > **AMD OpenCL GPU rendering is supported on Windows and Linux, but not on macOS.**
    
    > Due to OpenCL compiler bugs and discontinuation of OpenCL by Apple it was disabled on macOS platform. Other platforms still support OpenCL.
    
    Additionally, Nvidia has discontinued the development of the CUDA Toolkit for macOS. The last version that was release for it was [[ https://docs.nvidia.com/cuda/archive/10.2/cuda-toolkit-release-notes/index.html#cuda-general-new-features | 10.2 ]]. Consequentially, Blender 2.9x does not include support for CUDA rendering on macOS. This change is documented in the [[ https://wiki.blender.org/wiki/Reference/Release_Notes/2.90/Cycles#GPU_Rendering | release notes ]] and the [[ https://docs.blender.org/manual/en/dev/render/cycles/gpu_rendering.html#gpu-rendering | manual ]].
    
    Until Cycles is ported to Apple's Metal API through MoltenVK, you will either have to use [[ https://download.blender.org/release/Blender2.83/ | Blender 2.83 ]] or use another render engine that already supports the API and your GPU.

## Missing information

### GPU related crash - Windows

    Please open Blender's installation directory and double click on the `blender_debug_gpu.cmd`. This will start Blender in debug mode and create log files. Try to make Blender crash again. Once it crashes the Windows Explorer should open and show you up to two files, a debug log and the system information. Add them to your bug report by clicking on the upload button as shown in the screenshot below or via drag and drop. Please also upload the crash log located in `C:\Users\[your username]\AppData\Local\Temp\[project name].crash.txt` (or simply type `%TEMP%` into the path bar of the Windows Explorer).
    
    {F8190038}
