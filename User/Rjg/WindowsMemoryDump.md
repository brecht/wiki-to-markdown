# Analyzing a Windows Memory Dump with WinDbg

This is a small guide on how to get a stack trace from a \`.dmp\` file
with WinDbg in case Blender is unable to write a \`.crash.txt\`. Visual
Studio can be used to view \`.dmp\` files as well, but isn't covered
here.

## Preparation

In order to use WinDbg you should set up two directories and environment
variables pointing to them. They will be used to store the symbols
downloaded from the symbol server and for caching. In this guide,
\`C:\\symbols\` is used for storing the symbols and \`C:\\SymCache\` for
the cache, but you can replace these two directories with any other one
of your choice. Create the two directories, then add the following two
environment variables (adjust the path if you are not using the
suggested directories).

\`\_NT\_SYMBOL\_PATH=srv\*C:\\symbols\*<https://msdl.microsoft.com/download/symbols>\`

\`\_NT\_SYMCACHE\_PATH=C:\\SymCache\`

Additionally, you will need a copy of the exact Blender version that the
memory dump was created from. Blender is shipping a with a
\`blender.pdb\` file that contains a stripped version of the symbols.
WinDbg will be looking for a file named \`blender\_private.pdb\` though.
In order to get the stack trace with function names instead of memory
addresses, you will need to create a copy of the \`blender.pdb\` file
and rename it to \`blender\_private.pdb\`.

## Analyzing the Memory Dump

Open WinDbg and load the \`.dmp\` file. Next the debug symbols of
Blender need to be loaded, this is accomplished through the \`.sympath+
C:\\path\\to\\Blender\` command which adds a new directory to the paths
WinDbg checks for symbols. Replace the path with the actual path to the
Blender version, matching the one used in the \`.dmp\` file. After the
command has been run, WinDbg should automatically load Blender's
symbols. If it doesn't load the symbols, try to run \`.reload /f
C:\\path\\to\\Blender\\blender.exe\`.

Once this is set up you should be able to run \`\!analyze -v\` and get a
proper stack trace with function names.

If something doesn't work you can enable verbose error reporting through
\`\!sym noisy\` which should show where symbol loading fails to work.
The detailed output can be disabled with \`\!sym quiet\`.
