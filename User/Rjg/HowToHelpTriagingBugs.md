# How to help triage bugs?

This document explains how you can help on the bug tracker by ensuring
bug reports from fellow Blender users are understandable, complete and
reproducible. Given the ever growing number of Blender users and
simulatenous increase in reports on the bug tracker, it is vital that
the community helps with bug triaging, so that the developers can fix
bugs efficiently and only have to spend time on reports that describe
actual bugs.

## Introduction

### Prerequisites

In order to help you will need to:

  - Download the newest daily build of Blender version from
    [builder.blender.org](https://builder.blender.org/download/) or
    [compile Blender
    yourself](https://wiki.blender.org/wiki/Building_Blender) (only
    recommended for developers).
  - Create an account on
    [developer.blender.org](https://developer.blender.org/).
  - Join the
    [blender-triagers](https://blender.chat/channel/blender-triagers)
    channel.
  - Read the following document.

Keep in mind that Blender doesn't automatically update, you need to
ensure you're using the correct version before triaging a bug.

### What can/should I do?

After the introduction this document contains four sections that explain
how you can help, depending on the level of permissions that you have
been granted on the
[developer.blender.org](https://developer.blender.org/).

  - **Community Member**
      - Everybody with an account on
        [developer.blender.org](https://developer.blender.org/)
  - **Moderator**
      - Members of the
        [Moderator](https://developer.blender.org/project/view/1/)
        project
  - **Developer**
      - Members of the [BF
        Blender](https://developer.blender.org/project/view/2/) project
  - **Triager**
      - Member of the triaging team, which is not formally defined. They
        are treated like developers in this document.
  - **Module Member**
      - Members of the modules as listed on the
        [Modules](https://wiki.blender.org/wiki/Modules) page

The [Triaging
Playbook](https://wiki.blender.org/wiki/Process/Bug_Reports/Triaging_Playbook)
contains instructions that explain how common cases should be handled.
However, they are written from the perspective of a developer and not
everything on that page should or can be done by a community member.

Please remember that the bug tracker operates on a good-faith basis. You
may be able to perform some actions even with a basic user account, such
as assigning tasks to a developer, despite not being supposed to. We
trust our community members to follow the rules of the bug tracker and
follow the guidance provided on this page.

### Where are the bug reports?

On the front page of
[developer.blender.org](https://developer.blender.org/) are *Recent
Tasks*. Most of these tasks are bug reports that have been created
lately and will need to be triaged. The current status and priority is
indicated by both the color of the icon in front of each task and the
tooltip that will appear when hovering over the icon.

You can create a customized queries on the
[Maniphest](https://developer.blender.org/maniphest/) page by clicking
on the *Edit Query* button on the top right-hand side.

### What are subtypes?

Subtypes define what kind of a task it is. Not every task is a bug
report.

  - **Report**: A bug report.
  - **Bug**: A bug report that is will be addressed in the near future.
  - **Known Issue**: An issue that is already known but may take a
    longer time to be addressed.
  - **Design**: A design tasks describes how a new feature is intended
    to be designed.

### What does each status mean?

Status is the current state the bug report is in.

  - **Needs Triage**: Default status when a task is created. Needs to be
    checked for completeness and reproducibility.
  - **Needs Information from User**: The bug cannot be reproduced.
    Either the user that opened the task or fellow community members
    that experience the same problem need to provide the missing
    information.
  - **Needs Information from Developers**: The report is complete,
    tagged and reproducible, but the input of the module team is need to
    decide how the ticket should be handled. This status hands over the
    responsibility from the triaging team to the responsible module and
    should therefore be used sparingly. For instance this could be
    necessary when seemingly unusual behavior can be reproduced by the
    triagers, but it is unclear whether this is an intentional design
    decision, known limitation or bug.
  - **Confirmed**: The report is complete, tagged, reproducible, has
    been given a (preliminary) priority, and a triager or developer was
    able to confirm the bug.
  - **Resolved**: The bug has been fixed.
  - **Archived**: The task is closed and won't be investigated further.

### What are tags?

Tags are a way to associated projects and modules with a ticket. This
allows to organize the development and ensure that the right developers
see the bug reports for the area they are working on. An overview of the
modules can be found both in the sidebar on
[developer.blender.org](https://developer.blender.org/) and the
[module](https://wiki.blender.org/wiki/Modules) page.

### How does a (correct) bug report look like?

All bug reports should follow the [submission
template](https://developer.blender.org/maniphest/task/edit/form/1/).
Each of the lines should be filled with the requested information, which
means a complete bug report should provide:

  - A short and descriptive title that explains the problem
  - Operating system and version
  - Graphics card and graphics driver version
  - Broken Blender version
  - (Optionally) Previous Blender version that worked properly
  - A short description of the error
  - Step by step instructions to reproduce the error from the default
    startup file (or in the provided project file)
  - (Optionally) Additional information such as:
      - A *minimal* project file, that is reduced to the bare minimum
        necessary to demonstrate the problem
      - Debug logs or stack trace
      - Screenshots, video or GIF

Bug reports for add-ons should additionally include its name and
version.

#### Example of a complete bug report

**Title**: Crash using color drag and drop in the image editor

    **System Information**
    Operating system: Windows-10-10.0.18362 64 Bits
    Graphics card: GeForce GTX 1080 Ti/PCIe/SSE2 NVIDIA Corporation 4.5.0 NVIDIA 436.30
        
    **Blender Version**
    Broken: 
     - version: 2.82 (sub 7), branch: master, commit date: 2020-03-12 05:06, hash: `rB375c7dc4caf4`
     - version: 2.83 (sub 12), branch: master, commit date: 2020-04-06 15:48, hash: `rB71a52bbe2a2e`
    Worked: 2.80
        
    **Short description of error**
    Blender crashes when dragging and dropping a color onto the texture in the image editor.
        
    **Exact steps for others to reproduce the error**
    1. Open the Texture Paint workspace.
    2. Create a new texture in the image editor.
    3. Put the mouse cursor over the current color in the header and drag it onto the texture.

An ideal bug report should check the following boxes:

  - The report was created through *Help \> Report a Bug* in Blender or
    for add-ons, through the *Report a Bug* button in the add-on's
    preferences. This automatically fills in OS, GPU and graphics driver
    version and affected Blender version with commit. For add-ons it
    fills in the name and version. Should it be apparent that these
    fields were filled in by the user, suggest to them to use *Help \>
    Report a Bug* for future bug reports.
  - All of the requested information are provided in the ticket and not
    scattered throughout the comments.
  - It is possible to reproduce the issue with the provided instructions
    and \`.blend\` file without any guesswork. The steps are clear and
    unambiguous.
  - The step by step instructions and \`.blend\` file allow to reproduce
    the issue with only a few clicks.
  - If a \`.blend\` file is necessary, then it should only contain
    content that is required to reproduce the issue. No large production
    files are allowed, except for special circumstances that the
    developers will handle.

## Helping as a community member

This section explains how you can help bug triaging when you are a
regular community member without any special permissions on the bug
tracker. If you find a ticket that has the status *Needs Triage* or
*Needs Information from User* you can help in the following ways.

### Needs Triage

How to help if the ticket status is *Needs Triage*.

#### Is it a bug report?

Check whether it is actually a bug report or falls into one of the
following categories:

  - Feature Request
  - Feedback
  - Request for Support (Asking a question on how to use Blender)
  - Spam

If it isn't a bug report set the status to *Archived* and post a comment
as instructed by the [Triaging
Playbook](https://wiki.blender.org/wiki/Process/Bug_Reports/Triaging_Playbook#Low_quality_report).

**Important:** Do not close design tasks, this is only meant for reports
are supposed to be bug reports.

#### Is it complete?

Check that the report contains all the required information (see [How
does a (correct) bug report look
like?](HowToHelpTriagingBugs.md#How_does_a_.28correct.29_bug_report_look_like.3F)).
If it doesn't, set the status to *Needs Information from User* and
explain what information is missing or unclear. Also encourage that the
original report should be updated, instead of adding the information in
a comment. If there is nearly no information at all in the report (e.g.
only the title) or it isn't following the submission template, the
report can be closed as *Archived*. Post a comment as instructed by the
[Triaging
Playbook](https://wiki.blender.org/wiki/Process/Bug_Reports/Triaging_Playbook#Low_quality_report).

#### Does the system fulfill the minimum requirements?

Check that the reported hardware fulfills the [minimum
requirements](https://www.blender.org/download/requirements/) for
running Blender. If it doesn't then close the report as *Archived* and
follow the instructions provided by the [Triaging
Playbook](https://wiki.blender.org/wiki/Process/Bug_Reports/Triaging_Playbook).

#### Is it up to date?

Check that the Blender version in the report is still being maintained.
It should be the [current Blender
release](https://www.blender.org/download/), [current LTS
version](https://www.blender.org/download/lts/) or [one of the versions
currently under development](https://builder.blender.org/download/)). If
the report is for an older version of Blender, instruct the user to
install a more recent version and check if the problem still exists.

Check if the graphics driver is a relatively current version, especially
if the reported issue is a crash or about graphics glitches. As a rule
of thumb it shouldn't be older than a year or the most recent driver
available for the GPU, in case it is legacy hardware that is still
supported by Blender but doesn't receive updates by the manufacturer
anymore. The current driver version can be found on the websites of
[Nvidia](https://www.nvidia.com/Download/index.aspx?lang=en),
[AMD](https://www.amd.com/de/support) and
[Intel](https://downloadcenter.intel.com/product/80939/Graphics).

#### Is it understandable?

If you are unable to understand the report and believe that others may
face the same problem, ask the person who reported the issue to provide
a more concise explanation (short, well formatted) and set the status to
*Needs Information from User*. You may also ask for additional
screenshots, a .blend file or a .gif file showing the issue.

#### Is it reproducible?

Try to reproduce the bug with the provided instructions. If you cannot
reproduce it, but the instructions appear to be complete, leave the
ticket for a developer to triage. If you can reproduce it in the most
recent Blender version, leave a comment including your system
information and Blender version. You can also try to check if you are
able to reproduce it in [previous Blender
releases](https://download.blender.org/release/). Should you be able to
reproduce the bug in one version and not in another, then add this
information to the report. This is helps to narrow it down when the bug
was introduced and thereby what the cause might be.

#### Is it simple enough?

If you were able to reproduce the problem, see if you can simplify:

  - The steps by step instructions to reproduce the issue
  - The project file used to demonstrate the issue

Add your findings to the report.

### Needs Information from User

If the status of the ticket is *Needs Information from User* and you are
able to reproduce the issue on your system, add the missing/requested
information to the report and set the status back to *Needs Triage*.
Make sure that it really is the same issue though, as otherwise this
makes matters more complicated.

### What you shouldn't do\!

  - **Do not** assign the task to a developer (unless they have
    instructed you to).
  - **Do not** set the status to *Confirmed* or *Needs Information from
    Developers*
  - **Do not** set the status to *Archived* just because you weren't
    able to reproduce the issue. Some issues are platform or hardware
    specific.

## How to help as a Moderator

This section explains how you can help bug triaging when you are part of
the [Moderator](https://developer.blender.org/project/view/1/) project.

### Needs Triage

Follow the same instructions as [Helping as a community member - Needs
Triage](HowToHelpTriagingBugs.md#Needs_Triage) apply,
but additionally:

1.  Edit the bug report to include all relevant information that may
    have been added in the comments.

\# Ensure that the report is properly formatted and readable

\#\# Use the correct syntax to format lists properly

\#\# Enclose code in tags

    ```lines
    content here 
    ```

1.  1.  Fix spelling or grammar if it is distracting or hinders
        understanding

2.  Remove parts from the report if they are not relevant for the issue
    at hand, for instance if superfluous hardware information are added
    or if feature requests or feedback is interspersed in a valid bug
    report.

3.  Check if the report is a duplicate. If it is, merge the ticket into
    the older report. If there current report is more informative and
    the older report has not received any attention yet, you may also
    merge the older into the newer ticket. Only merge reports if you are
    certain that they are the same issue.

For everything else, follow the [Triaging
Playbook](https://wiki.blender.org/wiki/Process/Bug_Reports/Triaging_Playbook).

### Needs Information from User

Follow the same instructions as [Helping as a community member - Needs
Information from
User](HowToHelpTriagingBugs.md#Needs_Information_from_User).

### What you shouldn't do\!

Follow the same instructions as [Helping as a community member - What
you shouldn't
do\!](HowToHelpTriagingBugs.md#What_you_shouldn.27t_do.21).

## How to help as a Triager or Developer

This section explains how you can help when you are part of the [BF
Blender](https://developer.blender.org/project/view/2/) project.

### Needs Triage

Follow the same instructions as [Helping as a Moderator - Needs
Triage](HowToHelpTriagingBugs.md#Needs_Triage2) apply,
but additionally:

  - Should the report indicate that Blender prints an error message to
    the console, which isn't included in the bug report, instruct the
    user to create a debug log. Set the status to *Needs Information
    from User*.
  - Should Blender crash, but you are unable to reproduce the crash,
    instruct the user to create a debug log and upload the debug log,
    full system information and crash log. Set the status to *Needs
    Information from User*.
  - If you are are able to reproduce the issue, identify a Blender
    version where the problem did not occur, if one exists. Optionally,
    if you are able to, use `git bisect` to find the commit that
    introduced the bug.

If the issue is reproducible, the report contains all relevant
information and you are sure that it is a bug:

1.  Tag the module that is responsible for fixing the bug.
2.  Assign a preliminary priority based on the severity of the bug (see
    [Bug Triaging
    Playbook](../../Process/Bug_Reports/Triaging_Playbook.md)).
3.  Set the status to *Confirmed*.

If the issue is reproducible, the ticket contains all relevant
information but you are unsure if this is a bug:

1.  Ask other triagers about it in the
    [blender-triagers](https://blender.chat/channel/blender-triagers)
    channel first.
2.  If it requires a decision by a core developer of the affected
    module, set the status to *Needs Information from Developers*. This
    should only be used when it unclear how to proceed with the issue at
    hand.

For everything else, follow the [Triaging
Playbook](https://wiki.blender.org/wiki/Process/Bug_Reports/Triaging_Playbook).

### Needs Information from User

If the status of the ticket is *Needs Information from User* and you are
able to reproduce the issue on your system, add the missing/requested
information to the report and follow the instructions from the previous
section to confirm the report.

## How to help as a Module Member

This section explains how you can help bug triaging when you are part of
the [Modules](https://wiki.blender.org/wiki/Modules) project.

### Needs Information from a Developer

If you are a module member the ticket is tagged for, this report needs
your input on how to proceed. It is your decision whether the problem is
considered a bug and to set the appropriate status and subtype
accordingly.
