# The Troubleshooting FAQ

This is a collection of common problems when using Blender that may look
like a bug but actually aren't.
