## Finding Reviewers

If you do not have prior agreement from a
[Developer](Contact) to review your patch, then leave
“Reviewer” blank and instead use only the **Tags** and **Subscribers**
to notify the appropriate modules and developers. To find out who is
responsible for an area of code you can check the [Module Owners
List](Process/Module_Owners/List) or browse the [Commit
History](https://developer.blender.org/diffusion/).
