# Harley Acheson

#### Volunteer Developer

  
\* [developer.blender.org](https://developer.blender.org/)
[@harley](https://developer.blender.org/p/harley/) |
[Revisions](https://developer.blender.org/people/revisions/20967/) |
[Commits](https://developer.blender.org/people/commits/20967/) |
[Tasks](https://developer.blender.org/people/tasks/20967/) |
[Activity](https://developer.blender.org/feed/?userPHIDs=PHID-USER-5s2g7xqrqtmxcrw6eaab)

  - [devtalk.blender.org](https://devtalk.blender.org/)
    [Harleya](https://devtalk.blender.org/) |
    [Activity](https://devtalk.blender.org/u/harleya/activity)
  - [blender.chat](https://blender.chat/channel/blender-coders)
    [@Harleya](https://blender.chat/direct/Harleya)
  - [blenderartists.org](https://blenderartists.org/)
    [Harley](https://blenderartists.org/u/harley)
  - Email: <harley@hacheson.com>
  - Facebook:
    [facebook.com/harley.acheson](https://www.facebook.com/harley.acheson)
  - Strava: [Cervélo S5, Cannondale SuperX, Specialized SX
    Trail](https://www.strava.com/athletes/23497312)
  - Location: [Cowichan
    Valley](https://www.google.com/maps/@48.898801,-123.7466172,10z),
    [Vancouver
    Island](https://www.google.com/maps/@49.6222093,-124.8984774,7.75z),
    [British
    Columbia](https://www.google.com/maps/@55.0723918,-125.0111362,5.25z),
    [Canada](https://www.google.com/maps/@54.8577813,-96.0627005,4.75z)
  - Time Zone: [Pacific Standard (UTC-8)](https://time.is/UTC-8)
  - Platform: Windows
  - [Weekly Reports](WeeklyReports/index.md)

## Interests

User Interface, pixel-perfect layout, spelling, grammar, fonts, icons,
images, input, internationalization, translation, accessibility.

## Unapproved

  - Unicode Codepoint Entry.
    ([D13447](https://developer.blender.org/D13447))
  - Show User Fonts combined with System Fonts.
    ([D13293](https://developer.blender.org/D13293))
  - A File Loading Dialog.
    ([D10484](https://developer.blender.org/D10484))
  - Nicer Operator Confirm Dialog
    ([D10955](https://developer.blender.org/D10955))
  - Improved Font previews
    ([D12032](https://developer.blender.org/D12032))
  - Close After Area Duplicate
    ([D11093](https://developer.blender.org/D11093))
  - Scrollbars that dynamically change width.
    ([D6505](https://developer.blender.org/D6505))
  - Highlight currently-selected Enum
    ([D6655](https://developer.blender.org/D6655))
  - Various changes to Topbar menus.
    ([D8706](https://developer.blender.org/D8706)). [User poll on the
    subject](https://devtalk.blender.org/t/feedback-wanted-on-the-app-menu-poll/16868)
  - Collapse wide enum lists to single column if not enough space.
    ([D6362](https://developer.blender.org/D6362))
  - Directional Box Selection
    ([D10525](https://developer.blender.org/D10525))
  - Improved Poll function for FILE\_OT\_rename
    ([D10554](https://developer.blender.org/D10554))
  - Changes to Camera View Guides and Borders
    ([D11900](https://developer.blender.org/D11900))
  - Use Fonts in File Browser Font Lists.
    ([D12606](https://developer.blender.org/D12606))
  - Change position of Measure/Ruler text.
    ([D10128](https://developer.blender.org/D10128))
  - Adding meta-data, like video sizes and FPS, to thumbnails.
    ([D9454](https://developer.blender.org/D9454))
  - Experimenting with ideas for Camera View.
    ([D9506](https://developer.blender.org/D9506))
  - Windows File Ownership (local and domain) in case we need that one
    day. ([D9403](https://developer.blender.org/D9403))
  - Improved Contrast for 3DView Overlays
    ([D11967](https://developer.blender.org/D11967))

## 3.2

  - Add "OneDrive" Shortcut to Windows System List
    ([D11133](https://developer.blender.org/D11133))
  - UI: Allow AltGr Key + C,V,X Text Input
    ([D13781](https://developer.blender.org/D13781))
  - IME Cleanup: Unused GHOST\_TEventImeData Member
    ([D11799](https://developer.blender.org/D11799))
  - BLF: Reduction of use of BLF\_DRAW\_STR\_DUMMY\_MAX
    ([D13793](https://developer.blender.org/D13793))
  - BLF: UI\_fontstyle\_draw Usage
    ([D13794](https://developer.blender.org/D13794))
  - Fix T85706: wm\_window\_make\_drawable update DPI
    ([D10483](https://developer.blender.org/D10483))
  - Win32: Initialize GHOST\_WindowWin32 Members
    ([D13886](https://developer.blender.org/D13886))
  - Win IME: Ideographic Full Stop to Decimal Point
    ([D13903](https://developer.blender.org/D13903))
  - Fix T93626: Win IME Chinese Numpad Decimal
    ([D13902](https://developer.blender.org/D13902))
  - Fix T62651: Win32 Multiple Adapters Warning
    ([D13885](https://developer.blender.org/D13885))
  - BLF: blf\_glyph\_cache\_free Made Static
    ([D13395](https://developer.blender.org/D13395))
  - BLF: Save fixed\_width value in cache
    ([D13226](https://developer.blender.org/D13226))
  - BLF: Removal of blf\_font\_draw\_ascii Declaration
    ([D13624](https://developer.blender.org/D13624))
  - BLF: Default Size as Float
    ([D13230](https://developer.blender.org/D13230))
  - BLF: Cleanup blf\_glyph\_cache\_find & blf\_font\_size
    ([D13374](https://developer.blender.org/D13374))
  - BLF: Enable Filtering of woff and woff2 Fonts
    ([D13822](https://developer.blender.org/D13822))

## 3.1

  - Preferences / File Paths / Fonts now starts with an initial default
    path for Mac & Linux, as already done for Windows.
    ([D12802](https://developer.blender.org/D12802)).
  - Open File Browser in thumbnail view when browsing for fonts.
    ([D13040](https://developer.blender.org/D13040)).
  - Font point sizes can now be set using floating-point values.
    ([D8960](https://developer.blender.org/D8960)).
  - Improved feedback while doing interactive (live) area splitting
    ([T94071](https://developer.blender.org/T94071)).
  - Fix T93368: Dragging Blends Without Previews
    ([D13383](https://developer.blender.org/D13383)).
  - Fix T89587: Don't Change Line Width For Previews
    ([D13717](https://developer.blender.org/D13717))

## 3.0

  - Improved mouse cursor feedback during Area Split and Join at
    unsupported locations.
    ([3b1a16833b](https://projects.blender.org/blender/blender/commit/3b1a16833b58)).
  - Optionally use a capture of the entire main window as the blend
    preview thumbnail.
    ([58632a7f3c](https://projects.blender.org/blender/blender/commit/58632a7f3c0f)).

![../../images/WorkspaceThumbs.png](../../images/WorkspaceThumbs.png
"../../images/WorkspaceThumbs.png")

  - Increased resolution of blend file thumbnails.
    ([bf0ac711fd](https://projects.blender.org/blender/blender/commit/bf0ac711fde2)).

![../../images/BlendThumbnailSizeComparison.png](../../images/BlendThumbnailSizeComparison.png
"../../images/BlendThumbnailSizeComparison.png")

  - Area maintenance "Close" can now be scripted.
    ([9290b41381](https://projects.blender.org/blender/blender/commit/9290b41381fd))
  - Less jiggling of contents when moving nodes.
    ([400605c3a6](https://projects.blender.org/blender/blender/commit/400605c3a6a8)).
  - Resizing areas now snap (with Ctrl) to more consistent locations.
    ([e5ed9991ea](https://projects.blender.org/blender/blender/commit/e5ed9991eaf0)).
  - Substantial speed increases for interface text drawing.
    ([d5261e973b](https://projects.blender.org/blender/blender/commit/d5261e973b56),
    [0d7aab2375](https://projects.blender.org/blender/blender/commit/0d7aab2375e6))
  - Allow use of specialty and legacy fonts like Wingdings and Symbol.
    ([ae920d789e](https://projects.blender.org/blender/blender/commit/ae920d789ed3)).
  - Show Descriptive Font Names, not file names.
    ([8aa1c0a326](https://projects.blender.org/blender/blender/commit/8aa1c0a326a8),
    [b5bfb5f34c](https://projects.blender.org/blender/blender/commit/b5bfb5f34c12)).

![../../images/FontDisplayNames.png](../../images/FontDisplayNames.png
"../../images/FontDisplayNames.png")

  - Corner action zones allow joining any neighbors. Improved Header
    Context Menu.
    ([8b049e4c2a](https://projects.blender.org/blender/blender/commit/8b049e4c2a53),
    [c76141e425](https://projects.blender.org/blender/blender/commit/c76141e425aa)).

![../../images/BisectWithJoin.png](../../images/BisectWithJoin.png
"../../images/BisectWithJoin.png")

  - New 'Area Close' operator.
    ([8b049e4c2a](https://projects.blender.org/blender/blender/commit/8b049e4c2a53),
    [06e62adfb8](https://projects.blender.org/blender/blender/commit/06e62adfb8f2)).

![../../images/CloseOperator.png](../../images/CloseOperator.png
"../../images/CloseOperator.png")

  - If saving a file using CloseSave dialog, do not close if there is an
    error doing so, like if read-only.
    ([cfa20ff03b](https://projects.blender.org/blender/blender/commit/cfa20ff03bde)).
  - Capture object thumbnails at an oblique angle for better preview of
    the shapes.
    ([ecc7a83798](https://projects.blender.org/blender/blender/commit/ecc7a837982e)).

![../../images/PreviewOrientation.png](../../images/PreviewOrientation.png
"../../images/PreviewOrientation.png")

  - Do not create thumbnail previews of offline files, to avoid slow
    listings of Windows OneDrive folders.
    ([ee5ad46a0e](https://projects.blender.org/blender/blender/commit/ee5ad46a0ead)).

![../../images/OfflineFiles.png](../../images/OfflineFiles.png
"../../images/OfflineFiles.png")

  - Improved positioning of menu mnemonic underlines, especially when
    using custom font.
    ([0fcc063fd9](https://projects.blender.org/blender/blender/commit/0fcc063fd99c)).
  - "Render" Window now top-level (not child) on Windows platform, back
    to behavior prior to 2.93.
    ([bd87ba90e6](https://projects.blender.org/blender/blender/commit/bd87ba90e639)).
  - Improved placement of child windows when using multiple monitors
    with any above any others (Win32).
    ([d75e45d10c](https://projects.blender.org/blender/blender/commit/d75e45d10cbf)).
  - Windows users can now associate current installation with Blend
    files in Preferences.
    ([bcff0ef9ca](https://projects.blender.org/blender/blender/commit/bcff0ef9cabc)).

![../../images/Blend\_File\_Association.png](../../images/Blend_File_Association.png
"../../images/Blend_File_Association.png")

  - 3DView Statistics Overlay now shows Local statistics if you are in
    Local View
    ([c8e331f450](https://projects.blender.org/blender/blender/commit/c8e331f45003)).
  - Improved scaling of File Browser preview images.
    ([cb548329ea](https://projects.blender.org/blender/blender/commit/cb548329eaea)).

![../../images/PreviewScaling.png](../../images/PreviewScaling.png
"../../images/PreviewScaling.png")

  - Do not resize temporary windows (Preferences, Render, etc) if they
    are already open.
    ([643720f8ab](https://projects.blender.org/blender/blender/commit/643720f8abdb)).
  - Win32: Improved placement of windows when using multiple monitors
    that differ in DPI and scale.
    ([999f1f7504](https://projects.blender.org/blender/blender/commit/999f1f75045c)).
  - Easier Area Resizing
    ([84dcf12ceb](https://projects.blender.org/blender/blender/commit/84dcf12ceb7f)).
  - Open File Browser with Thumbnails for Fonts
    ([9cfffe8468](https://projects.blender.org/blender/blender/commit/9cfffe84681b)).
  - Change File Manager "Favorites" to "Bookmarks"
    ([a79c33e8f8](https://projects.blender.org/blender/blender/commit/a79c33e8f8f3)).

## 2.93

  - Transform arrow cursor improvements in shape and in response to line
    width changes.
    ([b4b02eb4ff](https://projects.blender.org/blender/blender/commit/b4b02eb4ff77)).

![../../images/TransformArrowComparison.png](../../images/TransformArrowComparison.png
"../../images/TransformArrowComparison.png")

  - Navigation Gizmo changes. Better indication of negative axes,
    consistent use of color and size to indicate orientation, ability to
    be resized.
    ([ded9484925](https://projects.blender.org/blender/blender/commit/ded9484925ed)).

![../../images/NavigateGizmoHighlight.png](../../images/NavigateGizmoHighlight.png
"../../images/NavigateGizmoHighlight.png")

  - Position Gizmo tooltips below their bounds so as to not obscure
    themselves.
    ([cf6d17a6aa](https://projects.blender.org/blender/blender/commit/cf6d17a6aa42)).

![../../images/GizmoTooltips.png](../../images/GizmoTooltips.png
"../../images/GizmoTooltips.png")

  - Use ellipsis character when truncating strings no matter how narrow.
    ([de3f369b30](https://projects.blender.org/blender/blender/commit/de3f369b30e5)).
  - Windows OS - Allow any input language, regardless of translated
    display language.
    ([f2781e1c7c](https://projects.blender.org/blender/blender/commit/f2781e1c7c82)).
  - Windows OS - Show child windows on top of parent windows.
    ([261fa052ac](https://projects.blender.org/blender/blender/commit/261fa052ac42)).
  - Temporary windows no longer take over the space of other windows.
    ([492e64c7bc](https://projects.blender.org/blender/blender/commit/492e64c7bcbd)).
  - Improved contrast for Status Bar report messages.
    ([694bc4d040](https://projects.blender.org/blender/blender/commit/694bc4d040a8)).

![../../images/Status\_Bar\_Reports.png](../../images/Status_Bar_Reports.png
"../../images/Status_Bar_Reports.png")

  - Window / "New Window" now opens with a single editor.
    ([be9842f65b](https://projects.blender.org/blender/blender/commit/be9842f65b85)).
  - Blender thumbnails now saved without "Camera View"-style
    Passepartout.
    ([9c395d6275](https://projects.blender.org/blender/blender/commit/9c395d6275a0)).
  - Improved positioning of child windows, relative to parents, when
    using multiple monitors.
    ([d447bd3e4a](https://projects.blender.org/blender/blender/commit/d447bd3e4a9a)).

## 2.91

  - Categorized List Menus get slight change in format with nicer
    headings.
    ([aa244a7a68](https://projects.blender.org/blender/blender/commit/aa244a7a68db)).

![../../images/Categorized\_Menu.png](../../images/Categorized_Menu.png
"../../images/Categorized_Menu.png")

## 2.90

  - Scene statistics available in 3D viewport with an (optional)
    "Statistics" Viewport Overlay.
    ([fd10ac9aca](https://projects.blender.org/blender/blender/commit/fd10ac9acaa0)).

![Statistics Overlay|600px](../../images/Statistics_Overlay.png
"Statistics Overlay|600px")

  - Status Bar now showing only version by default, but context menu can
    enable statistics, memory usage, etc.
    ([c08d847488](https://projects.blender.org/blender/blender/commit/c08d84748804)).

![Status Bar Context
Menu|600px](../../images/Status_Bar_Context_Menu.png
"Status Bar Context Menu|600px")

  - About Blender dialog added to the App menu to contain information
    like build, branch, and hash values. This declutters the Splash
    Screen and highlights only the release version.
    ([5b86fe6f33](https://projects.blender.org/blender/blender/commit/5b86fe6f3350)).

![../../images/About18.png](../../images/About18.png
"../../images/About18.png")

  - Area borders given much larger hit area to make them easier to
    resize.
    ([4e8693ffcd](https://projects.blender.org/blender/blender/commit/4e8693ffcddb))

![Increased Border Hit Area|600px](../../images/Border_Hit_Size.png
"Increased Border Hit Area|600px")

  - Support for Windows Shell Links (shortcuts) in File Browser.
    Extended Mac Alias usage. Better display of linked items.
    ([1f223b9a1f](https://projects.blender.org/blender/blender/commit/1f223b9a1fce))

![Shortcuts and Aliases|600px](../../images/Shortcuts.png
"Shortcuts and Aliases|600px")

  - Option to print text in bold or italics style, synthesized from a
    single base UI font.
    ([b74cc23dc4](https://projects.blender.org/blender/blender/commit/b74cc23dc478))

## 2.83

  - Support for file attributes and hidden files.
    ([1fb62d1272](https://projects.blender.org/blender/blender/commit/1fb62d1272db))
  - Show icons for special folder locations.
    ([1af8e0cc6c](https://projects.blender.org/blender/blender/commit/1af8e0cc6c47))
    ([a622e29a25](https://projects.blender.org/blender/blender/commit/a622e29a2541))
  - Improve toolbar width snapping
    ([ac7eb71089](https://projects.blender.org/blender/blender/commit/ac7eb710890))
  - Change Area Duplicate Icon
    ([452834f1e3](https://projects.blender.org/blender/blender/commit/452834f1e3a))
  - On Windows, restore app from minimized when closing from taskbar
    ([22ca8b8aee](https://projects.blender.org/blender/blender/commit/22ca8b8aee99))
  - Dynamically enable and disable Edit Menu items based on whether
    currently applicable
    ([b707504973](https://projects.blender.org/blender/blender/commit/b7075049732a))
  - Visual changes to Info Editor
    ([aa919f3e82](https://projects.blender.org/blender/blender/commit/aa919f3e8202))
  - Improved keyboard symbols for menus and status bar, following
    platform conventions.
    ([dc3f073d1c](https://projects.blender.org/blender/blender/commit/dc3f073d1c52))
    ([f051d47cdb](https://projects.blender.org/blender/blender/commit/f051d47cdbce))
  - Overflowing text lines now use ellipsis character to indicate line
    continuation.
    ([63d5b974cc](https://projects.blender.org/blender/blender/commit/63d5b974ccfa))
  - Larger Alert icons for dialogs
    ([a210b8297f](https://projects.blender.org/blender/blender/commit/a210b8297f5a))
  - Language Selection on "Quick Start" Splash screen
    ([a210b8297f](https://projects.blender.org/blender/blender/commit/a210b8297f5a))

## 2.82

  - Custom Face Orientation Colors, defined in the theme

![../../images/Screenshot\_2020-01-23\_at\_12.37.04.png](../../images/Screenshot_2020-01-23_at_12.37.04.png
"../../images/Screenshot_2020-01-23_at_12.37.04.png")

  - Various small tweaks to how the Text Editor looks
    ([8c6ce74239](https://projects.blender.org/blender/blender/commit/8c6ce742391))
  - The File Browser now uses special new icons for Volumes and System
    Lists
    ([7c2217cd12](https://projects.blender.org/blender/blender/commit/7c2217cd126))
