# Porting popular modifiers to Geometry Nodes - Final Report

## Summary

In this Summer of Code project, I ported multiple modifiers to nodes
like described in the proposal (see link below). I ported all proposed
modifies (see Changes and Additions), uploaded patches to
developer.blender.org and worked over them in multiple cleanup passes.
The developed nodes create precedences for porting other existing
modifiers and operators and add additional support for attributes. The
Mesh Inset node (formerly Extrude) was already showcased in the geometry
nodes attribute workshop.

## Project Links

  - [Project
    Proposal](https://wiki.blender.org/wiki/User:HobbesOS/GSOC2021/Proposal)
  - [Weekly
    Reports](https://devtalk.blender.org/t/gsoc-2021-porting-popular-modifiers-to-geometry-nodes-weekly-reports/)
  - [Build
    Windows](https://builder.blender.org/download/experimental/blender-3.0.0-alpha+soc-2021-porting-modifiers-to-nodes_all.cb66eb8da29f-linux.x86_64-release.tar.xz)
  - [Build MacOS
    (intel)](https://builder.blender.org/download/experimental/blender-3.0.0-alpha+soc-2021-porting-modifiers-to-nodes_all.cb66eb8da29f-darwin.x86_64-release.dmg)
  - [Build MacOS
    (Arm)](https://builder.blender.org/download/experimental/blender-3.0.0-alpha+soc-2021-porting-modifiers-to-nodes_all.cb66eb8da29f-darwin.arm64-release.dmg)
  - [Build
    Linux](https://builder.blender.org/download/experimental/blender-3.0.0-alpha+soc-2021-porting-modifiers-to-nodes_all.cb66eb8da29f-linux.x86_64-release.tar.xz)

## Branches

### Merged for testing

  - All - **\`soc-2021-porting-modifiers-to-nodes-all\`**

### Single

  - Solidify (Solidify) -
    **\`gsoc-2021-porting-modifiers-to-nodes-solidify\`**
  - Collapse, Unsubdivide, Dissolve (Decimate) -
    **\`soc-2021-porting-modifiers-to-nodes-decimate\`**
  - Remesh Voxel (Remesh) -
    **\`soc-2021-porting-modifiers-to-nodes-remesh-voxel\`**
  - Remesh Blocks (Remesh) -
    **\`soc-2021-porting-modifiers-to-nodes-remesh-blocks\`**
  - Merge by Distance (Weld) -
    **\`soc-2021-porting-modifiers-to-nodes-merge-by-distance\`**
  - Mesh Extrude -
    **\`soc-2021-porting-modifiers-to-nodes-extrude-and-move\`**
  - Mesh Inset - **\`soc-2021-porting-modifiers-to-nodes-extrude\`**

## Patches

  - Solidify (Solidify) -
    [**\`D11575\`**](https://developer.blender.org/D11575)
  - Collapse, Unsubdivide, Dissolve (decimate) -
    [**\`D11791\`**](https://developer.blender.org/D11791)
  - Remesh-voxel (remesh) -
    [**\`D11907\`**](https://developer.blender.org/D11907)
  - Remesh-blocks (remesh) -
    [**\`D11905\`**](https://developer.blender.org/D11905)
  - Merge by Distance (Weld) -
    [**\`D12019\`**](https://developer.blender.org/D12019)
  - Mesh Extrude -
    [**\`D12224\`**](https://developer.blender.org/D12224)
  - Mesh Inset - [**\`D12108\`**](https://developer.blender.org/D12108)

<!-- end list -->

  - Geometry Module -
    [**\`D11942\`**](https://developer.blender.org/D11942)

## Completed Nodes

### Solidify (Solidify)

The solidify node adds thickness to input geometry. You can supply the
distances of extrusion as attribute to get uneven thickness. As output
attribute you can get the selection for newly created side and top
faces. Shell and Top output are optional.

<table>
<tbody>
<tr class="odd">
<td><figure>
<img src="../../images/Mesh_solidify_node.png" title="../../images/Mesh_solidify_node.png" alt="../../images/Mesh_solidify_node.png" width="300" /><figcaption>../../images/Mesh_solidify_node.png</figcaption>
</figure></td>
<td><figure>
<img src="../../images/Selection_for_example_of_solidify_node.png" title="../../images/Selection_for_example_of_solidify_node.png" alt="../../images/Selection_for_example_of_solidify_node.png" width="300" /><figcaption>../../images/Selection_for_example_of_solidify_node.png</figcaption>
</figure>
<p><code> </code><img src="../../images/Mesh_solidify_node_result.png" title="fig:../../images/Mesh_solidify_node_result.png" alt="../../images/Mesh_solidify_node_result.png" width="300" /></p></td>
</tr>
</tbody>
</table>

### Collapse, Unsubdivide, Dissolve (decimate)

Collapse, Unsubdivide and Dissolve Nodes originate in the three nodes of
the decimate modifier.

Collapse and Subdivide got additional selection support.

Dissolve Limit modes are ported to use Attributes with different modes.
You can use Selection mode to just dissolve selected faces, limit mode
to dissolve unselected faces or edge attributes to limit dissolve.
Border mode can be used to limit dissolve around the border of the
selection.

<table>
<tbody>
<tr class="odd">
<td><figure>
<img src="../../images/Collapse_node.png" title="../../images/Collapse_node.png" alt="../../images/Collapse_node.png" width="300" /><figcaption>../../images/Collapse_node.png</figcaption>
</figure></td>
<td><figure>
<img src="../../images/Collapse_node_result.png" title="../../images/Collapse_node_result.png" alt="../../images/Collapse_node_result.png" width="300" /><figcaption>../../images/Collapse_node_result.png</figcaption>
</figure></td>
</tr>
<tr class="even">
<td><figure>
<img src="../../images/Unsubdivide_Node.png" title="Unsubdivide Node" alt="Unsubdivide Node" width="300" /><figcaption>Unsubdivide Node</figcaption>
</figure></td>
<td><figure>
<img src="../../images/Unsubdivide_node_result.png" title="Unsubdivide node result" alt="Unsubdivide node result" width="300" /><figcaption>Unsubdivide node result</figcaption>
</figure></td>
</tr>
<tr class="odd">
<td><figure>
<img src="../../images/Dissolve_node_limit.png" title="Limit Mode with edge selection" alt="Limit Mode with edge selection" width="300" /><figcaption>Limit Mode with edge selection</figcaption>
</figure></td>
<td><figure>
<img src="../../images/Dissolve_node_selection_limit.png" title="Edge crease" alt="Edge crease" width="300" /><figcaption>Edge crease</figcaption>
</figure>
<p><code> </code><img src="../../images/Dissolve_node_result_limit.png" title="fig:Result with limit mode" alt="Result with limit mode" width="300" /></p></td>
</tr>
<tr class="even">
<td><figure>
<img src="../../images/Dissolve_node_selection.png" title="Dissolve mode selection" alt="Dissolve mode selection" width="300" /><figcaption>Dissolve mode selection</figcaption>
</figure></td>
<td><figure>
<img src="../../images/Dissolve_node_example_selection.png" title="Face selection" alt="Face selection" width="300" /><figcaption>Face selection</figcaption>
</figure>
<p><code> </code><img src="../../images/Dissolve_node_result_selection.png" title="fig:Result with selection mode" alt="Result with selection mode" width="300" /></p></td>
</tr>
<tr class="odd">
<td><figure>
<img src="../../images/Dissolve_node_border.png" title="Dissolve mode border of selection" alt="Dissolve mode border of selection" width="300" /><figcaption>Dissolve mode border of selection</figcaption>
</figure></td>
<td><figure>
<img src="../../images/Dissolve_node_example_selection.png" title="Face selection" alt="Face selection" width="300" /><figcaption>Face selection</figcaption>
</figure>
<p><code> </code><img src="../../images/Dissolve_node_result_border.png" title="fig:Result with border mode" alt="Result with border mode" width="300" /></p></td>
</tr>
</tbody>
</table>

### Remesh-voxel (remesh)

This is a direct port of the Voxel mode from Remesh modifier.

|                                                                                   |                                                                                     |
| --------------------------------------------------------------------------------- | ----------------------------------------------------------------------------------- |
| ![Voxel Remesh Node](../../images/Voxel_remesh_node_node.png "Voxel Remesh Node") | ![Voxel Remesh Node](../../images/Voxel_remesh_node_result.png "Voxel Remesh Node") |

### Remesh-blocks (remesh)

This is a Port of the three original modes of the remesh modifier.

<table>
<tbody>
<tr class="odd">
<td><figure>
<img src="../../images/Block_remesh_node.png" title="Block remesh node" alt="Block remesh node" width="300" /><figcaption>Block remesh node</figcaption>
</figure></td>
<td><figure>
<img src="../../images/Block_remesh_result.png" title="Blocks Remesh result" alt="Blocks Remesh result" width="300" /><figcaption>Blocks Remesh result</figcaption>
</figure>
<p><code> </code><img src="../../images/Block_remesh_node_result_smooth.png" title="fig:Blocks Remesh result smooth" alt="Blocks Remesh result smooth" width="300" /><br />
<code> </code><img src="../../images/Block_remesh_node_result_sharp.png" title="fig:Block remesh node result sharp" alt="Block remesh node result sharp" width="300" /></p></td>
</tr>
</tbody>
</table>

### Merge by Distance (Weld)

This is a port of the Weld modifier, with additional support for merging
point cloud data. Selection attribute can be supplied to limit the merge
operation.

|                                                                                             |                                                                                                   |
| ------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------- |
| ![Merge by Distance node](../../images/Merge_by_distance_node.png "Merge by Distance node") | ![Merge by Distance result](../../images/Merge_by_distance_result.png "Merge by Distance result") |

### Mesh Extrude

This node mimics the corresponding bmesh operator. It can extrude
selected Vertices, Edges and Faces along a given vector. Selection can
be supplied as attributes. You can get the top of the extrusion as
output. NOTE: this node relies on a patch for attributes interpolation,
that is not yet in master.

<table>
<tbody>
<tr class="odd">
<td><figure>
<img src="../../images/Mesh_extrude_node.png" title="Mesh Extrude node" alt="Mesh Extrude node" width="300" /><figcaption>Mesh Extrude node</figcaption>
</figure></td>
<td><figure>
<img src="../../images/Mesh_Extrude_node_result_faces.png" title="Mesh Extrude node result faces" alt="Mesh Extrude node result faces" width="300" /><figcaption>Mesh Extrude node result faces</figcaption>
</figure>
<p><code> </code><img src="../../images/Mesh_extrude_node_result_edges.png" title="fig:Mesh extrude node result edges" alt="Mesh extrude node result edges" width="300" /></p></td>
</tr>
</tbody>
</table>

### Mesh Inset

This node mimics the edit mode inset operation, with additional support
for attributes. Attributes can be used to control selection, extrusion
distance and inset. You can get the side and top faces of the inset as
output selection.

<table>
<tbody>
<tr class="odd">
<td><figure>
<img src="../../images/Mesh_inset_node.png" title="Mesh Inset node" alt="Mesh Inset node" width="300" /><figcaption>Mesh Inset node</figcaption>
</figure></td>
<td><figure>
<img src="../../images/Mesh_inset_node_result_individual_selection.png" title="selection for individual" alt="selection for individual" width="300" /><figcaption>selection for individual</figcaption>
</figure>
<p><code> </code><img src="../../images/Mesh_inset_node_result_faces_individual.png" title="fig:Mesh Inset node individual result" alt="Mesh Inset node individual result" width="300" /><br />
<code> </code><img src="../../images/Mesh_inset_node_result_selection_group.png" title="fig:selection" alt="selection" /><br />
<code> </code><img src="../../images/Mesh_inset_node_result_faces.png" title="fig:Mesh inset node region result" alt="Mesh inset node region result" width="300" /></p></td>
</tr>
</tbody>
</table>

## Further Work

  - Make edge cases with solidify modifier, that uses shared code, work
    again.
  - Collect more feedback from artists. ([download experimental
    build](https://builder.blender.org/download/experimental/))
  - Some nodes e.g. Extrude rely on an updated attribute interpolation,
    which is not yet in master.
  - Bring nodes to master, which probably means, porting all the nodes
    to the fields system.
  - Write documentation with examples as soon as the nodes are in
    master.
