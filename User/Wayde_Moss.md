## Personal Info

Module: Animation & Rigging, developer focused on helping with the NLA
system.

|               |                                                             |
| ------------- | ----------------------------------------------------------- |
| Email         | wbmoss\_dev@yahoo.com                                       |
| Blender Chat  | GuiltyGhost                                                 |
| Twitter       | [@wbmoss2](https://twitter.com/wbmoss2)                     |
| BlenderMarket | [wayde-moss](https://blendermarket.com/creators/wayde-moss) |
| Gumroad       | [animationtoybox](https://gumroad.com/animationtoybox)      |

# Weekly Reports 2021

## May 17 - 23

**Patches:**

  - [D11330](http://developer.blender.org/D11330) D11330: Fix T88375:
    Bone Size Small After V3D.View\_All
  - [D11330](http://developer.blender.org/D11330) D11317: Fix T87160:
    DSE Keyframe Selection Now Selects Channels. Development halted due
    to design considerations.

**Reviewed:**

  - [D11282](http://developer.blender.org/D11282) Fix T88359: Issue of
    object matrix order use in pose bake.

'''Misc (May need to make bug reports and look into more if intended):
'''

  - Grease Pencil: (with the DopeSheetEditor open, annotations and GP
    objects exists) Outliner GP layer selection affects DSE channel
    selection in unexpected ways.

Expected behavior Example:

`(DSE) Make an annotation channel active. `  
`(Outliner) select a GP object layer. `  
`(DSE) now properly shows annotation and outliner GP layer as selected`

Unexpected behavior Example:

`(View3D) Ensure at least 3 GP objects exists`  
`(DSE) Make an annotation channel active. `  
`(DSE) Select a layer key from each of the 3 GP objects.`  
`(Outliner) Select a single GP layer`  
`(DSE) Unexpected, All GP layers become selected. `

  - Grease Pencil Annotations: (same example) Selecting GP channel
    results in no active GP annotation. Then View3D Drawing (D + MClick)
    adds a new annotation. Debug builds print complaints about no active
    annotation.
  - Graph Editor: Key deselection deselects the whole curve even if some
    keys are still selected.
  - Mask DopeSheet: You can Box/Region select invisible keyframes, those
    in the mask row.
  - Image Editor, Mask Mode: In Debug builds, having (N) Mask Panel open
    significantly reduces playback performance even in simple blend
    files.
  - Graph Editor/DopeSheet: In the GE select a keyframe handle but not
    the Co. Switch to DSE, the same key is shown as selected. Press G to
    try and move the key. Nothing happens.
  - Grease Pencil and Mask DSE: Alt+B (Box Select, Axis=Channel\_Range)
    doesn't work.

## May 10 - 16

**Commits:**

  - [cbeeca8167](https://projects.blender.org/blender/blender/commit/cbeeca8167f8)
    NLA: Extract ..get\_inverted\_upper\_snapshot()

**Patches:**

  - [D11248](http://developer.blender.org/D11248) Fix
    [\#88208](http://developer.blender.org/T88208): Missing Depsgraph
    Relations Updates.

**Looked into:**

  - [\#86233](http://developer.blender.org/T86233) 'Transformation'
    constraint ... does not work if the "Target" object is being
    evaluated in "Local" space. Turns out it's confusing but intended
    behavior.
  - [\#88068](http://developer.blender.org/T88068) Alt-I in 3D Viewport
    doesn't respect active keying set. Intended behavior.
  - [\#84774](http://developer.blender.org/T84774) NLA corrupt animation
    when adding armature strips. Intended behavior.
  - [\#87854](http://developer.blender.org/T87854) Follow Path: Camera
    overshoots path end and drops indefinetely in Z direction. Due to
    recent commit.
  - [\#85350](http://developer.blender.org/T85350) Copy Rotation World
    Space to World Space flips when applying Mix Before, After or
    Legacy. Intended behavior.
  - [\#87923](http://developer.blender.org/T87923) Inverse Kinematic
    constraints only work for first bone. Seems intended but is an
    annoying limitation. Still need to look into further to be sure.

**Misc (may need to make bug reports and look into further):**

  - Armature Edit Mode: Why are bone group colors not active during this
    mode? Group colors are intended help distinguish between bones so it
    should be enabled in editmode too.
  - Graph Editor/Python: Changing Fcurve action group color through
    python no longer does anything (regression).

Simple Repro Script (scene has single animated bone):

` import bpy`  
` action = bpy.context.active_object.animation_data.action`  
` action.groups[0].color_set = 'THEME05'`

CommandLine prints: \`Error: Cannot edit bone groups for proxies or
library overrides\`

  - Inconvenience, Graph Editor: Euler filter applies to all keys
    instead of supporting selection-only. Work around: Apply Euler
    filter, copy euler keys in desired range. Undo. Paste euler filtered
    keys. (AnimState Discord Gustavo Ruivinho)
  - IK angle limits: Min can never be positive. Max can never be
    negative. This makes the limits useless for preventing knee pop,
    where the min angle needs to be positive.
  - Looked at how depsgraph works with intent to find a way to improve
    Action Baking performance. We need a way to create a temporary
    filtered depsgraph where, given a set of input objects/bones, a
    graph is created that ensures only their transforms are properly
    evaluated and does no more than that. Currently, the filter
    granularity is per object. In the common case, this leads to
    expensive meshes and modifiers being unnecessarily evaluated. The
    solution is non-trivial as a transformation depends on constraints
    and drivers, both of which may depend on mesh vertex parents or
    bezier curve objects. So mesh evaluation may still be required
    depending on the rig. Action Baking workflows are becoming more
    common. Reducing the time needed to bake would greatly help such
    workflows.

## May 3 - 9 (Start of New Contract Period)

  - As of May 6th, started new contract period. Went through module and
    discussion to review what I missed and just generally catch up with
    development

## Feb 22 - March 21 (Summary Added in May)

End of contract period (March 16th)

**Committed:**

  - [dcd7dacc4f](https://projects.blender.org/blender/blender/commit/dcd7dacc4fd8):
    Graph Editor: FCurve Show Extrapolation Toggle

**Patches:**

  - [D10222](http://developer.blender.org/D10222) NLA: Extract
    nlasnapshot\_blend\_get\_inverted\_upper\_snapshot(). Apply review
    notes.
  - [D10504](http://developer.blender.org/D10504) NLA: Keyframe Remap
    Through Upper Strips.

**Task Designs:**

  - [\#82168](http://developer.blender.org/T82168): Improvements to
    "Bake Action..." Operator. Local development and planning.
  - [\#85352](http://developer.blender.org/T85352) Animation: Move
    Stashed Actions out from NLA to Outliner. Flesh out summary.
  - [\#83615](http://developer.blender.org/T83615) NLA: Strip
    Blend-space Transforms to Easily Align Actions. Flesh out summary.

## February 15 - 21

**Committed:**

  - [rBc48360c2](https://projects.blender.org/blender/blender/commit/c48360c2559a)
    Fix: NLA Blends Non-Animated Upper Channel Values
  - [rB17dddc94](https://projects.blender.org/blender/blender/commit/17dddc941714)
    Fix: NLA Blend Domain Enable All Typo

**Patches:**

  - [D10442](http://developer.blender.org/D10442) Graph Editor: FCurve
    Extrapolation Visibility.
  - [D10222](http://developer.blender.org/D10222) NLA: Extract
    nlasnapshot\_blend\_get\_inverted\_upper\_snapshot(). Added fix when
    input and output snapshots aren't the same pointer.

**Investigated/Spent time on:**

  - Looked into the more complicated features for
    [\#76472](http://developer.blender.org/T76472) Graph Editor: Fcurve
    extrapolation visibility. I think I've figured out a solution for
    accounting for Fcurve modifier properties (N Cycles, restricted
    frame range, limit fmodifier). A simple solution seems may be to
    create a range that only considers each FModifier. This is the range
    for the darkened fcurve segments. The range excludes the keyframe
    range. Fcurve Linear extrapolation extends this range to infinity
    and Const extrapolation has no effect.
  - Worked towards [D9875](http://developer.blender.org/D9875)
    Animation: Smart Bake Option for Bake Action. Which is added next
    week.

'''Issues found (may need to make proper tasks) '''

  - Bake Fcurve doesn't preserve Extrapolation flag.
  - Oddity: Ghost Fcurves don't sample the entire fcurve, only what's in
    view. As a user, creating a ghost fcurve then zooming out will make
    the ghost fcurve useless.
  - The Limit Fmodifier hides keyframes despite them still being useful.
    Others do too.

## February 8 - 14

**Patches:**

  - [D10222](http://developer.blender.org/D10222) NLA: Extract
    nlasnapshot\_blend\_get\_inverted\_upper\_snapshot(). Applied review
    notes
  - [D9925](http://developer.blender.org/D9925) NLA Tweak Mode: Evaluate
    Upper Stack. Apply review notes.
  - [DD10376](http://developer.blender.org/DD10376) Fix: NLA Swap
    Operator Bugs.
  - [D10377](http://developer.blender.org/D10377) Fix: NLA Duplicate
    Adds To Track Above Next Track.
  - [D10383](http://developer.blender.org/D10383) Fix: NLA Apply Scale
    Sets Wrong Action Range. This might've given insight into
    [\#84729](http://developer.blender.org/T84729) NLA: D9696 Solve
    Workaround.
  - [D10391](http://developer.blender.org/D10391) Action Bake: Explicit
    Option For Auto Frame Range.
  - [D10392](http://developer.blender.org/D10392) Action Bake: Overwrite
    Option to "Replace Entire Range".

## February 1 - 7

**Committed:**

  - [rB40b7929c](https://projects.blender.org/blender/blender/commit/40b7929cc040)
    NLA: Extract nlasnapshot\_blend()
  - [rB0352546c](https://projects.blender.org/blender/blender/commit/0352546cdd38)
    NLA: Fix nlasnapshot\_blend() Misplaced Null Check. Related to above
    commit.
  - [rB264af151](https://projects.blender.org/blender/blender/commit/264af1519e08)
    NLA: Fix Strip Truncate When Next to Transition.
  - [rB5bc9ddd9](https://projects.blender.org/blender/blender/commit/5bc9ddd98b36)
    Fix T85380: NLA Evaluation Missing Null Check.
  - [rB10e23fd1](https://projects.blender.org/blender/blender/commit/10e23fd1046a)
    NLA: Refactor Transition, Use Snapshot Blend Func
  - [rBabfb9dbf](https://projects.blender.org/blender/blender/commit/abfb9dbf593d)
    Fix: Disabled NLA Interferes with Action Eval

**Patches:**

  - [D8296](http://developer.blender.org/D8296) Feature: NLA: Evaluate
    Whole NLA Stack in Tweak Mode. Abandoned. The core idea is still
    being implemented with simpler smaller patches instead.
  - [D8808](http://developer.blender.org/D8808) Armature Animation
    Baking: Important performance patch. Author no longer available.
    Added patch that applied review notes.
  - [D10221](http://developer.blender.org/D10221) NLA: Refactor
    Transition, Use Snapshot Blend Func. Rebased due to recent commits
    (above).
  - [D10222](http://developer.blender.org/D10222) NLA: Extract
    nlasnapshot\_blend\_get\_inverted\_upper\_snapshot(). Rebased due to
    recent commits (above)
  - [D10102](http://developer.blender.org/D10102) NLA: Strip
    Post-transform Horizontal Shuffle. Rebased.
  - [D10339](http://developer.blender.org/D10339) Fix: NLA Blends
    Non-Animated Upper Channel Values. Introduced by
    [rB40b7929c](https://projects.blender.org/blender/blender/commit/40b7929cc040)
    NLA: Extract nlasnapshot\_blend()

**Tasks:**

  - [\#85352](http://developer.blender.org/T85352) Animation: Move
    Stashed Actions out from NLA to Outliner.

**Investigated/Spent time on:**

  - For further efficiency in
    [D8808](http://developer.blender.org/D8808), looked into extending
    Python collection.foreach\_set() and collection.foreach\_get() to
    accept a destination index offset. However, the cost of
    scene.frame\_set() alone is significantly more expensive than adding
    new keyframes so it doesn't seem worth while anymore.

## January 25 - 31

**Patches:**

  - [D10220](http://developer.blender.org/D10220): NLA: Extract
    nlasnapshot\_blend()
  - [D10221](http://developer.blender.org/D10221): NLA: Refactor
    Transition, Use Snapshot Blend Func.
  - [D10222](http://developer.blender.org/D10222): NLA: Extract
    nlasnapshot\_blend\_get\_inverted\_upper\_snapshot().
  - [D9690](http://developer.blender.org/D9690): Nla Refactor:
    nlaeval\_blend\_flush(), remove redundant branch. Abandoned. No
    longer relevant.
  - [D10228](http://developer.blender.org/D10228): Fix: DSE Set
    Ipo/Easing Ignores Hidden Channels.
  - [D10229](http://developer.blender.org/D10229): Fix T82234: NLA:
    Action Track Hold Forward Inconsistency. Patch already existed.
    Rebased onto recent commits.
  - [D9875](http://developer.blender.org/D9875): Animation: Smart Bake
    Option for Bake Action. Fixed indentation problems. Decided against
    preserving old (built-in automatic) operator panel drawing to avoid
    creating an implicit dependency.
  - [D10101](http://developer.blender.org/D10101): Cleanup: Nla:
    Refactor nla.c functions. Made review-ready, removed new warnings,
    removed user-side functional changes, etc.

**Investigated/Spent time on:**

  - Moving action stash out from NLA editor and data to outliner and its
    own separate data. Researched why it's in the NLA at all. Found:

` * `[`rBSac30378`](https://projects.blender.org/blender/blender-staging/commit/ac30378e3e3d790bad6d8027c2443b57cf47068e)` Action Editor: Stash Action Operator. (First commit for stash action support)`  
` * `[`#28453`](http://developer.blender.org/T28453)` Game animation workflow improvements. `  
` * `[`#43430`](http://developer.blender.org/T43430)` Action editor - saving (animation) actions is error prone and counter intuitive- DATA LOSS is too likely!.`  
` * `[`Response``   ``to``   ``the``   ``"Action``   ``Fake`` 
 ``User"`` 
 ``issue.`](http://aligorith.blogspot.com/2012/03/response-to-action-fake-user-issue.html)  
` * `[`Action``   ``Management``   ``Roadmap``   ``-``   ``2015`` 
 ``Version.`](https://aligorith.blogspot.com/2015/03/action-management-roadmap-2015-version.html)` `

In summary, stashed actions is a solution to saving the link between
actions and objects. This solution is better than Fake Users for this
reason. So when bones are renamed, related actions are properly updated
too. For game animators, they can make a lot of actions at a time
without worry of Blender automatically deleting them. Storing stashed
actions in the NLA was a quick hack since the NLA data already properly
handled Action references.

'''Issues found (may need to make proper tasks) '''

  - Nla NlaEvalStrip has unused members related to the track. Did not
    submit patch to allow more important NLA patches to be reviewed.
    Otherwise a low-priority dependency patch would've been created.
  - Nla fmodifier listbase join/split can be generalized to any
    listbase. Not submitted for the same reason above.
  - NLA nlastrip\_evaluate() and variants would be better named:
    nlasnapshot\_blend\_strip(). Not submitted for the same reason
    above.
  - Crash: Deleting the active object action from the outliner, undo,
    then try to delete it again. Crash.

## January 18 - 24

**Patches:**

  - [D10168](http://developer.blender.org/D10168) Fix T84486: Nla
    Disable "Sync Length" After Split. Fixes
    [\#84486](http://developer.blender.org/T84486) When a strip is cut
    and edited in nla editor for a library override character, the strip
    returns to its original length without the cut. blender 2.92 alpha.
  - [D9925](http://developer.blender.org/D9925) NLA Tweak Mode: Evaluate
    Upper Stack. Updated
  - [D7511](http://developer.blender.org/D7511) CollectionProperty:
    Keyframe Insert Using Element Name. Updated.

**Investigated/Spent time on:**

  - [D8296](http://developer.blender.org/D8296) Feature: NLA: Evaluate
    Whole NLA Stack in Tweak Mode. Roughly and locally re-implemented
    patch to account for recently committed patches while considering
    future needs([D8867](http://developer.blender.org/D8867)
    [\#83615](http://developer.blender.org/T83615)
    [\#64487](http://developer.blender.org/T64487)). Worked towards
    splitting into multiple patches to try and simplify reviews.
  - [\#84944](http://developer.blender.org/T84944) Sound track in NLA
    editor doesn't mute at the end of the NLA strip. Looked into whether
    it was a bug. Found that the support never intended.

**(Todo/Make design tasks):**

  - Make a task about changing NLA transition and meta implementation.
    They should support nested tracks. For transitions, instead of an
    implicit L/R strip, it should use the first and second track. This
    allows strips to overlap time-wise and allow for more uses when
    combined with meta strips that also support nested tracks. This
    implementations also removes a lot of UI problems related to
    transitions and their special handling.
  - For non-trivially invertibility of transitions, there's a potential
    solution. They're trivially invertible when the transition time is 0
    or 1. As an animator, it may only make sense to edit while all
    transitions evaluate completely to one side. For example,
    transitions are similar to blend trees and those are used to blend
    walks into runs. Although forcing transitions to evaluate completely
    to one side is limiting, the resulting edit-space remains intuitive
    for the animator to work with. So the limitation is acceptable in
    this case. In the future, transitions can be extended to support
    more than 2 strips and a separate transition time from general scene
    evaluation time.

## January 11 - 17

**Committed:**

  - [rB40d391fa](https://projects.blender.org/blender/blender/commit/40d391fa6068)
    Fix T81533: NLA Properly Draw FModifiers
  - [rB09709a7e](https://projects.blender.org/blender/blender/commit/09709a7e64ff)
    Nla Refactor: Split animsys\_evaluate\_nla()
  - [rB32e4ded2](https://projects.blender.org/blender/blender/commit/32e4ded24b6b)
    Nla Refactor: is\_fcurve\_evaluatable()
  - [rB10ce2a10](https://projects.blender.org/blender/blender/commit/10ce2a1016ae)
    NLA: Redundant NlaEvalChannel Valid Mask Write
  - [rBf03752b9](https://projects.blender.org/blender/blender/commit/f03752b92f8e)
    Nla: Rename NlaEvalChannel-\>valid to domain

**Patches:**

  - [D10083](http://developer.blender.org/D10083) NLA: Fix Strip
    Truncate When Next to Transition.
  - [D10101](http://developer.blender.org/D10101) NLA: Cleanup: Nla:
    Refactor nla.c functions.
  - [D10102](http://developer.blender.org/D10102) NLA: NLA: Strip
    Post-transform Horizontal Shuffle.
  - [D10103](http://developer.blender.org/D10103) NLA: NLA: Strip
    Post-transform Vertical Shuffle and Auto-Grow Track List.
  - [D9953](http://developer.blender.org/D9953) NLA: Fix T81533: NLA
    Properly Draw FModifiers. (apply review notes)
  - [D7511](http://developer.blender.org/D7511) NLA: CollectionProperty:
    Keyframe Insert Using Element Name. (bugfixes introduced by patch)

**Investigated/Spent time on:**

  - [D10073](http://developer.blender.org/D10073) Fix T84586: missing
    Outliner redraws for certain NLA operators. (user-side review)
  - [D8296](http://developer.blender.org/D8296) Feature: NLA: Evaluate
    Whole NLA Stack in Tweak Mode. Doing another pass of splitting into
    simpler patches while considering future needs
    ([D8867](http://developer.blender.org/D8867)
    [\#83615](http://developer.blender.org/T83615)
    [\#64487](http://developer.blender.org/T64487))

'''Issues found (may need to make proper tasks) '''

  - [c0bd240ad0](https://projects.blender.org/blender/blender/commit/c0bd240ad0a1)
    LibOverride: Add initial support for adding new NLA tracks. Raised
    concerns due to issues with NLA.(I should've made proper bug
    reports)
  - Duplicating strips then undoing causes new strips to be in a
    temp-meta.
  - Stashed actions should not be stored in the NLA system. It causes
    conflicts with the library override system (both assume first N
    tracks are their territory). Would be more ideal to store stashed
    actions elsewhere and display them in the outliner instead.

![../images/Displaying\_stashed\_actions\_in\_outliner..png](../images/Displaying_stashed_actions_in_outliner..png
"../images/Displaying_stashed_actions_in_outliner..png")

## January 4 - 10

**Patches:**

  - [D7511](http://developer.blender.org/D7511) CollectionProperty:
    Keyframe Insert Using Element Name. Apply review notes

**Investigated/Spent time on:**

  - [\#82241](http://developer.blender.org/T82241) NLA: Fix Dragging
    Strips and/or Remove Transitions (Solution 1). Roughly figured out
    implementation but won't properly split into separate patches and
    submitted until next week. Slowed down by a few issues due to
    [D9611](http://developer.blender.org/D9611).
  - [\#35708](http://developer.blender.org/T35708) F-curve editor
    displays incorrect curve for reversed NLA strips. Drawing is not the
    only issue. In general, existing code-base does not properly handle
    reversed fcurve keyframe manipulation.

'''Issues found (may need to make proper tasks) '''

  - NLA transform op creates metas out of sequences of strips which
    removes users ability to use strip gaps for proper placement.
  - Transitions are problematic by design. They can barely be treated
    like normal strips which leads to issues with their transform op.
    Transitions would be easier to handle if the strips they operate on
    were nested within the transition itself. That would remove
    transition's special boundary logic, transform interference, and
    prevents them from becoming invalid when adjacent strips are moved.
  - NLA Swap strips doesn't work for 2 adjacent strips when they're not
    the first strips in a track.
  - NLA Swap strips deletes a strip (memory leak) when swap results in
    overlap with each other and no other strip.
  - NLA Split metastrip ungroups the meta strip instead of actually
    splitting.
  - NLA Split broken for repeated strips.
  - NLA Apply Scale broken when strip action bounds doesn't match strip
    bounds. Action bounds changed to include all keyframes.
  - NLA scaled strips affects sensitivity of dragging keyframes in
    DopeSheet and Graph Editor.
  - NLA Strip Transform Extend broken. Dragging vertically deletes
    strip?
  - NLA why is "Cyclic time" not grouped with "strip time" ui property?

# Weekly Reports 2020

## December 28, 2020 - January 3, 2021

**Submitted Patches:**

  - [D9942](http://developer.blender.org/D9942) NLA: Remove Hold
    resetting between Hold\_Forward Behavior
  - [D9943](http://developer.blender.org/D9943) NLA: Strip Evaluate Held
    Strips Even When Not First Strip
  - [D9953](http://developer.blender.org/D9953) Fix
    [\#81533](http://developer.blender.org/T81533): NLA Properly Draw
    FModifiers

**Reviewed**

  - [D8687](http://developer.blender.org/D8687) Directly select
    animation curves in the graph editor
  - [D8808](http://developer.blender.org/D8808) Armature Animation
    Baking: Important performance patch

**Investigated/Spent time on:**

  - [D35708](http://developer.blender.org/D35708) F-curve editor
    displays incorrect curve for reversed NLA strips. Non-trivial to
    solve as it's not just a display problem. None of the graph editor
    tools properly handle reversed fcurves.

## December 21 - 27

**Submitted Patches:**

  - [D9925](http://developer.blender.org/D9925) NLA Tweak Mode: Evaluate
    Upper Stack. Should probably stop submitting NLA patches until
    existing NLA patches are accepted to avoid having to fix conflicts
    when patch dependencies are accepted.
  - [DT82234](http://developer.blender.org/DT82234) NLA: Action Track
    Hold Forward Inconsistency. Not properly submitted to avoid having
    to apply the fix twice.

**Investigated/Spent time on:**

  - Planned further splitting of
    [D8296](http://developer.blender.org/D8296) (Feature: NLA: Evaluate
    Whole NLA Stack in Tweak Mode) into evaluate upper stack,
    potentially refactor action clip evaluation into snapshots to allow
    pre-blend processing [\#83615](http://developer.blender.org/T83615)
    (Blend-space transform), proper keyframe remapping through action
    clips, through metas, and finally through transitions which will be
    immediately abandoned due to complexity of properly supporting
    keyframe remapping in the future. May leave action clip eval
    refactor to a patch that's more closely submitted with
    [\#83615](http://developer.blender.org/T83615) (Blend-space
    transform) for context.
  - Another review pass on Chris L's patches
    [\#81785](http://developer.blender.org/T81785) Implementation: Modal
    Key Manipulation Operators. Can no longer review any further until
    he makes changes.
  - [D64487](http://developer.blender.org/D64487) 2.80 Pose Breakdowner
    doesn't work in Add NLA Strips. Fix will have to wait until NLA
    exposes API to blend strips based on caller-given strips and
    snapshot for the broken-downed properties.

## December 14 - 20

**Submitted Patches:**

  - [D9875](http://developer.blender.org/D9875) Animation: Smart Bake
    Option for Bake Action

**Investigated/Spent time on:**

  - Review pass on Chris L's patches
    [\#81785](http://developer.blender.org/T81785) Implementation: Modal
    Key Manipulation Operators
