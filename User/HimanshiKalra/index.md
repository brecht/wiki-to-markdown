### Himanshi Kalra

Project links for Google Summer of Code Projects.

To know more about me: Check out the Bio sections in the proposals.

To know more about the projects: Check out the proposals, reports and
other documentation.

-----

  - [GSoC 2021](GSoC21/index.md)
  - [GSoC 2020](GSoC20.md)
  - [Blender Documentation
    Bits](BlenderDocs.md)
