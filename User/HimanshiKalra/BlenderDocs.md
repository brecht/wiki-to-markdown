## Documentation for reference

View2D is a way of describing viewport in 2D. Commonly used with
\`ARegion\` (area region or active region). Used for view panning and
scrolling purposes. The scrolling operations are present in
\`view2D\_ops.c\` file. Similar to other operations like subdivide in
Edit Mode.

Scroll by a length of one "page" happens when we click on an empty space
outside the size of the scrollbar. The scrollbar moves down by a fixed
amount called a page.

Lukas Tonne worked on a similar Edge Panning structure.
