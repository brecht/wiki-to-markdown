May 7/2020 - First kick-off meeting with mentor, Habib Gahbiche. General
discussion on GSoC, communication channels, doubts clearing, roles of
all the active devs and the field they are working or have worked on.

May 9/2020 - Discussion on updates in the proposal, and going over each
Deliverable to have a common understanding of what needs to be achieved.
Bug Tracker fixing and bonding.

May 14/2020 - Meeting with both the mentors, Bastien Montagne and Habib
Gahbiche, brief discussion of the proposal, some ingsights on
implementation. Discussion on starting the coding phase early.

May 15/2020 - Discussion on Deliverable 1, discussing various
alternatives, agreeing on a script-based approach for generating test
objects.

June 18/2020 - Meeting with both mentors, Bastien and Habib,

  - me updating on what has been accomplished so far with Deliverable 1
  - starting with Deliverable 2
  - would start with finishing off remaining Mesh modifiers
  - then Curves
  - general discussion on when we should all take a vacation ;)
  - another meeting in 1st or 2nd week of July

July 30/2020 - Discussion on everything what happened since last
meeting. Some important meeting points

  - shifting the focus from adding more tests to polishing the
    framework.
  - making it master-mergeable.
  - so next 2 weeks would be dedicated to this and revisiting the
    Physics framework.
  - then revisiting the test object generator script(if time permits)
    and last week would be for Developer documentation.

Note: Deliverable 3 will be skipped.
