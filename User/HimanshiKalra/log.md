Inspired by Hans Goudey and in case anyone is interested, I would like
to document my remaining summer here, although already more than half of
it is over, and I will try my best to be consistent and not turn it into
my personal diary ;)

-----

## July 24

-----

Today I worked on testing the Particle system, and finally tested the
Fluid modifier, there have been some significant changes from 2.83 to
2.90, the test was failing because I was testing with different
versions. The Particle system's Render property needs to be set to
Object to be able to convert it to Mesh and be testable, I am not yet
fully able to confirm whether it is reproducible or not.

## July 25

-----

Today, it was weekend time, didn't work on anything blender related,
participated in a hackathon, which went fine. Solved one problem out of
two, attempted all MCQs, mostly cyber security related questions about
BGP(Border Gateway Protocol) and something called YARA rules. Was trying
to get Windows updated to get WSL-2 working on the system, so that I can
run Docker, so that I can build my resume using Latex, and I don't know
how to use any of that :D, the most long way to do it. I have nothing to
write in it anyways.

## July 26

-----

Today, I was exploring WSL. Got WSL-2 running, added Ubuntu as a distro,
I would have preferred CentOS was looking forward to check it out, but
it is paid :(, got Docker running as well, exploring how to use it.
Started with one of those projects which I rarely finish. The main
reason for setting up WSL was to have a Linux system to run unit-tests
for Blender in a different environment, and get faster builds. So it is
partly blender-related. Got some basics cleared up for the Docker, won't
really need them anytime soon though. End of weekend.

## July 28

-----

Today, I worked on testing Particle systems, they are reproducible after
we set the seed and keep the world coordinates same. There is a kind of
inconsistency in the simulation of Particles, say when you add the
Particle system modifier at some frame n, the simulation is different if
you add it to the first frame and "play" and "pause" it at frame n.
Maybe "physics" is being re-calculated, should open a devtalk thread for
this.

## July 29

-----

Yesterday I tested the Particle System, it was reproducible using a
python script but hard to reproduce manually, it kind of took a lot of
time, I wasn't sure why I can't reproduce it, so I was trying out
different stuff, maybe world coordinates affect it or maybe setting a
seed value will make it reproducible, all of it was just too fuzzy and
not systematic, I reached no conclusion apart from "Just use the damn
script\!" I added a separate class for it and submitted a diff, next
plan is to add Dynamic Paint modifier.

## July 30

-----

Today, I worked on Dynamic Paint, I thought I was half-way through with
it in the afternoon, but now I just can't access the canvas settings
from the framework, its probably my fault somehow in the implementation
but I used the same technique for Fluid and Cloth and they are working
fine, they are not complaining at all. Will have a look again tomorrow
and would narrow it down with a script.

## July 31

-----

Yesterday, I had a brief meeting with Habib to start the refactoring
process, the approach discussed to parse the settings is using a Tree
based Depth First Search approach. I have had implemented that as a part
of my curriculum in C++, using nodes and pointers, have no idea how to
do that in Python. I will think of something. The Dynamic Paint Class
functionality was successfully merged with Fluid Class, both are
testable.

## Aug 4

-----

Aug 1 and 2 were weekends, Aug 3 was festival day, I was busy. So
yesterday, I worked on traversing a generic tree(not binary tree) in a
pre-order way, given that the tree is already given, I tried to use the
similar approach to traverse a nested dictionary to be able to set
nested settings of modifiers.

## Aug 5

-----

Today I implemented the traversal of nested parameters within the
framework, its up for review, the code looks a bit "fishy" but it works,
its recursive and has nested try-catch, I will try to simplify it
further

## Aug 7

-----

I missed to write on Aug 6 :(, I am writing this on Aug 8, so I use the
word "yesterday". Aug 6 - I worked on simplifying the code and made the
changes as requested in the review, converted the nested try-catch to an
if-else. Yesterday, I worked on merging Dynamic Paint and Physics Spec
into Modifier Spec, I added frame support, updated the
"\_bake\_current\_simulation" method to merge different types of baking
under one function only. Oh and there was another meeting with Habib to
discuss "Possible next steps" to be done for the remaining 2 weeks.

## Aug 8

-----

Yesterday I added a test for Particle Instance and created a final diff
for all the work I have done, its less compared to other students. Added
Bastien as a reviewer to my diff D8507, tried to switching to my svn soc
branch I created, should have logged that...The files started
re-downloading, it is always a blind error, I am in the wrong path most
of the times that's why.

In the end, I think Fluid simulation has randomness and in it and is not
reproducible, even increasing the threshold doesn't help, so I ended up
removing the Test for Fluid, as it was failing now.

## Aug 9

-----

Committed to my svn soc branch, added new and updated blend files,
updated the [Tools/Tests/Python](../../Tools/Tests/Python.md) page for
Windows users, plan to add more tests and add documentation for "How to
add more tests" and run them.

Okay here is the trick for svn, once and for all, for future me, while
switching branches using \`svn switch\` we should be in the same path as
relative link, in my case if I am in \`lib\\tests\`, the svn command
would be \`svn switch
<https://svn.blender.org/svnroot/bf-blender/branches/soc-2020-testing-frameworks/lib/tests>\`.
It is different from checkout in which we should be one step outside
\`../\` and then checkout.

## Aug 10

-----

Today I worked on adding more tests for Cloth modifier, added tests for
spring, self-collision, updated the documentation for testing including
the "why automated testing is needed" and "how automation" works. So the
newbies don't end up doing it manually like me. I thought I would be
busy today, not so much.

## Aug 12

-----

Less code work was done on Aug 11, I was looking at Release notes and
bug tracker, updated diff to include new cloth tests, merged master
locally and pushed to remote. Today I have been making improvements as
per the review.

## Aug 16

-----

I took off for around 4 days (Thursday to Sunday), I had a coding round,
did a little preparation, didn't go that well. Anyway, now its over, so
I am back to Blender.

## Aug 17

Today I am working on my test object generator script, I am trying to
abstract it so that the user can interact with it better, without being
exposed to the code much. I haven't worked on it for quite a while, so
its a bit rusty. My college classes are supposed to begin tomorrow.

## Aug 18

Today I worked on extending the generator script, basically adding
another class to handle the "file handling" part, generation and
creation. There is no fully correct way to check for path validity or I
am yet to find it out, I did it the old way using \`os.path.exists()\`
and asking for the filename as well as path, there are 2 approaches
either I can do it in the code, somehow split the string and extract the
filename, and path separately to check for path validity or just take it
from the user, I am going for the latter approach, its easier and the
user just have to type it once anyway. The new GenObject class will take
a list of TestObj. I managed to make it Exception safe, proper unlinking
from Scene/master collection is left.

## Aug 19

-----

I am a little blank on what I worked on today, its vague, some testing,
some work on script, and going through the review

## Aug 20

-----

Today I worked on improvements suggested by Bastien in the review,
code-formatting, renamed "test names" of bevel and boolean operator
tests, and I accidentally broke the operator tests, the verdict for
Cloth Spring and Self-Collision is non-reproducible, I will test again
after compiling on Ubuntu, finally WSL will pay off, already cloned it,
the libs are left.

## Aug 21

-----

Today I worked on the generator script finally its up for review, there
is a scope of adding back the Vertex group functionality, I will look
into it later. Finally compiled blender on Ubuntu. I also got feedback
on User Documentation wiki page, so I worked on it and updated it. I
also started with the documentation of the Final Report to submit it to
Google as the Work Project.

## Aug 23

-----

I tried getting some review work done on Saturday but got bored mid-way,
so took the weekend off. Sunday, asked Bastien for a meeting and updated
the weekly report.

## Aug 24

Today I am working on creating a new common class for \`ModifierTest\`
and \`DeformModifierTest\` from which they both can derive, its called
\`RunTest\` for now, I am facing some implementation problems...

## Aug 25

The original idea was different, it was to inherit from a parent class
and one of the parameters of the method of the Parent class was a child
object, hence it was a bit crazy but it worked.

The current implementation involves using an Abstract Base Class (ABC)
and to have a check for \`run\_test\` for the different Helper Modifier
Test classes, this is what I worked on today. There is some discussion
on merging both the Helper classes.

## Aug 26

Removed the abstract base class, as there is now only one Helper class,
merged DeformModifierTest and ModifierTest, so now there is only
ModifierTest class. Today I worked on Final Report, it took quite some
time to gather all the work I did and put it in words.

## Aug 27

Today I worked on removing Operator Test class and introduced a generic
Run Test class it can be used by both Operators and Modifiers, revamped
bevel, boolean and operator test files, used everything to minimize
time, find and replace is THE BEST, then later I used "multiple
cursors", it also increases speed. Overall it was tiring but its done,
it is generalized which is a good thing.

## Aug 28

Today I was working on my Btech project in the morning, I made some
tweaks in the final report, there were some issues with my remote branch
being diverged from my local branch. And I submitted the evaluation. I
think I don't intend to work on the weekends, might give a look at
Generator script. So its mostly a wrap-up for me.
