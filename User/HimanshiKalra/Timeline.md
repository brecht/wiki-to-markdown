### Timeline

**Week 1- May 18-24:** Discussing the structure of the framework for
automatic naming with the mentor and fellow developers and doing a
survey of test-able modifiers and mesh operators.

**Week 2- June 1-7:** Starting work on the framework for automatic
naming. **Deliverable 1**

**Week 3- June 8-14:** Finish working and testing the framework.

**Week 4- June 15-21:** Writing tests for the remaining Mesh
Modifiers.(Generate and Deform)

**Week 5- June 22-28:** Adding support for Object Operators.

-----

**June 29-July 3 \[ 1st \]**

**Week 6- June 29-July 5:** Extend framework for supporting
user-like-input **Deliverable 2**

**Week 7- July 6-July 12:** Extending framework Curve Modifiers.

**Week 8- July 13-July 19:** Adding tests for Deform Curve Modifiers.

**Week 9- July 20-July 26:** Extend framework for Physics modifiers and
add tests for the same.

-----

**Week 10- July 27-Aug 2**: Discussion on compositor automated testing.
Starting work on compositor automated testing.

**July 27 - 31 \[ 2nd \]**

**Week 11- Aug 3-9:** Writing tests for compositor.**Deliverable 3**

**Week 12 \[Aug 10-16\] -:** Buffer for above three deliverables

**Week 13- From Aug 17-23:** Developer documentation. **Deliverable 4**

-----

**August 24 - 31 \[ 3rd \]**
