## August 31

Was working on adding a new operator for the Spreadsheet Editor, was in
contact with Jacques Lucke. Also attended the GSoC Student Summit 2021
virtually. Moving the Geometry Nodes test adding doc to the main wiki.

## August 19

Worked on the Final Report.

Added tests for Primitives.

## August 18

Added tests for Attributes, uploaded diff for review and testing.
Covered around 60% of Attribute tests.

## August 17

Added few more tests. Don't have idea how to test attributes directly on
vertices without visualizing it using Point Instance. Point Instance
doesn't transfer attributes, so the custom data comparison of generic
attributes code won't really be helpful, coz they won't be checked.

## August 16

Back from holidays, updated and committed patches. Hans is away, pinged
him regarding double warnings.

  - [Committed Eitan's
    patch.](https://developer.blender.org/rBfecec1644ce54ea386eaeed5ca6748d4a7b2737b)
  - [Committed my custom color comparison patch in
    master.](https://developer.blender.org/rBc48a01a88add5247f08de6f8af8d223ebc4e28c9)

## August 5

Troubleshoot day.

Why random Attribute changes to the mesh not making the test fail? Note
from future me: I didn't re-initialize the layers to point at the first
attribute.

## August 4

Added custom data comparison finally\! Worked on Jacques' snippet, added
threshold and polished the code based on comments by Jacques.

## August 3

Woke up on wrong side of the bed or idk. Not productive today.

Hmm maybe should work on tests to have a change of perspective.

## August 2

Working on Jacques' boiler plate code for comparing Attributes. The
problem was the attributes are not in the same order on the expected vs
test object.

Debugging some code related to the length of the loop, iterating only
once etc. Don't use keyword \`len\` even in C, its a keyword here as
well.

Me from the future- NOOO, that was not it, I was sending the address of
a pointer and receiving it in an \`int\` that's why, when sending pass
by reference using pointer, receive it with pointer.

And for primitive data types, pass by value is a good practice, use by
pass by reference only when we want to change the value.

## July 27

Bisected the error for Curve tests, was Jacques's update for incorrect
rotation matrix. Updated the tests. Merged code to master.

Messed up the commit message, pushed the wrong hash, after \`--amend\`.

## July 26

Lowered the file size for tests. Added a directory check in cmake. Curve
tests broke. TODO: Shift focus to comparing custom data this week.

## July 22

Hm, the bounding box test fails only for me? I haven't updated it yet. I
worked a little on custom data color comparison, found \`MPropcolor\`\*
not correct type, can't remember.

## July 21

Finished testing with Curves nodes. Need to update the zip file with
correct bounding box file.

TODO

  - Update zip file.
  - Restart comparing custom data.

## July 20

Today's TODO:

  - Work on D11611, improve based on comments.
  - Compare custom data.

Updates:

  - Polished the script.
  - Found a failed Assert.
  - A one time bug, unable to reproduce now.
  - Tried testing (adding tests for)

`* Curve Length (used)`  
`* Curve Trim (Didn't work)`  
`* Curve Reverse (Didn't work)`  
`* Curve Resample (Works, read)`

## July 19

I created tests for Curves Geometry Nodes.

  - Curve Subdivide (Clear in Nurbs Curve/Path)
  - Curve to Points (Milky Way)
  - Curve Reverse (Didn't understand)
  - Curve Endpoints (TODO)

## July 16

I was working all day, haha. Polishing the script, only sounds easier.

Plans for next week:

  - Create tests for Curve Primitives.
  - Work on comparing Attributes as Custom Data (C side).

## July 15

TODO: While refactoring, I completely forgot to add the ability for just
viewing tests. I need to re-think about how the \`do\_compare\` flags
should work and which class should control that behavior and how...

I was working on trying to compare attributes for the 2 meshes. Still
wrapping my head around all the C and trying to write not-so-silly code.

Some test framework dilemma. How to handle failed tests with cmake For
BLendFileTest: Inside MeshTest.run\_test(), we could either: - using
sys.exit(1) - raise Exception

If one of the above options is not implemented, cmake shows the failed
tests are passing.(shouldn't happen)

But, this creates problem for existing tests (SpecMeshTest), as it errs
with: - Error while running Cmake (shouldn't happen either)

How is this solved previously? - The RunTest class runs all test and
collect failed tests, and raises an Exception after running all the
tests.

## July 14

Le quartoze Julliet\! I am stuck. Aim: Add custom data that will check
attributes for the two meshes. Method: Compare the two attributes and
their values. Stuck here: Where are these attribute values stored? Is it
being referred to these types \[CD\_PROP\_FLOAT, CD\_PROP\_FLOAT2,
CD\_PROP\_FLOAT3,

`CD_PROP_INT32, CD_PROP_BOOL, CD_PROP_COLOR ]`

Should we iterate over all vertices and attributes are part of it? and
then we compare..

How do we access the values for all these types? How will they be
compared?

Today, I didn't make much progress. I was looking up at bit-masking and
bit manipulation. Coz that's how we check if we have the right type of
attribute.

## July 13

Updates: - Polished the script a bit based on the review. - Changed the
comparison method. - Added better doc strings.

TODO: - Create tests for Curves. - Think of testing Attributes.

Didn't get: Attribute Processor Node

Had a meeting with Jacques. Discussed different proposals. Lukas's:
Remove the dropdown attribute thingie and replace it with a inbuilt
\`Result\` socket, so no temporary names in between. Problem is, still
is more or less linear tree. Connect one node to the other.

Jacques's: Introduces the concept of field, which works on a single
float value\*, but it can be a function which will pass on the entire
context to the next node. Another concept is, new attributes are
generated from scratch and old attributes get deleted just after their
use case is finished. E.g. To displace points which are based off of
Noise Texture node. Geometry Input -\> Displace Points -\> Output Noise
Texture -\> Displace Points \[Density\]

Noise Texture gives a field (a function) over which attribute density is
affected. It's done for the entire mesh density. Problem (maybe/can be
solved?) If the mesh topology changes, one can't carry the changes
forward, because the attribute has changed. E.g. Subdividing the mesh,
it will become n times.

There is also the concept of anonymous attributes (random unique ids are
assigned when they are calculated) but they can be exposed. Personal
clarification How does the user make them if .. Re-read the proposal,
the Point Distribute Node would output them itself in the form of a
input socket.

Dalai's: Similar to fields, but passes a list of values. We have an
initial set of attributes available freely with the Geometry. Then there
is some + button using which we can pass on that attributes if we want.

## July 12

Working on the review by Habib.

Had an evaluation meeting with the mentors. Discussed the future focus
shift of the project. The evaluation form had an error, gsoc support
team replied and updated it with a fix.

## July 8

Writing it from the future.

Submitted the diff for review sooner.., ran into a problem with Flags.
Changed defaults.

## July 7

I ran into some problems with BLENDER\_TEST\_UPDATE, apparently I forgot
to add the flag for applying the modifiers (apply\_modifier) and it was
creating trouble with applying\_operations method. Not applying the
modifier on the evaluated object (when it should have). Done

  - Add create\_expected\_object
  - Handle recursion --by force

Plans for tomorrow:

  - Add RunTest class.
  - Add the Spec classes.
  - Polish with comments.
  - Clean up.

Friday tasks:

  - Do the entire renaming, along with proposed re-naming of
    \`run\_test\`.
  - Put it up for review tomorrow or by Friday.

TODO

  - do\_selection (double declared)

## July 6

  - Restructured code for SpecMeshTest.
  - Discussed some naming changes with Habib to remove ambiguity.
  - Tested MeshTest.
  - Added \`apply\_operations\` for SpecMeshTest.

Plans for tomorrow or later:

  - Add create\_expected\_object
  - Handle recursion --by force
  - Handle cmd arguments.
  - Add RunTest

## July 5

  - Added demo test cases to test BlendFileTest.
  - Reviewed Jake's operator tests.

July 3-4 Weekend, July 2- small meet with Jacques. July 1 - forgot :/
probably updating the Mesh Test.

## June 30

  - Worked on the implementation of the \`comapare\_meshes\` method.
  - Updated the UML to reflect the new class design.

![UML Mesh Test](../../../images/UML_Mesh_Test.png "UML Mesh Test")

## June 29

Finished the prototype structure and got it quickly seen by mentor. Did
some reading of existing proposals for the "no hard-coded string"
situation on GN. Need to update the UML to show new the structure.
Schedule for tomorrow:

  - Finish testing the design of the Abstract class.
  - Re-design parts of it, if need be.
  - Start with the real implementation of the base abstract class.

Personal reflections: I am losing a bit of focus here again...
Nice-to-do's: Give feedback on Lukas Tönne's Edge panning/scrolling
patch.

## June 28

Attended the Geometry Nodes meeting, worked on the pseudo code for the
refactoring of Mesh Test. Dalai briefed about the about the Nodes
Workshop.

## June 24

Working on a UML diagram for the Base class.

## June 23

Had a meeting with mentor (Habib) Discussed the inheritance idea and all
the approaches. Have an abstract class that will have common test
functionalities.

## June 21

Working on refactoring mesh test framework. It's a mess, I changed too
many things to keep track of. I need to put a closure, make it smaller
and get some review here.

## June 19

Active on tracker, involved in 4-5 reports.

## June 18

Up for discussion, where I am stuck.

Discussion

  - Should refactoring of mesh\_test be considered if it doesn't satisfy
    the needs of the current testing approach.
  - To break functions like Mesh Test's \`run\_test\` into say
    \`compare\_meshes\`, \`validation\`, \`compare\_selection\` and the
    likes.

<!-- end list -->

  - Dealing with applying of existing modifier, two ways of applying
    modifier.
  - A test name, \[any name?\], would avoid having a collection in a
    blend file with only one test.

Issues:

  - Applying of modifier in the script and then re-use part of
    mesh\_test just for comparison.
  - Wouldn't duplicating some part of code serve better instead of
    re-using, if we are not per-se re-using.
  - Current state of the script can act as a module for others?

I think I will just implement it, coz trying to design it before is time
getting spent nowhere. Also, there is some gap at the moment in...to run
things by the mentor.

## June 17

  - Coder's block.
  - Not much.

## June 16

  - Mentor (Habib) helped me out with ctest \`FAILED\` condition.
    (sys.exit(1))
  - Was taken aback by ctest acting up by saying "PASSED" to clearly
    failing tests.
  - Worked on the review by Jacques.
  - Discussed with Jacques to include creating an expected object when
    none exists with the BLENDER\_UPDATE\_FLAG.
  - Had to check on why poll for mode\_set operation is failing.
  - Added unit test cases for basic nodes.

Running into some forward compatibility problem with Geometry Nodes.
Mostly noodle disconnection. Well, opening newer files in older
versions, some data loss is expected. I am a bit concerned about
regression testing then, but then they should always be compatible with
master.

## June 15

  - Re-setted VS Code autopep8 settings, it acting up.
  - Added a linter for VSCode.
  - Shifted the code to the \`soc\` branch.
  - Put the code up for review <https://developer.blender.org/D11611>
  - Updated locally the changes requested.
  - Currently something wrong with my code, did a fresh build, test is
    passing with \`ctest\` failing with the blender python command. And
    it should have failed. What did I miss...?

## June 14

  - Attended the Geometry Nodes Meeting.
  - Discussion about having a Compact Node before Attribute Processor
    Node.
  - Had a quick meet with Jacques to discuss the regression testing
    idea.

Summary of the idea The idea is to have a generic framework for every
operation. Comparing the entire files, as in datablocks.

  - Tests contained at a single place, right now \[Mesh Test Framework\]
    we need to change the Python script as well the blend file in SVN.
  - The idea to run Python files from inside the Blender text editor was
    dropped.
  - Testing of any operator, with emphasis on addons.

Another idea which was proposed and I liked was, say we have a big
Nodetree, then logging and printing which one caused the failure. In
ideal case, this might not exactly be needed if we have sufficient
unit-tests, so failures like this would be caught early on.

  - Should have logged sooner, I forgot pieces from in-between.
  - Worked on polishing the script.

## June 11

  - Added the functionality of running tests from \`ctest\`
  - Each file is a test in itself
  - To run the tests in bulk, use regex \`ctest -R geo\_node\` will run
    all geometry nodes tests.

The arguments mentioned in the cmakelists txt file are compulsory
arguments that need to be passed. It is like the expanded form of the
test \`ctest -R modifiers\` expands to the following from the code.

` add_blender_test(`  
` modifiers`  
` ${TEST_SRC_DIR}/modeling/modifiers.blend`  
` --python ${TEST_PYTHON_DIR}/modifiers.py`  
` --`  
` --run-all-tests`  
` )`

add\_blender\_test calls the blender executable and lets take in
parameters, the second is the blend file location and with python
argument the script, rest are arguments given here to actually run all
the tests. \`modifiers\` is like an alias here.

## June 10

Was experimenting with \`argparse\` module to give command-line
arguments to run the python test file. Had discussion on blender chat
with mentors. Jacques is exploring a new idea of regression testing,
where a blend file in itself is self-sufficient for a test. Need to
discuss that in the next meeting. Ran the workflow by the mentor
(Habib). Plans for tomorrow:

  - Make it work using ctest.
  - Using cmd line arguments to run the test file. (ALL tests).
  - Add --first time argument to automatically generate the expected
    object.
  - The script can be found here <https://developer.blender.org/P2165>

There is a mish-mesh and unclear layout of how all the tests

## June 9

Finishing up early today

  - Discussed the load from file approach with mentor \[Habib\].
  - Clarifying the design of the script and related questions. \[Failed
    tests, 1 node test/ blend file etc\]
  - Had disagreements with the mentor \[Habib\] wrt what \*was\* in the
    proposal, what \*is\* in the proposal and what is being agreed upon
    in the plan for this week and how to proceed with the project
    overall. The project will deviate a bit from the proposed solution
    to move forward in the best interest of the Geometry Nodes
    regression testing project.
  - Working on the script for load from blend file.

## June 8

Worked on directly loading a modifier from blend file. It uses the
"modifier name" to identify the modifier. I tweaked \`ModifierSpec\` to
"find" the modifier. Discussed the approach with mentor (Habib). Based
on the feedback:

  - This approach misuses the \`ModifierSpec\`, coz we are not exactly
    using it to send Modifier Specifications.
  - Try other options, an approach similar to Compositor Testing.
  - If following this approach, introduce a new class for
    \`ExistingModifierSpec\`.
  - Ran test for Point Distribute Node, it generates invalid geometry.
  - Here is the diff <https://developer.blender.org/P2155>, it's not on
    my \`soc\` branch yet...

## June 7

Had a meeting with Jacques to discuss some issues. Meeting Notes:

  - Discussed Geometry Nodes Python API issues with Jacques. The use the
    same socket name for different parameters, a socket is enabled or
    disabled depending on the Node type (Attribute or Vector). Although
    they have different identifiers (A\_001), but then they cannot be
    used as a \`key\` to the input value for accessing. (2 types Node
    type and then Socket type)
  - Addons can be called by overriding the context once they are enabled
    just like an operator.(need to look at that).
  - Versioning as in the utility of having to open a 2 month old blend
    file and see it still gives proper results.

Created my \`soc-branch\` although currently I am working on a different
local branch.

Today's problems and progress: Alright, so I thought I made good
progress today but...I can't test it. The \`modifiers\` file starts
complaining about the module error as soon as I edit the ModifierSpec,
so to trouble shoot step by step, I was looking at my own diff. The diff
has lot of unwanted formatting changes, so I disabled all the Python
extensions from VS Code and it still formatted again. I have now tried
to fix it by changing the settings by setting the line length to 119
chars. Since I couldn't make direct progress, it's a bit annoying. Will
leave the rest for tomorrow.

## June 4

Had a meeting Habib to discuss how the framework for GN should be. The
following is summarized below:

Meeting Agenda:

  - Run the Class design by the mentor.
  - Feedback on the current state of things.
  - Fixing a review day (if a review is due)
  - Rules for using an existing addon in the code or copying it.
  - Thought on Attributes and Utilities, can they be tested... or is it
    just a functionality (but Boolean nodes can be tested).
  - Open a TODO/Task for test/example case contributions for simple
    blend files for Geometry Nodes.

Issues

  - Python API for setting attributes of geometry nodes.
  - They are accessed by index\[i\].default value instead of name...
  - Discuss on the different types of attributes \[IMP\].
  - API for using existing addon "addon\_utils"
  - versioning

Meeting Notes:

  - Discussed Class Design and made some changes (Adding Geometry Nodes
    Modifier as part of the Geo Node class)
  - Reviewing within fixed number of days instead of a specific day.
    (e.g. within 2 working days)
  - Discussed to look at an approach with loading a blend file with
    existing Geometry Nodes tree.
  - Code Specific Issues and Class - Differentiating between the 2 types
    of parameters in Geometry Nodes.
  - Versioning Releases (2.9 vs 3.0 etc): Regression Testing already
    covers testing amongst various git versions. (Clarify with Jacques)
  - Deciding on leaving Attribute Nodes for a later period, also talk
    with Simon (technical artist) to provide for meaningful test cases.

## June 3

Exploring Lazy Dodo's Geometry Node Builder.

Looking at the old patch from mentor on Compositor Testing.

Designing the class prototype and meeting agendas.

## June 2

Using Curve to Mesh Node.

Raw Design for the Geometry Nodes class.

## June 1

Trying out different Nodes.

Material Nodes and Bounding Box.

IDEA: Having a common or integrated render test framework with existing
framework. How else will the Color and Material Nodes will be tested?

## May 31

Attended the meeting. The idea of using attributes from Spreadsheet
Editor API was cut short. There is no API available yet. Was exploring
Transform Node from Geometry.

The idea was to be able to use the \`position\` attribute to compare,
discussed that approach to an extent with mentor. Moreover, it might
fall under "Functional testing" and not "Regression testing", going out
of the scope of project at the moment. I re-read my own proposal to
shift the focus back to the original idea.

## May 29

Weekly Report BugTracker (Saturdays) Week 21 (May 24 - May 28)

**Involved in 14 reports:**

  - Confirmed: 3
  - Closed as Resolved: 1
  - Closed as Archived: 0
  - Closed as Duplicate: 1
  - Needs Info from User: 6
  - Needs Info from Developers: 1
  - Actions total: 27

**Review: 0**

**Created patches: 0**

**Patches worked on: 0**

**Commits:**

## May 28

Change in Plans: I am starting with Point nodes, coz they are the most
used ones. I found the blend files for GN, they were right in front of
my eyes, just didn't see them. Exploring blend files.

Any side ideas: Make a frequency counter for Geometry Nodes.

Note for future me: Apply the transformation scale before instancing
etc. \[IMP\]

## May 27

Resolved 2 bugs, Observations: The new zip file for blender has a really
long name and it can/is giving error for too long paths.

## May 26

Tracker life is busy life and sometimes addictive. I am trying to take
mental note of some Geometry node bugs. I stumbled upon a
forward/backward compatibility bug with Join Geometry (yet to report),
the noodles stay stick on one single input node when a file opened in
previous version which doesn't support multi-input socket.

## May 25

Had meeting with mentors, Jacques and Habib, discussed the channels of
discussion, timings and possible meeting in the first week of June.
Attended Geometry nodes module meeting, it was fun, understanding the
role of each member. Active on bug tracker, involved in 4-5 reports.

## May 24

  - Active on Bug Tracker.
  - Involved in 5 reports.
  - Reported one bug related to GN (T88534).
