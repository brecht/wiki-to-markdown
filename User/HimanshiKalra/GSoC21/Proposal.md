**Project Title:** Regression Testing of Geometry Nodes.

**Name:** Himanshi Kalra

**Contact:** Handle on \#blender-coders: Calra

**Benefits:** Following the spirit of Everything Nodes, having a
framework in place to support regression testing of Geometry Nodes will
be beneficial. Detect bugs during the development phase. Less cost of
fixing overall, less time spent by moderators on bug tracker. Would help
in detecting if anything breaks.

**Deliverables**

*Deliverable 1:*

Build a Framework for Geometry Nodes.

*Deliverable 2:* Cover test cases for Mesh based Nodes

  - Mesh (Modifiers)

`* Boolean`  
`* Subdivide`  
`* Subdivision Surface`  
`* Triangulate`

  - Geometry

`* Transform`  
`* Join Geometry`

  - Mesh Primitives

*Deliverable 3:*

  - Developer Documentation
  - Polishing
  - Project Report.

**Stretch Goal**: Add the script for neatly arranging the nodes in the
editor..

**Project Details:** Geometry Nodes are being developed at a fast rate.
A regression testing framework could help developers to make changes
without worrying about breaking stuff because that will be caught by the
Geometry Nodes tests.

Currently tests for modifiers and operators use the Mesh Test Framework.
It works in the following way.

*Working of the Mesh Test Framework:*

  - Takes a Test and an Expected object.
  - Creates a duplicate of the Test object.
  - Applies the Modifier/Operator on the duplicated test object.
  - Performs comparison between the duplicated test object and Expected
    object.
  - Deletes the duplicated test object.
  - Outputs the Result of the comparison.

Since Geometry Nodes is a “Nodes” Project, it is a good idea to see how
Compositor Nodes are currently tested:

*Working of Compositor Automated Testing:*

  - Create a node setup in a blend file.
  - Run the compositor testing script.
  - Compares the newly Rendered image with the existing image.
  - Gives the result. (Tests Passed/Failed)

*Solution: Extending the Mesh Test framework for Geometry Nodes*

General Idea:

  - Create a class for holding Nodes specifications.
  - Create a class that would facilitate linking between nodes.
  - Creates a Test Node Tree on the Test Mesh.
  - Duplicate the Test Mesh.
  - Apply Geometry Nodes Modifier.
  - Run the test using the existing Mesh Test Framework.
  - Compare the Test Mesh with Expected Mesh.

**Priorities:**

  - Working on the framework.
  - Adding test cases mentioned.

Then, working on the script that makes the nodes looks neat (would like
it be simultaneous, but this would be more of a stretch goal)

**Advantages:**

  - The framework not only tests the regressability of Nodes, it also
    indirectly tests the Blender Python API.
  - Get fine control from within the code.
  - If we were to ever lose blend files, we could recreate the node
    setup faster from the existing framework.

**Points of Concern:** Hard to visualize in case the Node Tree gets too
complex (e.g. one of Erindale’s still life) from within the code.

**Addressing the Concern:** One can still create a node tree and test it
using the existing Mesh Test framework in a way Compositor Nodes are
tested. (Add the Node setup in a blend file, add a test case like for
existing modifiers, run the script for Mesh Test, compare...and so on)

**Project Schedule:** I would have my exams in the first week of May and
some sessions in the month of May and June. I think they would be
manageable with GSoC.

**May 17- June 7** (Community Bonding Period):

  - Get a better understanding of the Geometry Nodes project.
  - Study what are Geometry Component Types, Attributes for the
    framework.
  - Discuss the idea in more detail with mentors and its feasibility.
  - Do some initial testing and gather information of the Python API
    available for Geometry Nodes. (I personally faced some issues)

**June 7- July 12** (Evaluations)

  - Work on Deliverable 1 (Creating the framework)
  - Creating classes to support Geometry Nodes
  - Adding support for Input and Output Links

**July 12 - August 16** (Evaluations)

  - Working on Deliverable 2 (Adding test cases)
  - Minimum cases covered would be
  - Geometry (Join, Transform)
  - Modifiers as Geometry Nodes
  - Mesh Primitives
  - Making adjustments to the framework to be able to support the same.

**Bio:** My Bio isn't the impressive one, I ain't a prodigy or anything.
I live more on the dreamer's side. If you have ever been close to me, I
will wake you up with "Wake up, I have a super cool idea\!". This
proposal is born out of one of those ideas and I am glad to be working
on it. It was in summer of 2018, after joining college I was
re-exploring my passion for Computer Animation, so I downloaded the old
software called Macromedia Flash and started fooling around with it. And
then to up the level, I was exploring Synfig. I thought there was
certainly something lacking and then I finally stumbled upon Blender. I
even showed it to my friend, a mysterious Cube sitting in the centre of
a 3D plane without any buttons on any side. It was not until summer
break was over, I mustered up the courage to explore it. The version was
2.79b. Sure, we have a long way from there. This is the premise to the
GSoC 2020 Bio, to read further in the story, read the old Bio.

Thank you for reading.
