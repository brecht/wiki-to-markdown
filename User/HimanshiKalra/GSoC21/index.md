All links for GSoC 2021

**Project**: This year I will be working on Regression Testing of
Geometry Nodes.

**Mentors**: Jacques Lucke, Habib Gahbiche

  - [Proposal](Proposal.md)
  - [Daily Log](log.md)
  - [Weekly
    Reports](https://devtalk.blender.org/t/gsoc-2021-regression-testing-of-geometry-node-weekly-reports)
  - [Adding test for Geometry
    Nodes.](https://wiki.blender.org/wiki/User:HimanshiKalra/GSoC21/GeometryNodeTests)
  - [Final Report](FinalReport.md)
