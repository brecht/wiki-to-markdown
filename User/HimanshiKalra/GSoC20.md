GSoC 2020 Project: Regression Testing Frameworks

Mentors: Habib Gahbiche, Bastien Montagne

  - [First Draft
    Proposal](https://devtalk.blender.org/t/gsoc20-draft-proposal-regression-testing/12203)
  - [Proposal](Proposal.md)
  - [Timeline](Timeline.md)
  - [Weekly
    Reports](https://devtalk.blender.org/t/gsoc-2020-regression-testing-framework-weekly-reports/13389)
  - [Meeting Notes](meeting_notes.md)
  - [Daily Log](log.md)
  - [Running Automated Tests](PythonTests.md)
  - [Final Report](Report.md)
