  - path tracer parametrised by path length and samples light at end of
    path only

<!-- end list -->

1.  adaptive importance on path length. This would allow for effort to
    be placed where light contributes most
2.  allows quicker first image and updates as \~half the number of ray
    queries are needed

<!-- end list -->

  - Static BVH might be faster if instanced objects were not
    pre-transformed saves memory and caches better
  - embree should use more optimised routines for faster CPU
