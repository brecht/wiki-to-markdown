## Setp 14th 2021

  - Ray offsetting removal
  - PMJ sampling summary
  - Scrambling change comming would be good to get it working with PMJ
  - Discussed new sample settings Adaptive sampling with time as default
    versus old samples based settings

<!-- end list -->

1.  Easier to parallelize sample based approach
2.  Need more adaptive sampling techniques
3.  Sampling paths in a non-branched path tracing requires more adaptive
    sampling such as path guiding etc.

<!-- end list -->

  - Discussed BSDFs and matching versus the Disney version some
    differences are ok but should be documented
