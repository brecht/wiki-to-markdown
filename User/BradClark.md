## August 7th-11th

Work on wrapping up animation design document work for review and
feedback add in wirframes and mock ups Meet with Nathan to review
document first pass and have feedback notes to address on how to clean
up the document flow to allow wider feedback and review. ...Contract
wrapping up for this push of the project but I am working on final
deliverable the rest of this month with more open community input.

## August 1st-4th

Catch up with things I missed during vacation Continue on working on
final document assembly for design goals for Animation 2025 Plan
meetings with Animation Module this week Check in /test exiting 4.0
targets for Animation tools and fixes for NLA

UI wireframe mockups for consolidated action editor and user story
workflow process examples I had planed to get further but it was better
time spent taking and moving my notes and rough documents and start
organize them to a more public facing document for review. This took a
large part of my time a the end of the week and to get ready to break
down the tools more and wireframe up the things that need extra
visulization.

## July 17th-19th

This week is a short week then I am pausing work until Aug. 1st. Monday
was working extra to build out a wire and research what features we
would need in a combined UI for dope sheet/action editor and NLA
updgrade. Meeting with Nathan to go over status updates and talk about
next steps about rig nodes and constraints "Work on documentation for
core design documents, getting heads in place and notes to turn into
more clear descriptions of areas of focus.

  - update mockup UI more layers. Step by step editor mockup breakdown.
  - step by step mockup story flow
  - Muli-strip summary when many strips are open/active
  - Metastrip filtering and expansion with clean
  - Layers/vs strips summary display
  - Layer only
  - Layer only + Strips
  - Nates view:) on what he wants to see
  - Core model outlined with user workflow examples
  - One character no layers
  - Animator with Mocap/Mix
  - Cinematic animation..view
  - Rig Nodes-constraints-
  - Nates 2025 task “what the heck does this look”
  - Animation level constraints
  - Constraints defined in the animation block not scene “rigging level”
  - Constraints as offsets-delta constraints on layers
  - Or
  - Constraints as interpolators, alone complicated
  - Or
  - Constraints as attributes of Strips\*\*\*\*best of both?
  - Strip parameter space, control rig nodes controlling animation over
    a section of time for the strip
  - Strip S/E transition gaps
  - Layer one regular animation
  - Layer two, clip frame 10-20, for just one object, that says it is
    connected while pulling the values from the base layer
  - Source filmmaker -constraint space rebake or update procedurally
    retargetale.
  - What does a constraint system strip do when you retarget
  - Simulation layers or strips
  - Means binding tightly relationships instead of just simple curve in
    an action
  - Tension between animation block fancy control vs. old simple action

"

Investigated speed issues with the current editors and found quite a few
issues with the display and playback and interaction speed when the
dopesheet is active and the more it displays the slower it gets but the
graph editor doesn't have any slow down issues.

## July 10th-14th

  - Working on organizing my Animation "Flow State" design document
    based on the workshop
  - Working on formatting my work based on the Blender design guidelines
  - Adding some quick reviews of the Unreal and Omniverse retarget
    system
  - Design document work for the new streamline animation editor (Action
    editor + dopesheet)
  - Review Blog post notes from the Animation Workshop
  - Follow up with NLA fixes and test 4.0 animation features like
    butterworth filter
  - Work on review and improve the documentation for the current tools
    and workflows that exist but are unknown like Time slide feature of
    translate tool in dopesheet
  - Animation Module meeting and patch review.

## July 3rd-7th

1.  Back from Animation Workshop and spent time organizing and reviewing
    notes from the week.
2.  Starting to narrow down the focus and this week will apply the new
    animation system language to my notes on the 2025 system design doc.
3.  Going to try and get the main format of the document outlined and
    start breaking things into sections and in some areas build out
    deeper design proposals like for the Bake animation system as this
    is something that is important for 4.0 and beyond.
4.  Will setup a weekly meeting to go over everything with the Animation
    Module owners once the main outline is ready and stubbed in.
5.  I will be away from computer work on family vacation in a few weeks
    and I want to get this all set for when I come back I can get eyes
    on it.

Coming out of the workshop the goals feel achievable for a new
foundation of the animation system that we can build on going forward
providing a foundation for workflows and tool flexibility that is
isolated into the armature that creates a lot of workarounds.

7-6

1.  Animation Module Meeting

## Jun 26th-30th

All day on site working through the animation mode details with use
stories and workout out needs and use cases. Just going to note that due
to the workshop I am not updating much here right now.

## June 19th-23rd

  - Start design document for Bake Animation and get feedback from addon
    creators that use Bake animation/action as a core tool or have had
    to work around it.
  - Meeting with Nathan and Sybren about animation armature improvements
    and the Action data model as well as some other discussions around
    the timeline for features.
  - Animation hands on testing to work out some ideas and testing the
    improvements we have been making in the Module.
  - Animation Module meeting and follow up prep for Animation Module in
    person meeting
  - Feedback and refinement of improved Bake animation proposal needs.
  - get ready to fly to Animation work shop

## June 12th-16th

Larger follow up meeting with Nate to work on more detailed plans about
armatures, actions and user stories.

Deep dive into what we need Bake action to do that it doesn't do now and
doesn't have control over. ...continued reviewing and understanding more
aspects of the past workflow including looking at the IPO editor and
it's keymode and editable ghosts back in 2.3

Benchmark goal for next gen animation tools How to Animate Your 3D
Characters Fast CG Boost (With 5 minutes of animation and 25 minutes of
complex setups and workarounds on what we need to fix) If this isn't the
best advertisement for having RigNodes as a core part of the rigging
toolset I can't think of a better one, video here
<https://www.youtube.com/watch?v=sTOgDe3EmQ0>

Started working on refining ideas from a week of helping studio
animators work in Blender and tying to look at core problems vs. one off
tools but this is just a rough brain dump and some quick ordering of
priorities that is still very much WIP

The higher level goals for the animation system

  - Faster animation creation speed. This will come from both from
    improved software performance around rigging and animation and speed
    and effortless posing of characters including interaction with self,
    others, props and environment through separating posing from
    interpolation and high level manipulators exposing direct access to
    hidden properties.
  - A New Animation Data Model to support a flexible workflow when
    armatures, objects and make sure that control over reuse of the
    action data is in the animator's hands and supports scene level
    multi character and character to environment and vfx interactions.
  - Create entry points to grow and expand art assistance systems like
    AI, Physics, Machine Learning and other sources of animation
    “solving” can be accessed and blended directly with hand keyframed
    animation.
  - The last upgrade allowed Blender to have “everything is animateable”
    but the rest of the software has to support better clarity and
    control over it, allowing for controlling multiple things animating
    all at once. Better keyset control and less one at a time or hidden
    away keysets
  - NLA and Action Node system to view and offer different types of
    control and visual representation of animation to the animator. Give
    them control to filter animation, replacing constraints and fcurve
    modifiers with flexible tools and faster systems allowing motion to
    be explored and Inspire exploration with flexibility and powerful
    enough standard tools to allow animators to explore as Grease Pencil
    and Geometry nodes have inspired the community to create amazing
    work faster than ever before.
  - Global animation track and layer system to allow an easy to use
    workflow that isn't clip/NLA dependent
    <https://wiki.blender.org/wiki/Modules/Animation-Rigging>

/Bigger\_Projects\#Animation\_Layers

  - Also digging back into my own short film issues that I hit and still
    working on to identify workflow areas of interest
  - Short film breakdown needs and issues:
  - Shared working files between two remote artists working with synced
    online sharing folders \*\*control over file paths for linking isn’t
    easy or clear
  - 2 Character rig files - linked
  - Environment - linked
  - Camera Layout animation file Camera rig animation across 3 scenes
  - Scene 1
  - Scene 2
  - Scene 3
  - NLA is being used to offset and manage animation in time across all
    scenes
  - Character and Environment are overridden for animation
  - Character 1 and 2 have hair, cloth, that needs physics animation
  - Scene has physics snow/trees moving animation
  - Characters interact with environment (Footprints in snow, pushing
    branches
  - Characters interact with each other, including one character
    touching fur of the other
  - Character 1 (snow man)
  - Shape keys on face, face rig is separate armature animated at the
    same time as body. \*I wanted to be able to upgrade the face rig
    without messing with the body rig and animation
  - Cloth cache doesn’t always look right and I can’t edit or bake the
    mesh (\*Animall? Geo nodes shapekeys workaround?)
  - Face rig shapes aren’t final and wanting to work on shape keys in
    the scene without having to go back to the source file and possibly
    break something (I lost animation before doing this and didn’t know
    it)
  - Character 2 (fox)
  - Fur that needs to interact with environment and other character
    hands
  - Feet need to push into snow and work with particles.
  - Runcycle and other animations for the fox were done isolated and
    will be brought in to assemble and edit and polish
  - NLA but NLA can’t offset when looping
  - NLA and Actions are hard to create new for each scene, they live
    across all scenes depending on how you setup your characters.

Friday meeting review of the animation action system exploration with
Nathan and Sybren and review Motion Graphics Needs for animation system.

## June 5th-9th

  - Test some ideas and continue to refine my notes from last weeks
    meetings, working on slides and possible workflow ideas for
    animation nodes/NLA/hybrid systems
  - Look over the current tools and improvements for dense data editing
    with new filters and timing tools
  - Prep for meeting with Nate today on the 6th.

6th

  - Meet with Nate and Sybren to go over user case/animation system
    needs and requirements
  - Homework for next meeting:

`   Moving capabilities from armatures to objects. And changes to armatures. "Why can't I use this on an object?"`  
`   User stories as requirements.`  
`   What stressors are we trying to solve?`

  - What this comes down to is, if everything can be animated in
    Blender, why are the animation tools only for pose mode character
    animators, why is everyone else using animation tools and doing work
    in Blender with Motion locked out of having copy/paste poses,
    animation blending tools for keyframes, pose libraries that work for
    a collection of objects/values/attributes setting etc..
  - Vertical slice of moving just "blend to neighbor or layers" out of
    the armature into the scene level of Blender and what issues,
    questions, work does that create?

<!-- end list -->

1.  If we move and allow the use of the blend slider from the pose mode
    to work on any animated node, what does this do to selection vs.
    active?
2.  What does this do to undo?
3.  What happens if we do copy /paste pose mirror for objects that don't
    have a left right? or a rest pose? We will need to mark "center" to
    mirror around and have copy/paste pose work for global values and be
    able to quickly define a pivot for them to all work.

7th-8th

  - Worked on and wrote up a Design vision plan outline to start putting
    my ideas into concrete actions required.
  - Tested some animation module fixes, features and current workflow
    issues to help understand what we can focus on now as quick fixes
    vs. longer term goals.
  - The pressure of 4.0 deadlines and moving quickly to tear out
    Armature layers.
  - I have about 23 more blog posts to read and digest on Aligoriths
    Lair to understand the choices and thoughts that got us to the
    system we have now and to try not to throw out or misunderstand the
    current systems while also knowing that it will be required to
    remove and change what we need to to improve it.

Balance comes from understanding the past and to not throw out the
massive amount of work that has gone into testing, using and improving
the existing system.. and I am learning about tools and features that I
would have never discovered on my own or for a long time from now.

Animation Module meeting today- Two of my "babies" Parent space
transformation tools and butterworth filtering making progress and fixes
as well as more design information on single Actions that can contain
and map more than one objects animation.

## May 29 - June 2nd

  - Working on setting up user personas for animators to help guide
    features/workflow tools
  - Created a small slide show example for meeting with Nathan V. in the
    morning to better show a unified animation editing workflow with the
    File level action/scene level track-object level animation filtering
    always being active so you are never outside a clip and it is easy
    to edit clip contents.
  - Having animation be able to have an Animation Node modifer tree to
    process and procedualy adjust animation like Geo-nodes now and write
    back to the clip as output for blending and layering in the NLA.

30th

  - Meet with Nathan to review actionable items for Animation design.

Where does the animation data live and how do we manage/control what
happens when a keyframe is created (what should get a key) and where
that data live Lots of notes and some animation storage research and
looking at other tools/systems for concepts.

  - Talked about user personas for animator types and larger goals with
    control over the entire process of importing, creating, exporting
    animation data.

June 1st

  - another Nate meeting to talk about animation use cases and keying
    sets as an existing way to map what gets keyed where
  - Module meeting
  - After module meeting to talk through pose library workflow issues
    and bugs
  - UI discussion about interactive feedback needed in the NLA but using
    the VSE as the use case since it has more features currently.
  - Short review of the
    <https://code.blender.org/2023/06/node-tools-interface-and-baking/#overview>
    and a short discussion about what possible cross over having
    animation filter nodes be able to process animation data and from
    and export back to NLA clips for example like geo-node modifers.

## May 22 - 26

22nd

  - Meet with Jason Schleifer to gather some animator feedback on
    blocking issues with current animation layer system/NLA

  - Start rewriting/rough draft of animation system goals proposal to
    send to Ton.

  - Continued reading and doing design sketches going over Aligoriths
    notes ( I have 12 tabs open on his site)

  - Explore some addons and look over the Blender studio workflows as
    research

  - 
23rd

  - Meeting with Nathan V. and Dr. Sybren to pitch my overall direction
    and current rough draft vision for the animation system.
  - Looking over IK systems and other interaction issues that stand in
    the way of faster posing.
  - Still reading over and making notes on Aligoriths pose brush and
    ideas on how the NLA was supposed to work, keying sets, pose brush
    to replace the need /want for a 3d edit motion path and more

  
WIP: A way forward

What came from my meetings so far and the rough outline is a goal of
clarity and consistency in the animation system and tools that build
trust for the animators. They want control and predictability from the
tools and the features we build. Having a rig and tools that don't loose
data, having UI that doesn't hide away where keyframes are stored and
presetting up an Action system to allow for the ability to work sparse,
bake dense data or add animation layers and modifiers and work in a
non-linear view all is moved forward and is always clear where you are
in the creative process.. allowing animators to feel like "I created
this, not I ended up with this" We talked about modeling and time
topology and sub-division sculpting and re-topology along with code
diff/merge control that is missing from animation...bringing us to the
other Major part of the forward looking needs of the animation system\!
Visualization and editing tools that allow you to know and control the
final pose (clarity) and being able to edit it in view and reverse solve
back to all the places/layers/data buckets that need to be updated to
fix the temporal issues. Just like the geometry nodes and code compare
let you see what changed, what is influencing the final result our
animation system and tools should also. With a Non-linear action manager
as the core and heart of the system where you are always in a clip to
tools that let you extract and fit and change animation and merge it
without loss of important hand edited work from animators to better in
view feedback and manipulators to always being able to control the
remap/retarget of actions to different rigs or manage rig changes
without losing performances... we build trust through these tools and in
that the animators can get predictable results that they control and
speed up the amount of final animation.

  - 3 hour meeting with Nate to talk over action storage and management,
    if clarity in the UI and having the core timeline /animation system
    be IN the NLA state the whole time and evaluate and talk through how
    other animation management systems seem to work and what we want to
    keep /what is good about the current action system. We have a follow
    up meeting to present more concrete examples of what we can start
    working on beyond just talking about what is need...time to show it.

<!-- end list -->

  - I am starting the documentation of the current issue and making am
    mock up of what it could be like.

## May 11 - 19

Hi, Brad here,Not new, but new here. I am coming on part time as an
animation product designer Last week and this one, I have been exploring
and getting setup.

  - On boarding: Getting accounts setup and reviewing process documents.
  - Working on testing and finishing up Animation Module NLA tests and
    exploring some UI tests getting ready for 3.6 release and 4.0 work.
  - Reading over the Action proposal from Nathan
    <https://perm.cessen.com/2023/animation_module/2023_05_04_animation_data_model/#/13>
  - Watching Human Rigging from Nathan to review and confirm my current
    understanding of past workflow design
  - Thinking/notes around rapid editing of dense animation data-
    Animation as Time Topology and the relationship between
    sculpt/retopo and Animation keyframe creation and editing as a
    concept that started while I was at the Blender Conference.

Reading through the Animation System roadmap 2015 and Aligoriths notes
on all this

  - <https://code.blender.org/2015/03/animation-system-roadmap-2015-edition/>
  - <https://aligorith.blogspot.com/2018/03/thoughts-about-geometry-editing-ux.html>
  - <https://aligorith.blogspot.com/2018/04/future-of-animation-tools-some-wip.html>
  - <https://aligorith.blogspot.com/2015/03/animation-system-roadmap.html>
  - <https://www.youtube.com/watch?v=kEJFAsbzcmk&t=1306s4>

Overall these two weeks have been a deep dive to gather and understand
the HOW/WHY things are the way they are and what I can learn from it as
I shape my thinking and work to balance the reality of what we have to
work with and what needs to be done with what would be nice some day and
stay realistic in the goals and ideas for the future of Animation in
Blender. More to do.

May 19th-

  - Meeting with Nathan Vegdahl to talk over ideas and understand more
    about the Action update proposal
  - Talk through ideas and to get on the same page about how this
    impacts the rest of blender, Overrides, NLA and animation workflow
    and to help me get a grasp on how to start processing all the new
    information from the week into my own view/take/vision.
  - More notes on moving upfront the NLA/Layer workflow/Mixing Animaide
    style animation offset with High level Curve editing and having it
    all be core to the timeline/animation process vs. a thing you move
    data in and out of.
