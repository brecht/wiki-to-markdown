# Displaying simulation data: Final report

## Links

  - Branch:
    [soc-2021-simulation-display](https://developer.blender.org/diffusion/B/browse/soc-2021-simulation-display/)
  - [Proposal](Displaying_simulation_data_for_rigid_bodies_and_cloth)
  - [Weekly
    Reports](https://devtalk.blender.org/t/gsoc-2021-displaying-simulation-data-for-rigid-bodies-and-cloth-weekly-reports/19061)

## Summary

This summer I worked on displaying rigidbody simulation data in the
viewport. I worked on displaying all the types of data mentioned in the
proposal, except for the cloth springs. I couldn’t work on this as I ran
out of time.

## Completed work

### Velocity, acceleration, and forces

Velocity, acceleration and forces acting on active rigid bodies are
displayed in the form of color coded vectors. The types of forces
displayed and their color coding is as follows:

1.  Forces due to effectors (pink)
2.  Normal forces (pink)
3.  Frictional forces (yellow)
4.  Gravitational forces (pink)
5.  The resultant of all the above (cyan)

Each of these can be toggled separately in the forces panel, along with
an option to display their magnitude as text.

The normal and frictional forces displayed are the average force applied
during the timestep, (which is the average of the forces applied in all
the substeps).

Right now, the maximum number of forces that can be stored of each type
of force, is 3.

![Normal, frictional and net forces on a
cube|thumb|left](../../videos/Forces_video.mp4
"Normal, frictional and net forces on a cube|thumb|left")  
  
  
  
  
  
  
  
  
  

### Collisions

The frame of the collision shapes lights up if an object collided with
something within the past timestep. For primitive collision shapes that
have faces, which are the box, cone and cylinder, if a face participates
in a collision, the face will light up as well.

![Colliding boxes|thumb|left](../../videos/Collisions_video.mp4
"Colliding boxes|thumb|left")  
  
  
  
  
  
  
  
  
  

### Rigidbody states

Indicating a rigidbody’s state can be toggled in the display options
panel. If enabled, the rigidbody will be displayed in one of 3 colors:  
Active: Light grey  
Passive : Dark grey  
Kinematic: Blue tint  

### Non primitive collision shapes

The convex hull and mesh type collision shapes are drawn as wireframes
around the rigidbody, similar to other primitive shapes.

Compound child shapes are scaled according to their parent shapes. I
added this scale factor to the code so that the primitive compound child
shapes that are drawn are scaled accordingly

### Constraints

After I started working on the constraints I realised that the ideas I
had mentioned in my proposal were underdeveloped. The behaviour of the
constraints proved to be far less straightforward than I thought.

After a lot of testing their behaviour and discussion with my mentor
Sebastian Parborg, we came up with the following ways to visualise
constraints:  
1\. Slider: Central rod along the constrained axis to which the 2
objects are connected by perpendicular connecting rods. These rods can
slide up and down the central rod. The linear limits are shown as blue
walls on both sides of the objects. The walls fade in and out as objects
move closer/ farther from them. The panel has an option to toggle fading
of walls. ![2 Boxes constrained by a
slider|thumb|left](../../videos/Slider_video.mp4
"2 Boxes constrained by a slider|thumb|left")  
  
  
  
  
  
  
  
  
  
2\. Hinge: Objects are connected to a central rod like the slider, but
the connecting rods are allowed only to rotate about the central rod.
Limits of rotation are shown as disks that limit the rods’ rotation. ![2
Boxes constrained by a hinge|thumb|left](../../videos/Hinge_video.mp4
"2 Boxes constrained by a hinge|thumb|left")  
  
  
  
  
  
  
  
  
  
3\. Piston: This is a combination of the above two. The connecting rods
are allowed to slide along the central rod, as well as rotate about it.
![2 boxes constrained by a piston](../../videos/Piston_video.mp4
"2 boxes constrained by a piston")  
  
  
  
  
  
  
  
  
  
4\. Fixed: This is simply a line connecting the 2 objects

## Limitations/ Known issues

**Collisions**:

  - Some edge cases for face collisions are not detected, possibly
    because the code does not pass collision points that lie exactly on
    an edge of the face.
  - The code iterates over the persistent manifold to find contact
    points after each substep. A better way to do it might be to use
    collision callbacks

**Convex hull and trimesh shapes**:

  - There seems to be a memory leak, possibly because I need to take
    into account the 2 copies of data while freeing the memory allocated
    for the mesh.

**constraints**:

  - I wasn’t able to create visualisation options for the generic and
    motor constraints. The motor uses a generic 6dof constraint
    internally. The generic constraint behaviour is complicated and
    difficult to represent by a simple mechanical model.
  - Constraints currently do not work with kinematic objects.

## Acknowledgements

A big thank you to Sebastian Parborg, for refining my ideas, and for
amazing guidance throughout the project; and to Sebastián Barschkis for
pointing me in the right direction and helping with version control and
other doubts. I’m incredibly grateful for getting to work on this. Thank
you so much\!
