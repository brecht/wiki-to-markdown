# macOS Libraries

Notes from Campbell are very relevant if you ignore the Linux specific
stuff:
[User:Ideasman42/BlenderLibsForLinux](../Ideasman42/BlenderLibsForLinux.md)

In particular SVN configuration and how to svn add/rm are important.

## Arm

The basic thing to build libraries is to run \`make deps\`, which will
download and compile all libraries in \`../build\_darwin/deps\`.

Most likely it will tell you that you are missing some tools, and give
you homebrew command to install them.

Äfter this is done it will automatically install the updated libraries
to \`lib/darwin\_arm64\` from where they can be committed.

However note that it will likely update all libraries even those that
had no change, as we do not have reproducible builds. So I often \`svn
revert\` parts that effectively stayed the same.

## Cross-Compiling x86\_64

x86\_64 libraries are currently cross compiled on Arm, which is
convenient to have on a single machine but it is a kludge.

### Homebrew

This requires two homebrew installs, one for arm and one for x86\_64.
They will be in different folders automatically.

macOS supports dropping into a x86\_64 terminal like this.

``` bash
arch -x86_64 zsh
```

From here you can install homebrew a second time.

I then added the following in \`\~/.zshrc\`, so it automatically picks
up the right homebrew install depending on the arch.

``` bash
ARCH=$(arch)
if [[ $ARCH == "i386" ]]; then
  eval "$(/usr/local/bin/brew shellenv)"
else
  eval "$(/opt/homebrew/bin/brew shellenv)"
fi
```

This probably replaces some existing code put there by homebrew.

### Old SDK

For x86\_64 you need to build with an older SDK than the one that comes
by default. The reasons are not fully clear, as generally you should be
able to use newer versions but it's currently failing otherwise.

I download the old 10.15 SDK from:
<https://github.com/phracker/MacOSX-SDKs/releases/download/11.3/MacOSX10.15.sdk.tar.xz>

The annoying part now is that getting all the libraries to pick this up
is difficult, I have to move the newer SDKs out of the way whenever I
build the x86\_64 libraries.

So the regular SDK folder for me is (this is for command line tools,
Xcode has a different path):

``` bash
/Library/Developer/CommandLineTools/SDKs
```

And I added another one next to it:

``` bash
/Library/Developer/CommandLineTools/SDKs.x86
```

There I put the MacOSX10.15.sdk folder, and a MacOSX.sdk symlink to it.

And then whenever I need to build, I swap it out with the regular SDKs
folder (see below).

### Building

So always drop into the x86\_64 terminal first.

``` bash
arch -x86_64 zsh
```

Setting up the build should be something like this, modify paths as
needed:

``` bash
cmake -DHARVEST_TARGET=/path/to/lib/darwin -DCMAKE_OSX_ARCHITECTURES=x86_64 -S blender/build_files/build_environment -B build_darwin/deps_x86
```

And then I have this script to build:

``` bash
sudo mv /Library/Developer/CommandLineTools/SDKs /Library/Developer/CommandLineTools/SDKs.arm
sudo mv /Library/Developer/CommandLineTools/SDKs.x86 /Library/Developer/CommandLineTools/SDKs
make -j16 install
sudo mv /Library/Developer/CommandLineTools/SDKs /Library/Developer/CommandLineTools/SDKs.x86
sudo mv /Library/Developer/CommandLineTools/SDKs.arm /Library/Developer/CommandLineTools/SDKs
```

## LTS and CVEs

We committed to updating libraries when security issues for them are
found, both for master and 3.3 LTS.

Checking for such issues is automated by "make cve\_check" in the
libraries build folder.

If you have enough disk space you may consider setting up an entirely
separate build and lib folder for 3.3 since switching with master is
painful, as you will need to clear the entire build folder for it to
rebuild correctly.

## Committing a Subset of Libs

``` bash
svn changelist -R tmp *
svn changelist -R --remove library_a library_b
svn revert -R --changelist tmp .
svn changelist -R --remove --changelist tmp .
svn status
```
