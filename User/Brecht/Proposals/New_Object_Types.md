# Proposal: New Object Types

NOTE: this is an outdated proposal that was largely implemented, and
there are specific code documentation pages with detail about the actual
implementation.

For more powerful procedural workflows and more clear design, we should
add 3 new object types.

## [Hair](Source/Objects/Hair)

Hair should be a system separate from the current particle system.

It would work like this:

  - Hair datablock similar to Mesh, containing hair curves and curve
    keys.
  - Object type hair that uses this datablock, which can be selected,
    hidden, duplicated, instanced, etc.
  - Hair objects would link to a mesh object to position hair on.
  - Edit mode would be similar to edit modes for other object types.
  - Hair objects would have modifiers. These would serve various
    purposes:
      - Create an initial distribution of hair on a mesh, which is then
        applied for editing.
      - Generate child hairs.
      - Add curls or any other deformation.
      - Physics simulation modifiers, again similar to meshes.
  - Rendered directly as hair by Cycles and Eevee.
  - Instancing objects along hair could be supported too, similar to
    instancing for verts/faces.

Modifiers could be implemented as nodes that have hair datablocks as
input and output.

With child hair generation being just another modifier, users would have
the choice to apply physics and deformations before and/or after.

Reading hair from Alembic caches would also be straightforward in this
design.

![../../../images/Design\_object\_types\_hair.png](../../../images/Design_object_types_hair.png
"../../../images/Design_object_types_hair.png")

## [Point Cloud](../../../Source/Objects/PointCloud.md)

Particles would be split off from mesh objects too. For basic particle
simulation the output could be a point cloud. This would mean:

  - PointCloud datablock with a list of points and custom data layers
    for velocity, rotation, color, etc.
  - Object type point cloud, which can be selected, hidden, duplicated,
    instanced, etc.
  - Cycles and Eevee would ideally be able to render such points
    directly with little memory usage.

In most cases procedural particle nodes would generate the point cloud
completely, without the need for a user created point cloud datablock.

Applying modifiers for manual editing or loading point clouds from
external files could be possible too. We can think of 3D scans or
objects like sand. But the first priority would be as the default output
data type for particles.

## [Volume](../../../Source/Objects/Volume.md)

Volumes are currently generated through the smoke modifier on meshes,
but really need their own data type too.

We can image many types of volumes:

  - Smoke simulation domain object.
  - OpenVDB file sequence from disk.
  - Procedurally generated volume with nodes.

Again we would have a volume datablock and object type. There could be
modifiers and nodes for loading volume data from disk, smoke simulation,
deformation, adding high resolution detail, etc.

An edit mode to manually sculpt volume shapes would be powerful, but
like particles the first priority would be procedural generation.

The underlying data structure could be OpenVDB, as this is already an
optimized representation and the library comes with many useful
operations that can be turned into nodes.

![../../../images/Design\_object\_types\_clouds.png](../../../images/Design_object_types_clouds.png
"../../../images/Design_object_types_clouds.png")

## Relations

By linking together different objects we can potentially do many
interesting effects.

  - Particle emission from one or more mesh objects.
  - Particle emission from other particle objects.
  - Mesh generated from fluid particles.
  - Volume clouds modeled with a mesh.
  - Hair generated from mesh shape.

The simplest way to do this would be to require creating an object for
each data type. Nodes on each object would have the same type of input
and output datablock. This fits well in the existing design, and in many
cases is most convenient. However for more procedural system we might
want something more flexible.

## Multiple Outputs and Changing Types

The [Geometry Nodes](../../../Source/Nodes/Modifier_Nodes.md) system now
implements ways to define relations and convert between these object
types.

A flexible node system could for example turn a mesh object into a
volume after evaluation. Or one object might output multiple other
objects with arbitrary types. Maybe the number of output objects would
be dynamically determined.

To what extent this should be supported would be for another design
document to delve into. This is both a technical and usability
challenge. It may be best to leave this as a later step, after initial
new object types and nodes are implemented.

![../../../images/Design\_object\_types\_multiple\_outputs.png](../../../images/Design_object_types_multiple_outputs.png
"../../../images/Design_object_types_multiple_outputs.png")

This was implemented in the form of [Geometry
Sets](../../../Source/Objects/Geometry_Sets.md) as part of the geometry
nodes project.
