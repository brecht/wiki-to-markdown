### Libraries Cross Compiling

In \`.zshrc\`:

``` bash
ARCH=$(arch)
if [[ $ARCH == "i386" ]]; then
  eval "$(/usr/local/bin/brew shellenv)"
else
  eval "$(/opt/homebrew/bin/brew shellenv)"
fi
```

Enter an x86\_64 terminal:

``` bash
arch -x86_64 zsh
```

\`CMAKE\_OSX\_SYSROOT\` set to 10.15 SDK.
