\_\_TOC\_\_

## September 14 - 18

  - Code review
  - Meetings and docs regarding future organization of development
  - Bugfixes
      - Fix T79929: crashes with Cycles denoising on older Macs
      - Fix T78793, T79707: Cycles crash on macOS with older CPUs
      - Fix OpenCL render error in large scenes
      - Fix T80630: potential crash in volume grids in Eevee shaders
  - Tests: bundle tests for some modules in their own executables
  - CMake: clean up setting of platform specific linker flags

## August 3 - 7

  - Code review
  - Design doc regarding USD and Collections
  - Particle nodes design discussion
  - Point Clouds: basic support for instancing objects on points
  - Buildbot: deploy changes for building every commit, additional fixes
  - Bugfixes
      - Cycles: load OpenVDB file earlier in Blender export
      - Fix Cycles + grease pencil render failing after recent GPU
        refactoring
      - Fix T79586: "rendering paused" not shown when viewport render
        starts paused
      - Fix T78869: denoising performance regression on Windows
      - Fix T78777: Cycles motion blur test differences between AVX/AVX2

## July 27 - 31

  - Code review
  - Release notes editing
  - Particle nodes presentation
  - Buildbot
      - Various fixes to get tests passing again
      - Support for building every commit
  - Bugfixes:
      - Fix T79397: blurry icons at some UI scales, after recent
        refactor
      - Fix T79355: switch to texture paint workspace changes image in
        other workspaces
      - Fix T61607: OpenEXR files with less common RGB channels names
        not loaded
      - Fix T79067: Cycles panorama viewport render stretched after
        recent changes
      - Fix T79219: Cycles NLM denoiser clean passes broken after recent
        changes

## July 13 - 17

  - Code review
  - Build: make update support for git tags
  - Bugfixes
      - Fix T78537: too much memory usage rendering animation with
        persistent images
      - Fix T78930: Cycles OpenCL error on graphics cards that don't
        support half float
      - Fix T78881: Cycles OpenImageDenoise not using albedo and normal
        correctly

## June 30 - July 3

  - Code Review
  - Bugfixes
      - Fix T77095: fix Cycles performance regression with AMD RX cards
      - Fix T78367: triangulate modifier does not preserve sculpt vertex
        colors
      - Fix error in new Hair data type file reading
      - Fix T77984: Cycles OpenCL error rendering empty scene
      - Fix missing GPU image free in background mode
      - Fix T77825: autosave missed too often while sculpting
      - Fix T78038: Cycles crash rendering with volume object and motion
        blur
      - Fix T78358: random crash editing shader nodes with textures
      - Fix T78447: Cycles vertex color node not working with hair
  - Cycles
      - Rename viewport denoise Fastest option to Automatic and extend
        tooltip
      - Work towards final render denoising with OpenImageDenoise
  - Refactored generic geometry attributes patch

## June 22 - 26

  - Code review
  - Cycles
      - Add Intel OpenImageDenoise support for viewport denoising
      - Add denoising settings to the render properties
      - Add support for rendering sculpt vertex colors
      - Add experimental preference to replace magic debug value 256
      - Commit Embree, hair curve and TBB changes
  - Python API: support integer default for bpy.props.EnumProperty
  - Bugfixes
      - Fix crash compiling Cycles OpenCL, after recent TBB changes
      - Fix T78295: reload scripts disables Cycles render passes in the
        compositor
      - Fix T78199: reload scripts error when icon is used multiple
        times in toolbar
      - Fix incorrect Python API description for bpy.data.version
      - Fix viewport denoising not working if start samples higher than
        total samples
      - Fix T78149: Cycles memory leak rendering animation with Embree
  - Build
      - Remove unused lapack/hidapi/mingw32 build infrastructure
      - Show helpful error when accidentally using GNUMakefile on
        Windows
      - Upgrade a few smaller Linux/macOS only libraries to latest
        versions

## June 15 - 19

  - Code review
  - UI and sculpt module meetings
  - Sync Cycles standalone repo
  - Buildbot changes to use separate builders for branches
  - Render/Cycles module phabricator project reorganization
  - Bugfixes
      - Fix for T77095: work around render artifacts with AMD Radeon RX
        4xx and 5xx
      - Fix T77915: Cycles OSL microfacet closure not working in custom
        shaders
      - Fix random crash in Cycles smoke volume loading

## June 8 - 12

  - Meetings with every module for 2.90 plans
  - Code review
  - Work on finishing Embree integration in Cycles
      - Optimize curve intersection for GPU
      - Improve shading and render results for ribbons
      - Various other bug fixes and polishing
  - Fix Cycles viewport missing refresh when changing scene hair
    settings

## June 2 - 5

  - Code review
  - Cycles: work towards enabling Embree by default
      - Build: use TBB as Embree tasking system
      - Attempt to optimize GPU render
      - Attempt to use ribbons with fake normals as fast option
  - Cycles: work towards using TBB for task pools and task scheduler
    (code cleanup day)
      - Cleanup: use move semantics for task pool functions
      - Cleanup: minor refactoring around DeviceTask
      - Cycles: make TBB a required library dependency, and use in a few
        places
      - Cleanup: use lambdas instead of functors for task pools, remove
        threadid
      - Cleanup: remove task pool stop() and finished()
      - Cycles: replace own task pool implementation with TBB task
        groups
  - Linux: update appdata for 2.83 release
  - Fix T77273: crash enabling portal lights

## May 25 - 29

  - Code review
  - Cycles: change perspective depth pass to be more standard
  - Blender: change bugfix release versioning from a/b/c to .1/.2/.3
  - Update Cycles standalone repo for 2.83
  - Release notes polishing
  - Bugfixing and triaging
      - Fix NaN in Cycles environment texture importance sampling in
        some scenes
      - Fix T77109: Cycles viewport render crash after object add and
        undo
      - Fix T77009: texture baking crash on Windows
      - Fix T76925: more Cycles OpenCL compile errors with some drivers
        on Linux

## May 18 - 22

  - Code review
  - Bugfixing and triaging
      - Fix T76925: Cycles OpenCL compile error with some drivers on
        Linux
      - Fix T76916: Cycles missing deform motion blur for shape key +
        subdiv modifier
      - Fix Cycles viewport render fully restarting after undo
      - Fix T76858: non-thread safe sculpt memory allocation when using
        --debug-memory
      - Fix PBVH parallel range not initializing user data correctly in
        some cases
      - Fix unnecessary buffer reallocation in sculpt mode, causing an
        assert
      - Fix potential crash due to dyntopo GPU buffer invalid memory
        access
      - Fix part of T76544: dyntopo sculpt undo stack memory leak
  - Refactoring
      - Cleanup: make guarded memory allocation always thread safe
      - Cleanup: remove legacy mmap memory allocation for 32 bit

## May 11 - 15

  - Code review
  - Proposal and patch for LTS versioning number changes
  - Bug fixing and triaging
      - Fix T73984: unnecessary Cycles viewport updates with object
        texture coordinates
      - Fix --debug-cycles printing CUDA devices twice
      - Fix T52782: crash rendering more virtual parent particles than
        children
      - Fix T59089: --engine command line option does not affect 3D
        viewport render
      - Fix T75071: rendering fails when all render layers disabled, but
        using compositor
      - Fix T66005: crash deleting render slot while rendering to it
      - Fix T75715: crash rendering animation with persistent images
      - Fix T76388: Freestyle not respecting Show Emitter settings
      - Fix T75365: memory leak when writing PNG fails due to lack of
        disk space
      - Fix T72920: Snap package fails to play audio and blocks audio in
        other apps
      - Fix T76535: Eevee + Freestyle render crash with many strokes
      - Fix CMake using different OpenEXR / USD includes and libraries
        in some cases
      - Fix missing standard OpenColorIO roles, as reported by ociocheck
      - Investigated NVIDIA CUDA driver screen freezes on Linux
  - Cycles
      - Code refactor to bake using regular render session and tiles
        (old patch)

## May 5 - 8

  - Code review
  - Bugfixes:
      - Fix T76427: edit mesh undo hangs when building without TBB
      - Fix T76324: face set operators not working from Python console
        by default
      - Fix T76471: timer not removed after changing file browser to
        another type
      - Fix T76525: crash drawing metaballs with multiple material slots
      - Fix T76510: Eevee OpenVDB render artifacts due to texture
        clamping
      - Fix T76468: volume sequence render not using correct frame in
        renders
      - Fix Python error in scene without sequencer
      - Fix T76414: crash using Python module that uses NVRTC
      - Tasks: support build with TBB version 2017
      - Fix T76318: lower part of new preferences window does not work
      - Fix Cycles Python error when device name is not a valid UTF-8
        string
  - Cycles: mark CUDA 10.2 as officially supported

### Next Week

Continue fixing bugs as part of the bug sprint for 2.83.

## April 28 - May 1

  - Code review
  - Task: Use TBB as Task Scheduler (most implemented months ago,
    finished by Jeroen)
  - Bugfixes
      - Fix T75995: Cycles render artifacts with overlapping volumes
      - Fix T76309: changing AOV type does not update compositor socket
      - Fix some LLVM symbols outside of the llvm namespace being public
        on Linux
      - Fix Python bz2 module failing to import on older macOS versions
      - Fix T71334: top part of render window disappears on repeated
        renders
      - Fix T75432: Cycles progressive refine render slow with denoising
        data
      - Fix T76044: update Cycles to build with OSL 1.11 master
      - Tests: fix some tests passing even if there are Python errors
      - Fix Python bundled module test error
      - Fix T75973: don't show raw Python errors to users for invalid
        shortcut paths

#### Next Week

More bug fixing and code review.

## April 20 - 24

  - Code review
  - Bug triaging
  - Objects: add infrastructure for hair, pointcloud, volume modifiers
  - Bugfixes
      - Fix T73195: stereo camera view does not show background image
      - Fix T75607: crash trying to sculpt while remesh is in progress
      - Fix T75611: slow transform of many objects at the same time
      - Fix T75987: crash entering edit mode with keyed particles
      - Fix T76005: BLI\_task test failing after recent changes
      - Fix headless build failure on macOS
      - Fix T74423: Cycles rendering artifacts with CUDA 10.2
      - Fix T75981: crash in sculpt mode with mesh that used to have
        multiple materials
      - Fix 3D viewport select using grease pencil engine when not
        needed
      - Fix T75625: crash on exit in macOS after selecting objects
      - Fix T75909: icons memory leak in headless build
      - Fix T75969: view layer add with Copy Settings does not copy all
        data
      - Fix clang-format differences between version 6 and 9

#### Next Week

Continue to focus on bugfixing, some catch up with code review.

## April 14 - 17

  - Code review
  - Bug triaging
  - Release management for 2.83
  - Manual updates for Cycles 2.83 features
  - Bugfixes
      - Fix poor video sequencer preferences UI layout
      - Fix T75774: rename Musgrave texture output from Fac to Height
      - Fix T73977, T73825: ignore Python user site-packages directory
        by default
      - Fix splash screen not showing button to load config from 2.83
        into 2.90
      - Fix T75542: toggling modifier visibility not working correct
        with undo speedup

#### Next Week

Mostly focus on bugfixing, trying to get the main showstopper bugs in my
areas fixed for 2.83.

## April 6 - 10

  - Code review
  - Cycles: more work towards decoupling GPU occupancy from tile size
    (in progress)
  - Cycles: user manual updates for new 2.83 features
  - Bugfixes:
      - Fix T75542: toggling modifier visibility not working correct
        with undo speedup
      - Fix T75288: crash in Cycles image loading with multithreaded
        shader compilation
      - Fix T75290: Cycles crash with out of bounds memory access in
        volume mesh build
      - Fix T75445: Filmic transform not working when using Turkish
        locale
      - Fix T74572: adaptive sampling still not working correct with
        shader AOVs
      - Fix missed depsgraph update after undo in some cases
      - Fix volume object not rendering correct frame right after
        loading
      - Fix volume object not loading frame sequences correct in some
        cases
  - Fix Blender not rebuilding when changing linker script
  - Cleanup:
      - Refactor adaptive sampling to more easily change some parameters
      - Remove GHOST API to query tablet state from Window
  - Build: use -no-pie for portable builds on Linux

#### Next Week

Cycles bug fixing for 2.83. Code review (particularly particle nodes
patches). Continue on Cycles adaptive sampling improvements for GPU,
this will be for 2.90. Release management for 2.83.

## March 30 - April 3

  - Cycles: work towards decoupling GPU occupancy from tile size (in
    progress)
  - Undo: change depsgraph recalc flags handling to improve performance
    (under review)
  - Volumes: add volume.grids.frame\_filepath to get the current frame
    filepath
  - Bugfixes:
      - Fix T75287: other Cycles render passes wrong when using
        Cryptomatte
      - Fix T74572: adaptive sampling not scaling render passes
        correctly
      - Fix (harmless) PCRE not found warning when configuring CMake on
        Linux
      - Fix T75223: Luxrender add-on failing to load on macOS
      - Fix undo incorrectly detecting mesh as always changed after
        edits
      - Fix undo misdetecting identical future chunk in some cases
      - Fix undo debug logging not printing all types of undo pushes
      - Fix missing NULL terminator for new brush option
  - Cleanup
      - Refactor \`read\_libblock()\` function for reading datablocks
        (under review)
      - Cleanup: simplify Linux buildbot config, where default values
        already match
  - Code review

#### Next Week

Continue on Cycles adaptive sampling improvements for GPU, more bug
tracker work and minor code review. Ended up spending more time on undo
last week than planned, but I think it was useful.

## March 23 - 27

  - Cycles OpenCL performance
      - Work around OpenCL performance regression after AOVs and vector
        rotate
      - Slightly improve OpenCL performance by reordering SVM enum
        values
      - Limit number of processes compiling OpenCL kernel based on
        memory
      - Review & testing: Use OpenCL C 2.0 if available, to improve
        performance for AMD
  - Windows high frequency tablet events: patch review, testing and
    minor fixes.
  - Font rendering:
      - Always use international font
      - Remove non-unicode font and simplify default font loading code
  - Provide ideas and feedback for animation playback performance
    project
  - Investigate current state and performance of undo optimization
    project
  - (Long) meetings about release management and GPU rendering
  - Code review
  - Bugfixes:
      - Fix T74417: Freestyle render removes image texture users
      - Fix T74711: tiling brush option in image editor not working
        anymore
      - Fix T75090: crash with old NVIDIA drivers after overlay refactor

#### Next Week

Didn't get to adaptive sampling improvements for GPU last week, will try
again this week. Further plan to work on Cycles bug tracker, review
shader node related Cycles patches for 2.83. And maybe do some small
volume object improvements from the task list if there is time left.

## March 16 - 20

  - New volume object committed to master
      - UI: add new icons for Volume, Hair and PointCloud
      - Objects: add Volume object type, and prototypes for Hair and
        PointCloud
      - Objects: Eevee and workbench rendering of new Volume, Hair,
        PointCloud
      - Cycles: support for different 3D transform per volume grid
      - Cycles: support rendering new Volume object type
      - Cycles: support for rendering of new Hair object prototype
      - Cycles: change volume step size controls, auto adjust based on
        voxel size
      - Volumes: add render settings for volume datablock
      - Tests: add OpenVDB volume tests and benchmark file
      - Write release notes
  - Refactoring
      - Cleanup: process colorspace conversion with a 1D pixel array
  - Bugfixes
      - Fix T73372: cryptomatte not filling last pass for odd number of
        levels
      - Fix T67712: cryptomatte sockets created in wrong order in some
        cases
      - Fix T68370, T74973: Cycles cryptomatte not working when other
        passes are enabled
      - Fix crash with empty volume object and points drawing
      - Python API: add bl\_use\_stereo\_viewport for RenderEngine
      - Fix T74345: missing albedo for Cycles principled hair BSDF
      - Fix Python error in Cycles baking panel
      - Fix Cycles crash in Windows debug mode with volumes
      - Fix headless and Python module build after recent alert icon
        changes
      - Fix error using CUDA in plug-ins on Linux/macOS, hide our CUDA
        symbols
      - Fix T74776: Cycles crash with missing image texture after recent
        changes
  - General code review and triaging

#### Next Week

Cycles bug fixing (including OpenCL performance regression), try to
improve adaptive sampling for GPU render, code review of smaller patches
for 2.83.

## March 9 - 13

  - Cycles: load OpenVDB grids directly in Cycles (still as dense grid)
  - Cycles: add internal default volume shader, to be used for new
    volume object
  - Cycles: disable RTTI only for OSL files, other libraries like
    OpenVDB need it
  - Cycles: add view layer setting to exclude volumes, like hair and
    surfaces
  - Volumes: add option to display points in wireframe mode
  - Eevee: internal support for arbitrary number of volume grids
  - Smoke: put density/color in separate textures, fixes for workbench
    shader
  - Refactoring
      - Cleanup: add device\_texture for images, distinct from other
        global memory
      - Cleanup: refactor image loading to use abstract ImageLoader base
        class
      - Cleanup: add ImageHandle to centralize image ownership logic
      - Cleanup: stop encoding image data type in slot index
  - Bugfixes
      - Fix T73626: crash scrubbing timeline with Cycles viewport and
        smoke/fire
      - Fix T67718: Cycles viewport render crash editing point density
        settings
      - Fix Cycles incorrect result when compressing some 8 bit log
        colorspace images
      - Fix T74612: file browser thumbnails not showing and using CPU
        continuously
      - Fix error in grease pencil vertex paint levels operator
      - Fix part of T73921: hang with Eevee light baking and Mantaflow
  - General code review (VR, undo, ..)

#### Next Week

Commit new volume object to master. Try again to find time for
optimizing Embree curve intersection code. More code review.

## March 2 - 6

  - Cycles adaptive sampling: Optix support and various fixes/polishing
  - Cycles: set up automated performance profiling over time
  - Bugfixes:
      - Fix T74375: grid levels theme preference reset to default
  - Refactoring
      - IDs: change FILTER\_ID\_\* to 64 bit to make space for new ID
        types
      - IDs: modify wm.previews\_clear operator to make space for new ID
        types
      - RNA: support 64 bit boolean bitflags in DNA
      - Investigate/prototype RNA runtime registration
  - Cleanup
      - Move camera, lights, world, hair, volume, points to IDTypeInfo
      - Tweak Cycles \#includes in preparation for clang-format sorting
  - General code review

#### Next Week

Mostly code review, work more on optimizing Embree curve intersection
code.

## February 24 - 28

  - New volume object:
      - Tweaks for better initial setup of volume object
      - Complete Eevee dense grid rendering
      - Make hair and pointcloud data types a build option
      - Submit code reviews and address review comments
      - Fix various bugs
  - Cycles: move sss and diffuse transmission into diffuse pass
  - Objects: make evaluated data runtime storage usable for types other
    than mesh
  - Builds
      - Add compatibility between precompiled libraries and new glibc
      - Ignore system paths when using precompiled libraries on Linux
      - Don't include WITH\_SYSTEM\_GLEW as part of make lite
      - Fix make deps failing to build opencollada on Linux, due to line
        endings
      - Fix .arcconfig not working with latest arcanist version
  - General code review
  - Bugfixes
      - Fix T74095: crash deleting all faces in edit mode with Cycles
        rendering
      - Fix Cycles Embree hair + motion blur failing after recent
        Catmull-Rom change
      - Fix T74063: Cycles light pass viewport display wrong with
        saturated colors
      - Fix T74262: Cycles OpenCL error on some devices after recent
        changes
  - Cleanup
      - Deduplicate OpenVDB library definitions/include/libs logic
      - Add ImageKey to avoid longer argument lists and duplicated code
      - Split image sequence detection into own file, and make it
        reusable
      - More refactoring of GPU material attributes and textures
  - Help writing blog post about release schedule

#### Next Week

Review and get Cycles adaptive sampling committed. Continue with volume
object code review and polishing. Attempt to make Embree curve
intersection code fast enough for the GPU. General code review and bug
fixing.

## February 17 - 21

  - Meetings and planning for asset manager and other UI work
  - New volume object:
      - Work towards support per-grid transform for Cycles and Eevee
      - Cleanup: add ImageKey to avoid longer argument lists and
        duplicated code
      - Cleanup: more refactoring of GPU material requested
        attributes/textures
  - Embree: work towards getting it ready to enable for CPU by default
      - Fix Embree failing on objects with a very high number of motion
        steps
      - Fix Cycles Embree crash with AO shader local only option
      - Fix Cycles Embree test failures with shadow catcher
      - Fix potential clashing Embree symbols with external renderer
        add-ons
      - Start porting Embree curve intersection to CUDA/OpenCL for
        compatibility
  - Code review
  - Bugfixes:
      - Fix T73932: modifying keyframes in nodes fails when there is an
        image sequence
      - Fix crash loading .blend file saved with Blender 2.25
      - Fix T73954: Cycles viewport render from camera not respecting
        aspect ratio
      - Fix macOS LLVM precompiled link error with older Xcode versions

#### Next Week

More work on getting Cycles patches commit ready, and continue work on
completing new volume object rendering and viewport drawing.

## February 10 - 14

  - New volume object:
      - Support arbitray number of grids for Eevee shaders
      - Add wireframe display using OpenVDB tree nodes
      - Smoke: put density/color in separate textures
      - Better integration into workbench drawing
      - Show data type in grids list
      - API functions to modify grids in the modifiers stack
      - Various bugfixes
  - Cleanup:
      - Work around clang-format differences between versions
      - Refactor GPU material attribute and texture requests
      - Split off code from gpu\_codegen.c into smaller files
      - Don't perform some GPU shader codegen operations twice
      - Fix macOS build warnings about Boost symbol visibility
  - macOS libraries upgrade for OSL / LLVM / Freetype / Embree
  - General code review (about 30 patches)
  - Bug triaging
  - Bugfixes:
      - Fix T68243: Python sqlite module not working on macOS
      - Fix potential crash with Eevee render of missing image textures
      - Fix Eevee shader node error when using both RGB and vector curve
        nodes
      - Fix crash in some Eevee shader node setups after vector math
        node changes
      - Fix T67924: transform right/up arrow keys not working on macOS

#### Next Week

More patch review, and work on completing new volume object rendering
and viewport drawing.

## February 3 - 7

  - New volume object:
      - Support for frame sequences
      - Browse VDB files from add menu, auto detect sequences, drag &
        drop
      - Add global volume grid cache (for sharing grids between
        datablocks)
      - Use principled volume by default in volume object materials
      - Debug logging for grid file load/unload
  - Cleanup
      - Refactor default materials and shader nodes
      - Add type inheritance for Cycles nodes
      - Export particle hair as a separate Cycles object
      - Split Cycles export into smaller files
      - Remove unnecessary operations in Cycles curves export
      - Split Cycles Hair and Mesh classes, with Geometry base class
      - Simplify Cycles primitive attribute map storage
  - Design discussions and planning for particle nodes
  - General code review
  - Bugfixes
      - Fix T73364: crash calling particle\_sytem.co\_hair() on disabled
        particles

#### Next Week

Review Cycles patches for 2.83. Attempt to complete new volume object
rendering and viewport drawing.

## January 27 - 31

  - New volume object:
      - Initial Cycles, Eevee and viewport rendering
      - Work on completing API and on-demand loading
  - New hair and pointcloud object: initial viewport rendering
  - General code review (about 30 patches)
  - Bugfixes
      - Fix T72143: editing image frame start/offset does not update in
        Eevee
      - Fix tests failing on AMD Ryzen, due TBB initialization order
        issue
      - Fix T73409: error deleting preset saved on different drive than
        Blender install
      - Fix issues in Cycles standalone, particularly related to render
        display

#### Next Week

Design discussions and planning for particle node and new object type
projects. Continue on volume object, likely volume sequence support and
tightening up the depsgraph related code. Review Cycles patches for
2.83.

## January 20 - 24

  - New hair, pointcloud and volume object types: initial data
    structures
  - Tablet events: refactor code to fix various bugs and simplify it
  - Review LANPR design with Sebastian
  - Review undo optimization patch
  - Fix various build and link issues after VFX platform upgrade
  - General triaging and code review

#### Next Week

Rendering support for new volume object. Catch up with patch review.

## January 15 - 17

  - Library upgrades to match VFX reference platform 2020
  - Planning with Dalai for 2020 development projects
  - Create 2.82 workboard, find and tag showstopper tasks
  - Linux: update release notes for appdata
  - Bugfixes:
      - Fix Cycles not correctly using Background.shader if specified
      - Fix T71830: invalid Linux appdata syntax
      - Fix T73129: sculpt mode slow on mesh with fake user
      - Fix T71952, T70125: Blender Linux crash on startup with some
        tablets
      - Fix T68000: load previous settings error if config folder
        already exists
