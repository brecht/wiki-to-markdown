## Nathan Craddock

### Google Summer of Code

  - [Google Summer of Code 2020: Continued Outliner
    Improvements](/GSoC2020)
  - [Google Summer of Code 2019: Outliner
    Improvements](/GSoC2019)

### Other

  - [Development Setup](/Setup): CMake and other config
