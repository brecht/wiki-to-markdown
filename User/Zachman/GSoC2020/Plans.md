## Plans

This can be considered my "updated proposal" where I keep my current
plans for each part of the project.

### Smaller Fixes

  - When adding manual item sorting, drag and drop could be improved:
      - Drag and drop of strips from outliner to sequencer
      - Remove need to hold Shift when parenting
          - One idea: A drop on an object name would do parenting,
            anywhere else could do collection link
          - Perhaps the highlights that indicate what action will be
            performed on a drag+drop could be improved. Color, location,
            etc. could indicate what action rather than relying on a
            keyboard shortcut
      - Drag and drop to reorder constraints and modifiers would be nice
        too
  - Design for collection color tagging
      - This is highly-requested
        [1](https://devtalk.blender.org/t/gsoc-2020-outliner-discussion-and-suggestions/13178/19).
        I think at least some decisions could be made on implementation.
  - Option to create a new collection from the selected elements
    [2](https://devtalk.blender.org/t/gsoc-2020-outliner-discussion-and-suggestions/13178/22)
      - This could be dropdown or simply the default behavior

### Selection Refactor

Outliner selection currently mixes activation, selection, and mode
toggling in ways that it becomes difficult to add new features. For
example, adding a feature to open the properties editor data tab when
clicking the data icon in the outliner would also toggle edit mode which
is not desirable. The selection code needs to be reworked to handle new
cases, and now is a good time to think about the different uses for
selection.

The line between activation and selection is not always clear, and some
data are more "disruptive" to activate. Collections are always set to
active when clicked, but if you wanted to select some collections to
reorder, then the current active collection is changed. Scenes and
cameras can also be set active, but it would be nice to also allow
clicking (to rename, move, etc) without setting active. Although
activation is used internally in the code, we can treat it like a simple
select in some cases, e.g. selecting a material in the outliner makes it
active, but the term "selection" works fine because only one material
can be selected at once.

Activation will fall under two broad categories objects and data.
Objects means Bases (objects, curves, armatures, collection instance,
...), pose bones, edit bones, and sequences while data refers to certain
"disruptive" datablocks like cameras, scenes, and collections.

Selection covers the remaining cases like materials, vertex groups,
grease pencil layers, modifiers, constraints, etc. Selection also refers
to basic selection of Bases, pose bones, edit bones, and sequences.

For now, selection will also handle the mode toggling case, but it will
be a separate function to make it easier to add the mode
toggle/activation column later.

When synced selection was first implemented, it was planned that
outliner selection would be isolated to the outliner when syncing was
disabled. Now that is not the case, because objects can still be
activated when syncing is disabled. Having a toggle for syncing implies
to me that one mode syncs, while another doesn't. I think it would be
best to keep selection isolated when syncing is disabled.

This brings to mind another question when future improvements are made.
Should the selection sync toggle effect the mode toggle column and
properties sync?

### Links

\[1\]
<https://devtalk.blender.org/t/gsoc-2020-outliner-discussion-and-suggestions/13178/19>  
\[2\]
<https://devtalk.blender.org/t/gsoc-2020-outliner-discussion-and-suggestions/13178/22>
