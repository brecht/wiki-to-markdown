## Google Summer of Code 2020

### Continued Outliner Improvements

This will be a continuation of the previous summer’s Outliner
Improvements project. With selection now syncing between the outliner
and other editors, other improvements can be made to the outliner to
increase usability. These include changes to the context menu, a
connection with the properties editor tabs, and increased control over
selection and activation of data types.

  - [Proposal](/Proposal)
  - [Design Task](https://developer.blender.org/T77408)
  - [Final Report](/Report)
