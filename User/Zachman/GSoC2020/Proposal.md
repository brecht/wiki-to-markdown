## GSoC 2020: Continued Outliner Improvements

### Name

Nathan Craddock

### Contact

  - Email: nzcraddock@gmail.com
  - blender.chat: natecraddock
  - Twitter: [@natecraddock](https://twitter.com/natecraddock)
  - Github: <https://github.com/natecraddock>

### Synopsis

This will be a continuation of the previous summer’s Outliner
Improvements project. With selection now syncing between the outliner
and other editors, other improvements can be made to the outliner to
increase usability. These include changes to the context menu, a
connection with the properties editor tabs, manual object sorting, and
increased control over selection and activation of data types.

### Benefits

The outliner provides a central overview of a project in Blender. By
increasing the capability of the outliner, users will be able to more
easily and intuitively manage their scenes. This project also aims to
simplify portions of the outliner code to aid future development.

### Deliverables

I have listed a number of targets here in the hope that I finish the
patches I have already started before the coding period begins. In the
time since last summer, I have worked with William Reynish and others in
defining some of these tasks.

**Decoupling of selection, activation, and interaction mode toggling
(​[D5817](https://developer.blender.org/D5817)​)**  
Selection, activation, and mode toggling are currently tied together
internally. For example, walk navigation currently can only select
because activating would also toggle modes. Decoupling these behaviors
will make the code cleaner and prepare for other changes.

**Connection of outliner datablocks with properties editor tabs
([T63991](https://developer.blender.org/T63991)​)**  
The properties editor updates to show the data from the active object.
This will be extended to the outliner when clicking on datablocks. For
example, selecting a modifier will switch the properties editor to the
modifiers tab for the parent object.

**Better UI/UX for mode toggling and activation
(​[T68498](https://developer.blender.org/T68498)​)**  
To connect the selection armatures, camera data, scenes, and other data
blocks with the properties editor, mode toggling and activation need to
be handled in a different way. Otherwise, selecting camera data would
both set the active camera and switch to the camera data tab in the
properties editor. This will add a new optional column on the left of
the outliner to control mode toggling and activation.

**Manual outliner item sorting
(​[T68502](https://developer.blender.org/T68502)​)**  
Currently only collections can be sorted manually through drag and drop.
This will extend manual sorting to objects.

**Context Menu Improvements**  
The context menu is not always intuitive. This project will make the
context menu more useful by showing menu options based on the
right-click target rather than inferring from the selected types.

**Other**  
Other smaller improvements will likely be included including

  - Drag and drop parenting
  - Create new collection from selected
  - Color tagging design. (Highly requested, a design could be good, but
    might not be necessary)
  - Others?

**Documentation**  
Code will be well documented, and user documentation will be added to
the manual.

### Project Details

This project is composed of smaller tasks that each improve the
outliner’s capabilities and usability. Some of the subprojects are
dependent on others. Some images come from the design tasks linked and
were made by William Reynish. More details can also be found in the
tasks.

As mentioned above, decoupling the selection, activation, and
interaction mode toggling code (selection refactor) will prepare for
other improvements to the outliner, which is why this will be the first
task. Once completed, the connection with the properties editor and mode
toggling UI can be added. The improvements to the context menu also
partially depend on the selection refactor. The outliner item sorting
improvements will be independent from these other changes.

**Connection of outliner datablocks with properties editor tabs
([T63991](https://developer.blender.org/T63991)​)**  
Once datablocks no longer toggle modes or activate in the case of scenes
and cameras, they can be selected to switch to the corresponding tab in
the properties editor. In last year’s summer of code project, this was a
highly requested improvement to the outliner. Additionally, it will add
more useful selection actions to the many types of datablocks that are
drawn in the outliner which currently do nothing or make minimal
changes.

It is possible that this connection could be disabled in each properties
editor, or from the outliner. However, this tab-switching behavior is
intuitive and will likely not need a toggle to disable the connection
between the outliner and properties editors.

**Better UI/UX for mode toggling and activation
([T68498](https://developer.blender.org/T68498))**  
A new column will be drawn on the left side of the outliner. This will
include a toggle in the popover just like other columns drawn on the
right side of the outliner. To allow properties syncing, mode toggling
and some types of activation need to be decoupled from selection. This
new column will draw icons to the left of items to indicate active
state. Icons will also draw next to objects that can be toggled into
edit mode.

For example, clicking in this column next to a camera will make that
camera the active scene camera, with the same behavior working for
scenes. With objects that support edit mode, an icon will be drawn to
switch that object to edit mode. This has the benefit of also allowing
easier inclusion or removal of other objects when in edit mode.

**Manual outliner item sorting
([T68502](https://developer.blender.org/T68502))**  
Sorting of objects is either done alphabetically or in the order of
creation. This will allow the option to sort objects manually like
collections. This was also a highly requested feature from the feedback
topic last summer.

The sorting toggle in the outliner currently only affects objects. This
should rather apply to both objects and collections and be expanded into
an enum to provide more than two options for sorting.

**Context Menu Improvements**  
Currently, the context menu doesn’t always work as expected. For
example, with multiple items selected including objects, a right-click
on a modifier will show a delete menu option; however, this delete is
for objects, not the modifier that was clicked. This is confusing
behavior.

A better solution would be to show options based on the target of the
right-click. In the case of the above example, the delete menu operator
would be shown. Some menu operators could always be shown no matter the
target (like a delete operator for both objects and collections).

Another current issue in the outliner context menu is lack of user
documentation. Many of the menu operators have missing or incomplete
tooltips. These will be improved and added as part of this subtask.

**Other**  
Last year, I found reviewing and committing the entire summer’s work at
once to be a more difficult process. If possible, I would like to commit
the changes for each subtask as it is completed.

### Project Schedule

I will be available to begin on or before the 1st of June, and I expect
the project to take the whole summer, even if I am able to get started
early. In the case that I complete the above targets early, last
summer’s [Devtalk
topic](https://devtalk.blender.org/t/gsoc-2019-outliner-improvements-ideas/7185)
to generate ideas for outliner improvements contains additional ideas
for new features. This schedule outlines a logical ordering of
improvements and approximate time goals.

Weeks 1 - 2: Refactor outliner selection This may be done before the
coding period because this patch is already mostly written. If so, the
timeline will be shifted up leaving more time at the end for sorting
improvements, other features, or extra time as needed.

Weeks 3-4: Mode toggling and activation column The refactor of selection
should make access to activation and mode toggling simpler through more
focused functions. The majority of the work will be drawing the new
column. Week 5: Buffer week and First Evaluation Extra time in case
previous tasks take longer than expected. This can also be used to solve
smaller issues.

Weeks 6-7: Outliner to properties editor tabs connection Functions
already exist in code for each type of data when it is selected. These
need to be connected to the properties editor tabs.

Week 8: Context menu improvements Work on making the context menu act on
the target.

Week 9: More work on the context menu and Second Evaluation Continue
working on the context menu and begin improving tooltips.

Weeks 10-12: Sorting improvements, documentation, and extra time if
needed I consider sorting to be the least urgent of the projects tasks,
so if needed the remainder of the time can be used to finish the
previously listed projects. I will also use this time to write
documentation.

Week 13: Final Evaluation Finalize everything and ensure documentation
is good.

I will be finishing a computer science course for the first two weeks,
but I should still have plenty of available time to work on this
project.

### Bio

I am a junior in the computer science program at Brigham Young
University in Utah, USA, where I also help students understand the
basics of C++ as a teaching assistant. Through classes, Blender
development, and personal projects I have become experienced in C, C++,
Python, and other languages. I have been using Blender for around ten
years now, with the last year focused more on addons and development
than artwork. Outside of computer science I enjoy hiking in the
mountains, and I have recently started learning to play the organ.

Last summer I successfully completed a Google Summer of Code project
improving Blender’s outliner editor. The project sparked additional
ideas for improvements to the outliner, and I have made small commits
through the months since then working toward those changes; however,
school and my part-time job as a teaching assistant have prevented me
from committing large amounts of time to Blender development. While my
current semester is ending, I will start development on the outliner
again through the summer. I would love the opportunity to make these
further improvements to the outliner as part of another Google Summer of
Code project. Among other commits to Blender, here is the diff for my
previous summer of code project. <https://developer.blender.org/D5388>
