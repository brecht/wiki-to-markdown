# Continued Outliner Improvements: Final Report

## Links

  - [Design Task](https://developer.blender.org/T77408)
  - [Development
    Branch](https://developer.blender.org/diffusion/B/history/soc-2020-outliner/)
    (\`soc-2020-outliner\`)

## Summary

Most of the original project goals are completed and are in review. The
status of the project can be seen on the design task linked above. As
the project progressed there were a few features that I had to drop
because of time or the original design wasn't completely perfect. For
example, the original proposal for the Mode Column also included data
activation. The implementation worked great, but it wasn't exactly what
was needed in the outliner. Additionally, my implementation showed me
that the "problem" I was attempting to solve with a data activation
column was not as severe as I originally thought.

## Report

### D5817: Selection cleanup

This was committed at the beginning of the summer. This refactor
simplified the selection operators in the outliner to make it easier to
add new features.

### D8317: Row Icons in menus

Still under review - this adds support to draw in-line icon buttons in
menus. Required for collections color tagging. If needed, it could be
postponed, and the color tagging could be done as a submenu.

### D8622: Collections color tagging

Adds support for adding color tagging to collections. This is the first
step, later improvements could be a viewport overlay (which I have
started on). This wasn't a part of the original proposal.

### D8638: Outliner -\> properties editor sync

Clicking on outliner elements will open the related tab in the
properties editor. The code is fine, but the design is going through a
few small modifications. One idea in development is to only sync with
properties editors that share an edge.

### D8641: Outliner mode toggling column

With properties sync now using the select action for mesh and armature
datablocks, we need a new method of mode toggling within the outliner.
This moves all mode toggling to a single column in the left of the
outliner. The View Layer display mode already has a column, and the
scenes view mode has a column added in.

One additional benefit of this is that we can toggle interaction modes
for modes other than edit and pose.

This has passed initial review and should be committed soon.

### D8642: Data stack drag and drop operator

Partly committed. Adds grease pencil modifiers and shader effects to the
tree.

The second part allows drag and drop on these datablocks to reorder,
copy, and link.

### D8646: Outliner manual object sorting

This feature works well, but isn't as mature as I would have liked. I'm
on the fence about if it should be committed, or redone in a more
reliable way. The downsides is that it doesn't support reordering for
bones, or child objects.

### D8647: Use right click target for context menu

This hasn't been reviewed yet. The context menu tries to intelligently
show options based on the selection, but it's more intuitive to show
based on the target data of the right click.

### D8648: Create new collection with selected objects

This has been mostly reviewed and we may make a few slight changes. The
idea is to default to moving the selected objects into a new collection
from the outliner.

### D8650: Various small fixes

These has been committed.

  - Using left and right arrows for walk navigation
  - Use Shift for bone recursive icon button toggling rather than Ctrl
    for consistency.

Overall, nearly everything on my original proposal is awaiting review,
and there are patches that were not on my proposal. Only manual object
sorting may not be included.
