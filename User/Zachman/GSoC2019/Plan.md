## Project Plan

This page will be frequently updated as I work throughout the summer.
This page contains a schedule and list of all ideas from [the Outliner
Ideas](../OutlinerIdeas) page that I feel I can implement
during the coding period this summer. The descriptions listed here are
more general, a detailed design document can be found
[here](Design/index.md).

##### Outliner Syncing

The major task for the summer of code is synced selection between the
outliner and other editors. This will include a toggle to enable/disable
the synced selection features per-outliner.

The easiest way to implement synced selection will be to call a function
in 3D View selection operators that sends the resulting selection state
to the outliner. And the outliner sends the state to the 3D view after
outliner selection operations. Once 3D viewport syncing is finished,
other editors may be added to the "connected" editors list. The
properties editor is one that would benefit. Clicking on modifiers,
material data, etc in the outliner currently does nothing. This could
either select the object it is a child of, or open that data in the
properties editor.

Editor syncing priorities

  - Viewport selection (3D View)
  - Properties
  - Other Editors

##### Interaction/Operators

  - <s>Shift+click range select</s>
  - <s>Ctrl+click extend selection</s>
  - <s>Click and drag for box select</s>
  - <s>Arrow key navigation (up/down for walk, right/left for open/close
    disclosure triangles)</s>
  - X for delete should work properly for all data types
  - Multi-object selections:
      - <s>Drag and drop parenting should work</s>
  - Fix and add other shortcut keys
  - <s>Modifier keys for Box, Range, Walk</s>

##### UI

  - Simplify highlights and selected element indication
      - <s>Active object highlighting</s>
      - <s>Blue-line selected object highlights</s>
      - Drag and drop highlighing
      - "Active Collection" highlighting (collection in which active
        object is in (or all selected objects)
  - Clean up context menu
  - <s>Add new object and bone constraint icons to outliner</s>
  - <s>Add toggles for enabling constraints from outliner</s>

##### Other

  - Make collection new objects are added to more obvious
      - This could be done by adding new objects to the collection in
        which the active object is in.
  - <s>Support eyedropper tool in outliner</s>

## Schedule

As mentioned in my proposal I have a CS course during the first three
weeks of the summer of code. I will do my best to follow this schedule,
but I have left the last few weeks with less detailed planning to allow
for flexibility. After that class ends I will be able to dedicate my
time to this project completely.

##### **Week 1** *May 27 – June 2*

  - Look at [Abid's work](https://developer.blender.org/D4849) on the
    outliner and implement and improve on his work in my branch
      - Active elements
      - Box selection
      - Range selection
      - Arrow key navigation
  - Determine simplified UI (highlights, active item)
      - Begin implementing this as part of integrating Abid's work.
  - Detailed planning for synced selection implementation ([look
    here](https://developer.blender.org/T63988))

##### **Week 2** *June 3 – June 9*

  - Continue implementing Abid's work
  - Begin implementing synced selection with 3D View

##### **Week 3** *June 10 – June 16*

  - Finish implementing features from Abid's patches
  - Modifier keys for selection
  - Synced selection

##### **Week 4** *June 17 – June 23*

*My CS course ends (17th). I will have one final exam this week (19th).*

  - Determine how to implement active outliner elements
      - This is part of synced selection. How could an active collection
        be handled? Collections cannot be active in the viewport,
        syncing?
  - Synced selection

##### **Week 5** *June 24 – June 30*

*Evaluation 1*

  - Prevent adding new objects to hidden collections
      - Perhaps by making the active object's collection the collection
        to add objects to by default.
  - Synced selection

##### **Week 6** *July 1 – July 7*

  - Finish synced selection for 3D View
      - Begin connecting outliner to other editors
  - X for delete should work properly for all data types
  - Fix and add other shortcut keys

##### **Week 7** *July 8 – July 14*

  - Connect outliner to properties editor
  - Drag on disclosure triangles [https://developer.blender.org/T57746
    link](https://developer.blender.org/T57746_link)

##### **Week 8** *July 15 – July 21*

  - Work more on outliner and properties editor syncing
  - Drag and drop parenting with multiple selected objects

##### **Week 9** *July 22 – July 28*

*Evaluation 2*

  - Support eyedropper tool in outliner

##### **Week 10** *July 29 – August 4*

  - Work on community ideas

##### **Week 11** *August 5 – August 11*

  - Begin work on documentation in the Blender manual
  - Work on community ideas

##### **Week 12** *August 12 – August 18*

  - Extra time for previous goals in case of delay
  - Extra projects

##### **Week 13** *August 19*

*Last day\! Make sure everything is done before final evaluations.*
