## Google Summer of Code 2019

### Outliner Improvements

With an increased focus on the outliner in Blender 2.8x, many
improvements could be implemented to increase the outliner's usability.
Synced selection between the outliner and other editors, arrow key
navigation, range selection, and other standard interactions will be
implemented to make the outliner more intuitive. Other various UI
tweaks, operator improvements, and menu organization will also be added
in this project.

### Final Report

  - [Final Report](/Report)

### Links

##### Wiki

  - [Proposal](/Proposal)
  - [Project Plan](/Plan)
  - [Design Document](/Design)
  - [Community Suggestions](/OutlinerIdeas)

##### Devtalk

  - [Suggestions
    thread](https://devtalk.blender.org/t/outliner-improvements-ideas/7185)
  - [Branch testing
    thread](https://devtalk.blender.org/t/gsoc-2019-outliner-branch-testing/7616/21)
  - [Weekly
    reports](https://devtalk.blender.org/t/gsoc-2019-outliner-weekly-reports/7642)
