## Collections

Currently collections could be handled a bit better in the outliner. For
instance:

It isn't always obvious which collection an object will be added to
(active collection is hidden)

Many people use collections to organize the outliner like:

    Scene Collection
      Lights
      Cameras
      Meshes

Which makes it difficult to determine which collection the active object
is in without expanding the collections (potentially layers deep) to
find the element.

Proposed solutions

  - Highlight the collection the active element is in. This could either
    highlight all parent collections, or would highlight the first
    visible parent collection of the active element.
  - Always make collection of the active element the active collection.
  - Prevent adding objects to hidden collections. Currently excluding a
    collection switches the active collection, this could be extended to
    the other visibility controls.
