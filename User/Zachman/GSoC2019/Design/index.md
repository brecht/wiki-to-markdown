### Synced Selection Design

Outliner synced selection will be a per-outliner toggle. When synced
selection is off, the outliner will continue to function as they
currently do in Blender 2.80, with the exception of not activating
elements. Selection will be isolated to the outliner. When synced
selection is enabled, the outliners, view layer, and sequencer will be
kept in harmony; their selection states will be equivalent between them.

In order to determine if an outliner is out of sync, a new \`dirty\`
flag will be introduced for each outliner. This will be set to false
initially, then when each outliner draws it syncs from the active view
layer. This will be true for the initial UI, and for switching
workspaces, opening new outliners, etc. This way each outliner will
indicate the current selected and active state in the viewport. Because
there are many outliners in a given Blender instance, hidden outliners
will be lazy updated when they are drawn. This will occur when changing
workspaces for example.

Syncing will go in two directions, (1) from 3D View to outliners, and
(2) from outliner to other outliners and 3D Views.

For the first direction, each outliner's \`dirty\` flag will be set.
Then the selection state in the current active view layer will be sent
to each outliner. The elements will be compared, and any item in the
outliner that is active and/or selected in the 3D view will have the
same flags set in the outliner. This could be done per-outliner, or it
could be done on one, and then the outliner function to sync with other
dirty outliners could be used.

For going in the other direction, a selection operator in the outliner
will occur. This will be for either single element activate, range
select, box select, or walk select. Before any of these operators finish
execution, they will flag all other outliners in the blend as \`dirty\`
if not already dirty. Then the current state of the active outliner will
be flushed to each visible outliner and to the 3D view.

##### Outliner to Outliner

When each outliner receives the state of the non-dirty outliner, it will
read through the tree and set it's own tree to match any selected and
active elements. This will be done by comparing the trees and copying
over the flag data for active and selected elements. (I will have to
verify this, but I believe the treestore of an outliner may be an
efficient way to determine the state of any given element. I'm not sure
how connected the treestores between two different outliners are.) After
the sync, the synced outliners will have the \`dirty\` flag reset.

For example, Outliner A has a box select that selects the Camera, Cube,
and Light objects. At the end of operator execution the other outliners
are marked as \`dirty\` and then visible outliners are sent the state of
the selection. Say outliner B is the only other visible outliner. It
receives the tree from Outliner A and then copies all the selected and
active flag data to it's own tree. This could be done by iterating over
each tree \`O(A X B)\` or by using the treestore (if possible, as
mentioned above). This is done and then the area is redrawn.

##### Outliner to 3D View

A similar process to syncing outliners will take place, however, it will
only have to occur one time for the active view layer. Each object in
the view layer will be set to selected and/or active depending on the
current selection state in the tree.

#### Other thoughts

Depending on how complex the syncing function actually is, the non-dirty
outliner could send only the selected elements in a flat list to other
outliners to reduce looping iterations.

Some operations will require a full selection sync. These include:

  - Opening a file with Load UI disabled
  - Switching the active scene
  - Undo
  - Others...
