## Outliner Ideas

This page is a collection of suggestions for improving the outliner
compiled from
[Devtalk](https://devtalk.blender.org/t/outliner-improvements-ideas) and
[Twitter](https://twitter.com/natecraddock/status/1127984762423955456).
The purpose of this list is to help me keep ideas organized, and to
provide a single source for all suggested improvements. I have linked
and categorized every suggestion I have received and thought of. I do
not anticipate all of these ideas ever being implemented, especially
over the summer. However, I do think it is good to keep this list, ideas
that never find a place may still lead to other great ideas.

### Synced Selection

-----

Synced selection between the outliner and other editors will improve
workflow. The idea is to have a per-outliner toggle for synced
selection.

Synced selection will be very similar throughout most editors, some
won't even sync. The Properties Editor will be the most unique and
require the most thought for defaults.

##### 3D View / Editors

Synced selection will keep the outliner and other editors in harmony;
what is selected in one will always be selected in the other. Any
operators and tools that modify the selection in an editor (like select
all, box select, and add object/sequence) will reflect that new
selection in the outliner.

##### Other

  - Opening datablocks in other editors (Clicking material data would
    open that material in any open shader editors)
    [https://devtalk.blender.org/t/outliner-improvements-ideas/7185/12
    link](https://devtalk.blender.org/t/outliner-improvements-ideas/7185/12_link)
  - Using the outliner filter to filter visible objects in the 3D view
    as well
    [https://devtalk.blender.org/t/outliner-improvements-ideas/7185/18
    link](https://devtalk.blender.org/t/outliner-improvements-ideas/7185/18_link)
  - Highlight searched items in viewport (like in
    [Unity](https://www.dropbox.com/s/n7cshi4b2nn8bd5/Unity-search.mov?dl=0))
  - Autofocus in outliner of selected item in 3D view (autoscroll)

##### Properties

Synced selection with the properties editor is a newer idea to me. It
will require a much more refined plan for behavior. But in general it
would reflect the currently selected datablock in the outliner in the
properties editor. Clicking a modifier, for example, would open the
modifiers tab on that object. See this
[https://devtalk.blender.org/t/outliner-improvements-ideas/7185/2
link](https://devtalk.blender.org/t/outliner-improvements-ideas/7185/2_link)
for more details.

### Interaction

-----

##### Drag and Drop

  - Parenting should work for all selected/dragged objects
    [https://devtalk.blender.org/t/outliner-improvements-ideas/7185/27
    link](https://devtalk.blender.org/t/outliner-improvements-ideas/7185/27_link)
  - Automatic scrolling when dragging and dropping items in a long
    outliner list
    [https://devtalk.blender.org/t/automatic-scrolling-when-dragging-objects-in-outliner/7702
    link](https://devtalk.blender.org/t/automatic-scrolling-when-dragging-objects-in-outliner/7702_link)
  - Reorder constraints and modifiers
    [https://devtalk.blender.org/t/gsoc-2019-outliner-improvements-ideas/7185/146
    link](https://devtalk.blender.org/t/gsoc-2019-outliner-improvements-ideas/7185/146_link)
  - Add new collection should take selection into account
    [https://devtalk.blender.org/t/gsoc-2019-outliner-improvements-ideas/7185/268?u=natecraddock
    link](https://devtalk.blender.org/t/gsoc-2019-outliner-improvements-ideas/7185/268?u=natecraddock_link)

##### Selection

  - Click+Drag for box select
  - Shift+click range selection
  - Arrow Key Navigation (up/down for walk, right/left for open/close
    disclosure triangles)
      - Arrow key navigation from 3D view
        [https://devtalk.blender.org/t/outliner-improvements-ideas/7185/26
        link](https://devtalk.blender.org/t/outliner-improvements-ideas/7185/26_link)
  - Ctrl/Shift click on visibility for object hierarchies should
    isolate/recursive hide (not just collections)
    [https://devtalk.blender.org/t/outliner-improvements-ideas/7185/19
    link](https://devtalk.blender.org/t/outliner-improvements-ideas/7185/19_link)
  - Select elements inside collection
    [https://devtalk.blender.org/t/outliner-improvements-ideas/7185/46?
    link](https://devtalk.blender.org/t/outliner-improvements-ideas/7185/46?_link)
    [https://devtalk.blender.org/t/outliner-improvements-ideas/7185/69
    link](https://devtalk.blender.org/t/outliner-improvements-ideas/7185/69_link)
  - Double click (somewhere other than name) to select collection
    hierarchy
    [https://devtalk.blender.org/t/gsoc-2019-outliner-improvements-ideas/7185/123
    link](https://devtalk.blender.org/t/gsoc-2019-outliner-improvements-ideas/7185/123_link)
  - Click on unfolded child icons to interact
    [https://devtalk.blender.org/t/gsoc-2019-outliner-improvements-ideas/7185/133?u=natecraddock
    link](https://devtalk.blender.org/t/gsoc-2019-outliner-improvements-ideas/7185/133?u=natecraddock_link)

##### Other

  - Make the restriction toggles operate on all selected items
    [https://devtalk.blender.org/t/outliner-improvements-ideas/7185/22
    link](https://devtalk.blender.org/t/outliner-improvements-ideas/7185/22_link)
  - Bulk rename
    [https://devtalk.blender.org/t/outliner-improvements-ideas/7185/14
    link](https://devtalk.blender.org/t/outliner-improvements-ideas/7185/14_link)
  - Duplicate data (like objects) from within the outliner
    [https://devtalk.blender.org/t/outliner-improvements-ideas/7185/14
    link](https://devtalk.blender.org/t/outliner-improvements-ideas/7185/14_link)
  - Restriction column lock (can be achieved by hiding it?)
    [https://devtalk.blender.org/t/outliner-improvements-ideas/7185/33
    link](https://devtalk.blender.org/t/outliner-improvements-ideas/7185/33_link)
  - Multiple selection should operate on all selected outliner elements
    [https://devtalk.blender.org/t/outliner-improvements-ideas/7185/48
    link](https://devtalk.blender.org/t/outliner-improvements-ideas/7185/48_link)
  - Open/Close all collections/data operator
    [https://devtalk.blender.org/t/outliner-improvements-ideas/7185/61
    link](https://devtalk.blender.org/t/outliner-improvements-ideas/7185/61_link)
  - Dragging over disclosure triangles to open/close
    [https://developer.blender.org/T57746
    T57746](https://developer.blender.org/T57746_T57746)
  - X to delete should work properly
    [https://devtalk.blender.org/t/outliner-improvements-ideas/7185/67
    link](https://devtalk.blender.org/t/outliner-improvements-ideas/7185/67_link)
  - Set active camera from outliner
    [https://devtalk.blender.org/t/gsoc-2019-outliner-improvements-ideas/7185/120?u=natecraddock
    link](https://devtalk.blender.org/t/gsoc-2019-outliner-improvements-ideas/7185/120?u=natecraddock_link)
      - Switch to camera view when setting active camera
        [https://devtalk.blender.org/t/gsoc-2019-outliner-branch-testing/7616/86
        link](https://devtalk.blender.org/t/gsoc-2019-outliner-branch-testing/7616/86_link)
  - Add constraint toggles for viewport and render
    [https://devtalk.blender.org/t/gsoc-2019-outliner-improvements-ideas/7185/146
    link](https://devtalk.blender.org/t/gsoc-2019-outliner-improvements-ideas/7185/146_link)
  - Open linked .blend file data from outliner
  - Ctrl+click on disclosure triangle to close others and open current
    element subtree.

### UI

-----

  - Adjust row spacing
    [https://devtalk.blender.org/t/outliner-improvements-ideas/7185/3
    link](https://devtalk.blender.org/t/outliner-improvements-ideas/7185/3_link)
  - Per-collection color
    [https://devtalk.blender.org/t/outliner-improvements-ideas/7185/25
    link](https://devtalk.blender.org/t/outliner-improvements-ideas/7185/25_link)
  - Always visible scrollbar
    [https://devtalk.blender.org/t/outliner-improvements-ideas/7185/40
    link](https://devtalk.blender.org/t/outliner-improvements-ideas/7185/40_link)
  - Add icons for mesh object types and constraints
    [https://devtalk.blender.org/t/gsoc-2019-outliner-improvements-ideas/7185/146
    link](https://devtalk.blender.org/t/gsoc-2019-outliner-improvements-ideas/7185/146_link)

##### Highlights

  - Active object highlighting
    [https://devtalk.blender.org/t/outliner-improvements-ideas/7185/7
    link](https://devtalk.blender.org/t/outliner-improvements-ideas/7185/7_link)
  - Better selected object highlighting
    [https://devtalk.blender.org/t/outliner-improvements-ideas/7185/30
    link](https://devtalk.blender.org/t/outliner-improvements-ideas/7185/30_link)
  - Active collection highlighting

### Other

-----

  - Prevent adding objects to hidden collections
    [https://devtalk.blender.org/t/outliner-improvements-ideas/7185/16
    link](https://devtalk.blender.org/t/outliner-improvements-ideas/7185/16_link).
    Perhaps by making the active object's collection the collection to
    add objects to by default.
  - Show collections contents on Scenes view
    [https://devtalk.blender.org/t/outliner-improvements-ideas/7185/20
    link](https://devtalk.blender.org/t/outliner-improvements-ideas/7185/20_link)
  - Show stats on objects (Like polys, tris, etc. interesting concept,
    could get cluttered)
    [https://devtalk.blender.org/t/outliner-improvements-ideas/7185/21
    link](https://devtalk.blender.org/t/outliner-improvements-ideas/7185/21_link)
  - Collection numbering system
    [https://devtalk.blender.org/t/outliner-improvements-ideas/7185/23
    link](https://devtalk.blender.org/t/outliner-improvements-ideas/7185/23_link)
  - Unify orphan data view with normal view (for objects at least)
    [https://devtalk.blender.org/t/outliner-improvements-ideas/7185/24
    link](https://devtalk.blender.org/t/outliner-improvements-ideas/7185/24_link)
    [https://devtalk.blender.org/t/outliner-improvements-ideas/7185/45
    link](https://devtalk.blender.org/t/outliner-improvements-ideas/7185/45_link)
  - Arbitrary outliner item ordering
    [https://devtalk.blender.org/t/outliner-improvements-ideas/7185/29
    link](https://devtalk.blender.org/t/outliner-improvements-ideas/7185/29_link)
      - We would keep alphabetical, and order of creation sorting as
        well.
        [https://blender.chat/channel/blender-coders?msg=qz8DmNvEW75eNzXkW
        link](https://blender.chat/channel/blender-coders?msg=qz8DmNvEW75eNzXkW_link)
  - Make collection from current selection
    [https://devtalk.blender.org/t/outliner-improvements-ideas/7185/34
    link](https://devtalk.blender.org/t/outliner-improvements-ideas/7185/34_link)
  - Pick outliner objects with eyedropper tool
    [https://devtalk.blender.org/t/outliner-improvements-ideas/7185/75
    link](https://devtalk.blender.org/t/outliner-improvements-ideas/7185/75_link)
  - A data editing/outliner workspace?
    [https://devtalk.blender.org/t/gsoc-2019-outliner-improvements-ideas/7185/96?u=natecraddock
    link](https://devtalk.blender.org/t/gsoc-2019-outliner-improvements-ideas/7185/96?u=natecraddock_link)
  - Add filter for invisible objects dropdown
    [https://devtalk.blender.org/t/gsoc-2019-outliner-improvements-ideas/7185/137?u=natecraddock
    link](https://devtalk.blender.org/t/gsoc-2019-outliner-improvements-ideas/7185/137?u=natecraddock_link)

### Issues

  - Toggling collections with subcollection enables disabled
    subcollections
    [https://devtalk.blender.org/t/gsoc-2019-outliner-improvements-ideas/7185/105?u=natecraddock
    link](https://devtalk.blender.org/t/gsoc-2019-outliner-improvements-ideas/7185/105?u=natecraddock_link)
  - Look into expand/collapse children under a node
    [https://devtalk.blender.org/t/gsoc-2019-outliner-improvements-ideas/7185/84?u=natecraddock
    link](https://devtalk.blender.org/t/gsoc-2019-outliner-improvements-ideas/7185/84?u=natecraddock_link)
