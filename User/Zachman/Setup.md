## Development Setup

### CMake

  - Everything from the [Developer](https://developer.blender.org/D5149)
    config
  - Ninja (faster than Make and less output)
  - \`CMAKE\_CXX\_COMPILER\_LAUNCHER\`,
    \`CMAKE\_C\_COMPILER\_LAUNCHER\`: \`ccache\`
  - \`CMAKE\_CXX\_FLAGS\`, \`CMAKE\_C\_FLAGS\`:
    \`-fdiagnostics-color=always\` Color ninja output
  - \`WITH\_LINKER\_LLD\` (faster than WITH\_LINKER\_GOLD)

### Environment Variables

  - \`NINJA\_STATUS="\[%f/%t %p %e\]"\` Set the ninja status line to
    display targets, percent completion, and elapsed time.
  - \`ASAN\_OPTIONS=abort\_on\_error=1\` Cause GDB to stop when ASAN
    finds an error.
  - LSAN\_OPTIONS="print\_suppressions=false:suppressions=$HOME/path/to/suppressions.txt"
      - [LSAN
        Suppressions](../../Tools/Debugging/ASAN_Address_Sanitizer.md)
        list.
      - Also \`leak:libpulse\*\`, \`leak:libxcb\*\`,
        \`leak:iris\_dri\*\`
