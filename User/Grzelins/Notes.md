# Notes

## Tasks that I may do in free time

  - support drag and drop log (or any text in info editor) -\> fancy
  - pick shortest path -\> convert to active tool
      - tool is not exposed in menu
      - top bar is not used for the tool
  - Menu in edit mode: mesh -\> Extrude disappears instead of being grey
    (edge select)
  - menu in edit mode \`ctrl E\` and context menu is different - is it
    intended?
  - about page is reseting to center position while moving floating
    window
  - when select tool is expanded online manual button does nothing
  - the circle select is still drawn in header top bar
  - when use nodes is not selected - make interface grayed out
  - show UI depracation warnings:
      - wen we want to change UI, for next release annotate it with
        different color?

## Some useful loggers

  - \`wm.session\` - some basic read preference, and quit messages
  - \`wm.operators\` - show unregistered operators
  - \`bke.reports\` - show reports as logs with report type converted to
    severity
  - \`blenloader.versioning\` - some messages here are critical for data
    integrity
  - \`blenloader.readfile\` \`blenloader.writefile\` - do I need to
    explain?
