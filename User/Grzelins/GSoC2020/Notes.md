# Drawing text areas in blender

## Console and info editor

Drawing is separated into \`TextViewContext\`. It can be used to display
text in row by row basis. Supports very basic coloring and icons. Holds
callbacks to drawing functions, iterator etc.

## Text editor

  - Basic syntax highlighting in \`format\_draw\_color\`. It is
    implmented as 2 strings: input: \`import sys\` and format:
    \`bbbbbb\_qqq\` (FMT\_TYPE\_KEYWORD = 'b' and so on)
  - although there is helper struct \`TextDrawContext\` the majoriy of
    drawing logic is married to \`SpaceText\`. On the other hand text
    itself is stored in struct \`Text\` which owns list of \`TextLine\`
    (which has text itself and format)
  - <https://blender.community/c/rightclickselect/fzgbbc/>

## Outliner

\`TreeViewContext\` and more, seems complicated.

## File select

This editor is interesting because this is the only place in blender
which uses table view.
