## Google Summer of Code 2020

### Info Editor Improvements

Work on developer oriented features regarding log system.

  - [Main page for all GSoC 2020
    projects](https://wiki.blender.org/wiki/GSoC/2020)
  - [ Initial Proposal](/Proposal)
  - [Project Details](/ProjectDetails) - my personal study
    about logging in blender
  - [Notes](/Notes) - personal list of notes
  - [Final Report](/Report)

External links:

  - [Initial devtalk
    thread](https://devtalk.blender.org/t/gsoc-2020-draft-info-editor-improvements/12317)
  - [Feedback (discussion and suggestions) devtalk
    thread](https://devtalk.blender.org/t/gsoc-2020-info-editor-improvements-discussion-and-suggestions/13718)
  - [Weekly reports devtalk
    thread](https://devtalk.blender.org/t/gsoc-2020-info-editor-improvements-weekly-reports/13659)
  - [My
    branch](https://developer.blender.org/diffusion/B/history/soc-2020-info-editor/)
    named \`soc-2020-info-editor\`
  - [Daily builds](https://blender.community/c/graphicall/Zmbbbc/)
  - [Parent task T78164](https://developer.blender.org/T78164)
