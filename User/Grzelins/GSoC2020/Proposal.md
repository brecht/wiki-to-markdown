## Google Summer of Code 2020: Info Editor Improvements (initial proposal)

This page is copied from original proposal and will not be edited. For
updated design see [ Project Details
page](ProjectDetails.md)

### Name

Mateusz Grzeliński

### Contact

  - Email: grzelinskimat@gmail.com
  - blender.chat: @grzelins
  - <https://developer.blender.org/p/grzelins/>
  - Github: <https://github.com/Mateusz-Grzelinski>

### Synopsis

Show logs and debugs in info editor. Add UI features to make it
comfortable to work with logs such as filtering, searching,
enable/disable on the fly.

### Benefits

This is developer oriented feature. Currently logs and debugs can be
viewed in console. Main motivation is to create UI which allows for
easier search in logs. Some UI improvements have been proposed in
[\#68448](http://developer.blender.org/T68448) and are partially
implemented, but I would like to take more technical approach to issue.

### Deliverables

Main features are:

  - show log info in Info Editor
  - show debug info in Info Editor
  - create appropriate UI for 2 above: filtering and searching,
    enable/disable logs from UI, add icons show what kind of log levels
    are available
  - import/export logs, mainly to conveniently search user provided logs

These features would lay foundation for no-console log. After this is
done we can go fancy like real time view, plotting value from logs and
so on.

Additional features to consider, nice to have or nice to start with:

  - make report and log system into one?
  - industry compat keymap: change selection keymap to match that from
    outliner - currently info editor follows old blender convention
  - display blender commit and version info in menu (like blender -v,
    but available from UI)
  - report log size (memory footprint), set maximum log size, delete
    logs if above maximum size
  - UI: wrap multiple occurences of the same log to one record
  - navigate from log to source code
  - address missing logging information mentioned in [this video
    (?)](https://www.youtube.com/watch?v=ugVNYwTM_Rk)

### Project Details

Current status: blender has 3 ways of printing information internally:
as debug, as log or as report.

#### Logs

Log is fine detail info visible only in console, to change log settings
you need to start blender with appropriate \`--log\*\` flag. Many files
creates logger locally ex. \`static CLG\_LogRef LOG = {"ed.undo"};\`.
Simplifying, there is one logger type wrapped in several functions and
macros which ends with \`puts\` function. Logging function takes log as
string - this string is already formatted.

Logging macros (CLOG) enforce text structure:

<LEVEL>` (`<LOG_ID>`): `<SOURCE_FILE>`:`<LINE_NUMBER>` `<FUNCTION_NAME>`: `<CUSTOM_MESSAGE>

Example:

`INFO (wm.msgbus.sub): /home/mat/blender-sources/blender-src/source/blender/windowmanager/message_bus/intern/wm_message_bus_rna.c:292 WM_msg_subscribe_rna_params: rna(id='`<none>`', Object.mode, info='view3d_main_region_message_subscribe')`

Because of this defined structure and centralized control in macros it
will be easier to change.

UI will be a challenge as logs are typical table like data and blender
does not have this concept and introducing completely new concept might
be not worth the hassle. There are also external log viewers, so
advanced UI for logs is not a priority.

There are around 420 \`CLOG\` entries in code (\`grep -r CLOG | wc
-l\`).

Conclusion: it makes sense to make every CLOG visible in Info Editor.

#### Debugs

Debugs are flag based. They are stored in \`Global.debug\`. The use of
debug is complicated in blender:

  - sometimes it calls log: \`if (G.debug & G\_DEBUG) then CLOG\`
  - sometimes it calls to stdout/err directly \`if (G.debug & G\_DEBUG)
    fprintf(stderr, ...); fflush(stderr);\`
  - some libraries have its own loggers and issue must be looked one by
    one. Ex. FFMPEG has \`av\_log\_default\_callback\`
  - depsgraph has dedicated module for debugging
  - some modules can be build separately (like cycles) - investigate if
    any conflicts will occur
  - for reference: there are 821 uses of std (\`grep -r 'fprintf(std' |
    wc -l\`) and 5554 uses of print itself (\`grep -r "printf(" | wc
    -l\`). Of course some of them are legitimate uses

In majority of cases prints should be simply replaced by logs, what
solves the issue of printing debug information in Info Editor. See for
example:

`if (G.debug & G_DEBUG) {`  
`    printf("%s: something wrong in areafullscreen\n", __func__);  `  
`}`

Developer wanted to know where is this called from - it is basically
log.

Conclusion: convert every debug print to CLOG. There are only few cases
there it can not be applied and it needs to studied further (like
external libraries). Note: depsgraph also uses print.

### Project Schedule

#### May 4 - June 1 : community bonding period

  - getting familiar with prior work regarding info editor, like
    [rBS145f851](https://projects.blender.org/blender/blender-staging/commit/145f851731655910f653763f4122288bc12c5629),
    [rBSaa919f3](https://projects.blender.org/blender/blender-staging/commit/aa919f3e82020a4d9e953ec826a390bf2837cadc),
    [D5468](http://developer.blender.org/D5468),
    [D6491](http://developer.blender.org/D6491),
    [D7102](http://developer.blender.org/D7102) and so on
  - Look at existing implementation of search functionality in blender
  - work on design: if multiple log types is the way to go, if
    centralized formatting in Info Editor is good idea, if reports logs
    and debugs should be converted into one system etc.

#### June 1 - June 29 - First evaluations

I do not want to promise too many features in this period as this is the
end of term and start of exams.

  - Industry compat keymap: change selection pattern in info editor to
    be consistent with outliner - simple task to get started
  - make log display in Info Editor

#### June 29 - July 27 - Second evaluations

  - add text based search
  - log to filters search
  - UI: show current level of log
  - UI: show current level of debugs
  - dynamically disable/enable logs, debugs
  - import/export logs

#### July 27 - August 31 - submit code and evaluations

  - print debugs in Info Editor

### Bio

I am master student from AGH university in Kraków, Poland. I study
Computer Science, System Modeling and Data Analysis (master). I am 21.

I did not submit many patches to blender itself, but I have been
following development of blender for 6 years doing both: a little bit of
artistic, a little bit of coding. I have moderate experience in C from
my study classes and first work (first work was in embedded c
programming), basic experience in C++ (mainly from studies) and strong
experience in python from both my second job and by bachelor thesis.
Also I use linux (and zsh, SpaceVim) as my daily driver.
