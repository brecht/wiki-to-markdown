# Info Editor Improvements Final Report

The main goal in my GSoC was:

  - to implement log viewer
  - side goal was to convert print to log, as many as I could
  - create python API for Blender log system and finally
  - expose some log/debug options in preferences.

I managed to create at least prototypes for all of my goal, they are
currently under committed to review. During development I noticed place
for minor improvements, for example that by introducing logs deprecates
some parts of Report system (report is user oriented message), I dealt
with it on the fly. As most of the features are under review, the best
list of changes is in this code diff:
<https://developer.blender.org/D8677>

### Links

  - [Initial
    Proposal](https://wiki.blender.org/wiki/User:Grzelins/GSoC2020/Proposal),
    [Initial devtalk
    thread](https://devtalk.blender.org/t/gsoc-2020-draft-info-editor-improvements/12317)
  - [Weekly reports devtalk
    thread](https://devtalk.blender.org/t/gsoc-2020-info-editor-improvements-weekly-reports/13659),
    [Feedback (discussion and suggestions) devtalk
    thread](https://devtalk.blender.org/t/gsoc-2020-info-editor-improvements-discussion-and-suggestions/13718)
  - [My
    branch](https://developer.blender.org/diffusion/B/history/soc-2020-info-editor/)
    named \`soc-2020-info-editor\`
  - All the code reviews and designs are linked to [parent task
    T78164](https://developer.blender.org/T78164), here are some direct
    links to code:
      - <https://developer.blender.org/D8677> - Add core functionalities
        from GSoC Info Editor Improvements - under review
      - <https://developer.blender.org/D8655> - Add log based timing
        functions - depends on D8677 - under review
      - <https://developer.blender.org/D8691> - Convert printfs to CLOG
        - depends on D8677 - under review

## Details

Blender log system is simple system (simpler than \`glog\`) what is not
enough for complex 3d application. Blender logging system would benefit
from improvements like ability to contextualize logs per render,
graphical representation of blender graph structures (dependency graph),
but it was not the focus of this project. In this project I got familiar
with many log styles, created log viewer and converted many files to use
blender's clog.

Blender is created by many individuals who have their favorite log
method, most likely \`printf\` with some file-wide macro. As I was going
through a lot of code I created [best practices for logging
proposal](https://devtalk.blender.org/t/gsoc-2020-info-editor-improvements-discussion-and-suggestions-logs-reports-friends/13718/6)
which summed up common log patterns for further discussion.

With growing use of logs, I wanted to make it easier to access them. I
created interface for log and debug settings in user preferences.
Unfortunately in early design I missed 3 important flaws of this design:

1.  preferences are read almost at the end of blender initialization,
    what means log and debug settings are applied really late
2.  debug flags often trigger special mechanism (ex. turn on memory
    debug) which must be applied early in startup
3.  there is conflict with command line argument

I managed to solve 3-rd problem, but not 1 and 2.

Blender UI is written in python which also makes extensive use of
prints. Printing in python files is particularly tricky to track down as
debugger will not stop in python files. That is why I designed python
API for clog ([T80072](https://developer.blender.org/T80072)) and
created prototype [D8688](https://developer.blender.org/D8688).
