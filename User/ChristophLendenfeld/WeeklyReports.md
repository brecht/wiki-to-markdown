## December 12 - 15

New

  - [PR
    \#116100](https://projects.blender.org/blender/blender/pulls/116100)
    Refactor: Remove dependencies on Editor code from animrig (already
    landed)

Updated

  - [PR
    \#115522](https://projects.blender.org/blender/blender/pulls/115522)
    Anim: Don't use keying sets when inserting keyframes during
    autokeying
  - [PR
    \#115798](https://projects.blender.org/blender/blender/pulls/115798)
    Anim: Add hotkey for keying set operators
  - [PR
    \#111263](https://projects.blender.org/blender/blender/pulls/111263)
    Anim: Bake Channel operator

## December 5 - 8

New

  - [PR
    \#115792](https://projects.blender.org/blender/blender/pulls/115792)
    Fix \#115678: Padding not applied correctly when framing keyframes
    (already landed)
  - [PR
    \#115788](https://projects.blender.org/blender/blender/pulls/115788)
    Fix \#115683: Crash with IK constraint (already landed)
  - [PR
    \#115798](https://projects.blender.org/blender/blender/pulls/115798)
    WIP: Anim: Add pie menu for adding keyframes
  - [PR
    \#115886](https://projects.blender.org/blender/blender/pulls/115886)
    Refactor: combine x and y for insert\_vert\_fcurve into float2
    (already landed)
  - [PR
    \#115898](https://projects.blender.org/blender/blender/pulls/115898)
    Refactor: Pass Keyframe settings as arguments (already landed)

Updated

  - [PR
    \#111263](https://projects.blender.org/blender/blender/pulls/111263)
    Anim: Bake Channel operator

Landed

  - [PR
    \#114845](https://projects.blender.org/blender/blender/pulls/114845)
    Fix \#114828: Don't rebuild outliner tree when frame scrubbing

## November 28 - December 1

Reviewed

  - [PR
    \#115354](https://projects.blender.org/blender/blender/pulls/115354)
    Anim: store BoneCollections in a flat array
  - [PR
    \#114805](https://projects.blender.org/blender/blender/pulls/114805)
    Anim: Refactoring NLA menu into more sensible format
  - [PR
    \#115409](https://projects.blender.org/blender/blender/pulls/115409)
    Fix \#44834: Add bone selection icon next to face and vertex
    selection in weight paint mode

New

  - [PR
    \#115517](https://projects.blender.org/blender/blender/pulls/115517)
    Fix: FCurve colors on bones (already landed)
  - [PR
    \#115525](https://projects.blender.org/blender/blender/pulls/115525)
    WIP: Anim: Separate keying flags
  - [PR
    \#115522](https://projects.blender.org/blender/blender/pulls/115522)
    Anim: Don't use keying sets when inserting keyframes during
    autokeying
  - [PR
    \#115675](https://projects.blender.org/blender/blender/pulls/115675)
    WIP: world space copy/paste prototype

Landed

  - [PR
    \#115360](https://projects.blender.org/blender/blender/pulls/115360)
    Anim: Change how Only Insert Needed works

## November 21 - 24

New

  - [PR
    \#115295](https://projects.blender.org/blender/blender/pulls/115295)
    Refactor: Rename eAutokey\_Flag enum (already landed)
  - [PR
    \#115297](https://projects.blender.org/blender/blender/pulls/115297)
    Fix: Inserting keys no longer sets the FCurve channel color (already
    landed)
  - [PR
    \#115349](https://projects.blender.org/blender/blender/pulls/115349)
    Refactor: Move insert\_key\_rna from editors to animrig (already
    landed)
  - [PR
    \#115360](https://projects.blender.org/blender/blender/pulls/115360)
    Anim: Change how Only Insert Needed works

Landed

  - [PR
    \#114407](https://projects.blender.org/blender/blender/pulls/114407)
    Anim: View FCurve of Property in the Graph Editor
  - [PR
    \#113504](https://projects.blender.org/blender/blender/pulls/113504)
    Anim: Insert keyframes without keying sets
  - [PR
    \#115064](https://projects.blender.org/blender/blender/pulls/115064)
    Anim: Add basic autokey tests
  - [PR
    \#114854](https://projects.blender.org/blender/blender/pulls/114854)
    Anim: Restrict range of fcurve\_to\_keylist

Updated Wiki

  - [PR
    \#104642](https://projects.blender.org/blender/blender-manual/pulls/104642)
    Anim: Change description for inserting keyframes
  - [PR
    \#104643](https://projects.blender.org/blender/blender-manual/pulls/104643)
    Anim: Add tip for the "View in Graph Editor" operator

## November 14 - 17

Updated

  - [PR
    \#114407](https://projects.blender.org/blender/blender/pulls/114407)
    Anim: View FCurve of Property in the Graph Editor
  - [PR
    \#111263](https://projects.blender.org/blender/blender/pulls/111263)
    Anim: Bake Channel operator

New

  - [PR
    \#114845](https://projects.blender.org/blender/blender/pulls/114845)
    Fix \#114828: Don't rebuild outliner tree when frame scrubbing
  - [PR
    \#114851](https://projects.blender.org/blender/blender/pulls/114851)
    Refactor: code style in keyframes\_keylist.cc (already landed)
  - [PR
    \#114854](https://projects.blender.org/blender/blender/pulls/114854)
    WIP: Anim: Restrict range of fcurve\_to\_keylist
  - [PR
    \#115039](https://projects.blender.org/blender/blender/pulls/115039)
    Refactor: Extract function to initialize a BezTriple (already
    landed)
  - [PR
    \#115051](https://projects.blender.org/blender/blender/pulls/115051)
    Refactor: move autokeyframe\_pose (already landed)
  - [PR
    \#115064](https://projects.blender.org/blender/blender/pulls/115064)
    Anim: Add basic autokey tests

Landed

  - [PR
    \#114724](https://projects.blender.org/blender/blender/pulls/114724)
    BLI: Two digit ms precision for SCOPED\_TIMER macro

## November 7 - 10

New

  - [PR
    \#114568](https://projects.blender.org/blender/blender/pulls/114568)
    Refactor: fcurve.cc (already landed)
  - [PR
    \#114575](https://projects.blender.org/blender/blender/pulls/114575)
    Refactor: Move code for actions into its own file (already landed)
  - [PR
    \#114570](https://projects.blender.org/blender/blender/pulls/114570)
    Refactor: move code related to fcurve keyframe insertion (already
    landed)
  - [PR
    \#114581](https://projects.blender.org/blender/blender/pulls/114581)
    Refactor: Move code related to animdata to animrig (already landed)
  - [PR
    \#114584](https://projects.blender.org/blender/blender/pulls/114584)
    Refactor: Rename functions in ANIM\_animdata.hh (already landed)
  - [PR
    \#114586](https://projects.blender.org/blender/blender/pulls/114586)
    Cleanup: comment style in animrig (already landed)
  - [PR
    \#114607](https://projects.blender.org/blender/blender/pulls/114607)
    Fix \#114588: Graph Editor increment snap not working (already
    landed)
  - [PR
    \#114676](https://projects.blender.org/blender/blender/pulls/114676)
    Refactor: More specific arguments for
    ANIM\_get\_normalization\_flags

Landed

  - [PR
    \#114465](https://projects.blender.org/blender/blender/pulls/114465)
    Anim: Unit tests for inserting keyframes

Updated

  - [PR
    \#114407](https://projects.blender.org/blender/blender/pulls/114407)
    WIP: Anim: View FCurve of Property in the Graph Editor
  - [PR
    \#113504](https://projects.blender.org/blender/blender/pulls/113504)
    WIP: Anim: Insert keyframes without keying sets

## October 31 - November 3

Spent time on the bugtracker

Categorised as "Known Issue"

  - [\#97886](https://projects.blender.org/blender/blender/issues/97886)
    Keying sets other than Whole Character (or keyframing in general) do
    not respect channel locks
  - [\#104055](https://projects.blender.org/blender/blender/issues/104055)
    Action Editor: Animations no longer work after renaming bones
  - [\#109481](https://projects.blender.org/blender/blender/issues/109481)
    There can be infinite camera markers on one keyframe, active camera
    can't be uniquely determined.

Closed

  - [\#114317](https://projects.blender.org/blender/blender/issues/114317)
    crach when adding animation (disabled in the viewport, disabled
    during rendering)

New

  - [PR
    \#114325](https://projects.blender.org/blender/blender/pulls/114325)
    Fix: NLA mapping not taken into account when framing FCurves
    (already landed)
  - [PR
    \#114407](https://projects.blender.org/blender/blender/pulls/114407)
    WIP: Anim: Frame FCurve of Property in the Graph Editor
  - [PR
    \#114444](https://projects.blender.org/blender/blender/pulls/114444)
    Refactor: move ANIM\_setting\_get\_rna\_values to animrig (already
    landed)
  - [PR
    \#114445](https://projects.blender.org/blender/blender/pulls/114445)
    Refactor: Move fcurve functions to animrig (already landed)
  - [PR
    \#114448](https://projects.blender.org/blender/blender/pulls/114448)
    Cleanup: Comments in animrig (already landed)
  - [PR
    \#114450](https://projects.blender.org/blender/blender/pulls/114450)
    Refactor: Rename functions in animrig to remove prefix (already
    landed)
  - [PR
    \#114465](https://projects.blender.org/blender/blender/pulls/114465)
    Anim: Unit tests for inserting keyframes

Updated

  - [PR
    \#113504](https://projects.blender.org/blender/blender/pulls/113504)
    WIP: Anim: Insert keyframes without keying sets

## October 24 - 27

Prepared the presentation for Bcon23 on Tuesday

Thursday and Friday I was at Bcon23

## October 17 - 20

Prepared the presentation with Sybren and Nathan for Bcon23

New

  - [PR
    \#113817](https://projects.blender.org/blender/blender/pulls/113817)
    Refactor: Remove nla\_cache parameter from insert\_keyframe (already
    landed)
  - [PR
    \#113931](https://projects.blender.org/blender/blender/pulls/113931)
    Refactor: ANIM\_setting\_get\_rna\_values to return a Vector
    (already landed)
  - [PR
    \#113940](https://projects.blender.org/blender/blender/pulls/113940)
    Fix: autokeyframing when pasting poses no longer worked (already
    landed)

Updated

  - [PR
    \#113504](https://projects.blender.org/blender/blender/pulls/113504)
    WIP: Anim: Insert keyframes without keying sets

Documentation

  - [PR
    \#104595](https://projects.blender.org/blender/blender-manual/pulls/104595)
    Anim: Update paint select mode shortcuts

## October 10 - 13

New

  - [PR
    \#113504](https://projects.blender.org/blender/blender/pulls/113504)
    WIP: ANIM: Insert keyframes without keying sets
  - [PR
    \#113503](https://projects.blender.org/blender/blender/pulls/113503)
    Refactor: Move keyframing code to animrig (already landed)
  - [PR
    \#113588](https://projects.blender.org/blender/blender/pulls/113588)
    Refactor: remove bAction\* parameter from ANIM\_apply\_keyingset
    (already landed)
  - [PR
    \#113595](https://projects.blender.org/blender/blender/pulls/113595)
    Refactor: keyframing.cc (already landed)
  - [PR
    \#113607](https://projects.blender.org/blender/blender/pulls/113607)
    Refactor: Move autokeyframing functions to animrig (already landed)
  - [PR
    \#113610](https://projects.blender.org/blender/blender/pulls/113610)
    Refactor: Replace autokey macros with functions (already landed)
  - [PR
    \#113612](https://projects.blender.org/blender/blender/pulls/113612)
    Refactor: remove ED\_ prefix from autokeying functions (already
    landed)
  - [PR
    \#113617](https://projects.blender.org/blender/blender/pulls/113617)
    Refactor: keyframing\_auto.cc code cleanup (already landed)
  - [PR
    \#113666](https://projects.blender.org/blender/blender/pulls/113666)
    Refactor: keyingsets.cc (already landed)
  - [PR
    \#113675](https://projects.blender.org/blender/blender/pulls/113675)
    Refactor: convert ListBase of tRKS\_DSource to Vector (already
    landed)
  - [PR
    \#113677](https://projects.blender.org/blender/blender/pulls/113677)
    Refactor: remove argument from ANIM\_builtin\_keyingset\_get\_named

Landed

  - [PR
    \#113056](https://projects.blender.org/blender/blender/pulls/113056)
    Fix \#111553: Double IK constraint not working

Reviewed

  - [PR
    \#113363](https://projects.blender.org/blender/blender/pulls/113363)
    Fix \#87219: 'affect transform' not working for Pose Space
  - [PR
    \#113554](https://projects.blender.org/blender/blender/pulls/113554)
    Fix \#113505: Scale strips in nla with snap active seems broken

## October 3 - 6

Worked on designs for 4.1, specifically for snapping and keying sets.
And worked on the bugtracker to fix bugs/close issues that have been
solved.

Reviewed

  - [PR
    \#112601](https://projects.blender.org/blender/blender/pulls/112601)
    Fix \#112580: Limit Constraint with 'Affect Transform' not working
    properly in 'World Space'

New

  - [\#113278](https://projects.blender.org/blender/blender/issues/113278)
    Keying Sets Rework
  - [PR
    \#113335](https://projects.blender.org/blender/blender/pulls/113335)
    Fix \#87160: Clean Keyframes only works if channels are selected

Updated

  - [\#91973](https://projects.blender.org/blender/blender/issues/91973)
    Design: More frame snapping options
  - [PR
    \#113056](https://projects.blender.org/blender/blender/pulls/113056)
    Fix \#111553: Double IK constraint not working

Closed

  - [\#97485](https://projects.blender.org/blender/blender/issues/97485)
    Broken curve drawing in normalized view in graph editor
  - [\#95364](https://projects.blender.org/blender/blender/issues/95364)
    Autokey function hinders execution of drivers
  - [\#37512](https://projects.blender.org/blender/blender/issues/37512)
    Design Required: How can users choose what datablock/level to edit
    in the Action Editor?

Worked on the Manual

  - [PR
    \#104580](https://projects.blender.org/blender/blender-manual/pulls/104580)
    Asset Shelf: Add manual entry

## September 26 - 29

Only worked half a day on the 26th due to sickness. Also off sick on the
28th

Reviewed

  - [PR
    \#112861](https://projects.blender.org/blender/blender/pulls/112861)
    Anim: Improve readability of channel colors in channel list

Worked on Bug reports

  - [\#112850](https://projects.blender.org/blender/blender/issues/112850)
    Butterworth Filter is causing issues on geometry

New

  - [PR
    \#113056](https://projects.blender.org/blender/blender/pulls/113056)
    Fix \#111553: Double IK constraint not working

## September 19 - 22

**I was on holiday on the 19th.**

New

  - [PR
    \#112670](https://projects.blender.org/blender/blender/pulls/112670)
    Fix \#97628: Clear and Keep Transformation not working when keyed
    (already landed)

Updated

  - [PR
    \#111263](https://projects.blender.org/blender/blender/pulls/111263)
    Anim: Bake Channel operator

Landed

  - [PR
    \#112419](https://projects.blender.org/blender/blender/pulls/112419)
    Anim: Multi Editing for FCurve modifiers
  - [PR
    \#110540](https://projects.blender.org/blender/blender/pulls/110540)
    Anim: Time Offset Slider
  - [PR
    \#112388](https://projects.blender.org/blender/blender/pulls/112388)
    Anim: Graph Editor Push/Pull operator

## September 12 - 15

Updated the manual to reflect operator renames

  - [PR
    \#104549](https://projects.blender.org/blender/blender-manual/pulls/104549)
    Animation: Operator Renames

New

  - [PR
    \#112365](https://projects.blender.org/blender/blender/pulls/112365)
    Fix: Graph Editor first key handles not drawn
  - [PR
    \#112387](https://projects.blender.org/blender/blender/pulls/112387)
    WIP: Animation: Graph Editor Scale From Neighbor
  - [PR
    \#112419](https://projects.blender.org/blender/blender/pulls/112419)
    Anim: Multi Editing for FCurve modifiers
  - [PR
    \#112388](https://projects.blender.org/blender/blender/pulls/112388)
    Anim: Graph Editor Push/Pull operator

Updated

  - [PR
    \#112131](https://projects.blender.org/blender/blender/pulls/112131)
    WIP: Fix \#112085: Not able to insert keys on subframes
  - [PR
    \#110540](https://projects.blender.org/blender/blender/pulls/110540)
    Animation: Time Offset Slider
  - [PR
    \#111263](https://projects.blender.org/blender/blender/pulls/111263)
    Animation: Bake Channel operator

Landed

  - [PR
    \#112148](https://projects.blender.org/blender/blender/pulls/112148)
    Animation: Rename Graph\_OT\_sample
  - [PR
    \#112151](https://projects.blender.org/blender/blender/pulls/112151)
    Animation: Rename ACTION\_OT\_sample
  - [PR
    \#111510](https://projects.blender.org/blender/blender/pulls/111510)
    Refactor: Rename functions and structs in keyframes\_draw.cc
  - [PR
    \#112131](https://projects.blender.org/blender/blender/pulls/112131)
    Fix \#112085: Not able to insert keys on subframes
  - [PR
    \#111986](https://projects.blender.org/blender/blender/pulls/111986)
    Animation: Graph Editor locked key drawing
  - [PR
    \#111984](https://projects.blender.org/blender/blender/pulls/111984)
    Fix \#110108: NLA absolute snapping

## September 5 - 8

Landed

  - [PR
    \#109015](https://projects.blender.org/blender/blender/pulls/109015)
    Animation: Move Snapping to Scene
  - [PR
    \#110788](https://projects.blender.org/blender/blender/pulls/110788)
    Animation: Graph Editor - Don't draw curve points if they are too
    close to each other
  - [PR
    \#108518](https://projects.blender.org/blender/blender/pulls/108518)
    Animation: Keep icons aligned when curves are baked
  - [PR
    \#111744](https://projects.blender.org/blender/blender/pulls/111744)
    Animation: Scale Average slider for Graph Editor
  - [PR
    \#111049](https://projects.blender.org/blender/blender/pulls/111049)
    Animation: Rename "Bake Curve" to "Keys to Samples"

New

  - [PR
    \#111968](https://projects.blender.org/blender/blender/pulls/111968)
    Refactor: Code indentation in transform\_convert\_nla.cc (already
    landed)
  - [PR
    \#111984](https://projects.blender.org/blender/blender/pulls/111984)
    Fix \#110108: NLA absolute snapping
  - [PR
    \#112072](https://projects.blender.org/blender/blender/pulls/112072)
    Fix: Graph Editor extrapolation line drawing issue (already landed)
  - [PR
    \#112088](https://projects.blender.org/blender/blender/pulls/112088)
    Fix: Animation Editor snapping not working when using rotate or
    scale (already landed)
  - [PR
    \#112126](https://projects.blender.org/blender/blender/pulls/112126)
    Animation: Graph Editor optimize handle drawing (already landed)
  - [PR
    \#112151](https://projects.blender.org/blender/blender/pulls/112151)
    Animation: Rename ACTION\_OT\_sample
  - [PR
    \#112148](https://projects.blender.org/blender/blender/pulls/112148)
    Animation: Rename Graph\_OT\_sample

## August 31 - September 1

Manual

  - [PR
    \#104539](https://projects.blender.org/blender/blender-manual/pulls/104539)
    Animation: Add latest Graph Editor operators

New

  - [PR
    \#111735](https://projects.blender.org/blender/blender/pulls/111735)
    Animation: Shear operator for Graph Editor
  - [PR
    \#111745](https://projects.blender.org/blender/blender/pulls/111745)
    Fix: Blend to Ease keyframes property name (already landed)
  - [PR
    \#111744](https://projects.blender.org/blender/blender/pulls/111744)
    Animation: Scale Average slider for Graph Editor
  - [PR
    \#111785](https://projects.blender.org/blender/blender/pulls/111785)
    Fix: Cursor wrapping in Graph Editor slider operators (already
    landed)
  - [PR
    \#111793](https://projects.blender.org/blender/blender/pulls/111793)
    Fix \#111776: Bone Collections with linked file from 3.6

Updated

  - [PR
    \#111510](https://projects.blender.org/blender/blender/pulls/111510)
    Refactor: Rename functions and structs in keyframes\_draw.cc
  - [PR
    \#111263](https://projects.blender.org/blender/blender/pulls/111263)
    Animation: Bake Channel operator

## August 24-25

Reviewed:

  - [PR
    \#109976](https://projects.blender.org/blender/blender/pulls/109976)
    Replace Bone Layers+Groups with Bone Collections

New

  - [PR
    \#111464](https://projects.blender.org/blender/blender/pulls/111464)
    Fix: Memory leak in Graph editor keyframe jump (already landed)
  - [PR
    \#111510](https://projects.blender.org/blender/blender/pulls/111510)
    Refactor: Rename functions and structs in keyframes\_draw.cc

Updated

  - [PR
    \#111254](https://projects.blender.org/blender/blender/pulls/111254)
    Fix \#111212: Crash when running search while cursor is outside
    Blender window
  - [PR
    \#110540](https://projects.blender.org/blender/blender/pulls/110540)
    Animation: Time Offset Slider

## August 17-18

New

  - [PR
    \#111254](https://projects.blender.org/blender/blender/pulls/111254)
    Fix \#111212: Crash when running search while cursor is outside
    Blender window
  - [PR
    \#111269](https://projects.blender.org/blender/blender/pulls/111269)
    Fix: Number input on slider
  - [PR
    \#111263](https://projects.blender.org/blender/blender/pulls/111263)
    WIP: Animation: Bake Channel operator

Landed

  - [PR
    \#110567](https://projects.blender.org/blender/blender/pulls/110567)
    Animation: Match Slope slider
  - [PR
    \#110795](https://projects.blender.org/blender/blender/pulls/110795)
    Fix \#110053: Crash on linked animation data with drivers

Updated

  - [PR
    \#111037](https://projects.blender.org/blender/blender/pulls/111037)
    Refactor: resolution\_scale in graph\_draw.cc
  - [PR
    \#110788](https://projects.blender.org/blender/blender/pulls/110788)
    Animation: Graph Editor - Don't draw keys if they are too close to
    each other
  - [PR
    \#110540](https://projects.blender.org/blender/blender/pulls/110540)
    Animation: Time Offset Slider
  - [PR
    \#110795](https://projects.blender.org/blender/blender/pulls/110795)
    Fix \#110053: Crash on linked animation data with drivers

## August 10-11

Updated

  - [PR
    \#110764](https://projects.blender.org/blender/blender/pulls/110764)
    Animation: Common curve drawing for FCurves
  - [PR
    \#110544](https://projects.blender.org/blender/blender/pulls/110544)
    Animation: Blend Offset Slider
  - [PR
    \#110567](https://projects.blender.org/blender/blender/pulls/110567)
    Animation: Blend to Infinity slider
  - [PR
    \#110567](https://projects.blender.org/blender/blender/pulls/110567)
    Animation: Blend to Infinity slider
  - [PR
    \#110788](https://projects.blender.org/blender/blender/pulls/110788)
    Animation: Graph Editor - Don't draw keys if they are too close to
    each other

Landed

  - [PR
    \#110790](https://projects.blender.org/blender/blender/pulls/110790)
    Fix \#110789: Apply NLA mapping when jumping to keys in the Graph
    Editor
  - [PR
    \#110586](https://projects.blender.org/blender/blender/pulls/110586)
    Animation: Right align slider unit text
  - [PR
    \#110764](https://projects.blender.org/blender/blender/pulls/110764)
    Animation: Common curve drawing for FCurves
  - [PR
    \#110544](https://projects.blender.org/blender/blender/pulls/110544)
    Animation: Blend Offset Slider
  - [PR
    \#110566](https://projects.blender.org/blender/blender/pulls/110566)
    Animation: Blend To Ease Slider

New

  - [PR
    \#111049](https://projects.blender.org/blender/blender/pulls/111049)
    Animation: Rename "Bake Curve" to "Keys to Samples"

## August 03-04

New proposal to remove the Bake FCurve feature:
<https://devtalk.blender.org/t/removing-baked-fcurves/30553>

New

  - [PR
    \#110764](https://projects.blender.org/blender/blender/pulls/110764)
    Animation: Common curve drawing for FCurves
  - [PR
    \#110788](https://projects.blender.org/blender/blender/pulls/110788)
    WIP: Animation: Graph Editor - Don't draw keys if they are too close
    to each other
  - [PR
    \#110790](https://projects.blender.org/blender/blender/pulls/110790)
    Fix \#110789: Apply NLA mapping when jumping to keys in the Graph
    Editor
  - [PR
    \#110795](https://projects.blender.org/blender/blender/pulls/110795)
    WIP: Fix \#110053: Crash on linked animation data with drivers

Updated:

  - [PR
    \#109015](https://projects.blender.org/blender/blender/pulls/109015)
    Animation: Move Snapping to Scene

Landed:

  - [PR
    \#110569](https://projects.blender.org/blender/blender/pulls/110569)
    Fix \#108961: Wrong Label and Tooltip for Timeline
  - [PR
    \#110301](https://projects.blender.org/blender/blender/pulls/110301)
    Animation: Graph Editor curve drawing performance improvement

## July 27-28

Had a look at dope sheet drawing and if it can be improved. The
bottleneck is the keylist system, and I don't see how this can be easily
improved. Probably best to wait for the new animation system to be
established

Taken over abandoned patches:

  - [PR
    \#110544](https://projects.blender.org/blender/blender/pulls/110544)
    Animation: Blend Offset Slider
  - [PR
    \#110540](https://projects.blender.org/blender/blender/pulls/110540)
    WIP: Animation: Time Offset Slider
  - [PR
    \#110566](https://projects.blender.org/blender/blender/pulls/110566)
    Animation: Blend To Ease Slider
  - [PR
    \#110567](https://projects.blender.org/blender/blender/pulls/110567)
    Animation: Blend to Infinity slider

New

  - [PR
    \#110569](https://projects.blender.org/blender/blender/pulls/110569)
    Fix \#108961: Wrong Label and Tooltip for Timeline
  - [PR
    \#110586](https://projects.blender.org/blender/blender/pulls/110586)
    Animation: Right align slider unit text

Landed

  - [PR
    \#110306](https://projects.blender.org/blender/blender/pulls/110306)
    Animation: Disable NLA mapping if no NLA tracks are present
  - [PR
    \#110290](https://projects.blender.org/blender/blender/pulls/110290)
    Refactor: extract code from dopesheet draw function

Updated

  - [PR
    \#110301](https://projects.blender.org/blender/blender/pulls/110301)
    Animation: Graph Editor curve drawing performance improvement
  - [PR
    \#109015](https://projects.blender.org/blender/blender/pulls/109015)
    Animation: Move Snapping to Scene

## July 20-21

Landed

  - [PR
    \#108309](https://projects.blender.org/blender/blender/pulls/108309)
    Animation: Merge Push/Relax Rest Pose operators

Updated

  - [PR
    \#109015](https://projects.blender.org/blender/blender/pulls/109015)
    Animation: Move Snapping to Scene

New

  - [PR
    \#110290](https://projects.blender.org/blender/blender/pulls/110290)
    Refactor: extract code from dopsheet draw function
  - [PR
    \#110306](https://projects.blender.org/blender/blender/pulls/110306)
    Animation: Disable NLA mapping if no NLA tracks are present
  - [PR
    \#110301](https://projects.blender.org/blender/blender/pulls/110301)
    Animation: Graph Editor curve drawing performance improvement

## July 13-14

Landed

  - [PR
    \#106952](https://projects.blender.org/blender/blender/pulls/106952)
    Animation: Butterworth Smoothing filter
  - [PR
    \#109813](https://projects.blender.org/blender/blender/pulls/109813)
    Fix \#109781: Linked datablocks are keyable

Updated

  - [PR
    \#109015](https://projects.blender.org/blender/blender/pulls/109015)
    Animation: Move Snapping to Scene

New

  - (Blender-Manual) [PR
    \#104511](https://projects.blender.org/blender/blender-manual/pulls/104511)
    Animation: Added Butterworth Filter (already landed)
  - [PR
    \#110059](https://projects.blender.org/blender/blender/pulls/110059)
    Fix \#109799: Smooth (Gaussian) introduces stepping (already landed)
  - [PR
    \#110060](https://projects.blender.org/blender/blender/pulls/110060)
    Fix: Stepping issue with Butterworth filter (already landed)

## July 6-7 2023

Was looking into allowing to add user pref properties to the quick
favourites menu

New

  - [PR
    \#109768](https://projects.blender.org/blender/blender/pulls/109768)
    Animation: Allow setting the slider unit and mode (split off from
    \#106952)
  - [PR
    \#109813](https://projects.blender.org/blender/blender/pulls/109813)
    Fix \#109781: Linked datablocks are keyable

Updated

  - [PR
    \#106952](https://projects.blender.org/blender/blender/pulls/106952)
    Animation: Butterworth Smoothing filter
  - [PR
    \#106052](https://projects.blender.org/blender/blender/pulls/106052)
    Animation: Improve drawing of locked FCurves
  - [PR
    \#109015](https://projects.blender.org/blender/blender/pulls/109015)
    WIP: Animation: Snapping options new GUI

Landed

  - [PR
    \#106052](https://projects.blender.org/blender/blender/pulls/106052)
    Animation: Improve drawing of locked FCurves
  - [PR
    \#109768](https://projects.blender.org/blender/blender/pulls/109768)
    Animation: Allow setting the slider unit and mode

## June 26-30 2023

Animation Workshop

## June 22-23 2023

Moved the days to the following week for the animation workshop

## June 15-16 2023

Reviewed

  - [PR
    \#108142](https://projects.blender.org/blender/blender/pulls/108142)
    Animation: Graph Editor Handle Selection

Landed

  - [PR
    \#108748](https://projects.blender.org/blender/blender/pulls/108748)
    Refactor: draw fcurve bezt function

New

  - [PR
    \#109015](https://projects.blender.org/blender/blender/pulls/109015)
    Animation: Snapping options new GUI

Updated

  - [PR
    \#106952](https://projects.blender.org/blender/blender/pulls/106952)
    Animation: Butterworth Smoothing filter
  - [PR
    \#108518](https://projects.blender.org/blender/blender/pulls/108518)
    Animation: Draw lock icon on baked curves
  - [\#102217](https://projects.blender.org/blender/blender/issues/102217)
    Design: Onion Skinning for Blender

## June 08-09 2023

New

  - [PR
    \#108748](https://projects.blender.org/blender/blender/pulls/108748)
    Refactor: draw fcurve bezt function
  - [PR
    \#108753](https://projects.blender.org/blender/blender/pulls/108753)
    Fix: Face loop select toggle behaviour in paint mode (already
    landed)
  - [PR
    \#108759](https://projects.blender.org/blender/blender/pulls/108759)
    Fix: Crash when using loop select on edge that has only 1 face
    (already landed)
  - [PR
    \#108770](https://projects.blender.org/blender/blender/pulls/108770)
    WIP: Paint: Vertex loop select
  - [PR
    \#108809](https://projects.blender.org/blender/blender/pulls/108809)
    Fix: Paint loop select to use distance to edge

Landed

  - [PR
    \#108549](https://projects.blender.org/blender/blender/pulls/108549)
    Fix \#102737: Keyframe jump operator includes hidden curves in Graph
    Editor

Updated

  - [PR
    \#106952](https://projects.blender.org/blender/blender/pulls/106952)
    Animation: Butterworth Smoothing filter
  - [PR
    \#108309](https://projects.blender.org/blender/blender/pulls/108309)
    Animation: Merge Push/Relax Rest Pose operators

## June 01-02 2023

Updated the VS-Code documentation for Clang-Format
<https://wiki.blender.org/wiki/Developer_Intro/Environment/Portable_CMake_VSCode>

Went through bug tracker

  - closed because already fixed:
    [\#102709](https://projects.blender.org/blender/blender/issues/102709)
    Selection of channels in Timeline inconsistent or not working in
    comparison to View Layer

New

  - [PR
    \#108549](https://projects.blender.org/blender/blender/pulls/108549)
    Fix \#102737: Keyframe jump operator includes hidden curves in Graph
    Editor

Landed

  - [PR
    \#107653](https://projects.blender.org/blender/blender/pulls/107653)
    Paint: Add loop select for faces
  - [PR
    \#107173](https://projects.blender.org/blender/blender/pulls/107173)
    Refactor: Bidirectionality on Graph Editor Sliders

Updated

  - [PR
    \#107653](https://projects.blender.org/blender/blender/pulls/107653)
    Paint: Add loop select for faces \#107653
  - [PR
    \#108309](https://projects.blender.org/blender/blender/pulls/108309)
    Animation: Merge Push/Relax Rest Pose operators \#108309
  - [PR
    \#106952](https://projects.blender.org/blender/blender/pulls/106952)
    Animation: Butterworth Smoothing filter
  - [PR
    \#106052](https://projects.blender.org/blender/blender/pulls/106052)
    Animation: Improve drawing of locked FCurves

## May 25-26 2023

Continued work on the onion skinning prototype

Updated release notes for 3.6 and 4.0

Landed

  - [PR
    \#107866](https://projects.blender.org/blender/blender/pulls/107866)
    Animation: change Slider Popup menu hotkeys
  - [PR
    \#105120](https://projects.blender.org/blender/blender/pulls/105120)
    Animation: Remove collection hotkeys from pose mode
  - [PR
    \#107887](https://projects.blender.org/blender/blender/pulls/107887)
    Fix: Channel select keys doesn't add undo step

Updated

  - [PR
    \#107653](https://projects.blender.org/blender/blender/pulls/107653)
    Paint: Add loop select for faces
  - [PR
    \#106952](https://projects.blender.org/blender/blender/pulls/106952)
    Animation: Butterworth Smoothing filter

## May 18-19 2023

Holiday week

## May 11-12 2023

Added more screenshots and videos to the release notes
<https://wiki.blender.org/wiki/Reference/Release_Notes/3.6/Animation_Rigging#Graph_Editor>

worked on the manual:
[\#104439](https://projects.blender.org/blender/blender-manual/issues/104439)
Updates to Graph Editor manual

New

  - [PR
    \#107866](https://projects.blender.org/blender/blender/pulls/107866)
    Animation: change Slider Popup menu hotkeys
  - [PR
    \#107887](https://projects.blender.org/blender/blender/pulls/107887)
    Fix: Channel select keys doesn't add undo step

Updated

  - [PR
    \#107173](https://projects.blender.org/blender/blender/pulls/107173)
    Refactor: Bidirectionality on Graph Editor Sliders
  - [PR
    \#107653](https://projects.blender.org/blender/blender/pulls/107653)
    Paint: Add loop select for faces
  - [PR
    \#106952](https://projects.blender.org/blender/blender/pulls/106952)
    Animation: Butterworth Smoothing filter

## May 04-05 2023

New:

  - [PR
    \#107610](https://projects.blender.org/blender/blender/pulls/107610)
    Refactor: pose\_slide.c (already landed)
  - [PR
    \#107641](https://projects.blender.org/blender/blender/pulls/107641)
    WIP: Onion Skinning Prototype
  - [PR
    \#107653](https://projects.blender.org/blender/blender/pulls/107653)
    WIP: Paint: Add face loop select

Landed

  - [PR
    \#107435](https://projects.blender.org/blender/blender/pulls/107435)
    Fix: Graph Editor - add operator flags for cursor wrapping
  - [PR
    \#107402](https://projects.blender.org/blender/blender/pulls/107402)
    Refactor: Replace move\_key with
    BKE\_fcurve\_keyframe\_move\_value\_with\_handles
  - [PR
    \#106113](https://projects.blender.org/blender/blender/pulls/106113)
    Animation: Clean up "Key" menu in Graph Editor
  - [PR
    \#107406](https://projects.blender.org/blender/blender/pulls/107406)
    Refactor: Allow to explicitly set the range on the slider UI element

Updated

  - [PR
    \#107406](https://projects.blender.org/blender/blender/pulls/107406)
    Refactor: Allow to explicitly set the range on the slider UI element

## April 27-28 2023

Started cleaning up the release notes:
<https://wiki.blender.org/wiki/Reference/Release_Notes/3.6/Animation_Rigging>

New

  - [PR
    \#107397](https://projects.blender.org/blender/blender/pulls/107397)
    Fix: Slider bidirectionality in pose\_slide.c
  - [PR
    \#107402](https://projects.blender.org/blender/blender/pulls/107402)
    Refactor: Replace move\_key with
    BKE\_fcurve\_keyframe\_move\_value\_with\_handles
  - [PR
    \#107406](https://projects.blender.org/blender/blender/pulls/107406)
    Refactor: Allow to explicitly set the range on the slider UI element
  - [PR
    \#107435](https://projects.blender.org/blender/blender/pulls/107435)
    Fix: Graph Editor - add operator flags for cursor wrapping

Landed

  - [PR
    \#107170](https://projects.blender.org/blender/blender/pulls/107170)
    Refactor: Deduplicate code in graph\_slider\_ops.c

Updated

  - [PR
    \#106952](https://projects.blender.org/blender/blender/pulls/106952)
    Animation: Butterworth Smoothing filter
  - [PR
    \#106113](https://projects.blender.org/blender/blender/pulls/106113)
    Animation: Clean up "Key" menu in Graph Editor

## April 20-21 2023

New

  - [PR
    \#107170](https://projects.blender.org/blender/blender/pulls/107170)
    Refactor: Deduplicate code in graph\_slider\_ops.c
  - [PR
    \#107173](https://projects.blender.org/blender/blender/pulls/107173)
    Fix: Bidirectionality on Graph Editor Sliders

Landed

  - [PR
    \#106959](https://projects.blender.org/blender/blender/pulls/106959)
    Animation: make properties from motion path library overrideable
  - [PR
    \#106890](https://projects.blender.org/blender/blender/pulls/106890)
    Fix: Normalization with baked curves and preview range
  - [PR
    \#106904](https://projects.blender.org/blender/blender/pulls/106904)
    Fix \#106771: Selection offset in timeline when NLA track is offset

Updated

  - [PR
    \#106952](https://projects.blender.org/blender/blender/pulls/106952)
    Animation: Butterworth Smoothing filter

Reviewed

  - [PR
    \#106520](https://projects.blender.org/blender/blender/pulls/106520)
    Animation: time offset slider
  - [PR
    \#106519](https://projects.blender.org/blender/blender/pulls/106519)
    Animation: blend to ease slider
  - [PR
    \#106518](https://projects.blender.org/blender/blender/pulls/106518)
    Animation: blend offset slider
  - [PR
    \#106517](https://projects.blender.org/blender/blender/pulls/106517)
    Animation: blend to infinity slider
  - [PR
    \#106521](https://projects.blender.org/blender/blender/pulls/106521)
    Animation: shear left slider
  - [PR
    \#104565](https://projects.blender.org/blender/blender/pulls/104565)
    Allow select range in animation editor

## April 13-14 2023

New

  - [PR
    \#106888](https://projects.blender.org/blender/blender/pulls/106888)
    FIx: Respect preview range when auto normalizing in Graph Editor
    (already landed)
  - [PR
    \#106890](https://projects.blender.org/blender/blender/pulls/106890)
    Fix: Normalization with baked curves and preview range
  - [PR
    \#106904](https://projects.blender.org/blender/blender/pulls/106904)
    Fix \#106771: Selection offset in timeline when NLA track is offset
  - [PR
    \#106952](https://projects.blender.org/blender/blender/pulls/106952)
    Animation: Butterworth Smoothing filter
  - [PR
    \#106959](https://projects.blender.org/blender/blender/pulls/106959)
    Animation: make properties from motion path library overrideable

Reviewed

  - [PR
    \#106517](https://projects.blender.org/blender/blender/pulls/106517)
    Animation: blend to infinity slider
  - [PR
    \#106519](https://projects.blender.org/blender/blender/pulls/106519)
    Animation: blend to ease slider
  - [PR
    \#106518](https://projects.blender.org/blender/blender/pulls/106518)
    Animation: blend offset slider
  - [PR
    \#106520](https://projects.blender.org/blender/blender/pulls/106520)
    Animation: time offset slider

## April 06-07 2023

Holiday week

## Mar 30-31 2023

Landed

  - [PR
    \#105857](https://projects.blender.org/blender/blender/pulls/105857)
    Animation: Auto frame curves Y extents when hitting normalize
  - [PR
    \#105633](https://projects.blender.org/blender/blender/pulls/105633)
    Animation: Weight Paint select more/less for vertices
  - [PR
    \#105607](https://projects.blender.org/blender/blender/pulls/105607)
    Animation: Weight Paint select more/less for faces

New

  - [PR
    \#106302](https://projects.blender.org/blender/blender/pulls/106302)
    Animation: Graph Editor - grey out area outside of normalization
    range (already landed)
  - [PR
    \#106307](https://projects.blender.org/blender/blender/pulls/106307)
    Animation: Insert Keyframe only on active FCurve (already landed)
  - [PR
    \#106365](https://projects.blender.org/blender/blender/pulls/106365)
    WIP: Animation: Smoothing without Shrinkage

Updated

  - [PR
    \#106113](https://projects.blender.org/blender/blender/pulls/106113)
    Animation: Clean up "Key" menu in Graph Editor

## Mar 23-24 2023

New

  - [PR
    \#106100](https://projects.blender.org/blender/blender/pulls/106100)
    Fix \#106095: FCurves not drawn when Extrapolation is disabled
    (already fixed)
  - [PR
    \#106102](https://projects.blender.org/blender/blender/pulls/106102)
    Fix: Crash when trying to get FCurve segments of baked curve
    (already landed)
  - [PR
    \#106113](https://projects.blender.org/blender/blender/pulls/106113)
    Animation: Clean up "Key" menu in Graph Editor

Landed

  - [PR
    \#104882](https://projects.blender.org/blender/blender/pulls/104882)
    Animation: Add duration display to frame range settings
  - [PR
    \#105635](https://projects.blender.org/blender/blender/pulls/105635)
    Animation: Gaussian Smooth operator for Graph Editor

Updated

  - [PR
    \#105633](https://projects.blender.org/blender/blender/pulls/105633)
    Animation: Weight Paint select more/less for vertices
  - [PR
    \#105607](https://projects.blender.org/blender/blender/pulls/105607)
    Animation: Weight Paint select more/less for faces
  - [PR
    \#105857](https://projects.blender.org/blender/blender/pulls/105857)
    Animation: Auto frame curves Y extents when hitting normalize
  - [PR
    \#105635](https://projects.blender.org/blender/blender/pulls/105635)
    Animation: Gaussian Smooth operator for Graph Editor

## Mar 16-17 2023

New

  - [PR
    \#105635](https://projects.blender.org/blender/blender/pulls/105635)
    Animation: Gauss Smooth operator for Graph Editor
  - [PR
    \#105857](https://projects.blender.org/blender/blender/pulls/105857)
    Animation: Auto frame curves Y extents when hitting normalize

Updated

  - [PR
    \#105633](https://projects.blender.org/blender/blender/pulls/105633)
    Animation: Weight Paint select more/less for vertices
  - [PR
    \#105607](https://projects.blender.org/blender/blender/pulls/105607)
    Animation: Weight Paint select more/less for faces

Reviewed

  - [PR
    \#105427](https://projects.blender.org/blender/blender/pulls/105427)
    Bone relation lines: draw between axis points

## Mar 09-10 2023

New PRs:

  - [PR
    \#105607](https://projects.blender.org/blender/blender/pulls/105607)
    Animation: Weight Paint select more/less for faces
  - [PR
    \#105633](https://projects.blender.org/blender/blender/pulls/105633)
    Animation: Weight Paint select more/less for vertices
  - [PR
    \#105635](https://projects.blender.org/blender/blender/pulls/105635)
    Wip: Animation: Gauss Smooth operator for Graph Editor

Updated PRs

  - [PR
    \#104882](https://projects.blender.org/blender/blender/pulls/104882)
    Animation: Add duration display to frame range settings
  - [PR
    \#104532](https://projects.blender.org/blender/blender/pulls/104532)
    Animation: Move Graph Editor settings to User Preferences
  - [PR
    \#105177](https://projects.blender.org/blender/blender/pulls/105177)
    Refactor: fcurve bounds functions

Landed:

  - [PR
    \#104532](https://projects.blender.org/blender/blender/pulls/104532)
    Animation: Move Graph Editor settings to User Preferences
  - [PR
    \#105177](https://projects.blender.org/blender/blender/pulls/105177)
    Refactor: fcurve bounds functions

## Mar 02-03 2023

Sybren and Nathan and I started building a map of Blenders function
calls focused on the animation side.

New PRs

  - [PR
    \#105401](https://projects.blender.org/blender/blender/pulls/105401)
    Fix \#105329: Keyframe handles staying in place when using slider
    operators (already landed)

Updated PRs

  - [PR
    \#105177](https://projects.blender.org/blender/blender/pulls/105177)
    Refactor: fcurve bounds functions
  - [PR
    \#104882](https://projects.blender.org/blender/blender/pulls/104882)
    Animation: Add duration display to frame range settings

Landed PRs

  - [PR
    \#105179](https://projects.blender.org/blender/blender/pulls/105179)
    Fix: Frame Channels fails when no keys in range

## Feb 23-24 2023

looked at implementing a solution for
<https://projects.blender.org/blender/blender/issues/79024> but need
help on that one

updated the proposal for changing the look of FCurves
<https://projects.blender.org/blender/blender/issues/104867>

Landed

  - [PR
    \#104512](https://projects.blender.org/blender/blender/pulls/104512)
    Animation: Paste Keys in Graph Editor with value offset
  - [PR
    \#104530](https://projects.blender.org/blender/blender/pulls/104530)
    Animation: Add Slider operators to hotkey menu

New Pull Requests

  - [PR
    \#105120](https://projects.blender.org/blender/blender/pulls/105120)
    Animation: Remove collection hotkeys from pose mode
  - [PR
    \#105123](https://projects.blender.org/blender/blender/pulls/105123)
    Fix: Using "Frame Channel" in Dope sheet resets view to top
  - [PR
    \#105121](https://projects.blender.org/blender/blender/pulls/105121)
    Fix: Frame Channel when using normalized view
  - [PR
    \#105177](https://projects.blender.org/blender/blender/pulls/105177)
    Refactor: fcurve bounds functions
  - [PR
    \#105179](https://projects.blender.org/blender/blender/pulls/105179)
    Fix: Frame Channels fails when no keys in range

## Feb 16-17 2023

Making sure the Release Notes for A\&R are up to date

New Design Task

  - [\#104867](https://projects.blender.org/blender/blender/issues/104867)
    Design: Graph Editor Curve Display

Landed

  - [PR
    \#104516](https://projects.blender.org/blender/blender/pulls/104516)
    Animation: Clamp V2D so keyframes cannot go offscreen
  - [PR
    \#104523](https://projects.blender.org/blender/blender/pulls/104523)
    Animation: Add "Frame Channel" operators

New PRs

  - [PR
    \#104858](https://projects.blender.org/blender/blender/pulls/104858)
    Fix \#95400: Crash when running Euler Filter on baked Curves
    (already merged)

Updated Pull Requests

  - [PR
    \#104577](https://projects.blender.org/blender/blender/pulls/104577)
    Refactor: Weight Paint Select Linked Faces
  - [PR
    \#104532](https://projects.blender.org/blender/blender/pulls/104532)
    Animation: Move Graph Editor settings to User Preferences
  - [PR
    \#104530](https://projects.blender.org/blender/blender/pulls/104530)
    Animation: Add Slider operators to hotkey menu
  - [PR
    \#104523](https://projects.blender.org/blender/blender/pulls/104523)
    Animation: Add "Frame Channel" operators
  - [PR
    \#104516](https://projects.blender.org/blender/blender/pulls/104516)
    Animation: Clamp V2D so keyframes cannot go offscreen
  - [PR
    \#104512](https://projects.blender.org/blender/blender/pulls/104512)
    Animation: Paste Keys in Graph Editor with value offset

## Feb 09-10 2023

New Pull Requests

  - Remove drawing of keyframes on locked FCurves (WIP):
    <https://projects.blender.org/blender/blender/pulls/104561>
  - Refactor selecting linked faces in weight paint mode:
    <https://projects.blender.org/blender/blender/pulls/104577>

Migrating things to Gitea

  - yscale refactor for dope sheet:
    <https://projects.blender.org/blender/blender/pulls/104500>
  - fix for dope sheet channel box select:
    <https://projects.blender.org/blender/blender/pulls/104505>
  - paste keyframes with a value offset:
    <https://projects.blender.org/blender/blender/pulls/104512>
  - clamp v2d so keyframes cannot go offscreen:
    <https://projects.blender.org/blender/blender/pulls/104516>
  - add frame channels operator:
    <https://projects.blender.org/blender/blender/pulls/104523>
  - popup menu for graph editor sliders:
    <https://projects.blender.org/blender/blender/pulls/104530>
  - move graph editor settings to preferences:
    <https://projects.blender.org/blender/blender/pulls/104532>

## Feb 02-03 2023

Submitted Patches

  - <https://developer.blender.org/D17182>
  - <https://developer.blender.org/D17192>
  - <https://developer.blender.org/D17197>

Landed Patches

  - <https://developer.blender.org/D17061>
  - <https://developer.blender.org/D16848>

## Jan 26-27 2023

Submitted Patches

  - <https://developer.blender.org/D17126>
  - <https://developer.blender.org/D17130>
  - <https://developer.blender.org/D17138>

Updated Patches

  - <https://developer.blender.org/D17065>

## Jan 19-20 2023

I spent my 2 days mostly working on battling papercuts with the
animation editors as listed in this design doc:
<https://developer.blender.org/T103855>

Updated Patches

  - <https://developer.blender.org/D17005>
  - <https://developer.blender.org/D16848>

Submitted Patches

  - <https://developer.blender.org/D17061>
  - <https://developer.blender.org/D17065>
  - <https://developer.blender.org/D17066>

Landed Patches

  - <https://developer.blender.org/D16987>

## Jan 12-13 2023

Added recent changes/additions to the Release Notes:
<https://wiki.blender.org/wiki/Reference/Release_Notes/3.5/Animation_Rigging>

Added a design task for animation editors paper cuts:
<https://developer.blender.org/T103855>

Updated Patches

  - <https://developer.blender.org/D16848>

Submitted Patches

  - <https://developer.blender.org/D16987>

Landed Patches

  - <https://developer.blender.org/D16937>
  - <https://developer.blender.org/D16982>

## Jan 5-6 2023

Working Thursday and Friday from now on.

Abandoned Patches

  - <https://developer.blender.org/D9477> (other user has taken over)

Submitted Patches

  - <https://developer.blender.org/D16925>

Updated Patches

  - <https://developer.blender.org/D16848>

Landed Patches

  - <https://developer.blender.org/D16654>
  - <https://developer.blender.org/D9479>
  - <https://developer.blender.org/D16771>
  - <https://developer.blender.org/D16651>

## Dez 29-30 2022

Holiday week

## Dez 21-22 2022

Todo:

  - based on Sybren's feedback on D9479 implement functions for the
    slider GUI to set min and max

Submitted Patches:

  - <https://developer.blender.org/D16848>

Updated Patches

  - \-

Landed Patches

  - *<https://developer.blender.org/D9479>* (actually landed in January)

## Dez 14-15 2022

Looked at
<https://web.archive.org/web/20190222092630id_/http://pdfs.semanticscholar.org/346b/fa0b3cdc11b3a6f1ef4e6a296b1edbd0c215.pdf>
for more curve filtering algorithms. Submitted Patches:

  - <https://developer.blender.org/D16771>
  - <https://developer.blender.org/D16780>
  - <https://developer.blender.org/D16778> - new Gauss based FCurve
    smoothing operator

Updated Patches:

  - <https://developer.blender.org/D16654>
  - <https://developer.blender.org/D16651>

TODO:

  - DONE - Remove "While Held" option from \`Pose Propagate\` operator
  - DONE - Finish D16654
  - get D16671 into a reviewable state

## Dez 7-8 2022

Looked into why \`ED\_keylist\_find\_next\` does sometimes not return
the next keyframe and how the use of \`BEZT\_BINARYSEARCH\_THRESH\` can
be reduced.

Submitted patches

  - <https://developer.blender.org/D16723>

Updated patches

  - <https://developer.blender.org/D16654>

## Nov 30-31 2022

Worked on a new smoothing filter for FCurves using the Butterworth
algorithm. The result will be discussed during the A\&R module. I have
my doubts about it because the impulse response of the filter has
ripples, which makes it sub-optimal for filtering out sudden changes in
key values The patch can be found here:
<https://developer.blender.org/D16671>

Submitted patches

  - <https://developer.blender.org/D16654>
  - <https://developer.blender.org/D16651>

Updated patches

  - <https://developer.blender.org/D9479>

Landed:

  - <https://developer.blender.org/D16593>

## Nov 23-24 2022

Working through the bug reports on the Animation\&Rigging board to:

  - remove duplicates
  - check if they are still valid
  - evaluate if they would be fixed by the planned changes for the
    Animation 2025 project

Submitted 2 patches: <https://developer.blender.org/D16593>
<https://developer.blender.org/D16595>
