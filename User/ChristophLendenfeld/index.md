![../../images/Christoph\_Lendenfeld.jpg](../../images/Christoph_Lendenfeld.jpg
"../../images/Christoph_Lendenfeld.jpg")

## Personal Info

  -   
    **Name:** Christoph Lendenfeld
    **Email:** chris.lenden@gmail.com
    **WWW:** [christoph.lendenfeld.at](http://christoph.lendenfeld.at/)
    **LinkedIn:**
    [christophlendenfeld](https://www.linkedin.com/in/christophlendenfeld/)
    **Twitter:** [@vieleanimations](https://twitter.com/vieleanimations)

<!-- end list -->

  -   
    **on developer.blender.org:**
    [ChrisLend](https://developer.blender.org/p/ChrisLend/)
    **on blender.chat:**
    [ChristophLendenfeld](https://blender.chat/direct/ChristophLendenfeld)

<!-- end list -->

  -   
    **Weekly Reports:** [Reports
    Page](https://wiki.blender.org/wiki/User:ChristophLendenfeld/WeeklyReports)

## About me

I'm an Animation/PipelineTD and Blender developer.

I studied at the lovely University of Applied Sciences Hagenberg, in
rural Austria. The bachelor's course taught me the basics of code and in
the master's course I was able to focus on Animation and Pipeline work.

With this blend of skills and a lot of time spent at home due to the
pandemic I started my journey as a Blender developer. Having never
touched C with faded knownledge of C++ it was a bit of a struggle at
first but I persevered. I am mostly active in the Animation\&Rigging
module, since that's the part of Blender I use most.
