# Julian Eisel

-----

## Reports

Weekly reports on work done while under Blender Institute contract:

  - [2023](Reports/2023.md)
  - [2022](Reports/2022.md)
  - [2021](Reports/2021.md)
  - [March - December 2020](Reports/2020.md)
  - [September 2019 - Mid-March
    2020](Reports/Institute_2019.md)

## Google Summer of Code

  - 2016: [Layer
    Manager](https://archive.blender.org/wiki/index.php/User:Julianeisel/GSoC-2016/)
  - 2019: [Core Support of Virtual Reality Headsets through
    OpenXR](GSoC-2019/index.md)

## Documentation

Documentation for ongoing projects.

  - [Asset Browser - How to
    test](Asset_Browser/How_to_Test.md)
  - [User:Severin/Technical\_Documentation\_Template - Technical
    Documentation
    Template](User:Severin/Technical_Documentation_Template_-_Technical_Documentation_Template)
