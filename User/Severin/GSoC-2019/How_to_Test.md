# Core Support of Virtual Reality Headsets through OpenXR

## How to Test

<table>
<tbody>
<tr class="odd">
<td><div class="note_title">
<p><strong>Only for preservation purposes!</strong></p>
</div>
<div class="note_content">
<p>Information on this page was replaced by a new guide on the manual: <a href="https://docs.blender.org/manual/en/2.83/getting_started/configuration/hardware.html#getting-started">https://docs.blender.org/manual/en/2.83/getting_started/configuration/hardware.html#getting-started</a>.</p>
</div></td>
</tr>
</tbody>
</table>

The following are introductions for testing the
\`vr\_scene\_inspection\` branch with HMDs (head mounted displays)
supported by available OpenXR platforms. Blender itself doesn't drive
the device; it communicates with installed device drivers (very loosely
speaking) via the new OpenXR specification interface. A little bit of
setup is needed for this, explained in the following.

### Grab a Build

Windows builds are uploaded regularly to Graphicall:
<https://blender.community/c/graphicall/3dbbbc/>  
The only Linux compatible runtime is Monado - which isn't ready for user
testing really. If you still want to use Linux, you'll have to compile
both Blender and Monado by yourself.

<div id="about_compiling">

<div class="card">

<div class="card-header" id="heading_about_compiling">

<div class="btn btn-link" data-toggle="collapse" data-target="#collapse_about_compiling" aria-expanded="true" aria-controls="collapse_about_compiling">

Compiling the Branch yourself? Read This\!

</div>

</div>

<div id="collapse_about_compiling" class="collapse" aria-labelledby="heading_about_compiling" data-parent="#about_compiling">

<div class="card-body">

If you want to compile the branch yourself, you'll have to make sure the
OpenXR-SDK dependency is installed correctly. On Windows it's already
bundled with the precompiled libraries, so simply updating these will
get you there (e.g. \`svn up\` in
\`C:\\blender-git\\lib\\win64\_vc14\`). On Linux, \`install\_deps.sh\`
can be used as usual.

Also checkout the \`soc-2019-openxr\` Add-ons branch (e.g. \`git
checkout soc-2019-openxr\` in
\`C:\\blender-git\\blender\\release\\scripts\\addons\\\`).

</div>

</div>

</div>

</div>

\_\_NOEDITSECTION\_\_

### First: Choose a Runtime

<div id="about_oxr_runtime">

<div class="card">

<div class="card-header" id="heading_about_oxr_runtime">

<div class="btn btn-link" data-toggle="collapse" data-target="#collapse_about_oxr_runtime" aria-expanded="true" aria-controls="collapse_about_oxr_runtime">

What is an OpenXR runtime?

</div>

</div>

<div id="collapse_about_oxr_runtime" class="collapse" aria-labelledby="heading_about_oxr_runtime" data-parent="#about_oxr_runtime">

<div class="card-body">

To use your HMD with Blender - or any OpenXR application in fact - an
OpenXR runtime is required. This is the part that actually implements
the OpenXR specification, much like a graphics driver implements the
OpenGL specification. It may help to think about the runtime as the
device driver, although it's a higher level concept.

Given that OpenXR is quite new, runtimes for it are still in early-ish
development too. So don't expect production ready software\! Getting
them up and running will probably become even easier in the future.

</div>

</div>

</div>

</div>

\_\_NOEDITSECTION\_\_

There are three runtimes with OpenXR support available:

  - [Windows Mixed
    Reality](https://www.microsoft.com/en/windows/windows-mixed-reality)
    (WinMR) Runtime.
  - [Oculus Runtime](https://www.oculus.com/setup/) (Windows only, not
    officially released yet)  
  - [Monado](https://monado.dev/)  
    Free and Open Source VR platform with an OpenXr runtime. No binaries
    available, has to be compiled for testing.

If you have a WinMR or Oculus HMD, your choice is obvious. If you run
Linux and feel adventurous, you can try compiling Monado.

#### Windows Mixed Reality

![../../../images/Blender\_OpenXR\_Windows\_Mixed\_Reality.png](../../../images/Blender_OpenXR_Windows_Mixed_Reality.png
"../../../images/Blender_OpenXR_Windows_Mixed_Reality.png")

To use the Windows Mixed Reality platform with OpenXR, make sure you
have recent Windows updates installed. More specifically, the October
2018 Update (1809) or newer needs to be installed.  
All you have to do is install the [Mixed Reality OpenXR Developer
Portal](https://www.microsoft.com/en-us/p/mixed-reality-openxr-developer-preview/9n5cvvl23qbt?activetab=pivot:overviewtab),
launch it and press "Set as active runtime".  
(For the October 2018 Update, an additional *Compatibility Pack* has to
be installed too.  
For more information and detailed steps, refer to
<https://docs.microsoft.com/en-us/windows/mixed-reality/openxr>.)

  

#### Oculus (Windows only)

![../../../images/Blender\_OpenXR\_Oculus\_Runtime.jpg](../../../images/Blender_OpenXR_Oculus_Runtime.jpg
"../../../images/Blender_OpenXR_Oculus_Runtime.jpg")

While Oculus doesn't yet consider their OpenXR support ready to be
considered part of their feature set, you can use their in development
support. All you have to do is [install the Oculus
app](https://www.oculus.com/setup/), and launch Blender through the
\`blender\_oculus.cmd\` script, located next to the \`blender.exe\` in
Windows builds of the branch.

  

### Enable the Blender VR Add-on

We decided to not expose the VR features in Blender by default for now.
An Add-on was created that has to be enabled to use VR.

(If you compiled Blender yourself, make sure that the Add-ons sub-module
has the \`soc-2019-openxr\` branch checked out too.)

Then it's just a matter of enabling the *Basic VR Viewer* Add-on from
<span class="literal">Preferences</span> »
<span class="literal">Add-ons</span>

### Start a Blender VR Session

![../../../images/VR\_session\_panel.png](../../../images/VR_session_panel.png
"../../../images/VR_session_panel.png")

With a runtime installed and set up, a VR session can be launched in
Blender through <span class="literal">Window</span> »
<span class="literal">Toggle VR Session</span>. To end it, use the same
button or close the runtime (e.g. by closing the Mixed Reality Portal
when using the Windows Mixed Reality runtime).

That is it\! All you can do for now is inspect the scene from the active
camera in VR. Of couse, this is just the start, by no means the end of
it.

  

### Feedback and Bug Reports

I hope to get feedback to iron out quirks, bugs and to make sure usage
is as users would expect it. The best place for this is the [devtalk
project
thread](https://devtalk.blender.org/t/gsoc-2019-core-support-of-virtual-reality-headsets-through-openxr/7614/17).
Note that there are some known issues already, see
[\#67083](http://developer.blender.org/T67083). Don't hesitate to
contact me directly either.  
Please don't create a normal bug report, this is not a *master* project.

In case you want to troubleshoot problems yourself, the new
\`--debug-xr\` commandline option for Blender gives rather detailed
debugging output.
