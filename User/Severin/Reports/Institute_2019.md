# Weekly Reports: September - Mid-March 2020

## March 2 - March 8

Focus of the week was getting the first VR patches into master. In fact,
the first two landed, review for the next ones is in progress and
looking good\!

  - VR:
      - Addressed points in the VR patches from review
        ([6cceecb82f](https://projects.blender.org/blender/blender/commit/6cceecb82f6c),
        [9b5a1edbc4](https://projects.blender.org/blender/blender/commit/9b5a1edbc4ac),
        [65ceb8ee5d](https://projects.blender.org/blender/blender/commit/65ceb8ee5de7),
        [6fda295034](https://projects.blender.org/blender/blender/commit/6fda295034af),
        [49f7848589](https://projects.blender.org/blender/blender/commit/49f78485896a),
        [202623419e](https://projects.blender.org/blender/blender/commit/202623419efc),
        [3e41ea03be](https://projects.blender.org/blender/blender/commit/3e41ea03be8d),
        [a74af5f6f0](https://projects.blender.org/blender/blender/commit/a74af5f6f0e5),
        [6f977c5e6d](https://projects.blender.org/blender/blender/commit/6f977c5e6d8d)).
      - First two VR patches landed in master, the build-system changes
        for the OpenXR-SDK dependency and the DirectX compatibility
        layer
        ([a22573e243](https://projects.blender.org/blender/blender/commit/a22573e243d7),
        [3b1ef223ba](https://projects.blender.org/blender/blender/commit/3b1ef223bad5)).
      - Created a Wiki page with [notes on updating the OpenXR-SDK
        dependency](../../../Source/OpenXR_SDK_Dependency.md).
      - (Mostly) finished the VR mirror feature
        ([25b3287ce6](https://projects.blender.org/blender/blender/commit/25b3287ce604)).
      - Brought back vertically mirrored drawing for DirectX
        compatibility
        ([be7b41df3f](https://projects.blender.org/blender/blender/commit/be7b41df3ff4)).
  - Split task for UI code quality improvements
    ([\#73781](http://developer.blender.org/T73781)) into multiple ones:
    [\#74430](http://developer.blender.org/T74430),
    [\#74429](http://developer.blender.org/T74429),
    [\#74432](http://developer.blender.org/T74432)
  - Added Windows support for
    [D5153](http://developer.blender.org/D5153): Event system & keymap
    support for key detecting/ignoring key repeat events
  - Tested and gave more fedback on Sculpt Face-Sets patch
    ([D6070](http://developer.blender.org/D6070)). There was also some
    back and forth on design questions, but we ended up accepting it.

Code cleanup day:

  - Rename ARegion variables from ar to region
    ([b2ee1770d4](https://projects.blender.org/blender/blender/commit/b2ee1770d4c3),
    [\#74432](http://developer.blender.org/T74432)).
  - Reduce context usage in UI functions
    ([d5572eacc5](https://projects.blender.org/blender/blender/commit/d5572eacc595),
    [\#74429](http://developer.blender.org/T74429)).

### Next Week

The idea is that I focus on getting the VR patches in before Bcon1 ends.
I can work on further fixes and improvements for milestone 1 in Bcon2
then.

  - Get remaining VR patches into master\!
  - If that doesn't take up all my time:
      - Patch review
      - Work on the remaining tasks for the first VR milestone
        ([\#71347](http://developer.blender.org/T71347)).

## February 24 - March 1

Most time went to VR feature polishing and patch review.

  - VR:
      - Got an initial working version of the "VR Mirror" working (show
        the VR output in a regular, interactive 3D View). Will finish
        and commit next week.
      - Rename "VR Pose Bookmarks" -\> "VR Landmarks"
        ([a227278065](https://projects.blender.org/blender/blender/commit/a227278065ed)).
      - Support storing Landmarks in .blend files
        ([8e0198052c](https://projects.blender.org/blender/blender/commit/8e0198052c50)).
      - Update OpenXR SDK dependency to version 1.0.6
        ([af00df3469](https://projects.blender.org/blender/blender/commit/af00df346943)).
      - Updated the patch for OpenXR SDK build system changes
        ([D6188](http://developer.blender.org/D6188)).
      - Fixed broken stereo rendering with positional tracking disabled
        ([d65ce022b6](https://projects.blender.org/blender/blender/commit/d65ce022b661)).
        Commit isn't big, but took quite some time. Result fixes some
        further issues so I'm quite happy with it.
      - Various fixes
        ([d0d0cc998e](https://projects.blender.org/blender/blender/commit/d0d0cc998e2d),
        [85e5bbe0a0](https://projects.blender.org/blender/blender/commit/85e5bbe0a056),
        [d205f36906](https://projects.blender.org/blender/blender/commit/d205f36906c1)).
  - C++14: The OpenXR-SDK is currently transitioning to C++14, which
    seems like a good moment to investigate if we should do the switch
    too. I spent some time checking the chances for this and it seems
    like we're good to go. Brecht confirmed. Will likely submit a patch
    once I confirmed it works on common systems (installed some virtual
    machines to test this).

Review:

  - [D6679](http://developer.blender.org/D6679) \[requested changes\]:
    Fix T60682: adds macOS alias redirection for directories.
  - [D6877](http://developer.blender.org/D6877) \[accepted &
    committed\]: Add theme option for time markers line
    ([a4a1074f3d](https://projects.blender.org/blender/blender/commit/a4a1074f3d24)).
  - [D834](http://developer.blender.org/D834) \[rejected\]: Don't close
    menus when clicking on checkboxes.
  - [D6070](http://developer.blender.org/D6070) \[requested changes\]:
    Sculpt Face Sets.
  - [D1514](http://developer.blender.org/D1514) \[requested changes\]:
    Eyedropper Color Preview.
  - [D6941](http://developer.blender.org/D6941) \[accepted\]: File
    Browser: Add Ctrl+F shortcut to activate filter textbox.
  - [D6961](http://developer.blender.org/D6961) \[accepted\]: Theming
    shadows of nodes.
  - [D6942](http://developer.blender.org/D6942) \[accepted &
    committed\]: Fix T73587: Wrong sub-panel animation on double-click
    ([8e8b4ec3a3](https://projects.blender.org/blender/blender/commit/8e8b4ec3a320)).
  - [D6791](http://developer.blender.org/D6791) \[accepted\]: Theming
    colors and size of background (transparency) checkerboard pattern

### Next Week

Same as last week:

  - Try to get first VR patches reviewed, approved and merged.
  - Try to finish remaining tasks for the first VR milestone
    ([\#71347](http://developer.blender.org/T71347)).
  - Tracker Curfew: Patch review and bug fixing

## February 17 - February 23

  - William Reynish came to Amsterdam for a two day UI sprint. Main
    topics we discussed were:
      - "Everything Nodes", mainly Particle Nodes
      - Asset Manager, see updates in
        [\#73366](http://developer.blender.org/T73366).
      - Organizational topics (design review, patch review, how to
        handle UI meetings, UI development resources, etc.)
      - Current projects (sculpt face-maps, attribute painting, brush
        management)
  - VR:
      - Merged master into the VR branches, which broke quite some stuff
        because of the viewport color-management changes in master.
        Addressed these issues
        ([8d5996595f](https://projects.blender.org/blender/blender/commit/8d5996595fb0),
        [8dd6f89a5d](https://projects.blender.org/blender/blender/commit/8dd6f89a5d14),
        [63d34abc0f](https://projects.blender.org/blender/blender/commit/63d34abc0f3a)).
      - I was able to do some improvements and minor optimizations due
        to this work
        ([6dbe2d781d](https://projects.blender.org/blender/blender/commit/6dbe2d781d53),
        [27bbbc9c57](https://projects.blender.org/blender/blender/commit/27bbbc9c575a)).
      - This was unexpected work, so I didn't get to spend time on the
        first VR milestone features as planned.
  - Investigated a big, continuous memory leak an artist had in the
    studio here. Turns out this is a known issue with Grease Pencil
    ([\#71867](http://developer.blender.org/T71867)) and is addressed in
    the Grease Pencil refactor branch. Helped the artist with a
    workaround.
  - RNA: Fail makesrna if enum identifiers contain spaces
    ([03a4d3c33f](https://projects.blender.org/blender/blender/commit/03a4d3c33f82)).

### Next Week

  - Try to get first VR patches reviewed, approved and merged.
  - Try to finish remaining tasks for the first VR milestone
    ([\#71347](http://developer.blender.org/T71347)).
  - Tracker Curfew: Patch review and bug fixing

## January 10 - February 16

As expected, had to take two more days off to deal with university stuff
again, things are back to normal now :) Worked mostly on finishing
existing features for the first VR milestone
([\#71347](http://developer.blender.org/T71347)). I think the project is
approaching the final stretch.

  - UI: Allow gizmo-only redraw tagging
    ([D6838](http://developer.blender.org/D6838),
    [c4b9cb0af0](https://projects.blender.org/blender/blender/commit/c4b9cb0af0ae)).
  - Created task for UI code improvements: Code Quality Improvements -
    Low-Hanging Fruits to be Discussed
    ([\#73781](http://developer.blender.org/T73781))
  - VR:
      - Refactored and improved base pose setup
        ([763eb3d016](https://projects.blender.org/blender/blender/commit/763eb3d016fd),
        [34372607de](https://projects.blender.org/blender/blender/commit/34372607de3c)).
      - Finished VR pose bookmarking system, adding more features and
        polishing
        ([78643f0767](https://projects.blender.org/blender/blender/commit/78643f076715),
        [rBA0527b70](https://projects.blender.org/blender/blender-addons/commit/0527b707b308),
        [rBA52d383c](https://projects.blender.org/blender/blender-addons/commit/52d383c574b1),
        [rBA200d349](https://projects.blender.org/blender/blender-addons/commit/200d3499c783)).
      - Force continuous redraws of the VR camera gizmos
        ([e6e81376b7](https://projects.blender.org/blender/blender/commit/e6e81376b7eb),
        [rBAbacfe28](https://projects.blender.org/blender/blender-addons/commit/bacfe2815e87)).
      - Make VR camera gizmo a per 3D view option
        ([rBA6cbec3e](https://projects.blender.org/blender/blender-addons/commit/6cbec3e789a6)).

Fixes:

  - Fix failing assert & uninitialized paint settings in empty scene
    ([9b243b9a53](https://projects.blender.org/blender/blender/commit/9b243b9a53ca)).

### Next Week

  - Try to finish remaining tasks for the first VR milestone
    ([\#71347](http://developer.blender.org/T71347)).
  - William Reynish will be in Amsterdam for two days, so there will be
    lots of UI design meetings.

## February 3 - February 9

  - VR:
      - Got most work done for the VR pose bookmarking system
        ([5d4f8d31a5](https://projects.blender.org/blender/blender/commit/5d4f8d31a5b4),
        [a3c97a49c79f](https://developer.blender.org/rBAa3c97a49c79)).
      - Updated project task to list TODOs for milestone 1
        ([\#68998](http://developer.blender.org/T68998)).
  - Created task to collect known issues with pop-up handling
    ([\#73565](http://developer.blender.org/T73565)).
  - Code quality day:
      - Prepared task to propose low hanging fruit changes to UI code,
        will upload shortly.
      - Made a first patch to reduce \`bContext\` reliance (see
        [P1250](https://developer.blender.org/P1250)). Will discuss this
        with others first.
  - Regular tracker curfew work

Review:

  - [D6470](http://developer.blender.org/D6470) \[requested changes &
    proposed patch\]: Add Free Handle Types to CurveProfile Widget
  - [D5770](http://developer.blender.org/D5770) \[commented\]: Drag
    threshold too sensitive for back/forward mouse interactions.
  - [D6655](http://developer.blender.org/D6655) \[commented\]: UI:
    Highlight Selected Enum.
  - [D6748](http://developer.blender.org/D6748) \[accepted\]: Fix
    T67084: Modal keymaps could show the wrong shortcut.
  - [D5882](http://developer.blender.org/D5882) \[accepted\]: UI:
    Changes to graph editor channels drawing.

## January 27 - February 2

Had to spontaneously take two days off to deal with university stuff.
Most time was spent on various fixes.

  - Decoupled Gizmo redraws:
      - Finished patch
        ([56ae8edcd4](https://projects.blender.org/blender/blender/commit/56ae8edcd45b),
        [3085b94595](https://projects.blender.org/blender/blender/commit/3085b945958b),
        [d2de88f47b](https://projects.blender.org/blender/blender/commit/d2de88f47b7c),
        [f652e38b37](https://projects.blender.org/blender/blender/commit/f652e38b3710))
        and submitted for review
        ([D6685](http://developer.blender.org/D6685)).
      - Turns out Clement was working on some architectural changes that
        are incompatible with my patch. So it will have to be reworked.
  - [\#70457](http://developer.blender.org/T70457): Dopesheet: Avoid
    deselect-all triggering on every box-select
    ([395dfff103](https://projects.blender.org/blender/blender/commit/395dfff103e1)).
  - VR: Support all 3D View shading options for the VR session
    ([e99e914352](https://projects.blender.org/blender/blender/commit/e99e91435212),
    [rBA12686a8](https://projects.blender.org/blender/blender-addons/commit/12686a856dbf))

Review:

  - [D6363](http://developer.blender.org/D6363) \[accepted\]:
    Disfunctional clear button in the Image slot in the Brush panel.
  - [D6707](http://developer.blender.org/D6707) \[accepted\]: Add
    ability to extend or subtract animation channel box selections.
  - [D6708](http://developer.blender.org/D6708) \[accepted\]: Theme
    tweak to animation channels
  - [D6491](http://developer.blender.org/D6491) \[accepted\]: UI: Info
    Editor Changes

Fixes:

  - HUD toggle ("Adjust Last Operation") missing in VSE
    ([82f08cb2b0](https://projects.blender.org/blender/blender/commit/82f08cb2b066)).
  - [\#73428](http://developer.blender.org/T73428): Editor type dropdown
    menu missing in VSE
    ([8a7859b9ad](https://projects.blender.org/blender/blender/commit/8a7859b9adff)).
  - [\#66920](http://developer.blender.org/T66920): Shortcut Bind Camera
    to Marker in timeline not working
    ([6dbc254c0b](https://projects.blender.org/blender/blender/commit/6dbc254c0b3f)).
  - [\#63999](http://developer.blender.org/T63999): Filepath property
    won't open filebrowser from a popover
    ([a5790b2656](https://projects.blender.org/blender/blender/commit/a5790b26563c),
    [7c9b5523ff](https://projects.blender.org/blender/blender/commit/7c9b5523ff4c)).
  - Fix memory leak of NLA child panel-types
    ([7b5f09c068](https://projects.blender.org/blender/blender/commit/7b5f09c06850)).
  - [\#73487](http://developer.blender.org/T73487): Crash when opening
    filebrowser while error is displayed
    ([9cb7ecefce](https://projects.blender.org/blender/blender/commit/9cb7ecefceee)).
  - [\#73453](http://developer.blender.org/T73453): Selecting playhead
    in VSE deselects all selected strips
    ([e3f89237fe](https://projects.blender.org/blender/blender/commit/e3f89237fecc)).

### Next Week

Will likely have to take a few days off again (at some time over the
next two weeks). But generally:

  - Start work on VR location bookmarks (see
    [\#71347](http://developer.blender.org/T71347)).
  - Tracker-curfew: Partially triaging, partially reviewing.

## January 20 - January 26

Reviewed a bunch of patches, mostly small ones, did some regular tracker
work and continued work on decoupled gizmo redraws. The latter is in the
"it works" state now. In effect it resolves flickering from
anti-aliasing and laggy responses with gimzmo interactions:

| Old                                                                                                                                 | New                                                                                                                       |
| ----------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------- |
| ![../../../videos/05\_gizmos\_redraw\_old.mp4](../../../videos/05_gizmos_redraw_old.mp4 "../../../videos/05_gizmos_redraw_old.mp4") | ![../../../videos/gizmos\_redraw\_new.mp4](../../../videos/gizmos_redraw_new.mp4 "../../../videos/gizmos_redraw_new.mp4") |

Review:

  - [D6636](http://developer.blender.org/D6636) \[accepted\]: Adopt
    2.8-style layout for the Graph Editor sidebar.
  - [D6309](http://developer.blender.org/D6309) \[accepted\]: Fix
    T68677: Graph Editor zoom to selected ignores scrollbar occlusion.
  - [D6632](http://developer.blender.org/D6632) \[accepted\]: UI: Change
    'Lock Time to Other Windows' \> 'Sync Visible Range' + add to
    Sequencer.
  - [D6050](http://developer.blender.org/D6050) \[accepted\]: Fix
    [\#T69413](http://developer.blender.org/TT69413): Scroll bar in
    maximized view invisible.
  - [D6657](http://developer.blender.org/D6657) \[accepted\]: Narrower
    Ellipsis for Line Continuation.
  - [D6496](http://developer.blender.org/D6496) \[suggested changes\]:
    Make default Info editor theme more consistent with Outliner.
  - [D6491](http://developer.blender.org/D6491) \[requested changes\]:
    Info Editor Changes.

Fixes:

  - [\#71810](http://developer.blender.org/T71810): Flipping Sidebar
    with tabs breaks alignment
    ([5d69d2a863](https://projects.blender.org/blender/blender/commit/5d69d2a86358)).
  - [\#73191](http://developer.blender.org/T73191): Buttons in lower
    left of Preferences broken
    ([084f072aae](https://projects.blender.org/blender/blender/commit/084f072aae73)).
  - [\#71509](http://developer.blender.org/T71509): Rendering workspace
    has empty tool-header
    ([c68c160e7b](https://projects.blender.org/blender/blender/commit/c68c160e7b4e)).
  - [\#73357](http://developer.blender.org/T73357) -- own mistake in a
    previous commit: Multiple importers fail
    ([b59adcaa36](https://projects.blender.org/blender/blender/commit/b59adcaa3683),
    [47a32a5370](https://projects.blender.org/blender/blender/commit/47a32a5370d3)).

### Next Week

  - Get changes for decoupled gizmo redraws into master.
  - Start work on VR location bookmarks (see
    [\#71347](http://developer.blender.org/T71347)).
  - Tracker-curfew: Partially triaging, partially reviewing.

## January 13 - January 19

  - As mentioned in last week's report, for VR we need a way to
    continuously redraw gizmos without redrawing the entire viewport.
      - Spent time investigating how this could work and checked with
        Jeroen and Clément on integrating this in the draw-manager. See
        [\#73198](http://developer.blender.org/T73198).
      - Worked on the first step for this, that is to have a separate
        redraw tag to signal that only gizmos need redrawing
        ([1053fb9d9d](https://projects.blender.org/blender/blender/commit/1053fb9d9dcc)).
  - Added utilities to sanitize BLI\_rct rectangles
    ([388d43d85a](https://projects.blender.org/blender/blender/commit/388d43d85a5d)),
    which allowed me to catch and fix some issues in our region
    coordinate resolution code
    ([e4bf08a363](https://projects.blender.org/blender/blender/commit/e4bf08a363d9),
    [c167e8ba18](https://projects.blender.org/blender/blender/commit/c167e8ba18d5),
    [758361556b](https://projects.blender.org/blender/blender/commit/758361556b5b)).

Review:

  -   - [D6505](http://developer.blender.org/D6505) \[requested
        changes\]: Scrollbars experiments.
      - [D6604](http://developer.blender.org/D6604) \[requested
        changes\]: Gizmo dial: Draw arc only over one rotation to avoid
        visual artifacts.
      - [D4752](http://developer.blender.org/D4752) \[rejected\]: Region
        Overlap Background Transparent.
      - [D6108](http://developer.blender.org/D6108) \[committed
        alternative\]: Fix T70965: Blender crashes when the undoing in
        certain cases.
      - [D6572](http://developer.blender.org/D6572) \[accepted\]: Fix
        T72803: Texture Paint: Shortcut 'Shift + S' Does Not Save an
        Image.
      - [D6579](http://developer.blender.org/D6579) \[requested changes,
        accepted update\]: Fix T72200: Split Quad View Region Crash.

Rest was spent on general fixes an polish:

  -   - Usability issues
        ([25cb12dc71](https://projects.blender.org/blender/blender/commit/25cb12dc71e8),
        [003be8aa7c](https://projects.blender.org/blender/blender/commit/003be8aa7c6d)).
      - Bug-fixes
        ([ca49643f3c](https://projects.blender.org/blender/blender/commit/ca49643f3c25),
        [eca8bae671](https://projects.blender.org/blender/blender/commit/eca8bae6718d),
        [d52551401e](https://projects.blender.org/blender/blender/commit/d52551401e18),
        [3cd1c8ccff](https://projects.blender.org/blender/blender/commit/3cd1c8ccffec)).
      - Abandoned outdated/redundant patches for OpenHMD support
        ([D2902](http://developer.blender.org/D2902),
        [D2133](http://developer.blender.org/D2133)).

### Next Week

  - Continue work on decoupled gizmo redraws
  - Tracker-curfew: Partially triaging, partially reviewing

## January 6 - January 12

![../../../videos/VR\_view\_camera\_gizmo.webm](../../../videos/VR_view_camera_gizmo.webm
"../../../videos/VR_view_camera_gizmo.webm")

  - (More or less) finished VR camera gizmo, which indicates location
    and rotation of the VR camera, but within a regular 3D View
    ([9b1d318222](https://projects.blender.org/blender/blender/commit/9b1d31822266),
    [b67b4f3a1b](https://projects.blender.org/blender/blender/commit/b67b4f3a1bf5)).  
    What's missing is a way to continuously redraw this widget with
    updated location/rotation, without having to redraw the entire 3D
    View.
  - Tests: Natural string comparing (BLI\_strcasecmp\_natural()) -
    ([525b0e0ccb](https://projects.blender.org/blender/blender/commit/525b0e0ccb08)).
  - Noticed a big-ish memory leak in Mantaflow when just opening and
    closing Blender. Investigated and [submitted pull request
    upstream](https://bitbucket.org/mantaflow/manta/pull-requests/5/fix-big-ish-memory-leaks-when-not-actually/diff).
  - Looked into to further issues that seem fixable, but require a bit
    more work to be finished - will upload my code so far but don't want
    to spend much more time on this:
      - Clicking on opaque Sidebar with *Region Overlap* enabled sends
        events to viewport region, i.e. as if the region was transparent
        ([\#72994](http://developer.blender.org/T72994)).
      - Marker region in VSE overlaps/blocks strips, with no way to
        scroll them back into view.
  - Regular bug tracker, patch tracker and fixing work.

### Next Week

  - Remaining parts for first VR milestone
    ([\#71347](http://developer.blender.org/T71347))
  - Look into decoupling gizmo redraws from viewport redraws, so 3D View
    VR gizmos can be updated without updating the entire 3D View.
  - Curfew: Focus on reviewing new UI patches (many came in over
    holidays)

  

## December 16 - January 5

Holidays.

## December 9 - December 15

## December 2 - December 8

A quite unsatisfying week with little tangible outcome. Focus was on VR.

General:

  - Did a longer feedback session with Julien Kaspar on the tool system,
    based on changes implemented by Campbell. Overall a great
    improvement.
  - To discuss our findings, we also did a long video chat meeting with
    William and Campbell.
  - Based on feedback we got on the Graph Editor select/transform
    changes (see [this
    reply](https://developer.blender.org/T70634#817939)), I was looking
    into making box selection work as desired. While this should be
    trivial to do for other editors, the Graph Editor is especially
    challenging again. Issue is how the selection and the visibility of
    handles is linked, and how operations are performed on the curve
    points. Spent some hours trying to get this to work without bigger
    changes, but figured it wasn't worth that much time.
  - Again, quite some time went into general meetings, mails,
    discussions, etc.

VR:

  - Started the week with some research on how other apps, especially
    game engines handle VR specific setup. E.g.: How do they define the
    starting coordinate and orientation of a session? How do they
    determine the starting height so that it matches the physical floor?
    How do they deal with 3DoF vs. 6DoF display, etc. I've never done
    any VR game like programming before, so I still had to get familiar
    with these things.
  - Although this was quite informative, none of their solutions could
    simply be applied to Blender. Their workflows are too different than
    what we need in Blender.
  - Continued work on the positional tracking (3DoF) option. Given the
    way we want this to work (see
    [\#71347](http://developer.blender.org/T71347)), I had to extend the
    Ghost-XR API a bit. For some reason this still doesn't want to
    behave like it should, which is an unsolved issue.
  - To check if this issue is caused by a misunderstanding of the OpenXR
    specification on my side, I compiled the OpenXR SDK testing examples
    again. This turned out to be a hassle due to a MSVC update and then
    I had issues with OpenXR in general...

Review:

  - [D5802](http://developer.blender.org/D5802) \[found issues, accepted
    once addressed\]: File Browser Volumes and System Lists Icons
  - [D5107](http://developer.blender.org/D5107) \[accepted\]: Win32 -
    Restore Minimized App On WM\_Close

## November 25 - December 1

Tried to do some more VR related work, although various other things
kept me busy.

  - Smaller, general UI tasks
    ([85cf56ecbc](https://projects.blender.org/blender/blender/commit/85cf56ecbc62),
    [5bcb0c9935](https://projects.blender.org/blender/blender/commit/5bcb0c993503)).
  - Went through [UI todo list](https://developer.blender.org/T55364) to
    check which items can be marked as done. Took quite some effort to
    dive into individual tasks and check status.
  - General tracker work.
  - LANPR: Got asked to help resolving file read & write issues. Found
    and fixed a couple of general issues:
      - Various memory usage bugs
        ([4227b81826](https://projects.blender.org/blender/blender/commit/4227b8182625),
        [6ccd672fee](https://projects.blender.org/blender/blender/commit/6ccd672feedb),
        [cf38c4d49f](https://projects.blender.org/blender/blender/commit/cf38c4d49f0e)),
        also seem to have caused the file read & write issues.
      - General cleanup, warnings
        ([692f20604c](https://projects.blender.org/blender/blender/commit/692f20604c41),
        [cf623d8e7b](https://projects.blender.org/blender/blender/commit/cf623d8e7b8b))
        and small changes
        ([541fb672ec](https://projects.blender.org/blender/blender/commit/541fb672ec4a)).
  - VR:
      - Sebastian König wrote down some design requirements & ideas for
        the first milestone: scene inspection
        ([\#71347](http://developer.blender.org/T71347)). I investigated
        feasibility and replied with some technical considerations.
        Seems mostly reasonable and feasible though.
      - General fixes for Windows related changes in the SDK and the
        Mixed Reality platform
        ([0c7ac28b8a](https://projects.blender.org/blender/blender/commit/0c7ac28b8a94),
        [06138de13a](https://projects.blender.org/blender/blender/commit/06138de13adf)).
      - Implemented option for positional tracking, but figured for the
        scene inspection use-case it will need to work in a bit more
        advanced way.

Review:

  - [D4339](http://developer.blender.org/D4339): Window manager: window
    size/position persistence
  - [D6295](http://developer.blender.org/D6295) \[accepted\]: UI: Widget
    Text Cursor Color
  - [D6290](http://developer.blender.org/D6290) \[rejected\]: UI: Move
    workspaces left-right instead of front-back
  - [D6112](http://developer.blender.org/D6112) \[accepted\]: Fix
    T69530: Remove Lag in File Browser
  - [D6328](http://developer.blender.org/D6328) \[raised concerns on UI
    decisions, to be discussed further\]: GPencil: Add Opacity y Onion
    switch to Dopesheet

### Next Week

Over the last weeks, too much of my time was spent on general
responsibilities (bug tracker, patch review, documentation, meetings,
etc.). Too few was left for important projects that I have
responsibilities in (e.g. VR and asset management). I'll have to find
ways to manage this time better. So next week I'll try to focus on VR,
in the hope that I can move that forward quite a bit. I might neglect
other, general responsibilities during that, but not for too long.

## November 18 - November 24

  - Tried to finish updates for File Browser manual, but had trouble
    with the Sphinx theme so still didn't get to finish it.
  - Updated the [UI release notes
    page](../../../Release_Notes/2.81/UI.md) to include missing
    changes and to add more screenshots.
      - Also compiled a complete list of shortcut changes for the
        release notes page.
  - "Commandeered" and painfully updated Jacques' patch to rewrite the
    window-manager drag & drop system
    ([D4071](http://developer.blender.org/D4071)). We'll likely need
    this for the asset manager.
  - Pushed the Graph Editor changes that I mentioned in earlier reports
    to master
    ([b037ba2665](https://projects.blender.org/blender/blender/commit/b037ba2665f4)).

Review:

  - [D6237](http://developer.blender.org/D6237) \[accepted\]: Graph
    Editor: Fix selection and View \> Selected with 'View Only Selected
    Curve Keyframes' option enabled.
  - [D6083](http://developer.blender.org/D6083) \[accepted\]: Allow
    deletion of directories in the file browser.
  - Reviewed a good part of the huge LANPR patch -
    [D5442](http://developer.blender.org/D5442).
      - Focused on build-system, file read, write & compatibility, RNA,
        DNA, Python UI
      - Overal patch is okay, although I did find some issues that will
        have to be addressed.
  - [D6295](http://developer.blender.org/D6295) \[accepted\]: UI: Widget
    Text Cursor Color

## November 11 - November 17

  - Started updating the manual for the File Browser changes, unfinished
    and unpushed though.
  - Created patch for Graph Editor selection changes
    ([D6235](http://developer.blender.org/D6235)).
      - Sybren raised concerns about general readability of the code,
        which was already bad before, our proposed changes would make
        things worse. So I put quite some efforts into "leaving the
        campground cleaner than I found it". Updated the patch.
  - Updated patch to introduce OpenXR SDK dependency
    ([D6188](http://developer.blender.org/D6188)).

Fixes:

  - Fix [\#71474](http://developer.blender.org/T71474): Temporary image
    editor cancels to file editor after file operation
    ([b962253ed1](https://projects.blender.org/blender/blender/commit/b962253ed158)).
  - Fix [\#71590](http://developer.blender.org/T71590): Closing file
    browser reopens previously closed render view
    ([96ce32dca6](https://projects.blender.org/blender/blender/commit/96ce32dca6ab)).
  - Fix [\#70991](http://developer.blender.org/T70991): Maximized file
    browser hides file name bar on Windows
    ([f641c60530](https://projects.blender.org/blender/blender/commit/f641c60530fb)).

Review:

  - [D6212](http://developer.blender.org/D6212) \[accepted &
    committed\]: VSE: open file browser sidebar by default when adding
    external strips.
  - [D6234](http://developer.blender.org/D6234) \[requested bigger
    changes\]: Add a filter to the theme preferences.
  - [D6260](http://developer.blender.org/D6260) \[accepted\]: Fix
    filebrowser saving dialog size when maximized.
  - [D6259](http://developer.blender.org/D6259) \[found better fix\]:
    File Explorer: Maximize window no the filename textbox on Windows.

Also (although I'm not sure if this counts to my work under BI
contract):

  - Submitted merge request to fix multiple memory issues in Monado
    (FOSS OpenXR runtime for Linux). Got merged immediately:
    [05b069b0a0](https://gitlab.freedesktop.org/monado/monado/commit/05b069b0a0f8721e5f8).

## November 4 - November 10

Had two days off, work hours were mostly spend on making the OpenXR
branch review more manageable, the graph editor selection changes, as
well as general fixes.

  - Split up changes from my soc-2019-openxr GSoC branch into multiple,
    more manageable patches
    ([D6188](http://developer.blender.org/D6188),
    [D6190](http://developer.blender.org/D6190),
    [D6192](http://developer.blender.org/D6192),
    [D6193](http://developer.blender.org/D6193)). Also created a parent
    task for the review
    ([\#71365](http://developer.blender.org/T71365)).
  - Finished rewrite of temporary fullscreen logic
    ([ef7fd50f8a](https://projects.blender.org/blender/blender/commit/ef7fd50f8a93)).
  - Did more tweaks and fixes to the graph editor selection, based on
    animator feedback
    ([1d3da5aa84](https://projects.blender.org/blender/blender/commit/1d3da5aa84bc),
    [2779d0328f](https://projects.blender.org/blender/blender/commit/2779d0328fa8),
    [d5d2a10056](https://projects.blender.org/blender/blender/commit/d5d2a1005688)).
    Need to check with them again, if there are no new issues, this
    should be ready to go into master.
  - Fix broken selection behavior in multiple editors right after saving
    ([3ed4097292](https://projects.blender.org/blender/blender/commit/3ed40972920d)).

## October 28 - November 3

Most time was spent on meetings, Graph Editor selection/transform
changes for [\#57918](http://developer.blender.org/T57918) and general
fixes/tweaks.

  - Wrote down notes from BConf VR meeting and [sent them to
    bf-committers](https://lists.blender.org/pipermail/bf-committers/2019-October/050270.html).
  - Fix [\#71019](http://developer.blender.org/T71019): Disappearing
    file thumbnails & crash on area split
    ([7c1fbe24ca](https://projects.blender.org/blender/blender/commit/7c1fbe24ca33)).
  - UI: Only show render result in image editors in view mode
    ([f069f5be7f](https://projects.blender.org/blender/blender/commit/f069f5be7fe5)).
  - Continued work on Graph Editor selection/transform changes
    ([\#70634](http://developer.blender.org/T70634),
    [\#57918](http://developer.blender.org/T57918)):
      - Spend quite some effort make the transform code only transform
        selected handles of the clicked on side
        ([1ee7405a98](https://projects.blender.org/blender/blender/commit/1ee7405a9881),
        [0a7628d633](https://projects.blender.org/blender/blender/commit/0a7628d63393),
        [0cfee25a10](https://projects.blender.org/blender/blender/commit/0cfee25a100a),
        [ca737350a8](https://projects.blender.org/blender/blender/commit/ca737350a846),
        [19f520780c](https://projects.blender.org/blender/blender/commit/19f520780c28)).
      - Allow deselecting individual handles & keys with ctrl+box select
        ([1ba0bd1d8d](https://projects.blender.org/blender/blender/commit/1ba0bd1d8d4d)).
      - Checked with animators on feedback and made Hjalti use my branch
        for testing. One (easy to fix) issue got pointed out, this
        should be ready for master soon.
  - Spend a while rewriting temporary fullscreen logic to fix a
    regression with stacked, temporary fullscreens. This can probably be
    committed on Monday.

## October 21 - October 27

Had a few days off, directly followed by the conference.

## October 14 - October 20

Did a bit of general UI development/maintenance and quite some work for
VR, mainly addressing review points.  
Worth mentioning: Bastien arrived in Amsterdam where he will stay for
some weeks, so we did an internal meeting with Pablo and Andy on the
asset manager project. Plan is that I work on the UI side of it,
hopefully starting after the conference.

Note that I've taken some days off until the conference for some "family
business".

  - Fix [\#70581](http://developer.blender.org/T70581): Node Wrangler
    output switching broken
    ([80fe0ac7ff](https://projects.blender.org/blender/blender/commit/80fe0ac7ff8c)).
  - File Browser: Add F2 shortcut to rename files
    ([5edfde58fe](https://projects.blender.org/blender/blender/commit/5edfde58fe60)).
  - UI: Add renaming to Node and VSE strip context menu
    ([760481518b](https://projects.blender.org/blender/blender/commit/760481518b8d)).
  - Fix: First item in File Browser can't be renamed
    ([9a85592dde](https://projects.blender.org/blender/blender/commit/9a85592ddea2)).
  - Fix [\#70815](http://developer.blender.org/T70815): Missing tool
    settings redraw when using Annotate Tool
    ([4d3a317258](https://projects.blender.org/blender/blender/commit/4d3a3172582d)).
  - Fix: Some ID-Filters not enabled on Link/Append
    ([a6b9e1dfdb](https://projects.blender.org/blender/blender/commit/a6b9e1dfdbf7)).
  - UI: Add missing workspace icon to link/append ID-Filters
    ([a3f7ae1b96](https://projects.blender.org/blender/blender/commit/a3f7ae1b9682)).
  - UI: Remember ID-Filter in-between File Browser calls
    ([b546263642](https://projects.blender.org/blender/blender/commit/b54626364253)).
  - FBX IO: Bring back experimental hint for apply transform option
    ([90c32d2957](https://projects.blender.org/blender/blender/commit/90c32d295772)).

### VR

  - Support VR Session settings & add some basic ones
    ([1ada9f5bf9](https://projects.blender.org/blender/blender/commit/1ada9f5bf95d),
    [752b4989d6](https://projects.blender.org/blender/blender/commit/752b4989d675)).
      - Adds the needed bits to support VR session settings and access
        them in the VR view drawing code.
      - Added settings for: shading mode, grid floor, annotations,
        clipping. More can easily be added.
      - The Add-on adds a "VR" tab in the side bar, containing buttons
        for the added settings.
  - General minor improvements & cleanup
    ([fa9a841d43](https://projects.blender.org/blender/blender/commit/fa9a841d4361),
    [927c300c02](https://projects.blender.org/blender/blender/commit/927c300c0221),
    [51af7510fb](https://projects.blender.org/blender/blender/commit/51af7510fba6),
    [350b7b378d](https://projects.blender.org/blender/blender/commit/350b7b378d8b),
    [3bed8a7338](https://projects.blender.org/blender/blender/commit/3bed8a73384d)).
  - Fix too dark rendering on Monado runtime
    ([0615321ca0](https://projects.blender.org/blender/blender/commit/0615321ca085)).
    [Commented on the patch](https://developer.blender.org/D5537#139996)
    with more info on this.
  - Updated [D5537](http://developer.blender.org/D5537): GSoC 2019: Core
    Support of Virtual Reality Headsets through OpenXR

## October 7 - October 13

Finally, I was able to focus on to topics not related to the File
Browser. So I worked on general UI topics, but was also able to do some
further testing for [my OpenXR GSoC
branch](../GSoC-2019/Final_Report.md). I finally got
it to work with the FOSS Monado OpenXR runtime\! That means I can now
test VR work on Linux.

  - Tested & accepted [D4585](http://developer.blender.org/D4585): Move
    files to OS recycling bin when deleted in file explorer.
  - UI: Move all Selected NLA-Strips when Dragging
    ([7dea058546](https://projects.blender.org/blender/blender/commit/7dea0585468d)).
  - Fix [\#70522](http://developer.blender.org/T70522): Sidebars in Clip
    Editor work incorrectly in Dopesheet mode
    ([95749f5d54](https://projects.blender.org/blender/blender/commit/95749f5d548c)).
  - Fix: region toggle operator being able to toggle regions it
    shouldn't
    ([b1f1c8c33f](https://projects.blender.org/blender/blender/commit/b1f1c8c33fab)).
  - Created [\#70634](http://developer.blender.org/T70634) (design
    task): Graph Editor: Reevaluate Key and Handle Selection Behavior.  
    Committed some changes to a new branch for experimenting with a
    possible solution:
      - Always move handles with key, regardless of selection
        ([6adb4001ff](https://projects.blender.org/blender/blender/commit/6adb4001ff51)).
      - Add drag-all-selected support
        ([961937a568](https://projects.blender.org/blender/blender/commit/961937a56821)).
      - Don't change selection of handles when clicking just on key
        ([76eecb97cc](https://projects.blender.org/blender/blender/commit/76eecb97cc62)).
      - Make handle drag and selection consistent with other cases
        ([2da911e6c0](https://projects.blender.org/blender/blender/commit/2da911e6c06b)).
  - Created & committed [D6021](http://developer.blender.org/D6021):
    Transform Manipulator: Only hide locked axis if lock and manipulator
    space match
    ([1857aa32bd](https://projects.blender.org/blender/blender/commit/1857aa32bd3b)).
  - Fix: Manipulator visible if root or tip of locked bone is selected
    ([7c88d84515](https://projects.blender.org/blender/blender/commit/7c88d845157e)).
  - Fix missing manipulator update when toggling bone lock
    ([cf192bdd43](https://projects.blender.org/blender/blender/commit/cf192bdd43b8)).
  - Created quick hack [\#70730](http://developer.blender.org/T70730):
    VSE: "Include Handles" option for Box Select.
  - VR: Strictly follow specification to get OpenXR extension functions
    ([941e1f5a98](https://projects.blender.org/blender/blender/commit/941e1f5a9875)).

## September 30 - October 6

Did some more or less final tweaks for the File Browser for 2.81, but
moved on to general UI tasks this week. Mostly, addressing the remaining
points in [\#57918](http://developer.blender.org/T57918).

  - Reviewed & updated [D4585](http://developer.blender.org/D4585): Move
    files to OS recycling bin when deleted in file explorer.
  - Reviewed & updated [D5153](http://developer.blender.org/D5153):
    Event system & keymap support for key detecting/ignoring key repeat
    events.
  - UI: Remember File Browser Display Options in Preferences
    ([ddb157999e](https://projects.blender.org/blender/blender/commit/ddb157999eed)).
  - UI: Register File Browser as Child/Dialog-Window for the OS
    ([edffb0e9b1](https://projects.blender.org/blender/blender/commit/edffb0e9b19d)).
  - Worked on remaining tasks for
    [\#57918](http://developer.blender.org/T57918) *Tweaks & Fixes for
    Improved Left Click Select Support (Parent task)*:
      - Experimented with a different way to implement support for
        click-dragging multiple selected items (VSE strips, markers,
        dopesheet keyframes, ect.), see
        [P1123](https://developer.blender.org/P1123). Ended up going
        with a solution suggested by Brecht.
      - Created [D5979](http://developer.blender.org/D5979): Generic
        drag-all-selected support (Node Editor, VSE, Dopesheet Graph
        Editor, Markers).
      - WM: Utilities for select operators to work with click-dragging
        items
        ([be2cd4bb53](https://projects.blender.org/blender/blender/commit/be2cd4bb5325)).
      - Node Editor: Use new operator utilities for node selection and
        dragging
        ([5f51e78172](https://projects.blender.org/blender/blender/commit/5f51e7817206)).
      - UI: Move all Selected Strips and Strip Handles when Dragging
        ([1f5ae1a5a5](https://projects.blender.org/blender/blender/commit/1f5ae1a5a505)).
      - UI: Move all Selected Dopesheet Keys when Dragging
        ([d4d036ae14](https://projects.blender.org/blender/blender/commit/d4d036ae140a)).
      - UI: Move all Selected Markers when Dragging
        ([809ab298f1](https://projects.blender.org/blender/blender/commit/809ab298f1ca)).
  - Fix [\#70462](http://developer.blender.org/T70462): Shift+Click on
    neighbour folder enters it
    ([db66c33efe](https://projects.blender.org/blender/blender/commit/db66c33efe42)).
  - There's consensus within the UI team to start setting up guidelines
    and documentation on the design vision, rationales and known design
    trade-offs. E.g. the Blender *Human Interface Guidelines* or *Design
    Bible*.
      - I wrote a landing page proposal for this, which includes a
        proposed vision description. It's too early to show this
        publicly, this is not something to take lightly.

## September 23 - September 29

A bit of an ineffective week, spent lots of time experimenting with
things, or fighting the X server trying to make it manage windows the
way we want it to. Once again, this work was mostly for the file
browser.

  - Experimented quite a bit with different ways of storing file browser
    properties like display type or window size, so that this
    information is remembered after the browser is closed. Ideally in
    the Preferences. In consultation with Brecht, I ended up with the
    least versatile, but simplest solution:
      - *\[Submitted and updated twice\]*
        [D5893](http://developer.blender.org/D5893): Remember file
        browser display options in preferences.
  - We'd like to make improvements to the handling of secondary windows
    in general soon, but for the file browser some immediate
    improvements to the multi-window behavior are needed. I spent lots
    of time trying to get enough control over the behavior of X-based
    Linux window managers to get windows to work like we want (see
    [\#69819](http://developer.blender.org/T69819)). Windows also took
    some time, but was less of an issue. Specifically for the file
    browser I've settled on a solution now:
      - *\[Updated with a complete rewrite\]*
        [D5810](http://developer.blender.org/D5810): Let the OS treat
        the file browser as proper dialog window.
  - Keymaps: Don't show confirm prompt when creating directories
    ([f8b57dbb81](https://projects.blender.org/blender/blender/commit/f8b57dbb816c)).
  - Let file browser tool/bookmarks region push upper bar in
    ([57519f237a](https://projects.blender.org/blender/blender/commit/57519f237a91)).
  - Use responive layout for upper bar in file browser
    ([98c0d16da5](https://projects.blender.org/blender/blender/commit/98c0d16da53a)).

Review:

  - [D5871](http://developer.blender.org/D5871): Fix topbar padding to
    fit the tool icons
  - [D5898](http://developer.blender.org/D5898): Fix T70255: Filebrowser
    (python): Setting bookmarks\_active crash

## September 16 - September 22

Once again most time was spent on the file browser, but I'm confident I
can move on to other tasks soon.

### General Usability

  - Show in-/decrement buttons for exporters
    ([8d6b0eda5d](https://projects.blender.org/blender/blender/commit/8d6b0eda5d)).
  - Open file options region for more operations
    ([28ee0f97c3](https://projects.blender.org/blender/blender/commit/28ee0f97c3)).
  - Refactor temp-space opening for optional fullscreen mode
    ([95373e2749](https://projects.blender.org/blender/blender/commit/95373e274908)).
  - Move render display type to Preferences
    ([cac756a92a](https://projects.blender.org/blender/blender/commit/cac756a92aae)).
  - Preference for file browser as fullscreen area
    ([f5bbaf55ac](https://projects.blender.org/blender/blender/commit/f5bbaf55ac3d)).
  - Cleanup: Add/use utility to remove regions
    ([f5dc979a7e](https://projects.blender.org/blender/blender/commit/f5dc979a7eb0)).
  - Rewrite file region handling for non-editor mode
    ([d1cc340e56](https://projects.blender.org/blender/blender/commit/d1cc340e5669)).
  - Refactor: Ensure there's always a valid file editor tool region
    ([b20182e334](https://projects.blender.org/blender/blender/commit/b20182e33418)).
  - Remove redundant file bookmarks region toggle operator
    ([2f1e8f4b97](https://projects.blender.org/blender/blender/commit/2f1e8f4b97e7)).
  - Use vertical file list for "Recover Auto Save"
    ([f0ec7c2ec6](https://projects.blender.org/blender/blender/commit/f0ec7c2ec6ed)).
  - Replace big options button in file browser
    ([adfe68e202](https://projects.blender.org/blender/blender/commit/adfe68e2025b)).
  - Avoid file browser directory change if path didn't change
    ([ac15bf1646](https://projects.blender.org/blender/blender/commit/ac15bf16462b)).
  - Submitted [D5810](http://developer.blender.org/D5810): Ghost: Make
    temporary windows on-top popup windows.

Fixes:

  - [\#69755](http://developer.blender.org/T69755): 'Enter' over file
    name not working
    ([af9ca138ba](https://projects.blender.org/blender/blender/commit/af9ca138ba)).
  - Crash in local collections with excluded layer
    ([1353158aa2](https://projects.blender.org/blender/blender/commit/1353158aa24b)).
  - [\#70074](http://developer.blender.org/T70074): Missing file execute
    region in some files (crashes)
    ([ee12af9c97](https://projects.blender.org/blender/blender/commit/ee12af9c97f7)).
  - [\#70048](http://developer.blender.org/T70048): Assert when
    cancelling Append window
    ([4a5af65fe9](https://projects.blender.org/blender/blender/commit/4a5af65fe9e3)).
  - Preferences opens file browser in wrong window
    ([c8df6f2cf9](https://projects.blender.org/blender/blender/commit/c8df6f2cf9df)).
  - Empty file options region in regular editor
    ([09b728ff3e](https://projects.blender.org/blender/blender/commit/09b728ff3e37)).

Review:

  - [D2772](http://developer.blender.org/D2772): Tracking: Highlight
    keyframes in path visualization.

### VR

Did some fixes and cleanups in the \`soc2019-openxr\` branch, so I can
update the review patch soon:

  - Fix mistakes in openxr build options
    ([3c38e67765](https://projects.blender.org/blender/blender/commit/3c38e67765)).
  - Fix Linux linker config name for the OpenXR-SDK
    ([9baa9e2bd3](https://projects.blender.org/blender/blender/commit/9baa9e2bd3)).
  - Cleanup
    ([639dffa43a](https://projects.blender.org/blender/blender/commit/639dffa43aa4),
    [6af2f222ca](https://projects.blender.org/blender/blender/commit/6af2f222caea),
    [cddf043cca](https://projects.blender.org/blender/blender/commit/cddf043ccace)).

## September 9 - September 15

Moved to Amsterdam this week\! Unfortunately doing that took me quite
some time, so this report will be much shorter than the last one.

  - Refactor buttton extra icons to support multiple icons with custom
    operators ([D5730](https://developer.blender.org/D5730),
    [828905190e](https://developer.blender.org/rB828905190e12432228d88563e0dcb74054b488a9)).
  - Add superimposed + and - icons for file increasing/decreasing
    ([2aa3e9c67c](https://developer.blender.org/rB2aa3e9c67cc554dd285f37147e83d5e6f82625c3)).
  - File browser deselect on click on empty space
    ([42c062c98a](https://developer.blender.org/rB42c062c98a2e8e48a63d909ec39ed7d6677a7504)).
  - *\[Submitted for review\]* Preferences options for temporary editor
    display type ([D5754](http://developer.blender.org/D5754)).
  - *\[Submitted, but abandoned for alternative solution\]* Ghost: Add
    support for always-on-top windows
    ([D5765](http://developer.blender.org/D5765)).

Review:

  - Reviewed/tested [D5682](http://developer.blender.org/D5682).

Fixes:

  - Fix [\#69791](http://developer.blender.org/T69791): Fix crash
    reading old file browser in temporary window
    ([914f4308fb](https://projects.blender.org/blender/blender/commit/914f4308fb)).
  - Fix [\#69736](http://developer.blender.org/T69736): Flipping
    bookmarks region empties it
    ([69e0f485c9](https://projects.blender.org/blender/blender/commit/69e0f485c9)).

## September 1 - September 8

Tried to get the file browser UI design overhaul in early in the week.
Managed to do that, but had to spend most remaining time fixing many
related issues (not all caused by our changes). This did however raise
some concerns regarding testing for me, which I'd like to discuss with
the UI team soon.  
Initially I wanted to address some other usability related tasks while
I'm still working from home (mostly
[\#57918](http://developer.blender.org/T57918)). We got plenty of
feedback on the file browser changes though and we want to further tweak
some things. So I will continue work on the file browser. Pablo,
William, Brecht and I seem to agree on this.

### General Usability

#### File Browser Design Overhaul Merge

![../../../images/Overhauled\_File\_Browser\_UI\_Design.png](../../../images/Overhauled_File_Browser_UI_Design.png
"../../../images/Overhauled_File_Browser_UI_Design.png")

  - Updated patch ([D5601](http://developer.blender.org/D5601)):
      - Addressed remaining requested changes
        ([bee91a5a5a](https://projects.blender.org/blender/blender/commit/bee91a5a5a),
        [cbe7143589](https://projects.blender.org/blender/blender/commit/cbe7143589))
      - Gray out number increment/decrement button for file open
        operations
        ([99acf30edd](https://projects.blender.org/blender/blender/commit/99acf30edd))
      - Update Blender version for changes to IO Add-ons
        ([c3025ad122](https://projects.blender.org/blender/blender/commit/c3025ad122))
      - General Fixes
        ([aa0db0038e](https://projects.blender.org/blender/blender/commit/aa0db0038e),
        [82bc196736](https://projects.blender.org/blender/blender/commit/82bc196736),
        [d35efde181](https://projects.blender.org/blender/blender/commit/d35efde181))
  - Merged file browser design overhaul into master
    ([ee8f69c96c](https://projects.blender.org/blender/blender/commit/ee8f69c96c),
    [53aec1ccff](https://projects.blender.org/blender/blender/commit/53aec1ccff))

#### Fixes:

All related to the file browser redesign (though, not all caused by it):

  - Don't show button context menu for drag-labels
    ([2356f60c62](https://projects.blender.org/blender/blender/commit/2356f60c62))
  - [\#69451](http://developer.blender.org/T69451): Walk-select in empty
    directory asserts
  - Selecting multiple files ignoring first file
    ([197653e087](https://projects.blender.org/blender/blender/commit/197653e087))
  - Access to disabled OBJ exporter property
    ([0abe5a6a37](https://developer.blender.org/rBA0abe5a6a37d0014d5881f34543fa84f0bd0f66cc))
  - [\#69467](http://developer.blender.org/T69467): Temporary Info
    Editor window crashes
    ([0c7bfdf9a5](https://projects.blender.org/blender/blender/commit/0c7bfdf9a5))
  - [\#69463](http://developer.blender.org/T69463): File Browser opens
    off-center on hiDPI
    ([da25aca267](https://projects.blender.org/blender/blender/commit/da25aca267))
  - [\#69469](http://developer.blender.org/T69469): Overrun in file
    action type RNA enum
    ([718989d662](https://projects.blender.org/blender/blender/commit/718989d662))
  - [\#69457](http://developer.blender.org/T69457): Move file execute
    region and file path button back to C, fixing multiple bugs
    ([45d4c92579](https://projects.blender.org/blender/blender/commit/45d4c92579),
    [9972d6c306](https://projects.blender.org/blender/blender/commit/9972d6c306))
  - Bring back confirmation prompt for file path auto-create
    ([e10f8c27a2](https://projects.blender.org/blender/blender/commit/e10f8c27a2))
  - [\#69495](http://developer.blender.org/T69495): Crash changing
    action in file browser
    ([fbf6898f58](https://projects.blender.org/blender/blender/commit/fbf6898f58))
  - [\#69498](http://developer.blender.org/T69498): Crash on export UV
    Layout
    ([4c4a8bf588](https://projects.blender.org/blender/blender/commit/4c4a8bf588))
  - Failing assert on directory auto-creation
    ([5fd46d27f5](https://projects.blender.org/blender/blender/commit/5fd46d27f5))
  - [\#69581](http://developer.blender.org/T69581): File browser errors
    not reported in the UI
    ([4c20c53b89](https://projects.blender.org/blender/blender/commit/4c20c53b89),
    [83a7d98a32](https://projects.blender.org/blender/blender/commit/83a7d98a32))
  - Saving images from temp Image Editor failing
    ([ab823176d3](https://projects.blender.org/blender/blender/commit/ab823176d3))
  - Crash closing stacked file browser window
    ([a566b71333](https://projects.blender.org/blender/blender/commit/a566b71333))
  - [\#67756](http://developer.blender.org/T67756): File drag starts on
    file browser open
    ([e7476b667e](https://projects.blender.org/blender/blender/commit/e7476b667e))

#### Review

Reviewed a few smaller patches for the file browser:

  - [D5667](http://developer.blender.org/D5667): File Browser Optional
    Created and Accessed Date Columns (requested changes)
  - [D5713](http://developer.blender.org/D5713): File Browser Custom
    Folder Color (commented with suggestions, didn't want to accept or
    request changes)
  - [D5671](http://developer.blender.org/D5671): Fbx export crashes
    blender

Also participated in the UI meeting on Wednesday.

  

### VR

Not much happened on the VR front, plan is to step things up once I'm in
Amsterdam. Dalai reviewed my GSoC branch
([D5537](http://developer.blender.org/D5537)) and didn't seem to have
any bigger concern.  
There's also continuous contact with potential XR module team members.
I've asked to move design discussion to design tasks now
([\#68998](http://developer.blender.org/T68998),
[\#68994](http://developer.blender.org/T68994),
[\#68995](http://developer.blender.org/T68995)), so expect some activity
there soon-ish.
