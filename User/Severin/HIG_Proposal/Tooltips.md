# UI Tooltip Guidelines

<table>
<tbody>
<tr class="odd">
<td><div class="note_title">
<p><strong>WIP</strong></p>
</div>
<div class="note_content">
<p>These rules are not valid, yet. This page will get updated when they are!</p>
</div></td>
</tr>
</tbody>
</table>

## Introduction

Tooltips in Blender serve as hints to provide the user more information
about the tool a button represents in a quick, easy and usual way. To
ensure that these are of a high quality, we need to specify some
guidelines and follow these strictly.

Links:
<https://msdn.microsoft.com/en-us/library/windows/desktop/dn742443.aspx>

-----

## General rules

##### What tooltips should contain

  - The name of the tool
  - A description of the tool (see below\!)
  - If a button is disabled, the tooltip should always show useful
    information on why it is disabled and ideally how to change that.
  - Additional <b>useful</b> info (Shortcuts, Python path, etc. -
    *Automatically generated*)

##### What tooltips should <u>not</u> contain

  - Code snippets
  - Anything with very involved details (e.g. troubleshooting, corner
    cases that might not work, etc.)

## The tool description

The description of the tool should basically be <b>short and to the
point</b>. But there are more rules to follow:

  - Do not use more than a couple of lines, try to stick with two or
    three.
  - Use full sentences (periods and commas are allowed).
  - Examples of situations where you might use the tool are allowed.
  - Avoid using the described term to explained itself.
