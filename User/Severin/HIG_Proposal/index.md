# Blender Human Interface Guidelines (WIP/Proposal)

On a project with the dynamics and size of Blender, measurements have to
be taken to ensure common ground in the way interfaces for user
interactions are designed and built. Therefore, we - the Blender UI Team
- have established the Blender Human Interface Guidelines (HIG). Apart
from that, there are also rationales behind design decisions explained
here.

To us, terms like *user interface*, *user experience* or *usability*
refer to much more than just the cosmetics of our graphical user
interface; they are about the possibilities, limitations and behavior of
Blender, as well as the users workflows and even their emotional
connection to it. More concretely, we talk about things like
customizability, tool design, structure, interaction feedback, etc.

**Subpages**:
[:Special:PrefixIndex/API/](:Special:PrefixIndex/API/)

## Usability

The term usability has special relevance to us. ISO 9241 (ISO
9241-11:2018, sec. 3.1.1) defines it as follows:

"*\[E\]xtend to which a system, product or service can be used by
**specified users** to achieve **specified goals** with effectiveness,
efficiency and satisfaction in a **specified context** of use*"
(emphasis added)

There's an imporant implication in it: If we want to maximize
effectiveness of our usability engineering process, we need a deep
understanding of who uses Blender, what for and in which context. Our
aim is to build the process around this implication.

## Vision

<table>
<tbody>
<tr class="odd">
<td><div class="note_title">
<p><strong>PROPOSAL, NOT VALID!</strong></p>
</div>
<div class="note_content">
<p>There hasn't been any formal decision on the following. It's a mere proposal!<br />
Defining this vision is not something we can do on the go, we should define it carefully and upon consensus amongst people involved.</p>
</div></td>
</tr>
</tbody>
</table>

\[Maybe each section could show a real life example to show its point?
This may help making things less abstract, more practical.\]  
\[This section can be improved a lot, just trying to show the basic
idea.\]

**Made for Professional Use**

Blender is made to be usable by professionals in production
environments. By building a free and open source environment with this
goal, professional level tools become accessible to the masses. Our
belief is that hobbyists, freelancers and the like will greatly benefit
from this goal too.

We (the UI team) do not think that this necessarily makes software hard
to use for beginners. Professionals and beginners are not opposites.
There are many things that can be done to improve the experience for
both groups, although sometimes trade-offs need to be accepted.

**Workflow Flexibility**

The fields in which Blender is used are incredibly diverse, just like
the backgrounds of its users. While there are limitations to what can be
reasonably done, flexibility is an important quality. Blender should be
adoptable to the users needs and not enforce workflows.

Any complexity above what's needed should be avoided. Simplicity is key,
but not at the cost of limiting what's possible or big efficiency loss.

**Optimized for Specific Usages**

When trying to cater to such a diverse audience, there's a high risk of
generalizing too much towards the mysterious "users" without a clear
idea of who they are, what they do and what context they are in (see
definition of
[usability](index.md#Usability)). For that
reason, efforts should be put into understanding the core use-cases
Blender is aimed at. These define where Blender needs to be particularly
good at.

\[TODO what are these usages? Who defines them?\]

<div id="vision_rationale">

<div class="card">

<div class="card-header" id="heading_vision_rationale">

<div class="btn btn-link" data-toggle="collapse" data-target="#collapse_vision_rationale" aria-expanded="true" aria-controls="collapse_vision_rationale">

Rationale

</div>

</div>

<div id="collapse_vision_rationale" class="collapse" aria-labelledby="heading_vision_rationale" data-parent="#vision_rationale">

<div class="card-body">

Blender isn't solely used for 3D animation. People use it for VFX,
2D-ish animation, game production, architectural visualization, product
development, medical research, ... The unsatisfying reality is: We can't
build a perfect product for all these usages. If we try that, Blender
will end up being the jack of all trades, master of none.

</div>

</div>

</div>

</div>

\_\_NOEDITSECTION\_\_

**Facilitate Creativity**

Software interfaces tend to steal away user attention from actual
content they try to create. Tools should feel reliable, natural, smooth
and overall a pleasure to use. Only then is it possible that a user
forgets about the tools and gets to focus on the content instead. Then
the interface can ignite sparks, rather than absorb them.  
Again, this depends on the user, goals and context of usage (see
definition of
[usability](index.md#Usability)): Something
that feels natural to one user may not feel so to another one. Even just
the circumstances (goals and context) may change that.
