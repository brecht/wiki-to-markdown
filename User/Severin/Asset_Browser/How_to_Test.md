# Asset Browser: How to Test

Testing the Asset Browser at this stage involves the following:

  - Create Assets in a .blend file.
  - Set-up that file as repository.
  - Browse assets in a repository.
  - Use assets.

Note: Expect limitations, glitches and inconveniences. Right now focus
is only on getting the essentials to work.

## Creating Assets

Currently, only data-blocks can become assets. They can be created in
the following ways:

  - Right-click data-block in the Outliner \> *Make Asset* (if there's
    an *ID Data* sub-menu, it is in there).
  - Right-click any button that represents a data-block (e.g.
    [data-block
    selector](https://docs.blender.org/manual/en/latest/interface/controls/templates/data_block.html))
    \> *Make Asset*

Two **important** notes:

  - To make an Asset Browser show new assets, it has to be refreshed\!
    E.g. by pressing the
    <span class="hotkeybg"><span class="hotkey">R</span></span> key.  
    (This will likely be made more discover-able for later iterations).
  - To show assets from another file, that file has to be saved\!

The Asset Browser can show the assets in the current file by choosing
the "Current File" repository (see below).  

## Custom Repositories

![../../../images/Asset\_Repoitories\_Preferences.png](../../../images/Asset_Repoitories_Preferences.png
"../../../images/Asset_Repoitories_Preferences.png")

It is possible to select any .blend file as asset repository. This can
be done in Preferences \> File Paths \> Asset Repositories.  
In the future it will likely be possible to select directories as
repositories as well.

There is a default repository pointing to the following paths:

  - Linux, macOS: \`\~/assets.blend\`
  - Windows: \`\[user-directory\]/Documents/assets.blend\`

  

## Browsing/Using Assets

![The current icon is a placeholder of course
:)](../../../images/Asset_Browser_Editor_Menu.png
"The current icon is a placeholder of course :)")

The Asset Browser is a new, regular editor.

You can:

  - Select a repository
  - Choose a category of assets to show
  - Drag objects, images and materials into 3D Views.

Most display and filter options won't work, that is expected.

  

![../../../images/Asset\_Browser\_Repository\_Selector.png](../../../images/Asset_Browser_Repository_Selector.png
"../../../images/Asset_Browser_Repository_Selector.png")

Valid repositories will show up in the *Repository* menu of the Asset
Browser. The tooltip shows its file-path. The Asset Browser will then
display assets in the selected repository.
