## Issues with the current Principled BSDF

  - MultiGGX is a headache - slower, noisier, has bugs, hard to maintain
  - No "proper" metallic Fresnel, just basic F0 control
  - Sheen is outdated and designed to fix a problem that no longer
    exists
  - Tint options are limited - if we're giving non-physical control for
    artistic tweaks, might as well do it properly
  - Subsurface controls are not designed for "real" SSS
      - Subsurface blend is unintuitive
      - Subsurface color is unnecessary
      - Subsurface IOR is not really doing much
  - Random Walk SSS gives white edges at low albedo
  - Fresnel is implemented strangly
  - Three separate IOR inputs (two explicit, one implicit through
    Specular input)
  - Transmission roughness is strange

## Components of the new model

  - Base layer
      - Metallic specular
          - Use artist-friendly Fresnel from Imageworks slides, provides
            edge color and falloff control
              - At falloff=0.2, matches classic Schlick-F0-based
                metallic, so use that as default
          - Doesn't support accurate behavior (base-\>tint-\>white), so
            also support Gulbrandsen model as an option?
              - Maybe at falloff=0? Would mean a jump once you start
                adjusting the slider, though.
              - Do the linearization (compute F at g=0 and g=1, then
                lerp per-channel based on edge color)?
          - Only difference to Dielectric specular is Fresnel term, so
            can be implemented as one Closure
      - Diffuse
          - Do we want Oren-Nayar roughness?
              - If yes, would also need to be supported at SSS exit
          - Scale albedo based on albedo of other layers (specular,
            coatings) to conserve energy
              - Maybe optional?
      - Subsurface
          - Replace "Subsurface" factor with scale input, zero disables
            SSS
          - Try refracting into volume instead of a diffuse entry bounce
              - The problem with the current approach appears to be that
                at high radius, many random walks go directly from entry
                to exit
              - Surface color is controlled using single-scatter albedo,
                but if there is no scatter event it can't have any
                impact
              - Due to the diffuse entry bounce, near edges many parts
                start towards the adjacent face and immediately leave,
                leading to white color
              - Actual refraction at the entrance would prevent that,
                unless the edge is towards a backfacing face (where the
                white is realistic)
          - Still do diffuse exit to maintain smooth transition to
            diffuse at scale-\>0 and have efficient light connection
      - Dielectric specular
          - Use proper microsurface Fresnel term
      - Transmission
          - Shares IOR and roughness controls with dielectric specular
          - Consider supporting embedded volume automatically
              - Absorption and/or scattering
          - TODO: Should this be influenced by the base color? Really
            unrealistic, tint of glass objects should be volumetric.
          - Instead of computing macrosurface Fresnel during shader
            evaluation and using it to mix closures, use single closure
            based on microsurface Fresnel
              - Requires shared roughness
          - Also have thin surface mode where entry and exit refraction
            are handled in one shader
              - Could maybe automatically become transparent to shadow
                rays and secondary bounces if roughness is low enough?
  - Coatings
      - Sheen
          - Use Imageworks paper, looks fine
          - <http://www.aconty.com/pdf/s2017_pbs_imageworks_sheen.pdf>
      - Clearcoat
          - Keep using GTR1 distribution
          - Add tint control (based on tangent of incoming angle) for
            more realistic tinted "main" specular highlights
  - Translucent?
      - Only in thin sheet model
      - Try if we even need this, maybe rough transmission and/or
        subsurface is enough
  - Additional elements
      - Emission
          - Use opportunity to change defaults: Color defaults to white,
            Strength defaults to zero
      - Transparency

## Inputs of the new model

  - Base color (RGB)
      - Controls diffuse/subsurface color and metallic reflectance
  - Roughness (0-1)
      - Affects transmission and both specular lobes
  - Metallic (0-1)
      - Blends the base layer between metallic specular and
        diffuse+subsurface+dielectric+transmission
  - Transmission (0-1)
      - Blends the non-metallic part between transmission and
        diffuse+subsurface+dielectric
  - IOR (0.1-10 or so)
      - Affects dielectric specular, refraction and subsurface
  - Maybe: Diffuse roughness (0-1)
      - Zero for Lambertian, nonzero for Oren-Nayar
  - Subsurface radius (RGB vector), scale (0-inf)
      - Zero scale disables SSS, nonzero gets multiplied with radius
  - Anisotropy strength/rotation
      - Affects specular lobes (arguably only metallic would be more
        realistic, but this way we can share the closure)
  - Metallic edge color (RGB) and falloff (0-1)
      - Controls metallic color gradient from perpendicular to grazing
        angles
      - Default: White and 0.2, matches standard Schlick metallic
      - Maybe: Fallback 0 enters physical conductive Fresnel mode (using
        artist-friendly parametrization)?
  - Sheen strength (0-1), tint (RGB), roughness (0-1)
  - Clearcoat stength (0-1), tint (RGB), roughness (0-1)
      - Unclear: Disney BSDF scales the strength by 0.25. Do we want to
        keep that? If yes, maybe make range 0-4.
  - Normal
  - Clearcoat Normal
  - Tangent
  - TBD: Extra tint controls
      - Extra non-physical multipliers on top of specific lobes
      - Can be used to adjust brightness and/or color for artistic
        control
      - Default to white for physically-based and energy-conserving
        model
  - Thin surface mode (boolean)
      - Most components remain the same
      - Notable difference: Transmission does not actually refract
          - Sample microfacet and compute Fresnel as usual, but when we
            pick refraction, compute reflection and flip to other
            surface side instead
  - Alpha
  - Emission
  - Emission Strength

## Compatibility with old model

  - The basic outline of the model is the mostly the same, notable
    differences are:
      - Sheen is significantly different, despite the same name
      - Subsurface IOR is merged into the regular IOR
      - Subsurface strength control is no longer 0-1
      - Subsurface color is gone
          - In order to control the color of the scattered light (e.g.
            reddish for skin), use the radius controls
      - Dielectric specular strength input is gone
          - Used to be converted to an IOR, now the IOR input is used
            directly
          - For non-physical control where strength does not match IOR,
            use the non-physical tint input
  - Visual difference is TBD

## Energy conservation considerations

  - Make use of albedo scaling
  - Precompute correct albedo for given cosI and roughness using
    multiscattering model
  - Avoiding missing energy:
      - Adjust closure weight to account for energy loss in BSDF (divide
        by albedo)
  - Avoiding extra energy:
      - Adjust closure weight of "lower" layers according to albedo of
        upper layers
      - For example, clearcoat, sheen and specular reflection all end up
        dimming diffuse/subsurface

## Future ideas

  - Non-exponential SSS?
  - Iridescent coating
      - <https://belcour.github.io/blog/research/publication/2017/05/01/brdf-thin-film.html>
  - Group parameters better in UI
  - Also support coatings and metallic as standalone BSDF nodes?
      - Coatings would have BSDF input for base layer and scale it based
        on coating albedo for energy conservation.
