## 2023

### Week 30 to 37 (2023-07-25 to 2023-09-18)

  - Was at SIGGRAPH and then on vacation
  - Principled v2:
      - Updated and merged Principled Sheen rework PR ([PR
        \#109949](https://projects.blender.org/blender/blender/pulls/109949))
      - Created, updated and merged Principled layering rework PR ([PR
        \#110864](https://projects.blender.org/blender/blender/pulls/110864))
      - Created, updated and merged Principled Subsurface rework PR ([PR
        \#110989](https://projects.blender.org/blender/blender/pulls/110989))
      - Created, updated and merged Principled Clearcoat rework PR ([PR
        \#110993](https://projects.blender.org/blender/blender/pulls/110993))
      - Created, updated and merged Principled Emission rework PR ([PR
        \#111155](https://projects.blender.org/blender/blender/pulls/111155))
      - Created OSL closure layering bug fix PR ([PR
        \#112213](https://projects.blender.org/blender/blender/pulls/112213))
      - Created Principled Metallic improvement PR ([PR
        \#112551](https://projects.blender.org/blender/blender/pulls/112551))
  - Fixes for OSL vs. SVM differences:
      - Backfacing IOR in generalized\_schlick\_bsdf
        ([65d56143ab](https://projects.blender.org/blender/blender/commit/65d56143ab67fcbf3b1db3aacca1a58c488a354f))
      - Subsurface roughness
        ([c082e43f1e](https://projects.blender.org/blender/blender/commit/c082e43f1e210c056675c509c80eaf5424db92d4))
      - Layering of Principled Emission
        ([26ed117049](https://projects.blender.org/blender/blender/commit/26ed11704989e8c32a4bcbfb48d55abab2463401))
      - Reflection Normal adjustment on curves
        ([02ace97df7](https://projects.blender.org/blender/blender/commit/02ace97df7aa5993e1029ba153aea1df2269f453))
      - Geometry Node Tangent output on curves
        ([c963e22a0c](https://projects.blender.org/blender/blender/commit/c963e22a0cf5f22b71a76a05a5acb48bb6d98290))
  - Various fixes:
      - Build error after ShaderData change
        ([b19011e2db](https://projects.blender.org/blender/blender/commit/b19011e2dbe16aee2ca934b7c183f27ae993fde8))
      - Tests error after Sheen change
        ([4bd8f24b81](https://projects.blender.org/blender/blender/commit/4bd8f24b8123077cb2d86d0476cc873dde51f420))
      - Unused argument in Microfacet BSDF code
        ([825cc14e74](https://projects.blender.org/blender/blender/commit/825cc14e74dcb9871f4c73f4ea48c07f2eee4d48))
      - Wasteful reflection normal calculation when not using Coat
        ([e6296acdba](https://projects.blender.org/blender/blender/commit/e6296acdba44636e3a749f573c7e9a0a488a333e))
      - Unused Normal input on Hair BSDF
        ([5939810b3c](https://projects.blender.org/blender/blender/commit/5939810b3c7954f13ac4c0c8cc32b1e924767f4f))
      - Wasteful Closure Extra allocation for Chiang Hair BSDF
        ([1b92284f86](https://projects.blender.org/blender/blender/commit/1b92284f863b69adf8292ff4bd4a12f2387d16bf))
  - Reviews:
      - Reviewed (and made minor changes to) Microfacet Hair BSDF PR
        ([PR
        \#105600](https://projects.blender.org/blender/blender/pulls/105600))
      - Reviewed Multiscatter GGX default PR ([PR
        \#111267](https://projects.blender.org/blender/blender/pulls/111267))
      - Reviewed Microfacet Fresnel calculation refactor PR ([PR
        \#112158](https://projects.blender.org/blender/blender/pulls/112158))
      - Reviewed Principled BSDF node panel PR ([PR
        \#112314](https://projects.blender.org/blender/blender/pulls/112314))
  - Various Pull requests:
      - Opened PR for SVM cleanup ([PR
        \#110443](https://projects.blender.org/blender/blender/pulls/110443))
      - Opened WIP PR for Layer BSDF node ([PR
        \#110444](https://projects.blender.org/blender/blender/pulls/110444))

### Week 28 and 29 (2023-07-11 to 2023-07-24)

  - Merged Sheen BSDF ([PR
    \#108869](https://projects.blender.org/blender/blender/pulls/108869))
  - Created and merged cleanup to shrink ShaderData ([PR
    \#110366](https://projects.blender.org/blender/blender/pulls/110366))
  - Rebased Principled Sheen rework ([PR
    \#109949](https://projects.blender.org/blender/blender/pulls/109949))
  - Merged OSL Clearcoat cleanup ([PR
    \#109951](https://projects.blender.org/blender/blender/pulls/109951))
  - Merged Transmission Roughness removal ([PR
    \#109950](https://projects.blender.org/blender/blender/pulls/109950))
  - Fixed Fresnel handling in MNEE caustics code
    ([8639bf013c](https://projects.blender.org/blender/blender/commit/8639bf013cdc93ee13fb6eec3f49203625a13e01))
  - Continued to work on upcoming Principled changes (Specular changes,
    Subsurface changes)
  - Implemented BSDF layering for GPU OSL and SVN
  - University exam today, so no PR for the last two points yet

### Week 25 to 27 (2023-06-20 to 2023-07-10)

  - Reworked the [Sheen BSDF
    PR](https://projects.blender.org/blender/blender/pulls/108869) to no
    longer be WIP. Now it implements the LTC-based model, with
    versioning code etc., so it's in a state where it could be merged.
  - Created [a WIP
    PR](https://projects.blender.org/blender/blender/pulls/109949) for
    replacing the Sheen component of the Principled BSDF, is waiting for
    the first Sheen PR to be merged.
  - Created [a
    PR](https://projects.blender.org/blender/blender/pulls/109950) for
    removing the transmission roughness option from the Principled BSDF.
    Later found an issue with the MNEE caustics code, still needs a fix
    before merging.
  - Created [a
    PR](https://projects.blender.org/blender/blender/pulls/109951) for
    simplifying the OSL implementation of the clearcoat component.
  - Pushed [a branch to my
    repo](https://projects.blender.org/LukasStockner/blender/src/branch/principled)
    that combines all the PRs above, plus some commits that still need
    to be finished and split up into PRs:
      - Replacing the Principled Diffuse component with a regular
        lambertian Diffuse component
      - Removing the Principled Diffuse code, since it's now unused
      - Implementing layering support in OSL
          - This one still needs some work to support it on GPUs as
            well, the current code is CPU-only
      - Reworking the Principled BSDF to use layer stacking to enforce
        energy conservation
      - Using precomputed tables for accurately estimating specular
        layer albedo for correct layering
          - We should eventually have tables for all layerable closures,
            but for Principled this one and Clearcoat are the two that
            matter
  - Started working on the Principled BSDF Specular input rework (change
    Specular Tint to color input, change Specular to non-physical
    multiplier)
  - Started working on Principled BSDF Subsurface rework (removing SSS
    Color, changing Subsurface input to scale, adding entry refraction)
  - Reviewed [Sharp Distribution cleanup
    PR](https://projects.blender.org/blender/blender/pulls/109902)

### Week 24 (2023-06-13 to 2023-06-19)

  - Merged the [\#108211 (Glass BSDF glossy/transmission channel
    split)](https://projects.blender.org/blender/blender/issues/108211)
    fix from last week.
  - Fixed
    [\#105555](https://projects.blender.org/blender/blender/issues/105555)
    (a longstanding bug related to baking indirect light).
  - Created and merged [PR
    \#108955](https://projects.blender.org/blender/blender/pulls/108955)
    to fix an unreported bug that caused some direct light (especially
    from the background) leaking into indirect-only baking.
  - Created [PR
    \#108996](https://projects.blender.org/blender/blender/pulls/108996)
    to change how the sun lamp (aka distant lamp) in Cycles works, in
    order to fix
    [\#108957](https://projects.blender.org/blender/blender/issues/108957).
    With this change, the brightness of the sun lamp is constant across
    its full extent, while previous versions would have a darker center
    at high angles.
  - Updated [the Sheen BSDF PR
    \#108869](https://projects.blender.org/blender/blender/pulls/108869)
    to also have OSL support. Test builds are available
    [here](https://builder.blender.org/download/patch/PR108869/).

### Week 22 and 23 (2023-05-29 to 2023-06-12)

  - Created WIP patch for new sheen models
    ([\#108869](https://projects.blender.org/blender/blender/pulls/108869)).
    One of those will be used for Principled v2, but for now I'm adding
    both as options to the Velvet BSDF to get feedback on which one is
    better.
  - Fixed
    [\#108560](https://projects.blender.org/blender/blender/issues/108560)
    by adding support for setting light groups for instanced objects
    (meshes and lamps) through the collection instance. If the original
    object already has a light group assigned, it continues to use it
    for its instances.
  - Created [a
    branch](https://projects.blender.org/LukasStockner/blender/src/branch/glass-albedo-fix)
    to fix
    [\#108211](https://projects.blender.org/blender/blender/issues/108211).
    This changes the diffuse/glossy/transmission split logic to be able
    to have both glossy and transmission contributions from a single
    closure.
  - Fixed asserts in the [Principled
    BSDF](https://projects.blender.org/blender/blender/commit/3b4182f272790eb6746a8cf49b0d163c94470db8)
    and [Velvet
    BSDF](https://projects.blender.org/blender/blender/commit/801897ed4eb219ab226be7cc8a8dbebc0cb508c6)
    test cases.
  - Merged [the MultiGGX
    removal](https://projects.blender.org/blender/blender/pulls/107958).

### Week 20 and 21 (2023-05-16 to 2023-05-28)

In week 20, I implemented two missing parts of the MultiGGX removal
patch: On the one hand, I had forgotten OSL support. Turns out that the
OSL microfacet handling could be simplified a lot in general, so in
addition to making it work the patch now also removes 10 or so legacy
closure types from the OSL interface. On the other hand, the albedo
passes (Diffuse/Glossy/Transmission Color) had not worked correctly. The
fix is not as elegant as I'd like, but it works and it's the best option
to minimize memory requirements that I can see. The patch is ready to be
merged now.

Between week 21 and 22, I'm on vacation, so I didn't merge it yet. I did
work on the sheen BSDFs before I left, and will post the patch (and
merge the MultiGGX patch) in a few days.

### Week 19 (2023-05-08 to 2023-05-15)

**Finally** finished the MultiGGX removal and albedo scaling patch after
spending waay too much time getting lost in trying to find an accurate
solution for the multibounce Fresnel for the generalized Schlick model.
Finally decided that it makes no sense to try and account for the tint
factors, which simplifies the whole thing a lot. This was the main thing
blocking Principled v2.

### Week 18 (2023-05-01 to 2023-05-07)

  - Fixed
    [\#72011](https://projects.blender.org/blender/blender/issues/72011)
    (Quantization artifacts when baking bevel normals to 8-bit image)
  - Fixed
    [\#107577](https://projects.blender.org/blender/blender/issues/107577)
    (Crash when using denoising with Ashikhmin-Shirley BSDF)
  - Looked into
    [\#107580](https://projects.blender.org/blender/blender/issues/107580)
    (Report about the Glossy Color change in 3.6)
  - Updated
    [\#104445](https://projects.blender.org/blender/blender/pulls/104445)
    (Merging Glossy and Anisotropic BSDF)

### Week 4 and 5 (2023-01-23 to 2023-02-05)

  - Fixed [T103888](http://developer.blender.org/T103888) (Side-by-side
    stereoscopic renders in 3.3.1 ignore color management)
  - Fixed [T104041](http://developer.blender.org/T104041) (Incorrect
    diffuse color pass for Sheen)
  - Looked into [T103876](http://developer.blender.org/T103876) (AO node
    is not supported for direct lighting transparency)
  - Looked into [T103795](http://developer.blender.org/T103795)
    (Emissive meshes and shadowcatcher)
  - Started submitting pieces of the Principled v2 work:
      - Created and merged [D17099](http://developer.blender.org/D17099)
        (Switching to non-separable shadowing-masking term for
        Microfacet BSDFs)
      - Created and merged [D17100](http://developer.blender.org/D17100)
        (Using a faster and exact GGX VNDF sampling algorithm)
      - Created and merged [D17101](http://developer.blender.org/D17101)
        (Reorganizing Fresnel handling in the Microfacet BSDFs)
      - Created [D17123](http://developer.blender.org/D17123) (Merge
        Anisotropic BSDF node into Glossy BSDF node)
      - Created [D17149](http://developer.blender.org/D17149) (Use
        per-microfacet Fresnel term for Glass closures)
      - Created [D17211](http://developer.blender.org/D17211)
        (Reorganizing Fresnel handling further, implement OSL MaterialX
        closures)
      - Working on splitting out the next step: Removing the MultiGGX
        code
  - Created and merged [D17107](http://developer.blender.org/D17107)
    (Cycles tests comparison helper option)
  - Committed
    [rB329eeacc66d13c9007e22f200304ebe902737abb](http://developer.blender.org/rB329eeacc66d13c9007e22f200304ebe902737abb)
    (Small Cycles closure cleanup)

## 2022

### Week 48 to 3 (2022-11-28 to 2023-01-22)

  - Looked into [T72011](http://developer.blender.org/T72011) (artifacts
    when baking Bevel normals in Cycles)
      - Created and merged [D16727](http://developer.blender.org/D16727)
        to fix one of the two issues with Bevel baking
  - Fixed [T103067](http://developer.blender.org/T103067) (Workbench
    render crash in 3.4)
  - Looked into and closed
    [T103255](http://developer.blender.org/T103255) (Unexpected behavior
    when combining Color Attributes with White Noise Texture)
  - Fixed [T98951](http://developer.blender.org/T98951) (Incorrect Depth
    Pass rendering for Shadow Catcher Objects)
  - Looked into [T103327](http://developer.blender.org/T103327), was
    already fixed
  - Fixed [T103507](http://developer.blender.org/T103507) (World
    background contributing to wrong lightgroup)
  - Fixed [T103403](http://developer.blender.org/T103403) (Lightgroups
    receiving light on shadowcatcher objects)
  - Fixed [T103408](http://developer.blender.org/T103408) (Cycles
    viewport deadlock)
  - Looked into and closed
    [T103491](http://developer.blender.org/T103491) (Black areas in
    Refraction BSDF)
  - Fixed [T94698](http://developer.blender.org/T94698) (Incorrect light
    passes with volumetric scattering)
  - Fixed [T95244](http://developer.blender.org/T95244) (AO pass broken
    for shadow catcher objects)
  - Fixed [T94752](http://developer.blender.org/T94752) (Incorrect
    stereoscopic panoramas)
  - Fixed [T89037](http://developer.blender.org/T89037) (Several Cycles
    issues with objects with negative scale)
  - Fixed [T88849](http://developer.blender.org/T88849) (Slightly
    incorrect handling of motion blur shutter curve)
  - Fixed [T103786](http://developer.blender.org/T103786) (Missing
    viewport update for shadowcatcher self-shadowing)
  - Looked into [T102761](http://developer.blender.org/T102761) (New
    particles aren't rendered when Vector pass is enabled)
  - Fixed [T102920](http://developer.blender.org/T102920) (Incorrect
    handling of muted nodes in Cycles)
  - Started creating additional Cycles tests to cover some of the bugs
    above
  - Looked into updating the Node socket Panel patch, still need to
    discuss data layout further
  - Started splitting some of the parts of Principled v2 out into
    separate patches can be reviewed, first up are Microfacet changes
    and removal of MultiGGX code
  - Working on finishing several changes to Principled v2 so they can be
    pushed (thin sheet mode, metallic thin film, new sheen model,
    artistic controls)
  - Christmas break :)

### Week 43 to 47 (2022-10-24 to 2022-11-27)

  - Blender Conference\!
  - Continued work on Principled v2
      - Implemented thin film option for iridescence effects
        ([rBa8869c97646b6cc253cd23088f52149de0f87c4b](http://developer.blender.org/rBa8869c97646b6cc253cd23088f52149de0f87c4b))
      - Implemented node sections to keep large nodes manageable
        ([D16633](http://developer.blender.org/D16633))
      - Added Wavelength unit for thin film and wavelength node
        ([rB45cd4c9c829c903c2596c6c2054ebda5e31f7c44](http://developer.blender.org/rB45cd4c9c829c903c2596c6c2054ebda5e31f7c44))
      - Various small fixes and improvements
      - Worked on the following topics, not yet finished:
          - Fixing SSS on GPUs
          - Nested dielectrics
          - Thin Sheet mode
          - Artistic controls
          - Thin Film support for metals
          - Improved sheen model:
            <https://tizianzeltner.com/projects/Zeltner2022Practical/>
  - Merged [D16317](http://developer.blender.org/D16317) (Parallel ImBuf
    conversion to float)
  - Fixed [T92416](http://developer.blender.org/T92416) (First render
    with unknown image colorspace looks different) with
    [D16427](http://developer.blender.org/D16427)
  - Created [D16627](http://developer.blender.org/D16627) to fix
    [T98951](http://developer.blender.org/T98951) (Shadow catcher
    objects are double-counting data passes)
  - Investigated [T101726](http://developer.blender.org/T101726) (Cycles
    does not generate the exact same images when a scene is rendered
    twice)

### Week 41 and 42 (2022-10-09 to 2022-10-23)

  - Investigated [T101270](http://developer.blender.org/T101270) (Random
    Object Info not working on instanced curves)
  - Investigated [T101263](http://developer.blender.org/T101263) (Vector
    pass not being saved correctly when using the File Output node)
      - Created [D16257](http://developer.blender.org/D16257) to fix it
  - Fixed [T99450](http://developer.blender.org/T99450) (Animated
    Holdout not updating on frame change)
  - Fixed [T98672](http://developer.blender.org/T98672) (Noise texture
    shows incorrect behaviour for large scale values)
  - Fixed [T99151](http://developer.blender.org/T99151) (Regression:
    cycles not updated when off object types in "View object Types"
    menu)
  - Investigated [T92028](http://developer.blender.org/T92028) (Cycles:
    not updating for homogeneous to heterogeneous volume change in
    viewport render), was fixed already
  - Investigated [T92416](http://developer.blender.org/T92416) (First
    render of barbershop appears different)
  - Investigated [T93187](http://developer.blender.org/T93187) (Problem
    mixing Hair shader using color ramp and Hair Info Intercept value),
    was fixed already
  - Fixed [T93382](http://developer.blender.org/T93382) (Blender still
    generates subsurface render passes)
      - Ended up being a overall change to the way Blender handles
        render passes, see [D16295](http://developer.blender.org/D16295)
  - Fixed [T94136](http://developer.blender.org/T94136) (Cycles: No Hair
    Shadows with Transparent BSDF)
  - Fixed [T101969](http://developer.blender.org/T101969) (Crash when
    using large texture with nonstandard colorspace)
      - Also created [D16317](http://developer.blender.org/D16317) to
        speed up the image loading here
  - Fixed [T101329](http://developer.blender.org/T101329) (Regression:
    EXR 'JPG Preview' doesn't use color space anymore)

### Week 40 (2022-10-02 to 2022-10-08)

Finally back and able to spend time on Blender properly again\!

  - Merged latest master changes (e.g. Spectral data type preparation,
    ray differentials change) into Principled v2 branch
  - Unified old and new Principled clearcoat BSDFs
  - Started working on more general Fresnel control for Principled v2
    (to allow e.g. compatibility with glTF and Spec/Gloss workflow)
  - Fixed [T101185](http://developer.blender.org/T101185) (Crash in the
    new Mikktspace code for meshes consisting only of invalid triangles)
  - Fixed [T101651](http://developer.blender.org/T101651) (Cycles crash
    when device initialization fails)
  - Fixed Cycles side of [T101702](http://developer.blender.org/T101702)
    (Crash when using a boolean input in an OSL script)

### Week 31 to 33 (2022-07-25 to 2022-08-21)

  - Vacation\! I was away for most of the time, but I did manage to get
    some stuff in.
  - Created [D15589](http://developer.blender.org/D15589) (Mikktspace
    C++ port for more performance)
      - Most of the time I had was spent on the initial port as well as
        tons of cleanup and optimization
      - Also created [D15636](http://developer.blender.org/D15636) for
        moving the relevant BKE files to C++
  - Looked into bugs [T99450](http://developer.blender.org/T99450) and
    [T99565](http://developer.blender.org/T99565)
  - Continued working on Principled v2
      - Not a lot though, most of the time went into Mikktspace

### Week 29 and 30 (2022-07-11 to 2022-07-24)

  - Updated [D13522](http://developer.blender.org/D13522) (Nishita Sky
    support in Eevee), should be ready for merging now
  - Merged [D14740](http://developer.blender.org/D14740) (Render: Update
    lightgroup membership in objects and world if lightgroup is renamed)
  - Looked into [T98113](http://developer.blender.org/T98113) (Black
    dots in Shadow Catcher)
  - Fixed [T98367](http://developer.blender.org/T98367) (Light group
    passes do not work when shadow catcher is used)
  - Continued working on Principled v2
      - Looked through user feedback, especially regarding Fresnel term
      - Spent more time looking into how others are doing Fresnel
        controls to ensure broad compatibility
      - Replaced Generalized Schlick Fresnel for the metallic component
        with [Adobe's F82
        model](https://substance3d.adobe.com/documentation/s3d/files/225969599/225969601/1/1647019577092/Adobe+Standard+Material+-+Technical+Documentation.pdf)
          - Provides more realistic look (darkening at near-grazing
            angles, full reflection at grazing angles)
          - Compatible with Principled v1 at default (edge color white)
      - Created script for fitting the F82 metallic model to real-world
        metal measurements
          - Provided values and example renders for several metals in
            [T99447](http://developer.blender.org/T99447)
  - Created [D15485](http://developer.blender.org/D15485), the sampling
    patch from the last weeks

### Week 27 and 28 (2022-06-28 to 2022-07-10)

  - Continued working on Principled v2
      - Published first version of the branch
        (https://developer.blender.org/diffusion/B/history/principled-v2/)
      - Created [T99447](http://developer.blender.org/T99447), a task
        for tracking development progress
      - Fixed several bugs (e.g. incorrect glass reflection/transmission
        pick, black glass at zero roughness)
      - Started working on Iridescence component
      - Looked closer into what others in the field are doing for
        compatibility
  - Went to EGSR 2022, saw many great papers and talked to many great
    people
  - Revisited the sampling work from week 22/23, WIP version of the
    patch will go up soon
      - Significantly reduces noise in some cases
          - The improved stratification helps when individual dimensions
            are causing the noise (e.g. time dimension for strong motion
            blur, lens dimensions for DoF, light picking dimension for
            direct lighting)
          - When the noise comes from the overall light transport (e.g.
            indirect lighting), the effect is not really noticeable
      - Some details still need to be worked out (e.g. jittering
        distance controls)

### Week 26 (2022-06-20 to 2022-06-27)

  - Continued working on Principled v2
      - Fixed incorrect clearcoat tint logic
          - Spent too much time trying to get an analytical and later
            numerical approximation to the average exit bounce
            extinction, ended up just leaving a comment for now
      - Replaced GTR1 distribution for clearcoat with GGX
      - Implemented proper energy conservation for diffuse/SSS and
        clearcoat layers
  - Started reviewing [D15286](http://developer.blender.org/D15286)
    (path guiding)
  - Started reviewing current state of
    [D14664](http://developer.blender.org/D14664)) (GSoC multi light
    sampling)
  - Started reading EGSR 2022 papers

### Week 25 (2022-06-13 to 2022-06-19)

  - Continued working on Principled v2
      - Basic components are all working now
      - Cleanup is still needed, but probably good enough to publish as
        a branch
      - Spent a lot of time on trying to avoid or shrink albedo LUTs,
        unfortunately with mixed results

### Week 24 (2022-06-06 to 2022-06-12)

  - Continuing work on Principled, Sampling and Mikktspace
  - Created Wiki page with notes regarding Principled:
    <https://wiki.blender.org/wiki/User:Lukasstockner97/Principled_v2>

### Week 22 and 23 (2022-05-23 to 2022-06-05)

  - Worked on "Principled v2"
      - Refactored SVM implementation of Principled node to clean up
        input passing
      - Split SVM implementation of old Principled BSDF into components
        (diffuse+SSS, specular, glass etc.)
      - Started implementing components of new Principled BSDF
  - Spent some time looking into Cycles sampling improvements
      - Better hash function for reordering samples, preserving 2D
        stratification for PMJ during reordering, proper hash-based Owen
        scrambling, padding Sobol sequence to avoid higher dimension
        vectors...
      - Got some promising results but needs further investigation
      - Will also look into screen-space blue noise
  - Ported Mikktspace tangent code to C++
      - Allows to simplify code considerably
      - Currently \~30% faster
      - New code opens up parallelization opportunities
      - Many additional small optimizations possible (e.g. SIMDify
        vector operations, use saved memory to cache normals, ...)
      - Should also look into parallelizing tangent computation across
        meshes in Cycles since this step does not make use of the
        Blender API

### Week 21 (2022-05-16 to 2022-05-22)

  - Spent the entire week on-site in Amsterdam
  - Cleaned up the Microfacet code further to improve maintainability
  - Replaced Multi-scattering GGX with a albedo-scaling factor on top of
    the regular GGX closures
  - Worked on design for "Principled v2" (placeholder name) and started
    implementation
  - Still need to create a design task and branch

### Week 20 (2022-05-09 to 2022-05-15)

  - Not a lot happened this week, I spent more time on university so I
    can spend the entire next week on Blender while I'm in Amsterdam.
  - Mostly researched options for the Principled BSDF work
      - The entire thing might be growing from a few small fixes into a
        Principled v2 at this point
      - Goal would be to "catch up" to improvements in the
        physically-based shading space over the last few years
      - Tricky part will be balancing flexibility and power vs. ease of
        use (avoid having 50 inputs...)

### Week 19 (2022-05-02 to 2022-05-08)

  - Fixed [T97169](http://developer.blender.org/T97169), then realized
    that a commit with the same fix (for a different bug) had already
    been committed a few days ago and I hadn't updated my checkout :(
  - Looked into some other open bugs
    ([T96606](http://developer.blender.org/T96606),
    [T96719](http://developer.blender.org/T96719), and a bunch of others
    related to Multiscattering GGX)
  - Created [D14889](http://developer.blender.org/D14889) (GGX
    Microfacet refactor)
      - Not much changes from the user perspective, just slightly
        brighter reflections at grazing angles due to a more accurate
        shadowing-masking function
      - Also basically no impact on performance or noise, but makes the
        code easier to understand and maintain
      - Finally produces expected results for approximate
        multiscattering precomputation (see Week 17)
  - Got initial version of the two approximate multiscattering
    approaches working, will compare further
      - Still needs glass version and color handling to properly replace
        the current multiscattering code
      - Will create test build when it's ready for user feedback

### Week 18 (2022-04-25 to 2022-05-01)

  - Reviewed [D14754](http://developer.blender.org/D14754) (LRU cache
    for dynamic Cycles kernels)
  - Fixed [T96576](http://developer.blender.org/T96576) (Light leaks
    with Multiscatter GGX and normal maps)
  - Created [D14815](http://developer.blender.org/D14815) to fix
    [T95644](http://developer.blender.org/T95644) (Object Attributes not
    updating when rendering on GPU)
  - Started to fix [T96718](http://developer.blender.org/T96718),
    currently tracked it down to an uninitialized stack read in the SVM
    code of a shader
  - Looked into the GGX issue mentioned last week a bit more, nothing
    solid yet. Might be related to different versions of the Smith
    shadowing term?
  - More planning regarding Principled BSDF improvements, will write
    proper description soon.

### Week 17 (2022-04-18 to 2022-04-24)

  - Merged [D14675](http://developer.blender.org/D14675) (Speeing up
    tangent space computation)
  - Created [D14735](http://developer.blender.org/D14735), fixes
    [T97386](http://developer.blender.org/T97386) (Nodes not draggable
    when clicking on labels since the tooltip change)
  - Created [D14740](http://developer.blender.org/D14740) (Updating
    lightgroup membership of objects and world when renaming
    lightgroups)
  - Started to work on Principled BSDF improvements:
      - Created precomputation code for the approximate multiscattering
        code, noticed discrepancy between Cycles' GGX and other
        renderers at grazing angles. Need to investigate further.
      - Implemented improved GGX VNDF importance sampling code, doesn't
        make much difference for noise or render time but is simpler.

### Week 16 (2022-04-11 to 2022-04-17)

  - Merged [D9967](http://developer.blender.org/D9967) (Node socket
    tooltip support), Blender is ready for adding socket descriptions
    now
  - Fixed small unreported issue
    [rB6f1ad5f5e77c](http://developer.blender.org/rB6f1ad5f5e77c) (Same
    Material selection in grease pencil used the wrong hash type)
  - Reviewed [D14667](http://developer.blender.org/D14667) (Fix for UDIM
    texture detection)
  - Looked into [T97378](http://developer.blender.org/T97378) (Slow
    initialization of viewport rendering when adding an unconnected
    Normal Map node)
  - Created [D14675](http://developer.blender.org/D14675) (Speeding up
    tangent space computation), also spent a lot of time on additional
    optimization that didn't really work out yet.
  - Started to look into Principled BSDF improvements:
      - Current ideas:
          - Replacing the Monte-Carlo-based Multiscattering GGX with an
            approximate approach
              - Would solve the same problem (darkening at high
                roughness) but reduce noise, render times and code
                complexity
              - Two approaches to correction terms are known
                (Kelemen-style or Turquin-style), would prefer the
                latter currently
          - Add support for metallic Fresnel term using an
            artist-friendly parametrization (e.g. "edge color" input)
              - Could maybe expose physical parameters in e.g. the
                Glossy BSDF
          - Fix current ad-hoc (and sometimes questionable) mixing of
            BSDF lobes by defining (and documenting\!) a consistent and
            physically plausible model
              - Includes reformulating the sheen and clearcoat parts as
                coatings (see SPI slides below)
          - Replacing diffuse entry into random-walk SSS by refraction,
            would improve the bright edges at low albedo
          - Adding a thin sheet BSDF, possibly as a separate node?
      - Would need to remain close to the current BSDF of course to
        avoid notable changes in existing scenes
      - Relevant material:
          - ["Extending the Disney BRDF to a BSDF with Integrated
            Subsurface Scattering" by Brent Burley, main reference for
            the model used in the Principled
            BSDF](https://blog.selfshadow.com/publications/s2015-shading-course/burley/s2015_pbs_disney_bsdf_notes.pdf)
          - ["A Multi-Faceted Exploration" by Stephen Hill, great blog
            series about multiscattering
            corrections](https://blog.selfshadow.com/2018/05/13/multi-faceted-part-1/)
          - ["Practical multiple scattering compensation for microfacet
            models" by Emmanuel Turquin, technical report about
            multiscattering
            correction](https://blog.selfshadow.com/publications/turquin/ms_comp_final.pdf)
          - ["Revisiting Physically Based Shading at Imageworks" by
            Christopher Kulla and Alejandro Conty, fantastic reference
            for physically-based BSDF
            models](https://blog.selfshadow.com/publications/s2017-shading-course/imageworks/s2017_pbs_imageworks_slides_v2.pdf)

### Week 15 (2022-04-04 to 2022-04-10)

  - Reviewed [D14395](http://developer.blender.org/D14395) (UDIM
    packing), ready for merge now.
  - Updated [T78008](http://developer.blender.org/T78008) (Light group
    improvement collection)
  - Merged [D14540](http://developer.blender.org/D14540) (Light Group UI
    improvement: Adding light groups from object/world properties)
  - Fixed [T97159](http://developer.blender.org/T97159) (Color AOV
    passes missing their alpha pass since Blender 3.0)
  - Created [D14596](http://developer.blender.org/D14596) (Light group
    improvement: Operators for adding all used or removing all unused
    lightgroups)
  - Started working on showing lightgroups in viewport rendering
  - Updated [D9967](http://developer.blender.org/D9967) (Node socket
    tooltip support), much cleaner implementation now.

### Week 14 (2022-03-28 to 2022-04-03)

  - Reviewed, polished and merged
    [D12871](http://developer.blender.org/D12871) (Cycles Light Groups)
  - Reviewed and merged [D14527](http://developer.blender.org/D14527)
    (Fix for Light Groups on lamps)
  - Fixed [T96978](http://developer.blender.org/T96978) (Emitters
    themselves not showing up in Light Groups)
  - Reviewed [D14395](http://developer.blender.org/D14395) (UDIM
    packing)
  - Created [D14540](http://developer.blender.org/D14540) (Light Group
    UI improvement: Adding light groups from object/world properties)

### Week 13 (2022-03-21 to 2022-03-27)

  - Continued to catch up on code changes (e.g. C++ification of the
    core)
  - Updated [D13522](http://developer.blender.org/D13522) (Nishita Sky
    support in Eevee) to address review comments.
  - Updated [D9967](http://developer.blender.org/D9967) (Tooltip support
    for node sockets) to address review comments
      - Uses a different approach now to reduce hackiness and also
        supports tooltips in the Material Properties tab
      - Also spent way too much time digging through the UI code trying
        yet another approach that went nowhere...
  - Reviewed [D14327](http://developer.blender.org/D14327) (Preparation
    for UDIM packing)
  - Reviewed [D14395](http://developer.blender.org/D14395) (UDIM
    packing)
  - Started reviewing [D12871](http://developer.blender.org/D12871)
    (Cycles Light Groups)

### Week 12 (2022-03-14 to 2022-03-20)

  - Went through the onboarding process
  - Set up my local development setup properly again
  - Caught up on the development process changes and the Cycles-X
    architecture (not fully happy with my understanding yet, will
    continue, probably by fixing bugs)
  - Updated [D13522](http://developer.blender.org/D13522) (Nishita Sky
    support in Eevee) and prepared compatibility with rewrite branch
  - Updated [D9967](http://developer.blender.org/D9967) (Tooltip support
    for node sockets) and integrated the existing work on Geometry Node
    introspection. The actual tooltip content and showing them in the
    Material Properties tab is still TODO.
