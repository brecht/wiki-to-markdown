# Weekly Reports

## Week 01 (June 01 - 06)

Less active on tracker this week due to University exams. Spent the week
on triaging and a little time on devtalk. Next week, I will be doing
triaging and devtalk moderation.

**Total actions on tracker: 124**

  - Confirmed: 14

[T88873](http://developer.blender.org/T88873),
[T88848](http://developer.blender.org/T88848),
[T88834](http://developer.blender.org/T88834),
[T88812](http://developer.blender.org/T88812),
[T88792](http://developer.blender.org/T88792),
[T88755](http://developer.blender.org/T88755),
[T88710](http://developer.blender.org/T88710),
[T88685](http://developer.blender.org/T88685),
[T88674](http://developer.blender.org/T88674),
[T88673](http://developer.blender.org/T88673),
[T88672](http://developer.blender.org/T88672),
[T88665](http://developer.blender.org/T88665),
[T88685](http://developer.blender.org/T88685),
[T88732](http://developer.blender.org/T88732)

  - Closed as Archived: 12

[T88886](http://developer.blender.org/T88886),
[T88888](http://developer.blender.org/T88888),
[T88867](http://developer.blender.org/T88867),
[T88829](http://developer.blender.org/T88829),
[T88797](http://developer.blender.org/T88797),
[T88825](http://developer.blender.org/T88825),
[T88820](http://developer.blender.org/T88820),
[T88772](http://developer.blender.org/T88772),
[T88453](http://developer.blender.org/T88453),
[T88474](http://developer.blender.org/T88474),
[T83523](http://developer.blender.org/T83523),
[T88465](http://developer.blender.org/T88465)

  - Closed as Duplicate: 2

[T88817](http://developer.blender.org/T88817),
[T88746](http://developer.blender.org/T88746)

  - Closed as Resolved: 2

[T88730](http://developer.blender.org/T88730),
[T84135](http://developer.blender.org/T84135)

  - Needs Information from User (open reports):

[T88857](http://developer.blender.org/T88857),
[T88880](http://developer.blender.org/T88880),
[T88818](http://developer.blender.org/T88818),
[T88778](http://developer.blender.org/T88778),
[T88768](http://developer.blender.org/T88768),
[T88757](http://developer.blender.org/T88757)

  - Needs Information from Developers: 0

<!-- end list -->

  - Diff submitted: 2

[D11495](http://developer.blender.org/D11495),
[D11510](http://developer.blender.org/D11510)

## Week 02 (June 07 - 13)

**Total actions on tracker: 231**

Did triaging this week. Exams are over so I can spend more time on
tracker from next week.  
Next week: Triaging and spending some extra time on dev talk posts.

  - Confirmed: 17

[T89099](http://developer.blender.org/T89099),
[T89065](http://developer.blender.org/T89065),
[T89056](http://developer.blender.org/T89056),
[T88978](http://developer.blender.org/T88978),
[T89002](http://developer.blender.org/T89002),
[T89036](http://developer.blender.org/T89036),
[T88944](http://developer.blender.org/T88944),
[T89029](http://developer.blender.org/T89029),
[T89021](http://developer.blender.org/T89021),
[T88999](http://developer.blender.org/T88999),
[T89001](http://developer.blender.org/T89001),
[T88957](http://developer.blender.org/T88957),
[T88974](http://developer.blender.org/T88974),
[T88909](http://developer.blender.org/T88909),
[T88924](http://developer.blender.org/T88924),
[T88912](http://developer.blender.org/T88912),
[T88914](http://developer.blender.org/T88914)

  - Unconfirmed: 1

[T88530](http://developer.blender.org/T88530)

  - Closed as Archived: 27

[T89108](http://developer.blender.org/T89108),
[T89019](http://developer.blender.org/T89019),
[T89083](http://developer.blender.org/T89083),
[T89080](http://developer.blender.org/T89080),
[T89066](http://developer.blender.org/T89066),
[T89026](http://developer.blender.org/T89026),
[T89031](http://developer.blender.org/T89031),
[T89007](http://developer.blender.org/T89007),
[T89027](http://developer.blender.org/T89027),
[T85006](http://developer.blender.org/T85006),
[T89023](http://developer.blender.org/T89023),
[T89005](http://developer.blender.org/T89005),
[T86216](http://developer.blender.org/T86216),
[T88671](http://developer.blender.org/T88671),
[T88694](http://developer.blender.org/T88694),
[T87141](http://developer.blender.org/T87141),
[T87362](http://developer.blender.org/T87362),
[T88925](http://developer.blender.org/T88925),
\[<http://developer.blender.org/T87809>: T87809:\],
[T88008](http://developer.blender.org/T88008),
[T88258](http://developer.blender.org/T88258),
[T88358](http://developer.blender.org/T88358),
[T88403](http://developer.blender.org/T88403),
[T88406](http://developer.blender.org/T88406),
[T88425](http://developer.blender.org/T88425),
[T88583](http://developer.blender.org/T88583),
[T88645](http://developer.blender.org/T88645)

  - Needs Information from User: 11

[T89107](http://developer.blender.org/T89107),
[T89096](http://developer.blender.org/T89096),
[T89094](http://developer.blender.org/T89094),
[T89085](http://developer.blender.org/T89085),
[T89078](http://developer.blender.org/T89078),
[T88979](http://developer.blender.org/T88979),
[T88967](http://developer.blender.org/T88967),
[T88928](http://developer.blender.org/T88928),
[T88881](http://developer.blender.org/T88881),
[T88875](http://developer.blender.org/T88875),
[T88863](http://developer.blender.org/T88863)

  - Needs Information from Developers: 3

[T81497](http://developer.blender.org/T81497),
[T89004](http://developer.blender.org/T89004),
[T88528](http://developer.blender.org/T88528)

  - Closed as Duplicate: 7

[T89111](http://developer.blender.org/T89111),
[T89095](http://developer.blender.org/T89095),
[T89086](http://developer.blender.org/T89086),
[T89042](http://developer.blender.org/T89042),
[T88866](http://developer.blender.org/T88866),
[T88949](http://developer.blender.org/T88949),
[T88915](http://developer.blender.org/T88915)

  - Closed as Resolved: 4

[T89072](http://developer.blender.org/T89072),
[T80386](http://developer.blender.org/T80386),
[T88946](http://developer.blender.org/T88946),
[T88661](http://developer.blender.org/T88661)

  - Diff submitted: 0

## Week 03 (June 14 - 20)

Spent the time on triaging the reports. Was inactive from Thursday to
Sunday (Was away from home).  
Next week: triaging and spending some time on dev talk posts.

  - Confirmed: 16

[T89310](http://developer.blender.org/T89310),
[T89122](http://developer.blender.org/T89122),
[T89271](http://developer.blender.org/T89271),
[T89268](http://developer.blender.org/T89268),
[T89221](http://developer.blender.org/T89221),
[T89252](http://developer.blender.org/T89252),
[T89219](http://developer.blender.org/T89219),
[T89199](http://developer.blender.org/T89199),
[T89172](http://developer.blender.org/T89172),
[T89186](http://developer.blender.org/T89186),
[T89169](http://developer.blender.org/T89169),
[T89175](http://developer.blender.org/T89175),
[T89176](http://developer.blender.org/T89176),
[T89063](http://developer.blender.org/T89063),
[T89166](http://developer.blender.org/T89166),
[T89135](http://developer.blender.org/T89135)

  - Closed as Archived: 32

[T89285](http://developer.blender.org/T89285),
[T88543](http://developer.blender.org/T88543),
[T88789](http://developer.blender.org/T88789),
[T88928](http://developer.blender.org/T88928),
[T88962](http://developer.blender.org/T88962),
[T88967](http://developer.blender.org/T88967),
[T88979](http://developer.blender.org/T88979),
[T89008](http://developer.blender.org/T89008),
[T88980](http://developer.blender.org/T88980),
[T89217](http://developer.blender.org/T89217),
[T88863](http://developer.blender.org/T88863),
[T88057](http://developer.blender.org/T88057),
[T88509](http://developer.blender.org/T88509),
[T88875](http://developer.blender.org/T88875),
[T88881](http://developer.blender.org/T88881),
[T88890](http://developer.blender.org/T88890),
[T88935](http://developer.blender.org/T88935),
[T88939](http://developer.blender.org/T88939),
[T88652](http://developer.blender.org/T88652),
[T88717](http://developer.blender.org/T88717),
[T88753](http://developer.blender.org/T88753),
[T89168](http://developer.blender.org/T89168),
[T89150](http://developer.blender.org/T89150),
[T87992](http://developer.blender.org/T87992),
[T88768](http://developer.blender.org/T88768),
[T88778](http://developer.blender.org/T88778),
[T89126](http://developer.blender.org/T89126),
[T88810](http://developer.blender.org/T88810),
[T88818](http://developer.blender.org/T88818),
[T88823](http://developer.blender.org/T88823),
[T88827](http://developer.blender.org/T88827),
[T89117](http://developer.blender.org/T89117)

  - Closed as Duplicate: 7

[T89296](http://developer.blender.org/T89296),
[T89293](http://developer.blender.org/T89293),
[T89132](http://developer.blender.org/T89132),
[T89179](http://developer.blender.org/T89179),
[T89154](http://developer.blender.org/T89154),
[T89107](http://developer.blender.org/T89107),
[T89118](http://developer.blender.org/T89118)

  - Closed as Resolved: 4

[T89251](http://developer.blender.org/T89251),
[T88880](http://developer.blender.org/T88880),
[T89070](http://developer.blender.org/T89070),
[T89115](http://developer.blender.org/T89115)

  - Needs Information from User: 14

[T87506](http://developer.blender.org/T87506),
[T89230](http://developer.blender.org/T89230),
[T89191](http://developer.blender.org/T89191),
[T89192](http://developer.blender.org/T89192),
[T88872](http://developer.blender.org/T88872),
[T89184](http://developer.blender.org/T89184),
[T89174](http://developer.blender.org/T89174),
[T89073](http://developer.blender.org/T89073),
[T89084](http://developer.blender.org/T89084),
[T89162](http://developer.blender.org/T89162),
[T89089](http://developer.blender.org/T89089),
[T89093](http://developer.blender.org/T89093),
[T89104](http://developer.blender.org/T89104),
[T89133](http://developer.blender.org/T89133)

  - Needs Information from Developers: 2

[T88846](http://developer.blender.org/T88846),
[T89171](http://developer.blender.org/T89171)

  - Diff submitted: 0

## Week 04 (June 21 - 27)

Spent the time on triaging the reports.  
Next week: triaging and spending some time on dev talk posts.

**Total actions on tracker: 205**

  - Confirmed (status: open or resolved): 25

[T89450](http://developer.blender.org/T89450),
[T89449](http://developer.blender.org/T89449),
[T87973](http://developer.blender.org/T87973),
[T87185](http://developer.blender.org/T87185),
[T89415](http://developer.blender.org/T89415),
[T85846](http://developer.blender.org/T85846),
[T89417](http://developer.blender.org/T89417),
[T89416](http://developer.blender.org/T89416),
[T89405](http://developer.blender.org/T89405),
[T88481](http://developer.blender.org/T88481),
[T88616](http://developer.blender.org/T88616),
[T88676](http://developer.blender.org/T88676),
[T88721](http://developer.blender.org/T88721),
[T88760](http://developer.blender.org/T88760),
[T88784](http://developer.blender.org/T88784),
[T88518](http://developer.blender.org/T88518),
[T89283](http://developer.blender.org/T89283),
[T89101](http://developer.blender.org/T89101),
[T89192](http://developer.blender.org/T89192),
[T89342](http://developer.blender.org/T89342),
[T89327](http://developer.blender.org/T89327),
[T89283](http://developer.blender.org/T89283),
[T89303](http://developer.blender.org/T89303),
[T89306](http://developer.blender.org/T89306),
[T89323](http://developer.blender.org/T89323)

  - Closed as Archived: 27

[T89462](http://developer.blender.org/T89462),
[T89448](http://developer.blender.org/T89448),
[T89184](http://developer.blender.org/T89184),
[T89073](http://developer.blender.org/T89073),
[T89419](http://developer.blender.org/T89419),
[T88530](http://developer.blender.org/T88530),
[T88872](http://developer.blender.org/T88872),
[T88857](http://developer.blender.org/T88857),
[T89104](http://developer.blender.org/T89104),
[T89071](http://developer.blender.org/T89071),
[T89401](http://developer.blender.org/T89401),
[T88733](http://developer.blender.org/T88733),
[T89380](http://developer.blender.org/T89380),
[T89174](http://developer.blender.org/T89174),
[T89363](http://developer.blender.org/T89363),
[T89089](http://developer.blender.org/T89089),
[T89093](http://developer.blender.org/T89093),
[T89112](http://developer.blender.org/T89112),
[T89346](http://developer.blender.org/T89346),
[T88468](http://developer.blender.org/T88468),
[T88491](http://developer.blender.org/T88491),
[T88690](http://developer.blender.org/T88690),
[T88757](http://developer.blender.org/T88757),
[T88857](http://developer.blender.org/T88857),
[T89076](http://developer.blender.org/T89076),
[T89078](http://developer.blender.org/T89078),
[T89085](http://developer.blender.org/T89085)

  - Closed as Duplicate: 3

[T89461](http://developer.blender.org/T89461),
[T89084](http://developer.blender.org/T89084),
[T89278](http://developer.blender.org/T89278)

  - Closed as Resolved: 7

[T87915](http://developer.blender.org/T87915),
[T89087](http://developer.blender.org/T89087),
[T89133](http://developer.blender.org/T89133),
[T89162](http://developer.blender.org/T89162),
[T89303](http://developer.blender.org/T89303),
[T89323](http://developer.blender.org/T89323),
[T89290](http://developer.blender.org/T89290)

  - Needs Information from User: 12

[T89354](http://developer.blender.org/T89354),
[T89319](http://developer.blender.org/T89319),
[T89156](http://developer.blender.org/T89156),
[T89079](http://developer.blender.org/T89079),
[T88819](http://developer.blender.org/T88819),
[T88793](http://developer.blender.org/T88793),
[T88764](http://developer.blender.org/T88764),
[T88720](http://developer.blender.org/T88720),
[T88692](http://developer.blender.org/T88692),
[T88487](http://developer.blender.org/T88487),
[T88130](http://developer.blender.org/T88130),
[T87513](http://developer.blender.org/T87513)

  - Needs Information from Developers: 4

[T87480](http://developer.blender.org/T87480),
[T88728](http://developer.blender.org/T88728),
[T88763](http://developer.blender.org/T88763),
[T89328](http://developer.blender.org/T89328)

  - Diff submitted: [D11717](http://developer.blender.org/D11717)

## Week 05 (June 28 - 04)

Spent the time on triaging the reports.  
Next week: triaging and spending some time on dev talk posts.  
**Total actions on tracker: 215**

  - Confirmed: 19

[T89634](http://developer.blender.org/T89634),
[T89605](http://developer.blender.org/T89605),
[T89395](http://developer.blender.org/T89395),
[T89592](http://developer.blender.org/T89592),
[T89575](http://developer.blender.org/T89575),
[T89560](http://developer.blender.org/T89560),
[T89549](http://developer.blender.org/T89549),
[T89545](http://developer.blender.org/T89545),
[T89544](http://developer.blender.org/T89544),
[T89523](http://developer.blender.org/T89523),
[T89079](http://developer.blender.org/T89079),
[T89523](http://developer.blender.org/T89523),
[T89044](http://developer.blender.org/T89044),
[T89479](http://developer.blender.org/T89479),
[T89499](http://developer.blender.org/T89499),
[T85840](http://developer.blender.org/T85840),
[T89476](http://developer.blender.org/T89476),
[T89484](http://developer.blender.org/T89484),
[T89487](http://developer.blender.org/T89487)

  - Closed as Archived: 23

[T87393](http://developer.blender.org/T87393),
[T88802](http://developer.blender.org/T88802),
[T89617](http://developer.blender.org/T89617),
[T89620](http://developer.blender.org/T89620),
[T89600](http://developer.blender.org/T89600),
[T88130](http://developer.blender.org/T88130),
[T88487](http://developer.blender.org/T88487),
[T88720](http://developer.blender.org/T88720),
[T88764](http://developer.blender.org/T88764),
[T89385](http://developer.blender.org/T89385),
[T89318](http://developer.blender.org/T89318),
[T89528](http://developer.blender.org/T89528),
[T89281](http://developer.blender.org/T89281),
[T89548](http://developer.blender.org/T89548),
[T89354](http://developer.blender.org/T89354),
[T89156](http://developer.blender.org/T89156),
[T88097](http://developer.blender.org/T88097),
[T87847](http://developer.blender.org/T87847),
[T88427](http://developer.blender.org/T88427),
[T89511](http://developer.blender.org/T89511),
[T89256](http://developer.blender.org/T89256),
[T88824](http://developer.blender.org/T88824),
[T89475](http://developer.blender.org/T89475)

  - Closed as Duplicate: 9

[T89640](http://developer.blender.org/T89640),
[T88119](http://developer.blender.org/T88119),
[T86041](http://developer.blender.org/T86041),
[T87119](http://developer.blender.org/T87119),
[T88737](http://developer.blender.org/T88737),
[T89453](http://developer.blender.org/T89453),
[T89550](http://developer.blender.org/T89550),
[T89533](http://developer.blender.org/T89533),
[T89480](http://developer.blender.org/T89480)

  - Closed as Resolved: 4

[T88481](http://developer.blender.org/T88481),
[T83089](http://developer.blender.org/T83089),
[T83142](http://developer.blender.org/T83142),
[T89518](http://developer.blender.org/T89518)

  - Needs Information from User: 14

[T89645](http://developer.blender.org/T89645),
[T89642](http://developer.blender.org/T89642),
[T89584](http://developer.blender.org/T89584),
[T89574](http://developer.blender.org/T89574),
[T89530](http://developer.blender.org/T89530),
[T89529](http://developer.blender.org/T89529),
[T89522](http://developer.blender.org/T89522),
[T89488](http://developer.blender.org/T89488),
[T89474](http://developer.blender.org/T89474),
[T89370](http://developer.blender.org/T89370),
[T88144](http://developer.blender.org/T88144),
[T87890](http://developer.blender.org/T87890),
[T86443](http://developer.blender.org/T86443),
[T86013](http://developer.blender.org/T86013)

  - Needs Information from Developers: 2

[T89622](http://developer.blender.org/T89622),
[T89592](http://developer.blender.org/T89592)

  - Diff submitted: [D11725](http://developer.blender.org/D11725),
    [D11794](http://developer.blender.org/D11794)

## Week 06 (July 05 - 11)

Spent the time on triaging the reports. Looked at the older reports.
Merged the old duplicate reports.  
Next week: Triaging and spending some time on dev talk posts.

**Total actions on tracker: 211**

  - Confirmed: 14

[T89770](http://developer.blender.org/T89770),
[T89750](http://developer.blender.org/T89750),
[T89745](http://developer.blender.org/T89745),
[T86974](http://developer.blender.org/T86974),
[T89688](http://developer.blender.org/T89688),
[T89726](http://developer.blender.org/T89726),
[T89724](http://developer.blender.org/T89724),
[T89584](http://developer.blender.org/T89584),
[T89709](http://developer.blender.org/T89709),
[T89123](http://developer.blender.org/T89123),
[T89680](http://developer.blender.org/T89680),
[T89691](http://developer.blender.org/T89691),
[T89687](http://developer.blender.org/T89687),
[T89647](http://developer.blender.org/T89647),

  - Closed as Archived: 25

[T87890](http://developer.blender.org/T87890),
[T89775](http://developer.blender.org/T89775),
[T89772](http://developer.blender.org/T89772),
[T89762](http://developer.blender.org/T89762),
[T84894](http://developer.blender.org/T84894),
[T85704](http://developer.blender.org/T85704),
[T82621](http://developer.blender.org/T82621),
[T87436](http://developer.blender.org/T87436),
[T80479](http://developer.blender.org/T80479),
[T89512](http://developer.blender.org/T89512),
[T89529](http://developer.blender.org/T89529),
[T88809](http://developer.blender.org/T88809),
[T84113](http://developer.blender.org/T84113),
[T89558](http://developer.blender.org/T89558),
[T80405](http://developer.blender.org/T80405),
[T89703](http://developer.blender.org/T89703),
[T89522](http://developer.blender.org/T89522),
[T89474](http://developer.blender.org/T89474),
[T89488](http://developer.blender.org/T89488),
[T89677](http://developer.blender.org/T89677),
[T78774](http://developer.blender.org/T78774),
[T88842](http://developer.blender.org/T88842),
[T89409](http://developer.blender.org/T89409),
[T89437](http://developer.blender.org/T89437),
[T89443](http://developer.blender.org/T89443)

  - Closed as Duplicate: 16

[T89766](http://developer.blender.org/T89766),
[T81279](http://developer.blender.org/T81279),
[T79921](http://developer.blender.org/T79921),
[T88028](http://developer.blender.org/T88028),
[T88095](http://developer.blender.org/T88095),
[T89744](http://developer.blender.org/T89744),
[T89742](http://developer.blender.org/T89742),
[T89740](http://developer.blender.org/T89740),
[T89728](http://developer.blender.org/T89728),
[T89555](http://developer.blender.org/T89555),
[T86617](http://developer.blender.org/T86617),
[T89685](http://developer.blender.org/T89685),
[T88417](http://developer.blender.org/T88417),
[T89668](http://developer.blender.org/T89668),
[T89631](http://developer.blender.org/T89631),
[T88862](http://developer.blender.org/T88862)

  - Closed as Resolved: 4

[T87546](http://developer.blender.org/T87546),
[T89499](http://developer.blender.org/T89499),
[T87972](http://developer.blender.org/T87972),
[T89169](http://developer.blender.org/T89169)

  - Needs Information from User: 13

[T89739](http://developer.blender.org/T89739),
[T89695](http://developer.blender.org/T89695),
[T89686](http://developer.blender.org/T89686),
[T89675](http://developer.blender.org/T89675),
[T89674](http://developer.blender.org/T89674),
[T89389](http://developer.blender.org/T89389),
[T89238](http://developer.blender.org/T89238),
[T85243](http://developer.blender.org/T85243),
[T86908](http://developer.blender.org/T86908),
[T86929](http://developer.blender.org/T86929),
[T86999](http://developer.blender.org/T86999),
[T87485](http://developer.blender.org/T87485),
[T88794](http://developer.blender.org/T88794)

  - Needs Information from Developers: 0

<!-- end list -->

  - Diff submitted: 0

## Week 07 (July 12 - 18)

Spent the time on triaging the reports.  
Next week: triaging and spending some time on dev talk posts.  
**Total actions on tracker: 183**

  - Confirmed: 16

[T89929](http://developer.blender.org/T89929),
[T89912](http://developer.blender.org/T89912),
[T89904](http://developer.blender.org/T89904),
[T89733](http://developer.blender.org/T89733),
[T89891](http://developer.blender.org/T89891),
[T89318](http://developer.blender.org/T89318),
[T89868](http://developer.blender.org/T89868),
[T89857](http://developer.blender.org/T89857),
[T88765](http://developer.blender.org/T88765),
[T89836](http://developer.blender.org/T89836),
[T89826](http://developer.blender.org/T89826),
[T89827](http://developer.blender.org/T89827),
[T89829](http://developer.blender.org/T89829),
[T89810](http://developer.blender.org/T89810),
[T87821](http://developer.blender.org/T87821),
[T89781](http://developer.blender.org/T89781)

  - Closed as Archived: 29

[T88668](http://developer.blender.org/T88668),
[T87364](http://developer.blender.org/T87364),
[T88794](http://developer.blender.org/T88794),
[T89917](http://developer.blender.org/T89917),
[T89590](http://developer.blender.org/T89590),
[T89892](http://developer.blender.org/T89892),
[T89682](http://developer.blender.org/T89682),
[T89865](http://developer.blender.org/T89865),
[T89674](http://developer.blender.org/T89674),
[T89832](http://developer.blender.org/T89832),
[T89867](http://developer.blender.org/T89867),
[T89695](http://developer.blender.org/T89695),
[T89818](http://developer.blender.org/T89818),
[T89530](http://developer.blender.org/T89530),
[T87741](http://developer.blender.org/T87741),
[T89675](http://developer.blender.org/T89675),
[T89552](http://developer.blender.org/T89552),
[T89563](http://developer.blender.org/T89563),
[T89650](http://developer.blender.org/T89650),
[T89830](http://developer.blender.org/T89830),
[T89830](http://developer.blender.org/T89830),
[T89787](http://developer.blender.org/T89787),
[T89398](http://developer.blender.org/T89398),
[T89790](http://developer.blender.org/T89790),
[T89642](http://developer.blender.org/T89642),
[T89645](http://developer.blender.org/T89645),
[T89800](http://developer.blender.org/T89800),
[T89786](http://developer.blender.org/T89786),
[T89793](http://developer.blender.org/T89793),

  - Closed as Duplicate: 8

[T89907](http://developer.blender.org/T89907),
[T88672](http://developer.blender.org/T88672),
[T89843](http://developer.blender.org/T89843),
[T79396](http://developer.blender.org/T79396),
[T89848](http://developer.blender.org/T89848),
[T89760](http://developer.blender.org/T89760),
[T89807](http://developer.blender.org/T89807),
[T89788](http://developer.blender.org/T89788)

  - Closed as Resolved: 2

[T81080](http://developer.blender.org/T81080),
[T82128](http://developer.blender.org/T82128)

  - Needs Information from User: 4

[T89802](http://developer.blender.org/T89802),
[T89846](http://developer.blender.org/T89846),
[T89776](http://developer.blender.org/T89776),
[T89774](http://developer.blender.org/T89774)

  - Needs Information from Developers: 1

[T89909](http://developer.blender.org/T89909)

  - Diff submitted (Updated):
    [D11794](http://developer.blender.org/D11794)

## Week 08 (July 19 - 25)

Spent the time on triaging the reports.  
Next week: triaging and spending some time on dev talk posts.

**Total actions on tracker: 214**

  - Confirmed: 18

[T90129](http://developer.blender.org/T90129),
[T90118](http://developer.blender.org/T90118),
[T90102](http://developer.blender.org/T90102),
[T89774](http://developer.blender.org/T89774),
[T90087](http://developer.blender.org/T90087),
[T90063](http://developer.blender.org/T90063),
[T90051](http://developer.blender.org/T90051),
[T90042](http://developer.blender.org/T90042),
[T90036](http://developer.blender.org/T90036),
[T90032](http://developer.blender.org/T90032),
[T90014](http://developer.blender.org/T90014),
[T90007](http://developer.blender.org/T90007),
[T89998](http://developer.blender.org/T89998),
[T89979](http://developer.blender.org/T89979),
[T89968](http://developer.blender.org/T89968),
[T89963](http://developer.blender.org/T89963),
[T89885](http://developer.blender.org/T89885),
[T89941](http://developer.blender.org/T89941)

  - Closed as Archived: 25

[T90117](http://developer.blender.org/T90117),
[T90114](http://developer.blender.org/T90114),
[T89914](http://developer.blender.org/T89914),
[T86616](http://developer.blender.org/T86616),
[T89824](http://developer.blender.org/T89824),
[T90088](http://developer.blender.org/T90088),
[T89602](http://developer.blender.org/T89602),
[T90024](http://developer.blender.org/T90024),
[T90059](http://developer.blender.org/T90059),
[T90060](http://developer.blender.org/T90060),
[T90038](http://developer.blender.org/T90038),
[T90037](http://developer.blender.org/T90037),
[T90010](http://developer.blender.org/T90010),
[T90013](http://developer.blender.org/T90013),
[T90009](http://developer.blender.org/T90009),
[T89846](http://developer.blender.org/T89846),
[T82732](http://developer.blender.org/T82732),
[T85243](http://developer.blender.org/T85243),
[T87485](http://developer.blender.org/T87485),
[T89802](http://developer.blender.org/T89802),
[T89776](http://developer.blender.org/T89776),
[T89980](http://developer.blender.org/T89980),
[T89821](http://developer.blender.org/T89821),
[T89949](http://developer.blender.org/T89949),
[T87112](http://developer.blender.org/T87112)

  - Closed as Duplicate: 14

[T90108](http://developer.blender.org/T90108),
[T90095](http://developer.blender.org/T90095),
[T88749](http://developer.blender.org/T88749),
[T86864](http://developer.blender.org/T86864),
[T89764](http://developer.blender.org/T89764),
[T90030](http://developer.blender.org/T90030),
[T90003](http://developer.blender.org/T90003),
[T89990](http://developer.blender.org/T89990),
[T89854](http://developer.blender.org/T89854),
[T89966](http://developer.blender.org/T89966),
[T89939](http://developer.blender.org/T89939),
[T89921](http://developer.blender.org/T89921),
[T89923](http://developer.blender.org/T89923),
[T89947](http://developer.blender.org/T89947)

  - Closed as Resolved: 2

[T89062](http://developer.blender.org/T89062),
[T90004](http://developer.blender.org/T90004)

  - Needs Information from User: 9

[T90111](http://developer.blender.org/T90111),
[T90094](http://developer.blender.org/T90094),
[T90070](http://developer.blender.org/T90070),
[T90049](http://developer.blender.org/T90049),
[T90034](http://developer.blender.org/T90034),
[T90001](http://developer.blender.org/T90001),
[T89967](http://developer.blender.org/T89967),
[T89779](http://developer.blender.org/T89779),
[T87841](http://developer.blender.org/T87841)

  - Needs Information from Developers: 1

[T89913](http://developer.blender.org/T89913)

  - Diff submitted: 0

## Week 09 (July 26 - 01)

Spent the time on triaging the reports. Minor grammar fixes in manual.  
Next week: triaging and spending some time on dev talk posts. Will try
to update D11419 after triaging work.

**Total actions on tracker: 210**

  - Confirmed: 29

[T90328](http://developer.blender.org/T90328),
[T90338](http://developer.blender.org/T90338),
[T90343](http://developer.blender.org/T90343),
[T90332](http://developer.blender.org/T90332),
[T90326](http://developer.blender.org/T90326),
[T90318](http://developer.blender.org/T90318),
[T90314](http://developer.blender.org/T90314),
[T90312](http://developer.blender.org/T90312),
[T90295](http://developer.blender.org/T90295),
[T90284](http://developer.blender.org/T90284),
[T90291](http://developer.blender.org/T90291),
[T90207](http://developer.blender.org/T90207),
[T90275](http://developer.blender.org/T90275),
[T90269](http://developer.blender.org/T90269),
[T90256](http://developer.blender.org/T90256),
[T90213](http://developer.blender.org/T90213),
[T90249](http://developer.blender.org/T90249),
[T90234](http://developer.blender.org/T90234),
[T90237](http://developer.blender.org/T90237),
[T90221](http://developer.blender.org/T90221),
[T90212](http://developer.blender.org/T90212),
[T90211](http://developer.blender.org/T90211),
[T90210](http://developer.blender.org/T90210),
[T90204](http://developer.blender.org/T90204),
[T90153](http://developer.blender.org/T90153),
[T90154](http://developer.blender.org/T90154),
[T90120](http://developer.blender.org/T90120),
[T90104](http://developer.blender.org/T90104),
[T90131](http://developer.blender.org/T90131)

  - Closed as Archived: 30

[T90354](http://developer.blender.org/T90354),
[T90315](http://developer.blender.org/T90315),
[T90094](http://developer.blender.org/T90094),
[T90070](http://developer.blender.org/T90070),
[T90028](http://developer.blender.org/T90028),
[T90333](http://developer.blender.org/T90333),
[T90325](http://developer.blender.org/T90325),
[T88819](http://developer.blender.org/T88819),
[T90320](http://developer.blender.org/T90320),
[T90049](http://developer.blender.org/T90049),
[T90048](http://developer.blender.org/T90048),
[T79634](http://developer.blender.org/T79634),
[T90279](http://developer.blender.org/T90279),
[T90266](http://developer.blender.org/T90266),
[T89779](http://developer.blender.org/T89779),
[T90001](http://developer.blender.org/T90001),
[T90282](http://developer.blender.org/T90282),
[T90020](http://developer.blender.org/T90020),
[T90273](http://developer.blender.org/T90273),
[T89988](http://developer.blender.org/T89988),
[T90251](http://developer.blender.org/T90251),
[T90242](http://developer.blender.org/T90242),
[T90245](http://developer.blender.org/T90245),
[T90206](http://developer.blender.org/T90206),
[T90167](http://developer.blender.org/T90167),
[T90157](http://developer.blender.org/T90157),
[T90137](http://developer.blender.org/T90137),
[T86999](http://developer.blender.org/T86999),
[T88692](http://developer.blender.org/T88692),
[T88742](http://developer.blender.org/T88742)

  - Closed as Duplicate: 5

[T90330](http://developer.blender.org/T90330),
[T90294](http://developer.blender.org/T90294),
[T90229](http://developer.blender.org/T90229),
[T90214](http://developer.blender.org/T90214),
[T90150](http://developer.blender.org/T90150)

  - Closed as Resolved: 3

[T90269](http://developer.blender.org/T90269),
[T90226](http://developer.blender.org/T90226),
[T90102](http://developer.blender.org/T90102)

  - Needs Information from User: 7

[T90337](http://developer.blender.org/T90337),
[T90331](http://developer.blender.org/T90331),
[T90309](http://developer.blender.org/T90309),
[T90306](http://developer.blender.org/T90306),
[T90218](http://developer.blender.org/T90218),
[T90151](http://developer.blender.org/T90151),
[T90115](http://developer.blender.org/T90115)

  - Needs Information from Developers: 1

[T79996](http://developer.blender.org/T79996)

  - Diff submitted: 0

Commits (Manual): 3  
[rBM8242](http://developer.blender.org/rBM8242),
[rBM8259](http://developer.blender.org/rBM8259),
[rBM8262](http://developer.blender.org/rBM8262)

## Week 10 (Aug 02 - 08)

Spent the time on triaging the reports.  
Next week: triaging and spending some time on dev talk posts.

**Total actions on tracker: 196**

  - Confirmed: 20

[T90514](http://developer.blender.org/T90514),
[T90515](http://developer.blender.org/T90515),
[T90482](http://developer.blender.org/T90482),
[T90477](http://developer.blender.org/T90477),
[T89037](http://developer.blender.org/T89037),
[T90458](http://developer.blender.org/T90458),
[T90403](http://developer.blender.org/T90403),
[T89456](http://developer.blender.org/T89456),
[T90427](http://developer.blender.org/T90427),
[T90424](http://developer.blender.org/T90424),
[T90421](http://developer.blender.org/T90421),
[T90403](http://developer.blender.org/T90403),
[T90400](http://developer.blender.org/T90400),
[T90399](http://developer.blender.org/T90399),
[T90391](http://developer.blender.org/T90391),
[T90395](http://developer.blender.org/T90395),
[T90358](http://developer.blender.org/T90358),
[T90356](http://developer.blender.org/T90356),
[T90339](http://developer.blender.org/T90339),
[T90329](http://developer.blender.org/T90329)

  - Closed as Archived: 25

[T90513](http://developer.blender.org/T90513),
[T90331](http://developer.blender.org/T90331),
[T90495](http://developer.blender.org/T90495),
[T90138](http://developer.blender.org/T90138),
[T90309](http://developer.blender.org/T90309),
[T90322](http://developer.blender.org/T90322),
[T90306](http://developer.blender.org/T90306),
[T88750](http://developer.blender.org/T88750),
[T90080](http://developer.blender.org/T90080),
[T90215](http://developer.blender.org/T90215),
[T90218](http://developer.blender.org/T90218),
[T90449](http://developer.blender.org/T90449),
[T90419](http://developer.blender.org/T90419),
[T90169](http://developer.blender.org/T90169),
[T90171](http://developer.blender.org/T90171),
[T90174](http://developer.blender.org/T90174),
[T90061](http://developer.blender.org/T90061),
[T90083](http://developer.blender.org/T90083),
[T90121](http://developer.blender.org/T90121),
[T90107](http://developer.blender.org/T90107),
[T90378](http://developer.blender.org/T90378),
[T90103](http://developer.blender.org/T90103),
[T90115](http://developer.blender.org/T90115),
[T90141](http://developer.blender.org/T90141),
[T90090](http://developer.blender.org/T90090),
[T90363](http://developer.blender.org/T90363)

  - Closed as Duplicate: 9

[T90480](http://developer.blender.org/T90480),
[T89305](http://developer.blender.org/T89305),
[T90473](http://developer.blender.org/T90473),
[T88758](http://developer.blender.org/T88758),
[T90446](http://developer.blender.org/T90446),
[T90394](http://developer.blender.org/T90394),
[T90377](http://developer.blender.org/T90377),
[T90368](http://developer.blender.org/T90368),
[T90370](http://developer.blender.org/T90370),

  - Closed as Resolved: 4

[T90478](http://developer.blender.org/T90478),
[T87805](http://developer.blender.org/T87805),
[T90293](http://developer.blender.org/T90293),
[T87635](http://developer.blender.org/T87635)

  - Needs Information from User: 19

[T90434](http://developer.blender.org/T90434),
[T90396](http://developer.blender.org/T90396),
[T90388](http://developer.blender.org/T90388),
[T90365](http://developer.blender.org/T90365),
[T90362](http://developer.blender.org/T90362),
[T90349](http://developer.blender.org/T90349),
[T89934](http://developer.blender.org/T89934),
[T89142](http://developer.blender.org/T89142),
[T88948](http://developer.blender.org/T88948),
[T88847](http://developer.blender.org/T88847),
[T88843](http://developer.blender.org/T88843),
[T88840](http://developer.blender.org/T88840),
[T88835](http://developer.blender.org/T88835),
[T88740](http://developer.blender.org/T88740),
[T87985](http://developer.blender.org/T87985),
[T87960](http://developer.blender.org/T87960),
[T87531](http://developer.blender.org/T87531),
[T86572](http://developer.blender.org/T86572),
[T82247](http://developer.blender.org/T82247)

  - Needs Information from Developers: 0

<!-- end list -->

  - Diff submitted: 0

## Week 11 (Aug 09 - 15)

Spent the time on triaging the reports.  
Next week: triaging and spending some time on dev talk posts.

**Total actions on tracker: 202**

  - Confirmed: 25

[T90658](http://developer.blender.org/T90658),
[T90665](http://developer.blender.org/T90665),
[T90654](http://developer.blender.org/T90654),
[T90646](http://developer.blender.org/T90646),
[T90644](http://developer.blender.org/T90644),
[T90642](http://developer.blender.org/T90642),
[T90635](http://developer.blender.org/T90635),
[T90483](http://developer.blender.org/T90483),
[T90614](http://developer.blender.org/T90614),
[T90149](http://developer.blender.org/T90149),
[T90600](http://developer.blender.org/T90600),
[T90606](http://developer.blender.org/T90606),
[T90159](http://developer.blender.org/T90159),
[T90574](http://developer.blender.org/T90574),
[T90589](http://developer.blender.org/T90589),
[T90573](http://developer.blender.org/T90573),
[T90168](http://developer.blender.org/T90168),
[T90566](http://developer.blender.org/T90566),
[T90564](http://developer.blender.org/T90564),
[T90542](http://developer.blender.org/T90542),
[T90278](http://developer.blender.org/T90278),
[T90480](http://developer.blender.org/T90480),
[T90494](http://developer.blender.org/T90494),
[T90540](http://developer.blender.org/T90540),
[T90506](http://developer.blender.org/T90506)

  - Closed as Archived: 35

[T90684](http://developer.blender.org/T90684),
[T90503](http://developer.blender.org/T90503),
[T87531](http://developer.blender.org/T87531),
[T88740](http://developer.blender.org/T88740),
[T88847](http://developer.blender.org/T88847),
[T89238](http://developer.blender.org/T89238),
[T90643](http://developer.blender.org/T90643),
[T88948](http://developer.blender.org/T88948),
[T90439](http://developer.blender.org/T90439),
[T87960](http://developer.blender.org/T87960),
[T90434](http://developer.blender.org/T90434),
[T90631](http://developer.blender.org/T90631),
[T90605](http://developer.blender.org/T90605),
[T90622](http://developer.blender.org/T90622),
[T89721](http://developer.blender.org/T89721),
[T88835](http://developer.blender.org/T88835),
[T87985](http://developer.blender.org/T87985),
[T84255](http://developer.blender.org/T84255),
[T86929](http://developer.blender.org/T86929),
[T90603](http://developer.blender.org/T90603),
[T90601](http://developer.blender.org/T90601),
[T90596](http://developer.blender.org/T90596),
[T82247](http://developer.blender.org/T82247),
[T88843](http://developer.blender.org/T88843),
[T90349](http://developer.blender.org/T90349),
[T90388](http://developer.blender.org/T90388),
[T90335](http://developer.blender.org/T90335),
[T90362](http://developer.blender.org/T90362),
[T90568](http://developer.blender.org/T90568),
[T90365](http://developer.blender.org/T90365),
[T90034](http://developer.blender.org/T90034),
[T89791](http://developer.blender.org/T89791),
[T90500](http://developer.blender.org/T90500),
[T90348](http://developer.blender.org/T90348),
[T90342](http://developer.blender.org/T90342)

  - Closed as Duplicate: 10

[T90672](http://developer.blender.org/T90672),
[T90647](http://developer.blender.org/T90647),
[T90624](http://developer.blender.org/T90624),
[T90625](http://developer.blender.org/T90625),
[T90613](http://developer.blender.org/T90613),
[T90581](http://developer.blender.org/T90581),
[T90571](http://developer.blender.org/T90571),
[T90563](http://developer.blender.org/T90563),
[T90543](http://developer.blender.org/T90543),
[T90538](http://developer.blender.org/T90538)

  - Closed as Resolved: 5

[T90623](http://developer.blender.org/T90623),
[T88840](http://developer.blender.org/T88840),
[T89101](http://developer.blender.org/T89101),
[T90567](http://developer.blender.org/T90567),
[T87635](http://developer.blender.org/T87635)

  - Needs Information from User: 10

[T90674](http://developer.blender.org/T90674),
[T90668](http://developer.blender.org/T90668),
[T90602](http://developer.blender.org/T90602),
[T90590](http://developer.blender.org/T90590),
[T90583](http://developer.blender.org/T90583),
[T90544](http://developer.blender.org/T90544),
[T90533](http://developer.blender.org/T90533),
[T90528](http://developer.blender.org/T90528),
[T90516](http://developer.blender.org/T90516),
[T90152](http://developer.blender.org/T90152)

  - Own Reported: 2

[T90644](http://developer.blender.org/T90644),
[T90648](http://developer.blender.org/T90648)

  - Helped to backport: 2

[T90567](http://developer.blender.org/T90567),
[T89101](http://developer.blender.org/T89101)

  - Needs Information from Developers: 0

<!-- end list -->

  - Diff submitted: 0

## Week 12 (Aug 16 - 22)

Spent the time on triaging the reports.  
Next week: triaging and spending some time on dev talk posts.

**Total actions on tracker: 186**

  - Confirmed: 22

[T90847](http://developer.blender.org/T90847),
[T90819](http://developer.blender.org/T90819),
[T90815](http://developer.blender.org/T90815),
[T90804](http://developer.blender.org/T90804),
[T90798](http://developer.blender.org/T90798),
[T90797](http://developer.blender.org/T90797),
[T86468](http://developer.blender.org/T86468),
[T90796](http://developer.blender.org/T90796),
[T90786](http://developer.blender.org/T90786),
[T85130](http://developer.blender.org/T85130),
[T90787](http://developer.blender.org/T90787),
[T89632](http://developer.blender.org/T89632),
[T90733](http://developer.blender.org/T90733),
[T90742](http://developer.blender.org/T90742),
[T90732](http://developer.blender.org/T90732),
[T90731](http://developer.blender.org/T90731),
[T90727](http://developer.blender.org/T90727),
[T90719](http://developer.blender.org/T90719),
[T90718](http://developer.blender.org/T90718),
[T90706](http://developer.blender.org/T90706),
[T85840](http://developer.blender.org/T85840),
[T90659](http://developer.blender.org/T90659)

  - Closed as Archived: 17

[T90057](http://developer.blender.org/T90057),
[T90588](http://developer.blender.org/T90588),
[T90636](http://developer.blender.org/T90636),
[T90800](http://developer.blender.org/T90800),
[T90792](http://developer.blender.org/T90792),
[T90516](http://developer.blender.org/T90516),
[T90533](http://developer.blender.org/T90533),
[T90544](http://developer.blender.org/T90544),
[T90587](http://developer.blender.org/T90587),
[T90749](http://developer.blender.org/T90749),
[T90602](http://developer.blender.org/T90602),
[T90709](http://developer.blender.org/T90709),
[T90724](http://developer.blender.org/T90724),
[T90497](http://developer.blender.org/T90497),
[T90509](http://developer.blender.org/T90509),
[T90524](http://developer.blender.org/T90524),
[T90512](http://developer.blender.org/T90512)

  - Closed as Duplicate: 6

[T90820](http://developer.blender.org/T90820),
[T90774](http://developer.blender.org/T90774),
[T90764](http://developer.blender.org/T90764),
[T86514](http://developer.blender.org/T86514),
[T89142](http://developer.blender.org/T89142),
[T90702](http://developer.blender.org/T90702)

  - Closed as Resolved: 4

[T90589](http://developer.blender.org/T90589),
[T90690](http://developer.blender.org/T90690),
[T90697](http://developer.blender.org/T90697),
[T90692](http://developer.blender.org/T90692)

  - Needs Information from User: 7

[T90763](http://developer.blender.org/T90763),
[T90748](http://developer.blender.org/T90748),
[T90714](http://developer.blender.org/T90714),
[T90693](http://developer.blender.org/T90693),
[T89613](http://developer.blender.org/T89613),
[T89579](http://developer.blender.org/T89579),
[T88610](http://developer.blender.org/T88610)

  - Needs Information from Developers: 0

<!-- end list -->

  - Helped to backport: [T90690](http://developer.blender.org/T90690)

<!-- end list -->

  - Diff submitted: 0

## Week 13 (Aug 23 - 29)

Spent the time on triaging the reports.  
Next week: triaging and spending some time on dev talk posts. Part-time
work from 1st September

**Total actions on tracker: 223**

  - Confirmed: 17

[T91023](http://developer.blender.org/T91023),
[T90999](http://developer.blender.org/T90999),
[T90995](http://developer.blender.org/T90995),
[T90976](http://developer.blender.org/T90976),
[T90959](http://developer.blender.org/T90959),
[T90923](http://developer.blender.org/T90923),
[T90928](http://developer.blender.org/T90928),
[T90915](http://developer.blender.org/T90915),
[T90911](http://developer.blender.org/T90911),
[T90902](http://developer.blender.org/T90902),
[T90893](http://developer.blender.org/T90893),
[T90880](http://developer.blender.org/T90880),
[T90839](http://developer.blender.org/T90839),
[T90840](http://developer.blender.org/T90840),
[T90846](http://developer.blender.org/T90846),
[T90845](http://developer.blender.org/T90845),
[T90853](http://developer.blender.org/T90853)

  - Closed as Archived: 30

[T90997](http://developer.blender.org/T90997),
[T90992](http://developer.blender.org/T90992),
[T90781](http://developer.blender.org/T90781),
[T90794](http://developer.blender.org/T90794),
[T90957](http://developer.blender.org/T90957),
[T90975](http://developer.blender.org/T90975),
[T90980](http://developer.blender.org/T90980),
[T90763](http://developer.blender.org/T90763),
[T90779](http://developer.blender.org/T90779),
[T90783](http://developer.blender.org/T90783),
[T90956](http://developer.blender.org/T90956),
[T90951](http://developer.blender.org/T90951),
[T90925](http://developer.blender.org/T90925),
[T90938](http://developer.blender.org/T90938),
[T90748](http://developer.blender.org/T90748),
[T90899](http://developer.blender.org/T90899),
[T90904](http://developer.blender.org/T90904),
[T90905](http://developer.blender.org/T90905),
[T90891](http://developer.blender.org/T90891),
[T90152](http://developer.blender.org/T90152),
[T89102](http://developer.blender.org/T89102),
[T86572](http://developer.blender.org/T86572),
[T89967](http://developer.blender.org/T89967),
[T90454](http://developer.blender.org/T90454),
[T90693](http://developer.blender.org/T90693),
[T90863](http://developer.blender.org/T90863),
[T90833](http://developer.blender.org/T90833),
[T90632](http://developer.blender.org/T90632),
[T90668](http://developer.blender.org/T90668),
[T90674](http://developer.blender.org/T90674),

  - Closed as Duplicate: 12

[T91010](http://developer.blender.org/T91010),
[T90996](http://developer.blender.org/T90996),
[T90937](http://developer.blender.org/T90937),
[T90962](http://developer.blender.org/T90962),
[T90936](http://developer.blender.org/T90936),
[T90926](http://developer.blender.org/T90926),
[T90934](http://developer.blender.org/T90934),
[T90909](http://developer.blender.org/T90909),
[T90894](http://developer.blender.org/T90894),
[T87563](http://developer.blender.org/T87563),
[T90813](http://developer.blender.org/T90813),
[T90861](http://developer.blender.org/T90861)

  - Closed as Resolved: 3

[T90965](http://developer.blender.org/T90965),
[T87841](http://developer.blender.org/T87841),
[T90917](http://developer.blender.org/T90917)

  - Needs Information from User: 7

[T90983](http://developer.blender.org/T90983),
[T90977](http://developer.blender.org/T90977),
[T90942](http://developer.blender.org/T90942),
[T90933](http://developer.blender.org/T90933),
[T90896](http://developer.blender.org/T90896),
[T90885](http://developer.blender.org/T90885),
[T90858](http://developer.blender.org/T90858)

  - Needs Information from Developers: 0

<!-- end list -->

  - Diff submitted: 0

## Week 14 (Aug 30 - 05)

Spent the time on triaging the reports. Part-time work from 1st
September.  
I was less active because of college exams  
Next week: triaging and spending some time on dev talk posts.

**Total actions on tracker: 151**

  - Confirmed: 18

[T91170](http://developer.blender.org/T91170),
[T91160](http://developer.blender.org/T91160),
[T91133](http://developer.blender.org/T91133),
[T91141](http://developer.blender.org/T91141),
[T91139](http://developer.blender.org/T91139),
[T91114](http://developer.blender.org/T91114),
[T91123](http://developer.blender.org/T91123),
[T90124](http://developer.blender.org/T90124),
[T90921](http://developer.blender.org/T90921),
[T91006](http://developer.blender.org/T91006),
[T91013](http://developer.blender.org/T91013),
[T91060](http://developer.blender.org/T91060),
[T91064](http://developer.blender.org/T91064),
[T91008](http://developer.blender.org/T91008),
[T91019](http://developer.blender.org/T91019),
[T91040](http://developer.blender.org/T91040),
[T91035](http://developer.blender.org/T91035),
[T91038](http://developer.blender.org/T91038)

  - Closed as Archived: 17

[T90969](http://developer.blender.org/T90969),
[T90942](http://developer.blender.org/T90942),
[T90977](http://developer.blender.org/T90977),
[T90983](http://developer.blender.org/T90983),
[T90990](http://developer.blender.org/T90990),
[T91126](http://developer.blender.org/T91126),
[T90933](http://developer.blender.org/T90933),
[T90896](http://developer.blender.org/T90896),
[T91107](http://developer.blender.org/T91107),
[T90993](http://developer.blender.org/T90993),
[T90885](http://developer.blender.org/T90885),
[T90528](http://developer.blender.org/T90528),
[T88610](http://developer.blender.org/T88610),
[T89613](http://developer.blender.org/T89613),
[T89579](http://developer.blender.org/T89579),
[T91024](http://developer.blender.org/T91024),
[T91042](http://developer.blender.org/T91042)

  - Closed as Duplicate: 5

[T91147](http://developer.blender.org/T91147),
[T91115](http://developer.blender.org/T91115),
[T91118](http://developer.blender.org/T91118),
[T91029](http://developer.blender.org/T91029),
[T90339](http://developer.blender.org/T90339)

  - Closed as Resolved: 0

<!-- end list -->

  - Needs Information from User: 4

[T91149](http://developer.blender.org/T91149),
[T91145](http://developer.blender.org/T91145),
[T91142](http://developer.blender.org/T91142),
[T91063](http://developer.blender.org/T91063)

  - Needs Information from Developers: 0

<!-- end list -->

  - Diff submitted: 0

## Week 15 (Sep 06 - 12)

Spent the time on triaging the reports. Took a day off on Friday due to
festival.  
Next week: triaging and spending some time on dev talk posts.

**Total actions on tracker: 130**

  - Confirmed: 13

[T91303](http://developer.blender.org/T91303),
[T91285](http://developer.blender.org/T91285),
[T91256](http://developer.blender.org/T91256),
[T91241](http://developer.blender.org/T91241),
[T91234](http://developer.blender.org/T91234),
[T91225](http://developer.blender.org/T91225),
[T91219](http://developer.blender.org/T91219),
[T91219](http://developer.blender.org/T91219),
[T91188](http://developer.blender.org/T91188),
[T91199](http://developer.blender.org/T91199),
[T91191](http://developer.blender.org/T91191),
[T91187](http://developer.blender.org/T91187),
[T91203](http://developer.blender.org/T91203)

  - Closed as Archived: 16

[T91349](http://developer.blender.org/T91349),
[T90858](http://developer.blender.org/T90858),
[T91142](http://developer.blender.org/T91142),
[T91287](http://developer.blender.org/T91287),
[T91276](http://developer.blender.org/T91276),
[T91275](http://developer.blender.org/T91275),
[T91273](http://developer.blender.org/T91273),
[T91259](http://developer.blender.org/T91259),
[T91100](http://developer.blender.org/T91100),
[T91063](http://developer.blender.org/T91063),
[T91248](http://developer.blender.org/T91248),
[T91039](http://developer.blender.org/T91039),
[T91016](http://developer.blender.org/T91016),
[T91161](http://developer.blender.org/T91161),
[T90590](http://developer.blender.org/T90590),
[T91200](http://developer.blender.org/T91200)

  - Closed as Duplicate: 9

[T91339](http://developer.blender.org/T91339),
[T91267](http://developer.blender.org/T91267),
[T91309](http://developer.blender.org/T91309),
[T91284](http://developer.blender.org/T91284),
[T91283](http://developer.blender.org/T91283),
[T91229](http://developer.blender.org/T91229),
[T91228](http://developer.blender.org/T91228),
[T91230](http://developer.blender.org/T91230),
[T91219](http://developer.blender.org/T91219)

  - Closed as Resolved: 0

<!-- end list -->

  - Needs Information from User: 5

[T91331](http://developer.blender.org/T91331),
[T91304](http://developer.blender.org/T91304),
[T91286](http://developer.blender.org/T91286),
[T91270](http://developer.blender.org/T91270),
[T91149](http://developer.blender.org/T91149)

  - Needs Information from Developers: 1

[T91223](http://developer.blender.org/T91223)

  - Diff submitted: 1

[D12450](http://developer.blender.org/D12450)

## Week 16 (Sep 13 - 19)

Spent the time on triaging the reports.  
Next week: triaging and spending some time on dev talk posts.

**Total actions on tracker: 123**

  - Confirmed: 16

[T91501](http://developer.blender.org/T91501),
[T91390](http://developer.blender.org/T91390),
[T91263](http://developer.blender.org/T91263),
[T91458](http://developer.blender.org/T91458),
[T89778](http://developer.blender.org/T89778),
[T91131](http://developer.blender.org/T91131),
[T91444](http://developer.blender.org/T91444),
[T91424](http://developer.blender.org/T91424),
[T91411](http://developer.blender.org/T91411),
[T91390](http://developer.blender.org/T91390),
[T91393](http://developer.blender.org/T91393),
[T91320](http://developer.blender.org/T91320),
[T91341](http://developer.blender.org/T91341),
[T91369](http://developer.blender.org/T91369),
[T91355](http://developer.blender.org/T91355),
[T91358](http://developer.blender.org/T91358)

  - Closed as Archived: 13

[T91027](http://developer.blender.org/T91027),
[T91513](http://developer.blender.org/T91513),
[T91233](http://developer.blender.org/T91233),
[T91301](http://developer.blender.org/T91301),
[T91494](http://developer.blender.org/T91494),
[T91304](http://developer.blender.org/T91304),
[T91472](http://developer.blender.org/T91472),
[T91471](http://developer.blender.org/T91471),
[T91449](http://developer.blender.org/T91449),
[T91436](http://developer.blender.org/T91436),
[T91391](http://developer.blender.org/T91391),
[T91351](http://developer.blender.org/T91351),
[T89971](http://developer.blender.org/T89971)

  - Closed as Duplicate: 4

[T91514](http://developer.blender.org/T91514),
[T91501](http://developer.blender.org/T91501),
[T91356](http://developer.blender.org/T91356),
[T91354](http://developer.blender.org/T91354)

  - Closed as Resolved: 1

[T91331](http://developer.blender.org/T91331)

  - Needs Information from User: 7

[T91488](http://developer.blender.org/T91488),
[T91434](http://developer.blender.org/T91434),
[T91420](http://developer.blender.org/T91420),
[T91381](http://developer.blender.org/T91381),
[T91360](http://developer.blender.org/T91360),
[T91262](http://developer.blender.org/T91262),
[T89028](http://developer.blender.org/T89028)

  - Needs Information from Developers: 1

[T91269](http://developer.blender.org/T91269)

  - Diff submitted: 0

## Week 17 (Sep 20 - 26)

Spent the time on triaging the reports.  
Next week: triaging and spending some time on dev talk posts.

**Total actions on tracker: 144**

  - Confirmed: 20

[T91694](http://developer.blender.org/T91694),
[T91698](http://developer.blender.org/T91698),
[T91666](http://developer.blender.org/T91666),
[T91669](http://developer.blender.org/T91669),
[T91664](http://developer.blender.org/T91664),
[T91667](http://developer.blender.org/T91667),
[T91663](http://developer.blender.org/T91663),
[T91629](http://developer.blender.org/T91629),
[T91622](http://developer.blender.org/T91622),
[T91314](http://developer.blender.org/T91314),
[T91600](http://developer.blender.org/T91600),
[T91597](http://developer.blender.org/T91597),
[T91576](http://developer.blender.org/T91576),
[T91583](http://developer.blender.org/T91583),
[T91534](http://developer.blender.org/T91534),
[T91557](http://developer.blender.org/T91557),
[T91533](http://developer.blender.org/T91533),
[T91132](http://developer.blender.org/T91132),
[T91516](http://developer.blender.org/T91516),
[T91518](http://developer.blender.org/T91518)

  - Closed as Archived: 13

[T91488](http://developer.blender.org/T91488),
[T91262](http://developer.blender.org/T91262),
[T91623](http://developer.blender.org/T91623),
[T91412](http://developer.blender.org/T91412),
[T91336](http://developer.blender.org/T91336),
[T91381](http://developer.blender.org/T91381),
[T91418](http://developer.blender.org/T91418),
[T91434](http://developer.blender.org/T91434),
[T91601](http://developer.blender.org/T91601),
[T91558](http://developer.blender.org/T91558),
[T91380](http://developer.blender.org/T91380),
[T91097](http://developer.blender.org/T91097),
[T91525](http://developer.blender.org/T91525)

  - Closed as Duplicate: 8

[T91723](http://developer.blender.org/T91723),
[T91701](http://developer.blender.org/T91701),
[T91640](http://developer.blender.org/T91640),
[T91631](http://developer.blender.org/T91631),
[T91593](http://developer.blender.org/T91593),
[T91559](http://developer.blender.org/T91559),
[T91524](http://developer.blender.org/T91524),
[T91517](http://developer.blender.org/T91517)

  - Closed as Resolved: 1

[T91664](http://developer.blender.org/T91664)

  - Needs Information from User: 4

[T91696](http://developer.blender.org/T91696),
[T91628](http://developer.blender.org/T91628),
[T91548](http://developer.blender.org/T91548),
[T91539](http://developer.blender.org/T91539)

  - Needs Information from Developers: 1

[T91574](http://developer.blender.org/T91574)

  - Own Reported: 1

[T91622](http://developer.blender.org/T91622)

  - Helped to backport: 1

[T91643](http://developer.blender.org/T91643)

  - Diff submitted: 0

## Week 18 (Sep 27 - 03)

Spent the time on triaging the reports.  
Next week: triaging and spending some time on dev talk posts.

**Total actions on tracker: 146**

  - Confirmed: 18

[T91911](http://developer.blender.org/T91911),
[T91889](http://developer.blender.org/T91889),
[T91894](http://developer.blender.org/T91894),
[T91893](http://developer.blender.org/T91893),
[T91873](http://developer.blender.org/T91873),
[T91862](http://developer.blender.org/T91862),
[T91866](http://developer.blender.org/T91866),
[T91850](http://developer.blender.org/T91850),
[T91834](http://developer.blender.org/T91834),
[T91835](http://developer.blender.org/T91835),
[T91829](http://developer.blender.org/T91829),
[T91803](http://developer.blender.org/T91803),
[T91795](http://developer.blender.org/T91795),
[T91785](http://developer.blender.org/T91785),
[T91766](http://developer.blender.org/T91766),
[T91709](http://developer.blender.org/T91709),
[T91725](http://developer.blender.org/T91725),
[T91734](http://developer.blender.org/T91734)

  - Closed as Archived: 16

[T91905](http://developer.blender.org/T91905),
[T91696](http://developer.blender.org/T91696),
[T91628](http://developer.blender.org/T91628),
[T91858](http://developer.blender.org/T91858),
[T91853](http://developer.blender.org/T91853),
[T91760](http://developer.blender.org/T91760),
[T91270](http://developer.blender.org/T91270),
[T91578](http://developer.blender.org/T91578),
[T90468](http://developer.blender.org/T90468),
[T91781](http://developer.blender.org/T91781),
[T91801](http://developer.blender.org/T91801),
[T91748](http://developer.blender.org/T91748),
[T91539](http://developer.blender.org/T91539),
[T91740](http://developer.blender.org/T91740),
[T91738](http://developer.blender.org/T91738),
[T91185](http://developer.blender.org/T91185)

  - Closed as Duplicate: 10

[T91321](http://developer.blender.org/T91321),
[T91860](http://developer.blender.org/T91860),
[T91799](http://developer.blender.org/T91799),
[T91811](http://developer.blender.org/T91811),
[T91800](http://developer.blender.org/T91800),
[T91769](http://developer.blender.org/T91769),
[T91772](http://developer.blender.org/T91772),
[T91753](http://developer.blender.org/T91753),
[T91708](http://developer.blender.org/T91708),
[T91737](http://developer.blender.org/T91737)

  - Closed as Resolved: 3

[T91896](http://developer.blender.org/T91896),
[T91776](http://developer.blender.org/T91776),
[T91285](http://developer.blender.org/T91285)

  - Needs Information from User: 5

[T91782](http://developer.blender.org/T91782),
[T91730](http://developer.blender.org/T91730),
[T91715](http://developer.blender.org/T91715),
[T91896](http://developer.blender.org/T91896),
[T91799](http://developer.blender.org/T91799)

  - Needs Information from Developers: 0

<!-- end list -->

  - Diff submitted: 1

[D12728](http://developer.blender.org/D12728)

## Week 19 (Oct 04 - 10)

Spent the time on triaging the reports.  
Next week: triaging and spending some time on dev talk posts.

**Total actions on tracker: 143**

  - Confirmed: 14

[T92079](http://developer.blender.org/T92079),
[T87868](http://developer.blender.org/T87868),
[T92037](http://developer.blender.org/T92037),
[T92006](http://developer.blender.org/T92006),
[T92002](http://developer.blender.org/T92002),
[T92003](http://developer.blender.org/T92003),
[T91986](http://developer.blender.org/T91986),
[T91983](http://developer.blender.org/T91983),
[T91981](http://developer.blender.org/T91981),
[T91943](http://developer.blender.org/T91943),
[T91958](http://developer.blender.org/T91958),
[T91957](http://developer.blender.org/T91957),
[T91953](http://developer.blender.org/T91953),
[T91940](http://developer.blender.org/T91940)

  - Closed as Archived: 13

[T91842](http://developer.blender.org/T91842),
[T91984](http://developer.blender.org/T91984),
[T92031](http://developer.blender.org/T92031),
[T91782](http://developer.blender.org/T91782),
[T91816](http://developer.blender.org/T91816),
[T91991](http://developer.blender.org/T91991),
[T91526](http://developer.blender.org/T91526),
[T91961](http://developer.blender.org/T91961),
[T91361](http://developer.blender.org/T91361),
[T91561](http://developer.blender.org/T91561),
[T91730](http://developer.blender.org/T91730),
[T91586](http://developer.blender.org/T91586),
[T91705](http://developer.blender.org/T91705)

  - Closed as Duplicate: 13

[T92086](http://developer.blender.org/T92086),
[T92085](http://developer.blender.org/T92085),
[T92064](http://developer.blender.org/T92064),
[T92058](http://developer.blender.org/T92058),
[T92034](http://developer.blender.org/T92034),
[T92010](http://developer.blender.org/T92010),
[T92008](http://developer.blender.org/T92008),
[T91965](http://developer.blender.org/T91965),
[T91989](http://developer.blender.org/T91989),
[T91974](http://developer.blender.org/T91974),
[T91875](http://developer.blender.org/T91875),
[T91954](http://developer.blender.org/T91954),
[T91909](http://developer.blender.org/T91909)

  - Closed as Resolved: 2

[T92038](http://developer.blender.org/T92038),
[T92015](http://developer.blender.org/T92015)

  - Needs Information from User: 7

[T92033](http://developer.blender.org/T92033),
[T92019](http://developer.blender.org/T92019),
[T91975](http://developer.blender.org/T91975),
[T91980](http://developer.blender.org/T91980),
[T91964](http://developer.blender.org/T91964),
[T91943](http://developer.blender.org/T91943),
[T91939](http://developer.blender.org/T91939)

  - Needs Information from Developers: 3

[T92032](http://developer.blender.org/T92032),
[T91971](http://developer.blender.org/T91971),
[T91900](http://developer.blender.org/T91900)

  - Diff submitted (Updated): 1

[D12728](http://developer.blender.org/D12728)

## Week 20 (Oct 11 - 17)

Spent the time on triaging the reports. Took a break on Friday for the
festival (Also a national Holiday)  
Next week: triaging and spending some time on dev talk posts.

**Total actions on tracker: 149**

  - Confirmed: 18

[T92272](http://developer.blender.org/T92272),
[T92248](http://developer.blender.org/T92248),
[T92256](http://developer.blender.org/T92256),
[T92204](http://developer.blender.org/T92204),
[T92197](http://developer.blender.org/T92197),
[T92194](http://developer.blender.org/T92194),
[T92048](http://developer.blender.org/T92048),
[T92136](http://developer.blender.org/T92136),
[T92053](http://developer.blender.org/T92053),
[T92130](http://developer.blender.org/T92130),
[T92120](http://developer.blender.org/T92120),
[T92121](http://developer.blender.org/T92121),
[T92131](http://developer.blender.org/T92131),
[T92083](http://developer.blender.org/T92083),
[T92091](http://developer.blender.org/T92091),
[T92102](http://developer.blender.org/T92102),
[T92105](http://developer.blender.org/T92105),
[T92098](http://developer.blender.org/T92098)

  - Closed as Archived: 24

[T91689](http://developer.blender.org/T91689),
[T92071](http://developer.blender.org/T92071),
[T92282](http://developer.blender.org/T92282),
[T92281](http://developer.blender.org/T92281),
[T92280](http://developer.blender.org/T92280),
[T92276](http://developer.blender.org/T92276),
[T91975](http://developer.blender.org/T91975),
[T91988](http://developer.blender.org/T91988),
[T92019](http://developer.blender.org/T92019),
[T92023](http://developer.blender.org/T92023),
[T92033](http://developer.blender.org/T92033),
[T92055](http://developer.blender.org/T92055),
[T91548](http://developer.blender.org/T91548),
[T91286](http://developer.blender.org/T91286),
[T91980](http://developer.blender.org/T91980),
[T91908](http://developer.blender.org/T91908),
[T92175](http://developer.blender.org/T92175),
[T92161](http://developer.blender.org/T92161),
[T91948](http://developer.blender.org/T91948),
[T92075](http://developer.blender.org/T92075),
[T91715](http://developer.blender.org/T91715),
[T91621](http://developer.blender.org/T91621),
[T91688](http://developer.blender.org/T91688),
[T91916](http://developer.blender.org/T91916)

  - Closed as Duplicate: 10

[T92261](http://developer.blender.org/T92261),
[T92205](http://developer.blender.org/T92205),
[T92176](http://developer.blender.org/T92176),
[T81554](http://developer.blender.org/T81554),
[T92074](http://developer.blender.org/T92074),
[T92115](http://developer.blender.org/T92115),
[T92118](http://developer.blender.org/T92118),
[T92107](http://developer.blender.org/T92107),
[T92095](http://developer.blender.org/T92095),
[T92100](http://developer.blender.org/T92100)

  - Closed as Resolved: 4

[T92277](http://developer.blender.org/T92277),
[T92248](http://developer.blender.org/T92248),
[T92173](http://developer.blender.org/T92173),
[T92104](http://developer.blender.org/T92104)

  - Needs Information from User: 6

[T92273](http://developer.blender.org/T92273),
[T92257](http://developer.blender.org/T92257),
[T92195](http://developer.blender.org/T92195),
[T92170](http://developer.blender.org/T92170),
[T92078](http://developer.blender.org/T92078),
[T92051](http://developer.blender.org/T92051)

  - Needs Information from Developers: 0

<!-- end list -->

  - Diff submitted: 0

## Week 21 (Oct 18 - 24)

Spent the time on triaging the reports  
Next week: triaging and spending some time on dev talk posts.

**Total actions on tracker: 143**

  - Confirmed: 16

[T92405](http://developer.blender.org/T92405),
[T92406](http://developer.blender.org/T92406),
[T92404](http://developer.blender.org/T92404),
[T92372](http://developer.blender.org/T92372),
[T92371](http://developer.blender.org/T92371),
[T92369](http://developer.blender.org/T92369),
[T92362](http://developer.blender.org/T92362),
[T92348](http://developer.blender.org/T92348),
[T92327](http://developer.blender.org/T92327),
[T92361](http://developer.blender.org/T92361),
[T92355](http://developer.blender.org/T92355),
[T92241](http://developer.blender.org/T92241),
[T92316](http://developer.blender.org/T92316),
[T92313](http://developer.blender.org/T92313),
[T92221](http://developer.blender.org/T92221),
[T92268](http://developer.blender.org/T92268)

  - Closed as Archived: 16

[T91521](http://developer.blender.org/T91521),
[T92257](http://developer.blender.org/T92257),
[T92429](http://developer.blender.org/T92429),
[T92245](http://developer.blender.org/T92245),
[T92431](http://developer.blender.org/T92431),
[T92195](http://developer.blender.org/T92195),
[T92187](http://developer.blender.org/T92187),
[T92051](http://developer.blender.org/T92051),
[T92133](http://developer.blender.org/T92133),
[T92170](http://developer.blender.org/T92170),
[T92317](http://developer.blender.org/T92317),
[T92304](http://developer.blender.org/T92304),
[T92322](http://developer.blender.org/T92322),
[T92070](http://developer.blender.org/T92070),
[T92300](http://developer.blender.org/T92300),
[T92092](http://developer.blender.org/T92092),

  - Closed as Duplicate: 9

[T92428](http://developer.blender.org/T92428),
[T92373](http://developer.blender.org/T92373),
[T92358](http://developer.blender.org/T92358),
[T92356](http://developer.blender.org/T92356),
[T92337](http://developer.blender.org/T92337),
[T92319](http://developer.blender.org/T92319),
[T92231](http://developer.blender.org/T92231),
[T92297](http://developer.blender.org/T92297),
[T92267](http://developer.blender.org/T92267)

  - Closed as Resolved: 0

<!-- end list -->

  - Needs Information from User: 11

[T92437](http://developer.blender.org/T92437),
[T92420](http://developer.blender.org/T92420),
[T92403](http://developer.blender.org/T92403),
[T92395](http://developer.blender.org/T92395),
[T92390](http://developer.blender.org/T92390),
[T92385](http://developer.blender.org/T92385),
[T92379](http://developer.blender.org/T92379),
[T92338](http://developer.blender.org/T92338),
[T92330](http://developer.blender.org/T92330),
[T92307](http://developer.blender.org/T92307),
[T92262](http://developer.blender.org/T92262)

  - Needs Information from Developers: 0

<!-- end list -->

  - Diff submitted: 0

<!-- end list -->

  - Own Reported: 2

[T92367](http://developer.blender.org/T92367),
[T92313](http://developer.blender.org/T92313)

## Week 22 (Oct 25 - 31)

**Total actions on tracker: 153**

Spent the time on triaging the reports. Had exams from Monday to
Wednesday so shifted working time to weekend  
Next week: triaging and spending some time on dev talk posts.

  - Confirmed: 21

[T92652](http://developer.blender.org/T92652),
[T92655](http://developer.blender.org/T92655),
[T92663](http://developer.blender.org/T92663),
[T92662](http://developer.blender.org/T92662),
[T92631](http://developer.blender.org/T92631),
[T92636](http://developer.blender.org/T92636),
[T92576](http://developer.blender.org/T92576),
[T92590](http://developer.blender.org/T92590),
[T92592](http://developer.blender.org/T92592),
[T92595](http://developer.blender.org/T92595),
[T92533](http://developer.blender.org/T92533),
[T92552](http://developer.blender.org/T92552),
[T92538](http://developer.blender.org/T92538),
[T90629](http://developer.blender.org/T90629),
[T81064](http://developer.blender.org/T81064),
[T92539](http://developer.blender.org/T92539),
[T92527](http://developer.blender.org/T92527),
[T92526](http://developer.blender.org/T92526),
[T91261](http://developer.blender.org/T91261),
[T92489](http://developer.blender.org/T92489),
[T92453](http://developer.blender.org/T92453)

  - Closed as Archived: 24

[T92651](http://developer.blender.org/T92651),
[T92207](http://developer.blender.org/T92207),
[T92437](http://developer.blender.org/T92437),
[T92642](http://developer.blender.org/T92642),
[T92302](http://developer.blender.org/T92302),
[T92420](http://developer.blender.org/T92420),
[T92379](http://developer.blender.org/T92379),
[T92390](http://developer.blender.org/T92390),
[T92602](http://developer.blender.org/T92602),
[T92560](http://developer.blender.org/T92560),
[T92599](http://developer.blender.org/T92599),
[T92199](http://developer.blender.org/T92199),
[T92385](http://developer.blender.org/T92385),
[T92553](http://developer.blender.org/T92553),
[T92543](http://developer.blender.org/T92543),
[T92183](http://developer.blender.org/T92183),
[T92273](http://developer.blender.org/T92273),
[T92342](http://developer.blender.org/T92342),
[T92211](http://developer.blender.org/T92211),
[T92242](http://developer.blender.org/T92242),
[T92262](http://developer.blender.org/T92262),
[T92283](http://developer.blender.org/T92283),
[T92294](http://developer.blender.org/T92294),
[T92307](http://developer.blender.org/T92307)

  - Closed as Duplicate: 12

[T92685](http://developer.blender.org/T92685),
[T92646](http://developer.blender.org/T92646),
[T92648](http://developer.blender.org/T92648),
[T92660](http://developer.blender.org/T92660),
[T92637](http://developer.blender.org/T92637),
[T91038](http://developer.blender.org/T91038),
[T92547](http://developer.blender.org/T92547),
[T92562](http://developer.blender.org/T92562),
[T92558](http://developer.blender.org/T92558),
[T92557](http://developer.blender.org/T92557),
[T92544](http://developer.blender.org/T92544),
[T92454](http://developer.blender.org/T92454)

  - Closed as Resolved: 1

[T92603](http://developer.blender.org/T92603)

  - Needs Information from User: 5

[T92676](http://developer.blender.org/T92676),
[T92638](http://developer.blender.org/T92638),
[T92556](http://developer.blender.org/T92556),
[T92535](http://developer.blender.org/T92535),
[T92554](http://developer.blender.org/T92554),
[T92521](http://developer.blender.org/T92521)

  - Needs Information from Developers: 0

<!-- end list -->

  - Diff submitted: 0

## Week 23 (Nov 01 - 07)

**Total actions on tracker: 152**

Spent the time on triaging the reports. Less active on Friday as I was
travelling away from home  
Next week: triaging and spending some time on dev talk posts.  
Travelling back to home so I won't be able to join on Monday. Will shift
working to Saturday.

  - Confirmed: 17

[T92871](http://developer.blender.org/T92871),
[T92848](http://developer.blender.org/T92848),
[T90659](http://developer.blender.org/T90659),
[T92799](http://developer.blender.org/T92799),
[T92802](http://developer.blender.org/T92802),
[T92777](http://developer.blender.org/T92777),
[T92612](http://developer.blender.org/T92612),
[T92612](http://developer.blender.org/T92612),
[T92771](http://developer.blender.org/T92771),
[T92772](http://developer.blender.org/T92772),
[T92775](http://developer.blender.org/T92775),
[T92736](http://developer.blender.org/T92736),
[T92750](http://developer.blender.org/T92750),
[T92677](http://developer.blender.org/T92677),
[T92705](http://developer.blender.org/T92705),
[T92680](http://developer.blender.org/T92680),
[T92682](http://developer.blender.org/T92682)

  - Closed as Archived: 21

[T91951](http://developer.blender.org/T91951),
[T92441](http://developer.blender.org/T92441),
[T92897](http://developer.blender.org/T92897),
[T92638](http://developer.blender.org/T92638),
[T92644](http://developer.blender.org/T92644),
[T92833](http://developer.blender.org/T92833),
[T92554](http://developer.blender.org/T92554),
[T92556](http://developer.blender.org/T92556),
[T92828](http://developer.blender.org/T92828),
[T92330](http://developer.blender.org/T92330),
[T92411](http://developer.blender.org/T92411),
[T92521](http://developer.blender.org/T92521),
[T92523](http://developer.blender.org/T92523),
[T92237](http://developer.blender.org/T92237),
[T92021](http://developer.blender.org/T92021),
[T92451](http://developer.blender.org/T92451),
[T92776](http://developer.blender.org/T92776),
[T92725](http://developer.blender.org/T92725),
[T92587](http://developer.blender.org/T92587),
[T92440](http://developer.blender.org/T92440),
[T92679](http://developer.blender.org/T92679)

  - Closed as Duplicate: 9

[T92895](http://developer.blender.org/T92895),
[T92899](http://developer.blender.org/T92899),
[T92846](http://developer.blender.org/T92846),
[T92753](http://developer.blender.org/T92753),
[T92769](http://developer.blender.org/T92769),
[T92735](http://developer.blender.org/T92735),
[T92741](http://developer.blender.org/T92741),
[T92745](http://developer.blender.org/T92745),
[T92690](http://developer.blender.org/T92690)

  - Closed as Resolved: 2

[T92677](http://developer.blender.org/T92677),
[T92663](http://developer.blender.org/T92663)

  - Needs Information from User: 9

[T92896](http://developer.blender.org/T92896),
[T92774](http://developer.blender.org/T92774),
[T92707](http://developer.blender.org/T92707),
[T92695](http://developer.blender.org/T92695),
[T92686](http://developer.blender.org/T92686),
[T92497](http://developer.blender.org/T92497),
[T92062](http://developer.blender.org/T92062),
[T90659](http://developer.blender.org/T90659),
[T89370](http://developer.blender.org/T89370)

  - Needs Information from Developers: 1

[T92844](http://developer.blender.org/T92844)

  - Diff submitted: 1

[D13056](http://developer.blender.org/D13056)

## Week 24 (Nov 08 - 14)

Spent the time on triaging the reports. Inactive on Monday so shifted it
to Saturday  
Checked a couple of patches (tagged module, asked developer to review
them, etc).  
Next week: triaging and spending some time on dev talk posts.

**Total actions on tracker: 128**

  - Confirmed: 11

[T92661](http://developer.blender.org/T92661),
[T92948](http://developer.blender.org/T92948),
[T92892](http://developer.blender.org/T92892),
[T92906](http://developer.blender.org/T92906),
[T92962](http://developer.blender.org/T92962),
[T92953](http://developer.blender.org/T92953),
[T92954](http://developer.blender.org/T92954),
[T92934](http://developer.blender.org/T92934),
[T92952](http://developer.blender.org/T92952),
[T92939](http://developer.blender.org/T92939),
[T92950](http://developer.blender.org/T92950)

  - Closed as Archived: 11

[T92719](http://developer.blender.org/T92719),
[T92496](http://developer.blender.org/T92496),
[T92806](http://developer.blender.org/T92806),
[T89370](http://developer.blender.org/T89370),
[T92062](http://developer.blender.org/T92062),
[T92695](http://developer.blender.org/T92695),
[T92676](http://developer.blender.org/T92676),
[T92774](http://developer.blender.org/T92774),
[T92912](http://developer.blender.org/T92912),
[T92896](http://developer.blender.org/T92896),
[T92933](http://developer.blender.org/T92933)

  - Closed as Duplicate: 9

[T93070](http://developer.blender.org/T93070),
[T93048](http://developer.blender.org/T93048),
[T93028](http://developer.blender.org/T93028),
[T93021](http://developer.blender.org/T93021),
[T92996](http://developer.blender.org/T92996),
[T92966](http://developer.blender.org/T92966),
[T92974](http://developer.blender.org/T92974),
[T92937](http://developer.blender.org/T92937),
[T92936](http://developer.blender.org/T92936)

  - Closed as Resolved: 3

[T92997](http://developer.blender.org/T92997),
[T90659](http://developer.blender.org/T90659),
[T91835](http://developer.blender.org/T91835)

  - Needs Information from User: 12

[T93049](http://developer.blender.org/T93049),
[T93042](http://developer.blender.org/T93042),
[T93041](http://developer.blender.org/T93041),
[T93038](http://developer.blender.org/T93038),
[T93014](http://developer.blender.org/T93014),
[T93000](http://developer.blender.org/T93000),
[T92976](http://developer.blender.org/T92976),
[T92929](http://developer.blender.org/T92929),
[T92920](http://developer.blender.org/T92920),
[T92289](http://developer.blender.org/T92289),
[T87331](http://developer.blender.org/T87331),
[T77802](http://developer.blender.org/T77802)

  - Needs Information from Developers: 1

[T92998](http://developer.blender.org/T92998)

  - Diff submitted: 1

[D13199](http://developer.blender.org/D13199)

## Week 25 (Nov 15 - 21)

Spent the time on triaging the reports. Helped in
[D13136](http://developer.blender.org/D13136)  
Next week: triaging and spending some time on dev talk posts.

**Total actions on tracker: 149**

  - Confirmed: 24

[T93242](http://developer.blender.org/T93242),
[T93250](http://developer.blender.org/T93250),
[T93255](http://developer.blender.org/T93255),
[T93256](http://developer.blender.org/T93256),
[T93231](http://developer.blender.org/T93231),
[T93215](http://developer.blender.org/T93215),
[T93190](http://developer.blender.org/T93190),
[T93201](http://developer.blender.org/T93201),
[T93194](http://developer.blender.org/T93194),
[T93189](http://developer.blender.org/T93189),
[T93184](http://developer.blender.org/T93184),
[T93122](http://developer.blender.org/T93122),
[T93158](http://developer.blender.org/T93158),
[T93172](http://developer.blender.org/T93172),
[T93169](http://developer.blender.org/T93169),
[T93179](http://developer.blender.org/T93179),
[T93154](http://developer.blender.org/T93154),
[T93152](http://developer.blender.org/T93152),
[T93100](http://developer.blender.org/T93100),
[T93102](http://developer.blender.org/T93102),
[T93064](http://developer.blender.org/T93064),
[T93062](http://developer.blender.org/T93062),
[T93076](http://developer.blender.org/T93076),
[T93080](http://developer.blender.org/T93080)

  - Closed as Archived: 18

[T93228](http://developer.blender.org/T93228),
[T92289](http://developer.blender.org/T92289),
[T92686](http://developer.blender.org/T92686),
[T93042](http://developer.blender.org/T93042),
[T93049](http://developer.blender.org/T93049),
[T92756](http://developer.blender.org/T92756),
[T92918](http://developer.blender.org/T92918),
[T93000](http://developer.blender.org/T93000),
[T93181](http://developer.blender.org/T93181),
[T92935](http://developer.blender.org/T92935),
[T92976](http://developer.blender.org/T92976),
[T92955](http://developer.blender.org/T92955),
[T93144](http://developer.blender.org/T93144),
[T92678](http://developer.blender.org/T92678),
[T92931](http://developer.blender.org/T92931),
[T92920](http://developer.blender.org/T92920),
[T93072](http://developer.blender.org/T93072),
[T92471](http://developer.blender.org/T92471)

  - Closed as Duplicate: 7

[T93260](http://developer.blender.org/T93260),
[T93241](http://developer.blender.org/T93241),
[T93207](http://developer.blender.org/T93207),
[T93057](http://developer.blender.org/T93057),
[T93132](http://developer.blender.org/T93132),
[T93135](http://developer.blender.org/T93135),
[T93041](http://developer.blender.org/T93041)

  - Closed as Resolved: 0

<!-- end list -->

  - Needs Information from User: 11

[T93193](http://developer.blender.org/T93193),
[T93164](http://developer.blender.org/T93164),
[T93149](http://developer.blender.org/T93149),
[T93133](http://developer.blender.org/T93133),
[T93119](http://developer.blender.org/T93119),
[T93112](http://developer.blender.org/T93112),
[T93104](http://developer.blender.org/T93104),
[T93101](http://developer.blender.org/T93101),
[T93079](http://developer.blender.org/T93079),
[T93077](http://developer.blender.org/T93077),
[T93069](http://developer.blender.org/T93069)

  - Needs Information from Developers: 1

[T93200](http://developer.blender.org/T93200)

  - Diff submitted: 0

## Week 26 (Nov 22 - 28)

Spent the time on triaging the reports.  
Next week: triaging and spending some time on dev talk posts.

**Total actions on tracker: 144**

  - Confirmed: 19

[T92929](http://developer.blender.org/T92929),
[T93431](http://developer.blender.org/T93431),
[T93422](http://developer.blender.org/T93422),
[T93401](http://developer.blender.org/T93401),
[T93368](http://developer.blender.org/T93368),
[T93398](http://developer.blender.org/T93398),
[T93117](http://developer.blender.org/T93117),
[T93381](http://developer.blender.org/T93381),
[T93384](http://developer.blender.org/T93384),
[T93360](http://developer.blender.org/T93360),
[T93357](http://developer.blender.org/T93357),
[T93334](http://developer.blender.org/T93334),
[T93355](http://developer.blender.org/T93355),
[T93294](http://developer.blender.org/T93294),
[T93316](http://developer.blender.org/T93316),
[T93331](http://developer.blender.org/T93331),
[T93310](http://developer.blender.org/T93310),
[T93248](http://developer.blender.org/T93248),
[T93278](http://developer.blender.org/T93278)

  - Closed as Archived: 19

[T92376](http://developer.blender.org/T92376),
[T92911](http://developer.blender.org/T92911),
[T93149](http://developer.blender.org/T93149),
[T93387](http://developer.blender.org/T93387),
[T93385](http://developer.blender.org/T93385),
[T93093](http://developer.blender.org/T93093),
[T93133](http://developer.blender.org/T93133),
[T93016](http://developer.blender.org/T93016),
[T93287](http://developer.blender.org/T93287),
[T93079](http://developer.blender.org/T93079),
[T93104](http://developer.blender.org/T93104),
[T93112](http://developer.blender.org/T93112),
[T93119](http://developer.blender.org/T93119),
[T93300](http://developer.blender.org/T93300),
[T93273](http://developer.blender.org/T93273),
[T93261](http://developer.blender.org/T93261),
[T87331](http://developer.blender.org/T87331),
[T93027](http://developer.blender.org/T93027),
[T93038](http://developer.blender.org/T93038)

  - Closed as Duplicate: 7

[T93359](http://developer.blender.org/T93359),
[T93351](http://developer.blender.org/T93351),
[T93299](http://developer.blender.org/T93299),
[T93280](http://developer.blender.org/T93280),
[T93247](http://developer.blender.org/T93247),
[T93275](http://developer.blender.org/T93275),
[T93276](http://developer.blender.org/T93276)

  - Closed as Resolved: 2

[T93366](http://developer.blender.org/T93366),
[T85093](http://developer.blender.org/T85093)

  - Needs Information from User: 6

[T93434](http://developer.blender.org/T93434),
[T93426](http://developer.blender.org/T93426),
[T93403](http://developer.blender.org/T93403),
[T93373](http://developer.blender.org/T93373),
[T93347](http://developer.blender.org/T93347),
[T93313](http://developer.blender.org/T93313)

  - Needs Information from Developers: 0

<!-- end list -->

  - Own Reported: 2

[T93398](http://developer.blender.org/T93398),
[T93355](http://developer.blender.org/T93355)

  - Diff submitted: 1

[D13398](http://developer.blender.org/D13398)

## Week 27 (Nov 29 - 05)

This week I spent time on triaging.  
Next week: triaging and spending some time on dev talk posts.

**Total actions on tracker: 145**

  - Confirmed: 20

[T93691](http://developer.blender.org/T93691),
[T93644](http://developer.blender.org/T93644),
[T93634](http://developer.blender.org/T93634),
[T93568](http://developer.blender.org/T93568),
[T93585](http://developer.blender.org/T93585),
[T93578](http://developer.blender.org/T93578),
[T93512](http://developer.blender.org/T93512),
[T93511](http://developer.blender.org/T93511),
[T93555](http://developer.blender.org/T93555),
[T93546](http://developer.blender.org/T93546),
[T93541](http://developer.blender.org/T93541),
[T93536](http://developer.blender.org/T93536),
[T93526](http://developer.blender.org/T93526),
[T93521](http://developer.blender.org/T93521),
[T93434](http://developer.blender.org/T93434),
[T93412](http://developer.blender.org/T93412),
[T93403](http://developer.blender.org/T93403),
[T93508](http://developer.blender.org/T93508),
[T93488](http://developer.blender.org/T93488),
[T93442](http://developer.blender.org/T93442)

  - Closed as Archived: 12

[T93373](http://developer.blender.org/T93373),
[T93164](http://developer.blender.org/T93164),
[T93336](http://developer.blender.org/T93336),
[T93343](http://developer.blender.org/T93343),
[T93347](http://developer.blender.org/T93347),
[T93530](http://developer.blender.org/T93530),
[T93547](http://developer.blender.org/T93547),
[T93222](http://developer.blender.org/T93222),
[T93313](http://developer.blender.org/T93313),
[T93487](http://developer.blender.org/T93487),
[T93415](http://developer.blender.org/T93415),
[T93465](http://developer.blender.org/T93465)

  - Closed as Duplicate: 13

[T93697](http://developer.blender.org/T93697),
[T93694](http://developer.blender.org/T93694),
[T93693](http://developer.blender.org/T93693),
[T93597](http://developer.blender.org/T93597),
[T93571](http://developer.blender.org/T93571),
[T93564](http://developer.blender.org/T93564),
[T93221](http://developer.blender.org/T93221),
[T93527](http://developer.blender.org/T93527),
[T93537](http://developer.blender.org/T93537),
[T93532](http://developer.blender.org/T93532),
[T93432](http://developer.blender.org/T93432),
[T93438](http://developer.blender.org/T93438),
[T92676](http://developer.blender.org/T92676)

  - Closed as Resolved: 6

[T93696](http://developer.blender.org/T93696),
[T93593](http://developer.blender.org/T93593),
[T93427](http://developer.blender.org/T93427),
[T93430](http://developer.blender.org/T93430),
[T93426](http://developer.blender.org/T93426),
[T93431](http://developer.blender.org/T93431)

  - Needs Information from User: 10

[T93690](http://developer.blender.org/T93690),
[T93641](http://developer.blender.org/T93641),
[T93638](http://developer.blender.org/T93638),
[T93581](http://developer.blender.org/T93581),
[T93529](http://developer.blender.org/T93529),
[T93496](http://developer.blender.org/T93496),
[T93492](http://developer.blender.org/T93492),
[T93460](http://developer.blender.org/T93460),
[T93458](http://developer.blender.org/T93458),
[T93441](http://developer.blender.org/T93441)

  - Needs Information from Developers: 0

<!-- end list -->

  - Diff submitted (updated): 1

[D13398](http://developer.blender.org/D13398)

## Week 28 (Dec 06 - 12)

This week I spent time on triaging. Less active on Tuesday (wasn't at
home).  
Next week: triaging. Will spend some time on dev talk posts.

**Total actions on tracker: 149**

  - Confirmed: 20

[T93963](http://developer.blender.org/T93963),
[T93929](http://developer.blender.org/T93929),
[T93931](http://developer.blender.org/T93931),
[T93930](http://developer.blender.org/T93930),
[T93925](http://developer.blender.org/T93925),
[T93673](http://developer.blender.org/T93673),
[T93629](http://developer.blender.org/T93629),
[T93892](http://developer.blender.org/T93892),
[T93774](http://developer.blender.org/T93774),
[T93855](http://developer.blender.org/T93855),
[T93863](http://developer.blender.org/T93863),
[T93845](http://developer.blender.org/T93845),
[T93813](http://developer.blender.org/T93813),
[T93773](http://developer.blender.org/T93773),
[T93683](http://developer.blender.org/T93683),
[T93749](http://developer.blender.org/T93749),
[T93728](http://developer.blender.org/T93728),
[T93738](http://developer.blender.org/T93738),
[T93727](http://developer.blender.org/T93727),
[T93733](http://developer.blender.org/T93733)

  - Closed as Archived: 16

[T93326](http://developer.blender.org/T93326),
[T93444](http://developer.blender.org/T93444),
[T93581](http://developer.blender.org/T93581),
[T93926](http://developer.blender.org/T93926),
[T93443](http://developer.blender.org/T93443),
[T93458](http://developer.blender.org/T93458),
[T93529](http://developer.blender.org/T93529),
[T93492](http://developer.blender.org/T93492),
[T93841](http://developer.blender.org/T93841),
[T93794](http://developer.blender.org/T93794),
[T93460](http://developer.blender.org/T93460),
[T93690](http://developer.blender.org/T93690),
[T93747](http://developer.blender.org/T93747),
[T93205](http://developer.blender.org/T93205),
[T93377](http://developer.blender.org/T93377),
[T93737](http://developer.blender.org/T93737)

  - Closed as Duplicate: 12

[T93973](http://developer.blender.org/T93973),
[T93970](http://developer.blender.org/T93970),
[T93955](http://developer.blender.org/T93955),
[T93938](http://developer.blender.org/T93938),
[T93937](http://developer.blender.org/T93937),
[T93902](http://developer.blender.org/T93902),
[T93676](http://developer.blender.org/T93676),
[T93860](http://developer.blender.org/T93860),
[T93812](http://developer.blender.org/T93812),
[T93752](http://developer.blender.org/T93752),
[T93707](http://developer.blender.org/T93707),
[T93709](http://developer.blender.org/T93709)

  - Closed as Resolved: 1

[T93831](http://developer.blender.org/T93831)

  - Needs Information from User: 13

[T93923](http://developer.blender.org/T93923),
[T93894](http://developer.blender.org/T93894),
[T93859](http://developer.blender.org/T93859),
[T93857](http://developer.blender.org/T93857),
[T93848](http://developer.blender.org/T93848),
[T93846](http://developer.blender.org/T93846),
[T93825](http://developer.blender.org/T93825),
[T93793](http://developer.blender.org/T93793),
[T93741](http://developer.blender.org/T93741),
[T93730](http://developer.blender.org/T93730),
[T93713](http://developer.blender.org/T93713),
[T93306](http://developer.blender.org/T93306),
[T92884](http://developer.blender.org/T92884)

  - Needs Information from Developers: 0

<!-- end list -->

  - Diff submitted: 1

[D13544](http://developer.blender.org/D13544)

## Week 29 (Dec 13 - 19)

This week I spent time on triaging.  
Next week: triaging. Will spend some time on dev talk posts.

**Total actions on tracker: 161**

  - Confirmed: 32

[T94241](http://developer.blender.org/T94241),
[T94208](http://developer.blender.org/T94208),
[T94243](http://developer.blender.org/T94243),
[T94149](http://developer.blender.org/T94149),
[T93782](http://developer.blender.org/T93782),
[T94155](http://developer.blender.org/T94155),
[T94145](http://developer.blender.org/T94145),
[T94156](http://developer.blender.org/T94156),
[T94166](http://developer.blender.org/T94166),
[T94173](http://developer.blender.org/T94173),
[T94109](http://developer.blender.org/T94109),
[T94122](http://developer.blender.org/T94122),
[T94140](http://developer.blender.org/T94140),
[T94115](http://developer.blender.org/T94115),
[T94085](http://developer.blender.org/T94085),
[T94089](http://developer.blender.org/T94089),
[T94090](http://developer.blender.org/T94090),
[T94097](http://developer.blender.org/T94097),
[T94093](http://developer.blender.org/T94093),
[T94095](http://developer.blender.org/T94095),
[T94061](http://developer.blender.org/T94061),
[T94016](http://developer.blender.org/T94016),
[T94038](http://developer.blender.org/T94038),
[T94041](http://developer.blender.org/T94041),
[T94045](http://developer.blender.org/T94045),
[T93944](http://developer.blender.org/T93944),
[T93995](http://developer.blender.org/T93995),
[T93994](http://developer.blender.org/T93994),
[T93979](http://developer.blender.org/T93979),
[T93974](http://developer.blender.org/T93974),
[T93975](http://developer.blender.org/T93975),
[T93965](http://developer.blender.org/T93965)

  - Closed as Archived: 17

[T94124](http://developer.blender.org/T94124),
[T94141](http://developer.blender.org/T94141),
[T93553](http://developer.blender.org/T93553),
[T93746](http://developer.blender.org/T93746),
[T93825](http://developer.blender.org/T93825),
[T93514](http://developer.blender.org/T93514),
[T93621](http://developer.blender.org/T93621),
[T93801](http://developer.blender.org/T93801),
[T93808](http://developer.blender.org/T93808),
[T93848](http://developer.blender.org/T93848),
[T93857](http://developer.blender.org/T93857),
[T94126](http://developer.blender.org/T94126),
[T94065](http://developer.blender.org/T94065),
[T94014](http://developer.blender.org/T94014),
[T93713](http://developer.blender.org/T93713),
[T93638](http://developer.blender.org/T93638),
[T93641](http://developer.blender.org/T93641)

  - Closed as Duplicate: 11

[T94211](http://developer.blender.org/T94211),
[T94229](http://developer.blender.org/T94229),
[T94138](http://developer.blender.org/T94138),
[T94098](http://developer.blender.org/T94098),
[T94087](http://developer.blender.org/T94087),
[T84205](http://developer.blender.org/T84205),
[T89056](http://developer.blender.org/T89056),
[T94047](http://developer.blender.org/T94047),
[T94019](http://developer.blender.org/T94019),
[T93992](http://developer.blender.org/T93992),
[T94006](http://developer.blender.org/T94006)

  - Closed as Resolved: 0

<!-- end list -->

  - Needs Information from User(open reports): 5

[T94130](http://developer.blender.org/T94130),
[T94096](http://developer.blender.org/T94096),
[T94091](http://developer.blender.org/T94091),
[T94004](http://developer.blender.org/T94004),
[T93972](http://developer.blender.org/T93972)

  - Needs Information from Developers: 0
  - Diff submitted: 0

## Week 30 (Dec 20 - 26)

This week I spent time on triaging.  
Next week: triaging. Will be active on Saturday instead of Monday (away
from Home ATM). Spending some time on dev talk posts.

**Total actions on tracker: 135**

  - Confirmed: 19

[T94387](http://developer.blender.org/T94387),
[T94375](http://developer.blender.org/T94375),
[T94366](http://developer.blender.org/T94366),
[T94017](http://developer.blender.org/T94017),
[T94196](http://developer.blender.org/T94196),
[T94008](http://developer.blender.org/T94008),
[T94292](http://developer.blender.org/T94292),
[T94313](http://developer.blender.org/T94313),
[T93772](http://developer.blender.org/T93772),
[T93577](http://developer.blender.org/T93577),
[T94299](http://developer.blender.org/T94299),
[T94237](http://developer.blender.org/T94237),
[T94197](http://developer.blender.org/T94197),
[T94267](http://developer.blender.org/T94267),
[T94264](http://developer.blender.org/T94264),
[T94207](http://developer.blender.org/T94207),
[T94117](http://developer.blender.org/T94117),
[T94113](http://developer.blender.org/T94113),
[T94103](http://developer.blender.org/T94103)

  - Closed as Archived: 22

[T94377](http://developer.blender.org/T94377),
[T94359](http://developer.blender.org/T94359),
[T93441](http://developer.blender.org/T93441),
[T93741](http://developer.blender.org/T93741),
[T93760](http://developer.blender.org/T93760),
[T94058](http://developer.blender.org/T94058),
[T94157](http://developer.blender.org/T94157),
[T92884](http://developer.blender.org/T92884),
[T94081](http://developer.blender.org/T94081),
[T94091](http://developer.blender.org/T94091),
[T94096](http://developer.blender.org/T94096),
[T93842](http://developer.blender.org/T93842),
[T94273](http://developer.blender.org/T94273),
[T94272](http://developer.blender.org/T94272),
[T94070](http://developer.blender.org/T94070),
[T93730](http://developer.blender.org/T93730),
[T93820](http://developer.blender.org/T93820),
[T93846](http://developer.blender.org/T93846),
[T93859](http://developer.blender.org/T93859),
[T93878](http://developer.blender.org/T93878),
[T93894](http://developer.blender.org/T93894),
[T93923](http://developer.blender.org/T93923)

  - Closed as Duplicate: 6

[T94361](http://developer.blender.org/T94361),
[T94180](http://developer.blender.org/T94180),
[T94343](http://developer.blender.org/T94343),
[T94325](http://developer.blender.org/T94325),
[T94269](http://developer.blender.org/T94269),
[T94201](http://developer.blender.org/T94201)

  - Closed as Resolved: 1

[T93930](http://developer.blender.org/T93930)

  - Needs Information from User (open): 3

[T94358](http://developer.blender.org/T94358),
[T94285](http://developer.blender.org/T94285),
[T93698](http://developer.blender.org/T93698)

  - Needs Information from Developers: 1

[T94117](http://developer.blender.org/T94117)

  - Own Reported: 1

[T94360](http://developer.blender.org/T94360)

  - Diff submitted: 0

## Week 31 (Dec 27 - 02)

This week: triaging. Wasn't at home on Monday so I worked on weekend  
Next week: triaging

**Total actions on tracker: 126**

  - Confirmed: 18

[T94544](http://developer.blender.org/T94544),
[T94542](http://developer.blender.org/T94542),
[T94499](http://developer.blender.org/T94499),
[T94495](http://developer.blender.org/T94495),
[T94418](http://developer.blender.org/T94418),
[T94471](http://developer.blender.org/T94471),
[T94486](http://developer.blender.org/T94486),
[T94463](http://developer.blender.org/T94463),
[T94480](http://developer.blender.org/T94480),
[T94464](http://developer.blender.org/T94464),
[T94465](http://developer.blender.org/T94465),
[T94178](http://developer.blender.org/T94178),
[T94441](http://developer.blender.org/T94441),
[T94448](http://developer.blender.org/T94448),
[T94411](http://developer.blender.org/T94411),
[T94392](http://developer.blender.org/T94392),
[T94384](http://developer.blender.org/T94384),
[T94380](http://developer.blender.org/T94380)

  - Closed as Archived: 14

[T94562](http://developer.blender.org/T94562),
[T94285](http://developer.blender.org/T94285),
[T85837](http://developer.blender.org/T85837),
[T94447](http://developer.blender.org/T94447),
[T94132](http://developer.blender.org/T94132),
[T94411](http://developer.blender.org/T94411),
[T93800](http://developer.blender.org/T93800),
[T94403](http://developer.blender.org/T94403),
[T90539](http://developer.blender.org/T90539),
[T94257](http://developer.blender.org/T94257),
[T94000](http://developer.blender.org/T94000),
[T94002](http://developer.blender.org/T94002),
[T94012](http://developer.blender.org/T94012),
[T94046](http://developer.blender.org/T94046)

  - Closed as Duplicate: 8

[T94503](http://developer.blender.org/T94503),
[T94539](http://developer.blender.org/T94539),
[T94504](http://developer.blender.org/T94504),
[T94500](http://developer.blender.org/T94500),
[T94485](http://developer.blender.org/T94485),
[T94483](http://developer.blender.org/T94483),
[T94449](http://developer.blender.org/T94449),
[T94393](http://developer.blender.org/T94393)

  - Closed as Resolved: 0

<!-- end list -->

  - Needs Information from User (open): 4

[T94450](http://developer.blender.org/T94450),
[T94401](http://developer.blender.org/T94401),
[T94379](http://developer.blender.org/T94379),
[T94198](http://developer.blender.org/T94198)

  - Needs Information from Developers: 1

[T94554](http://developer.blender.org/T94554)

  - Diff submitted: 2

1\. [D13697](http://developer.blender.org/D13697): Fix T89252: Unable to
interact with Stabilize stroke text on click  
2\. [D13705](http://developer.blender.org/D13705): Fix T90819: Reset to
default value sets offset value to 1

# 2022

## Week 32 (Jan 03 - 09)

This week: Triaging. Wasn't active on Monday (had fever)  
Next week: Triaging

**Total actions on tracker: 164**

  - Confirmed: 13

[T94758](http://developer.blender.org/T94758),
[T94735](http://developer.blender.org/T94735),
[T94707](http://developer.blender.org/T94707),
[T94363](http://developer.blender.org/T94363),
[T94413](http://developer.blender.org/T94413),
[T94355](http://developer.blender.org/T94355),
[T94656](http://developer.blender.org/T94656),
[T94659](http://developer.blender.org/T94659),
[T94641](http://developer.blender.org/T94641),
[T94545](http://developer.blender.org/T94545),
[T94605](http://developer.blender.org/T94605),
[T94599](http://developer.blender.org/T94599),
[T94008](http://developer.blender.org/T94008)

  - Closed as Archived: 24

[T94436](http://developer.blender.org/T94436),
[T94431](http://developer.blender.org/T94431),
[T94497](http://developer.blender.org/T94497),
[T92260](http://developer.blender.org/T92260),
[T93743](http://developer.blender.org/T93743),
[T93828](http://developer.blender.org/T93828),
[T94508](http://developer.blender.org/T94508),
[T94694](http://developer.blender.org/T94694),
[T94709](http://developer.blender.org/T94709),
[T94655](http://developer.blender.org/T94655),
[T94691](http://developer.blender.org/T94691),
[T94379](http://developer.blender.org/T94379),
[T94389](http://developer.blender.org/T94389),
[T94430](http://developer.blender.org/T94430),
[T94482](http://developer.blender.org/T94482),
[T94518](http://developer.blender.org/T94518),
[T94401](http://developer.blender.org/T94401),
[T93698](http://developer.blender.org/T93698),
[T94353](http://developer.blender.org/T94353),
[T94381](http://developer.blender.org/T94381),
[T94623](http://developer.blender.org/T94623),
[T94398](http://developer.blender.org/T94398),
[T94405](http://developer.blender.org/T94405),
[T94591](http://developer.blender.org/T94591)

  - Closed as Duplicate: 9

[T94742](http://developer.blender.org/T94742),
[T94721](http://developer.blender.org/T94721),
[T94690](http://developer.blender.org/T94690),
[T94678](http://developer.blender.org/T94678),
[T94666](http://developer.blender.org/T94666),
[T94664](http://developer.blender.org/T94664),
[T94606](http://developer.blender.org/T94606),
[T94587](http://developer.blender.org/T94587),
[T94596](http://developer.blender.org/T94596)

  - Closed as Resolved: 0

<!-- end list -->

  - Needs Information from User: 11

[T94747](http://developer.blender.org/T94747),
[T94745](http://developer.blender.org/T94745),
[T94706](http://developer.blender.org/T94706),
[T94684](http://developer.blender.org/T94684),
[T94613](http://developer.blender.org/T94613),
[T94580](http://developer.blender.org/T94580),
[T94568](http://developer.blender.org/T94568),
[T94561](http://developer.blender.org/T94561),
[T94558](http://developer.blender.org/T94558),
[T94290](http://developer.blender.org/T94290),
[T94008](http://developer.blender.org/T94008)

  - Needs Information from Developers: 1

[T94516](http://developer.blender.org/T94516)

  - Diff submitted: 2

\- [D13773](http://developer.blender.org/D13773): Fix T94241: Grayed out
Front-Face Falloff checkbox in popover  
\- [D13780](http://developer.blender.org/D13780): Fix T94085: Crash with
empty stroke list

## Week 33 (Jan 10 - 16)

This week: Triaging  
Next week: Triaging. Submitting patches for claimed reports

**Total actions on tracker: 152**

  - Confirmed: 23

[T93014](http://developer.blender.org/T93014),
[T94888](http://developer.blender.org/T94888),
[T94883](http://developer.blender.org/T94883),
[T94882](http://developer.blender.org/T94882),
[T94878](http://developer.blender.org/T94878),
[T94877](http://developer.blender.org/T94877),
[T94867](http://developer.blender.org/T94867),
[T94852](http://developer.blender.org/T94852),
[T93680](http://developer.blender.org/T93680),
[T93688](http://developer.blender.org/T93688),
[T94824](http://developer.blender.org/T94824),
[T94828](http://developer.blender.org/T94828),
[T94819](http://developer.blender.org/T94819),
[T94799](http://developer.blender.org/T94799),
[T94802](http://developer.blender.org/T94802),
[T94806](http://developer.blender.org/T94806),
[T94800](http://developer.blender.org/T94800),
[T94804](http://developer.blender.org/T94804),
[T94808](http://developer.blender.org/T94808),
[T94763](http://developer.blender.org/T94763),
[T94766](http://developer.blender.org/T94766),
[T94768](http://developer.blender.org/T94768),
[T94779](http://developer.blender.org/T94779)

  - Closed as Archived: 25

[T94558](http://developer.blender.org/T94558),
[T90710](http://developer.blender.org/T90710),
[T92882](http://developer.blender.org/T92882),
[T94477](http://developer.blender.org/T94477),
[T94088](http://developer.blender.org/T94088),
[T94423](http://developer.blender.org/T94423),
[T94452](http://developer.blender.org/T94452),
[T94929](http://developer.blender.org/T94929),
[T94614](http://developer.blender.org/T94614),
[T94684](http://developer.blender.org/T94684),
[T94876](http://developer.blender.org/T94876),
[T94889](http://developer.blender.org/T94889),
[T94892](http://developer.blender.org/T94892),
[T94880](http://developer.blender.org/T94880),
[T94583](http://developer.blender.org/T94583),
[T94628](http://developer.blender.org/T94628),
[T94643](http://developer.blender.org/T94643),
[T93306](http://developer.blender.org/T93306),
[T94668](http://developer.blender.org/T94668),
[T93875](http://developer.blender.org/T93875),
[T94833](http://developer.blender.org/T94833),
[T94840](http://developer.blender.org/T94840),
[T94839](http://developer.blender.org/T94839),
[T92873](http://developer.blender.org/T92873),
[T94369](http://developer.blender.org/T94369)

  - Closed as Duplicate: 7

[T94940](http://developer.blender.org/T94940),
[T94897](http://developer.blender.org/T94897),
[T94855](http://developer.blender.org/T94855),
[T94831](http://developer.blender.org/T94831),
[T94843](http://developer.blender.org/T94843),
[T94841](http://developer.blender.org/T94841),
[T94763](http://developer.blender.org/T94763)

  - Closed as Resolved: 2

[T94625](http://developer.blender.org/T94625),
[T93979](http://developer.blender.org/T93979)

  - Needs Information from User (open): 10

[T94879](http://developer.blender.org/T94879),
[T94829](http://developer.blender.org/T94829),
[T94813](http://developer.blender.org/T94813),
[T94810](http://developer.blender.org/T94810),
[T94801](http://developer.blender.org/T94801),
[T94788](http://developer.blender.org/T94788),
[T94777](http://developer.blender.org/T94777),
[T94776](http://developer.blender.org/T94776),
[T94760](http://developer.blender.org/T94760),
[T93552](http://developer.blender.org/T93552)

  - Needs Information from Developers: 1

[T94814](http://developer.blender.org/T94814)

  - Diff submitted: 1

[D13841](http://developer.blender.org/D13841) - Fix T90845: Add missing
geometry node type

## Week 34 (Jan 17 - 23)

This week: Triaging. Couldn't find enough time this week to work on
pending claimed reports.  
Next week: Triaging.

**Total actions on tracker: 195**

  - Confirmed: 25

[T95139](http://developer.blender.org/T95139),
[T95105](http://developer.blender.org/T95105),
[T93037](http://developer.blender.org/T93037),
[T92886](http://developer.blender.org/T92886),
[T92027](http://developer.blender.org/T92027),
[T83115](http://developer.blender.org/T83115),
[T82645](http://developer.blender.org/T82645),
[T86618](http://developer.blender.org/T86618),
[T94172](http://developer.blender.org/T94172),
[T92696](http://developer.blender.org/T92696),
[T94174](http://developer.blender.org/T94174),
[T94470](http://developer.blender.org/T94470),
[T94476](http://developer.blender.org/T94476),
[T95035](http://developer.blender.org/T95035),
[T94858](http://developer.blender.org/T94858),
[T94187](http://developer.blender.org/T94187),
[T94990](http://developer.blender.org/T94990),
[T94995](http://developer.blender.org/T94995),
[T94977](http://developer.blender.org/T94977),
[T94976](http://developer.blender.org/T94976),
[T94972](http://developer.blender.org/T94972),
[T94936](http://developer.blender.org/T94936),
[T94967](http://developer.blender.org/T94967),
[T94961](http://developer.blender.org/T94961),
[T90480](http://developer.blender.org/T90480)

  - Closed as Archived: 23

[T93789](http://developer.blender.org/T93789),
[T95138](http://developer.blender.org/T95138),
[T95140](http://developer.blender.org/T95140),
[T94561](http://developer.blender.org/T94561),
[T94879](http://developer.blender.org/T94879),
[T93069](http://developer.blender.org/T93069),
[T92084](http://developer.blender.org/T92084),
[T94829](http://developer.blender.org/T94829),
[T95067](http://developer.blender.org/T95067),
[T95073](http://developer.blender.org/T95073),
[T93792](http://developer.blender.org/T93792),
[T88473](http://developer.blender.org/T88473),
[T94602](http://developer.blender.org/T94602),
[T94568](http://developer.blender.org/T94568),
[T94745](http://developer.blender.org/T94745),
[T94788](http://developer.blender.org/T94788),
[T94706](http://developer.blender.org/T94706),
[T94776](http://developer.blender.org/T94776),
[T94777](http://developer.blender.org/T94777),
[T94747](http://developer.blender.org/T94747),
[T94973](http://developer.blender.org/T94973),
[T94959](http://developer.blender.org/T94959),
[T94962](http://developer.blender.org/T94962)

  - Closed as Duplicate: 21

[T86915](http://developer.blender.org/T86915),
[T95141](http://developer.blender.org/T95141),
[T94638](http://developer.blender.org/T94638),
[T95133](http://developer.blender.org/T95133),
[T95119](http://developer.blender.org/T95119),
[T95083](http://developer.blender.org/T95083),
[T95081](http://developer.blender.org/T95081),
[T95077](http://developer.blender.org/T95077),
[T95063](http://developer.blender.org/T95063),
[T92026](http://developer.blender.org/T92026),
[T95068](http://developer.blender.org/T95068),
[T95071](http://developer.blender.org/T95071),
[T95051](http://developer.blender.org/T95051),
[T94462](http://developer.blender.org/T94462),
[T95030](http://developer.blender.org/T95030),
[T95002](http://developer.blender.org/T95002),
[T94073](http://developer.blender.org/T94073),
[T94996](http://developer.blender.org/T94996),
[T94991](http://developer.blender.org/T94991),
[T94999](http://developer.blender.org/T94999),
[T94958](http://developer.blender.org/T94958)

  - Closed as Resolved: 2

[T94976](http://developer.blender.org/T94976),
[T94813](http://developer.blender.org/T94813)

  - Needs Information from User: 20

[T95098](http://developer.blender.org/T95098),
[T95074](http://developer.blender.org/T95074),
[T95040](http://developer.blender.org/T95040),
[T94963](http://developer.blender.org/T94963),
[T94960](http://developer.blender.org/T94960),
[T94946](http://developer.blender.org/T94946),
[T94945](http://developer.blender.org/T94945),
[T94937](http://developer.blender.org/T94937),
[T94907](http://developer.blender.org/T94907),
[T94744](http://developer.blender.org/T94744),
[T92932](http://developer.blender.org/T92932),
[T92870](http://developer.blender.org/T92870),
[T92683](http://developer.blender.org/T92683),
[T92258](http://developer.blender.org/T92258),
[T92093](http://developer.blender.org/T92093),
[T92018](http://developer.blender.org/T92018),
[T91967](http://developer.blender.org/T91967),
[T86991](http://developer.blender.org/T86991),
[T86435](http://developer.blender.org/T86435),
[T86266](http://developer.blender.org/T86266)

  - Needs Information from Developers: 2

[T93113](http://developer.blender.org/T93113),
[T93455](http://developer.blender.org/T93455)

  - Own Reported: 1

[T94967](http://developer.blender.org/T94967)

  - Diff submitted: 0

## Week 35 (Jan 24 - 30)

This week: Triaging. Gone through a few old reports.  
Next week: Triaging.

**Total actions on tracker: 164**

  - Confirmed: 24

[T95299](http://developer.blender.org/T95299),
[T95292](http://developer.blender.org/T95292),
[T95280](http://developer.blender.org/T95280),
[T87647](http://developer.blender.org/T87647),
[T95257](http://developer.blender.org/T95257),
[T95268](http://developer.blender.org/T95268),
[T95242](http://developer.blender.org/T95242),
[T95226](http://developer.blender.org/T95226),
[T95237](http://developer.blender.org/T95237),
[T95212](http://developer.blender.org/T95212),
[T95207](http://developer.blender.org/T95207),
[T95205](http://developer.blender.org/T95205),
[T95202](http://developer.blender.org/T95202),
[T95179](http://developer.blender.org/T95179),
[T95183](http://developer.blender.org/T95183),
[T95176](http://developer.blender.org/T95176),
[T95177](http://developer.blender.org/T95177),
[T95169](http://developer.blender.org/T95169),
[T95154](http://developer.blender.org/T95154),
[T87323](http://developer.blender.org/T87323),
[T95158](http://developer.blender.org/T95158),
[T95164](http://developer.blender.org/T95164),
[T95132](http://developer.blender.org/T95132),
[T90868](http://developer.blender.org/T90868)

  - Closed as Archived: 14

[T95329](http://developer.blender.org/T95329),
[T90929](http://developer.blender.org/T90929),
[T95021](http://developer.blender.org/T95021),
[T95270](http://developer.blender.org/T95270),
[T95272](http://developer.blender.org/T95272),
[T94946](http://developer.blender.org/T94946),
[T95074](http://developer.blender.org/T95074),
[T95184](http://developer.blender.org/T95184),
[T77802](http://developer.blender.org/T77802),
[T93895](http://developer.blender.org/T93895),
[T94937](http://developer.blender.org/T94937),
[T94945](http://developer.blender.org/T94945),
[T94960](http://developer.blender.org/T94960),
[T89420](http://developer.blender.org/T89420)

  - Closed as Duplicate: 12

[T95271](http://developer.blender.org/T95271),
[T83269](http://developer.blender.org/T83269),
[T95224](http://developer.blender.org/T95224),
[T95229](http://developer.blender.org/T95229),
[T95234](http://developer.blender.org/T95234),
[T95225](http://developer.blender.org/T95225),
[T89945](http://developer.blender.org/T89945),
[T89796](http://developer.blender.org/T89796),
[T88871](http://developer.blender.org/T88871),
[T88841](http://developer.blender.org/T88841),
[T95098](http://developer.blender.org/T95098),
[T85628](http://developer.blender.org/T85628)

  - Closed as Resolved: 2

[T85498](http://developer.blender.org/T85498),
[T95183](http://developer.blender.org/T95183)

  - Needs Information from User: 20

[T95303](http://developer.blender.org/T95303),
[T95301](http://developer.blender.org/T95301),
[T95297](http://developer.blender.org/T95297),
[T95263](http://developer.blender.org/T95263),
[T95239](http://developer.blender.org/T95239),
[T95233](http://developer.blender.org/T95233),
[T95200](http://developer.blender.org/T95200),
[T95199](http://developer.blender.org/T95199),
[T95163](http://developer.blender.org/T95163),
[T93148](http://developer.blender.org/T93148),
[T90696](http://developer.blender.org/T90696),
[T90680](http://developer.blender.org/T90680),
[T90559](http://developer.blender.org/T90559),
[T90151](http://developer.blender.org/T90151),
[T90119](http://developer.blender.org/T90119),
[T90111](http://developer.blender.org/T90111),
[T89806](http://developer.blender.org/T89806),
[T88815](http://developer.blender.org/T88815),
[T86188](http://developer.blender.org/T86188),
[T83531](http://developer.blender.org/T83531)

  - Needs Information from Developers: 2

[T95175](http://developer.blender.org/T95175),
[T91469](http://developer.blender.org/T91469)

  - Diff submitted: 0

## Week 36 (Jan 31 - 06)

This week: Triaging.  
Next week: Triaging.

**Total actions on tracker: 178**

  - Confirmed: 15

[T95517](http://developer.blender.org/T95517),
[T95493](http://developer.blender.org/T95493),
[T95479](http://developer.blender.org/T95479),
[T95458](http://developer.blender.org/T95458),
[T95420](http://developer.blender.org/T95420),
[T95419](http://developer.blender.org/T95419),
[T95417](http://developer.blender.org/T95417),
[T95278](http://developer.blender.org/T95278),
[T95298](http://developer.blender.org/T95298),
[T95365](http://developer.blender.org/T95365),
[T95323](http://developer.blender.org/T95323),
[T95326](http://developer.blender.org/T95326),
[T95331](http://developer.blender.org/T95331),
[T95332](http://developer.blender.org/T95332),
[T95335](http://developer.blender.org/T95335)

  - Closed as Archived: 22

[T95491](http://developer.blender.org/T95491),
[T95239](http://developer.blender.org/T95239),
[T95469](http://developer.blender.org/T95469),
[T94198](http://developer.blender.org/T94198),
[T92018](http://developer.blender.org/T92018),
[T86435](http://developer.blender.org/T86435),
[T95199](http://developer.blender.org/T95199),
[T95233](http://developer.blender.org/T95233),
[T95440](http://developer.blender.org/T95440),
[T95429](http://developer.blender.org/T95429),
[T95403](http://developer.blender.org/T95403),
[T86266](http://developer.blender.org/T86266),
[T95220](http://developer.blender.org/T95220),
[T94896](http://developer.blender.org/T94896),
[T94607](http://developer.blender.org/T94607),
[T92683](http://developer.blender.org/T92683),
[T94744](http://developer.blender.org/T94744),
[T94750](http://developer.blender.org/T94750),
[T94801](http://developer.blender.org/T94801),
[T95163](http://developer.blender.org/T95163),
[T95040](http://developer.blender.org/T95040),
[T95330](http://developer.blender.org/T95330)

  - Closed as Duplicate: 12

[T95524](http://developer.blender.org/T95524),
[T95513](http://developer.blender.org/T95513),
[T95494](http://developer.blender.org/T95494),
[T95485](http://developer.blender.org/T95485),
[T95459](http://developer.blender.org/T95459),
[T95464](http://developer.blender.org/T95464),
[T95434](http://developer.blender.org/T95434),
[T95425](http://developer.blender.org/T95425),
[T95380](http://developer.blender.org/T95380),
[T95327](http://developer.blender.org/T95327),
[T95351](http://developer.blender.org/T95351),
[T95348](http://developer.blender.org/T95348)

  - Closed as Resolved: 4

[T95463](http://developer.blender.org/T95463),
[T95412](http://developer.blender.org/T95412),
[T95297](http://developer.blender.org/T95297),
[T95337](http://developer.blender.org/T95337)

  - Needs Information from User (open reports): 8

[T95508](http://developer.blender.org/T95508),
[T95451](http://developer.blender.org/T95451),
[T95441](http://developer.blender.org/T95441),
[T95424](http://developer.blender.org/T95424),
[T95421](http://developer.blender.org/T95421),
[T95409](http://developer.blender.org/T95409),
[T93077](http://developer.blender.org/T93077),
[T91220](http://developer.blender.org/T91220)

  - Needs Information from Developers: 0

<!-- end list -->

  - Diff submitted: 1

[D13980](http://developer.blender.org/D13980) Magic UV: Pass correct
argument for operator identifier

## Week 37 (Feb 07 - 13)

This week: Triaging.  
Next week: Triaging.

**Total actions on tracker: 177**

  - Confirmed: 23

[T91541](http://developer.blender.org/T91541),
[T95723](http://developer.blender.org/T95723),
[T95721](http://developer.blender.org/T95721),
[T95720](http://developer.blender.org/T95720),
[T95716](http://developer.blender.org/T95716),
[T95709](http://developer.blender.org/T95709),
[T95662](http://developer.blender.org/T95662),
[T95654](http://developer.blender.org/T95654),
[T95613](http://developer.blender.org/T95613),
[T95603](http://developer.blender.org/T95603),
[T95620](http://developer.blender.org/T95620),
[T95624](http://developer.blender.org/T95624),
[T95626](http://developer.blender.org/T95626),
[T95604](http://developer.blender.org/T95604),
[T95598](http://developer.blender.org/T95598),
[T95596](http://developer.blender.org/T95596),
[T95589](http://developer.blender.org/T95589),
[T95591](http://developer.blender.org/T95591),
[T95578](http://developer.blender.org/T95578),
[T95518](http://developer.blender.org/T95518),
[T95523](http://developer.blender.org/T95523),
[T95500](http://developer.blender.org/T95500),
[T95444](http://developer.blender.org/T95444)

  - Closed as Archived: 36

[T94901](http://developer.blender.org/T94901),
[T95460](http://developer.blender.org/T95460),
[T95307](http://developer.blender.org/T95307),
[T95319](http://developer.blender.org/T95319),
[T91292](http://developer.blender.org/T91292),
[T95418](http://developer.blender.org/T95418),
[T95409](http://developer.blender.org/T95409),
[T91220](http://developer.blender.org/T91220),
[T95441](http://developer.blender.org/T95441),
[T95451](http://developer.blender.org/T95451),
[T95622](http://developer.blender.org/T95622),
[T89096](http://developer.blender.org/T89096),
[T95421](http://developer.blender.org/T95421),
[T95424](http://developer.blender.org/T95424),
[T95660](http://developer.blender.org/T95660),
[T95546](http://developer.blender.org/T95546),
[T95617](http://developer.blender.org/T95617),
[T95618](http://developer.blender.org/T95618),
[T94345](http://developer.blender.org/T94345),
[T95240](http://developer.blender.org/T95240),
[T95149](http://developer.blender.org/T95149),
[T95156](http://developer.blender.org/T95156),
[T95581](http://developer.blender.org/T95581),
[T90696](http://developer.blender.org/T90696),
[T90151](http://developer.blender.org/T90151),
[T90680](http://developer.blender.org/T90680),
[T93552](http://developer.blender.org/T93552),
[T91438](http://developer.blender.org/T91438),
[T86991](http://developer.blender.org/T86991),
[T86188](http://developer.blender.org/T86188),
[T94008](http://developer.blender.org/T94008),
[T94810](http://developer.blender.org/T94810),
[T94905](http://developer.blender.org/T94905),
[T94928](http://developer.blender.org/T94928),
[T95301](http://developer.blender.org/T95301),
[T95303](http://developer.blender.org/T95303),

  - Closed as Duplicate: 6

[T95726](http://developer.blender.org/T95726),
[T95717](http://developer.blender.org/T95717),
[T95661](http://developer.blender.org/T95661),
[T95659](http://developer.blender.org/T95659),
[T95630](http://developer.blender.org/T95630),
[T95619](http://developer.blender.org/T95619)

  - Closed as Resolved: 0

<!-- end list -->

  - Needs Information from User (open): 8

[T95718](http://developer.blender.org/T95718),
[T95715](http://developer.blender.org/T95715),
[T95658](http://developer.blender.org/T95658),
[T95650](http://developer.blender.org/T95650),
[T95646](http://developer.blender.org/T95646),
[T95632](http://developer.blender.org/T95632),
[T95521](http://developer.blender.org/T95521),
[T95510](http://developer.blender.org/T95510)

  - Needs Information from Developers: 2

[T95621](http://developer.blender.org/T95621),
[T93148](http://developer.blender.org/T93148)

  - Own Reported: 2

[T95663](http://developer.blender.org/T95663),
[T95578](http://developer.blender.org/T95578)

  - Diff submitted (updated): 1

[D13980](http://developer.blender.org/D13980): Magic UV: Pass correct
argument for operator identifier

## Week 38 (Feb 14 - 20)

This week: Triaging. Submitted a couple of patches.  
Next week: Triaging.

**Total actions on tracker: 171**

  - Confirmed: 16

[T95854](http://developer.blender.org/T95854),
[T95849](http://developer.blender.org/T95849),
[T95847](http://developer.blender.org/T95847),
[T95843](http://developer.blender.org/T95843),
[T95827](http://developer.blender.org/T95827),
[T95800](http://developer.blender.org/T95800),
[T95810](http://developer.blender.org/T95810),
[T95809](http://developer.blender.org/T95809),
[T95796](http://developer.blender.org/T95796),
[T95787](http://developer.blender.org/T95787),
[T95791](http://developer.blender.org/T95791),
[T95756](http://developer.blender.org/T95756),
[T95755](http://developer.blender.org/T95755),
[T95749](http://developer.blender.org/T95749),
[T95740](http://developer.blender.org/T95740),
[T95725](http://developer.blender.org/T95725)

  - Closed as Archived: 27

[T89806](http://developer.blender.org/T89806),
[T95402](http://developer.blender.org/T95402),
[T95533](http://developer.blender.org/T95533),
[T95556](http://developer.blender.org/T95556),
[T95850](http://developer.blender.org/T95850),
[T81777](http://developer.blender.org/T81777),
[T95844](http://developer.blender.org/T95844),
[T95346](http://developer.blender.org/T95346),
[T95483](http://developer.blender.org/T95483),
[T95826](http://developer.blender.org/T95826),
[T95808](http://developer.blender.org/T95808),
[T94551](http://developer.blender.org/T94551),
[T94525](http://developer.blender.org/T94525),
[T95100](http://developer.blender.org/T95100),
[T93077](http://developer.blender.org/T93077),
[T95760](http://developer.blender.org/T95760),
[T95748](http://developer.blender.org/T95748),
[T95794](http://developer.blender.org/T95794),
[T95510](http://developer.blender.org/T95510),
[T90119](http://developer.blender.org/T90119),
[T95263](http://developer.blender.org/T95263),
[T95508](http://developer.blender.org/T95508),
[T95793](http://developer.blender.org/T95793),
[T95779](http://developer.blender.org/T95779),
[T95777](http://developer.blender.org/T95777),
[T95746](http://developer.blender.org/T95746),
[T95745](http://developer.blender.org/T95745),

  - Closed as Duplicate: 12

[T95883](http://developer.blender.org/T95883),
[T83700](http://developer.blender.org/T83700),
[T95860](http://developer.blender.org/T95860),
[T95695](http://developer.blender.org/T95695),
[T90111](http://developer.blender.org/T90111),
[T95795](http://developer.blender.org/T95795),
[T95786](http://developer.blender.org/T95786),
[T95766](http://developer.blender.org/T95766),
[T95409](http://developer.blender.org/T95409),
[T95785](http://developer.blender.org/T95785),
[T95782](http://developer.blender.org/T95782),
[T95728](http://developer.blender.org/T95728)

  - Closed as Resolved: 1

[T93526](http://developer.blender.org/T93526)

  - Needs Information from User: 10

[T95846](http://developer.blender.org/T95846),
[T95822](http://developer.blender.org/T95822),
[T95820](http://developer.blender.org/T95820),
[T95792](http://developer.blender.org/T95792),
[T95757](http://developer.blender.org/T95757),
[T95751](http://developer.blender.org/T95751),
[T95750](http://developer.blender.org/T95750),
[T95748](http://developer.blender.org/T95748),
[T87176](http://developer.blender.org/T87176),
[T81603](http://developer.blender.org/T81603)

  - Needs Information from Developers: 4

[T81777](http://developer.blender.org/T81777),
[T95823](http://developer.blender.org/T95823),
[T95740](http://developer.blender.org/T95740),
[T95735](http://developer.blender.org/T95735)

  - Diff submitted: 4

[D14155](http://developer.blender.org/D14155): Fix T93873: Broken UI of
color socket input  
[D14158](http://developer.blender.org/D14158): Fix T93629: Reset to
defaults undoes all steps when applied twice  
[D14156](http://developer.blender.org/D14156): Fix T95444: dxf import
issue with curve object  
[D14142](http://developer.blender.org/D14142): Fix T93526: Missing
tooltip for attribute search button  

## Week 39 (Feb 21 - 27)

This week: Triaging.  
Next week: Triaging.

**Total actions on tracker: 175**

  - Confirmed: 13

[T84081](http://developer.blender.org/T84081),
[T96036](http://developer.blender.org/T96036),
[T95997](http://developer.blender.org/T95997),
[T95963](http://developer.blender.org/T95963),
[T95979](http://developer.blender.org/T95979),
[T95969](http://developer.blender.org/T95969),
[T95964](http://developer.blender.org/T95964),
[T81668](http://developer.blender.org/T81668),
[T95939](http://developer.blender.org/T95939),
[T95901](http://developer.blender.org/T95901),
[T95902](http://developer.blender.org/T95902),
[T95903](http://developer.blender.org/T95903),
[T95919](http://developer.blender.org/T95919)

  - Closed as Archived: 21

[T96040](http://developer.blender.org/T96040),
[T95405](http://developer.blender.org/T95405),
[T96021](http://developer.blender.org/T96021),
[T94907](http://developer.blender.org/T94907),
[T95846](http://developer.blender.org/T95846),
[T95805](http://developer.blender.org/T95805),
[T92093](http://developer.blender.org/T92093),
[T96007](http://developer.blender.org/T96007),
[T96001](http://developer.blender.org/T96001),
[T95968](http://developer.blender.org/T95968),
[T95956](http://developer.blender.org/T95956),
[T95646](http://developer.blender.org/T95646),
[T92698](http://developer.blender.org/T92698),
[T95650](http://developer.blender.org/T95650),
[T95658](http://developer.blender.org/T95658),
[T95718](http://developer.blender.org/T95718),
[T95750](http://developer.blender.org/T95750),
[T95751](http://developer.blender.org/T95751),
[T95757](http://developer.blender.org/T95757),
[T95881](http://developer.blender.org/T95881),
[T95889](http://developer.blender.org/T95889)

  - Closed as Duplicate: 14

[T96039](http://developer.blender.org/T96039),
[T96037](http://developer.blender.org/T96037),
[T96022](http://developer.blender.org/T96022),
[T96003](http://developer.blender.org/T96003),
[T96002](http://developer.blender.org/T96002),
[T96009](http://developer.blender.org/T96009),
[T84247](http://developer.blender.org/T84247),
[T85088](http://developer.blender.org/T85088),
[T95999](http://developer.blender.org/T95999),
[T95974](http://developer.blender.org/T95974),
[T95970](http://developer.blender.org/T95970),
[T95876](http://developer.blender.org/T95876),
[T95900](http://developer.blender.org/T95900),
[T95912](http://developer.blender.org/T95912)

  - Closed as Resolved: 5

[T95606](http://developer.blender.org/T95606),
[T95822](http://developer.blender.org/T95822),
[T93479](http://developer.blender.org/T93479),
[T95899](http://developer.blender.org/T95899),
[T95444](http://developer.blender.org/T95444)

  - Needs Information from User: 11

[T96046](http://developer.blender.org/T96046),
[T96045](http://developer.blender.org/T96045),
[T96042](http://developer.blender.org/T96042),
[T96038](http://developer.blender.org/T96038),
[T96018](http://developer.blender.org/T96018),
[T95980](http://developer.blender.org/T95980),
[T89616](http://developer.blender.org/T89616),
[T88182](http://developer.blender.org/T88182),
[T87176](http://developer.blender.org/T87176),
[T86771](http://developer.blender.org/T86771),
[T83791](http://developer.blender.org/T83791)

  - Needs Information from Developers: 3

[T92958](http://developer.blender.org/T92958),
[T96006](http://developer.blender.org/T96006),
[T90544](http://developer.blender.org/T90544)

  - Unconfirmed: 1

[T92395](http://developer.blender.org/T92395)

  - Diff submitted: 0

## Week 40 (Feb 28 - 06)

This week: Triaging.  
Next week: Triaging.

**Total actions on tracker: 165**

  - Confirmed: 18

[T96180](http://developer.blender.org/T96180),
[T96157](http://developer.blender.org/T96157),
[T96163](http://developer.blender.org/T96163),
[T96160](http://developer.blender.org/T96160),
[T96147](http://developer.blender.org/T96147),
[T96146](http://developer.blender.org/T96146),
[T96145](http://developer.blender.org/T96145),
[T96139](http://developer.blender.org/T96139),
[T96140](http://developer.blender.org/T96140),
[T96123](http://developer.blender.org/T96123),
[T96116](http://developer.blender.org/T96116),
[T96104](http://developer.blender.org/T96104),
[T96097](http://developer.blender.org/T96097),
[T96076](http://developer.blender.org/T96076),
[T96063](http://developer.blender.org/T96063),
[T96057](http://developer.blender.org/T96057),
[T96054](http://developer.blender.org/T96054),
[T96048](http://developer.blender.org/T96048)

  - Closed as Archived: 32

[T96174](http://developer.blender.org/T96174),
[T96051](http://developer.blender.org/T96051),
[T96181](http://developer.blender.org/T96181),
[T90559](http://developer.blender.org/T90559),
[T91604](http://developer.blender.org/T91604),
[T92932](http://developer.blender.org/T92932),
[T95744](http://developer.blender.org/T95744),
[T95824](http://developer.blender.org/T95824),
[T95888](http://developer.blender.org/T95888),
[T95921](http://developer.blender.org/T95921),
[T95926](http://developer.blender.org/T95926),
[T95925](http://developer.blender.org/T95925),
[T95983](http://developer.blender.org/T95983),
[T96141](http://developer.blender.org/T96141),
[T96142](http://developer.blender.org/T96142),
[T96128](http://developer.blender.org/T96128),
[T95790](http://developer.blender.org/T95790),
[T95792](http://developer.blender.org/T95792),
[T89432](http://developer.blender.org/T89432),
[T95875](http://developer.blender.org/T95875),
[T95904](http://developer.blender.org/T95904),
[T95943](http://developer.blender.org/T95943),
[T88970](http://developer.blender.org/T88970),
[T95521](http://developer.blender.org/T95521),
[T95748](http://developer.blender.org/T95748),
[T95851](http://developer.blender.org/T95851),
[T95820](http://developer.blender.org/T95820),
[T96090](http://developer.blender.org/T96090),
[T96084](http://developer.blender.org/T96084),
[T96082](http://developer.blender.org/T96082),
[T96056](http://developer.blender.org/T96056),
[T96067](http://developer.blender.org/T96067)

  - Closed as Duplicate: 11

[T96185](http://developer.blender.org/T96185),
[T96149](http://developer.blender.org/T96149),
[T96183](http://developer.blender.org/T96183),
[T96143](http://developer.blender.org/T96143),
[T96122](http://developer.blender.org/T96122),
[T87176](http://developer.blender.org/T87176),
[T88329](http://developer.blender.org/T88329),
[T96096](http://developer.blender.org/T96096),
[T96083](http://developer.blender.org/T96083),
[T96063](http://developer.blender.org/T96063),
[T96062](http://developer.blender.org/T96062)

  - Closed as Resolved: 2

[T94103](http://developer.blender.org/T94103),
[T86592](http://developer.blender.org/T86592)

  - Needs Information from User (open): 11

[T96172](http://developer.blender.org/T96172),
[T96115](http://developer.blender.org/T96115),
[T96081](http://developer.blender.org/T96081),
[T96055](http://developer.blender.org/T96055),
[T96052](http://developer.blender.org/T96052),
[T87497](http://developer.blender.org/T87497),
[T86631](http://developer.blender.org/T86631),
[T86564](http://developer.blender.org/T86564),
[T86418](http://developer.blender.org/T86418),
[T86102](http://developer.blender.org/T86102),
[T85074](http://developer.blender.org/T85074)

  - Needs Information from Developers: 1

[T96091](http://developer.blender.org/T96091)

  - Own Reported : 2

[T96163](http://developer.blender.org/T96163),
[T96104](http://developer.blender.org/T96104)

  - Diff submitted: 0

## Week 41 (Mar 07 - 13)

This week: Triaging.  
Next week: Triaging.

**Total actions on tracker: 160**

  - Confirmed: 23

[T95298](http://developer.blender.org/T95298),
[T96324](http://developer.blender.org/T96324),
[T96306](http://developer.blender.org/T96306),
[T96316](http://developer.blender.org/T96316),
[T96305](http://developer.blender.org/T96305),
[T96304](http://developer.blender.org/T96304),
[T96294](http://developer.blender.org/T96294),
[T96281](http://developer.blender.org/T96281),
[T96279](http://developer.blender.org/T96279),
[T96278](http://developer.blender.org/T96278),
[T96243](http://developer.blender.org/T96243),
[T96255](http://developer.blender.org/T96255),
[T96229](http://developer.blender.org/T96229),
[T96249](http://developer.blender.org/T96249),
[T96247](http://developer.blender.org/T96247),
[T96233](http://developer.blender.org/T96233),
[T96230](http://developer.blender.org/T96230),
[T96228](http://developer.blender.org/T96228),
[T96213](http://developer.blender.org/T96213),
[T96201](http://developer.blender.org/T96201),
[T96205](http://developer.blender.org/T96205),
[T96194](http://developer.blender.org/T96194),
[T96187](http://developer.blender.org/T96187)

  - Closed as Archived: 12

[T96400](http://developer.blender.org/T96400),
[T96301](http://developer.blender.org/T96301),
[T96313](http://developer.blender.org/T96313),
[T96287](http://developer.blender.org/T96287),
[T96284](http://developer.blender.org/T96284),
[T96266](http://developer.blender.org/T96266),
[T92286](http://developer.blender.org/T92286),
[T94450](http://developer.blender.org/T94450),
[T95555](http://developer.blender.org/T95555),
[T96081](http://developer.blender.org/T96081),
[T86137](http://developer.blender.org/T86137),
[T83331](http://developer.blender.org/T83331)

  - Closed as Duplicate: 12

[T96399](http://developer.blender.org/T96399),
[T96374](http://developer.blender.org/T96374),
[T96358](http://developer.blender.org/T96358),
[T95939](http://developer.blender.org/T95939),
[T96345](http://developer.blender.org/T96345),
[T96270](http://developer.blender.org/T96270),
[T96264](http://developer.blender.org/T96264),
[T96257](http://developer.blender.org/T96257),
[T73226](http://developer.blender.org/T73226),
[T96235](http://developer.blender.org/T96235),
[T96192](http://developer.blender.org/T96192),
[T96188](http://developer.blender.org/T96188)

  - Closed as Resolved: 4

[T96268](http://developer.blender.org/T96268),
[T96242](http://developer.blender.org/T96242),
[T96199](http://developer.blender.org/T96199),
[T96228](http://developer.blender.org/T96228)

  - Needs Information from User (open): 11

[T96390](http://developer.blender.org/T96390),
[T96300](http://developer.blender.org/T96300),
[T96280](http://developer.blender.org/T96280),
[T96277](http://developer.blender.org/T96277),
[T96272](http://developer.blender.org/T96272),
[T96269](http://developer.blender.org/T96269),
[T96246](http://developer.blender.org/T96246),
[T96244](http://developer.blender.org/T96244),
[T96189](http://developer.blender.org/T96189),
[T96175](http://developer.blender.org/T96175),
[T82406](http://developer.blender.org/T82406)

  - Needs Information from Developers: 6

[T96356](http://developer.blender.org/T96356),
[T96308](http://developer.blender.org/T96308),
[T96267](http://developer.blender.org/T96267),
[T96263](http://developer.blender.org/T96263),
[T96186](http://developer.blender.org/T96186),
[T96184](http://developer.blender.org/T96184)

  - Diff submitted: 1

[D14273](http://developer.blender.org/D14273): Fix T96228: TypeError on
use of Copy from Active Track operation

## Week 42 (Mar 14 - 20)

This week: Triaging. Submitted a small fix
[D14329](http://developer.blender.org/D14329).  
Made the list of regression reports submitted after 3.1 (fixing them is
perhaps important for corrective release)
[P2839](http://developer.blender.org/P2839)  
Next week: Triaging.

**Total actions on tracker: 191**

  - Confirmed: 23

[T96589](http://developer.blender.org/T96589),
[T96585](http://developer.blender.org/T96585),
[T96584](http://developer.blender.org/T96584),
[T96582](http://developer.blender.org/T96582),
[T96344](http://developer.blender.org/T96344),
[T96524](http://developer.blender.org/T96524),
[T96521](http://developer.blender.org/T96521),
[T96543](http://developer.blender.org/T96543),
[T96520](http://developer.blender.org/T96520),
[T96512](http://developer.blender.org/T96512),
[T96473](http://developer.blender.org/T96473),
[T96386](http://developer.blender.org/T96386),
[T96491](http://developer.blender.org/T96491),
[T96487](http://developer.blender.org/T96487),
[T96481](http://developer.blender.org/T96481),
[T96432](http://developer.blender.org/T96432),
[T96420](http://developer.blender.org/T96420),
[T96424](http://developer.blender.org/T96424),
[T96415](http://developer.blender.org/T96415),
[T96378](http://developer.blender.org/T96378),
[T96423](http://developer.blender.org/T96423),
[T96396](http://developer.blender.org/T96396),
[T96416](http://developer.blender.org/T96416)

  - Closed as Archived: 31

[T96586](http://developer.blender.org/T96586),
[T96591](http://developer.blender.org/T96591),
[T96581](http://developer.blender.org/T96581),
[T96580](http://developer.blender.org/T96580),
[T96547](http://developer.blender.org/T96547),
[T93459](http://developer.blender.org/T93459),
[T93598](http://developer.blender.org/T93598),
[T90396](http://developer.blender.org/T90396),
[T89616](http://developer.blender.org/T89616),
[T87497](http://developer.blender.org/T87497),
[T86771](http://developer.blender.org/T86771),
[T83791](http://developer.blender.org/T83791),
[T86564](http://developer.blender.org/T86564),
[T96038](http://developer.blender.org/T96038),
[T96042](http://developer.blender.org/T96042),
[T96055](http://developer.blender.org/T96055),
[T96172](http://developer.blender.org/T96172),
[T96189](http://developer.blender.org/T96189),
[T96175](http://developer.blender.org/T96175),
[T96272](http://developer.blender.org/T96272),
[T86418](http://developer.blender.org/T86418),
[T96545](http://developer.blender.org/T96545),
[T96516](http://developer.blender.org/T96516),
[T96410](http://developer.blender.org/T96410),
[T96360](http://developer.blender.org/T96360),
[T96431](http://developer.blender.org/T96431),
[T96480](http://developer.blender.org/T96480),
[T96425](http://developer.blender.org/T96425),
[T96363](http://developer.blender.org/T96363),
[T96422](http://developer.blender.org/T96422),
[T96409](http://developer.blender.org/T96409)

  - Closed as Duplicate: 12

[T96592](http://developer.blender.org/T96592),
[T96560](http://developer.blender.org/T96560),
[T96554](http://developer.blender.org/T96554),
[T96549](http://developer.blender.org/T96549),
[T96522](http://developer.blender.org/T96522),
[T96277](http://developer.blender.org/T96277),
[T96507](http://developer.blender.org/T96507),
[T96509](http://developer.blender.org/T96509),
[T96506](http://developer.blender.org/T96506),
[T96483](http://developer.blender.org/T96483),
[T96426](http://developer.blender.org/T96426),
[T96418](http://developer.blender.org/T96418)

  - Closed as Resolved: 1

[T96280](http://developer.blender.org/T96280)

  - Needs Information from User (open): 10

[T96551](http://developer.blender.org/T96551),
[T96546](http://developer.blender.org/T96546),
[T96508](http://developer.blender.org/T96508),
[T96492](http://developer.blender.org/T96492),
[T96489](http://developer.blender.org/T96489),
[T96474](http://developer.blender.org/T96474),
[T96472](http://developer.blender.org/T96472),
[T96469](http://developer.blender.org/T96469),
[T96428](http://developer.blender.org/T96428),
[T94005](http://developer.blender.org/T94005)

  - Needs Information from Developers: 4

[T94129](http://developer.blender.org/T94129),
[T96499](http://developer.blender.org/T96499),
[T96246](http://developer.blender.org/T96246),
[T96513](http://developer.blender.org/T96513)

  - Unconfirmed: 1

[T94005](http://developer.blender.org/T94005)

  - Own Reported: 1

[T96424](http://developer.blender.org/T96424)

  - Diff submitted: 1

[D14329](http://developer.blender.org/D14329): Fix T96424: Don't
register undo step for empty knife cut

## Week 43 (Mar 21 - 27)

This week: Triaging. Helped reviewing a few patches. Less active on
Thursday  
Next week: Triaging.

**Total actions on tracker: 171**

  - Confirmed: 16

[T96786](http://developer.blender.org/T96786),
[T96787](http://developer.blender.org/T96787),
[T96790](http://developer.blender.org/T96790),
[T96756](http://developer.blender.org/T96756),
[T96732](http://developer.blender.org/T96732),
[T96394](http://developer.blender.org/T96394),
[T96724](http://developer.blender.org/T96724),
[T96721](http://developer.blender.org/T96721),
[T96715](http://developer.blender.org/T96715),
[T96696](http://developer.blender.org/T96696),
[T96683](http://developer.blender.org/T96683),
[T96685](http://developer.blender.org/T96685),
[T96684](http://developer.blender.org/T96684),
[T96401](http://developer.blender.org/T96401),
[T96624](http://developer.blender.org/T96624),
[T96631](http://developer.blender.org/T96631)

  - Closed as Archived: 21

[T96353](http://developer.blender.org/T96353),
[T96350](http://developer.blender.org/T96350),
[T96428](http://developer.blender.org/T96428),
[T96477](http://developer.blender.org/T96477),
[T96496](http://developer.blender.org/T96496),
[T96508](http://developer.blender.org/T96508),
[T96541](http://developer.blender.org/T96541),
[T96546](http://developer.blender.org/T96546),
[T96769](http://developer.blender.org/T96769),
[T96738](http://developer.blender.org/T96738),
[T96462](http://developer.blender.org/T96462),
[T96657](http://developer.blender.org/T96657),
[T88182](http://developer.blender.org/T88182),
[T96489](http://developer.blender.org/T96489),
[T96472](http://developer.blender.org/T96472),
[T96390](http://developer.blender.org/T96390),
[T96244](http://developer.blender.org/T96244),
[T96723](http://developer.blender.org/T96723),
[T96692](http://developer.blender.org/T96692),
[T96697](http://developer.blender.org/T96697),
[T96690](http://developer.blender.org/T96690)

  - Closed as Duplicate: 17

[T96805](http://developer.blender.org/T96805),
[T96793](http://developer.blender.org/T96793),
[T96362](http://developer.blender.org/T96362),
[T96768](http://developer.blender.org/T96768),
[T96766](http://developer.blender.org/T96766),
[T96766](http://developer.blender.org/T96766),
[T96739](http://developer.blender.org/T96739),
[T96734](http://developer.blender.org/T96734),
[T96725](http://developer.blender.org/T96725),
[T96700](http://developer.blender.org/T96700),
[T96695](http://developer.blender.org/T96695),
[T96682](http://developer.blender.org/T96682),
[T96610](http://developer.blender.org/T96610),
[T96621](http://developer.blender.org/T96621),
[T96626](http://developer.blender.org/T96626),
[T96658](http://developer.blender.org/T96658),
[T96665](http://developer.blender.org/T96665)

  - Closed as Resolved: 2

[T93765](http://developer.blender.org/T93765),
[T96624](http://developer.blender.org/T96624)

  - Needs Information from User (open): 9

[T96806](http://developer.blender.org/T96806),
[T96794](http://developer.blender.org/T96794),
[T96791](http://developer.blender.org/T96791),
[T96789](http://developer.blender.org/T96789),
[T96761](http://developer.blender.org/T96761),
[T96714](http://developer.blender.org/T96714),
[T96687](http://developer.blender.org/T96687),
[T96666](http://developer.blender.org/T96666),
[T96625](http://developer.blender.org/T96625)

  - Needs Information from Developers: 2

[T96759](http://developer.blender.org/T96759),
[T96669](http://developer.blender.org/T96669)

  - Code review help: 3

[D13931](http://developer.blender.org/D13931),
[D14352](http://developer.blender.org/D14352),
[D14421](http://developer.blender.org/D14421)

  - Diff submitted: 2

[D14407](http://developer.blender.org/D14407): Fix T96624: NLA crash
when reordering tracks if no object is selected  
[D14329](http://developer.blender.org/D14329) (updated): Fix T96424:
Don't register undo step for empty knife cut

## Week 44 (Mar 28 - 03)

This week: Triaging. Submitted a few patches  
Next week: Triaging.

**Total actions on tracker: 166**

  - Confirmed: 18

[T96964](http://developer.blender.org/T96964),
[T96958](http://developer.blender.org/T96958),
[T96956](http://developer.blender.org/T96956),
[T96931](http://developer.blender.org/T96931),
[T96925](http://developer.blender.org/T96925),
[T96837](http://developer.blender.org/T96837),
[T96837](http://developer.blender.org/T96837),
[T96860](http://developer.blender.org/T96860),
[T96856](http://developer.blender.org/T96856),
[T96845](http://developer.blender.org/T96845),
[T96844](http://developer.blender.org/T96844),
[T96841](http://developer.blender.org/T96841),
[T96836](http://developer.blender.org/T96836),
[T96833](http://developer.blender.org/T96833),
[T96356](http://developer.blender.org/T96356),
[T96799](http://developer.blender.org/T96799),
[T96820](http://developer.blender.org/T96820),
[T96796](http://developer.blender.org/T96796)

  - Closed as Archived: 24

[T96962](http://developer.blender.org/T96962),
[T96953](http://developer.blender.org/T96953),
[T94709](http://developer.blender.org/T94709),
[T96918](http://developer.blender.org/T96918),
[T86631](http://developer.blender.org/T86631),
[T96492](http://developer.blender.org/T96492),
[T96625](http://developer.blender.org/T96625),
[T96666](http://developer.blender.org/T96666),
[T96551](http://developer.blender.org/T96551),
[T95599](http://developer.blender.org/T95599),
[T96687](http://developer.blender.org/T96687),
[T96714](http://developer.blender.org/T96714),
[T96910](http://developer.blender.org/T96910),
[T96882](http://developer.blender.org/T96882),
[T96869](http://developer.blender.org/T96869),
[T96843](http://developer.blender.org/T96843),
[T96478](http://developer.blender.org/T96478),
[T96834](http://developer.blender.org/T96834),
[T96832](http://developer.blender.org/T96832),
[T96569](http://developer.blender.org/T96569),
[T96807](http://developer.blender.org/T96807),
[T96823](http://developer.blender.org/T96823),
[T96798](http://developer.blender.org/T96798),
[T96802](http://developer.blender.org/T96802)

  - Closed as Duplicate: 11

[T96947](http://developer.blender.org/T96947),
[T96914](http://developer.blender.org/T96914),
[T96913](http://developer.blender.org/T96913),
[T96806](http://developer.blender.org/T96806),
[T96857](http://developer.blender.org/T96857),
[T96863](http://developer.blender.org/T96863),
[T96881](http://developer.blender.org/T96881),
[T96883](http://developer.blender.org/T96883),
[T96861](http://developer.blender.org/T96861),
[T96770](http://developer.blender.org/T96770),
[T96871](http://developer.blender.org/T96871)

  - Closed as Resolved: 1

[T96416](http://developer.blender.org/T96416)

  - Needs Information from User: 12

[T96960](http://developer.blender.org/T96960),
[T96934](http://developer.blender.org/T96934),
[T96932](http://developer.blender.org/T96932),
[T96924](http://developer.blender.org/T96924),
[T96912](http://developer.blender.org/T96912),
[T96907](http://developer.blender.org/T96907),
[T96886](http://developer.blender.org/T96886),
[T96880](http://developer.blender.org/T96880),
[T96842](http://developer.blender.org/T96842),
[T96830](http://developer.blender.org/T96830),
[T96551](http://developer.blender.org/T96551),
[T88541](http://developer.blender.org/T88541)

  - Needs Information from Developers: 5

[T96926](http://developer.blender.org/T96926),
[T96837](http://developer.blender.org/T96837),
[T96862](http://developer.blender.org/T96862),
[T96839](http://developer.blender.org/T96839),
[T96783](http://developer.blender.org/T96783)

  - Diff submitted: 3

[D14518](http://developer.blender.org/D14518): Knife tool: Fix UI
alignment in extra settings popover.  
[D14511](http://developer.blender.org/D14511): Avoid unnecessary undo
step creation when executing duplicate operation.  
[D14477](http://developer.blender.org/D14477): Add soft limits for Bevel
segments.  

## Week 45 (Apr 04 - 10)

This week: Triaging.  
Next week: Triaging.

**Total actions on tracker: 166**

  - Confirmed: 14

[T97151](http://developer.blender.org/T97151),
[T97150](http://developer.blender.org/T97150),
[T97132](http://developer.blender.org/T97132),
[T97133](http://developer.blender.org/T97133),
[T97110](http://developer.blender.org/T97110),
[T97053](http://developer.blender.org/T97053),
[T97050](http://developer.blender.org/T97050),
[T97077](http://developer.blender.org/T97077),
[T97090](http://developer.blender.org/T97090),
[T97056](http://developer.blender.org/T97056),
[T97049](http://developer.blender.org/T97049),
[T96980](http://developer.blender.org/T96980),
[T97010](http://developer.blender.org/T97010),
[T96966](http://developer.blender.org/T96966)

  - Closed as Archived: 19

[T97219](http://developer.blender.org/T97219),
[T97217](http://developer.blender.org/T97217),
[T97189](http://developer.blender.org/T97189),
[T97187](http://developer.blender.org/T97187),
[T97126](http://developer.blender.org/T97126),
[T97112](http://developer.blender.org/T97112),
[T97055](http://developer.blender.org/T97055),
[T96641](http://developer.blender.org/T96641),
[T96652](http://developer.blender.org/T96652),
[T96661](http://developer.blender.org/T96661),
[T96679](http://developer.blender.org/T96679),
[T96746](http://developer.blender.org/T96746),
[T96761](http://developer.blender.org/T96761),
[T96773](http://developer.blender.org/T96773),
[T96794](http://developer.blender.org/T96794),
[T96830](http://developer.blender.org/T96830),
[T96971](http://developer.blender.org/T96971),
[T96979](http://developer.blender.org/T96979),
[T96950](http://developer.blender.org/T96950)

  - Closed as Duplicate: 13

[T97192](http://developer.blender.org/T97192),
[T97178](http://developer.blender.org/T97178),
[T97134](http://developer.blender.org/T97134),
[T97141](http://developer.blender.org/T97141),
[T97128](http://developer.blender.org/T97128),
[T97114](http://developer.blender.org/T97114),
[T97130](http://developer.blender.org/T97130),
[T97108](http://developer.blender.org/T97108),
[T97085](http://developer.blender.org/T97085),
[T97091](http://developer.blender.org/T97091),
[T97078](http://developer.blender.org/T97078),
[T97006](http://developer.blender.org/T97006),
[T96043](http://developer.blender.org/T96043)

  - Closed as Resolved: 2

[T96965](http://developer.blender.org/T96965),
[T96424](http://developer.blender.org/T96424)

  - Needs Information from User: 16

[T97155](http://developer.blender.org/T97155),
[T97146](http://developer.blender.org/T97146),
[T97140](http://developer.blender.org/T97140),
[T97131](http://developer.blender.org/T97131),
[T97129](http://developer.blender.org/T97129),
[T97125](http://developer.blender.org/T97125),
[T97066](http://developer.blender.org/T97066),
[T97057](http://developer.blender.org/T97057),
[T97052](http://developer.blender.org/T97052),
[T97050](http://developer.blender.org/T97050),
[T97044](http://developer.blender.org/T97044),
[T97042](http://developer.blender.org/T97042),
[T97020](http://developer.blender.org/T97020),
[T97008](http://developer.blender.org/T97008),
[T96976](http://developer.blender.org/T96976),
[T96972](http://developer.blender.org/T96972)

  - Needs Information from Developers: 3

[T97210](http://developer.blender.org/T97210),
[T97041](http://developer.blender.org/T97041),
[T97010](http://developer.blender.org/T97010)

  - Diff submitted (updated): 1

[D14511](http://developer.blender.org/D14511): WM: avoid unnecessary
undo step creation when duplicating

## Week 46 (Apr 11 - 17)

**Total actions on tracker: 194**

This week: Triaging.  
Next week: Triaging.

  - Confirmed: 18

[T97409](http://developer.blender.org/T97409),
[T97370](http://developer.blender.org/T97370),
[T97356](http://developer.blender.org/T97356),
[T97340](http://developer.blender.org/T97340),
[T97338](http://developer.blender.org/T97338),
[T96894](http://developer.blender.org/T96894),
[T97304](http://developer.blender.org/T97304),
[T97257](http://developer.blender.org/T97257),
[T97289](http://developer.blender.org/T97289),
[T97297](http://developer.blender.org/T97297),
[T97284](http://developer.blender.org/T97284),
[T97278](http://developer.blender.org/T97278),
[T97260](http://developer.blender.org/T97260),
[T97262](http://developer.blender.org/T97262),
[T97183](http://developer.blender.org/T97183),
[T97209](http://developer.blender.org/T97209),
[T97213](http://developer.blender.org/T97213),
[T97214](http://developer.blender.org/T97214)

  - Closed as Archived: 24

[T97052](http://developer.blender.org/T97052),
[T88541](http://developer.blender.org/T88541),
[T96300](http://developer.blender.org/T96300),
[T96960](http://developer.blender.org/T96960),
[T96934](http://developer.blender.org/T96934),
[T96780](http://developer.blender.org/T96780),
[T96924](http://developer.blender.org/T96924),
[T96932](http://developer.blender.org/T96932),
[T96972](http://developer.blender.org/T96972),
[T96976](http://developer.blender.org/T96976),
[T96912](http://developer.blender.org/T96912),
[T97042](http://developer.blender.org/T97042),
[T97020](http://developer.blender.org/T97020),
[T97044](http://developer.blender.org/T97044),
[T97066](http://developer.blender.org/T97066),
[T96474](http://developer.blender.org/T96474),
[T96045](http://developer.blender.org/T96045),
[T97057](http://developer.blender.org/T97057),
[T97008](http://developer.blender.org/T97008),
[T97256](http://developer.blender.org/T97256),
[T97263](http://developer.blender.org/T97263),
[T97212](http://developer.blender.org/T97212),
[T97208](http://developer.blender.org/T97208),
[T97227](http://developer.blender.org/T97227)

  - Closed as Duplicate: 16

[T97376](http://developer.blender.org/T97376),
[T97358](http://developer.blender.org/T97358),
[T97355](http://developer.blender.org/T97355),
[T97291](http://developer.blender.org/T97291),
[T97335](http://developer.blender.org/T97335),
[T97325](http://developer.blender.org/T97325),
[T97170](http://developer.blender.org/T97170),
[T97050](http://developer.blender.org/T97050),
[T97236](http://developer.blender.org/T97236),
[T97258](http://developer.blender.org/T97258),
[T96654](http://developer.blender.org/T96654),
[T97211](http://developer.blender.org/T97211),
[T97221](http://developer.blender.org/T97221),
[T97224](http://developer.blender.org/T97224),
[T97228](http://developer.blender.org/T97228),
[T97226](http://developer.blender.org/T97226)

  - Closed as Resolved: 1

[T97238](http://developer.blender.org/T97238)

  - Needs Information from User: 11

[T97377](http://developer.blender.org/T97377),
[T97339](http://developer.blender.org/T97339),
[T97334](http://developer.blender.org/T97334),
[T97294](http://developer.blender.org/T97294),
[T97229](http://developer.blender.org/T97229),
[T97220](http://developer.blender.org/T97220),
[T97215](http://developer.blender.org/T97215),
[T97205](http://developer.blender.org/T97205),
[T97184](http://developer.blender.org/T97184),
[T97155](http://developer.blender.org/T97155),
[T96926](http://developer.blender.org/T96926)

  - Needs Information from Developers: 4

[T97286](http://developer.blender.org/T97286),
[T97257](http://developer.blender.org/T97257),
[T97170](http://developer.blender.org/T97170),
[T97223](http://developer.blender.org/T97223)

  - Own reported: 1

[T97304](http://developer.blender.org/T97304)

  - Helped in review: 2

[D14641](http://developer.blender.org/D14641),
[D14614](http://developer.blender.org/D14614)

  - Diff submitted: 0

## Week 47 (Apr 18 - 24)

This week: Triaging.  
Next week: Triaging.

**Total actions on tracker: 164**

  - Confirmed: 16

[T97519](http://developer.blender.org/T97519),
[T97518](http://developer.blender.org/T97518),
[T97530](http://developer.blender.org/T97530),
[T97540](http://developer.blender.org/T97540),
[T97507](http://developer.blender.org/T97507),
[T97536](http://developer.blender.org/T97536),
[T97486](http://developer.blender.org/T97486),
[T97490](http://developer.blender.org/T97490),
[T97449](http://developer.blender.org/T97449),
[T97449](http://developer.blender.org/T97449),
[T97378](http://developer.blender.org/T97378),
[T97423](http://developer.blender.org/T97423),
[T97417](http://developer.blender.org/T97417),
[T97394](http://developer.blender.org/T97394),
[T97386](http://developer.blender.org/T97386),
[T97408](http://developer.blender.org/T97408)

  - Closed as Archived: 19

[T97573](http://developer.blender.org/T97573),
[T97538](http://developer.blender.org/T97538),
[T97205](http://developer.blender.org/T97205),
[T97513](http://developer.blender.org/T97513),
[T96025](http://developer.blender.org/T96025),
[T96014](http://developer.blender.org/T96014),
[T94214](http://developer.blender.org/T94214),
[T97492](http://developer.blender.org/T97492),
[T97461](http://developer.blender.org/T97461),
[T97294](http://developer.blender.org/T97294),
[T96886](http://developer.blender.org/T96886),
[T96052](http://developer.blender.org/T96052),
[T96018](http://developer.blender.org/T96018),
[T96900](http://developer.blender.org/T96900),
[T97459](http://developer.blender.org/T97459),
[T97342](http://developer.blender.org/T97342),
[T97392](http://developer.blender.org/T97392),
[T97406](http://developer.blender.org/T97406),
[T97411](http://developer.blender.org/T97411)

  - Closed as Duplicate: 12

[T97534](http://developer.blender.org/T97534),
[T97507](http://developer.blender.org/T97507),
[T97494](http://developer.blender.org/T97494),
[T97493](http://developer.blender.org/T97493),
[T97447](http://developer.blender.org/T97447),
[T97455](http://developer.blender.org/T97455),
[T97457](http://developer.blender.org/T97457),
[T97389](http://developer.blender.org/T97389),
[T97419](http://developer.blender.org/T97419),
[T97381](http://developer.blender.org/T97381),
[T97404](http://developer.blender.org/T97404),
[T97407](http://developer.blender.org/T97407)

  - Closed as Resolved: 2

[T97437](http://developer.blender.org/T97437),
[T97416](http://developer.blender.org/T97416)

  - Needs Information from User (open): 11

[T97520](http://developer.blender.org/T97520),
[T97487](http://developer.blender.org/T97487),
[T97460](http://developer.blender.org/T97460),
[T97414](http://developer.blender.org/T97414),
[T97413](http://developer.blender.org/T97413),
[T97384](http://developer.blender.org/T97384),
[T97334](http://developer.blender.org/T97334),
[T97233](http://developer.blender.org/T97233),
[T97229](http://developer.blender.org/T97229),
[T97115](http://developer.blender.org/T97115),
[T93729](http://developer.blender.org/T93729)

  - Needs Information from Developers: 8

[T97511](http://developer.blender.org/T97511),
[T95609](http://developer.blender.org/T95609),
[T91967](http://developer.blender.org/T91967),
[T97359](http://developer.blender.org/T97359),
[T97431](http://developer.blender.org/T97431),
[T97401](http://developer.blender.org/T97401),
[T97403](http://developer.blender.org/T97403),
[T97412](http://developer.blender.org/T97412)

  - Own Reported: 1

[T97449](http://developer.blender.org/T97449)

  - Diff submitted: 1

[D14729](http://developer.blender.org/D14729): Fix T96925: Fast diagonal
movement in walk navigation

## Week 48 (Apr 25 - 01)

This week: Triaging. Visited all reports of "Needs info from user" tag
which were untouched since
[Feb 2022](https://developer.blender.org/maniphest/project/2/query/oCwnJUydD2gR/#R)  
Next week: Triaging.

**Total actions on tracker: 166**

  - Confirmed: 19

[T97733](http://developer.blender.org/T97733),
[T97710](http://developer.blender.org/T97710),
[T97709](http://developer.blender.org/T97709),
[T97700](http://developer.blender.org/T97700),
[T97698](http://developer.blender.org/T97698),
[T97688](http://developer.blender.org/T97688),
[T97687](http://developer.blender.org/T97687),
[T97677](http://developer.blender.org/T97677),
[T97675](http://developer.blender.org/T97675),
[T97674](http://developer.blender.org/T97674),
[T97520](http://developer.blender.org/T97520),
[T97631](http://developer.blender.org/T97631),
[T97520](http://developer.blender.org/T97520),
[T97588](http://developer.blender.org/T97588),
[T97580](http://developer.blender.org/T97580),
[T97584](http://developer.blender.org/T97584),
[T97553](http://developer.blender.org/T97553),
[T97595](http://developer.blender.org/T97595),
[T97596](http://developer.blender.org/T97596)

  - Closed as Archived: 19

[T97730](http://developer.blender.org/T97730),
[T97184](http://developer.blender.org/T97184),
[T97215](http://developer.blender.org/T97215),
[T97460](http://developer.blender.org/T97460),
[T94005](http://developer.blender.org/T94005),
[T97671](http://developer.blender.org/T97671),
[T95988](http://developer.blender.org/T95988),
[T87611](http://developer.blender.org/T87611),
[T96012](http://developer.blender.org/T96012),
[T94494](http://developer.blender.org/T94494),
[T97087](http://developer.blender.org/T97087),
[T95151](http://developer.blender.org/T95151),
[T97220](http://developer.blender.org/T97220),
[T97334](http://developer.blender.org/T97334),
[T97377](http://developer.blender.org/T97377),
[T97414](http://developer.blender.org/T97414),
[T97583](http://developer.blender.org/T97583),
[T97576](http://developer.blender.org/T97576),
[T97590](http://developer.blender.org/T97590)

  - Closed as Duplicate: 12

[T97725](http://developer.blender.org/T97725),
[T97729](http://developer.blender.org/T97729),
[T97668](http://developer.blender.org/T97668),
[T97661](http://developer.blender.org/T97661),
[T97643](http://developer.blender.org/T97643),
[T94474](http://developer.blender.org/T94474),
[T97641](http://developer.blender.org/T97641),
[T97373](http://developer.blender.org/T97373),
[T97604](http://developer.blender.org/T97604),
[T97598](http://developer.blender.org/T97598),
[T97566](http://developer.blender.org/T97566),
[T97587](http://developer.blender.org/T97587)

  - Closed as Resolved: 3

[T97520](http://developer.blender.org/T97520),
[T97654](http://developer.blender.org/T97654),
[T96925](http://developer.blender.org/T96925)

  - Needs Information from User (open): 5

[T97692](http://developer.blender.org/T97692),
[T97649](http://developer.blender.org/T97649),
[T97594](http://developer.blender.org/T97594),
[T97191](http://developer.blender.org/T97191),
[T97017](http://developer.blender.org/T97017)

  - Needs Information from Developers: 1

[T97682](http://developer.blender.org/T97682)

  - Diff submitted: 0

## Week 49 (May 02 - 08)

This week: Triaging. Less active on Thursday (exam took most of the
time)  
Next week: Triaging. Revisiting all(subscribed) open reports with "Need
info from user" tag

**Total actions on tracker: 147**

  - Confirmed: 15

[T97915](http://developer.blender.org/T97915),
[T97881](http://developer.blender.org/T97881),
[T97894](http://developer.blender.org/T97894),
[T97872](http://developer.blender.org/T97872),
[T97828](http://developer.blender.org/T97828),
[T97827](http://developer.blender.org/T97827),
[T97794](http://developer.blender.org/T97794),
[T97793](http://developer.blender.org/T97793),
[T97743](http://developer.blender.org/T97743),
[T97787](http://developer.blender.org/T97787),
[T97784](http://developer.blender.org/T97784),
[T97774](http://developer.blender.org/T97774),
[T97779](http://developer.blender.org/T97779),
[T97761](http://developer.blender.org/T97761),
[T97737](http://developer.blender.org/T97737)

  - Closed as Archived: 10

[T97925](http://developer.blender.org/T97925),
[T97897](http://developer.blender.org/T97897),
[T97896](http://developer.blender.org/T97896),
[T97885](http://developer.blender.org/T97885),
[T97822](http://developer.blender.org/T97822),
[T97819](http://developer.blender.org/T97819),
[T97786](http://developer.blender.org/T97786),
[T97791](http://developer.blender.org/T97791),
[T97759](http://developer.blender.org/T97759),
[T97115](http://developer.blender.org/T97115)

  - Closed as Duplicate: 17

[T97942](http://developer.blender.org/T97942),
[T97883](http://developer.blender.org/T97883),
[T97891](http://developer.blender.org/T97891),
[T97892](http://developer.blender.org/T97892),
[T97893](http://developer.blender.org/T97893),
[T97873](http://developer.blender.org/T97873),
[T97861](http://developer.blender.org/T97861),
[T97840](http://developer.blender.org/T97840),
[T97837](http://developer.blender.org/T97837),
[T97825](http://developer.blender.org/T97825),
[T97824](http://developer.blender.org/T97824),
[T97821](http://developer.blender.org/T97821),
[T97806](http://developer.blender.org/T97806),
[T97789](http://developer.blender.org/T97789),
[T97781](http://developer.blender.org/T97781),
[T97778](http://developer.blender.org/T97778),
[T97736](http://developer.blender.org/T97736)

  - Closed as Resolved: 2

[T97483](http://developer.blender.org/T97483),
[T97140](http://developer.blender.org/T97140)

  - Needs Information from User: 9

[T95292](http://developer.blender.org/T95292),
[T97923](http://developer.blender.org/T97923),
[T97912](http://developer.blender.org/T97912),
[T97875](http://developer.blender.org/T97875),
[T97814](http://developer.blender.org/T97814),
[T97782](http://developer.blender.org/T97782),
[T97777](http://developer.blender.org/T97777),
[T97765](http://developer.blender.org/T97765),
[T97726](http://developer.blender.org/T97726)

  - Needs Information from Developers: 4

[T97886](http://developer.blender.org/T97886),
[T97839](http://developer.blender.org/T97839),
[T97812](http://developer.blender.org/T97812),
[T97716](http://developer.blender.org/T97716)

  - Diff submitted: 0

## Week 50 (May 09 - 15)

This week: Triaging.  
Next week: Triaging.

**Total actions on tracker: 174**

  - Confirmed: 14

[T98079](http://developer.blender.org/T98079),
[T98063](http://developer.blender.org/T98063),
[T98025](http://developer.blender.org/T98025),
[T98061](http://developer.blender.org/T98061),
[T98056](http://developer.blender.org/T98056),
[T98055](http://developer.blender.org/T98055),
[T98052](http://developer.blender.org/T98052),
[T98035](http://developer.blender.org/T98035),
[T98021](http://developer.blender.org/T98021),
[T97987](http://developer.blender.org/T97987),
[T98003](http://developer.blender.org/T98003),
[T97932](http://developer.blender.org/T97932),
[T97911](http://developer.blender.org/T97911),
[T97921](http://developer.blender.org/T97921)

  - Closed as Archived: 24

[T98107](http://developer.blender.org/T98107),
[T98112](http://developer.blender.org/T98112),
[T95085](http://developer.blender.org/T95085),
[T97726](http://developer.blender.org/T97726),
[T97817](http://developer.blender.org/T97817),
[T98082](http://developer.blender.org/T98082),
[T98053](http://developer.blender.org/T98053),
[T94530](http://developer.blender.org/T94530),
[T96842](http://developer.blender.org/T96842),
[T96551](http://developer.blender.org/T96551),
[T95980](http://developer.blender.org/T95980),
[T95231](http://developer.blender.org/T95231),
[T94677](http://developer.blender.org/T94677),
[T93263](http://developer.blender.org/T93263),
[T98000](http://developer.blender.org/T98000),
[T89445](http://developer.blender.org/T89445),
[T93793](http://developer.blender.org/T93793),
[T97329](http://developer.blender.org/T97329),
[T97339](http://developer.blender.org/T97339),
[T97684](http://developer.blender.org/T97684),
[T97765](http://developer.blender.org/T97765),
[T97953](http://developer.blender.org/T97953),
[T97970](http://developer.blender.org/T97970),
[T97958](http://developer.blender.org/T97958)

  - Closed as Duplicate: 11

[T98105](http://developer.blender.org/T98105),
[T98084](http://developer.blender.org/T98084),
[T98075](http://developer.blender.org/T98075),
[T98083](http://developer.blender.org/T98083),
[T98034](http://developer.blender.org/T98034),
[T98005](http://developer.blender.org/T98005),
[T98004](http://developer.blender.org/T98004),
[T97972](http://developer.blender.org/T97972),
[T97928](http://developer.blender.org/T97928),
[T97951](http://developer.blender.org/T97951),
[T97963](http://developer.blender.org/T97963)

  - Closed as Resolved: 7

[T95292](http://developer.blender.org/T95292),
[T98079](http://developer.blender.org/T98079),
[T98058](http://developer.blender.org/T98058),
[T97993](http://developer.blender.org/T97993),
[T98003](http://developer.blender.org/T98003),
[T97952](http://developer.blender.org/T97952),
[T97931](http://developer.blender.org/T97931)

  - Needs Information from User: 12

[T98059](http://developer.blender.org/T98059),
[T98051](http://developer.blender.org/T98051),
[T98050](http://developer.blender.org/T98050),
[T98046](http://developer.blender.org/T98046),
[T98033](http://developer.blender.org/T98033),
[T98002](http://developer.blender.org/T98002),
[T97998](http://developer.blender.org/T97998),
[T97971](http://developer.blender.org/T97971),
[T97968](http://developer.blender.org/T97968),
[T97964](http://developer.blender.org/T97964),
[T97960](http://developer.blender.org/T97960),
[T97371](http://developer.blender.org/T97371)

  - Needs Information from Developers: 2

[T97925](http://developer.blender.org/T97925),
[T98001](http://developer.blender.org/T98001)

  - Own Reported: 1

[T98003](http://developer.blender.org/T98003)

  - Diff submitted: 0

## Week 51 (May 16 - 22)

This week: Triaging. Less active on exam days(Wednesday, Saturday)  
Next week: Triaging.

**Total actions on tracker: 145**

  - Confirmed: 21

[T98268](http://developer.blender.org/T98268),
[T98266](http://developer.blender.org/T98266),
[T98258](http://developer.blender.org/T98258),
[T98252](http://developer.blender.org/T98252),
[T98230](http://developer.blender.org/T98230),
[T98239](http://developer.blender.org/T98239),
[T98236](http://developer.blender.org/T98236),
[T98237](http://developer.blender.org/T98237),
[T98235](http://developer.blender.org/T98235),
[T98245](http://developer.blender.org/T98245),
[T98231](http://developer.blender.org/T98231),
[T98224](http://developer.blender.org/T98224),
[T98168](http://developer.blender.org/T98168),
[T98192](http://developer.blender.org/T98192),
[T98177](http://developer.blender.org/T98177),
[T98171](http://developer.blender.org/T98171),
[T98178](http://developer.blender.org/T98178),
[T98145](http://developer.blender.org/T98145),
[T98111](http://developer.blender.org/T98111),
[T98093](http://developer.blender.org/T98093),
[T98136](http://developer.blender.org/T98136)

  - Closed as Archived: 6

[T98261](http://developer.blender.org/T98261),
[T98059](http://developer.blender.org/T98059),
[T98285](http://developer.blender.org/T98285),
[T98238](http://developer.blender.org/T98238),
[T98118](http://developer.blender.org/T98118),
[T98170](http://developer.blender.org/T98170)

  - Closed as Duplicate: 13

[T98281](http://developer.blender.org/T98281),
[T98250](http://developer.blender.org/T98250),
[T98277](http://developer.blender.org/T98277),
[T98278](http://developer.blender.org/T98278),
[T98174](http://developer.blender.org/T98174),
[T98199](http://developer.blender.org/T98199),
[T98189](http://developer.blender.org/T98189),
[T98154](http://developer.blender.org/T98154),
[T98186](http://developer.blender.org/T98186),
[T98169](http://developer.blender.org/T98169),
[T98137](http://developer.blender.org/T98137),
[T98130](http://developer.blender.org/T98130),
[T98125](http://developer.blender.org/T98125)

  - Closed as Resolved: 2

[T98256](http://developer.blender.org/T98256),
[T98106](http://developer.blender.org/T98106)

  - Needs Information from User: 10

[T98279](http://developer.blender.org/T98279),
[T98275](http://developer.blender.org/T98275),
[T98273](http://developer.blender.org/T98273),
[T98259](http://developer.blender.org/T98259),
[T98242](http://developer.blender.org/T98242),
[T98233](http://developer.blender.org/T98233),
[T98217](http://developer.blender.org/T98217),
[T98197](http://developer.blender.org/T98197),
[T98184](http://developer.blender.org/T98184),
[T98156](http://developer.blender.org/T98156)

  - Needs Information from Developers: 4

[T98194](http://developer.blender.org/T98194),
[T98271](http://developer.blender.org/T98271),
[T98183](http://developer.blender.org/T98183),
[T98096](http://developer.blender.org/T98096)

  - Own Reported: 1

[T98245](http://developer.blender.org/T98245)

  - help in Review process: 1

[D15007](http://developer.blender.org/D15007)

  - Diff submitted: 0

## Week 52 (May 23 - 29)

This week: Triaging. Less active on Tuesday and Friday (Exams)  
Next week: Triaging.

**Total actions on tracker: 130**

  - Confirmed: 11

[T98461](http://developer.blender.org/T98461),
[T98392](http://developer.blender.org/T98392),
[T98091](http://developer.blender.org/T98091),
[T98384](http://developer.blender.org/T98384),
[T98385](http://developer.blender.org/T98385),
[T98359](http://developer.blender.org/T98359),
[T98259](http://developer.blender.org/T98259),
[T98316](http://developer.blender.org/T98316),
[T98210](http://developer.blender.org/T98210),
[T98240](http://developer.blender.org/T98240),
[T98293](http://developer.blender.org/T98293)

  - Closed as Archived: 20

[T98476](http://developer.blender.org/T98476),
[T98217](http://developer.blender.org/T98217),
[T98233](http://developer.blender.org/T98233),
[T98362](http://developer.blender.org/T98362),
[T95920](http://developer.blender.org/T95920),
[T96469](http://developer.blender.org/T96469),
[T98156](http://developer.blender.org/T98156),
[T97594](http://developer.blender.org/T97594),
[T97649](http://developer.blender.org/T97649),
[T97875](http://developer.blender.org/T97875),
[T97912](http://developer.blender.org/T97912),
[T97923](http://developer.blender.org/T97923),
[T98184](http://developer.blender.org/T98184),
[T98051](http://developer.blender.org/T98051),
[T97371](http://developer.blender.org/T97371),
[T98046](http://developer.blender.org/T98046),
[T98033](http://developer.blender.org/T98033),
[T98002](http://developer.blender.org/T98002),
[T97968](http://developer.blender.org/T97968),
[T98314](http://developer.blender.org/T98314)

  - Closed as Duplicate: 8

[T98466](http://developer.blender.org/T98466),
[T98465](http://developer.blender.org/T98465),
[T98242](http://developer.blender.org/T98242),
[T98387](http://developer.blender.org/T98387),
[T98390](http://developer.blender.org/T98390),
[T98389](http://developer.blender.org/T98389),
[T97487](http://developer.blender.org/T97487),
[T98301](http://developer.blender.org/T98301)

  - Closed as Resolved: 0

<!-- end list -->

  - Needs Information from User (Open): 5

[T98360](http://developer.blender.org/T98360),
[T98318](http://developer.blender.org/T98318),
[T98308](http://developer.blender.org/T98308),
[T98297](http://developer.blender.org/T98297),
[T97039](http://developer.blender.org/T97039)

  - Needs Information from Developers: 3

[T98306](http://developer.blender.org/T98306),
[T98286](http://developer.blender.org/T98286),
[T98327](http://developer.blender.org/T98327)

  - Helped in review process: 3

[D14994](http://developer.blender.org/D14994),
[D15006](http://developer.blender.org/D15006),
[D15008](http://developer.blender.org/D15008)

  - Diff submitted: 1

[D15050](http://developer.blender.org/D15050): Fix T98445: Knife Tool
always cuts through.

## Week 53 (May 30 - 05)

This week: Triaging. Less active on Tuesday (had Exam. Over now).  
Spent free time to understand undo code and checked a few bug reports  
Next week: Triaging.

**Total actions on tracker: 135**

  - Confirmed: 15

[T98565](http://developer.blender.org/T98565),
[T98564](http://developer.blender.org/T98564),
[T98531](http://developer.blender.org/T98531),
[T98546](http://developer.blender.org/T98546),
[T98523](http://developer.blender.org/T98523),
[T98537](http://developer.blender.org/T98537),
[T98530](http://developer.blender.org/T98530),
[T98370](http://developer.blender.org/T98370),
[T98525](http://developer.blender.org/T98525),
[T98526](http://developer.blender.org/T98526),
[T98527](http://developer.blender.org/T98527),
[T98528](http://developer.blender.org/T98528),
[T98491](http://developer.blender.org/T98491),
[T98442](http://developer.blender.org/T98442),
[T98455](http://developer.blender.org/T98455)

  - Closed as Archived: 14

[T97384](http://developer.blender.org/T97384),
[T97692](http://developer.blender.org/T97692),
[T97960](http://developer.blender.org/T97960),
[T98297](http://developer.blender.org/T98297),
[T98360](http://developer.blender.org/T98360),
[T98548](http://developer.blender.org/T98548),
[T98543](http://developer.blender.org/T98543),
[T98531](http://developer.blender.org/T98531),
[T97413](http://developer.blender.org/T97413),
[T98394](http://developer.blender.org/T98394),
[T98308](http://developer.blender.org/T98308),
[T98318](http://developer.blender.org/T98318),
[T98439](http://developer.blender.org/T98439),
[T98452](http://developer.blender.org/T98452)

  - Closed as Duplicate: 2

[T98569](http://developer.blender.org/T98569),
[T98508](http://developer.blender.org/T98508)

  - Closed as Resolved: 1

[T98528](http://developer.blender.org/T98528),
[T98445](http://developer.blender.org/T98445)

  - Needs Information from User: 9

[T98588](http://developer.blender.org/T98588),
[T98561](http://developer.blender.org/T98561),
[T98549](http://developer.blender.org/T98549),
[T98521](http://developer.blender.org/T98521),
[T98516](http://developer.blender.org/T98516),
[T98360](http://developer.blender.org/T98360),
[T98348](http://developer.blender.org/T98348),
[T97369](http://developer.blender.org/T97369),
[T95935](http://developer.blender.org/T95935)

  - Needs Information from Developers: 2

[T98540](http://developer.blender.org/T98540),
[T98538](http://developer.blender.org/T98538),

  - Help in Review process: 1

[D15120](http://developer.blender.org/D15120)

  - Diff submitted: 1

[D15123](http://developer.blender.org/D15123): Fix T93759: Undo doesn't
work with image type empty.

## Week 54 (June 06 - 12)

This week: Triaging. Less activity on Tuesday (So did pending work on
weekend).  
Spent time in understanding automated testing framework  
Next week: Triaging. Will add some operator tests as a initial exercise

**Total actions on tracker: 169**

  - Confirmed: 19

[T98795](http://developer.blender.org/T98795),
[T98773](http://developer.blender.org/T98773),
[T98782](http://developer.blender.org/T98782),
[T98745](http://developer.blender.org/T98745),
[T98753](http://developer.blender.org/T98753),
[T98699](http://developer.blender.org/T98699),
[T98697](http://developer.blender.org/T98697),
[T98700](http://developer.blender.org/T98700),
[T98693](http://developer.blender.org/T98693),
[T98647](http://developer.blender.org/T98647),
[T98620](http://developer.blender.org/T98620),
[T98624](http://developer.blender.org/T98624),
[T98597](http://developer.blender.org/T98597),
[T98603](http://developer.blender.org/T98603),
[T98610](http://developer.blender.org/T98610),
[T98617](http://developer.blender.org/T98617),
[T98618](http://developer.blender.org/T98618),
[T98621](http://developer.blender.org/T98621),
[T98615](http://developer.blender.org/T98615)

  - Closed as Archived: 19

[T98412](http://developer.blender.org/T98412),
[T98749](http://developer.blender.org/T98749),
[T97531](http://developer.blender.org/T97531),
[T97611](http://developer.blender.org/T97611),
[T97749](http://developer.blender.org/T97749),
[T97813](http://developer.blender.org/T97813),
[T97948](http://developer.blender.org/T97948),
[T98151](http://developer.blender.org/T98151),
[T98311](http://developer.blender.org/T98311),
[T98381](http://developer.blender.org/T98381),
[T98659](http://developer.blender.org/T98659),
[T97125](http://developer.blender.org/T97125),
[T97229](http://developer.blender.org/T97229),
[T94042](http://developer.blender.org/T94042),
[T98050](http://developer.blender.org/T98050),
[T98419](http://developer.blender.org/T98419),
[T98537](http://developer.blender.org/T98537),
[T98637](http://developer.blender.org/T98637),
[T98619](http://developer.blender.org/T98619)

  - Closed as Duplicate: 10

[T98790](http://developer.blender.org/T98790),
[T98732](http://developer.blender.org/T98732),
[T98696](http://developer.blender.org/T98696),
[T98706](http://developer.blender.org/T98706),
[T98669](http://developer.blender.org/T98669),
[T98300](http://developer.blender.org/T98300),
[T98639](http://developer.blender.org/T98639),
[T98646](http://developer.blender.org/T98646),
[T98614](http://developer.blender.org/T98614),
[T97633](http://developer.blender.org/T97633)

  - Closed as Resolved: 1

[T98783](http://developer.blender.org/T98783)

  - Needs Information from User(open): 8

[T98792](http://developer.blender.org/T98792),
[T98789](http://developer.blender.org/T98789),
[T98754](http://developer.blender.org/T98754),
[T98748](http://developer.blender.org/T98748),
[T98746](http://developer.blender.org/T98746),
[T98670](http://developer.blender.org/T98670),
[T98658](http://developer.blender.org/T98658),
[T98600](http://developer.blender.org/T98600)

  - Needs Information from Developers: 3

[T98703](http://developer.blender.org/T98703),
[T97323](http://developer.blender.org/T97323),
[T98615](http://developer.blender.org/T98615)

  - Own Reported: 1

[T98624](http://developer.blender.org/T98624)

  - Diff submitted: 0

## Week 55 (June 13 - 19)

This week: Triaging. Submitted a few operator test  
Next week: Triaging. I'll again submit tests and will also explore code
related to undo

**Total actions on tracker: 213**

  - Confirmed: 16

[T98949](http://developer.blender.org/T98949),
[T98932](http://developer.blender.org/T98932),
[T98658](http://developer.blender.org/T98658),
[T98920](http://developer.blender.org/T98920),
[T98919](http://developer.blender.org/T98919),
[T98903](http://developer.blender.org/T98903),
[T98917](http://developer.blender.org/T98917),
[T98904](http://developer.blender.org/T98904),
[T98884](http://developer.blender.org/T98884),
[T98889](http://developer.blender.org/T98889),
[T98726](http://developer.blender.org/T98726),
[T98797](http://developer.blender.org/T98797),
[T98798](http://developer.blender.org/T98798),
[T98870](http://developer.blender.org/T98870),
[T98813](http://developer.blender.org/T98813),
[T98834](http://developer.blender.org/T98834)

  - Closed as Archived: 18

[T98198](http://developer.blender.org/T98198),
[T97129](http://developer.blender.org/T97129),
[T98275](http://developer.blender.org/T98275),
[T98356](http://developer.blender.org/T98356),
[T98360](http://developer.blender.org/T98360),
[T98516](http://developer.blender.org/T98516),
[T98748](http://developer.blender.org/T98748),
[T98521](http://developer.blender.org/T98521),
[T98549](http://developer.blender.org/T98549),
[T98561](http://developer.blender.org/T98561),
[T98588](http://developer.blender.org/T98588),
[T98746](http://developer.blender.org/T98746),
[T98754](http://developer.blender.org/T98754),
[T98828](http://developer.blender.org/T98828),
[T98922](http://developer.blender.org/T98922),
[T98789](http://developer.blender.org/T98789),
[T98824](http://developer.blender.org/T98824),
[T98827](http://developer.blender.org/T98827)

  - Closed as Duplicate: 12

[T98921](http://developer.blender.org/T98921),
[T96685](http://developer.blender.org/T96685),
[T98934](http://developer.blender.org/T98934),
[T98791](http://developer.blender.org/T98791),
[T98876](http://developer.blender.org/T98876),
[T98625](http://developer.blender.org/T98625),
[T98900](http://developer.blender.org/T98900),
[T98872](http://developer.blender.org/T98872),
[T98791](http://developer.blender.org/T98791),
[T98804](http://developer.blender.org/T98804),
[T98821](http://developer.blender.org/T98821),
[T98822](http://developer.blender.org/T98822)

  - Closed as Resolved: 4

[T95935](http://developer.blender.org/T95935),
[T94209](http://developer.blender.org/T94209),
[T98917](http://developer.blender.org/T98917),
[T96647](http://developer.blender.org/T96647)

  - Needs Information from User: 10

[T98948](http://developer.blender.org/T98948),
[T98946](http://developer.blender.org/T98946),
[T98935](http://developer.blender.org/T98935),
[T98883](http://developer.blender.org/T98883),
[T98846](http://developer.blender.org/T98846),
[T98835](http://developer.blender.org/T98835),
[T98830](http://developer.blender.org/T98830),
[T98814](http://developer.blender.org/T98814),
[T98806](http://developer.blender.org/T98806),
[T98752](http://developer.blender.org/T98752)

  - Needs Information from Developers: 2

[T98937](http://developer.blender.org/T98937),
[T98836](http://developer.blender.org/T98836)

  - Own Submitted: 1

[T98923](http://developer.blender.org/T98923)

  - Diff submitted: 3

\- [D15187](http://developer.blender.org/D15187): Automated testing: Add
operators tests  
\- [D15224](http://developer.blender.org/D15224): Fix T98658: Bsurfaces
error when cyclic cross and follow is checked in redo panel  
\- [D15225](http://developer.blender.org/D15225): Fix T98902: Btracer
Particle Trace is broken

## Week 56 (June 20 - 26)

This week: Triaging. Submitted two small revisions  
Next week: Triaging. Understanding undo system

**Total actions on tracker: 205**

  - Confirmed: 17

[T99182](http://developer.blender.org/T99182),
[T99004](http://developer.blender.org/T99004),
[T99104](http://developer.blender.org/T99104),
[T99100](http://developer.blender.org/T99100),
[T99051](http://developer.blender.org/T99051),
[T99070](http://developer.blender.org/T99070),
[T99072](http://developer.blender.org/T99072),
[T99037](http://developer.blender.org/T99037),
[T99027](http://developer.blender.org/T99027),
[T99028](http://developer.blender.org/T99028),
[T99033](http://developer.blender.org/T99033),
[T99036](http://developer.blender.org/T99036),
[T98968](http://developer.blender.org/T98968),
[T98972](http://developer.blender.org/T98972),
[T98992](http://developer.blender.org/T98992),
[T99003](http://developer.blender.org/T99003),
[T99004](http://developer.blender.org/T99004)

  - Closed as Archived: 17

[T93004](http://developer.blender.org/T93004),
[T98883](http://developer.blender.org/T98883),
[T98846](http://developer.blender.org/T98846),
[T98835](http://developer.blender.org/T98835),
[T98916](http://developer.blender.org/T98916),
[T98830](http://developer.blender.org/T98830),
[T98814](http://developer.blender.org/T98814),
[T98806](http://developer.blender.org/T98806),
[T98792](http://developer.blender.org/T98792),
[T98666](http://developer.blender.org/T98666),
[T98504](http://developer.blender.org/T98504),
[T98284](http://developer.blender.org/T98284),
[T97369](http://developer.blender.org/T97369),
[T97265](http://developer.blender.org/T97265),
[T98743](http://developer.blender.org/T98743),
[T98950](http://developer.blender.org/T98950),
[T99010](http://developer.blender.org/T99010)

  - Closed as Duplicate: 19

[T99177](http://developer.blender.org/T99177),
[T99119](http://developer.blender.org/T99119),
[T99122](http://developer.blender.org/T99122),
[T99098](http://developer.blender.org/T99098),
[T99090](http://developer.blender.org/T99090),
[T99048](http://developer.blender.org/T99048),
[T99064](http://developer.blender.org/T99064),
[T99049](http://developer.blender.org/T99049),
[T98816](http://developer.blender.org/T98816),
[T99050](http://developer.blender.org/T99050),
[T99056](http://developer.blender.org/T99056),
[T99042](http://developer.blender.org/T99042),
[T88610](http://developer.blender.org/T88610),
[T99031](http://developer.blender.org/T99031),
[T99012](http://developer.blender.org/T99012),
[T98999](http://developer.blender.org/T98999),
[T98983](http://developer.blender.org/T98983),
[T98994](http://developer.blender.org/T98994),
[T98990](http://developer.blender.org/T98990)

  - Closed as Resolved: 2

[T97162](http://developer.blender.org/T97162),
[T98993](http://developer.blender.org/T98993)

  - Needs Information from User (open): 6

[T99069](http://developer.blender.org/T99069),
[T99035](http://developer.blender.org/T99035),
[T99002](http://developer.blender.org/T99002),
[T98989](http://developer.blender.org/T98989),
[T98967](http://developer.blender.org/T98967),
[T96053](http://developer.blender.org/T96053)

  - Needs Information from Developers: 6

[T99117](http://developer.blender.org/T99117),
[T85267](http://developer.blender.org/T85267),
[T98705](http://developer.blender.org/T98705),
[T99071](http://developer.blender.org/T99071),
[T99032](http://developer.blender.org/T99032),
[T98865](http://developer.blender.org/T98865)

  - Own Reported: 1

[T99182](http://developer.blender.org/T99182)

  - Diff submitted: 2

[D15256](http://developer.blender.org/D15256): Fix T60609: Avoid undo
push when no changes have been made in color panel  
[D15270](http://developer.blender.org/D15270): Fix T99070: Apply
transform fails to clear delta transform values

## Week 57 (June 27 - 03)

This week: Triaging. Submitted two small revisions  
Next week: Triaging.

**Total actions on tracker: 203**

  - Confirmed: 27

[T99335](http://developer.blender.org/T99335),
[T99323](http://developer.blender.org/T99323),
[T99296](http://developer.blender.org/T99296),
[T99305](http://developer.blender.org/T99305),
[T99308](http://developer.blender.org/T99308),
[T99310](http://developer.blender.org/T99310),
[T99270](http://developer.blender.org/T99270),
[T99267](http://developer.blender.org/T99267),
[T99282](http://developer.blender.org/T99282),
[T99287](http://developer.blender.org/T99287),
[T99179](http://developer.blender.org/T99179),
[T99248](http://developer.blender.org/T99248),
[T99256](http://developer.blender.org/T99256),
[T99204](http://developer.blender.org/T99204),
[T99218](http://developer.blender.org/T99218),
[T99172](http://developer.blender.org/T99172),
[T99161](http://developer.blender.org/T99161),
[T99157](http://developer.blender.org/T99157),
[T99151](http://developer.blender.org/T99151),
[T99174](http://developer.blender.org/T99174),
[T99200](http://developer.blender.org/T99200),
[T99196](http://developer.blender.org/T99196),
[T99191](http://developer.blender.org/T99191),
[T99192](http://developer.blender.org/T99192),
[T99176](http://developer.blender.org/T99176),
[T99178](http://developer.blender.org/T99178),
[T99165](http://developer.blender.org/T99165)

  - Closed as Archived: 11

[T99345](http://developer.blender.org/T99345),
[T99302](http://developer.blender.org/T99302),
[T99311](http://developer.blender.org/T99311),
[T99312](http://developer.blender.org/T99312),
[T98967](http://developer.blender.org/T98967),
[T99035](http://developer.blender.org/T99035),
[T99069](http://developer.blender.org/T99069),
[T99274](http://developer.blender.org/T99274),
[T99277](http://developer.blender.org/T99277),
[T99289](http://developer.blender.org/T99289),
[T99252](http://developer.blender.org/T99252)

  - Closed as Duplicate: 11

[T99354](http://developer.blender.org/T99354),
[T99351](http://developer.blender.org/T99351),
[T99339](http://developer.blender.org/T99339),
[T99307](http://developer.blender.org/T99307),
[T99298](http://developer.blender.org/T99298),
[T99243](http://developer.blender.org/T99243),
[T99236](http://developer.blender.org/T99236),
[T99167](http://developer.blender.org/T99167),
[T99163](http://developer.blender.org/T99163),
[T99199](http://developer.blender.org/T99199),
[T99193](http://developer.blender.org/T99193)

  - Closed as Resolved: 3

[T99200](http://developer.blender.org/T99200),
[T98842](http://developer.blender.org/T98842),
[T98836](http://developer.blender.org/T98836)

  - Needs Information from User: 13

[T99295](http://developer.blender.org/T99295),
[T99286](http://developer.blender.org/T99286),
[T99279](http://developer.blender.org/T99279),
[T99264](http://developer.blender.org/T99264),
[T99263](http://developer.blender.org/T99263),
[T99260](http://developer.blender.org/T99260),
[T99258](http://developer.blender.org/T99258),
[T99234](http://developer.blender.org/T99234),
[T99219](http://developer.blender.org/T99219),
[T99214](http://developer.blender.org/T99214),
[T99170](http://developer.blender.org/T99170),
[T99162](http://developer.blender.org/T99162),
[T99143](http://developer.blender.org/T99143)

  - Needs Information from Developers: 2

[T99187](http://developer.blender.org/T99187),
[T99208](http://developer.blender.org/T99208)

  - Own Reported: 1

[T99288](http://developer.blender.org/T99288)

  - Diff submitted: 2

[D15345](http://developer.blender.org/D15345): Fix T92099: No undo when
moving viewport with camera locked to view  
[D15313](http://developer.blender.org/D15313): Fix T99161: Set reroute
node active when adding through lasso gesture

## Week 58 (July 04 - 10)

This week: Triaging. General code contributions (patches mentioned
below)  
Next week: Triaging. College reopening so won't be active at the usual
morning time

**Total actions on tracker: 182**

  - Confirmed: 21

[T99536](http://developer.blender.org/T99536),
[T99533](http://developer.blender.org/T99533),
[T99501](http://developer.blender.org/T99501),
[T99491](http://developer.blender.org/T99491),
[T99503](http://developer.blender.org/T99503),
[T99505](http://developer.blender.org/T99505),
[T99005](http://developer.blender.org/T99005),
[T99279](http://developer.blender.org/T99279),
[T99462](http://developer.blender.org/T99462),
[T99478](http://developer.blender.org/T99478),
[T99473](http://developer.blender.org/T99473),
[T99373](http://developer.blender.org/T99373),
[T99443](http://developer.blender.org/T99443),
[T99445](http://developer.blender.org/T99445),
[T99453](http://developer.blender.org/T99453),
[T99449](http://developer.blender.org/T99449),
[T99288](http://developer.blender.org/T99288),
[T99360](http://developer.blender.org/T99360),
[T99362](http://developer.blender.org/T99362),
[T99364](http://developer.blender.org/T99364),
[T99368](http://developer.blender.org/T99368)

  - Closed as Archived: 18

[T99575](http://developer.blender.org/T99575),
[T99556](http://developer.blender.org/T99556),
[T99204](http://developer.blender.org/T99204),
[T99115](http://developer.blender.org/T99115),
[T99143](http://developer.blender.org/T99143),
[T99162](http://developer.blender.org/T99162),
[T99234](http://developer.blender.org/T99234),
[T99258](http://developer.blender.org/T99258),
[T99260](http://developer.blender.org/T99260),
[T99286](http://developer.blender.org/T99286),
[T99367](http://developer.blender.org/T99367),
[T99463](http://developer.blender.org/T99463),
[T99470](http://developer.blender.org/T99470),
[T98948](http://developer.blender.org/T98948),
[T99446](http://developer.blender.org/T99446),
[T99377](http://developer.blender.org/T99377),
[T99375](http://developer.blender.org/T99375),
[T99374](http://developer.blender.org/T99374)

  - Closed as Duplicate: 11

[T99579](http://developer.blender.org/T99579),
[T99530](http://developer.blender.org/T99530),
[T99537](http://developer.blender.org/T99537),
[T99538](http://developer.blender.org/T99538),
[T99509](http://developer.blender.org/T99509),
[T99510](http://developer.blender.org/T99510),
[T99481](http://developer.blender.org/T99481),
[T99452](http://developer.blender.org/T99452),
[T99378](http://developer.blender.org/T99378),
[T99379](http://developer.blender.org/T99379),
[T99376](http://developer.blender.org/T99376)

  - Closed as Resolved: 5

[T99478](http://developer.blender.org/T99478),
[T99467](http://developer.blender.org/T99467),
[T98902](http://developer.blender.org/T98902),
[T98658](http://developer.blender.org/T98658),
[T99453](http://developer.blender.org/T99453)

  - Needs Information from User (open): 17

[T99580](http://developer.blender.org/T99580),
[T99567](http://developer.blender.org/T99567),
[T99561](http://developer.blender.org/T99561),
[T99555](http://developer.blender.org/T99555),
[T99541](http://developer.blender.org/T99541),
[T99538](http://developer.blender.org/T99538),
[T99535](http://developer.blender.org/T99535),
[T99525](http://developer.blender.org/T99525),
[T99508](http://developer.blender.org/T99508),
[T99500](http://developer.blender.org/T99500),
[T99495](http://developer.blender.org/T99495),
[T99480](http://developer.blender.org/T99480),
[T99479](http://developer.blender.org/T99479),
[T99471](http://developer.blender.org/T99471),
[T99451](http://developer.blender.org/T99451),
[T99357](http://developer.blender.org/T99357),
[T99320](http://developer.blender.org/T99320)

  - Needs Information from Developers: 1

[T99361](http://developer.blender.org/T99361)

  - own Reported: 2

[T99486](http://developer.blender.org/T99486),
[T99453](http://developer.blender.org/T99453)

  - Diff submitted: 4

[D15370](http://developer.blender.org/D15370): Fix T99453: Regression:
Crash on calling menu search  
[D15371](http://developer.blender.org/D15371): Fix T99383: Regression:
Crash in sculpting with color filter  
[D15256](http://developer.blender.org/D15256): Fix T60609: Avoid undo
push when no changes have been made in color panel  
[D15345](http://developer.blender.org/D15345): Fix T92099: No undo when
moving viewport with camera locked to view  

  - doc contribution: 1

[rBM9395](http://developer.blender.org/rBM9395): Fix T99478: Typo in
Weight Paint Mode

## Week 59 (July 11 - 17)

This week: Triaging. General code contributions (patches mentioned
below)  
Next week: Triaging.

**Total actions on tracker: 212**

  - Confirmed: 26

[T99471](http://developer.blender.org/T99471),
[T99704](http://developer.blender.org/T99704),
[T99706](http://developer.blender.org/T99706),
[T99715](http://developer.blender.org/T99715),
[T99633](http://developer.blender.org/T99633),
[T99677](http://developer.blender.org/T99677),
[T99678](http://developer.blender.org/T99678),
[T99641](http://developer.blender.org/T99641),
[T99672](http://developer.blender.org/T99672),
[T99629](http://developer.blender.org/T99629),
[T99648](http://developer.blender.org/T99648),
[T99654](http://developer.blender.org/T99654),
[T99656](http://developer.blender.org/T99656),
[T99660](http://developer.blender.org/T99660),
[T99661](http://developer.blender.org/T99661),
[T99646](http://developer.blender.org/T99646),
[T99583](http://developer.blender.org/T99583),
[T99598](http://developer.blender.org/T99598),
[T99617](http://developer.blender.org/T99617),
[T99626](http://developer.blender.org/T99626),
[T99623](http://developer.blender.org/T99623),
[T99624](http://developer.blender.org/T99624),
[T99584](http://developer.blender.org/T99584),
[T99586](http://developer.blender.org/T99586),
[T99597](http://developer.blender.org/T99597),
[T99606](http://developer.blender.org/T99606)

  - Closed as Archived: 20

[T99756](http://developer.blender.org/T99756),
[T99465](http://developer.blender.org/T99465),
[T97121](http://developer.blender.org/T97121),
[T96053](http://developer.blender.org/T96053),
[T99264](http://developer.blender.org/T99264),
[T99451](http://developer.blender.org/T99451),
[T99479](http://developer.blender.org/T99479),
[T99480](http://developer.blender.org/T99480),
[T99495](http://developer.blender.org/T99495),
[T99500](http://developer.blender.org/T99500),
[T99508](http://developer.blender.org/T99508),
[T99525](http://developer.blender.org/T99525),
[T99541](http://developer.blender.org/T99541),
[T99719](http://developer.blender.org/T99719),
[T99690](http://developer.blender.org/T99690),
[T99691](http://developer.blender.org/T99691),
[T99679](http://developer.blender.org/T99679),
[T99649](http://developer.blender.org/T99649),
[T99650](http://developer.blender.org/T99650),
[T99604](http://developer.blender.org/T99604)

  - Closed as Duplicate: 10

[T99782](http://developer.blender.org/T99782),
[T99699](http://developer.blender.org/T99699),
[T99708](http://developer.blender.org/T99708),
[T99716](http://developer.blender.org/T99716),
[T99685](http://developer.blender.org/T99685),
[T99683](http://developer.blender.org/T99683),
[T99645](http://developer.blender.org/T99645),
[T99637](http://developer.blender.org/T99637),
[T99632](http://developer.blender.org/T99632),
[T99611](http://developer.blender.org/T99611)

  - Closed as Resolved: 2

[T99646](http://developer.blender.org/T99646),
[T99655](http://developer.blender.org/T99655)

  - Needs Information from User: 20

[T99783](http://developer.blender.org/T99783),
[T99720](http://developer.blender.org/T99720),
[T99714](http://developer.blender.org/T99714),
[T99686](http://developer.blender.org/T99686),
[T99682](http://developer.blender.org/T99682),
[T99675](http://developer.blender.org/T99675),
[T99671](http://developer.blender.org/T99671),
[T99670](http://developer.blender.org/T99670),
[T99636](http://developer.blender.org/T99636),
[T99631](http://developer.blender.org/T99631),
[T99627](http://developer.blender.org/T99627),
[T99625](http://developer.blender.org/T99625),
[T99619](http://developer.blender.org/T99619),
[T99610](http://developer.blender.org/T99610),
[T99601](http://developer.blender.org/T99601),
[T99594](http://developer.blender.org/T99594),
[T99589](http://developer.blender.org/T99589),
[T99585](http://developer.blender.org/T99585),
[T99578](http://developer.blender.org/T99578),
[T99529](http://developer.blender.org/T99529)

  - Needs Information from Developers: 6

[T99357](http://developer.blender.org/T99357),
[T99590](http://developer.blender.org/T99590),
[T99635](http://developer.blender.org/T99635),
[T99577](http://developer.blender.org/T99577),
[T99592](http://developer.blender.org/T99592),
[T99603](http://developer.blender.org/T99603)

  - Own reported: 1

[T99641](http://developer.blender.org/T99641)

  - Code Contribution: 5

[D15453](http://developer.blender.org/D15453): Fix memfile undo push
when in texture paint mode  
[D15345](http://developer.blender.org/D15345): Fix T92099: No undo when
moving viewport with camera locked to view  
[D15432](http://developer.blender.org/D15432): Fix T93422: ANT Landscape
Tools - Landscape Eroder not working  
[D15431](http://developer.blender.org/D15431): Fix T99583: Missing
update for interpolation option in particle edit mode.  
[D15123](http://developer.blender.org/D15123): Fix T93759: Undo doesn't
work with image type empty  

  - Manual Contribution: 1

[rBM9419](http://developer.blender.org/rBM9419): Fix typo in
library\_overrides

## Week 60 (July 18 - 24)

This week: Triaging. Updated one pending patch. Revisited some old
inactive reports  
Next week: Triaging.

**Total actions on tracker: 198**

  - Confirmed: 16

[T99936](http://developer.blender.org/T99936),
[T99928](http://developer.blender.org/T99928),
[T99926](http://developer.blender.org/T99926),
[T99710](http://developer.blender.org/T99710),
[T99902](http://developer.blender.org/T99902),
[T76083](http://developer.blender.org/T76083),
[T99884](http://developer.blender.org/T99884),
[T99219](http://developer.blender.org/T99219),
[T99874](http://developer.blender.org/T99874),
[T99854](http://developer.blender.org/T99854),
[T99851](http://developer.blender.org/T99851),
[T99850](http://developer.blender.org/T99850),
[T99824](http://developer.blender.org/T99824),
[T99805](http://developer.blender.org/T99805),
[T99791](http://developer.blender.org/T99791),
[T99787](http://developer.blender.org/T99787)

  - Closed as Archived: 31

[T99939](http://developer.blender.org/T99939),
[T96702](http://developer.blender.org/T96702),
[T98805](http://developer.blender.org/T98805),
[T99636](http://developer.blender.org/T99636),
[T99535](http://developer.blender.org/T99535),
[T99454](http://developer.blender.org/T99454),
[T99566](http://developer.blender.org/T99566),
[T99295](http://developer.blender.org/T99295),
[T99561](http://developer.blender.org/T99561),
[T99580](http://developer.blender.org/T99580),
[T99589](http://developer.blender.org/T99589),
[T99600](http://developer.blender.org/T99600),
[T99601](http://developer.blender.org/T99601),
[T99610](http://developer.blender.org/T99610),
[T99625](http://developer.blender.org/T99625),
[T99627](http://developer.blender.org/T99627),
[T99631](http://developer.blender.org/T99631),
[T99671](http://developer.blender.org/T99671),
[T99882](http://developer.blender.org/T99882),
[T99846](http://developer.blender.org/T99846),
[T99852](http://developer.blender.org/T99852),
[T99148](http://developer.blender.org/T99148),
[T99247](http://developer.blender.org/T99247),
[T98867](http://developer.blender.org/T98867),
[T99534](http://developer.blender.org/T99534),
[T99675](http://developer.blender.org/T99675),
[T99818](http://developer.blender.org/T99818),
[T99826](http://developer.blender.org/T99826),
[T99529](http://developer.blender.org/T99529),
[T99736](http://developer.blender.org/T99736),
[T99801](http://developer.blender.org/T99801)

  - Closed as Duplicate: 10

[T99938](http://developer.blender.org/T99938),
[T99922](http://developer.blender.org/T99922),
[T99901](http://developer.blender.org/T99901),
[T99861](http://developer.blender.org/T99861),
[T99843](http://developer.blender.org/T99843),
[T99813](http://developer.blender.org/T99813),
[T99832](http://developer.blender.org/T99832),
[T99594](http://developer.blender.org/T99594),
[T99780](http://developer.blender.org/T99780),
[T99802](http://developer.blender.org/T99802)

  - Closed as Resolved: 6

[T99937](http://developer.blender.org/T99937),
[T99921](http://developer.blender.org/T99921),
[T99710](http://developer.blender.org/T99710),
[T99874](http://developer.blender.org/T99874),
[T99853](http://developer.blender.org/T99853),
[T99583](http://developer.blender.org/T99583)

  - Needs Information from User: 12

[T99925](http://developer.blender.org/T99925),
[T99896](http://developer.blender.org/T99896),
[T99883](http://developer.blender.org/T99883),
[T99860](http://developer.blender.org/T99860),
[T99833](http://developer.blender.org/T99833),
[T99828](http://developer.blender.org/T99828),
[T99821](http://developer.blender.org/T99821),
[T99806](http://developer.blender.org/T99806),
[T99804](http://developer.blender.org/T99804),
[T99803](http://developer.blender.org/T99803),
[T99225](http://developer.blender.org/T99225),
[T95873](http://developer.blender.org/T95873)

  - Needs Information from Developers: 1

[T99736](http://developer.blender.org/T99736)

  - Own Reported: 0

<!-- end list -->

  - Diff submitted (updated): 1

[D15313](http://developer.blender.org/D15313) Fix T99161: Set reroute
node active when adding through lasso gesture

  - Manual Contribution: 2

[rBM9422](http://developer.blender.org/rBM9422): Fix T99710: Remove
repeated option description in IK contraint  
[rBM9420](http://developer.blender.org/rBM9420): Video Editing: Hotkeys
for Lock/Unlock are incorrect

## Week 61 (July 25 - 31)

This week: Triaging. General code contribution (Patches listed below)  
Next week: Triaging.

**Total actions on tracker: 175**

  - Confirmed: 15

[T100048](http://developer.blender.org/T100048),
[T100053](http://developer.blender.org/T100053),
[T100041](http://developer.blender.org/T100041),
[T100040](http://developer.blender.org/T100040),
[T100016](http://developer.blender.org/T100016),
[T100012](http://developer.blender.org/T100012),
[T100002](http://developer.blender.org/T100002),
[T99994](http://developer.blender.org/T99994),
[T99979](http://developer.blender.org/T99979),
[T99947](http://developer.blender.org/T99947),
[T99984](http://developer.blender.org/T99984),
[T99963](http://developer.blender.org/T99963),
[T99933](http://developer.blender.org/T99933),
[T99932](http://developer.blender.org/T99932),
[T99949](http://developer.blender.org/T99949)

  - Closed as Archived: 21

[T100077](http://developer.blender.org/T100077),
[T98863](http://developer.blender.org/T98863),
[T99002](http://developer.blender.org/T99002),
[T99538](http://developer.blender.org/T99538),
[T99686](http://developer.blender.org/T99686),
[T99714](http://developer.blender.org/T99714),
[T99783](http://developer.blender.org/T99783),
[T99803](http://developer.blender.org/T99803),
[T99804](http://developer.blender.org/T99804),
[T99833](http://developer.blender.org/T99833),
[T99860](http://developer.blender.org/T99860),
[T99883](http://developer.blender.org/T99883),
[T97146](http://developer.blender.org/T97146),
[T100033](http://developer.blender.org/T100033),
[T99999](http://developer.blender.org/T99999),
[T99986](http://developer.blender.org/T99986),
[T99982](http://developer.blender.org/T99982),
[T99974](http://developer.blender.org/T99974),
[T99946](http://developer.blender.org/T99946),
[T99930](http://developer.blender.org/T99930),
[T99942](http://developer.blender.org/T99942)

  - Closed as Duplicate: 10

[T100080](http://developer.blender.org/T100080),
[T100046](http://developer.blender.org/T100046),
[T100047](http://developer.blender.org/T100047),
[T99720](http://developer.blender.org/T99720),
[T100036](http://developer.blender.org/T100036),
[T100019](http://developer.blender.org/T100019),
[T100021](http://developer.blender.org/T100021),
[T99988](http://developer.blender.org/T99988),
[T99944](http://developer.blender.org/T99944),
[T99950](http://developer.blender.org/T99950)

  - Closed as Resolved: 1

[T93422](http://developer.blender.org/T93422)

  - Needs Information from User(open): 19

[T100045](http://developer.blender.org/T100045),
[T100039](http://developer.blender.org/T100039),
[T100015](http://developer.blender.org/T100015),
[T100011](http://developer.blender.org/T100011),
[T100007](http://developer.blender.org/T100007),
[T100005](http://developer.blender.org/T100005),
[T99990](http://developer.blender.org/T99990),
[T99985](http://developer.blender.org/T99985),
[T99972](http://developer.blender.org/T99972),
[T99971](http://developer.blender.org/T99971),
[T99954](http://developer.blender.org/T99954),
[T99952](http://developer.blender.org/T99952),
[T99951](http://developer.blender.org/T99951),
[T99945](http://developer.blender.org/T99945),
[T99941](http://developer.blender.org/T99941),
[T99940](http://developer.blender.org/T99940),
[T99935](http://developer.blender.org/T99935),
[T99918](http://developer.blender.org/T99918),
[T99910](http://developer.blender.org/T99910)

  - Needs Information from Developers: 6

[T100030](http://developer.blender.org/T100030),
[T100042](http://developer.blender.org/T100042),
[T100029](http://developer.blender.org/T100029),
[T99987](http://developer.blender.org/T99987),
[T99993](http://developer.blender.org/T99993),
[T99929](http://developer.blender.org/T99929)

  - Own Reported: 0

<!-- end list -->

  - Diff submitted: 3

[D15587](http://developer.blender.org/D15587): Fix T100040: Crash when
transform applied on multi-user image  
[D15256](http://developer.blender.org/D15256): Fix T60609: Avoid undo
push when no changes have been made in color panel  
[D15574](http://developer.blender.org/D15574): Fix T99997: Calling
teleport without waiting for the previous event disables gravity

## Week 62 (Aug 01 - 07)

This week: Triaging. Updated one patch  
Next week: Triaging.

**Total actions on tracker: 176**

  - Confirmed: 15

[T100264](http://developer.blender.org/T100264),
[T100191](http://developer.blender.org/T100191),
[T100200](http://developer.blender.org/T100200),
[T100189](http://developer.blender.org/T100189),
[T100182](http://developer.blender.org/T100182),
[T100168](http://developer.blender.org/T100168),
[T100166](http://developer.blender.org/T100166),
[T98574](http://developer.blender.org/T98574),
[T100138](http://developer.blender.org/T100138),
[T100091](http://developer.blender.org/T100091),
[T100093](http://developer.blender.org/T100093),
[T100074](http://developer.blender.org/T100074),
[T100085](http://developer.blender.org/T100085),
[T100099](http://developer.blender.org/T100099),
[T100103](http://developer.blender.org/T100103)

  - Closed as Archived: 32

[T100228](http://developer.blender.org/T100228),
[T98453](http://developer.blender.org/T98453),
[T100039](http://developer.blender.org/T100039),
[T99952](http://developer.blender.org/T99952),
[T100217](http://developer.blender.org/T100217),
[T97522](http://developer.blender.org/T97522),
[T98656](http://developer.blender.org/T98656),
[T98811](http://developer.blender.org/T98811),
[T98800](http://developer.blender.org/T98800),
[T92761](http://developer.blender.org/T92761),
[T97039](http://developer.blender.org/T97039),
[T98649](http://developer.blender.org/T98649),
[T96552](http://developer.blender.org/T96552),
[T99839](http://developer.blender.org/T99839),
[T98131](http://developer.blender.org/T98131),
[T98895](http://developer.blender.org/T98895),
[T99896](http://developer.blender.org/T99896),
[T99918](http://developer.blender.org/T99918),
[T99925](http://developer.blender.org/T99925),
[T99941](http://developer.blender.org/T99941),
[T99951](http://developer.blender.org/T99951),
[T99954](http://developer.blender.org/T99954),
[T99972](http://developer.blender.org/T99972),
[T99990](http://developer.blender.org/T99990),
[T99985](http://developer.blender.org/T99985),
[T100005](http://developer.blender.org/T100005),
[T100007](http://developer.blender.org/T100007),
[T100015](http://developer.blender.org/T100015),
[T100156](http://developer.blender.org/T100156),
[T100011](http://developer.blender.org/T100011),
[T100140](http://developer.blender.org/T100140),
[T99945](http://developer.blender.org/T99945)

  - Closed as Duplicate: 9

[T100223](http://developer.blender.org/T100223),
[T100159](http://developer.blender.org/T100159),
[T100180](http://developer.blender.org/T100180),
[T100181](http://developer.blender.org/T100181),
[T100154](http://developer.blender.org/T100154),
[T100108](http://developer.blender.org/T100108),
[T100087](http://developer.blender.org/T100087),
[T100089](http://developer.blender.org/T100089),
[T100090](http://developer.blender.org/T100090)

  - Closed as Resolved: 3

[T100040](http://developer.blender.org/T100040),
[T92099](http://developer.blender.org/T92099), Part of
[T84999](http://developer.blender.org/T84999)

  - Needs Information from User: 10

[T100263](http://developer.blender.org/T100263),
[T100161](http://developer.blender.org/T100161),
[T100158](http://developer.blender.org/T100158),
[T100130](http://developer.blender.org/T100130),
[T100098](http://developer.blender.org/T100098),
[T100095](http://developer.blender.org/T100095),
[T100094](http://developer.blender.org/T100094),
[T100084](http://developer.blender.org/T100084),
[T100068](http://developer.blender.org/T100068),
[T100057](http://developer.blender.org/T100057)

  - Needs Information from Developers: 3

[T100212](http://developer.blender.org/T100212),
[T100096](http://developer.blender.org/T100096),
[T100106](http://developer.blender.org/T100106)

  - Own Reported: 0

<!-- end list -->

  - Diff submitted(update): 1

[D15256](http://developer.blender.org/D15256): Fix T60609: Avoid undo
push when no changes have been made in color panel

## Week 63 (Aug 08 - 14)

This week: Triaging. Submitted one fix. Visited few "Confirmed" reports
of \`Undo\` module  
Next week: Triaging.

**Total actions on tracker: 178**

  - Confirmed: 13

[T100381](http://developer.blender.org/T100381),
[T100384](http://developer.blender.org/T100384),
[T99254](http://developer.blender.org/T99254),
[T100350](http://developer.blender.org/T100350),
[T100321](http://developer.blender.org/T100321),
[T100340](http://developer.blender.org/T100340),
[T100261](http://developer.blender.org/T100261),
[T100302](http://developer.blender.org/T100302),
[T100306](http://developer.blender.org/T100306),
[T100300](http://developer.blender.org/T100300),
[T100235](http://developer.blender.org/T100235),
[T100277](http://developer.blender.org/T100277),
[T100278](http://developer.blender.org/T100278)

  - Closed as Archived: 26

[T100383](http://developer.blender.org/T100383),
[T99348](http://developer.blender.org/T99348),
[T96118](http://developer.blender.org/T96118),
[T99570](http://developer.blender.org/T99570),
[T99806](http://developer.blender.org/T99806),
[T99821](http://developer.blender.org/T99821),
[T99940](http://developer.blender.org/T99940),
[T100045](http://developer.blender.org/T100045),
[T100057](http://developer.blender.org/T100057),
[T100068](http://developer.blender.org/T100068),
[T100084](http://developer.blender.org/T100084),
[T100094](http://developer.blender.org/T100094),
[T100095](http://developer.blender.org/T100095),
[T100098](http://developer.blender.org/T100098),
[T100161](http://developer.blender.org/T100161),
[T100176](http://developer.blender.org/T100176),
[T100355](http://developer.blender.org/T100355),
[T100357](http://developer.blender.org/T100357),
[T100341](http://developer.blender.org/T100341),
[T100274](http://developer.blender.org/T100274),
[T100305](http://developer.blender.org/T100305),
[T100309](http://developer.blender.org/T100309),
[T95873](http://developer.blender.org/T95873),
[T100279](http://developer.blender.org/T100279),
[T100272](http://developer.blender.org/T100272),
[T100271](http://developer.blender.org/T100271)

  - Closed as Duplicate: 13

[T100380](http://developer.blender.org/T100380),
[T100382](http://developer.blender.org/T100382),
[T100359](http://developer.blender.org/T100359),
[T100348](http://developer.blender.org/T100348),
[T100313](http://developer.blender.org/T100313),
[T100292](http://developer.blender.org/T100292),
[T100288](http://developer.blender.org/T100288),
[T100283](http://developer.blender.org/T100283),
[T100284](http://developer.blender.org/T100284),
[T100229](http://developer.blender.org/T100229),
[T100230](http://developer.blender.org/T100230),
[T100247](http://developer.blender.org/T100247),
[T100267](http://developer.blender.org/T100267)

  - Closed as Resolved: 3

[T100261](http://developer.blender.org/T100261),
[T100170](http://developer.blender.org/T100170),
[T100340](http://developer.blender.org/T100340)

  - Needs Information from User: 12

[T100363](http://developer.blender.org/T100363),
[T100358](http://developer.blender.org/T100358),
[T100353](http://developer.blender.org/T100353),
[T100338](http://developer.blender.org/T100338),
[T100336](http://developer.blender.org/T100336),
[T100335](http://developer.blender.org/T100335),
[T100333](http://developer.blender.org/T100333),
[T100304](http://developer.blender.org/T100304),
[T100287](http://developer.blender.org/T100287),
[T100253](http://developer.blender.org/T100253),
[T100243](http://developer.blender.org/T100243),
[T100238](http://developer.blender.org/T100238)

  - Needs Information from Developers: 4

[T100255](http://developer.blender.org/T100255),
[T100260](http://developer.blender.org/T100260),
[T100265](http://developer.blender.org/T100265),
[T100272](http://developer.blender.org/T100272)

  - Own Reported: 0

<!-- end list -->

  - Diff submitted: 1

[D15671](http://developer.blender.org/D15671): Fix T100200: Undo step
created when index of a UIList is changed

## Week 64 (Aug 15 - 21)

This week: Triaging. Submitted one fix.  
Visited a few inactive reports tagged as "Need Information from
Developers"  
I was less active on Monday ([national
Holiday](https://en.wikipedia.org/wiki/Independence_Day_\(India\)))

Next week: Triaging.

**Total actions on tracker: 179**

  - Confirmed: 10

[T100521](http://developer.blender.org/T100521),
[T100533](http://developer.blender.org/T100533),
[T100527](http://developer.blender.org/T100527),
[T94633](http://developer.blender.org/T94633),
[T100485](http://developer.blender.org/T100485),
[T100470](http://developer.blender.org/T100470),
[T100425](http://developer.blender.org/T100425),
[T100430](http://developer.blender.org/T100430),
[T100377](http://developer.blender.org/T100377),
[T100385](http://developer.blender.org/T100385)

  - Closed as Archived: 23

[T100510](http://developer.blender.org/T100510),
[T100518](http://developer.blender.org/T100518),
[T99025](http://developer.blender.org/T99025),
[T100238](http://developer.blender.org/T100238),
[T100243](http://developer.blender.org/T100243),
[T100253](http://developer.blender.org/T100253),
[T100263](http://developer.blender.org/T100263),
[T100287](http://developer.blender.org/T100287),
[T100333](http://developer.blender.org/T100333),
[T100335](http://developer.blender.org/T100335),
[T100336](http://developer.blender.org/T100336),
[T100358](http://developer.blender.org/T100358),
[T100487](http://developer.blender.org/T100487),
[T96019](http://developer.blender.org/T96019),
[T96588](http://developer.blender.org/T96588),
[T100311](http://developer.blender.org/T100311),
[T100088](http://developer.blender.org/T100088),
[T99688](http://developer.blender.org/T99688),
[T100472](http://developer.blender.org/T100472),
[T100455](http://developer.blender.org/T100455),
[T100471](http://developer.blender.org/T100471),
[T100458](http://developer.blender.org/T100458),
[T100399](http://developer.blender.org/T100399)

  - Closed as Duplicate: 6

[T100473](http://developer.blender.org/T100473),
[T85441](http://developer.blender.org/T85441),
[T100456](http://developer.blender.org/T100456),
[T100434](http://developer.blender.org/T100434),
[T90253](http://developer.blender.org/T90253),
[T100386](http://developer.blender.org/T100386)

  - Closed as Resolved: 1

[T100489](http://developer.blender.org/T100489)

  - Needs Information from User: 16

[T100534](http://developer.blender.org/T100534),
[T100514](http://developer.blender.org/T100514),
[T100510](http://developer.blender.org/T100510),
[T100488](http://developer.blender.org/T100488),
[T100484](http://developer.blender.org/T100484),
[T100453](http://developer.blender.org/T100453),
[T100449](http://developer.blender.org/T100449),
[T100426](http://developer.blender.org/T100426),
[T100405](http://developer.blender.org/T100405),
[T100404](http://developer.blender.org/T100404),
[T100388](http://developer.blender.org/T100388),
[T98584](http://developer.blender.org/T98584),
[T82412](http://developer.blender.org/T82412),
[T82406](http://developer.blender.org/T82406),
[T79445](http://developer.blender.org/T79445),
[T78495](http://developer.blender.org/T78495)

  - Needs Information from Developers: 2

[T100474](http://developer.blender.org/T100474),
[T100447](http://developer.blender.org/T100447)

  - Own Reported: 2

[T100521](http://developer.blender.org/T100521),
[T100475](http://developer.blender.org/T100475)

  - Diff submitted: 1

[D15739](http://developer.blender.org/D15739): Fix T98968: Reroute node
doesn't belongs to the frame automatically when added through lasso
gesture

## Week 65 (Aug 22 - 28)

This week: Triaging. Updated one patch. AFK on Tuesday so shifted
working hours to Saturday  
Next week: Triaging. Unavailable on Monday (Will again adjust working
hours)  
[Public holiday](https://en.wikipedia.org/wiki/Ganesh_Chaturthi) in
Maharashtra on Wednesday

**Total actions on tracker: 159**

  - Confirmed: 12

[T100654](http://developer.blender.org/T100654),
[T100649](http://developer.blender.org/T100649),
[T100637](http://developer.blender.org/T100637),
[T100632](http://developer.blender.org/T100632),
[T100617](http://developer.blender.org/T100617),
[T100614](http://developer.blender.org/T100614),
[T100584](http://developer.blender.org/T100584),
[T100582](http://developer.blender.org/T100582),
[T100546](http://developer.blender.org/T100546),
[T100542](http://developer.blender.org/T100542),
[T100538](http://developer.blender.org/T100538),
[T100545](http://developer.blender.org/T100545)

  - Closed as Archived: 20

[T100662](http://developer.blender.org/T100662),
[T79445](http://developer.blender.org/T79445),
[T98584](http://developer.blender.org/T98584),
[T99263](http://developer.blender.org/T99263),
[T99578](http://developer.blender.org/T99578),
[T99935](http://developer.blender.org/T99935),
[T99971](http://developer.blender.org/T99971),
[T100363](http://developer.blender.org/T100363),
[T100388](http://developer.blender.org/T100388),
[T100449](http://developer.blender.org/T100449),
[T100453](http://developer.blender.org/T100453),
[T100488](http://developer.blender.org/T100488),
[T100636](http://developer.blender.org/T100636),
[T100624](http://developer.blender.org/T100624),
[T100619](http://developer.blender.org/T100619),
[T100158](http://developer.blender.org/T100158),
[T100559](http://developer.blender.org/T100559),
[T100544](http://developer.blender.org/T100544),
[T100543](http://developer.blender.org/T100543),
[T100548](http://developer.blender.org/T100548)

  - Closed as Duplicate: 4

[T100643](http://developer.blender.org/T100643),
[T100635](http://developer.blender.org/T100635),
[T98409](http://developer.blender.org/T98409),
[T100615](http://developer.blender.org/T100615)

  - Closed as Resolved: 2

[T100405](http://developer.blender.org/T100405),
[T89928](http://developer.blender.org/T89928)

  - Needs Information from User: 23

[T100667](http://developer.blender.org/T100667),
[T100664](http://developer.blender.org/T100664),
[T100663](http://developer.blender.org/T100663),
[T100658](http://developer.blender.org/T100658),
[T100656](http://developer.blender.org/T100656),
[T100655](http://developer.blender.org/T100655),
[T100653](http://developer.blender.org/T100653),
[T100638](http://developer.blender.org/T100638),
[T100634](http://developer.blender.org/T100634),
[T100630](http://developer.blender.org/T100630),
[T100622](http://developer.blender.org/T100622),
[T100620](http://developer.blender.org/T100620),
[T100612](http://developer.blender.org/T100612),
[T100601](http://developer.blender.org/T100601),
[T100600](http://developer.blender.org/T100600),
[T100597](http://developer.blender.org/T100597),
[T100581](http://developer.blender.org/T100581),
[T100575](http://developer.blender.org/T100575),
[T100572](http://developer.blender.org/T100572),
[T100541](http://developer.blender.org/T100541),
[T100535](http://developer.blender.org/T100535),
[T100529](http://developer.blender.org/T100529),
[T100509](http://developer.blender.org/T100509)

  - Needs Information from Developers: 4

[T100659](http://developer.blender.org/T100659),
[T100639](http://developer.blender.org/T100639),
[T100623](http://developer.blender.org/T100623),
[T100531](http://developer.blender.org/T100531)

  - Own Reported: 1

[T100625](http://developer.blender.org/T100625)

  - Diff submitted(updated): 1

[D15739](http://developer.blender.org/D15739): Fix T98968: Reroute node
doesn't belongs to the frame automatically when added through lasso
gesture

## Week 66 (Aug 29 - 04)

This week: Triaging. Less active on Wednesday due to [Public
holiday](https://en.wikipedia.org/wiki/Ganesh_Chaturthi)  
Next week: Triaging.  
**Total actions on tracker: 149**

  - Confirmed: 11

[T100791](http://developer.blender.org/T100791),
[T100810](http://developer.blender.org/T100810),
[T100788](http://developer.blender.org/T100788),
[T100787](http://developer.blender.org/T100787),
[T100786](http://developer.blender.org/T100786),
[T100768](http://developer.blender.org/T100768),
[T100760](http://developer.blender.org/T100760),
[T100742](http://developer.blender.org/T100742),
[T100716](http://developer.blender.org/T100716),
[T100700](http://developer.blender.org/T100700),
[T100697](http://developer.blender.org/T100697)

  - Closed as Archived: 20

[T100798](http://developer.blender.org/T100798),
[T100809](http://developer.blender.org/T100809),
[T100802](http://developer.blender.org/T100802),
[T100781](http://developer.blender.org/T100781),
[T100759](http://developer.blender.org/T100759),
[T100597](http://developer.blender.org/T100597),
[T100612](http://developer.blender.org/T100612),
[T78495](http://developer.blender.org/T78495),
[T99945](http://developer.blender.org/T99945),
[T100304](http://developer.blender.org/T100304),
[T100395](http://developer.blender.org/T100395),
[T100510](http://developer.blender.org/T100510),
[T100514](http://developer.blender.org/T100514),
[T100529](http://developer.blender.org/T100529),
[T100535](http://developer.blender.org/T100535),
[T100572](http://developer.blender.org/T100572),
[T100743](http://developer.blender.org/T100743),
[T100726](http://developer.blender.org/T100726),
[T100725](http://developer.blender.org/T100725),
[T100549](http://developer.blender.org/T100549)

  - Closed as Duplicate: 8

[T100785](http://developer.blender.org/T100785),
[T100774](http://developer.blender.org/T100774),
[T100762](http://developer.blender.org/T100762),
[T100744](http://developer.blender.org/T100744),
[T100730](http://developer.blender.org/T100730),
[T100739](http://developer.blender.org/T100739),
[T100715](http://developer.blender.org/T100715),
[T100693](http://developer.blender.org/T100693)

  - Closed as Resolved: 5

[T100541](http://developer.blender.org/T100541),
[T100653](http://developer.blender.org/T100653),
[T100656](http://developer.blender.org/T100656),
[T100534](http://developer.blender.org/T100534),
[T98968](http://developer.blender.org/T98968)

  - Needs Information from User: 14

[T100808](http://developer.blender.org/T100808),
[T100799](http://developer.blender.org/T100799),
[T100790](http://developer.blender.org/T100790),
[T100789](http://developer.blender.org/T100789),
[T100766](http://developer.blender.org/T100766),
[T100764](http://developer.blender.org/T100764),
[T100740](http://developer.blender.org/T100740),
[T100727](http://developer.blender.org/T100727),
[T100722](http://developer.blender.org/T100722),
[T100713](http://developer.blender.org/T100713),
[T100698](http://developer.blender.org/T100698),
[T100618](http://developer.blender.org/T100618),
[T99097](http://developer.blender.org/T99097),
[T89124](http://developer.blender.org/T89124)

  - Needs Information from Developers: 2

[T100664](http://developer.blender.org/T100664),
[T100699](http://developer.blender.org/T100699)

  - Own Reported: 2

[T100702](http://developer.blender.org/T100702),
[T100700](http://developer.blender.org/T100700)

  - Diff submitted: 0

## Week 67 (Sep 05 - 11)

This week: Triaging.  
Next week: I've mid-semester next week so won't be active on Weekdays.
Triaging on weekend.  
**Total actions on tracker: 166**

  - Confirmed: 16

[T100959](http://developer.blender.org/T100959),
[T100920](http://developer.blender.org/T100920),
[T100926](http://developer.blender.org/T100926),
[T100908](http://developer.blender.org/T100908),
[T100911](http://developer.blender.org/T100911),
[T100903](http://developer.blender.org/T100903),
[T100890](http://developer.blender.org/T100890),
[T100895](http://developer.blender.org/T100895),
[T100862](http://developer.blender.org/T100862),
[T100863](http://developer.blender.org/T100863),
[T100857](http://developer.blender.org/T100857),
[T100841](http://developer.blender.org/T100841),
[T100843](http://developer.blender.org/T100843),
[T100842](http://developer.blender.org/T100842),
[T100822](http://developer.blender.org/T100822),
[T100820](http://developer.blender.org/T100820)

  - Closed as Archived: 6

[T100932](http://developer.blender.org/T100932),
[T100869](http://developer.blender.org/T100869),
[T100880](http://developer.blender.org/T100880),
[T100853](http://developer.blender.org/T100853),
[T100818](http://developer.blender.org/T100818),
[T100805](http://developer.blender.org/T100805),
[T100943](http://developer.blender.org/T100943)

  - Closed as Duplicate: 14

[T100964](http://developer.blender.org/T100964),
[T100965](http://developer.blender.org/T100965),
[T100955](http://developer.blender.org/T100955),
[T100944](http://developer.blender.org/T100944),
[T100933](http://developer.blender.org/T100933),
[T100929](http://developer.blender.org/T100929),
[T100930](http://developer.blender.org/T100930),
[T100927](http://developer.blender.org/T100927),
[T100916](http://developer.blender.org/T100916),
[T100865](http://developer.blender.org/T100865),
[T100868](http://developer.blender.org/T100868),
[T100828](http://developer.blender.org/T100828),
[T100817](http://developer.blender.org/T100817),
[T100799](http://developer.blender.org/T100799)

  - Closed as Resolved: 1

[T100857](http://developer.blender.org/T100857)

  - Needs Information from User: 16

[T100931](http://developer.blender.org/T100931),
[T100928](http://developer.blender.org/T100928),
[T100923](http://developer.blender.org/T100923),
[T100901](http://developer.blender.org/T100901),
[T100899](http://developer.blender.org/T100899),
[T100898](http://developer.blender.org/T100898),
[T100889](http://developer.blender.org/T100889),
[T100881](http://developer.blender.org/T100881),
[T100876](http://developer.blender.org/T100876),
[T100871](http://developer.blender.org/T100871),
[T100870](http://developer.blender.org/T100870),
[T100867](http://developer.blender.org/T100867),
[T100844](http://developer.blender.org/T100844),
[T100829](http://developer.blender.org/T100829),
[T100815](http://developer.blender.org/T100815),
[T100811](http://developer.blender.org/T100811)

  - Needs Information from Developers: 2

[T100914](http://developer.blender.org/T100914),
[T100819](http://developer.blender.org/T100819)

  - Own Reported: 1

[T100823](http://developer.blender.org/T100823)

  - Diff submitted: 0

<!-- end list -->

  - Manual Contribution: 1

[rBM9535](http://developer.blender.org/rBM9535)

## Week 68 (Sep 12 - 18)

This week: Had mid-semester exam this week. So did triaging on weekend
only.  
Next week: Triaging. Code contribution.

**Total actions on tracker: 92**

  - Confirmed: 2

[T99836](http://developer.blender.org/T99836),
[T100537](http://developer.blender.org/T100537)

  - Closed as Archived: 22

[T101147](http://developer.blender.org/T101147),
[T101132](http://developer.blender.org/T101132),
[T100870](http://developer.blender.org/T100870),
[T100509](http://developer.blender.org/T100509),
[T100630](http://developer.blender.org/T100630),
[T100663](http://developer.blender.org/T100663),
[T100713](http://developer.blender.org/T100713),
[T100740](http://developer.blender.org/T100740),
[T100764](http://developer.blender.org/T100764),
[T100789](http://developer.blender.org/T100789),
[T100790](http://developer.blender.org/T100790),
[T100808](http://developer.blender.org/T100808),
[T100815](http://developer.blender.org/T100815),
[T100829](http://developer.blender.org/T100829),
[T100844](http://developer.blender.org/T100844),
[T100867](http://developer.blender.org/T100867),
[T100871](http://developer.blender.org/T100871),
[T100876](http://developer.blender.org/T100876),
[T100898](http://developer.blender.org/T100898),
[T100901](http://developer.blender.org/T100901),
[T100923](http://developer.blender.org/T100923),
[T100931](http://developer.blender.org/T100931)

  - Closed as Duplicate: 6

[T83533](http://developer.blender.org/T83533),
[T101159](http://developer.blender.org/T101159),
[T101141](http://developer.blender.org/T101141),
[T101136](http://developer.blender.org/T101136),
[T101024](http://developer.blender.org/T101024),
[T101047](http://developer.blender.org/T101047)

  - Closed as Resolved: 1

[T99032](http://developer.blender.org/T99032)

  - Needs Information from User: 6

[T101145](http://developer.blender.org/T101145),
[T101143](http://developer.blender.org/T101143),
[T101142](http://developer.blender.org/T101142),
[T101135](http://developer.blender.org/T101135),
[T101127](http://developer.blender.org/T101127),
[T101021](http://developer.blender.org/T101021)

  - Needs Information from Developers: 1

[T101144](http://developer.blender.org/T101144)

  - Own Reported: 0

<!-- end list -->

  - Diff submitted: 0

## Week 69 (Sep 19 - 25)

This week: Triaging. General code contribution  
Next week: Triaging.

**Total actions on tracker: 164**

  - Confirmed: 18

[T101341](http://developer.blender.org/T101341),
[T101231](http://developer.blender.org/T101231),
[T101240](http://developer.blender.org/T101240),
[T101256](http://developer.blender.org/T101256),
[T85978](http://developer.blender.org/T85978),
[T101249](http://developer.blender.org/T101249),
[T101239](http://developer.blender.org/T101239),
[T101197](http://developer.blender.org/T101197),
[T101233](http://developer.blender.org/T101233),
[T101201](http://developer.blender.org/T101201),
[T101188](http://developer.blender.org/T101188),
[T101203](http://developer.blender.org/T101203),
[T101204](http://developer.blender.org/T101204),
[T101196](http://developer.blender.org/T101196),
[T101138](http://developer.blender.org/T101138),
[T101160](http://developer.blender.org/T101160),
[T101164](http://developer.blender.org/T101164),
[T101179](http://developer.blender.org/T101179)

  - Closed as Archived: 11

[T101344](http://developer.blender.org/T101344),
[T101319](http://developer.blender.org/T101319),
[T101292](http://developer.blender.org/T101292),
[T101265](http://developer.blender.org/T101265),
[T101257](http://developer.blender.org/T101257),
[T101246](http://developer.blender.org/T101246),
[T101247](http://developer.blender.org/T101247),
[T101238](http://developer.blender.org/T101238),
[T101230](http://developer.blender.org/T101230),
[T101200](http://developer.blender.org/T101200),
[T101191](http://developer.blender.org/T101191)

  - Closed as Duplicate: 8

[T101296](http://developer.blender.org/T101296),
[T101243](http://developer.blender.org/T101243),
[T101216](http://developer.blender.org/T101216),
[T101204](http://developer.blender.org/T101204),
[T101193](http://developer.blender.org/T101193),
[T101186](http://developer.blender.org/T101186),
[T101181](http://developer.blender.org/T101181),
[T101168](http://developer.blender.org/T101168)

  - Closed as Resolved: 2

[T101179](http://developer.blender.org/T101179),
[T101165](http://developer.blender.org/T101165)

  - Needs Information from User: 14

[T101346](http://developer.blender.org/T101346),
[T101345](http://developer.blender.org/T101345),
[T101342](http://developer.blender.org/T101342),
[T101334](http://developer.blender.org/T101334),
[T101276](http://developer.blender.org/T101276),
[T101250](http://developer.blender.org/T101250),
[T101245](http://developer.blender.org/T101245),
[T101241](http://developer.blender.org/T101241),
[T101219](http://developer.blender.org/T101219),
[T101173](http://developer.blender.org/T101173),
[T101170](http://developer.blender.org/T101170),
[T101162](http://developer.blender.org/T101162),
[T101161](http://developer.blender.org/T101161),
[T101157](http://developer.blender.org/T101157)

  - Needs Information from Developers: 1

[T101259](http://developer.blender.org/T101259)

  - Own Reported: 1

[T101209](http://developer.blender.org/T101209)

  - Diff submitted: 3

[D16040](http://developer.blender.org/D16040): UV editor: Improve Ui
alignment in overlays panel  
[D16031](http://developer.blender.org/D16031): Fix T101233: Crash on
deleting the object in outliner due to null pointer access  
[D15671](http://developer.blender.org/D15671): Fix T100200: Undo step
created when index of a UIList is changed  

## Week 70 (Sep 26 - 02)

This week: Triaging. General code contribution  
Next week: Triaging.

**Total actions on tracker: 154**

  - Confirmed: 17

[T101530](http://developer.blender.org/T101530),
[T101533](http://developer.blender.org/T101533),
[T101537](http://developer.blender.org/T101537),
[T101502](http://developer.blender.org/T101502),
[T101507](http://developer.blender.org/T101507),
[T101506](http://developer.blender.org/T101506),
[T101510](http://developer.blender.org/T101510),
[T101447](http://developer.blender.org/T101447),
[T101445](http://developer.blender.org/T101445),
[T101348](http://developer.blender.org/T101348),
[T101374](http://developer.blender.org/T101374),
[T101372](http://developer.blender.org/T101372),
[T101357](http://developer.blender.org/T101357),
[T101370](http://developer.blender.org/T101370),
[T101365](http://developer.blender.org/T101365),
[T101366](http://developer.blender.org/T101366),
[T101369](http://developer.blender.org/T101369)

  - Closed as Archived: 16

[T101512](http://developer.blender.org/T101512),
[T101513](http://developer.blender.org/T101513),
[T101478](http://developer.blender.org/T101478),
[T100622](http://developer.blender.org/T100622),
[T100766](http://developer.blender.org/T100766),
[T101127](http://developer.blender.org/T101127),
[T101135](http://developer.blender.org/T101135),
[T101142](http://developer.blender.org/T101142),
[T101143](http://developer.blender.org/T101143),
[T101145](http://developer.blender.org/T101145),
[T101157](http://developer.blender.org/T101157),
[T101170](http://developer.blender.org/T101170),
[T101173](http://developer.blender.org/T101173),
[T101241](http://developer.blender.org/T101241),
[T89124](http://developer.blender.org/T89124),
[T101382](http://developer.blender.org/T101382)

  - Closed as Duplicate: 7

[T101527](http://developer.blender.org/T101527),
[T101519](http://developer.blender.org/T101519),
[T101437](http://developer.blender.org/T101437),
[T101383](http://developer.blender.org/T101383),
[T100967](http://developer.blender.org/T100967),
[T101336](http://developer.blender.org/T101336),
[T101349](http://developer.blender.org/T101349)

  - Closed as Resolved: 6

[T101478](http://developer.blender.org/T101478),
[T101507](http://developer.blender.org/T101507),
[T100484](http://developer.blender.org/T100484),
[T101419](http://developer.blender.org/T101419),
[T100702](http://developer.blender.org/T100702),
[T101504](http://developer.blender.org/T101504)

  - Needs Information from User: 9

[T101546](http://developer.blender.org/T101546),
[T101544](http://developer.blender.org/T101544),
[T101541](http://developer.blender.org/T101541),
[T101540](http://developer.blender.org/T101540),
[T101537](http://developer.blender.org/T101537),
[T101534](http://developer.blender.org/T101534),
[T101520](http://developer.blender.org/T101520),
[T101359](http://developer.blender.org/T101359),
[T101324](http://developer.blender.org/T101324)

  - Needs Information from Developers: 2

[T101418](http://developer.blender.org/T101418),
[T101269](http://developer.blender.org/T101269)

  - Own Reported: 0

<!-- end list -->

  - Diff submitted: 2

[D16120](http://developer.blender.org/D16120): Fix T101504: Crash when
repeat value is zero  
[D15671](http://developer.blender.org/D15671): Fix T100200: Undo step
created when index of a UIList is changed

  - Manual Contribution: 1

[rBM9593](http://developer.blender.org/rBM9593): Fix T101507: Update
keymap information for fill tool

## Week 71 (Oct 03 - 09)

This week: Triaging. General code contribution.  
Got commit rights last week. Committed a patch to master branch
myself.  
Went through commit history and added relevant commits in 3.3LTS
candidates list.  
Next week: Triaging.

**Total actions on tracker: 163**

  - Confirmed: 16

[T101694](http://developer.blender.org/T101694),
[T101678](http://developer.blender.org/T101678),
[T101462](http://developer.blender.org/T101462),
[T100890](http://developer.blender.org/T100890),
[T101660](http://developer.blender.org/T101660),
[T101621](http://developer.blender.org/T101621),
[T101602](http://developer.blender.org/T101602),
[T101606](http://developer.blender.org/T101606),
[T101583](http://developer.blender.org/T101583),
[T101529](http://developer.blender.org/T101529),
[T101575](http://developer.blender.org/T101575),
[T101526](http://developer.blender.org/T101526),
[T101554](http://developer.blender.org/T101554),
[T101565](http://developer.blender.org/T101565),
[T101548](http://developer.blender.org/T101548),
[T101547](http://developer.blender.org/T101547)

  - Closed as Archived: 17

[T100426](http://developer.blender.org/T100426),
[T100655](http://developer.blender.org/T100655),
[T100698](http://developer.blender.org/T100698),
[T100889](http://developer.blender.org/T100889),
[T101250](http://developer.blender.org/T101250),
[T101342](http://developer.blender.org/T101342),
[T101345](http://developer.blender.org/T101345),
[T101346](http://developer.blender.org/T101346),
[T101664](http://developer.blender.org/T101664),
[T101634](http://developer.blender.org/T101634),
[T101589](http://developer.blender.org/T101589),
[T101597](http://developer.blender.org/T101597),
[T101614](http://developer.blender.org/T101614),
[T101600](http://developer.blender.org/T101600),
[T101582](http://developer.blender.org/T101582),
[T101585](http://developer.blender.org/T101585),
[T101574](http://developer.blender.org/T101574)

  - Closed as Duplicate: 3

[T101707](http://developer.blender.org/T101707),
[T101649](http://developer.blender.org/T101649),
[T101635](http://developer.blender.org/T101635)

  - Closed as Resolved: 2

[T101603](http://developer.blender.org/T101603),
[T101233](http://developer.blender.org/T101233)

  - Needs Information from User: 13

[T101695](http://developer.blender.org/T101695),
[T101639](http://developer.blender.org/T101639),
[T101637](http://developer.blender.org/T101637),
[T101633](http://developer.blender.org/T101633),
[T101608](http://developer.blender.org/T101608),
[T101605](http://developer.blender.org/T101605),
[T101587](http://developer.blender.org/T101587),
[T101581](http://developer.blender.org/T101581),
[T101573](http://developer.blender.org/T101573),
[T101570](http://developer.blender.org/T101570),
[T101566](http://developer.blender.org/T101566),
[T101555](http://developer.blender.org/T101555),
[T101516](http://developer.blender.org/T101516)

  - Needs Information from Developers: 1

[T100890](http://developer.blender.org/T100890)

  - Own reported: 2

[T101674](http://developer.blender.org/T101674),
[T101606](http://developer.blender.org/T101606)

  - Diff submitted: 3

[D16199](http://developer.blender.org/D16199): Fix T101694: Change
operator for unhide face sets  
[D15574](http://developer.blender.org/D15574): Fix T99997: Calling
teleport without waiting for the previous event disables gravity  
[D16031](http://developer.blender.org/D16031): Fix T101233: Crash on
deleting the object in outliner due to null pointer access  

  - Commits:1

[rB4186b0ebe4e86eb4de5e88dd333ea3884748173c](http://developer.blender.org/rB4186b0ebe4e86eb4de5e88dd333ea3884748173c)

## Week 72 (Oct 10 - 16)

This week: Triaging. General code contribution.  
Did some investigation of reported bugs:
[T100993](http://developer.blender.org/T100993),
[T101501](http://developer.blender.org/T101501),
[T101795](http://developer.blender.org/T101795)  
Next Week: Triaging  
**Total actions on tracker: 129**

  - Confirmed: 13

[T101851](http://developer.blender.org/T101851),
[T101843](http://developer.blender.org/T101843),
[T101836](http://developer.blender.org/T101836),
[T101794](http://developer.blender.org/T101794),
[T99089](http://developer.blender.org/T99089),
[T101786](http://developer.blender.org/T101786),
[T101111](http://developer.blender.org/T101111),
[T101259](http://developer.blender.org/T101259),
[T101727](http://developer.blender.org/T101727),
[T101737](http://developer.blender.org/T101737),
[T101699](http://developer.blender.org/T101699),
[T101681](http://developer.blender.org/T101681),
[T101669](http://developer.blender.org/T101669)

  - Closed as Archived: 10

[T101846](http://developer.blender.org/T101846),
[T101844](http://developer.blender.org/T101844),
[T100212](http://developer.blender.org/T100212),
[T101784](http://developer.blender.org/T101784),
[T101756](http://developer.blender.org/T101756),
[T99920](http://developer.blender.org/T99920),
[T101712](http://developer.blender.org/T101712),
[T100658](http://developer.blender.org/T100658),
[T101670](http://developer.blender.org/T101670),
[T101638](http://developer.blender.org/T101638)

  - Closed as Duplicate: 7

[T101847](http://developer.blender.org/T101847),
[T101839](http://developer.blender.org/T101839),
[T101837](http://developer.blender.org/T101837),
[T101828](http://developer.blender.org/T101828),
[T101783](http://developer.blender.org/T101783),
[T101754](http://developer.blender.org/T101754),
[T101687](http://developer.blender.org/T101687)

  - Closed as Resolved: 6

[T101786](http://developer.blender.org/T101786),
[T101209](http://developer.blender.org/T101209),
[T101722](http://developer.blender.org/T101722),
[T101699](http://developer.blender.org/T101699),
[T93936](http://developer.blender.org/T93936),
[T101694](http://developer.blender.org/T101694)

  - Needs Information from User: 6

[T101845](http://developer.blender.org/T101845),
[T101806](http://developer.blender.org/T101806),
[T101758](http://developer.blender.org/T101758),
[T101686](http://developer.blender.org/T101686),
[T101264](http://developer.blender.org/T101264),
[T100576](http://developer.blender.org/T100576)

  - Needs Information from Developers: 3

[T100993](http://developer.blender.org/T100993),
[T101501](http://developer.blender.org/T101501),
[T101710](http://developer.blender.org/T101710)

  - Own Reported: 2

[T101743](http://developer.blender.org/T101743),
[T101722](http://developer.blender.org/T101722)

  - Diff submitted: 1

[D16206](http://developer.blender.org/D16206): Fix T101699: Crash on
hiding all face sets

  - Review and commit: 1

[rBf940ecde7aa569c5377b31b9cdb0e7810c9064b0](http://developer.blender.org/rBf940ecde7aa569c5377b31b9cdb0e7810c9064b0)

## Week 73 (Oct 17 - 23)

This week: Triaging. Bug Fixing (Committed one fix)  
Next Week: Triaging. Attending Blender Conference  
**Total actions on tracker: 169**

  - Confirmed: 15

[T102009](http://developer.blender.org/T102009),
[T102003](http://developer.blender.org/T102003),
[T101985](http://developer.blender.org/T101985),
[T101965](http://developer.blender.org/T101965),
[T101964](http://developer.blender.org/T101964),
[T101933](http://developer.blender.org/T101933),
[T101913](http://developer.blender.org/T101913),
[T101928](http://developer.blender.org/T101928),
[T101896](http://developer.blender.org/T101896),
[T68990](http://developer.blender.org/T68990),
[T101884](http://developer.blender.org/T101884),
[T101883](http://developer.blender.org/T101883),
[T101861](http://developer.blender.org/T101861),
[T101860](http://developer.blender.org/T101860),
[T101856](http://developer.blender.org/T101856)

  - Closed as Archived: 26

[T102007](http://developer.blender.org/T102007),
[T102002](http://developer.blender.org/T102002),
[T102004](http://developer.blender.org/T102004),
[T101987](http://developer.blender.org/T101987),
[T101962](http://developer.blender.org/T101962),
[T101961](http://developer.blender.org/T101961),
[T101958](http://developer.blender.org/T101958),
[T100576](http://developer.blender.org/T100576),
[T82406](http://developer.blender.org/T82406),
[T101161](http://developer.blender.org/T101161),
[T101264](http://developer.blender.org/T101264),
[T101324](http://developer.blender.org/T101324),
[T101534](http://developer.blender.org/T101534),
[T101540](http://developer.blender.org/T101540),
[T101546](http://developer.blender.org/T101546),
[T101566](http://developer.blender.org/T101566),
[T101639](http://developer.blender.org/T101639),
[T101936](http://developer.blender.org/T101936),
[T101923](http://developer.blender.org/T101923),
[T101914](http://developer.blender.org/T101914),
[T101570](http://developer.blender.org/T101570),
[T101573](http://developer.blender.org/T101573),
[T101581](http://developer.blender.org/T101581),
[T101605](http://developer.blender.org/T101605),
[T101555](http://developer.blender.org/T101555),
[T101854](http://developer.blender.org/T101854)

  - Closed as Duplicate: 8

[T101986](http://developer.blender.org/T101986),
[T101961](http://developer.blender.org/T101961),
[T101935](http://developer.blender.org/T101935),
[T101918](http://developer.blender.org/T101918),
[T101902](http://developer.blender.org/T101902),
[T101899](http://developer.blender.org/T101899),
[T101876](http://developer.blender.org/T101876),
[T101842](http://developer.blender.org/T101842)

  - Closed as Resolved: 2

[T99997](http://developer.blender.org/T99997),
[T101884](http://developer.blender.org/T101884)

  - Needs Information from User: 12

[T102010](http://developer.blender.org/T102010),
[T102006](http://developer.blender.org/T102006),
[T101967](http://developer.blender.org/T101967),
[T101963](http://developer.blender.org/T101963),
[T101950](http://developer.blender.org/T101950),
[T101944](http://developer.blender.org/T101944),
[T101940](http://developer.blender.org/T101940),
[T101938](http://developer.blender.org/T101938),
[T101934](http://developer.blender.org/T101934),
[T101921](http://developer.blender.org/T101921),
[T101911](http://developer.blender.org/T101911),
[T101870](http://developer.blender.org/T101870)

  - Needs Information from Developers: 6

[T102002](http://developer.blender.org/T102002),
[T101960](http://developer.blender.org/T101960),
[T101920](http://developer.blender.org/T101920),
[T101887](http://developer.blender.org/T101887),
[T101897](http://developer.blender.org/T101897),
[T101861](http://developer.blender.org/T101861)

  - Own Reported: 0

<!-- end list -->

  - Diff submitted: 0

<!-- end list -->

  - Commits: 1

[rBead3fc4a075d24e3481dfebdc63d54e7143a027c](http://developer.blender.org/rBead3fc4a075d24e3481dfebdc63d54e7143a027c)

## Week 74 (Oct 24 - 30)

This week: Triaging. Travelling on Tuesday-Wednesday. Was at Blender
conference  
Next Week: Triaging. Travelling back to Home on Sunday-Monday.  
**Total actions on tracker: 49**

  - Confirmed: 3

[T101988](http://developer.blender.org/T101988),
[T102014](http://developer.blender.org/T102014),
[T102020](http://developer.blender.org/T102020),

  - Closed as Archived: 1

[T102073](http://developer.blender.org/T102073)

  - Closed as Duplicate: 1

[T102104](http://developer.blender.org/T102104)

  - Closed as Resolved: 1

[T102010](http://developer.blender.org/T102010)

  - Needs Information from User: 2

[T102039](http://developer.blender.org/T102039),
[T101853](http://developer.blender.org/T101853)

  - Needs Information from Developers: 3

[T102070](http://developer.blender.org/T102070),
[T102012](http://developer.blender.org/T102012),
[T102030](http://developer.blender.org/T102030)

  - Own Reported: 1

[T102032](http://developer.blender.org/T102032)

  - Help in Review process: 3

[D16324](http://developer.blender.org/D16324),
[D16321](http://developer.blender.org/D16321),
[D16331](http://developer.blender.org/D16331)

  - Diff submitted: 0

## Week 75 (Oct 31 - 06)

This week: Triaging. Returned home on Monday from Blender conference.  
Next Week: Triaging.  
**Total actions on tracker: 141**

  - Confirmed: 12

[T102290](http://developer.blender.org/T102290),
[T102257](http://developer.blender.org/T102257),
[T102123](http://developer.blender.org/T102123),
[T102242](http://developer.blender.org/T102242),
[T102241](http://developer.blender.org/T102241),
[T102232](http://developer.blender.org/T102232),
[T87558](http://developer.blender.org/T87558),
[T102224](http://developer.blender.org/T102224),
[T102218](http://developer.blender.org/T102218),
[T102216](http://developer.blender.org/T102216),
[T102198](http://developer.blender.org/T102198),
[T102191](http://developer.blender.org/T102191)

  - Closed as Archived: 11

[T102249](http://developer.blender.org/T102249),
[T102260](http://developer.blender.org/T102260),
[T102262](http://developer.blender.org/T102262),
[T102231](http://developer.blender.org/T102231),
[T102227](http://developer.blender.org/T102227),
[T102017](http://developer.blender.org/T102017),
[T102213](http://developer.blender.org/T102213),
[T102150](http://developer.blender.org/T102150),
[T102135](http://developer.blender.org/T102135),
[T102170](http://developer.blender.org/T102170),
[T102175](http://developer.blender.org/T102175)

  - Closed as Duplicate: 8

[T102299](http://developer.blender.org/T102299),
[T102258](http://developer.blender.org/T102258),
[T102239](http://developer.blender.org/T102239),
[T96006](http://developer.blender.org/T96006),
[T102212](http://developer.blender.org/T102212),
[T102207](http://developer.blender.org/T102207),
[T67276](http://developer.blender.org/T67276),
[T102181](http://developer.blender.org/T102181)

  - Closed as Resolved: 2

[T102236](http://developer.blender.org/T102236),
[T87558](http://developer.blender.org/T87558)

  - Needs Information from User: 16

[T102303](http://developer.blender.org/T102303),
[T102298](http://developer.blender.org/T102298),
[T102245](http://developer.blender.org/T102245),
[T102244](http://developer.blender.org/T102244),
[T102226](http://developer.blender.org/T102226),
[T102220](http://developer.blender.org/T102220),
[T102211](http://developer.blender.org/T102211),
[T102200](http://developer.blender.org/T102200),
[T102197](http://developer.blender.org/T102197),
[T102169](http://developer.blender.org/T102169),
[T102164](http://developer.blender.org/T102164),
[T102162](http://developer.blender.org/T102162),
[T102149](http://developer.blender.org/T102149),
[T102142](http://developer.blender.org/T102142),
[T102137](http://developer.blender.org/T102137),
[T102114](http://developer.blender.org/T102114)

  - Needs Information from Developers: 3

[T102259](http://developer.blender.org/T102259),
[T102214](http://developer.blender.org/T102214),
[T102187](http://developer.blender.org/T102187)

  - Own Reported: 1

[T102242](http://developer.blender.org/T102242)

  - Diff submitted:

[D16395](http://developer.blender.org/D16395): Fix T102187: Add knife
tool in mesh panel

## Week 76 (Nov 07 - 13)

This week: Triaging.  
Next Week: Triaging. End-semester exams from 15th so I'll be inactive on
alternate days (Wednesday, Friday)  
**Total actions on tracker: 164**

  - Confirmed: 15

[T101963](http://developer.blender.org/T101963),
[T102197](http://developer.blender.org/T102197),
[T102414](http://developer.blender.org/T102414),
[T102393](http://developer.blender.org/T102393),
[T102396](http://developer.blender.org/T102396),
[T102382](http://developer.blender.org/T102382),
[T102379](http://developer.blender.org/T102379),
[T102340](http://developer.blender.org/T102340),
[T102337](http://developer.blender.org/T102337),
[T102276](http://developer.blender.org/T102276),
[T102308](http://developer.blender.org/T102308),
[T102311](http://developer.blender.org/T102311),
[T102312](http://developer.blender.org/T102312),
[T102318](http://developer.blender.org/T102318),
[T102317](http://developer.blender.org/T102317)

  - Closed as Archived: 18

[T102476](http://developer.blender.org/T102476),
[T102465](http://developer.blender.org/T102465),
[T102467](http://developer.blender.org/T102467),
[T101520](http://developer.blender.org/T101520),
[T101633](http://developer.blender.org/T101633),
[T101758](http://developer.blender.org/T101758),
[T101863](http://developer.blender.org/T101863),
[T102245](http://developer.blender.org/T102245),
[T102025](http://developer.blender.org/T102025),
[T102137](http://developer.blender.org/T102137),
[T102164](http://developer.blender.org/T102164),
[T102169](http://developer.blender.org/T102169),
[T102220](http://developer.blender.org/T102220),
[T102226](http://developer.blender.org/T102226),
[T102371](http://developer.blender.org/T102371),
[T102370](http://developer.blender.org/T102370),
[T101695](http://developer.blender.org/T101695),
[T102314](http://developer.blender.org/T102314)

  - Closed as Duplicate: 8

[T102464](http://developer.blender.org/T102464),
[T102211](http://developer.blender.org/T102211),
[T102361](http://developer.blender.org/T102361),
[T102368](http://developer.blender.org/T102368),
[T102366](http://developer.blender.org/T102366),
[T102343](http://developer.blender.org/T102343),
[T102333](http://developer.blender.org/T102333),
[T102319](http://developer.blender.org/T102319)

  - Closed as Resolved: 3

[T101575](http://developer.blender.org/T101575),
[T102335](http://developer.blender.org/T102335),
[T102187](http://developer.blender.org/T102187)

  - Needs Information from User: 8

[T102416](http://developer.blender.org/T102416),
[T102412](http://developer.blender.org/T102412),
[T102320](http://developer.blender.org/T102320),
[T102286](http://developer.blender.org/T102286),
[T102281](http://developer.blender.org/T102281),
[T102277](http://developer.blender.org/T102277),
[T102133](http://developer.blender.org/T102133),
[T101359](http://developer.blender.org/T101359)

  - Needs Information from Developers: 3

[T102162](http://developer.blender.org/T102162),
[T102390](http://developer.blender.org/T102390),
[T102375](http://developer.blender.org/T102375)

  - Own Reported: 1

[T102419](http://developer.blender.org/T102419)

  - Diff submitted: 0

<!-- end list -->

  - Commits: 1

[rBb927cc9ba6c7567066f26fd82c95b0059a8cff75](http://developer.blender.org/rBb927cc9ba6c7567066f26fd82c95b0059a8cff75)

  - Manual Contribution: 1

[rBM9686](http://developer.blender.org/rBM9686)

## Week 77 (Nov 14 - 20)

This week: Triaging. Wasn't available on Wednesday and Friday due to
end-semester exams  
Did some work during weekend. Also checked few orphaned patches (closed.
assign devs for the review)  
No code contributions really. Will start again after exams  
Next Week: Triaging.  
**Total actions on tracker: 130**

  - Confirmed: 15

[T102625](http://developer.blender.org/T102625),
[T102623](http://developer.blender.org/T102623),
[T102620](http://developer.blender.org/T102620),
[T102610](http://developer.blender.org/T102610),
[T102618](http://developer.blender.org/T102618),
[T102582](http://developer.blender.org/T102582),
[T102584](http://developer.blender.org/T102584),
[T102548](http://developer.blender.org/T102548),
[T102522](http://developer.blender.org/T102522),
[T102507](http://developer.blender.org/T102507),
[T102503](http://developer.blender.org/T102503),
[T102436](http://developer.blender.org/T102436),
[T102481](http://developer.blender.org/T102481),
[T102482](http://developer.blender.org/T102482),
[T102488](http://developer.blender.org/T102488)

  - Closed as Archived: 1

[T102513](http://developer.blender.org/T102513)

  - Closed as Duplicate: 4

[T102573](http://developer.blender.org/T102573),
[T102525](http://developer.blender.org/T102525),
[T102478](http://developer.blender.org/T102478),
[T102416](http://developer.blender.org/T102416)

  - Closed as Resolved: 0

<!-- end list -->

  - Needs Information from User: 18

[T102630](http://developer.blender.org/T102630),
[T102621](http://developer.blender.org/T102621),
[T102614](http://developer.blender.org/T102614),
[T102592](http://developer.blender.org/T102592),
[T102589](http://developer.blender.org/T102589),
[T102565](http://developer.blender.org/T102565),
[T102528](http://developer.blender.org/T102528),
[T102517](http://developer.blender.org/T102517),
[T102509](http://developer.blender.org/T102509),
[T102504](http://developer.blender.org/T102504),
[T102496](http://developer.blender.org/T102496),
[T102491](http://developer.blender.org/T102491),
[T102489](http://developer.blender.org/T102489),
[T102473](http://developer.blender.org/T102473),
[T102471](http://developer.blender.org/T102471),
[T102469](http://developer.blender.org/T102469),
[T102466](http://developer.blender.org/T102466),
[T102448](http://developer.blender.org/T102448)

  - Needs Information from Developers: 3

[T102615](http://developer.blender.org/T102615),
[T102449](http://developer.blender.org/T102449),
[T102438](http://developer.blender.org/T102438)

  - Help in Patch Review: 11

[D16292](http://developer.blender.org/D16292),
[D16563](http://developer.blender.org/D16563),
[D16352](http://developer.blender.org/D16352),
[D16447](http://developer.blender.org/D16447),
[D16454](http://developer.blender.org/D16454),
[D16468](http://developer.blender.org/D16468),
[D16506](http://developer.blender.org/D16506),
[D16463](http://developer.blender.org/D16463),
[D16475](http://developer.blender.org/D16475),
[D16476](http://developer.blender.org/D16476),
[D16477](http://developer.blender.org/D16477)

  - Diff submitted: 0

## Week 78 (Nov 21 - 27)

This week: Triaging. Unavailable on Wednesday-Thursday(exams). Did some
work over the weekend  
Triaged old reports with no activity/reply pending. Checked very few
orphaned patches  
Next Week: Triaging.  
**Total actions on tracker: 141**

  - Confirmed: 15

[T102797](http://developer.blender.org/T102797),
[T102788](http://developer.blender.org/T102788),
[T102777](http://developer.blender.org/T102777),
[T102773](http://developer.blender.org/T102773),
[T102775](http://developer.blender.org/T102775),
[T102772](http://developer.blender.org/T102772),
[T102783](http://developer.blender.org/T102783),
[T102747](http://developer.blender.org/T102747),
[T102745](http://developer.blender.org/T102745),
[T102752](http://developer.blender.org/T102752),
[T102753](http://developer.blender.org/T102753),
[T102466](http://developer.blender.org/T102466),
[T102582](http://developer.blender.org/T102582),
[T102503](http://developer.blender.org/T102503),
[T102638](http://developer.blender.org/T102638)

  - Closed as Archived: 18

[T101544](http://developer.blender.org/T101544),
[T101587](http://developer.blender.org/T101587),
[T101662](http://developer.blender.org/T101662),
[T101911](http://developer.blender.org/T101911),
[T102039](http://developer.blender.org/T102039),
[T102142](http://developer.blender.org/T102142),
[T102200](http://developer.blender.org/T102200),
[T102412](http://developer.blender.org/T102412),
[T102469](http://developer.blender.org/T102469),
[T102473](http://developer.blender.org/T102473),
[T102489](http://developer.blender.org/T102489),
[T102496](http://developer.blender.org/T102496),
[T102504](http://developer.blender.org/T102504),
[T102517](http://developer.blender.org/T102517),
[T102528](http://developer.blender.org/T102528),
[T102565](http://developer.blender.org/T102565),
[T102642](http://developer.blender.org/T102642),
[T102614](http://developer.blender.org/T102614)

  - Closed as Duplicate: 3

[T102616](http://developer.blender.org/T102616),
[T102780](http://developer.blender.org/T102780),
[T102754](http://developer.blender.org/T102754)

  - Closed as Resolved: 2

[T102149](http://developer.blender.org/T102149),
[T102625](http://developer.blender.org/T102625)

  - Needs Information from User: 7

[T102789](http://developer.blender.org/T102789),
[T102774](http://developer.blender.org/T102774),
[T102694](http://developer.blender.org/T102694),
[T102677](http://developer.blender.org/T102677),
[T102641](http://developer.blender.org/T102641),
[T102640](http://developer.blender.org/T102640),
[T102636](http://developer.blender.org/T102636)

  - Needs Information from Developers: 1

[T102657](http://developer.blender.org/T102657)

  - Own Reported: 3

[T102804](http://developer.blender.org/T102804),
[T102650](http://developer.blender.org/T102650),
[T102649](http://developer.blender.org/T102649)

  - Diff submitted: 1

[D16583](http://developer.blender.org/D16583): Fix T102436: Crash using
any Filter when automasking options are activated

  - Review help: 5

[D16622](http://developer.blender.org/D16622),
[D16617](http://developer.blender.org/D16617),
[D16493](http://developer.blender.org/D16493),
[D16341](http://developer.blender.org/D16341),
[D16556](http://developer.blender.org/D16556)

## Week 79 (Nov 28 - 04)

This week: Triaging. Submitted one fix related to outliner tree
expansion.  
Less active on Friday due to practical exam. Triaged old reports with no
activity/reply pending.  
Next Week: Triaging.  
**Total actions on tracker: 146**

  - Confirmed: 11

[T102906](http://developer.blender.org/T102906),
[T102914](http://developer.blender.org/T102914),
[T102902](http://developer.blender.org/T102902),
[T102894](http://developer.blender.org/T102894),
[T102863](http://developer.blender.org/T102863),
[T102799](http://developer.blender.org/T102799),
[T102889](http://developer.blender.org/T102889),
[T102872](http://developer.blender.org/T102872),
[T102875](http://developer.blender.org/T102875),
[T102786](http://developer.blender.org/T102786),
[T102807](http://developer.blender.org/T102807)

  - Closed as Archived: 15

[T102640](http://developer.blender.org/T102640),
[T102891](http://developer.blender.org/T102891),
[T102589](http://developer.blender.org/T102589),
[T102592](http://developer.blender.org/T102592),
[T102617](http://developer.blender.org/T102617),
[T102621](http://developer.blender.org/T102621),
[T102630](http://developer.blender.org/T102630),
[T102637](http://developer.blender.org/T102637),
[T102636](http://developer.blender.org/T102636),
[T102641](http://developer.blender.org/T102641),
[T102855](http://developer.blender.org/T102855),
[T102841](http://developer.blender.org/T102841),
[T102815](http://developer.blender.org/T102815),
[T102810](http://developer.blender.org/T102810),
[T102814](http://developer.blender.org/T102814)

  - Closed as Duplicate: 8

[T102925](http://developer.blender.org/T102925),
[T102880](http://developer.blender.org/T102880),
[T102818](http://developer.blender.org/T102818),
[T102870](http://developer.blender.org/T102870),
[T102839](http://developer.blender.org/T102839),
[T102826](http://developer.blender.org/T102826),
[T102821](http://developer.blender.org/T102821),
[T102823](http://developer.blender.org/T102823)

  - Closed as Resolved: 2

[T102875](http://developer.blender.org/T102875),
[T101985](http://developer.blender.org/T101985)

  - Needs Information from User: 3

[T102890](http://developer.blender.org/T102890),
[T102884](http://developer.blender.org/T102884),
[T102830](http://developer.blender.org/T102830)

  - Needs Information from Developers: 3

[T102778](http://developer.blender.org/T102778),
[T102870](http://developer.blender.org/T102870),
[T102802](http://developer.blender.org/T102802)

  - Own Reported: 1

[T102893](http://developer.blender.org/T102893)

  - Diff submitted: 1

[D16661](http://developer.blender.org/D16661): Fix T102317: Any action
triggers expansion of all ViewLayers.

  - Commits: 1

[rBA12f6cb3bf27d15d6daa9fd94ca123b4a68a0edb3](http://developer.blender.org/rBA12f6cb3bf27d15d6daa9fd94ca123b4a68a0edb3)

## Week 80 (Dec 05 - 11)

This week: Triaging. Revisited few confirmed [bug
reports](https://developer.blender.org/maniphest/query/f98hBJSwZGMv/#R)
from User Interface module  
Did general bug fixing.  
Next Week: Triaging.  
**Total actions on tracker: 170**

  - Confirmed: 11

[T103042](http://developer.blender.org/T103042),
[T103037](http://developer.blender.org/T103037),
[T103027](http://developer.blender.org/T103027),
[T102870](http://developer.blender.org/T102870),
[T103013](http://developer.blender.org/T103013),
[T103001](http://developer.blender.org/T103001),
[T102991](http://developer.blender.org/T102991),
[T102993](http://developer.blender.org/T102993),
[T102930](http://developer.blender.org/T102930),
[T102964](http://developer.blender.org/T102964),
[T102922](http://developer.blender.org/T102922)

  - Closed as Archived: 4

[T103128](http://developer.blender.org/T103128),
[T103012](http://developer.blender.org/T103012),
[T103002](http://developer.blender.org/T103002),
[T91574](http://developer.blender.org/T91574)

  - Closed as Duplicate: 12

[T103096](http://developer.blender.org/T103096),
[T88882](http://developer.blender.org/T88882),
[T89195](http://developer.blender.org/T89195),
[T103030](http://developer.blender.org/T103030),
[T103056](http://developer.blender.org/T103056),
[T103017](http://developer.blender.org/T103017),
[T92680](http://developer.blender.org/T92680),
[T92779](http://developer.blender.org/T92779),
[T92894](http://developer.blender.org/T92894),
[T102788](http://developer.blender.org/T102788),
[T102864](http://developer.blender.org/T102864),
[T102947](http://developer.blender.org/T102947)

  - Closed as Resolved: 14

[T103001](http://developer.blender.org/T103001),
[T89199](http://developer.blender.org/T89199),
[T89412](http://developer.blender.org/T89412),
[T89770](http://developer.blender.org/T89770),
[T91263](http://developer.blender.org/T91263),
[T89782](http://developer.blender.org/T89782),
[T90673](http://developer.blender.org/T90673),
[T90914](http://developer.blender.org/T90914),
[T90895](http://developer.blender.org/T90895),
[T91023](http://developer.blender.org/T91023),
[T91040](http://developer.blender.org/T91040),
[T93683](http://developer.blender.org/T93683),
[T87974](http://developer.blender.org/T87974),
[T102917](http://developer.blender.org/T102917)

  - Needs Information from User: 6

[T103033](http://developer.blender.org/T103033),
[T103020](http://developer.blender.org/T103020),
[T102950](http://developer.blender.org/T102950),
[T102417](http://developer.blender.org/T102417),
[T89123](http://developer.blender.org/T89123),
[T88924](http://developer.blender.org/T88924)

  - Needs Information from Developers: 5

[T90338](http://developer.blender.org/T90338),
[T103031](http://developer.blender.org/T103031),
[T102998](http://developer.blender.org/T102998),
[T102996](http://developer.blender.org/T102996),
[T102930](http://developer.blender.org/T102930)

  - Own Reported: 0

<!-- end list -->

  - Diff submitted: 2

[D16739](http://developer.blender.org/D16739): Fix T90159: Inconsistent
display of active filters for file import/export file dialogs  
[D16719](http://developer.blender.org/D16719): Fix T103001: Width of
some compositor nodes is incorrect

  - Commits: 1

[rB7c5b7713f12ceb3d0704925d9e86adac839da9a9](http://developer.blender.org/rB7c5b7713f12ceb3d0704925d9e86adac839da9a9)

## Week 81 (Dec 12 - 18)

This week: Triaging. Did general bug fixing.  
Next Week: Triaging. I will be inactive on 20-22. Will do work on
weekend  
**Total actions on tracker: 193**

  - Confirmed: 20

[T103261](http://developer.blender.org/T103261),
[T103256](http://developer.blender.org/T103256),
[T103243](http://developer.blender.org/T103243),
[T103242](http://developer.blender.org/T103242),
[T103253](http://developer.blender.org/T103253),
[T103215](http://developer.blender.org/T103215),
[T103232](http://developer.blender.org/T103232),
[T103112](http://developer.blender.org/T103112),
[T103212](http://developer.blender.org/T103212),
[T103198](http://developer.blender.org/T103198),
[T103176](http://developer.blender.org/T103176),
[T103194](http://developer.blender.org/T103194),
[T103199](http://developer.blender.org/T103199),
[T103203](http://developer.blender.org/T103203),
[T103179](http://developer.blender.org/T103179),
[T103156](http://developer.blender.org/T103156),
[T103175](http://developer.blender.org/T103175),
[T103165](http://developer.blender.org/T103165),
[T103117](http://developer.blender.org/T103117),
[T103122](http://developer.blender.org/T103122)

  - Closed as Archived: 11

[T103300](http://developer.blender.org/T103300),
[T103247](http://developer.blender.org/T103247),
[T103246](http://developer.blender.org/T103246),
[T103193](http://developer.blender.org/T103193),
[T103209](http://developer.blender.org/T103209),
[T103172](http://developer.blender.org/T103172),
[T103202](http://developer.blender.org/T103202),
[T103146](http://developer.blender.org/T103146),
[T103169](http://developer.blender.org/T103169),
[T103149](http://developer.blender.org/T103149),
[T103142](http://developer.blender.org/T103142)

  - Closed as Duplicate: 17

[T103287](http://developer.blender.org/T103287),
[T103286](http://developer.blender.org/T103286),
[T103264](http://developer.blender.org/T103264),
[T103233](http://developer.blender.org/T103233),
[T103249](http://developer.blender.org/T103249),
[T103206](http://developer.blender.org/T103206),
[T103201](http://developer.blender.org/T103201),
[T103155](http://developer.blender.org/T103155),
[T103157](http://developer.blender.org/T103157),
[T103168](http://developer.blender.org/T103168),
[T103100](http://developer.blender.org/T103100),
[T103152](http://developer.blender.org/T103152),
[T103121](http://developer.blender.org/T103121),
[T103125](http://developer.blender.org/T103125),
[T103134](http://developer.blender.org/T103134),
[T103137](http://developer.blender.org/T103137),
[T103123](http://developer.blender.org/T103123)

  - Closed as Resolved: 0

<!-- end list -->

  - Needs Information from User: 14

[T103262](http://developer.blender.org/T103262),
[T103251](http://developer.blender.org/T103251),
[T103216](http://developer.blender.org/T103216),
[T103184](http://developer.blender.org/T103184),
[T103174](http://developer.blender.org/T103174),
[T103173](http://developer.blender.org/T103173),
[T103171](http://developer.blender.org/T103171),
[T103162](http://developer.blender.org/T103162),
[T103154](http://developer.blender.org/T103154),
[T103145](http://developer.blender.org/T103145),
[T103142](http://developer.blender.org/T103142),
[T103139](http://developer.blender.org/T103139),
[T103129](http://developer.blender.org/T103129),
[T103098](http://developer.blender.org/T103098)

  - Needs Information from Developers: 1

[T103221](http://developer.blender.org/T103221)

  - Diff submitted: 2

[D16804](http://developer.blender.org/D16804): Fix T89479: Unable to
select hierarchies of multiple objects from outliner  
[D16773](http://developer.blender.org/D16773): Fix T90893: Restoring a
key in the keymap editor doesn't work if the key is only disabled.

## Week 82 (Dec 19 - 25)

This week: Triaging. Did general bug fixing.  
I was Inactive during 20-22 so worked over weekend (consider 1
holiday)  
Next Week: Triaging.  
**Total actions on tracker: 134**

  - Confirmed: 9

[T103463](http://developer.blender.org/T103463),
[T103455](http://developer.blender.org/T103455),
[T103454](http://developer.blender.org/T103454),
[T103446](http://developer.blender.org/T103446),
[T103427](http://developer.blender.org/T103427),
[T89123](http://developer.blender.org/T89123),
[T103337](http://developer.blender.org/T103337),
[T103313](http://developer.blender.org/T103313),
[T103307](http://developer.blender.org/T103307)

  - Closed as Archived: 12

[T103459](http://developer.blender.org/T103459),
[T103462](http://developer.blender.org/T103462),
[T103053](http://developer.blender.org/T103053),
[T103451](http://developer.blender.org/T103451),
[T103020](http://developer.blender.org/T103020),
[T103129](http://developer.blender.org/T103129),
[T103154](http://developer.blender.org/T103154),
[T103173](http://developer.blender.org/T103173),
[T103184](http://developer.blender.org/T103184),
[T103333](http://developer.blender.org/T103333),
[T103277](http://developer.blender.org/T103277),
[T103216](http://developer.blender.org/T103216)

  - Closed as Duplicate: 10

[T103409](http://developer.blender.org/T103409),
[T103446](http://developer.blender.org/T103446),
[T103439](http://developer.blender.org/T103439),
[T89773](http://developer.blender.org/T89773),
[T103098](http://developer.blender.org/T103098),
[T103340](http://developer.blender.org/T103340),
[T103330](http://developer.blender.org/T103330),
[T103279](http://developer.blender.org/T103279),
[T103297](http://developer.blender.org/T103297),
[T103162](http://developer.blender.org/T103162)

  - Closed as Resolved: 1

[T88161](http://developer.blender.org/T88161)

  - Needs Information from User: 5

[T103461](http://developer.blender.org/T103461),
[T103428](http://developer.blender.org/T103428),
[T103418](http://developer.blender.org/T103418),
[T103310](http://developer.blender.org/T103310),
[T88125](http://developer.blender.org/T88125)

  - Needs Information from Developers: 2

[T103303](http://developer.blender.org/T103303),
[T103283](http://developer.blender.org/T103283)

  - Own Reported: 1

[T103332](http://developer.blender.org/T103332)

  - Diff submitted: 1

[D16865](http://developer.blender.org/D16865): Fix T103455: Modifier
tools add-on don't support GPencil modifiers

## Week 83 (Dec 26 - 01)

This week: Triaging. Did general bug fixing.  
Helped in the review process of
[D15351](http://developer.blender.org/D15351)  
Next Week: Triaging.  
**Total actions on tracker: 168**

  - Confirmed: 9

[T103526](http://developer.blender.org/T103526),
[T103507](http://developer.blender.org/T103507),
[T103496](http://developer.blender.org/T103496),
[T103477](http://developer.blender.org/T103477),
[T103471](http://developer.blender.org/T103471),
[T103474](http://developer.blender.org/T103474),
[T88125](http://developer.blender.org/T88125),
[T103456](http://developer.blender.org/T103456),
[T103469](http://developer.blender.org/T103469)

  - Closed as Archived: 8

[T103527](http://developer.blender.org/T103527),
[T103525](http://developer.blender.org/T103525),
[T103501](http://developer.blender.org/T103501),
[T103518](http://developer.blender.org/T103518),
[T103500](http://developer.blender.org/T103500),
[T103505](http://developer.blender.org/T103505),
[T103484](http://developer.blender.org/T103484),
[T103468](http://developer.blender.org/T103468)

  - Closed as Duplicate: 12

[T103552](http://developer.blender.org/T103552),
[T103539](http://developer.blender.org/T103539),
[T103548](http://developer.blender.org/T103548),
[T103541](http://developer.blender.org/T103541),
[T103504](http://developer.blender.org/T103504),
[T103483](http://developer.blender.org/T103483),
[T100728](http://developer.blender.org/T100728),
[T103478](http://developer.blender.org/T103478),
[T103481](http://developer.blender.org/T103481),
[T103476](http://developer.blender.org/T103476),
[T103145](http://developer.blender.org/T103145),
[T103465](http://developer.blender.org/T103465)

  - Closed as Resolved: 4

[T103310](http://developer.blender.org/T103310),
[T103544](http://developer.blender.org/T103544),
[T103445](http://developer.blender.org/T103445),
[T103455](http://developer.blender.org/T103455)

  - Needs Information from User: 9

[T103554](http://developer.blender.org/T103554),
[T103553](http://developer.blender.org/T103553),
[T103542](http://developer.blender.org/T103542),
[T103528](http://developer.blender.org/T103528),
[T103506](http://developer.blender.org/T103506),
[T103503](http://developer.blender.org/T103503),
[T103472](http://developer.blender.org/T103472),
[T103466](http://developer.blender.org/T103466),
[T103460](http://developer.blender.org/T103460)

  - Needs Information from Developers: 6

[T103546](http://developer.blender.org/T103546),
[T103502](http://developer.blender.org/T103502),
[T103499](http://developer.blender.org/T103499),
[T103479](http://developer.blender.org/T103479),
[T103473](http://developer.blender.org/T103473),
[T103464](http://developer.blender.org/T103464)

  - Diff submitted: 3

[D16878](http://developer.blender.org/D16878): Fix T88617: Wrong
annotation cursor in Preview of sequencer editor  
[D16890](http://developer.blender.org/D16890): Fix T102993: Incorrect
icon displaying of Weighted Normal modifier in the outliner  
[D16889](http://developer.blender.org/D16889): Fix T103563: Add display
mode UI in asset browser

  - Commits: 2

[rBAf131c922f9f35081376a32d19ed963603b8e2be6](http://developer.blender.org/rBAf131c922f9f35081376a32d19ed963603b8e2be6)  
[rBb6a11ae7d59acd17c56e2d46eaab928bdce3d984](http://developer.blender.org/rBb6a11ae7d59acd17c56e2d46eaab928bdce3d984)

  - Review: 1

[D15351](http://developer.blender.org/D15351): Outliner: Double click on
icon to select contents/hierarchy

# 2023

## Week 84 (Jan 02 - 08)

**Total actions on tracker: 176**

This week: Triaging. Did general bug fixing.  
Visited old confirmed bug reports from User Interface module  
Next Week: Triaging.  

  - Confirmed: 14

[T103671](http://developer.blender.org/T103671),
[T103657](http://developer.blender.org/T103657),
[T103676](http://developer.blender.org/T103676),
[T103656](http://developer.blender.org/T103656),
[T103635](http://developer.blender.org/T103635),
[T103667](http://developer.blender.org/T103667),
[T103646](http://developer.blender.org/T103646),
[T103624](http://developer.blender.org/T103624),
[T103559](http://developer.blender.org/T103559),
[T103601](http://developer.blender.org/T103601),
[T103592](http://developer.blender.org/T103592),
[T103566](http://developer.blender.org/T103566),
[T103587](http://developer.blender.org/T103587),
[T103564](http://developer.blender.org/T103564)

  - Closed as Archived: 9

[T103654](http://developer.blender.org/T103654),
[T103659](http://developer.blender.org/T103659),
[T85033](http://developer.blender.org/T85033),
[T103639](http://developer.blender.org/T103639),
[T103612](http://developer.blender.org/T103612),
[T103613](http://developer.blender.org/T103613),
[T103593](http://developer.blender.org/T103593),
[T103580](http://developer.blender.org/T103580),
[T103588](http://developer.blender.org/T103588)

  - Closed as Duplicate: 10

[T103702](http://developer.blender.org/T103702),
[T103701](http://developer.blender.org/T103701),
[T83651](http://developer.blender.org/T83651),
[T103677](http://developer.blender.org/T103677),
[T103655](http://developer.blender.org/T103655),
[T103627](http://developer.blender.org/T103627),
[T103647](http://developer.blender.org/T103647),
[T103611](http://developer.blender.org/T103611),
[T103565](http://developer.blender.org/T103565),
[T103583](http://developer.blender.org/T103583)

  - Closed as Resolved: 4

[T85428](http://developer.blender.org/T85428),
[T86022](http://developer.blender.org/T86022),
[T103597](http://developer.blender.org/T103597),
[T102993](http://developer.blender.org/T102993)

  - Needs Information from User: 10

[T103660](http://developer.blender.org/T103660),
[T103652](http://developer.blender.org/T103652),
[T103648](http://developer.blender.org/T103648),
[T103643](http://developer.blender.org/T103643),
[T103640](http://developer.blender.org/T103640),
[T103622](http://developer.blender.org/T103622),
[T103584](http://developer.blender.org/T103584),
[T103353](http://developer.blender.org/T103353),
[T102950](http://developer.blender.org/T102950),
[T87691](http://developer.blender.org/T87691)

  - Needs Information from Developers: 3

[T103662](http://developer.blender.org/T103662),
[T103649](http://developer.blender.org/T103649),
[T103590](http://developer.blender.org/T103590)

  - Own Reported: 1

[T103603](http://developer.blender.org/T103603)

  - Diff submitted: 3

[D16939](http://developer.blender.org/D16939): Fix T103679: Add missing
operators in object context menu for point cloud and curves  
[D16661](http://developer.blender.org/D16661): Fix T102317: Any action
triggers expansion of all ViewLayers  
[D16878](http://developer.blender.org/D16878): Fix T88617: Wrong
annotation cursor in Preview of sequencer editor

  - Commits: 1

[rBe438e8e04ee83c194c4be9dea8560c53b3aee429](http://developer.blender.org/rBe438e8e04ee83c194c4be9dea8560c53b3aee429)

## Week 85 (Jan 09 - 15)

This week: Triaging. Did general bug fixing and code review.  
Revisited very few reports from UI module  
Next Week: Triaging.  
**Total actions on tracker: 200**

  - Confirmed: 20

[T103820](http://developer.blender.org/T103820),
[T103818](http://developer.blender.org/T103818),
[T103852](http://developer.blender.org/T103852),
[T103856](http://developer.blender.org/T103856),
[T103811](http://developer.blender.org/T103811),
[T103837](http://developer.blender.org/T103837),
[T103785](http://developer.blender.org/T103785),
[T103795](http://developer.blender.org/T103795),
[T88924](http://developer.blender.org/T88924),
[T103722](http://developer.blender.org/T103722),
[T103793](http://developer.blender.org/T103793),
[T103783](http://developer.blender.org/T103783),
[T103753](http://developer.blender.org/T103753),
[T103749](http://developer.blender.org/T103749),
[T103747](http://developer.blender.org/T103747),
[T103412](http://developer.blender.org/T103412),
[T103719](http://developer.blender.org/T103719),
[T103707](http://developer.blender.org/T103707),
[T103710](http://developer.blender.org/T103710),
[T103700](http://developer.blender.org/T103700)

  - Closed as Archived: 16

[T103792](http://developer.blender.org/T103792),
[T103748](http://developer.blender.org/T103748),
[T102865](http://developer.blender.org/T102865),
[T103159](http://developer.blender.org/T103159),
[T103251](http://developer.blender.org/T103251),
[T103262](http://developer.blender.org/T103262),
[T103428](http://developer.blender.org/T103428),
[T103460](http://developer.blender.org/T103460),
[T103528](http://developer.blender.org/T103528),
[T103553](http://developer.blender.org/T103553),
[T103622](http://developer.blender.org/T103622),
[T103633](http://developer.blender.org/T103633),
[T103643](http://developer.blender.org/T103643),
[T103648](http://developer.blender.org/T103648),
[T103794](http://developer.blender.org/T103794),
[T103718](http://developer.blender.org/T103718)

  - Closed as Duplicate: 11

[T103817](http://developer.blender.org/T103817),
[T103830](http://developer.blender.org/T103830),
[T103821](http://developer.blender.org/T103821),
[T100395](http://developer.blender.org/T100395),
[T103793](http://developer.blender.org/T103793),
[T103753](http://developer.blender.org/T103753),
[T103769](http://developer.blender.org/T103769),
[T103712](http://developer.blender.org/T103712),
[T103716](http://developer.blender.org/T103716),
[T103745](http://developer.blender.org/T103745),
[T103741](http://developer.blender.org/T103741)

  - Closed as Resolved: 2

[T87691](http://developer.blender.org/T87691),
[T103679](http://developer.blender.org/T103679)

  - Needs Information from User: 12

[T103854](http://developer.blender.org/T103854),
[T103853](http://developer.blender.org/T103853),
[T103760](http://developer.blender.org/T103760),
[T103755](http://developer.blender.org/T103755),
[T103750](http://developer.blender.org/T103750),
[T103746](http://developer.blender.org/T103746),
[T103743](http://developer.blender.org/T103743),
[T103740](http://developer.blender.org/T103740),
[T103732](http://developer.blender.org/T103732),
[T103713](http://developer.blender.org/T103713),
[T103711](http://developer.blender.org/T103711),
[T103709](http://developer.blender.org/T103709)

  - Needs Information from Developers: 2

[T103791](http://developer.blender.org/T103791),
[T103723](http://developer.blender.org/T103723)

  - Own Reported: 1

[T103773](http://developer.blender.org/T103773)

  - Diff submitted: 2

[D17001](http://developer.blender.org/D17001): Fix: Build failure due to
type conversion  
[D16962](http://developer.blender.org/D16962): Fix T103753: Mask Slice &
Extract operators don't work with parenting

  - Commits: 1

[rB6f38ce5a406ec2771090e6536d96219b2c0f3743](http://developer.blender.org/rB6f38ce5a406ec2771090e6536d96219b2c0f3743)

  - Review: 2

[D16650](http://developer.blender.org/D16650),
[D16947](http://developer.blender.org/D16947)

## Week 86 (Jan 16 - 22)

This week: Triaging. Did general bug fixing.  
Next Week: Triaging.  
**Total actions on tracker: 160**

  - Confirmed: 16

[T104002](http://developer.blender.org/T104002),
[T104026](http://developer.blender.org/T104026),
[T104003](http://developer.blender.org/T104003),
[T103971](http://developer.blender.org/T103971),
[T103988](http://developer.blender.org/T103988),
[T103948](http://developer.blender.org/T103948),
[T103957](http://developer.blender.org/T103957),
[T103956](http://developer.blender.org/T103956),
[T103953](http://developer.blender.org/T103953),
[T103952](http://developer.blender.org/T103952),
[T103923](http://developer.blender.org/T103923),
[T103911](http://developer.blender.org/T103911),
[T103868](http://developer.blender.org/T103868),
[T103867](http://developer.blender.org/T103867),
[T103881](http://developer.blender.org/T103881),
[T103898](http://developer.blender.org/T103898)

  - Closed as Archived: 7

[T103859](http://developer.blender.org/T103859),
[T103987](http://developer.blender.org/T103987),
[T103877](http://developer.blender.org/T103877),
[T103342](http://developer.blender.org/T103342),
[T103917](http://developer.blender.org/T103917),
[T103882](http://developer.blender.org/T103882),
[T103900](http://developer.blender.org/T103900)

  - Closed as Duplicate: 10

[T104043](http://developer.blender.org/T104043),
[T104027](http://developer.blender.org/T104027),
[T103986](http://developer.blender.org/T103986),
[T103943](http://developer.blender.org/T103943),
[T103932](http://developer.blender.org/T103932),
[T103925](http://developer.blender.org/T103925),
[T103895](http://developer.blender.org/T103895),
[T103892](http://developer.blender.org/T103892),
[T103891](http://developer.blender.org/T103891),
[T103890](http://developer.blender.org/T103890)

  - Closed as Resolved: 3

[T103881](http://developer.blender.org/T103881),
[T103930](http://developer.blender.org/T103930),
[T101675](http://developer.blender.org/T101675)

  - Needs Information from User: 20

[T104042](http://developer.blender.org/T104042),
[T104020](http://developer.blender.org/T104020),
[T104018](http://developer.blender.org/T104018),
[T104017](http://developer.blender.org/T104017),
[T104014](http://developer.blender.org/T104014),
[T103996](http://developer.blender.org/T103996),
[T103990](http://developer.blender.org/T103990),
[T103989](http://developer.blender.org/T103989),
[T103984](http://developer.blender.org/T103984),
[T103951](http://developer.blender.org/T103951),
[T103926](http://developer.blender.org/T103926),
[T103893](http://developer.blender.org/T103893),
[T103869](http://developer.blender.org/T103869),
[T103862](http://developer.blender.org/T103862),
[T103847](http://developer.blender.org/T103847),
[T103843](http://developer.blender.org/T103843),
[T103819](http://developer.blender.org/T103819),
[T103681](http://developer.blender.org/T103681),
[T103652](http://developer.blender.org/T103652),
[T103164](http://developer.blender.org/T103164)

  - Own Reported: 0

<!-- end list -->

  - Needs Information from Developers: 2

[T103977](http://developer.blender.org/T103977),
[T103870](http://developer.blender.org/T103870)

  - Diff submitted: 2

[D17079](http://developer.blender.org/D17079): WIP: Allow select range
in animation editor  
[D17017](http://developer.blender.org/D17017): Fix T103881: Unlink
operation crash in Blender File view

  - Commits: 1

[rBa3c38667f0f8195ed560b32db5630ccaceb42d54](http://developer.blender.org/rBa3c38667f0f8195ed560b32db5630ccaceb42d54)

## Week 87 (Jan 23 - 29)

This week: Triaging. Did general bug fixing. Closed old reports without
response.  
Contributed to [Animation Editor
papercuts](https://developer.blender.org/T103855). Helped in technical
documentation of Outliner.  
Next Week: Triaging.  
**Total actions on tracker: 169**

  - Confirmed: 18

[T104157](http://developer.blender.org/T104157),
[T104154](http://developer.blender.org/T104154),
[T103984](http://developer.blender.org/T103984),
[T104145](http://developer.blender.org/T104145),
[T104143](http://developer.blender.org/T104143),
[T104138](http://developer.blender.org/T104138),
[T103485](http://developer.blender.org/T103485),
[T104128](http://developer.blender.org/T104128),
[T104124](http://developer.blender.org/T104124),
[T104121](http://developer.blender.org/T104121),
[T104091](http://developer.blender.org/T104091),
[T104090](http://developer.blender.org/T104090),
[T104007](http://developer.blender.org/T104007),
[T104040](http://developer.blender.org/T104040),
[T104068](http://developer.blender.org/T104068),
[T104033](http://developer.blender.org/T104033),
[T104074](http://developer.blender.org/T104074),
[T104076](http://developer.blender.org/T104076)

  - Closed as Archived: 24

[T103996](http://developer.blender.org/T103996),
[T104155](http://developer.blender.org/T104155),
[T103746](http://developer.blender.org/T103746),
[T103847](http://developer.blender.org/T103847),
[T103732](http://developer.blender.org/T103732),
[T102890](http://developer.blender.org/T102890),
[T103252](http://developer.blender.org/T103252),
[T103353](http://developer.blender.org/T103353),
[T103472](http://developer.blender.org/T103472),
[T103475](http://developer.blender.org/T103475),
[T103709](http://developer.blender.org/T103709),
[T103750](http://developer.blender.org/T103750),
[T103760](http://developer.blender.org/T103760),
[T103853](http://developer.blender.org/T103853),
[T103893](http://developer.blender.org/T103893),
[T103951](http://developer.blender.org/T103951),
[T104147](http://developer.blender.org/T104147),
[T104139](http://developer.blender.org/T104139),
[T104100](http://developer.blender.org/T104100),
[T104056](http://developer.blender.org/T104056),
[T104094](http://developer.blender.org/T104094),
[T104046](http://developer.blender.org/T104046),
[T104079](http://developer.blender.org/T104079),
[T104078](http://developer.blender.org/T104078)

  - Closed as Duplicate: 11

[T104152](http://developer.blender.org/T104152),
[T103770](http://developer.blender.org/T103770),
[T103735](http://developer.blender.org/T103735),
[T104148](http://developer.blender.org/T104148),
[T104146](http://developer.blender.org/T104146),
[T104122](http://developer.blender.org/T104122),
[T104130](http://developer.blender.org/T104130),
[T99362](http://developer.blender.org/T99362),
[T104081](http://developer.blender.org/T104081),
[T104060](http://developer.blender.org/T104060),
[T104075](http://developer.blender.org/T104075)

  - Closed as Resolved: 2

[T103755](http://developer.blender.org/T103755),
[T82180](http://developer.blender.org/T82180)

  - Needs Information from User: 12

[T104158](http://developer.blender.org/T104158),
[T104142](http://developer.blender.org/T104142),
[T104120](http://developer.blender.org/T104120),
[T104112](http://developer.blender.org/T104112),
[T104105](http://developer.blender.org/T104105),
[T104070](http://developer.blender.org/T104070),
[T104038](http://developer.blender.org/T104038),
[T104035](http://developer.blender.org/T104035),
[T104031](http://developer.blender.org/T104031),
[T103831](http://developer.blender.org/T103831),
[T103743](http://developer.blender.org/T103743),
[T103542](http://developer.blender.org/T103542)

  - Needs Information from Developers: 4

[T103859](http://developer.blender.org/T103859),
[T104065](http://developer.blender.org/T104065),
[T104080](http://developer.blender.org/T104080),
[T104073](http://developer.blender.org/T104073)

  - Own Reported: 0

<!-- end list -->

  - Diff submitted: 3

[D17142](http://developer.blender.org/D17142): Allow renaming F-curve
modifier  
[D17102](http://developer.blender.org/D17102): Add right margin in
channel block of dope sheet and NLA editor  
[D17079](http://developer.blender.org/D17079): Allow select range in
animation editor

## Week 88 (Jan 30 - 05)

This week: Triaging. Did general bug fixing.  
Did some updates in technical documentation of Outliner (had no enough
time this week).  
Next Week: Triaging. Will complete outliner documentation. Planning to
work on [D17142](http://developer.blender.org/D17142).  
**Total actions on tracker: 174**

  - Confirmed: 15

[T97017](http://developer.blender.org/T97017),
[T104306](http://developer.blender.org/T104306),
[T104297](http://developer.blender.org/T104297),
[T104300](http://developer.blender.org/T104300),
[T104281](http://developer.blender.org/T104281),
[T104252](http://developer.blender.org/T104252),
[T104244](http://developer.blender.org/T104244),
[T104243](http://developer.blender.org/T104243),
[T104241](http://developer.blender.org/T104241),
[T104166](http://developer.blender.org/T104166),
[T104230](http://developer.blender.org/T104230),
[T104174](http://developer.blender.org/T104174),
[T104188](http://developer.blender.org/T104188),
[T104212](http://developer.blender.org/T104212),
[T104226](http://developer.blender.org/T104226)

  - Closed as Archived: 8

[T104189](http://developer.blender.org/T104189),
[T104315](http://developer.blender.org/T104315),
[T104186](http://developer.blender.org/T104186),
[T104227](http://developer.blender.org/T104227),
[T103364](http://developer.blender.org/T103364),
[T103819](http://developer.blender.org/T103819),
[T104225](http://developer.blender.org/T104225),
[T104218](http://developer.blender.org/T104218)

  - Closed as Duplicate: 16

[T104342](http://developer.blender.org/T104342),
[T104349](http://developer.blender.org/T104349),
[T104337](http://developer.blender.org/T104337),
[T104311](http://developer.blender.org/T104311),
[T104286](http://developer.blender.org/T104286),
[T100634](http://developer.blender.org/T100634),
[T104268](http://developer.blender.org/T104268),
[T104161](http://developer.blender.org/T104161),
[T104185](http://developer.blender.org/T104185),
[T104231](http://developer.blender.org/T104231),
[T104229](http://developer.blender.org/T104229),
[T104208](http://developer.blender.org/T104208),
[T104215](http://developer.blender.org/T104215),
[T104224](http://developer.blender.org/T104224),
[T104228](http://developer.blender.org/T104228),
[T104185](http://developer.blender.org/T104185)

  - Closed as Resolved: 5

[T88617](http://developer.blender.org/T88617),
[T90893](http://developer.blender.org/T90893),
[T101950](http://developer.blender.org/T101950),
[T102275](http://developer.blender.org/T102275),
[T104180](http://developer.blender.org/T104180)

  - Needs Information from User: 20

[T104346](http://developer.blender.org/T104346),
[T104313](http://developer.blender.org/T104313),
[T104307](http://developer.blender.org/T104307),
[T104302](http://developer.blender.org/T104302),
[T104264](http://developer.blender.org/T104264),
[T104238](http://developer.blender.org/T104238),
[T104209](http://developer.blender.org/T104209),
[T104200](http://developer.blender.org/T104200),
[T104194](http://developer.blender.org/T104194),
[T104170](http://developer.blender.org/T104170),
[T104163](http://developer.blender.org/T104163),
[T104140](http://developer.blender.org/T104140),
[T103540](http://developer.blender.org/T103540),
[T103048](http://developer.blender.org/T103048),
[T103003](http://developer.blender.org/T103003),
[T101806](http://developer.blender.org/T101806),
[T101696](http://developer.blender.org/T101696),
[T101253](http://developer.blender.org/T101253),
[T99585](http://developer.blender.org/T99585),
[T99328](http://developer.blender.org/T99328)

  - Needs Information from Developers: 3

[T104309](http://developer.blender.org/T104309),
[T93199](http://developer.blender.org/T93199)

  - Diff submitted(updated): 2

[D16878](http://developer.blender.org/D16878): Fix T88617: Wrong
annotation cursor in Preview of sequencer editor  
[D16773](http://developer.blender.org/D16773): Fix T90893: Restoring a
key in the keymap editor doesn't work if the key is only disabled.

  - Commits: 2

[rB1a13940ef80890e3f73f41061c31ed00b8298640](http://developer.blender.org/rB1a13940ef80890e3f73f41061c31ed00b8298640)  
[rBd335db09e0a59499ca74a597d7da43cbb05e7708](http://developer.blender.org/rBd335db09e0a59499ca74a597d7da43cbb05e7708)

## Week 89 (Feb 06 - 12)

This week: Triaging. Did general bug fixing. Moved one diff to new
platform.  
Added [Technical Documentation of
Outliner](https://wiki.blender.org/wiki/Source/Interface/Outliner) on
wiki (Will improve it further).  
Next Week: Triaging. Will move all old revisions to Gitea.  
**Total actions on tracker:** I don't have exact data due to Gitea
migration.

  - Own Reported: 1

[\#104390](https://projects.blender.org/blender/blender/issues/104390)

  - PR submitted: 1

[PR \#104565](https://projects.blender.org/blender/blender/pulls/104565)

## Week 90 (Feb 13 - 19)

This week: Triaging. Did general bug fixing. Moved some diffs to new
platform.  
Visited old reports with "Needs info from User" tag  
Next Week: Triaging.  
**Total actions on tracker: 192**

  - PR Submitted: 3

[PR
\#104949](https://projects.blender.org/blender/blender/pulls/104949):
Allow renaming F-curve modifier  
[PR
\#104862](https://projects.blender.org/blender/blender/pulls/104862):
Fix \#102317: Any action triggers expansion of all ViewLayers  
[PR
\#104737](https://projects.blender.org/blender/blender/pulls/104737):
Fix \#89479: Unable to select hierarchies of multiple objects from
outliner

  - PR Review: 1

[PR
\#104694](https://projects.blender.org/blender/blender/pulls/104694):
Fix \#104166: Added redraw conditions for ID modifications (asset
marking or unmarking)

## Week 91 (Feb 20 - 26)

This week: Triaging. Did general bug fixing.  
Next Week: Triaging. Unavailable till Thursday due to Mid-semester
exams.  
**Total actions on tracker: 155**

  - Pull Requests: 2

[PR
\#105004](https://projects.blender.org/blender/blender/pulls/105004):
Fix \#104992: Crash on calling operation search in outliner  
[PR
\#105080](https://projects.blender.org/blender/blender/pulls/105080):
Fix \#105009: Restore GPencil layer drawing in dopesheet

  - Commits: 2

[f9bcd8c7e8](https://projects.blender.org/blender/blender/commit/f9bcd8c7e8e2abf16c3bc99872bd97c0ebb45a19)  
[b789980e27](https://projects.blender.org/blender/blender/commit/b789980e27ab4236687a94e7d2fdd16fcd9b3d8d)

## Week 92 (Feb 27 - 05)

This week: Triaging. Less activity due to exams (Monday-Thursday OFF).  
Updated PR after change requests.  
Next Week: Triaging.  
**Total actions on tracker: 49**

  - Confirmed: 3

[\#105329](https://projects.blender.org/blender/blender/issues/105329),
[\#105271](https://projects.blender.org/blender/blender/issues/105271),
[\#105326](https://projects.blender.org/blender/blender/issues/105326)

  - Closed: 12

[\#105426](https://projects.blender.org/blender/blender/issues/105426),
[\#103681](https://projects.blender.org/blender/blender/issues/103681),
[\#105398](https://projects.blender.org/blender/blender/issues/105398),
[\#105391](https://projects.blender.org/blender/blender/issues/105391),
[\#104910](https://projects.blender.org/blender/blender/issues/104910),
[\#104865](https://projects.blender.org/blender/blender/issues/104865),
[\#105350](https://projects.blender.org/blender/blender/issues/105350),
[\#103207](https://projects.blender.org/blender/blender/issues/103207),
[\#105367](https://projects.blender.org/blender/blender/issues/105367),
[\#104907](https://projects.blender.org/blender/blender/issues/104907),
[\#105213](https://projects.blender.org/blender/blender/issues/105213),
[\#103995](https://projects.blender.org/blender/blender/issues/103995)

  - PR submitted (updated): 2

[PR
\#104565](https://projects.blender.org/blender/blender/pulls/104565):
Allow select range in animation editor  
[PR
\#104862](https://projects.blender.org/blender/blender/pulls/104862):
Fix \#102317: Any action triggers expansion of all ViewLayers

## Week 93 (Mar 06 - 12)

This week: Triaging. Bug fixing. Reviewed one PR.  
Next Week: Triaging.  
**Total actions on tracker: 127**

  - Confirmed: 18

[\#105573](https://projects.blender.org/blender/blender/issues/105573),
[\#105224](https://projects.blender.org/blender/blender/issues/105224),
[\#105617](https://projects.blender.org/blender/blender/issues/105617),
[\#105625](https://projects.blender.org/blender/blender/issues/105625),
[\#105442](https://projects.blender.org/blender/blender/issues/105442),
[\#105583](https://projects.blender.org/blender/blender/issues/105583),
[\#105587](https://projects.blender.org/blender/blender/issues/105587),
[\#105575](https://projects.blender.org/blender/blender/issues/105575),
[\#105554](https://projects.blender.org/blender/blender/issues/105554),
[\#105548](https://projects.blender.org/blender/blender/issues/105548),
[\#105549](https://projects.blender.org/blender/blender/issues/105549),
[\#105557](https://projects.blender.org/blender/blender/issues/105557),
[\#105496](https://projects.blender.org/blender/blender/issues/105496),
[\#105518](https://projects.blender.org/blender/blender/issues/105518),
[\#105430](https://projects.blender.org/blender/blender/issues/105430),
[\#105438](https://projects.blender.org/blender/blender/issues/105438),
[\#105451](https://projects.blender.org/blender/blender/issues/105451),
[\#105455](https://projects.blender.org/blender/blender/issues/105455)

  - Closed: 24

[\#105674](https://projects.blender.org/blender/blender/issues/105674),
[\#100893](https://projects.blender.org/blender/blender/issues/100893),
[\#102317](https://projects.blender.org/blender/blender/issues/102317),
[\#105586](https://projects.blender.org/blender/blender/issues/105586),
[\#102830](https://projects.blender.org/blender/blender/issues/102830),
[\#105572](https://projects.blender.org/blender/blender/issues/105572),
[\#102786](https://projects.blender.org/blender/blender/issues/102786),
[\#105580](https://projects.blender.org/blender/blender/issues/105580),
[\#105582](https://projects.blender.org/blender/blender/issues/105582),
[\#104410](https://projects.blender.org/blender/blender-addons/issues/104410),
[\#105516](https://projects.blender.org/blender/blender/issues/105516),
[\#105552](https://projects.blender.org/blender/blender/issues/105552),
[\#105563](https://projects.blender.org/blender/blender/issues/105563),
[\#105525](https://projects.blender.org/blender/blender/issues/105525),
[\#105512](https://projects.blender.org/blender/blender/issues/105512),
[\#105517](https://projects.blender.org/blender/blender/issues/105517),
[\#104035](https://projects.blender.org/blender/blender/issues/104035),
[\#105456](https://projects.blender.org/blender/blender/issues/105456),
[\#105462](https://projects.blender.org/blender/blender/issues/105462),
[\#105412](https://projects.blender.org/blender/blender/issues/105412),
[\#105439](https://projects.blender.org/blender/blender/issues/105439),
[\#105458](https://projects.blender.org/blender/blender/issues/105458),
[\#105452](https://projects.blender.org/blender/blender/issues/105452),
[\#104287](https://projects.blender.org/blender/blender/issues/104287)

  - PR submitted (Updated): 1

[PR
\#104565](https://projects.blender.org/blender/blender/pulls/104565):
Allow select range in animation editor

  - Review: 1

[PR
\#105627](https://projects.blender.org/blender/blender/pulls/105627):
Fix \#105625: GPencil sculpt crash with subdivide modifier

  - Commit: 1

[rc2fdbcca3c4db354f36046a8d902742db08edfd5](http://projects.blender.org/scm/viewvc.php?view=rev&root=bf-blender&revision=c2fdbcca3c4db354f36046a8d902742db08edfd5):
Fix \#102317: Any action triggers expansion of all ViewLayers

## Week 94 (Mar 13 - 19)

This week: Triaging. Inactive on Friday (Adjusted working hours during
weekend).  
Revisited old reports where more information from user was needed.  
Next Week: Triaging. Will update the pending patches.  
**Total actions on tracker: 153**

  - Confirmed: 13

[\#105894](https://projects.blender.org/blender/blender/issues/105894),
[\#105891](https://projects.blender.org/blender/blender/issues/105891),
[\#105665](https://projects.blender.org/blender/blender/issues/105665),
[\#105831](https://projects.blender.org/blender/blender/issues/105831),
[\#105791](https://projects.blender.org/blender/blender/issues/105791),
[\#103509](https://projects.blender.org/blender/blender/issues/103509),
[\#104950](https://projects.blender.org/blender/blender/issues/104950),
[\#105695](https://projects.blender.org/blender/blender/issues/105695),
[\#105687](https://projects.blender.org/blender/blender/issues/105687),
[\#105749](https://projects.blender.org/blender/blender/issues/105749),
[\#105654](https://projects.blender.org/blender/blender/issues/105654),
[\#105655](https://projects.blender.org/blender/blender/issues/105655),
[\#105640](https://projects.blender.org/blender/blender/issues/105640)

  - Closed: 41

[\#104930](https://projects.blender.org/blender/blender/issues/104930),
[\#105876](https://projects.blender.org/blender/blender/issues/105876),
[\#105879](https://projects.blender.org/blender/blender/issues/105879),
[\#105883](https://projects.blender.org/blender/blender/issues/105883),
[\#105258](https://projects.blender.org/blender/blender/issues/105258),
[\#103048](https://projects.blender.org/blender/blender/issues/103048),
[\#105463](https://projects.blender.org/blender/blender/issues/105463),
[\#105057](https://projects.blender.org/blender/blender/issues/105057),
[\#105087](https://projects.blender.org/blender/blender/issues/105087),
[\#103174](https://projects.blender.org/blender/blender/issues/103174),
[\#105035](https://projects.blender.org/blender/blender/issues/105035),
[\#104901](https://projects.blender.org/blender/blender/issues/104901),
[\#104609](https://projects.blender.org/blender/blender/issues/104609),
[\#104367](https://projects.blender.org/blender/blender/issues/104367),
[\#104330](https://projects.blender.org/blender/blender/issues/104330),
[\#104336](https://projects.blender.org/blender/blender/issues/104336),
[\#104264](https://projects.blender.org/blender/blender/issues/104264),
[\#104194](https://projects.blender.org/blender/blender/issues/104194),
[\#103776](https://projects.blender.org/blender/blender/issues/103776),
[\#103831](https://projects.blender.org/blender/blender/issues/103831),
[\#103777](https://projects.blender.org/blender/blender/issues/103777),
[\#105481](https://projects.blender.org/blender/blender/issues/105481),
[\#104020](https://projects.blender.org/blender/blender/issues/104020),
[\#104017](https://projects.blender.org/blender/blender/issues/104017),
[\#104214](https://projects.blender.org/blender/blender/issues/104214),
[\#104827](https://projects.blender.org/blender/blender/issues/104827),
[\#105782](https://projects.blender.org/blender/blender/issues/105782),
[\#105082](https://projects.blender.org/blender/blender/issues/105082),
[\#105091](https://projects.blender.org/blender/blender/issues/105091),
[\#105219](https://projects.blender.org/blender/blender/issues/105219),
[\#105490](https://projects.blender.org/blender/blender/issues/105490),
[\#105522](https://projects.blender.org/blender/blender/issues/105522),
[\#105706](https://projects.blender.org/blender/blender/issues/105706),
[\#105704](https://projects.blender.org/blender/blender/issues/105704),
[\#105624](https://projects.blender.org/blender/blender/issues/105624),
[\#105684](https://projects.blender.org/blender/blender/issues/105684),
[\#105670](https://projects.blender.org/blender/blender/issues/105670),
[\#105694](https://projects.blender.org/blender/blender/issues/105694),
[\#105679](https://projects.blender.org/blender/blender/issues/105679),
[\#105658](https://projects.blender.org/blender/blender/issues/105658),
[\#105682](https://projects.blender.org/blender/blender/issues/105682)

  - PR: 0

<!-- end list -->

  - Commits: 0

## Week 95 (Mar 20 - 26)

This week: Triaging. Reviewed Pull requests. Updated/submitted PR.  
Revisited old untriaged reports.  
Next Week: Triaging.  
**Total actions on tracker: 103**

  - Confirmed: 8

[\#105978](https://projects.blender.org/blender/blender/issues/105978),
[\#105889](https://projects.blender.org/blender/blender/issues/105889),
[\#91389](https://projects.blender.org/blender/blender/issues/91389),
[\#105976](https://projects.blender.org/blender/blender/issues/105976),
[\#105942](https://projects.blender.org/blender/blender/issues/105942),
[\#105937](https://projects.blender.org/blender/blender/issues/105937),
[\#105936](https://projects.blender.org/blender/blender/issues/105936),
[\#105894](https://projects.blender.org/blender/blender/issues/105894)

  - Closed: 19

[\#106145](https://projects.blender.org/blender/blender/issues/106145),
[\#106111](https://projects.blender.org/blender/blender/issues/106111),
[\#106058](https://projects.blender.org/blender/blender/issues/106058),
[\#106055](https://projects.blender.org/blender/blender/issues/106055),
[\#98484](https://projects.blender.org/blender/blender/issues/98484),
[\#106056](https://projects.blender.org/blender/blender/issues/106056),
[\#94341](https://projects.blender.org/blender/blender/issues/94341),
[\#104476](https://projects.blender.org/blender/blender/issues/104476),
[\#105981](https://projects.blender.org/blender/blender/issues/105981),
[\#104260](https://projects.blender.org/blender/blender/issues/104260),
[\#105934](https://projects.blender.org/blender/blender/issues/105934),
[\#105923](https://projects.blender.org/blender/blender/issues/105923),
[\#105902](https://projects.blender.org/blender/blender/issues/105902),
[\#104494](https://projects.blender.org/blender/blender/issues/104494),
[\#104496](https://projects.blender.org/blender/blender/issues/104496),
[\#104495](https://projects.blender.org/blender/blender/issues/104495),
[\#105910](https://projects.blender.org/blender/blender/issues/105910),
[\#105911](https://projects.blender.org/blender/blender/issues/105911),
[\#105916](https://projects.blender.org/blender/blender/issues/105916)

  - PR Submitted: 3

[PR
\#106116](https://projects.blender.org/blender/blender/pulls/106116):
Clear active flag of GPencil layer when deselecting  
[PR
\#104565](https://projects.blender.org/blender/blender/pulls/104565):
Allow select range in animation editor  
[PR
\#104737](https://projects.blender.org/blender/blender/pulls/104737):
Fix \#89479: Unable to select hierarchies of multiple objects from
outliner  

  - Review: 2

[PR
\#105913](https://projects.blender.org/blender/blender/pulls/105913):
Fix \#94080: clicking empty space in timeline creates undo step  
[PR
\#105903](https://projects.blender.org/blender/blender/pulls/105903):
fix \#99584: Outliner selection not working correctly after undoing
object deletion  

  - Commits: 0

## Week 96 (Mar 27 - 02)

This week: Triaging. Updated/submitted PR.  
Next Week: Triaging.  
**Total actions on tracker: 141**

  - Confirmed: 13

[\#106326](https://projects.blender.org/blender/blender/issues/106326),
[\#106323](https://projects.blender.org/blender/blender/issues/106323),
[\#106314](https://projects.blender.org/blender/blender/issues/106314),
[\#106289](https://projects.blender.org/blender/blender/issues/106289),
[\#106251](https://projects.blender.org/blender/blender/issues/106251),
[\#106248](https://projects.blender.org/blender/blender/issues/106248),
[\#106246](https://projects.blender.org/blender/blender/issues/106246),
[\#106247](https://projects.blender.org/blender/blender/issues/106247),
[\#106105](https://projects.blender.org/blender/blender/issues/106105),
[\#106199](https://projects.blender.org/blender/blender/issues/106199),
[\#106092](https://projects.blender.org/blender/blender/issues/106092),
[\#106066](https://projects.blender.org/blender/blender/issues/106066),
[\#106139](https://projects.blender.org/blender/blender/issues/106139)

  - Closed: 31

[\#106377](https://projects.blender.org/blender/blender/issues/106377),
[\#106392](https://projects.blender.org/blender/blender/issues/106392),
[\#106329](https://projects.blender.org/blender/blender/issues/106329),
[\#106317](https://projects.blender.org/blender/blender/issues/106317),
[\#106357](https://projects.blender.org/blender/blender/issues/106357),
[\#106333](https://projects.blender.org/blender/blender/issues/106333),
[\#106331](https://projects.blender.org/blender/blender/issues/106331),
[\#105758](https://projects.blender.org/blender/blender/issues/105758),
[\#106285](https://projects.blender.org/blender/blender/issues/106285),
[\#106291](https://projects.blender.org/blender/blender/issues/106291),
[\#106272](https://projects.blender.org/blender/blender/issues/106272),
[\#106290](https://projects.blender.org/blender/blender/issues/106290),
[\#106284](https://projects.blender.org/blender/blender/issues/106284),
[\#106273](https://projects.blender.org/blender/blender/issues/106273),
[\#106179](https://projects.blender.org/blender/blender/issues/106179),
[\#106151](https://projects.blender.org/blender/blender/issues/106151),
[\#106207](https://projects.blender.org/blender/blender/issues/106207),
[\#106205](https://projects.blender.org/blender/blender/issues/106205),
[\#106198](https://projects.blender.org/blender/blender/issues/106198),
[\#105982](https://projects.blender.org/blender/blender/issues/105982),
[\#106184](https://projects.blender.org/blender/blender/issues/106184),
[\#106168](https://projects.blender.org/blender/blender/issues/106168),
[\#106202](https://projects.blender.org/blender/blender/issues/106202),
[\#106154](https://projects.blender.org/blender/blender/issues/106154),
[\#106163](https://projects.blender.org/blender/blender/issues/106163),
[\#106166](https://projects.blender.org/blender/blender/issues/106166),
[\#106147](https://projects.blender.org/blender/blender/issues/106147),
[\#106149](https://projects.blender.org/blender/blender/issues/106149),
[\#106143](https://projects.blender.org/blender/blender/issues/106143),
[\#106137](https://projects.blender.org/blender/blender/issues/106137),
[\#106153](https://projects.blender.org/blender/blender/issues/106153)

  - PR Submitted: 3

[PR
\#106374](https://projects.blender.org/blender/blender/pulls/106374):
Fix \#106246: Crash on deleting the multi-level hierarchy  
[PR
\#104737](https://projects.blender.org/blender/blender/pulls/104737):
Fix \#89479: Unable to select hierarchies of multiple objects from
outliner  
[PR
\#104565](https://projects.blender.org/blender/blender/pulls/104565):
Allow select range in animation editor  

  - Commits: 0

## Week 97 (Apr 03 - 09)

This week: Triaging. Reviewed proposals. Was working on [PR
\#104565](https://projects.blender.org/blender/blender/pulls/104565).  
Revisited bug reports where more information from reporter was needed.  
Next Week: Triaging.  
**Total actions on tracker: 177**

  - Confirmed: 6

[\#102919](https://projects.blender.org/blender/blender/issues/102919),
[\#106373](https://projects.blender.org/blender/blender/issues/106373),
[\#106641](https://projects.blender.org/blender/blender/issues/106641),
[\#106394](https://projects.blender.org/blender/blender/issues/106394),
[\#106369](https://projects.blender.org/blender/blender/issues/106369),
[\#106366](https://projects.blender.org/blender/blender/issues/106366)

  - Closed: 57

[\#106703](https://projects.blender.org/blender/blender/issues/106703),
[\#106618](https://projects.blender.org/blender/blender/issues/106618),
[\#106647](https://projects.blender.org/blender/blender/issues/106647),
[\#106650](https://projects.blender.org/blender/blender/issues/106650),
[\#106613](https://projects.blender.org/blender/blender/issues/106613),
[\#106646](https://projects.blender.org/blender/blender/issues/106646),
[\#106632](https://projects.blender.org/blender/blender/issues/106632),
[\#105068](https://projects.blender.org/blender/blender/issues/105068),
[\#103003](https://projects.blender.org/blender/blender/issues/103003),
[\#103542](https://projects.blender.org/blender/blender/issues/103542),
[\#103660](https://projects.blender.org/blender/blender/issues/103660),
[\#103713](https://projects.blender.org/blender/blender/issues/103713),
[\#104042](https://projects.blender.org/blender/blender/issues/104042),
[\#104232](https://projects.blender.org/blender/blender/issues/104232),
[\#104238](https://projects.blender.org/blender/blender/issues/104238),
[\#104317](https://projects.blender.org/blender/blender/issues/104317),
[\#105140](https://projects.blender.org/blender/blender/issues/105140),
[\#104675](https://projects.blender.org/blender/blender/issues/104675),
[\#104852](https://projects.blender.org/blender/blender/issues/104852),
[\#104851](https://projects.blender.org/blender/blender/issues/104851),
[\#105093](https://projects.blender.org/blender/blender/issues/105093),
[\#105158](https://projects.blender.org/blender/blender/issues/105158),
[\#105294](https://projects.blender.org/blender/blender/issues/105294),
[\#105440](https://projects.blender.org/blender/blender/issues/105440),
[\#105465](https://projects.blender.org/blender/blender/issues/105465),
[\#105641](https://projects.blender.org/blender/blender/issues/105641),
[\#105660](https://projects.blender.org/blender/blender/issues/105660),
[\#105668](https://projects.blender.org/blender/blender/issues/105668),
[\#105675](https://projects.blender.org/blender/blender/issues/105675),
[\#105887](https://projects.blender.org/blender/blender/issues/105887),
[\#105893](https://projects.blender.org/blender/blender/issues/105893),
[\#105845](https://projects.blender.org/blender/blender/issues/105845),
[\#106118](https://projects.blender.org/blender/blender/issues/106118),
[\#106109](https://projects.blender.org/blender/blender/issues/106109),
[\#106274](https://projects.blender.org/blender/blender/issues/106274),
[\#106547](https://projects.blender.org/blender/blender/issues/106547),
[\#106362](https://projects.blender.org/blender/blender/issues/106362),
[\#106429](https://projects.blender.org/blender/blender/issues/106429),
[\#106529](https://projects.blender.org/blender/blender/issues/106529),
[\#106390](https://projects.blender.org/blender/blender/issues/106390),
[\#106420](https://projects.blender.org/blender/blender/issues/106420),
[\#106486](https://projects.blender.org/blender/blender/issues/106486),
[\#106419](https://projects.blender.org/blender/blender/issues/106419),
[\#106337](https://projects.blender.org/blender/blender/issues/106337),
[\#106336](https://projects.blender.org/blender/blender/issues/106336),
[\#106445](https://projects.blender.org/blender/blender/issues/106445),
[\#106407](https://projects.blender.org/blender/blender/issues/106407),
[\#106364](https://projects.blender.org/blender/blender/issues/106364),
[\#106473](https://projects.blender.org/blender/blender/issues/106473),
[\#106464](https://projects.blender.org/blender/blender/issues/106464),
[\#106465](https://projects.blender.org/blender/blender/issues/106465),
[\#106468](https://projects.blender.org/blender/blender/issues/106468),
[\#106286](https://projects.blender.org/blender/blender/issues/106286),
[\#106447](https://projects.blender.org/blender/blender/issues/106447),
[\#106446](https://projects.blender.org/blender/blender/issues/106446),
[\#106408](https://projects.blender.org/blender/blender/issues/106408),
[\#106409](https://projects.blender.org/blender/blender/issues/106409)

  - PR submitted (Updated): 1

[PR \#104737](https://projects.blender.org/blender/blender/pulls/104737)

## Week 98 (Apr 10 - 16)

This week: Triaging. Worked on existing PRs.  
Next Week: Triaging.  
**Total actions on tracker: 118**

  - Confirmed: 18

[\#106934](https://projects.blender.org/blender/blender/issues/106934),
[\#106929](https://projects.blender.org/blender/blender/issues/106929),
[\#106849](https://projects.blender.org/blender/blender/issues/106849),
[\#106431](https://projects.blender.org/blender/blender/issues/106431),
[\#106893](https://projects.blender.org/blender/blender/issues/106893),
[\#106895](https://projects.blender.org/blender/blender/issues/106895),
[\#106879](https://projects.blender.org/blender/blender/issues/106879),
[\#106796](https://projects.blender.org/blender/blender/issues/106796),
[\#106776](https://projects.blender.org/blender/blender/issues/106776),
[\#106216](https://projects.blender.org/blender/blender/issues/106216),
[\#106715](https://projects.blender.org/blender/blender/issues/106715),
[\#106722](https://projects.blender.org/blender/blender/issues/106722),
[\#106706](https://projects.blender.org/blender/blender/issues/106706),
[\#106720](https://projects.blender.org/blender/blender/issues/106720),
[\#106730](https://projects.blender.org/blender/blender/issues/106730),
[\#106745](https://projects.blender.org/blender/blender/issues/106745),
[\#106746](https://projects.blender.org/blender/blender/issues/106746),
[\#106747](https://projects.blender.org/blender/blender/issues/106747)

  - Closed: 24

[\#106979](https://projects.blender.org/blender/blender/issues/106979),
[\#106968](https://projects.blender.org/blender/blender/issues/106968),
[\#105382](https://projects.blender.org/blender/blender/issues/105382),
[\#106910](https://projects.blender.org/blender/blender/issues/106910),
[\#106319](https://projects.blender.org/blender/blender/issues/106319),
[\#106893](https://projects.blender.org/blender/blender/issues/106893),
[\#106866](https://projects.blender.org/blender/blender/issues/106866),
[\#106896](https://projects.blender.org/blender/blender/issues/106896),
[\#104601](https://projects.blender.org/blender/blender/issues/104601),
[\#106761](https://projects.blender.org/blender/blender/issues/106761),
[\#106382](https://projects.blender.org/blender/blender/issues/106382),
[\#106836](https://projects.blender.org/blender/blender/issues/106836),
[\#106839](https://projects.blender.org/blender/blender/issues/106839),
[\#106767](https://projects.blender.org/blender/blender/issues/106767),
[\#106793](https://projects.blender.org/blender/blender/issues/106793),
[\#106756](https://projects.blender.org/blender/blender/issues/106756),
[\#106758](https://projects.blender.org/blender/blender/issues/106758),
[\#106709](https://projects.blender.org/blender/blender/issues/106709),
[\#106714](https://projects.blender.org/blender/blender/issues/106714),
[\#106737](https://projects.blender.org/blender/blender/issues/106737),
[\#106685](https://projects.blender.org/blender/blender/issues/106685),
[\#106729](https://projects.blender.org/blender/blender/issues/106729),
[\#106735](https://projects.blender.org/blender/blender/issues/106735),
[\#106733](https://projects.blender.org/blender/blender/issues/106733)

  - Own Reported: 1

[\#106930](https://projects.blender.org/blender/blender/issues/106930)

  - PR submitted (Updated): 2

[PR
\#104565](https://projects.blender.org/blender/blender/pulls/104565):
Allow select range in animation editor  
[PR
\#104949](https://projects.blender.org/blender/blender/pulls/104949):
Allow renaming F-curve modifier

## Week 99 (Apr 17 - 23)

This week: Triaging. Bug fixing. Worked on existing PRs.  
Reviewed one PR and some GSoC proposals.  
Next Week: Triaging.  
**Total actions on tracker: 137**

  - Confirmed: 20

[\#107186](https://projects.blender.org/blender/blender/issues/107186),
[\#107194](https://projects.blender.org/blender/blender/issues/107194),
[\#107198](https://projects.blender.org/blender/blender/issues/107198),
[\#107062](https://projects.blender.org/blender/blender/issues/107062),
[\#107095](https://projects.blender.org/blender/blender/issues/107095),
[\#107125](https://projects.blender.org/blender/blender/issues/107125),
[\#107102](https://projects.blender.org/blender/blender/issues/107102),
[\#107111](https://projects.blender.org/blender/blender/issues/107111),
[\#107023](https://projects.blender.org/blender/blender/issues/107023),
[\#107026](https://projects.blender.org/blender/blender/issues/107026),
[\#107060](https://projects.blender.org/blender/blender/issues/107060),
[\#106999](https://projects.blender.org/blender/blender/issues/106999),
[\#106971](https://projects.blender.org/blender/blender/issues/106971),
[\#107009](https://projects.blender.org/blender/blender/issues/107009),
[\#107002](https://projects.blender.org/blender/blender/issues/107002),
[\#107011](https://projects.blender.org/blender/blender/issues/107011),
[\#107005](https://projects.blender.org/blender/blender/issues/107005),
[\#107020](https://projects.blender.org/blender/blender/issues/107020),
[\#107021](https://projects.blender.org/blender/blender/issues/107021),
[\#107018](https://projects.blender.org/blender/blender/issues/107018)

  - Closed: 25

[\#104563](https://projects.blender.org/blender/blender-addons/issues/104563),
[\#107200](https://projects.blender.org/blender/blender/issues/107200),
[\#107211](https://projects.blender.org/blender/blender/issues/107211),
[\#107171](https://projects.blender.org/blender/blender/issues/107171),
[\#106550](https://projects.blender.org/blender/blender/issues/106550),
[\#107192](https://projects.blender.org/blender/blender/issues/107192),
[\#107201](https://projects.blender.org/blender/blender/issues/107201),
[\#107134](https://projects.blender.org/blender/blender/issues/107134),
[\#107157](https://projects.blender.org/blender/blender/issues/107157),
[\#107155](https://projects.blender.org/blender/blender/issues/107155),
[\#107128](https://projects.blender.org/blender/blender/issues/107128),
[\#107104](https://projects.blender.org/blender/blender/issues/107104),
[\#107113](https://projects.blender.org/blender/blender/issues/107113),
[\#107124](https://projects.blender.org/blender/blender/issues/107124),
[\#107112](https://projects.blender.org/blender/blender/issues/107112),
[\#107115](https://projects.blender.org/blender/blender/issues/107115),
[\#107046](https://projects.blender.org/blender/blender/issues/107046),
[\#90159](https://projects.blender.org/blender/blender/issues/90159),
[\#107026](https://projects.blender.org/blender/blender/issues/107026),
[\#107028](https://projects.blender.org/blender/blender/issues/107028),
[\#107029](https://projects.blender.org/blender/blender/issues/107029),
[\#106971](https://projects.blender.org/blender/blender/issues/106971),
[\#107012](https://projects.blender.org/blender/blender/issues/107012),
[\#107006](https://projects.blender.org/blender/blender/issues/107006),
[\#107018](https://projects.blender.org/blender/blender/issues/107018)

  - PR: 2

[PR
\#107034](https://projects.blender.org/blender/blender/pulls/107034):
Fix \#90159: Inconsistent display of active filters for import/export
file dialogs  
[PR
\#104949](https://projects.blender.org/blender/blender/pulls/104949):
Allow renaming F-curve modifier

  - Code Review: 1

[PR
\#105872](https://projects.blender.org/blender/blender/pulls/105872):
Fix \#104722: Outliner Renaming Double Click Error

  - Commits: 1

[7c927155b5](https://projects.blender.org/blender/blender/commit/7c927155b5e70a0c69cdc2ec26405cbc5f5e44f1)

## Week 100 (Apr 24 - 30)

This week: Triaging. Bug fixing.  
Revisited bug reports where more information from reporter was needed.  
Next Week: Triaging.  
**Total actions on tracker: 160**

  - Confirmed: 7

[\#107036](https://projects.blender.org/blender/blender/issues/107036),
[\#107416](https://projects.blender.org/blender/blender/issues/107416),
[\#107365](https://projects.blender.org/blender/blender/issues/107365),
[\#106563](https://projects.blender.org/blender/blender/issues/106563),
[\#107322](https://projects.blender.org/blender/blender/issues/107322),
[\#107286](https://projects.blender.org/blender/blender/issues/107286),
[\#107265](https://projects.blender.org/blender/blender/issues/107265)

  - Closed: 43

[\#101968](https://projects.blender.org/blender/blender/issues/101968),
[\#106009](https://projects.blender.org/blender/blender/issues/106009),
[\#105428](https://projects.blender.org/blender/blender/issues/105428),
[\#104905](https://projects.blender.org/blender/blender/issues/104905),
[\#103540](https://projects.blender.org/blender/blender/issues/103540),
[\#103506](https://projects.blender.org/blender/blender/issues/103506),
[\#107049](https://projects.blender.org/blender/blender/issues/107049),
[\#107000](https://projects.blender.org/blender/blender/issues/107000),
[\#106991](https://projects.blender.org/blender/blender/issues/106991),
[\#106941](https://projects.blender.org/blender/blender/issues/106941),
[\#106908](https://projects.blender.org/blender/blender/issues/106908),
[\#106818](https://projects.blender.org/blender/blender/issues/106818),
[\#106712](https://projects.blender.org/blender/blender/issues/106712),
[\#106768](https://projects.blender.org/blender/blender/issues/106768),
[\#106649](https://projects.blender.org/blender/blender/issues/106649),
[\#106717](https://projects.blender.org/blender/blender/issues/106717),
[\#106707](https://projects.blender.org/blender/blender/issues/106707),
[\#106626](https://projects.blender.org/blender/blender/issues/106626),
[\#106582](https://projects.blender.org/blender/blender/issues/106582),
[\#106562](https://projects.blender.org/blender/blender/issues/106562),
[\#106567](https://projects.blender.org/blender/blender/issues/106567),
[\#106436](https://projects.blender.org/blender/blender/issues/106436),
[\#106485](https://projects.blender.org/blender/blender/issues/106485),
[\#106463](https://projects.blender.org/blender/blender/issues/106463),
[\#106448](https://projects.blender.org/blender/blender/issues/106448),
[\#106603](https://projects.blender.org/blender/blender/issues/106603),
[\#107392](https://projects.blender.org/blender/blender/issues/107392),
[\#107395](https://projects.blender.org/blender/blender/issues/107395),
[\#107393](https://projects.blender.org/blender/blender/issues/107393),
[\#102580](https://projects.blender.org/blender/blender/issues/102580),
[\#107313](https://projects.blender.org/blender/blender/issues/107313),
[\#107366](https://projects.blender.org/blender/blender/issues/107366),
[\#107354](https://projects.blender.org/blender/blender/issues/107354),
[\#107368](https://projects.blender.org/blender/blender/issues/107368),
[\#107273](https://projects.blender.org/blender/blender/issues/107273),
[\#107239](https://projects.blender.org/blender/blender/issues/107239),
[\#107321](https://projects.blender.org/blender/blender/issues/107321),
[\#107319](https://projects.blender.org/blender/blender/issues/107319),
[\#107311](https://projects.blender.org/blender/blender/issues/107311),
[\#107305](https://projects.blender.org/blender/blender/issues/107305),
[\#107251](https://projects.blender.org/blender/blender/issues/107251),
[\#107268](https://projects.blender.org/blender/blender/issues/107268),
[\#107284](https://projects.blender.org/blender/blender/issues/107284)

  - PR submitted: 2

[PR
\#107466](https://projects.blender.org/blender/blender/pulls/107466):
Fix \#107011: Boolean Modifier Overlap Threshold property issue in UI
when value is 0  
[PR
\#107323](https://projects.blender.org/blender/blender/pulls/107323):
Fix \#107273: Group input is not listed in the node search

  - Review: 1

[PR
\#105872](https://projects.blender.org/blender/blender/pulls/105872):
Fix \#104722: Outliner Renaming Double Click Error

  - Commits: 1

[d742223197](https://projects.blender.org/blender/blender/commit/d742223197d25b7d2a9d4598fd09be6404c3f840)

## Week 101 (May 01 - 07)

This week: Triaging. Worked on existing animation editor PRs (merged
into main now).  
Next Week: Triaging.  
**Total actions on tracker: 113**

  - Confirmed: 9

[\#107694](https://projects.blender.org/blender/blender/issues/107694),
[\#107589](https://projects.blender.org/blender/blender/issues/107589),
[\#107603](https://projects.blender.org/blender/blender/issues/107603),
[\#107608](https://projects.blender.org/blender/blender/issues/107608),
[\#107570](https://projects.blender.org/blender/blender/issues/107570),
[\#107500](https://projects.blender.org/blender/blender/issues/107500),
[\#104576](https://projects.blender.org/blender/blender/issues/104576),
[\#107482](https://projects.blender.org/blender/blender/issues/107482),
[\#107168](https://projects.blender.org/blender/blender/issues/107168)

  - Closed: 19

[\#107701](https://projects.blender.org/blender/blender/issues/107701),
[\#107631](https://projects.blender.org/blender/blender/issues/107631),
[\#107532](https://projects.blender.org/blender/blender/issues/107532),
[\#107640](https://projects.blender.org/blender/blender/issues/107640),
[\#107601](https://projects.blender.org/blender/blender/issues/107601),
[\#106577](https://projects.blender.org/blender/blender/issues/106577),
[\#107481](https://projects.blender.org/blender/blender/issues/107481),
[\#106378](https://projects.blender.org/blender/blender/issues/106378),
[\#107518](https://projects.blender.org/blender/blender/issues/107518),
[\#107520](https://projects.blender.org/blender/blender/issues/107520),
[\#107527](https://projects.blender.org/blender/blender/issues/107527),
[\#107524](https://projects.blender.org/blender/blender/issues/107524),
[\#104570](https://projects.blender.org/blender/blender/issues/104570),
[\#104571](https://projects.blender.org/blender/blender/issues/104571),
[\#104572](https://projects.blender.org/blender/blender/issues/104572),
[\#104577](https://projects.blender.org/blender/blender/issues/104577),
[\#107477](https://projects.blender.org/blender/blender/issues/107477),
[\#107479](https://projects.blender.org/blender/blender/issues/107479),
[\#107490](https://projects.blender.org/blender/blender/issues/107490)

  - Own Reported: 1

[\#107577](https://projects.blender.org/blender/blender/issues/107577)

  - PR submitted (updated): 2

[PR
\#104565](https://projects.blender.org/blender/blender/pulls/104565):
Allow select range in animation editor  
[PR
\#104949](https://projects.blender.org/blender/blender/pulls/104949):
Allow renaming F-curve modifier

  - Commits: 3

[80feb13665](https://projects.blender.org/blender/blender/commit/80feb136653195551ae0b5bd49567d57e0ed6810):
Allow select range in animation editor  
[c04e709c6a](https://projects.blender.org/blender/blender/commit/c04e709c6a487d59053756f549474b0d220e88d5):
Bump blender file format subversion  
[0001485365](https://projects.blender.org/blender/blender/commit/0001485365ad7e4235a9a991778bf21959d02c29):
Allow renaming F-curve modifier  

## Week 102 (May 08 - 14)

This week: Triaging. Bug fixing. First round of code review.  
Final exams has started this week (till 24th May). Inactive on Friday.  
Next Week: Triaging.  
**Total actions on tracker: 102**

  - Confirmed: 12

[\#107924](https://projects.blender.org/blender/blender/issues/107924),
[\#107910](https://projects.blender.org/blender/blender/issues/107910),
[\#107898](https://projects.blender.org/blender/blender/issues/107898),
[\#107906](https://projects.blender.org/blender/blender/issues/107906),
[\#107852](https://projects.blender.org/blender/blender/issues/107852),
[\#107838](https://projects.blender.org/blender/blender/issues/107838),
[\#107830](https://projects.blender.org/blender/blender/issues/107830),
[\#107792](https://projects.blender.org/blender/blender/issues/107792),
[\#107791](https://projects.blender.org/blender/blender/issues/107791),
[\#107789](https://projects.blender.org/blender/blender/issues/107789),
[\#107718](https://projects.blender.org/blender/blender/issues/107718),
[\#107715](https://projects.blender.org/blender/blender/issues/107715)

  - Closed as Archived: 15

[\#105520](https://projects.blender.org/blender/blender/issues/105520),
[\#107908](https://projects.blender.org/blender/blender/issues/107908),
[\#104585](https://projects.blender.org/blender/blender/issues/104585),
[\#107377](https://projects.blender.org/blender/blender/issues/107377),
[\#107904](https://projects.blender.org/blender/blender/issues/107904),
[\#107815](https://projects.blender.org/blender/blender/issues/107815),
[\#107841](https://projects.blender.org/blender/blender/issues/107841),
[\#107821](https://projects.blender.org/blender/blender/issues/107821),
[\#104593](https://projects.blender.org/blender/blender/issues/104593),
[\#107813](https://projects.blender.org/blender/blender/issues/107813),
[\#107812](https://projects.blender.org/blender/blender/issues/107812),
[\#107160](https://projects.blender.org/blender/blender/issues/107160),
[\#107787](https://projects.blender.org/blender/blender/issues/107787),
[\#107788](https://projects.blender.org/blender/blender/issues/107788),
[\#107716](https://projects.blender.org/blender/blender/issues/107716)

  - Own Reported: 2

[\#104441](https://projects.blender.org/blender/blender-manual/issues/104441),
[\#107718](https://projects.blender.org/blender/blender/issues/107718)

  - PR submitted: 1

[PR
\#107719](https://projects.blender.org/blender/blender/pulls/107719):
Fix \#107718: Extend channel selection not working due to key conflicts

  - Review: 2

[PR
\#107695](https://projects.blender.org/blender/blender/pulls/107695):
Replace copy to selected op with data transfer  
[PR
\#104599](https://projects.blender.org/blender/blender-addons/pulls/104599):
NodeWrangler: Fix \#104598 Added SimNodes to reset ignore list

## Week 103 (May 15 - 21)

This week: Triaging. General code contribution.  
Short week due to exams (Half-day on Monday, holiday on Wednesday).  
Next Week: Triaging.  
**Total actions on tracker:107**

  - Confirmed: 2

[\#108047](https://projects.blender.org/blender/blender/issues/108047),
[\#107965](https://projects.blender.org/blender/blender/issues/107965)

  - Closed: 26

[\#107718](https://projects.blender.org/blender/blender/issues/107718),
[\#108073](https://projects.blender.org/blender/blender/issues/108073),
[\#106426](https://projects.blender.org/blender/blender/issues/106426),
[\#108075](https://projects.blender.org/blender/blender/issues/108075),
[\#108069](https://projects.blender.org/blender/blender/issues/108069),
[\#108071](https://projects.blender.org/blender/blender/issues/108071),
[\#108064](https://projects.blender.org/blender/blender/issues/108064),
[\#107114](https://projects.blender.org/blender/blender/issues/107114),
[\#108006](https://projects.blender.org/blender/blender/issues/108006),
[\#108044](https://projects.blender.org/blender/blender/issues/108044),
[\#108010](https://projects.blender.org/blender/blender/issues/108010),
[\#108009](https://projects.blender.org/blender/blender/issues/108009),
[\#107999](https://projects.blender.org/blender/blender/issues/107999),
[\#104602](https://projects.blender.org/blender/blender/issues/104602),
[\#107965](https://projects.blender.org/blender/blender/issues/107965),
[\#102783](https://projects.blender.org/blender/blender/issues/102783),
[\#107960](https://projects.blender.org/blender/blender/issues/107960),
[\#103827](https://projects.blender.org/blender/blender-addons/issues/103827),
[\#103836](https://projects.blender.org/blender/blender-addons/issues/103836),
[\#104032](https://projects.blender.org/blender/blender-addons/issues/104032),
[\#104173](https://projects.blender.org/blender/blender-addons/issues/104173),
[\#104314](https://projects.blender.org/blender/blender-addons/issues/104314),
[\#104328](https://projects.blender.org/blender/blender-addons/issues/104328),
[\#104475](https://projects.blender.org/blender/blender-addons/issues/104475),
[\#104575](https://projects.blender.org/blender/blender-addons/issues/104575),
[\#107751](https://projects.blender.org/blender/blender/issues/107751)

  - PR submitted: 1

[PR
\#107985](https://projects.blender.org/blender/blender/pulls/107985):
Animation: Set hardmin/max for frame start and end of action

  - Commits: 1

[6c4da68ad2](https://projects.blender.org/blender/blender/commit/6c4da68ad2320608798e6418b5bdd5222053686d):
Fix \#107718: Extend channel selection not working due to key conflicts

## Week 104 (May 22 - 28)

This week: Triaging. Updated existing PRs.  
Short week due to exams (holiday on Tuesday, Wednesday).  
Next Week: Triaging. (Back again from exams.)  
**Total actions on tracker: 86**

  - Confirmed: 11

[\#108316](https://projects.blender.org/blender/blender/issues/108316),
[\#108303](https://projects.blender.org/blender/blender/issues/108303),
[\#104639](https://projects.blender.org/blender/blender-addons/issues/104639),
[\#108198](https://projects.blender.org/blender/blender/issues/108198),
[\#108267](https://projects.blender.org/blender/blender/issues/108267),
[\#108258](https://projects.blender.org/blender/blender/issues/108258),
[\#108250](https://projects.blender.org/blender/blender/issues/108250),
[\#108247](https://projects.blender.org/blender/blender/issues/108247),
[\#108053](https://projects.blender.org/blender/blender/issues/108053),
[\#108106](https://projects.blender.org/blender/blender/issues/108106),
[\#108078](https://projects.blender.org/blender/blender/issues/108078)

  - Closed: 18

[\#108348](https://projects.blender.org/blender/blender/issues/108348),
[\#108066](https://projects.blender.org/blender/blender/issues/108066),
[\#104601](https://projects.blender.org/blender/blender/issues/104601),
[\#107910](https://projects.blender.org/blender/blender/issues/107910),
[\#107993](https://projects.blender.org/blender/blender/issues/107993),
[\#107995](https://projects.blender.org/blender/blender/issues/107995),
[\#108205](https://projects.blender.org/blender/blender/issues/108205),
[\#108269](https://projects.blender.org/blender/blender/issues/108269),
[\#108216](https://projects.blender.org/blender/blender/issues/108216),
[\#107769](https://projects.blender.org/blender/blender/issues/107769),
[\#107708](https://projects.blender.org/blender/blender/issues/107708),
[\#108255](https://projects.blender.org/blender/blender/issues/108255),
[\#107556](https://projects.blender.org/blender/blender/issues/107556),
[\#107571](https://projects.blender.org/blender/blender/issues/107571),
[\#107526](https://projects.blender.org/blender/blender/issues/107526),
[\#108102](https://projects.blender.org/blender/blender/issues/108102),
[\#108228](https://projects.blender.org/blender/blender/issues/108228),
[\#104629](https://projects.blender.org/blender/blender/issues/104629)

  - PR submitted (updated): 2

[PR
\#107466](https://projects.blender.org/blender/blender/pulls/107466):
Fix \#107011: Boolean Modifier Overlap Threshold property issue in UI
when value is 0  
[PR
\#107985](https://projects.blender.org/blender/blender/pulls/107985):
Animation: Set hardmin/max for frame start and end of action

## Week 105 (May 29 - 04)

This week: Triaging. General code contribution.  
Next Week: Triaging.  
**Total actions on tracker: 122**

  - Confirmed: 14

[\#108513](https://projects.blender.org/blender/blender/issues/108513),
[\#104660](https://projects.blender.org/blender/blender-addons/issues/104660),
[\#107162](https://projects.blender.org/blender/blender/issues/107162),
[\#108490](https://projects.blender.org/blender/blender/issues/108490),
[\#104642](https://projects.blender.org/blender/blender-addons/issues/104642),
[\#107159](https://projects.blender.org/blender/blender/issues/107159),
[\#107624](https://projects.blender.org/blender/blender/issues/107624),
[\#108372](https://projects.blender.org/blender/blender/issues/108372),
[\#108407](https://projects.blender.org/blender/blender/issues/108407),
[\#108403](https://projects.blender.org/blender/blender/issues/108403),
[\#108211](https://projects.blender.org/blender/blender/issues/108211),
[\#108384](https://projects.blender.org/blender/blender/issues/108384),
[\#107546](https://projects.blender.org/blender/blender/issues/107546),
[\#108363](https://projects.blender.org/blender/blender/issues/108363)

  - Closed: 24

[\#108490](https://projects.blender.org/blender/blender/issues/108490),
[\#107488](https://projects.blender.org/blender/blender/issues/107488),
[\#107428](https://projects.blender.org/blender/blender/issues/107428),
[\#107491](https://projects.blender.org/blender/blender/issues/107491),
[\#107590](https://projects.blender.org/blender/blender/issues/107590),
[\#108475](https://projects.blender.org/blender/blender/issues/108475),
[\#108058](https://projects.blender.org/blender/blender/issues/108058),
[\#108118](https://projects.blender.org/blender/blender/issues/108118),
[\#108065](https://projects.blender.org/blender/blender/issues/108065),
[\#108493](https://projects.blender.org/blender/blender/issues/108493),
[\#106938](https://projects.blender.org/blender/blender/issues/106938),
[\#106330](https://projects.blender.org/blender/blender/issues/106330),
[\#108101](https://projects.blender.org/blender/blender/issues/108101),
[\#107557](https://projects.blender.org/blender/blender/issues/107557),
[\#108443](https://projects.blender.org/blender/blender/issues/108443),
[\#108426](https://projects.blender.org/blender/blender/issues/108426),
[\#106543](https://projects.blender.org/blender/blender/issues/106543),
[\#108406](https://projects.blender.org/blender/blender/issues/108406),
[\#108405](https://projects.blender.org/blender/blender/issues/108405),
[\#108400](https://projects.blender.org/blender/blender/issues/108400),
[\#108384](https://projects.blender.org/blender/blender/issues/108384),
[\#104638](https://projects.blender.org/blender/blender-addons/issues/104638),
[\#108383](https://projects.blender.org/blender/blender/issues/108383),
[\#108062](https://projects.blender.org/blender/blender/issues/108062)

  - Own Reported: 1

[\#108448](https://projects.blender.org/blender/blender/issues/108448)

  - PR submitted: 2

[PR
\#108452](https://projects.blender.org/blender/blender/pulls/108452):
Fix \#106966: Register undo-step when unlinking data-block  
[PR
\#107985](https://projects.blender.org/blender/blender/pulls/107985):
Animation: Set hardmin/max for frame start and end of action

  - Commits: 1

[19a9941816](https://projects.blender.org/blender/blender/commit/19a9941816d000d4031c5b49f0918871e56d5ce7):
Fix \#107011: Support logarithmic scale when values are zero

## Week 106 (June 05 - 11)

This week: Triaging. General code contribution. First round of review.  
Next Week: Triaging.  
**Total actions on tracker: 133**

  - Confirmed: 12

[\#107625](https://projects.blender.org/blender/blender/issues/107625),
[\#108763](https://projects.blender.org/blender/blender/issues/108763),
[\#108703](https://projects.blender.org/blender/blender/issues/108703),
[\#108732](https://projects.blender.org/blender/blender/issues/108732),
[\#108667](https://projects.blender.org/blender/blender/issues/108667),
[\#108655](https://projects.blender.org/blender/blender/issues/108655),
[\#108665](https://projects.blender.org/blender/blender/issues/108665),
[\#108624](https://projects.blender.org/blender/blender/issues/108624),
[\#108581](https://projects.blender.org/blender/blender/issues/108581),
[\#108211](https://projects.blender.org/blender/blender/issues/108211),
[\#108574](https://projects.blender.org/blender/blender/issues/108574),
[\#104679](https://projects.blender.org/blender/blender-addons/issues/104679)

  - Closed: 23

[\#108812](https://projects.blender.org/blender/blender/issues/108812),
[\#106966](https://projects.blender.org/blender/blender/issues/106966),
[\#107272](https://projects.blender.org/blender/blender/issues/107272),
[\#104628](https://projects.blender.org/blender/blender/issues/104628),
[\#108377](https://projects.blender.org/blender/blender/issues/108377),
[\#108132](https://projects.blender.org/blender/blender/issues/108132),
[\#106784](https://projects.blender.org/blender/blender/issues/106784),
[\#104538](https://projects.blender.org/blender/blender/issues/104538),
[\#102675](https://projects.blender.org/blender/blender/issues/102675),
[\#104655](https://projects.blender.org/blender/blender-addons/issues/104655),
[\#108785](https://projects.blender.org/blender/blender/issues/108785),
[\#108739](https://projects.blender.org/blender/blender/issues/108739),
[\#108716](https://projects.blender.org/blender/blender/issues/108716),
[\#108732](https://projects.blender.org/blender/blender/issues/108732),
[\#104604](https://projects.blender.org/blender/blender-addons/issues/104604),
[\#104589](https://projects.blender.org/blender/blender-addons/issues/104589),
[\#104355](https://projects.blender.org/blender/blender-addons/issues/104355),
[\#108687](https://projects.blender.org/blender/blender/issues/108687),
[\#108644](https://projects.blender.org/blender/blender/issues/108644),
[\#104459](https://projects.blender.org/blender/blender-manual/issues/104459),
[\#108576](https://projects.blender.org/blender/blender/issues/108576),
[\#108588](https://projects.blender.org/blender/blender/issues/108588),
[\#108590](https://projects.blender.org/blender/blender/issues/108590)

  - Own Reported: 2

[\#108745](https://projects.blender.org/blender/blender/issues/108745),
[\#108688](https://projects.blender.org/blender/blender/issues/108688)

  - PR submitted: 4

[PR
\#108790](https://projects.blender.org/blender/blender/pulls/108790):
Animation: Change channel selection key preferences in IC keymap  
[PR
\#108452](https://projects.blender.org/blender/blender/pulls/108452):
Fix \#106966: Register undo-step when unlinking data-block  
[PR
\#108652](https://projects.blender.org/blender/blender/pulls/108652):
GPencil 3.0: Set default tool in draw mode  
[PR
\#104737](https://projects.blender.org/blender/blender/pulls/104737):
Fix \#89479: Unable to select hierarchies of multiple objects from
outliner  

  - Review: 3

[PR
\#108544](https://projects.blender.org/blender/blender/pulls/108544):
Fix \#104628: rename Texture-paint panel 'Brush Tip' to 'Cursor'  
[PR
\#108762](https://projects.blender.org/blender/blender/pulls/108762):
Fix \#108745: Crash in debug build when deleting object  
[PR
\#108494](https://projects.blender.org/blender/blender/pulls/108494):
Fix \#108303: Added missing Auto-Masking label  

  - Commits: 2

[9735b8ee71](https://projects.blender.org/blender/blender/commit/9735b8ee719efac2c15c75064cc3889799acf616):
Fix \#106966: Register undo-step when unlinking data-block  
[b84eedbb74](https://projects.blender.org/blender/blender/commit/b84eedbb744fd0545d6b65bc7b4d12d097202906):
GPv3: Set default tool in draw mode  

## Week 107 (June 12 - 18)

This week: Triaging. General code contribution. First round of review.  
Next Week: Triaging.  
**Total actions on tracker: 116**

  - Confirmed: 13

[\#109043](https://projects.blender.org/blender/blender/issues/109043),
[\#109004](https://projects.blender.org/blender/blender/issues/109004),
[\#108992](https://projects.blender.org/blender/blender/issues/108992),
[\#108960](https://projects.blender.org/blender/blender/issues/108960),
[\#108935](https://projects.blender.org/blender/blender/issues/108935),
[\#108958](https://projects.blender.org/blender/blender/issues/108958),
[\#108916](https://projects.blender.org/blender/blender/issues/108916),
[\#108901](https://projects.blender.org/blender/blender/issues/108901),
[\#108907](https://projects.blender.org/blender/blender/issues/108907),
[\#108911](https://projects.blender.org/blender/blender/issues/108911),
[\#108914](https://projects.blender.org/blender/blender/issues/108914),
[\#108912](https://projects.blender.org/blender/blender/issues/108912),
[\#108840](https://projects.blender.org/blender/blender/issues/108840)

  - Closed: 15

[\#108846](https://projects.blender.org/blender/blender/issues/108846),
[\#104697](https://projects.blender.org/blender/blender-addons/issues/104697),
[\#109018](https://projects.blender.org/blender/blender/issues/109018),
[\#109035](https://projects.blender.org/blender/blender/issues/109035),
[\#108967](https://projects.blender.org/blender/blender/issues/108967),
[\#108908](https://projects.blender.org/blender/blender/issues/108908),
[\#108898](https://projects.blender.org/blender/blender/issues/108898),
[\#108936](https://projects.blender.org/blender/blender/issues/108936),
[\#108925](https://projects.blender.org/blender/blender/issues/108925),
[\#108891](https://projects.blender.org/blender/blender/issues/108891),
[\#108814](https://projects.blender.org/blender/blender/issues/108814),
[\#108885](https://projects.blender.org/blender/blender/issues/108885),
[\#108827](https://projects.blender.org/blender/blender/issues/108827),
[\#108873](https://projects.blender.org/blender/blender/issues/108873),
[\#107791](https://projects.blender.org/blender/blender/issues/107791)

  - Own Reported: 1

[\#108923](https://projects.blender.org/blender/blender/issues/108923)

  - PR submitted: 3

[PR
\#108974](https://projects.blender.org/blender/blender/pulls/108974):
Fix \#107918: Weight Gradient tool paints over locked vertex groups  
[PR
\#108876](https://projects.blender.org/blender/blender/pulls/108876):
Fix \#108814: Right Click doesn't cancel 3D View navigation  
[PR
\#107985](https://projects.blender.org/blender/blender/pulls/107985):
Animation: Set hardmin/max for frame start and end of action  

  - PR Review: 1

[PR
\#108685](https://projects.blender.org/blender/blender/pulls/108685):
Cleanup: Remove redundant variables in io\_collada.cc

  - Commits: 1

[dc7678ecfa](https://projects.blender.org/blender/blender/commit/dc7678ecfa1bd562c17feec56d61be6995d394db):
Fix \#108814: Right Click doesn't cancel 3D View navigation

## Week 108 (June 19 - 25)

This week: Triaging. General code contribution.  
Next Week: Triaging.  
**Total actions on tracker: 139**

  - Confirmed: 16

[\#109219](https://projects.blender.org/blender/blender/issues/109219),
[\#109271](https://projects.blender.org/blender/blender/issues/109271),
[\#107756](https://projects.blender.org/blender/blender/issues/107756),
[\#109218](https://projects.blender.org/blender/blender/issues/109218),
[\#109207](https://projects.blender.org/blender/blender/issues/109207),
[\#109191](https://projects.blender.org/blender/blender/issues/109191),
[\#109186](https://projects.blender.org/blender/blender/issues/109186),
[\#109134](https://projects.blender.org/blender/blender/issues/109134),
[\#109142](https://projects.blender.org/blender/blender/issues/109142),
[\#104706](https://projects.blender.org/blender/blender-addons/issues/104706),
[\#109072](https://projects.blender.org/blender/blender/issues/109072),
[\#109074](https://projects.blender.org/blender/blender/issues/109074),
[\#109080](https://projects.blender.org/blender/blender/issues/109080),
[\#109086](https://projects.blender.org/blender/blender/issues/109086),
[\#109112](https://projects.blender.org/blender/blender/issues/109112),
[\#109085](https://projects.blender.org/blender/blender/issues/109085)

  - Closed: 27

[\#108523](https://projects.blender.org/blender/blender/issues/108523),
[\#108441](https://projects.blender.org/blender/blender/issues/108441),
[\#108389](https://projects.blender.org/blender/blender/issues/108389),
[\#108918](https://projects.blender.org/blender/blender/issues/108918),
[\#108913](https://projects.blender.org/blender/blender/issues/108913),
[\#108828](https://projects.blender.org/blender/blender/issues/108828),
[\#108577](https://projects.blender.org/blender/blender/issues/108577),
[\#108515](https://projects.blender.org/blender/blender/issues/108515),
[\#108224](https://projects.blender.org/blender/blender/issues/108224),
[\#108011](https://projects.blender.org/blender/blender/issues/108011),
[\#107324](https://projects.blender.org/blender/blender/issues/107324),
[\#107119](https://projects.blender.org/blender/blender/issues/107119),
[\#109216](https://projects.blender.org/blender/blender/issues/109216),
[\#109211](https://projects.blender.org/blender/blender/issues/109211),
[\#109226](https://projects.blender.org/blender/blender/issues/109226),
[\#109215](https://projects.blender.org/blender/blender/issues/109215),
[\#108178](https://projects.blender.org/blender/blender/issues/108178),
[\#105268](https://projects.blender.org/blender/blender/issues/105268),
[\#109177](https://projects.blender.org/blender/blender/issues/109177),
[\#109183](https://projects.blender.org/blender/blender/issues/109183),
[\#109152](https://projects.blender.org/blender/blender/issues/109152),
[\#109079](https://projects.blender.org/blender/blender/issues/109079),
[\#109124](https://projects.blender.org/blender/blender/issues/109124),
[\#104700](https://projects.blender.org/blender/blender-addons/issues/104700),
[\#109089](https://projects.blender.org/blender/blender/issues/109089),
[\#109101](https://projects.blender.org/blender/blender/issues/109101),
[\#109082](https://projects.blender.org/blender/blender/issues/109082)

  - PR submitted: 3

[PR
\#109321](https://projects.blender.org/blender/blender/pulls/109321):
GPv3: Crash when drawing on new layers  
[PR
\#109238](https://projects.blender.org/blender/blender/pulls/109238):
Fix: change poll function for add and remove layer operators  
[PR
\#108790](https://projects.blender.org/blender/blender/pulls/108790):
Animation: Change channel selection key preferences in IC keymap  

  - Commits: 3

[51a5be8913](https://projects.blender.org/blender/blender/commit/51a5be891308e174e2bd8bc862bdde41545b08d1):
GPv3: Change poll function of select alternate operator  
[3d3569852f](https://projects.blender.org/blender/blender/commit/3d3569852feb14164868489fe21c1bf0daf2f7e5):
Animation: Change channel selection key preferences in IC keymap  
[c1d6f8b567](https://projects.blender.org/blender/blender/commit/c1d6f8b567174de2a66cc90cea78a3dd91df2b60):
Fix: change poll function for add and remove layer operators  

## Week 109 (June 26 - 02)

This week: Triaging. General code contribution. Took break on Friday.  
Next Week: Triaging.  
**Total actions on tracker: 116**

  - Confirmed: 7

[\#109490](https://projects.blender.org/blender/blender/issues/109490),
[\#109398](https://projects.blender.org/blender/blender/issues/109398),
[\#109432](https://projects.blender.org/blender/blender/issues/109432),
[\#104715](https://projects.blender.org/blender/blender/issues/104715),
[\#109040](https://projects.blender.org/blender/blender/issues/109040),
[\#109354](https://projects.blender.org/blender/blender/issues/109354),
[\#109340](https://projects.blender.org/blender/blender/issues/109340)

  - Closed: 18

[\#109536](https://projects.blender.org/blender/blender/issues/109536),
[\#109566](https://projects.blender.org/blender/blender/issues/109566),
[\#109535](https://projects.blender.org/blender/blender/issues/109535),
[\#109534](https://projects.blender.org/blender/blender/issues/109534),
[\#109533](https://projects.blender.org/blender/blender/issues/109533),
[\#109542](https://projects.blender.org/blender/blender/issues/109542),
[\#109499](https://projects.blender.org/blender/blender/issues/109499),
[\#109503](https://projects.blender.org/blender/blender/issues/109503),
[\#109489](https://projects.blender.org/blender/blender/issues/109489),
[\#109428](https://projects.blender.org/blender/blender/issues/109428),
[\#109469](https://projects.blender.org/blender/blender/issues/109469),
[\#109395](https://projects.blender.org/blender/blender/issues/109395),
[\#109347](https://projects.blender.org/blender/blender/issues/109347),
[\#109353](https://projects.blender.org/blender/blender/issues/109353),
[\#107322](https://projects.blender.org/blender/blender/issues/107322),
[\#109340](https://projects.blender.org/blender/blender/issues/109340),
[\#109317](https://projects.blender.org/blender/blender/issues/109317),
[\#109355](https://projects.blender.org/blender/blender/issues/109355)

  - PR submitted: 2

[PR
\#109321](https://projects.blender.org/blender/blender/pulls/109321):
GPv3: Crash when drawing on new layers  
[PR
\#109356](https://projects.blender.org/blender/blender/pulls/109356):
Fix \#109340: Failed to generate API docs due to the missing context key

  - Commits: 1

[d66e12104b](https://projects.blender.org/blender/blender/commit/d66e12104b5a9ea7daa7a69b08633e0fa5bf164d):
GPv3: Crash when drawing on new layers

  - Review: 1

[PR
\#109103](https://projects.blender.org/blender/blender/pulls/109103):
Fix \#108960 fix obsolete link in custom nodes template documentation

## Week 110 (July 03 - 09)

This week: Triaging. General code contribution. Devtalk moderation.  
Next Week: Triaging.  
**Total actions on tracker: 169**

  - Confirmed: 13

[\#109770](https://projects.blender.org/blender/blender/issues/109770),
[\#109787](https://projects.blender.org/blender/blender/issues/109787),
[\#109781](https://projects.blender.org/blender/blender/issues/109781),
[\#109796](https://projects.blender.org/blender/blender/issues/109796),
[\#104725](https://projects.blender.org/blender/blender-addons/issues/104725),
[\#109631](https://projects.blender.org/blender/blender/issues/109631),
[\#109629](https://projects.blender.org/blender/blender/issues/109629),
[\#108810](https://projects.blender.org/blender/blender/issues/108810),
[\#109659](https://projects.blender.org/blender/blender/issues/109659),
[\#109558](https://projects.blender.org/blender/blender/issues/109558),
[\#109570](https://projects.blender.org/blender/blender/issues/109570),
[\#109579](https://projects.blender.org/blender/blender/issues/109579),
[\#109590](https://projects.blender.org/blender/blender/issues/109590)

  - Closed: 26

[\#109814](https://projects.blender.org/blender/blender/issues/109814),
[\#93229](https://projects.blender.org/blender/blender/issues/93229),
[\#109809](https://projects.blender.org/blender/blender/issues/109809),
[\#109806](https://projects.blender.org/blender/blender/issues/109806),
[\#109796](https://projects.blender.org/blender/blender/issues/109796),
[\#107965](https://projects.blender.org/blender/blender/issues/107965),
[\#104728](https://projects.blender.org/blender/blender-addons/issues/104728),
[\#109749](https://projects.blender.org/blender/blender/issues/109749),
[\#109766](https://projects.blender.org/blender/blender/issues/109766),
[\#109668](https://projects.blender.org/blender/blender/issues/109668),
[\#105628](https://projects.blender.org/blender/blender/issues/105628),
[\#104719](https://projects.blender.org/blender/blender-addons/issues/104719),
[\#104733](https://projects.blender.org/blender/blender-addons/issues/104733),
[\#108115](https://projects.blender.org/blender/blender/issues/108115),
[\#109477](https://projects.blender.org/blender/blender/issues/109477),
[\#109601](https://projects.blender.org/blender/blender/issues/109601),
[\#109680](https://projects.blender.org/blender/blender/issues/109680),
[\#109675](https://projects.blender.org/blender/blender/issues/109675),
[\#109678](https://projects.blender.org/blender/blender/issues/109678),
[\#109663](https://projects.blender.org/blender/blender/issues/109663),
[\#108960](https://projects.blender.org/blender/blender/issues/108960),
[\#109592](https://projects.blender.org/blender/blender/issues/109592),
[\#109593](https://projects.blender.org/blender/blender/issues/109593),
[\#109597](https://projects.blender.org/blender/blender/issues/109597),
[\#109615](https://projects.blender.org/blender/blender/issues/109615),
[\#109342](https://projects.blender.org/blender/blender/issues/109342)

  - PR submitted: 5

[PR
\#109775](https://projects.blender.org/blender/blender/pulls/109775):
Animation: Fcurve Channel color band alignment  
[PR
\#109815](https://projects.blender.org/blender/blender/pulls/109815):
Fix \#109561: Overwrite existing imagepath based on relativepath
property  
[PR
\#108974](https://projects.blender.org/blender/blender/pulls/108974):
Fix \#107918: Weight Gradient tool paints over locked vertex groups  
[PR
\#109722](https://projects.blender.org/blender/blender/pulls/109722):
GPv3: Missing default tool for paint mode  
[PR
\#107985](https://projects.blender.org/blender/blender/pulls/107985):
Animation: Set hardmin/max for frame start and end of action  

  - Review: 3

[PR
\#109857](https://projects.blender.org/blender/blender/pulls/109857):
UI: Grey out ShapeKey list entry on mute  
[PR
\#109810](https://projects.blender.org/blender/blender/pulls/109810):
Fix \#109802: Outliner "Blender File" view cant delete scene  
[PR
\#109103](https://projects.blender.org/blender/blender/pulls/109103):
Fix \#108960: Replace archived link from custom nodes template  

  - Commits: 3

[b14435fd93](https://projects.blender.org/blender/blender/commit/b14435fd939f9d9c20af265633b19c86054aee27):
GPv3: Missing default tool for paint mode  
[e73273e24b](https://projects.blender.org/blender/blender/commit/e73273e24b0d814fccadbdea359d992f0643b7c4):
Animation: Set hardmin/max for frame start and end of action  
[0ade0959d4](https://projects.blender.org/blender/blender/commit/0ade0959d47cc4a77c4b8748ce90482fffaa8163):
Fix: Compiler error due to missing header  

## Week 111 (July 10 - 16)

This week: Triaging. General code contribution. Devtalk moderation.  
Next Week: Triaging.  
**Total actions on tracker: 163**

  - Confirmed: 23

[\#110002](https://projects.blender.org/blender/blender/issues/110002),
[\#110079](https://projects.blender.org/blender/blender/issues/110079),
[\#110085](https://projects.blender.org/blender/blender/issues/110085),
[\#110047](https://projects.blender.org/blender/blender/issues/110047),
[\#110038](https://projects.blender.org/blender/blender/issues/110038),
[\#110041](https://projects.blender.org/blender/blender/issues/110041),
[\#109962](https://projects.blender.org/blender/blender/issues/109962),
[\#109978](https://projects.blender.org/blender/blender/issues/109978),
[\#109986](https://projects.blender.org/blender/blender/issues/109986),
[\#109968](https://projects.blender.org/blender/blender/issues/109968),
[\#109945](https://projects.blender.org/blender/blender/issues/109945),
[\#109955](https://projects.blender.org/blender/blender/issues/109955),
[\#109898](https://projects.blender.org/blender/blender/issues/109898),
[\#109868](https://projects.blender.org/blender/blender/issues/109868),
[\#109943](https://projects.blender.org/blender/blender/issues/109943),
[\#109864](https://projects.blender.org/blender/blender/issues/109864),
[\#109799](https://projects.blender.org/blender/blender/issues/109799),
[\#109873](https://projects.blender.org/blender/blender/issues/109873),
[\#109869](https://projects.blender.org/blender/blender/issues/109869),
[\#109875](https://projects.blender.org/blender/blender/issues/109875),
[\#109886](https://projects.blender.org/blender/blender/issues/109886),
[\#109884](https://projects.blender.org/blender/blender/issues/109884),
[\#109885](https://projects.blender.org/blender/blender/issues/109885)

  - Closed: 18

[\#109428](https://projects.blender.org/blender/blender/issues/109428),
[\#110068](https://projects.blender.org/blender/blender/issues/110068),
[\#110033](https://projects.blender.org/blender/blender/issues/110033),
[\#106167](https://projects.blender.org/blender/blender/issues/106167),
[\#109993](https://projects.blender.org/blender/blender/issues/109993),
[\#110035](https://projects.blender.org/blender/blender/issues/110035),
[\#109955](https://projects.blender.org/blender/blender/issues/109955),
[\#109962](https://projects.blender.org/blender/blender/issues/109962),
[\#109974](https://projects.blender.org/blender/blender/issues/109974),
[\#104746](https://projects.blender.org/blender/blender-addons/issues/104746),
[\#109954](https://projects.blender.org/blender/blender/issues/109954),
[\#109933](https://projects.blender.org/blender/blender/issues/109933),
[\#109930](https://projects.blender.org/blender/blender/issues/109930),
[\#109931](https://projects.blender.org/blender/blender/issues/109931),
[\#109861](https://projects.blender.org/blender/blender/issues/109861),
[\#109867](https://projects.blender.org/blender/blender/issues/109867),
[\#109872](https://projects.blender.org/blender/blender/issues/109872),
[\#106360](https://projects.blender.org/blender/blender/issues/106360)

  - Own Reported: 2

[\#110096](https://projects.blender.org/blender/blender/issues/110096),
[\#109994](https://projects.blender.org/blender/blender/issues/109994)

  - PR submitted: 4

[PR
\#110133](https://projects.blender.org/blender/blender/pulls/110133):
GPv3: Basic layer channel selection  
[PR
\#110055](https://projects.blender.org/blender/blender/pulls/110055):
GPv3: Missing property updates after initial dopesheet support  
[PR
\#110009](https://projects.blender.org/blender/blender/pulls/110009):
Fix \#109994: Layer unlinks from the list when drop location is same  
[PR
\#109957](https://projects.blender.org/blender/blender/pulls/109957):
Fix \#109898: Incorrect behavior when moving objects to linked
collection  

  - Review: 3

[PR
\#109827](https://projects.blender.org/blender/blender/pulls/109827):
Walk Navigation: Controls to adjust jump height interactively  
[PR
\#109857](https://projects.blender.org/blender/blender/pulls/109857):
UI: Grey out ShapeKey list entry on mute  
[PR
\#109944](https://projects.blender.org/blender/blender/pulls/109944):
Fix \#109086: add center action safe margin configuration  

  - Commits: 3

[414a17ab4f](https://projects.blender.org/blender/blender/commit/414a17ab4fda59bdb04fb8f0038a1aa14d0f7860):
GPv3: Missing property updates after initial dopesheet support  
[861931101c](https://projects.blender.org/blender/blender/commit/861931101c934068a591c5ee8c48b7958418b932):
Fix \#109994: Layer unlinks from the list when drop location is same  
[53e1bae929](https://projects.blender.org/blender/blender/commit/53e1bae929014fda573b270bc88e7df9e3b20fb8):
Fix \#109898: Incorrect behavior when moving objects to linked
collection  

## Week 112 (July 17 - 23)

This week: Triaging. General code contribution. Devtalk moderation.  
Next Week: Triaging.  
**Total actions on tracker: 220**

  - Confirmed: 9

[\#110336](https://projects.blender.org/blender/blender/issues/110336),
[\#110147](https://projects.blender.org/blender/blender/issues/110147),
[\#110308](https://projects.blender.org/blender/blender/issues/110308),
[\#110286](https://projects.blender.org/blender/blender/issues/110286),
[\#110255](https://projects.blender.org/blender/blender/issues/110255),
[\#109599](https://projects.blender.org/blender/blender/issues/109599),
[\#109572](https://projects.blender.org/blender/blender/issues/109572),
[\#110004](https://projects.blender.org/blender/blender/issues/110004),
[\#110170](https://projects.blender.org/blender/blender/issues/110170)

  - Closed: 46

[\#110340](https://projects.blender.org/blender/blender/issues/110340),
[\#110288](https://projects.blender.org/blender/blender/issues/110288),
[\#110332](https://projects.blender.org/blender/blender/issues/110332),
[\#110318](https://projects.blender.org/blender/blender/issues/110318),
[\#99585](https://projects.blender.org/blender/blender/issues/99585),
[\#101693](https://projects.blender.org/blender/blender/issues/101693),
[\#101171](https://projects.blender.org/blender/blender/issues/101171),
[\#99330](https://projects.blender.org/blender/blender/issues/99330),
[\#97076](https://projects.blender.org/blender/blender/issues/97076),
[\#110250](https://projects.blender.org/blender/blender/issues/110250),
[\#93891](https://projects.blender.org/blender/blender/issues/93891),
[\#104759](https://projects.blender.org/blender/blender/issues/104759),
[\#104761](https://projects.blender.org/blender/blender/issues/104761),
[\#110257](https://projects.blender.org/blender/blender/issues/110257),
[\#110080](https://projects.blender.org/blender/blender/issues/110080),
[\#107276](https://projects.blender.org/blender/blender/issues/107276),
[\#107377](https://projects.blender.org/blender/blender/issues/107377),
[\#107940](https://projects.blender.org/blender/blender/issues/107940),
[\#108119](https://projects.blender.org/blender/blender/issues/108119),
[\#108167](https://projects.blender.org/blender/blender/issues/108167),
[\#108530](https://projects.blender.org/blender/blender/issues/108530),
[\#108850](https://projects.blender.org/blender/blender/issues/108850),
[\#108981](https://projects.blender.org/blender/blender/issues/108981),
[\#109002](https://projects.blender.org/blender/blender/issues/109002),
[\#109034](https://projects.blender.org/blender/blender/issues/109034),
[\#109055](https://projects.blender.org/blender/blender/issues/109055),
[\#109096](https://projects.blender.org/blender/blender/issues/109096),
[\#109129](https://projects.blender.org/blender/blender/issues/109129),
[\#109131](https://projects.blender.org/blender/blender/issues/109131),
[\#109140](https://projects.blender.org/blender/blender/issues/109140),
[\#109227](https://projects.blender.org/blender/blender/issues/109227),
[\#109230](https://projects.blender.org/blender/blender/issues/109230),
[\#109308](https://projects.blender.org/blender/blender/issues/109308),
[\#109391](https://projects.blender.org/blender/blender/issues/109391),
[\#109573](https://projects.blender.org/blender/blender/issues/109573),
[\#109578](https://projects.blender.org/blender/blender/issues/109578),
[\#109607](https://projects.blender.org/blender/blender/issues/109607),
[\#109637](https://projects.blender.org/blender/blender/issues/109637),
[\#109571](https://projects.blender.org/blender/blender/issues/109571),
[\#110207](https://projects.blender.org/blender/blender/issues/110207),
[\#106341](https://projects.blender.org/blender/blender/issues/106341),
[\#104745](https://projects.blender.org/blender/blender-addons/issues/104745),
[\#110094](https://projects.blender.org/blender/blender/issues/110094),
[\#109040](https://projects.blender.org/blender/blender/issues/109040),
[\#110192](https://projects.blender.org/blender/blender/issues/110192),
[\#110129](https://projects.blender.org/blender/blender/issues/110129)

  - PR submitted: 3

[PR
\#110256](https://projects.blender.org/blender/blender/pulls/110256):
Fix \#110253: Regression: Extruding bone from root fails  
[PR
\#110378](https://projects.blender.org/blender/blender/pulls/110378):
GPv3: RNA property to set active layer  
[PR
\#110133](https://projects.blender.org/blender/blender/pulls/110133):
GPv3: Basic layer channel selection  

  - Review: 3

[PR
\#109827](https://projects.blender.org/blender/blender/pulls/109827):
Walk Navigation: Controls to adjust jump height interactively  
[PR
\#104516](https://projects.blender.org/blender/blender-manual/pulls/104516):
Adds list of supported multi-touch gestures  
[PR
\#110203](https://projects.blender.org/blender/blender/pulls/110203):
Docs: Fix layout of bpy.types.Object Python API documents  

  - Commits: 1

[b08b6984d0](https://projects.blender.org/blender/blender/commit/b08b6984d0cc6991c109fc142e02076df312cec9):
Animation: Fcurve Channel color band alignment  

## Week 113 (July 24 - 30)

This week: Triaging. General code contribution. Devtalk moderation.  
Next Week: Triaging.  
**Total actions on tracker: 180**

  - Confirmed: 16

[\#110409](https://projects.blender.org/blender/blender/issues/110409),
[\#110562](https://projects.blender.org/blender/blender/issues/110562),
[\#110471](https://projects.blender.org/blender/blender/issues/110471),
[\#110549](https://projects.blender.org/blender/blender/issues/110549),
[\#110472](https://projects.blender.org/blender/blender/issues/110472),
[\#110543](https://projects.blender.org/blender/blender/issues/110543),
[\#110556](https://projects.blender.org/blender/blender/issues/110556),
[\#110497](https://projects.blender.org/blender/blender/issues/110497),
[\#110485](https://projects.blender.org/blender/blender/issues/110485),
[\#110407](https://projects.blender.org/blender/blender/issues/110407),
[\#110476](https://projects.blender.org/blender/blender/issues/110476),
[\#110467](https://projects.blender.org/blender/blender/issues/110467),
[\#110333](https://projects.blender.org/blender/blender/issues/110333),
[\#110447](https://projects.blender.org/blender/blender/issues/110447),
[\#110385](https://projects.blender.org/blender/blender/issues/110385),
[\#110323](https://projects.blender.org/blender/blender/issues/110323)

  - Closed: 35

[\#110563](https://projects.blender.org/blender/blender/issues/110563),
[\#110418](https://projects.blender.org/blender/blender/issues/110418),
[\#110542](https://projects.blender.org/blender/blender/issues/110542),
[\#110554](https://projects.blender.org/blender/blender/issues/110554),
[\#110497](https://projects.blender.org/blender/blender/issues/110497),
[\#107918](https://projects.blender.org/blender/blender/issues/107918),
[\#110528](https://projects.blender.org/blender/blender/issues/110528),
[\#110503](https://projects.blender.org/blender/blender/issues/110503),
[\#104765](https://projects.blender.org/blender/blender-addons/issues/104765),
[\#110494](https://projects.blender.org/blender/blender/issues/110494),
[\#110357](https://projects.blender.org/blender/blender/issues/110357),
[\#110153](https://projects.blender.org/blender/blender/issues/110153),
[\#104696](https://projects.blender.org/blender/blender-addons/issues/104696),
[\#110147](https://projects.blender.org/blender/blender/issues/110147),
[\#104727](https://projects.blender.org/blender/blender-addons/issues/104727),
[\#104732](https://projects.blender.org/blender/blender-addons/issues/104732),
[\#104734](https://projects.blender.org/blender/blender-addons/issues/104734),
[\#110463](https://projects.blender.org/blender/blender/issues/110463),
[\#110449](https://projects.blender.org/blender/blender/issues/110449),
[\#110451](https://projects.blender.org/blender/blender/issues/110451),
[\#110416](https://projects.blender.org/blender/blender/issues/110416),
[\#110446](https://projects.blender.org/blender/blender/issues/110446),
[\#110422](https://projects.blender.org/blender/blender/issues/110422),
[\#110253](https://projects.blender.org/blender/blender/issues/110253),
[\#106445](https://projects.blender.org/blender/blender/issues/106445),
[\#110401](https://projects.blender.org/blender/blender/issues/110401),
[\#109625](https://projects.blender.org/blender/blender/issues/109625),
[\#110385](https://projects.blender.org/blender/blender/issues/110385),
[\#93180](https://projects.blender.org/blender/blender/issues/93180),
[\#110380](https://projects.blender.org/blender/blender/issues/110380),
[\#110367](https://projects.blender.org/blender/blender/issues/110367),
[\#110406](https://projects.blender.org/blender/blender/issues/110406),
[\#110395](https://projects.blender.org/blender/blender/issues/110395),
[\#110396](https://projects.blender.org/blender/blender/issues/110396),
[\#110400](https://projects.blender.org/blender/blender/issues/110400)

  - PR submitted/updated: 5

[PR
\#110133](https://projects.blender.org/blender/blender/pulls/110133):
GPv3: Basic layer channel selection  
[PR
\#110517](https://projects.blender.org/blender/blender/pulls/110517):
Fix \#110497: Quick favorites can be duplicated  
[PR
\#110378](https://projects.blender.org/blender/blender/pulls/110378):
GPv3: send notifiers and undo push when changing the active layer  
[PR
\#110256](https://projects.blender.org/blender/blender/pulls/110256):
Fix \#110253: Regression: Extruding bone from root fails  
[PR
\#108974](https://projects.blender.org/blender/blender/pulls/108974):
Fix \#107918: Weight Gradient tool paints over locked vertex groups  

  - Review: 3

[PR
\#110402](https://projects.blender.org/blender/blender/pulls/110402):
Fix \#110085: Ignore Scroller Zone when on a Screen Edge  
[PR
\#109827](https://projects.blender.org/blender/blender/pulls/109827):
Walk Navigation: Controls to adjust jump height interactively  
[PR
\#110404](https://projects.blender.org/blender/blender/pulls/110404):
Fix \#109663: Allow Animation Keymap in Spreadsheet Editor  

  - Commits: 4

[6f5873de5f](https://projects.blender.org/blender/blender/commit/6f5873de5f93446d2fbc50b6380eb3d122e4a643):
Fix \#110497: Quick favorites can be duplicated  
[4c12801532](https://projects.blender.org/blender/blender/commit/4c128015324d8c98a12826a84d81a41ae81728b0):
GPv3: Send notifiers and undo push when changing the active layer  
[7cdb9814c5](https://projects.blender.org/blender/blender/commit/7cdb9814c5a44704f70f65051b8e12048b2fa727):
Fix \#110253: Regression: Extruding bone from root fails  
[6f0477bd1d](https://projects.blender.org/blender/blender/commit/6f0477bd1d669061ea4076f4ca547f22d093a9d6):
Fix \#107918: Weight Gradient tool paints over locked vertex groups  

## Week 114 (July 31 - 06)

This week: Triaging. General code contribution. Devtalk moderation.  
Next Week: Triaging.  
**Total actions on tracker: 211**

  - Confirmed: 20

[\#106105](https://projects.blender.org/blender/blender/issues/106105),
[\#110666](https://projects.blender.org/blender/blender/issues/110666),
[\#109907](https://projects.blender.org/blender/blender/issues/109907),
[\#109906](https://projects.blender.org/blender/blender/issues/109906),
[\#104704](https://projects.blender.org/blender/blender/issues/104704),
[\#88449](https://projects.blender.org/blender/blender/issues/88449),
[\#110723](https://projects.blender.org/blender/blender/issues/110723),
[\#110721](https://projects.blender.org/blender/blender/issues/110721),
[\#110736](https://projects.blender.org/blender/blender/issues/110736),
[\#110701](https://projects.blender.org/blender/blender/issues/110701),
[\#110672](https://projects.blender.org/blender/blender/issues/110672),
[\#110706](https://projects.blender.org/blender/blender/issues/110706),
[\#110711](https://projects.blender.org/blender/blender/issues/110711),
[\#104787](https://projects.blender.org/blender/blender/issues/104787),
[\#110724](https://projects.blender.org/blender/blender/issues/110724),
[\#110641](https://projects.blender.org/blender/blender/issues/110641),
[\#110716](https://projects.blender.org/blender/blender/issues/110716),
[\#110602](https://projects.blender.org/blender/blender/issues/110602),
[\#110662](https://projects.blender.org/blender/blender/issues/110662),
[\#110745](https://projects.blender.org/blender/blender/issues/110745)

  - Closed: 37

[\#110700](https://projects.blender.org/blender/blender/issues/110700),
[\#96057](https://projects.blender.org/blender/blender/issues/96057),
[\#89815](https://projects.blender.org/blender/blender/issues/89815),
[\#106105](https://projects.blender.org/blender/blender/issues/106105),
[\#109800](https://projects.blender.org/blender/blender/issues/109800),
[\#110737](https://projects.blender.org/blender/blender/issues/110737),
[\#95971](https://projects.blender.org/blender/blender/issues/95971),
[\#109863](https://projects.blender.org/blender/blender/issues/109863),
[\#110748](https://projects.blender.org/blender/blender/issues/110748),
[\#88924](https://projects.blender.org/blender/blender/issues/88924),
[\#90642](https://projects.blender.org/blender/blender/issues/90642),
[\#110673](https://projects.blender.org/blender/blender/issues/110673),
[\#110726](https://projects.blender.org/blender/blender/issues/110726),
[\#110739](https://projects.blender.org/blender/blender/issues/110739),
[\#88449](https://projects.blender.org/blender/blender/issues/88449),
[\#85373](https://projects.blender.org/blender/blender/issues/85373),
[\#104807](https://projects.blender.org/blender/blender-addons/issues/104807),
[\#104816](https://projects.blender.org/blender/blender-addons/issues/104816),
[\#89521](https://projects.blender.org/blender/blender/issues/89521),
[\#109960](https://projects.blender.org/blender/blender/issues/109960),
[\#109880](https://projects.blender.org/blender/blender/issues/109880),
[\#109611](https://projects.blender.org/blender/blender/issues/109611),
[\#110710](https://projects.blender.org/blender/blender/issues/110710),
[\#110602](https://projects.blender.org/blender/blender/issues/110602),
[\#110662](https://projects.blender.org/blender/blender/issues/110662),
[\#104787](https://projects.blender.org/blender/blender/issues/104787),
[\#110658](https://projects.blender.org/blender/blender/issues/110658),
[\#109544](https://projects.blender.org/blender/blender/issues/109544),
[\#110739](https://projects.blender.org/blender/blender/issues/110739),
[\#110672](https://projects.blender.org/blender/blender/issues/110672),
[\#110710](https://projects.blender.org/blender/blender/issues/110710),
[\#110658](https://projects.blender.org/blender/blender/issues/110658)

  - PR submitted: 4

[PR
\#110753](https://projects.blender.org/blender/blender/pulls/110753):
GPv3: assert due to freeing nullptr  
[PR
\#110791](https://projects.blender.org/blender/blender/pulls/110791):
GPv3: Include other channel selection modes  
[PR
\#110133](https://projects.blender.org/blender/blender/pulls/110133):
GPv3: Basic layer channel selection  
[PR
\#110715](https://projects.blender.org/blender/blender/pulls/110715):
Fix \#110694: Edit bones draw as they were in pose mode  

  - Review: 2

[PR
\#104810](https://projects.blender.org/blender/blender-addons/pulls/104810):
Extra objects: Fix TypeError when torus knot added  
[PR
\#110619](https://projects.blender.org/blender/blender/pulls/110619):
Fix \#110096: Crash Crash on calling curve pen operator  

  - Commits: 2

[1cf4bc2719](https://projects.blender.org/blender/blender/commit/1cf4bc2719e82724115f548bb597d7fd9cfc9ee1):
Fix \#110694: Edit bones draw as they were in pose mode  
[56832ed59a](https://projects.blender.org/blender/blender/commit/56832ed59aefa3bc4bd499d5302c953b0b99d050):
GPv3: Basic layer channel selection  

## Week 115 (Aug 07 - 13)

This week: Triaging. General code contribution. Devtalk moderation.  
Next Week: Triaging.  
**Total actions on tracker: 172**

  - Confirmed: 25

[\#104866](https://projects.blender.org/blender/blender/issues/104866),
[\#110921](https://projects.blender.org/blender/blender/issues/110921),
[\#110966](https://projects.blender.org/blender/blender/issues/110966),
[\#104820](https://projects.blender.org/blender/blender-addons/issues/104820),
[\#104690](https://projects.blender.org/blender/blender-addons/issues/104690),
[\#104723](https://projects.blender.org/blender/blender-addons/issues/104723),
[\#104772](https://projects.blender.org/blender/blender-addons/issues/104772),
[\#104743](https://projects.blender.org/blender/blender-addons/issues/104743),
[\#110861](https://projects.blender.org/blender/blender/issues/110861),
[\#110976](https://projects.blender.org/blender/blender/issues/110976),
[\#110977](https://projects.blender.org/blender/blender/issues/110977),
[\#104712](https://projects.blender.org/blender/blender-addons/issues/104712),
[\#104773](https://projects.blender.org/blender/blender-addons/issues/104773),
[\#104691](https://projects.blender.org/blender/blender-addons/issues/104691),
[\#110849](https://projects.blender.org/blender/blender/issues/110849),
[\#104212](https://projects.blender.org/blender/blender/issues/104212),
[\#110879](https://projects.blender.org/blender/blender/issues/110879),
[\#110968](https://projects.blender.org/blender/blender/issues/110968),
[\#111024](https://projects.blender.org/blender/blender/issues/111024),
[\#104714](https://projects.blender.org/blender/blender-addons/issues/104714),
[\#110863](https://projects.blender.org/blender/blender/issues/110863),
[\#104771](https://projects.blender.org/blender/blender-addons/issues/104771),
[\#110874](https://projects.blender.org/blender/blender/issues/110874),
[\#110871](https://projects.blender.org/blender/blender/issues/110871)

  - Closed: 24

[\#106356](https://projects.blender.org/blender/blender/issues/106356),
[\#104742](https://projects.blender.org/blender/blender-addons/issues/104742),
[\#104827](https://projects.blender.org/blender/blender-addons/issues/104827),
[\#103698](https://projects.blender.org/blender/blender-addons/issues/103698),
[\#110866](https://projects.blender.org/blender/blender/issues/110866),
[\#111024](https://projects.blender.org/blender/blender/issues/111024),
[\#111028](https://projects.blender.org/blender/blender/issues/111028),
[\#110902](https://projects.blender.org/blender/blender/issues/110902),
[\#104705](https://projects.blender.org/blender/blender-addons/issues/104705),
[\#104674](https://projects.blender.org/blender/blender-addons/issues/104674),
[\#110969](https://projects.blender.org/blender/blender/issues/110969),
[\#104678](https://projects.blender.org/blender/blender-addons/issues/104678),
[\#110873](https://projects.blender.org/blender/blender/issues/110873),
[\#106514](https://projects.blender.org/blender/blender/issues/106514),
[\#110876](https://projects.blender.org/blender/blender/issues/110876),
[\#104757](https://projects.blender.org/blender/blender-addons/issues/104757),
[\#107105](https://projects.blender.org/blender/blender/issues/107105),
[\#111034](https://projects.blender.org/blender/blender/issues/111034),
[\#104754](https://projects.blender.org/blender/blender-addons/issues/104754),
[\#104826](https://projects.blender.org/blender/blender-addons/issues/104826),
[\#104723](https://projects.blender.org/blender/blender-addons/issues/104723),
[\#110870](https://projects.blender.org/blender/blender/issues/110870),
[\#110969](https://projects.blender.org/blender/blender/issues/110969),
[\#111024](https://projects.blender.org/blender/blender/issues/111024)

  - PR submitted: 4

[PR
\#111009](https://projects.blender.org/blender/blender/pulls/111009):
WIP: GPv3: Move channels  
[PR
\#111036](https://projects.blender.org/blender/blender/pulls/111036):
Fix \#111024: Crash when adding attribute to curve object  
[PR
\#110791](https://projects.blender.org/blender/blender/pulls/110791):
GPv3: Include other channel selection modes  
[PR
\#110890](https://projects.blender.org/blender/blender/pulls/110890):
GPv3: New is\_layer\_active function  

  - Review: 2

[PR
\#110937](https://projects.blender.org/blender/blender/pulls/110937):
WIP: Fix \#109968: Win32 Mouse Warp Bounds  
[PR
\#110938](https://projects.blender.org/blender/blender/pulls/110938):
GPv3: Frame delete operator (edit mode)  

  - Commits: 3

[d87db8d569](https://projects.blender.org/blender/blender/commit/d87db8d56936a8f703e88c0c27fd5233629f39e2):
Fix \#111024: Crash when adding attribute to curve object  
[ffafc183ba](https://projects.blender.org/blender/blender/commit/ffafc183bac4332da65bf7023f1333abcf2ff81f):
GPv3: Include other channel selection modes  
[6e66125f34](https://projects.blender.org/blender/blender/commit/6e66125f3440a99945434f6adc992a8bbd9b1d89):
GPv3: New is\_layer\_active function  

## Week 116 (Aug 14 - 20)

This week: Triaging. General code contribution. Devtalk moderation.  
Inactive on Tuesday (National Holiday: Independence day).  
Next Week: Triaging.  
**Total actions on tracker: 158**

  - Confirmed: 8

[\#111212](https://projects.blender.org/blender/blender/issues/111212),
[\#111098](https://projects.blender.org/blender/blender/issues/111098),
[\#111174](https://projects.blender.org/blender/blender/issues/111174),
[\#109739](https://projects.blender.org/blender/blender/issues/109739),
[\#111244](https://projects.blender.org/blender/blender/issues/111244),
[\#111244](https://projects.blender.org/blender/blender/issues/111244),
[\#111200](https://projects.blender.org/blender/blender/issues/111200),
[\#111086](https://projects.blender.org/blender/blender/issues/111086)

  - Closed: 24

[\#89479](https://projects.blender.org/blender/blender/issues/89479),
[\#111206](https://projects.blender.org/blender/blender/issues/111206),
[\#110286](https://projects.blender.org/blender/blender/issues/110286),
[\#111164](https://projects.blender.org/blender/blender/issues/111164),
[\#111057](https://projects.blender.org/blender/blender/issues/111057),
[\#111287](https://projects.blender.org/blender/blender/issues/111287),
[\#111088](https://projects.blender.org/blender/blender/issues/111088),
[\#111241](https://projects.blender.org/blender/blender/issues/111241),
[\#111159](https://projects.blender.org/blender/blender/issues/111159),
[\#111205](https://projects.blender.org/blender/blender/issues/111205),
[\#110808](https://projects.blender.org/blender/blender/issues/110808),
[\#111236](https://projects.blender.org/blender/blender/issues/111236),
[\#111102](https://projects.blender.org/blender/blender/issues/111102),
[\#111028](https://projects.blender.org/blender/blender/issues/111028),
[\#107822](https://projects.blender.org/blender/blender/issues/107822),
[\#107980](https://projects.blender.org/blender/blender/issues/107980),
[\#111208](https://projects.blender.org/blender/blender/issues/111208),
[\#110770](https://projects.blender.org/blender/blender/issues/110770),
[\#107825](https://projects.blender.org/blender/blender/issues/107825),
[\#107921](https://projects.blender.org/blender/blender/issues/107921),
[\#108024](https://projects.blender.org/blender/blender/issues/108024),
[\#111223](https://projects.blender.org/blender/blender/issues/111223),
[\#111241](https://projects.blender.org/blender/blender/issues/111241),
[\#111208](https://projects.blender.org/blender/blender/issues/111208)

  - Needs Information from User: 20

<!-- end list -->

  - PR submitted/updated: 4

[PR
\#111259](https://projects.blender.org/blender/blender/pulls/111259):
Fix \#108539: Bone Group color changes to RGB colorset  
[PR
\#111009](https://projects.blender.org/blender/blender/pulls/111009):
GPv3: Move channels  
[PR
\#104737](https://projects.blender.org/blender/blender/pulls/104737):
Fix \#89479: Unable to select hierarchies of multiple objects from
outliner  
[PR
\#111173](https://projects.blender.org/blender/blender/pulls/111173):
Fix: GPv3 layer renaming when name is unchanged  

  - Review: 2

[PR
\#110151](https://projects.blender.org/blender/blender/pulls/110151):
Outliner: Double-click on item icon to select contents/hierarchy  
[PR
\#110930](https://projects.blender.org/blender/blender/pulls/110930):
Fix \#110848: Trackpad: Allow changing the speed while fly navigating  

  - Commits: 2

[594dceda7f](https://projects.blender.org/blender/blender/commit/594dceda7fc54f315abc21177a6e1ce81242d31f):
Fix \#89479: Unable to select hierarchies of multiple objects from
outliner  
[338e4ef090](https://projects.blender.org/blender/blender/commit/338e4ef090b9c8c2d2f28473f4a553a3557c9eaf):
Fix: GPv3 layer renaming when name is unchanged  

## Week 117 (Aug 21 - 27)

This week: Triaging. General code contribution. Devtalk moderation.  
Checked/reviewed a few open PRs.  
Next Week: Triaging.  
**Total actions on tracker: 187**

  - Confirmed: 16

[\#110674](https://projects.blender.org/blender/blender/issues/110674),
[\#111499](https://projects.blender.org/blender/blender/issues/111499),
[\#111423](https://projects.blender.org/blender/blender/issues/111423),
[\#111447](https://projects.blender.org/blender/blender/issues/111447),
[\#111420](https://projects.blender.org/blender/blender/issues/111420),
[\#110998](https://projects.blender.org/blender/blender/issues/110998),
[\#111450](https://projects.blender.org/blender/blender/issues/111450),
[\#111453](https://projects.blender.org/blender/blender/issues/111453),
[\#104838](https://projects.blender.org/blender/blender-addons/issues/104838),
[\#111399](https://projects.blender.org/blender/blender/issues/111399),
[\#104711](https://projects.blender.org/blender/blender-addons/issues/104711),
[\#111300](https://projects.blender.org/blender/blender/issues/111300),
[\#110516](https://projects.blender.org/blender/blender/issues/110516),
[\#111434](https://projects.blender.org/blender/blender/issues/111434),
[\#111447](https://projects.blender.org/blender/blender/issues/111447),
[\#111244](https://projects.blender.org/blender/blender/issues/111244)

  - Closed: 22

[\#104838](https://projects.blender.org/blender/blender-addons/issues/104838),
[\#111372](https://projects.blender.org/blender/blender/issues/111372),
[\#110860](https://projects.blender.org/blender/blender/issues/110860),
[\#111499](https://projects.blender.org/blender/blender/issues/111499),
[\#96719](https://projects.blender.org/blender/blender/issues/96719),
[\#110740](https://projects.blender.org/blender/blender/issues/110740),
[\#111244](https://projects.blender.org/blender/blender/issues/111244),
[\#111008](https://projects.blender.org/blender/blender/issues/111008),
[\#111335](https://projects.blender.org/blender/blender/issues/111335),
[\#111279](https://projects.blender.org/blender/blender/issues/111279),
[\#111327](https://projects.blender.org/blender/blender/issues/111327),
[\#111460](https://projects.blender.org/blender/blender/issues/111460),
[\#104838](https://projects.blender.org/blender/blender-addons/issues/104838),
[\#109080](https://projects.blender.org/blender/blender/issues/109080),
[\#110841](https://projects.blender.org/blender/blender/issues/110841),
[\#111361](https://projects.blender.org/blender/blender/issues/111361),
[\#111365](https://projects.blender.org/blender/blender/issues/111365),
[\#110782](https://projects.blender.org/blender/blender/issues/110782),
[\#111543](https://projects.blender.org/blender/blender/issues/111543),
[\#111003](https://projects.blender.org/blender/blender/issues/111003),
[\#111300](https://projects.blender.org/blender/blender/issues/111300),
[\#111417](https://projects.blender.org/blender/blender/issues/111417)

  - Own Reported: 1

[\#111466](https://projects.blender.org/blender/blender/issues/111466)

  - Needs Information from User: 13

<!-- end list -->

  - PR submitted/updated: 5

[PR
\#111519](https://projects.blender.org/blender/blender/pulls/111519):
GPv3: Send updates after drop operation  
[PR
\#111377](https://projects.blender.org/blender/blender/pulls/111377):
Fix: Regression: Box select not working for GP/GPv3 channels  
[PR
\#111426](https://projects.blender.org/blender/blender/pulls/111426):
GPv3: Delete channels  
[PR
\#111509](https://projects.blender.org/blender/blender/pulls/111509):
Cleanup: Comment style  
[PR
\#109815](https://projects.blender.org/blender/blender/pulls/109815):
Fix \#109561: Overwrite existing imagepath based on relativepath
property  

  - Review: 2

[PR
\#110151](https://projects.blender.org/blender/blender/pulls/110151):
Outliner: Double-click on item icon to select contents/hierarchy  
[PR
\#111079](https://projects.blender.org/blender/blender/pulls/111079):
GPv3: Dissolve Operator  

  - Commits: 3

[4462100cee](https://projects.blender.org/blender/blender/commit/4462100ceed78301b43dae2f75cd459b8225614a):
GPv3: Delete channels  
[6ef40a3e9f](https://projects.blender.org/blender/blender/commit/6ef40a3e9f954665b1038f68c5bc7a186bb8a71e):
Fix: GPv3: Editable channels filtering was inverted for grease pencil
channels (Co-authored)  
[faff3d1d15](https://projects.blender.org/blender/blender/commit/faff3d1d152157054ebb4b9f27ef22549930621b):
Cleanup: Comment style  

## Week 118 (Aug 28 - 03)

This week: Triaging. General code contribution. Devtalk moderation.  
Next Week: Triaging.  
**Total actions on tracker: 258**

  - Confirmed: 10

[\#111532](https://projects.blender.org/blender/blender/issues/111532),
[\#111545](https://projects.blender.org/blender/blender/issues/111545),
[\#111574](https://projects.blender.org/blender/blender/issues/111574),
[\#111648](https://projects.blender.org/blender/blender/issues/111648),
[\#111595](https://projects.blender.org/blender/blender/issues/111595),
[\#111639](https://projects.blender.org/blender/blender/issues/111639),
[\#111721](https://projects.blender.org/blender/blender/issues/111721),
[\#111533](https://projects.blender.org/blender/blender/issues/111533),
[\#111517](https://projects.blender.org/blender/blender/issues/111517),
[\#111644](https://projects.blender.org/blender/blender/issues/111644)

  - Closed: 28

[\#111545](https://projects.blender.org/blender/blender/issues/111545),
[\#109561](https://projects.blender.org/blender/blender/issues/109561),
[\#109243](https://projects.blender.org/blender/blender/issues/109243),
[\#108875](https://projects.blender.org/blender/blender/issues/108875),
[\#109367](https://projects.blender.org/blender/blender/issues/109367),
[\#109843](https://projects.blender.org/blender/blender/issues/109843),
[\#111545](https://projects.blender.org/blender/blender/issues/111545),
[\#108945](https://projects.blender.org/blender/blender/issues/108945),
[\#110087](https://projects.blender.org/blender/blender/issues/110087),
[\#109561](https://projects.blender.org/blender/blender/issues/109561),
[\#109997](https://projects.blender.org/blender/blender/issues/109997),
[\#109762](https://projects.blender.org/blender/blender/issues/109762),
[\#109261](https://projects.blender.org/blender/blender/issues/109261),
[\#109279](https://projects.blender.org/blender/blender/issues/109279),
[\#111627](https://projects.blender.org/blender/blender/issues/111627),
[\#111539](https://projects.blender.org/blender/blender/issues/111539),
[\#110054](https://projects.blender.org/blender/blender/issues/110054),
[\#111598](https://projects.blender.org/blender/blender/issues/111598),
[\#111089](https://projects.blender.org/blender/blender/issues/111089),
[\#110134](https://projects.blender.org/blender/blender/issues/110134),
[\#108856](https://projects.blender.org/blender/blender/issues/108856),
[\#111731](https://projects.blender.org/blender/blender/issues/111731),
[\#109817](https://projects.blender.org/blender/blender/issues/109817),
[\#110026](https://projects.blender.org/blender/blender/issues/110026),
[\#109652](https://projects.blender.org/blender/blender/issues/109652),
[\#109616](https://projects.blender.org/blender/blender/issues/109616),
[\#111583](https://projects.blender.org/blender/blender/issues/111583),
[\#109839](https://projects.blender.org/blender/blender/issues/109839)

  - Needs Information from User: 13

<!-- end list -->

  - PR submitted/updated: 5

[PR
\#109815](https://projects.blender.org/blender/blender/pulls/109815):
Fix \#109561: Overwrite existing imagepath based on relativepath
property  
[PR
\#111692](https://projects.blender.org/blender/blender/pulls/111692):
Fix \#111613: Gesture selection does not deselect when elements hidden  
[PR
\#111723](https://projects.blender.org/blender/blender/pulls/111723):
Fix: Bone collection renaming when empty string  
[PR
\#111733](https://projects.blender.org/blender/blender/pulls/111733):
Bone collection: Set first collection active  
[PR
\#111646](https://projects.blender.org/blender/blender/pulls/111646):
Fix \#111545: Regression: Crash when drawing all view layers in
outliner  

  - Review: 0

<!-- end list -->

  - Commits: 5

[3d798e8486](https://projects.blender.org/blender/blender/commit/3d798e84862b535ea1fd5bf8d6b5ad72fcc0d7d6):
Cleanup: Remove unused parameter  
[1c0a1ae801](https://projects.blender.org/blender/blender/commit/1c0a1ae801a0d09a2c2ea1f863e7d941f0edeace):
Fix \#109561: Overwrite existing imagepath based on relativepath
property  
[c75d6ae0ed](https://projects.blender.org/blender/blender/commit/c75d6ae0ed5aee0eeee34dad75e80d320631778a):
Fix: Bone collection renaming when empty string  
[daa4c0f91e](https://projects.blender.org/blender/blender/commit/daa4c0f91e8c69e1e028af36db01841ff284b6ea):
Fix: set default bone collection active  
[1eea5ac82a](https://projects.blender.org/blender/blender/commit/1eea5ac82a7110217a31385ee6cf38ebfbbbe592):
Fix \#111545: Regression: Crash when drawing all view layers in
outliner  

## Week 119 (Sep 04 - 10)

This week: Triaging. General code contribution. Devtalk moderation.  
Next Week: Triaging.  
**Total actions on tracker: 184**

  - Confirmed: 27

[\#111963](https://projects.blender.org/blender/blender/issues/111963),
[\#111978](https://projects.blender.org/blender/blender/issues/111978),
[\#112011](https://projects.blender.org/blender/blender/issues/112011),
[\#112087](https://projects.blender.org/blender/blender/issues/112087),
[\#112075](https://projects.blender.org/blender/blender/issues/112075),
[\#111821](https://projects.blender.org/blender/blender/issues/111821),
[\#112021](https://projects.blender.org/blender/blender/issues/112021),
[\#111832](https://projects.blender.org/blender/blender/issues/111832),
[\#109903](https://projects.blender.org/blender/blender/issues/109903),
[\#112081](https://projects.blender.org/blender/blender/issues/112081),
[\#111840](https://projects.blender.org/blender/blender/issues/111840),
[\#112077](https://projects.blender.org/blender/blender/issues/112077),
[\#112061](https://projects.blender.org/blender/blender/issues/112061),
[\#112122](https://projects.blender.org/blender/blender/issues/112122),
[\#112103](https://projects.blender.org/blender/blender/issues/112103),
[\#111907](https://projects.blender.org/blender/blender/issues/111907),
[\#112014](https://projects.blender.org/blender/blender/issues/112014),
[\#111706](https://projects.blender.org/blender/blender/issues/111706),
[\#111777](https://projects.blender.org/blender/blender/issues/111777),
[\#107921](https://projects.blender.org/blender/blender/issues/107921),
[\#111910](https://projects.blender.org/blender/blender/issues/111910),
[\#112120](https://projects.blender.org/blender/blender/issues/112120),
[\#111960](https://projects.blender.org/blender/blender/issues/111960),
[\#112052](https://projects.blender.org/blender/blender/issues/112052),
[\#111848](https://projects.blender.org/blender/blender/issues/111848),
[\#111432](https://projects.blender.org/blender/blender/issues/111432)

  - Closed: 22

[\#111432](https://projects.blender.org/blender/blender/issues/111432),
[\#112178](https://projects.blender.org/blender/blender/issues/112178),
[\#111674](https://projects.blender.org/blender/blender/issues/111674),
[\#104853](https://projects.blender.org/blender/blender-addons/issues/104853),
[\#111358](https://projects.blender.org/blender/blender/issues/111358),
[\#112106](https://projects.blender.org/blender/blender/issues/112106),
[\#112133](https://projects.blender.org/blender/blender/issues/112133),
[\#111953](https://projects.blender.org/blender/blender/issues/111953),
[\#112080](https://projects.blender.org/blender/blender/issues/112080),
[\#111937](https://projects.blender.org/blender/blender/issues/111937),
[\#111922](https://projects.blender.org/blender/blender/issues/111922),
[\#112079](https://projects.blender.org/blender/blender/issues/112079),
[\#112008](https://projects.blender.org/blender/blender/issues/112008),
[\#104857](https://projects.blender.org/blender/blender-addons/issues/104857),
[\#104874](https://projects.blender.org/blender/blender-addons/issues/104874),
[\#112123](https://projects.blender.org/blender/blender/issues/112123),
[\#112118](https://projects.blender.org/blender/blender/issues/112118),
[\#111952](https://projects.blender.org/blender/blender/issues/111952),
[\#111961](https://projects.blender.org/blender/blender/issues/111961),
[\#111432](https://projects.blender.org/blender/blender/issues/111432),
[\#111849](https://projects.blender.org/blender/blender/issues/111849),
[\#112098](https://projects.blender.org/blender/blender/issues/112098),
[\#112120](https://projects.blender.org/blender/blender/issues/112120)

  - Needs Information from User: 21

<!-- end list -->

  - PR submitted: 3

[PR
\#112006](https://projects.blender.org/blender/blender/pulls/112006):
Fix \#111432: Shortcut for cycling fallback tool not working  
[PR
\#111954](https://projects.blender.org/blender/blender/pulls/111954):
Fix \#111896: Regression: Crash for entering into Armature Edit Mode  
[PR
\#111914](https://projects.blender.org/blender/blender/pulls/111914):
Fix \#111821: Sculpt Mode: Scene Statistics showing wrong results  

  - Commits: 2

[9f4f50ef70](https://projects.blender.org/blender/blender/commit/9f4f50ef70f340f99cd554ea209c2652001577d7):
Fix \#111432: Shortcut for cycling fallback tool not working  
[20b3e62e93](https://projects.blender.org/blender/blender/commit/20b3e62e932367bbb07d185af3fe2e1315cde622):
Fix \#111896: Regression: Crash for entering into Armature Edit Mode  

  - Review: 3

[PR
\#112127](https://projects.blender.org/blender/blender/pulls/112127):
Python API: Fix docstring about bpy.utils.load\_scripts  
[PR
\#111900](https://projects.blender.org/blender/blender/pulls/111900):
Text Editor: Auto Close Pairs on Selected Text  
[PR
\#111107](https://projects.blender.org/blender/blender/pulls/111107):
Python API: Fix docstring from method to classmethod  

## Week 120 (Sep 11 - 17)

This week: Triaging. General code contribution. Devtalk moderation.  
Next Week: Triaging.  
**Total actions on tracker: 191**

  - Confirmed: 15

[\#112146](https://projects.blender.org/blender/blender/issues/112146),
[\#112145](https://projects.blender.org/blender/blender/issues/112145),
[\#97524](https://projects.blender.org/blender/blender/issues/97524),
[\#112240](https://projects.blender.org/blender/blender/issues/112240),
[\#112312](https://projects.blender.org/blender/blender/issues/112312),
[\#112320](https://projects.blender.org/blender/blender/issues/112320),
[\#112183](https://projects.blender.org/blender/blender/issues/112183),
[\#112262](https://projects.blender.org/blender/blender/issues/112262),
[\#112094](https://projects.blender.org/blender/blender/issues/112094),
[\#112276](https://projects.blender.org/blender/blender/issues/112276),
[\#112357](https://projects.blender.org/blender/blender/issues/112357),
[\#112201](https://projects.blender.org/blender/blender/issues/112201),
[\#112353](https://projects.blender.org/blender/blender/issues/112353),
[\#111537](https://projects.blender.org/blender/blender/issues/111537),
[\#112258](https://projects.blender.org/blender/blender/issues/112258)

  - Closed: 27

[\#101550](https://projects.blender.org/blender/blender/issues/101550),
[\#112248](https://projects.blender.org/blender/blender/issues/112248),
[\#112315](https://projects.blender.org/blender/blender/issues/112315),
[\#111398](https://projects.blender.org/blender/blender/issues/111398),
[\#111503](https://projects.blender.org/blender/blender/issues/111503),
[\#109400](https://projects.blender.org/blender/blender/issues/109400),
[\#111705](https://projects.blender.org/blender/blender/issues/111705),
[\#112279](https://projects.blender.org/blender/blender/issues/112279),
[\#112317](https://projects.blender.org/blender/blender/issues/112317),
[\#111777](https://projects.blender.org/blender/blender/issues/111777),
[\#101550](https://projects.blender.org/blender/blender/issues/101550),
[\#112362](https://projects.blender.org/blender/blender/issues/112362),
[\#111237](https://projects.blender.org/blender/blender/issues/111237),
[\#111680](https://projects.blender.org/blender/blender/issues/111680),
[\#111597](https://projects.blender.org/blender/blender/issues/111597),
[\#112321](https://projects.blender.org/blender/blender/issues/112321),
[\#112260](https://projects.blender.org/blender/blender/issues/112260),
[\#112216](https://projects.blender.org/blender/blender/issues/112216),
[\#112364](https://projects.blender.org/blender/blender/issues/112364),
[\#112358](https://projects.blender.org/blender/blender/issues/112358),
[\#112265](https://projects.blender.org/blender/blender/issues/112265),
[\#111374](https://projects.blender.org/blender/blender/issues/111374),
[\#112318](https://projects.blender.org/blender/blender/issues/112318),
[\#112276](https://projects.blender.org/blender/blender/issues/112276),
[\#112036](https://projects.blender.org/blender/blender/issues/112036),
[\#112319](https://projects.blender.org/blender/blender/issues/112319),
[\#112356](https://projects.blender.org/blender/blender/issues/112356)

  - Own Reported: 1

[\#112402](https://projects.blender.org/blender/blender/issues/112402)

  - Needs Information from User: 25

<!-- end list -->

  - PR submitted/updated: 4

[PR
\#112370](https://projects.blender.org/blender/blender/pulls/112370):
Fix \#101550: Transfer Mode operator doesn't update outliner selection  
[PR
\#112380](https://projects.blender.org/blender/blender/pulls/112380):
Fix \#112122: Hover color matches with disabled buttons  
[PR
\#111519](https://projects.blender.org/blender/blender/pulls/111519):
GPv3: Send updates after drop operation  
[PR
\#111377](https://projects.blender.org/blender/blender/pulls/111377):
Fix: Regression: Box select not working for GP/GPv3 channels  

  - Review: 1

[PR
\#110151](https://projects.blender.org/blender/blender/pulls/110151):
Outliner: Double-click on item icon to select contents/hierarchy

  - Commits: 4

[59e8be7aa4](https://projects.blender.org/blender/blender/commit/59e8be7aa4e2fc6b887e8e616c3433d6ac46e66f):
Fix \#101550: Transfer Mode operator doesn't update outliner selection  
[0b8c83d273](https://projects.blender.org/blender/blender/commit/0b8c83d273cd85e46950be6b3b8a932670583a9b):
GPv3: Send updates after drop operation  
[a370acae0c](https://projects.blender.org/blender/blender/commit/a370acae0cc550c8ebf60560be07492767411879):
Fix: Regression: Box select not working for GP/GPv3 channels  
[b66c0676b4](https://projects.blender.org/blender/blender/commit/b66c0676b4d317a4ef3833cf72c1e7e76e17f72b):
GPv3: Move dopesheet channels (co-authored)  

## Week 121 (Sep 18 - 24)

This week: Triaging. General code contribution. Devtalk moderation.  
Took holiday on Tuesday for festival.  
Next Week: Triaging.  
**Total actions on tracker: 203**

  - Confirmed: 18

[\#109192](https://projects.blender.org/blender/blender/issues/109192),
[\#112500](https://projects.blender.org/blender/blender/issues/112500),
[\#112613](https://projects.blender.org/blender/blender/issues/112613),
[\#112651](https://projects.blender.org/blender/blender/issues/112651),
[\#112650](https://projects.blender.org/blender/blender/issues/112650),
[\#112423](https://projects.blender.org/blender/blender/issues/112423),
[\#112642](https://projects.blender.org/blender/blender/issues/112642),
[\#112610](https://projects.blender.org/blender/blender/issues/112610),
[\#112032](https://projects.blender.org/blender/blender/issues/112032),
[\#104893](https://projects.blender.org/blender/blender-addons/issues/104893),
[\#112490](https://projects.blender.org/blender/blender/issues/112490),
[\#112649](https://projects.blender.org/blender/blender/issues/112649),
[\#112653](https://projects.blender.org/blender/blender/issues/112653),
[\#112486](https://projects.blender.org/blender/blender/issues/112486),
[\#112646](https://projects.blender.org/blender/blender/issues/112646),
[\#112499](https://projects.blender.org/blender/blender/issues/112499),
[\#110162](https://projects.blender.org/blender/blender/issues/110162),
[\#112641](https://projects.blender.org/blender/blender/issues/112641)

  - Closed: 19

[\#112441](https://projects.blender.org/blender/blender/issues/112441),
[\#112650](https://projects.blender.org/blender/blender/issues/112650),
[\#112614](https://projects.blender.org/blender/blender/issues/112614),
[\#110854](https://projects.blender.org/blender/blender/issues/110854),
[\#112493](https://projects.blender.org/blender/blender/issues/112493),
[\#112647](https://projects.blender.org/blender/blender/issues/112647),
[\#110526](https://projects.blender.org/blender/blender/issues/110526),
[\#112441](https://projects.blender.org/blender/blender/issues/112441),
[\#112797](https://projects.blender.org/blender/blender/issues/112797),
[\#111546](https://projects.blender.org/blender/blender/issues/111546),
[\#112613](https://projects.blender.org/blender/blender/issues/112613),
[\#112555](https://projects.blender.org/blender/blender/issues/112555),
[\#110814](https://projects.blender.org/blender/blender/issues/110814),
[\#110850](https://projects.blender.org/blender/blender/issues/110850),
[\#112043](https://projects.blender.org/blender/blender/issues/112043),
[\#111569](https://projects.blender.org/blender/blender/issues/111569),
[\#112650](https://projects.blender.org/blender/blender/issues/112650),
[\#110550](https://projects.blender.org/blender/blender/issues/110550),
[\#112485](https://projects.blender.org/blender/blender/issues/112485)

  - Needs Information from User: 13

<!-- end list -->

  - PR submitted: 6

[PR
\#112719](https://projects.blender.org/blender/blender/pulls/112719):
Fix: Garbage string when searching in menu  
[PR
\#112652](https://projects.blender.org/blender/blender/pulls/112652):
Fix \#112630: Skip grave quotation when selecting  
[PR
\#112657](https://projects.blender.org/blender/blender/pulls/112657):
Fix \#112650: UI: Missing Header Status in Node Editor after Turning off
Auto-offset  
[PR
\#112497](https://projects.blender.org/blender/blender/pulls/112497):
Fix \#112441: Restore Key Map Item does not restore the active
property  
[PR
\#112573](https://projects.blender.org/blender/blender/pulls/112573):
GPv3: Add statistics information  
[PR
\#112621](https://projects.blender.org/blender/blender/pulls/112621):
GPv3: Re-evaluate object after deleting keyframes  

  - Review: 3

[PR
\#112704](https://projects.blender.org/blender/blender/pulls/112704):
Clarify UV align operator behavior. Fix tooltips.  
[PR
\#109146](https://projects.blender.org/blender/blender/pulls/109146):
Add Vertex Crease operator to vertex context menu  
[PR
\#111682](https://projects.blender.org/blender/blender/pulls/111682):
Walk Navigation: Add local up/down movement with R/F  

  - Commits: 4

[90f22739f7](https://projects.blender.org/blender/blender/commit/90f22739f74ae38a6484e21f784f015ed09bea61):
Fix \#112650: UI: Missing Header Status in Node Editor after Turning off
Auto-offset  
[bb39f4fa41](https://projects.blender.org/blender/blender/commit/bb39f4fa41d1bf5f292255be1aabc5fbe3512b1e):
Fix \#112441: Restore Key Map Item does not restore the active
property  
[7e944b1b2c](https://projects.blender.org/blender/blender/commit/7e944b1b2ccd1bf63b293f90c8a8269fe65d765c):
GPv3: Add statistics information  
[8eb2d193c5](https://projects.blender.org/blender/blender/commit/8eb2d193c5e1eb3aa5a2fe06c89e09258eb017e3):
GPv3: Re-evaluate object after deleting keyframes  

## Week 122 (Sep 25 - 01)

This week: Triaging. General code contribution. Devtalk moderation.  
AFK on Thursday-Friday so worked during weekend. Created GPv3 community
tasks:
[\#113116](https://projects.blender.org/blender/blender/issues/113116),
[\#113110](https://projects.blender.org/blender/blender/issues/113110)  
Next Week: Triaging.  
**Total actions on tracker: 164**

  - Confirmed: 14

[\#113010](https://projects.blender.org/blender/blender/issues/113010),
[\#113078](https://projects.blender.org/blender/blender/issues/113078),
[\#112769](https://projects.blender.org/blender/blender/issues/112769),
[\#112885](https://projects.blender.org/blender/blender/issues/112885),
[\#113039](https://projects.blender.org/blender/blender/issues/113039),
[\#112933](https://projects.blender.org/blender/blender/issues/112933),
[\#112904](https://projects.blender.org/blender/blender/issues/112904),
[\#112802](https://projects.blender.org/blender/blender/issues/112802),
[\#112825](https://projects.blender.org/blender/blender/issues/112825),
[\#112817](https://projects.blender.org/blender/blender/issues/112817),
[\#112734](https://projects.blender.org/blender/blender/issues/112734),
[\#112767](https://projects.blender.org/blender/blender/issues/112767),
[\#104901](https://projects.blender.org/blender/blender-addons/issues/104901)

  - Closed: 17

[\#112904](https://projects.blender.org/blender/blender/issues/112904),
[\#112896](https://projects.blender.org/blender/blender/issues/112896),
[\#109509](https://projects.blender.org/blender/blender/issues/109509),
[\#112768](https://projects.blender.org/blender/blender/issues/112768),
[\#112885](https://projects.blender.org/blender/blender/issues/112885),
[\#112904](https://projects.blender.org/blender/blender/issues/112904),
[\#113039](https://projects.blender.org/blender/blender/issues/113039),
[\#112896](https://projects.blender.org/blender/blender/issues/112896),
[\#94670](https://projects.blender.org/blender/blender-addons/issues/94670),
[\#113009](https://projects.blender.org/blender/blender/issues/113009),
[\#112778](https://projects.blender.org/blender/blender/issues/112778),
[\#104656](https://projects.blender.org/blender/blender-addons/issues/104656),
[\#113080](https://projects.blender.org/blender/blender/issues/113080),
[\#112950](https://projects.blender.org/blender/blender/issues/112950),
[\#112888](https://projects.blender.org/blender/blender/issues/112888),
[\#104915](https://projects.blender.org/blender/blender-addons/issues/104915),
[\#104916](https://projects.blender.org/blender/blender-addons/issues/104916)

  - Needs Information from User: 16

<!-- end list -->

  - Own Reported: 4

[\#113116](https://projects.blender.org/blender/blender/issues/113116),
[\#113110](https://projects.blender.org/blender/blender/issues/113110),
[\#112896](https://projects.blender.org/blender/blender/issues/112896),
[\#112936](https://projects.blender.org/blender/blender/issues/112936)

  - PR submitted/updated: 6

[PR
\#113082](https://projects.blender.org/blender/blender/pulls/113082):
Fix \#113022: Disabled menu item's text uses inner color  
[PR
\#113085](https://projects.blender.org/blender/blender/pulls/113085):
Fix \#113078: Texture paint masking options are always visible  
[PR
\#112955](https://projects.blender.org/blender/blender/pulls/112955):
Fix \#112767: Outliner unlink operation fails to unlink from scene  
[PR
\#112652](https://projects.blender.org/blender/blender/pulls/112652):
Fix \#112630: Skip grave quotation when selecting  
[PR
\#112938](https://projects.blender.org/blender/blender/pulls/112938):
Fix \#112896: Search menu: First character remains in text field  
[PR
\#112908](https://projects.blender.org/blender/blender/pulls/112908):
Fix \#112904: Outliner: Bones appear in different places in different
modes  

  - Review: 0

<!-- end list -->

  - Commits: 2

[cea02c15f8](https://projects.blender.org/blender/blender/commit/cea02c15f810beba5dd65a300a6389ccc29970ad):
Fix \#112896: Search menu: First character remains in text field  
[9c20a29259](https://projects.blender.org/blender/blender/commit/9c20a29259b826b152e1c051182f96c97573c754):
Fix \#112904: Outliner: Bones appear in different places in different
modes  

## Week 123 (Oct 02 - 08)

This week: Triaging. General code contribution. Devtalk moderation.  
Created GPv3 community task:
[\#113248](https://projects.blender.org/blender/blender/issues/113248)  
Next Week: Triaging.  
**Total actions on tracker: 183**

  - Confirmed: 21

[\#113168](https://projects.blender.org/blender/blender/issues/113168),
[\#113258](https://projects.blender.org/blender/blender/issues/113258),
[\#113112](https://projects.blender.org/blender/blender/issues/113112),
[\#113215](https://projects.blender.org/blender/blender/issues/113215),
[\#113109](https://projects.blender.org/blender/blender/issues/113109),
[\#113201](https://projects.blender.org/blender/blender/issues/113201),
[\#113306](https://projects.blender.org/blender/blender/issues/113306),
[\#113310](https://projects.blender.org/blender/blender/issues/113310),
[\#113182](https://projects.blender.org/blender/blender/issues/113182),
[\#113302](https://projects.blender.org/blender/blender/issues/113302),
[\#113117](https://projects.blender.org/blender/blender/issues/113117),
[\#113118](https://projects.blender.org/blender/blender/issues/113118),
[\#113309](https://projects.blender.org/blender/blender/issues/113309),
[\#113220](https://projects.blender.org/blender/blender/issues/113220),
[\#113186](https://projects.blender.org/blender/blender/issues/113186),
[\#113313](https://projects.blender.org/blender/blender/issues/113313),
[\#113316](https://projects.blender.org/blender/blender/issues/113316),
[\#113232](https://projects.blender.org/blender/blender/issues/113232),
[\#113235](https://projects.blender.org/blender/blender/issues/113235),
[\#113181](https://projects.blender.org/blender/blender/issues/113181),
[\#113124](https://projects.blender.org/blender/blender/issues/113124)

  - Closed: 22

[\#113078](https://projects.blender.org/blender/blender/issues/113078),
[\#113022](https://projects.blender.org/blender/blender/issues/113022),
[\#113236](https://projects.blender.org/blender/blender/issues/113236),
[\#113289](https://projects.blender.org/blender/blender/issues/113289),
[\#113221](https://projects.blender.org/blender/blender/issues/113221),
[\#113230](https://projects.blender.org/blender/blender/issues/113230),
[\#104933](https://projects.blender.org/blender/blender-addons/issues/104933),
[\#113078](https://projects.blender.org/blender/blender/issues/113078),
[\#113307](https://projects.blender.org/blender/blender/issues/113307),
[\#113113](https://projects.blender.org/blender/blender/issues/113113),
[\#113314](https://projects.blender.org/blender/blender/issues/113314),
[\#113272](https://projects.blender.org/blender/blender/issues/113272),
[\#113229](https://projects.blender.org/blender/blender/issues/113229),
[\#113228](https://projects.blender.org/blender/blender/issues/113228),
[\#113022](https://projects.blender.org/blender/blender/issues/113022),
[\#104930](https://projects.blender.org/blender/blender-addons/issues/104930),
[\#113117](https://projects.blender.org/blender/blender/issues/113117),
[\#113073](https://projects.blender.org/blender/blender/issues/113073),
[\#113098](https://projects.blender.org/blender/blender/issues/113098),
[\#104923](https://projects.blender.org/blender/blender-addons/issues/104923),
[\#113231](https://projects.blender.org/blender/blender/issues/113231),
[\#112032](https://projects.blender.org/blender/blender/issues/112032)

  - Needs Information from User:

<!-- end list -->

  - Own Reported: 2

[\#113248](https://projects.blender.org/blender/blender/issues/113248),
[\#113273](https://projects.blender.org/blender/blender/issues/113273)

  - PR submitted: 3

[PR
\#113408](https://projects.blender.org/blender/blender/pulls/113408):
GPv3: Insert keyframe when drawing if does not exist at current scene
frame  
[PR
\#113275](https://projects.blender.org/blender/blender/pulls/113275):
Fix \#113273: Status bar: Keymap info not cleared after cancelling the
operation  
[PR
\#113140](https://projects.blender.org/blender/blender/pulls/113140):
Fix \#113112: Loop Cut and Slide from menu is waiting for cursor input  

  - Review: 2

[PR
\#113274](https://projects.blender.org/blender/blender/pulls/113274):
Fix \#112683: Removing the last Annotations layer produces a warning  
[PR
\#109330](https://projects.blender.org/blender/blender/pulls/109330):
GPv3: Fix: Crash when drawing without a layer  

  - Commits: 3

[3a00ba53a1](https://projects.blender.org/blender/blender/commit/3a00ba53a13793d36bd8fa32ed991b2264cd4746):
Fix \#113078: Texture paint masking options are always visible  
[fcd6b6f4f6](https://projects.blender.org/blender/blender/commit/fcd6b6f4f621e14cc14920475c511fa843a02bd3):
Fix \#112630: Skip grave and acute quotations when selecting  
[b5ebf31fb1](https://projects.blender.org/blender/blender/commit/b5ebf31fb114d3d43c11227eca9b1f3ded9f06f1):
Fix \#113022: Disabled menu item's text uses inner color  

## Week 124 (Oct 09 - 15)

This week: Triaging. General code contribution. Devtalk moderation.  
Created GPv3 community tasks during Thursday-Friday (mentioned below).  
Next Week: Triaging.  
**Total actions on tracker: 147**

  - Confirmed: 10

[\#104752](https://projects.blender.org/blender/blender-addons/issues/104752),
[\#110326](https://projects.blender.org/blender/blender/issues/110326),
[\#113403](https://projects.blender.org/blender/blender/issues/113403),
[\#113466](https://projects.blender.org/blender/blender/issues/113466),
[\#113460](https://projects.blender.org/blender/blender/issues/113460),
[\#113563](https://projects.blender.org/blender/blender/issues/113563),
[\#113489](https://projects.blender.org/blender/blender/issues/113489),
[\#113365](https://projects.blender.org/blender/blender/issues/113365),
[\#113433](https://projects.blender.org/blender/blender/issues/113433),
[\#113416](https://projects.blender.org/blender/blender/issues/113416)

  - Closed: 20

[\#113460](https://projects.blender.org/blender/blender/issues/113460),
[\#113415](https://projects.blender.org/blender/blender/issues/113415),
[\#104721](https://projects.blender.org/blender/blender-addons/issues/104721),
[\#113567](https://projects.blender.org/blender/blender/issues/113567),
[\#104693](https://projects.blender.org/blender/blender-addons/issues/104693),
[\#113296](https://projects.blender.org/blender/blender/issues/113296),
[\#113091](https://projects.blender.org/blender/blender/issues/113091),
[\#113349](https://projects.blender.org/blender/blender/issues/113349),
[\#113528](https://projects.blender.org/blender/blender/issues/113528),
[\#113099](https://projects.blender.org/blender/blender/issues/113099),
[\#104834](https://projects.blender.org/blender/blender-addons/issues/104834),
[\#104763](https://projects.blender.org/blender/blender-addons/issues/104763),
[\#104720](https://projects.blender.org/blender/blender-addons/issues/104720),
[\#113426](https://projects.blender.org/blender/blender/issues/113426),
[\#104847](https://projects.blender.org/blender/blender-addons/issues/104847),
[\#113417](https://projects.blender.org/blender/blender/issues/113417),
[\#104840](https://projects.blender.org/blender/blender-addons/issues/104840),
[\#104814](https://projects.blender.org/blender/blender-addons/issues/104814),
[\#104850](https://projects.blender.org/blender/blender-addons/issues/104850),
[\#113184](https://projects.blender.org/blender/blender/issues/113184)

  - Needs Information from User: 6

<!-- end list -->

  - Own Reported (GPv3 tasks): 11

[\#113671](https://projects.blender.org/blender/blender/issues/113671),
[\#113599](https://projects.blender.org/blender/blender/issues/113599),
[\#113667](https://projects.blender.org/blender/blender/issues/113667),
[\#113590](https://projects.blender.org/blender/blender/issues/113590),
[\#113582](https://projects.blender.org/blender/blender/issues/113582),
[\#113641](https://projects.blender.org/blender/blender/issues/113641),
[\#113643](https://projects.blender.org/blender/blender/issues/113643),
[\#113640](https://projects.blender.org/blender/blender/issues/113640),
[\#113570](https://projects.blender.org/blender/blender/issues/113570),
[\#113481](https://projects.blender.org/blender/blender/issues/113481),
[\#113569](https://projects.blender.org/blender/blender/issues/113569)

  - PR submitted: 5

[PR
\#113573](https://projects.blender.org/blender/blender/pulls/113573):
GPv3: Assign material  
[PR
\#113490](https://projects.blender.org/blender/blender/pulls/113490):
Fix \#113365: Backdrop lines don't match height in nla  
[PR
\#113663](https://projects.blender.org/blender/blender/pulls/113663):
GPv3: Expose insert keyframe in menu  
[PR
\#113526](https://projects.blender.org/blender/blender/pulls/113526):
GPv3: Assign correct GP material  
[PR
\#113471](https://projects.blender.org/blender/blender/pulls/113471):
Update drawtype when forcefield is added to empty object  

  - Review: 1

[PR
\#113274](https://projects.blender.org/blender/blender/pulls/113274):
Fix \#112683: Removing the last Annotations layer produces a warning  

  - Commits: 4

[d836aa7931](https://projects.blender.org/blender/blender/commit/d836aa7931221306303d4c0b3488823c4d780e22):
GPv3: Expose insert keyframe in menu  
[4fabafa352](https://projects.blender.org/blender/blender/commit/4fabafa3527f8700a8cc1cbfe6e7dc46f2466fbb):
GPv3: Assign correct GP material  
[02b5ad05f6](https://projects.blender.org/blender/blender/commit/02b5ad05f6ac688d78a99e5b090bd8559c2572e8):
Update drawtype when forcefield is added to empty object  
[f37a8cda30](https://projects.blender.org/blender/blender/commit/f37a8cda30e9f8943756ff20a7d0ca85c9106c68):
GPv3: Insert keyframe when drawing if no key exists at current frame  

## Week 125 (Oct 16 - 22)

This week: Triaging. General code contribution. Devtalk moderation.  
Created GPv3 community tasks.  
Next Week: Triaging. Festival/national holiday on Tuesday. Also
travelling for Blender Conference on Tuesday.  
**Total actions on tracker: 225**

  - Confirmed: 24

[\#113752](https://projects.blender.org/blender/blender/issues/113752),
[\#113726](https://projects.blender.org/blender/blender/issues/113726),
[\#113774](https://projects.blender.org/blender/blender/issues/113774),
[\#113778](https://projects.blender.org/blender/blender/issues/113778),
[\#113898](https://projects.blender.org/blender/blender/issues/113898),
[\#113628](https://projects.blender.org/blender/blender/issues/113628),
[\#113856](https://projects.blender.org/blender/blender/issues/113856),
[\#113762](https://projects.blender.org/blender/blender/issues/113762),
[\#113862](https://projects.blender.org/blender/blender/issues/113862),
[\#113763](https://projects.blender.org/blender/blender/issues/113763),
[\#113730](https://projects.blender.org/blender/blender/issues/113730),
[\#113732](https://projects.blender.org/blender/blender/issues/113732),
[\#113266](https://projects.blender.org/blender/blender/issues/113266),
[\#113851](https://projects.blender.org/blender/blender/issues/113851),
[\#113909](https://projects.blender.org/blender/blender/issues/113909),
[\#113864](https://projects.blender.org/blender/blender/issues/113864),
[\#113747](https://projects.blender.org/blender/blender/issues/113747),
[\#113796](https://projects.blender.org/blender/blender/issues/113796),
[\#113871](https://projects.blender.org/blender/blender/issues/113871),
[\#113774](https://projects.blender.org/blender/blender/issues/113774),
[\#113812](https://projects.blender.org/blender/blender/issues/113812),
[\#113755](https://projects.blender.org/blender/blender/issues/113755),
[\#113943](https://projects.blender.org/blender/blender/issues/113943),
[\#113732](https://projects.blender.org/blender/blender/issues/113732)

  - Closed: 30

[\#113778](https://projects.blender.org/blender/blender/issues/113778),
[\#113481](https://projects.blender.org/blender/blender/issues/113481),
[\#113847](https://projects.blender.org/blender/blender/issues/113847),
[\#113808](https://projects.blender.org/blender/blender/issues/113808),
[\#113778](https://projects.blender.org/blender/blender/issues/113778),
[\#113913](https://projects.blender.org/blender/blender/issues/113913),
[\#112352](https://projects.blender.org/blender/blender/issues/112352),
[\#112558](https://projects.blender.org/blender/blender/issues/112558),
[\#113611](https://projects.blender.org/blender/blender/issues/113611),
[\#113813](https://projects.blender.org/blender/blender/issues/113813),
[\#112789](https://projects.blender.org/blender/blender/issues/112789),
[\#112430](https://projects.blender.org/blender/blender/issues/112430),
[\#113943](https://projects.blender.org/blender/blender/issues/113943),
[\#112476](https://projects.blender.org/blender/blender/issues/112476),
[\#111321](https://projects.blender.org/blender/blender/issues/111321),
[\#113729](https://projects.blender.org/blender/blender/issues/113729),
[\#113264](https://projects.blender.org/blender/blender/issues/113264),
[\#113785](https://projects.blender.org/blender/blender/issues/113785),
[\#113481](https://projects.blender.org/blender/blender/issues/113481),
[\#112629](https://projects.blender.org/blender/blender/issues/112629),
[\#113718](https://projects.blender.org/blender/blender/issues/113718),
[\#113715](https://projects.blender.org/blender/blender/issues/113715),
[\#113637](https://projects.blender.org/blender/blender/issues/113637),
[\#112755](https://projects.blender.org/blender/blender/issues/112755),
[\#112707](https://projects.blender.org/blender/blender/issues/112707),
[\#112787](https://projects.blender.org/blender/blender/issues/112787),
[\#113888](https://projects.blender.org/blender/blender/issues/113888),
[\#111774](https://projects.blender.org/blender/blender/issues/111774),
[\#113764](https://projects.blender.org/blender/blender/issues/113764),
[\#112683](https://projects.blender.org/blender/blender/issues/112683)

  - Needs Information from User: 19

<!-- end list -->

  - Own Reported: 4

[\#113991](https://projects.blender.org/blender/blender/issues/113991),
[\#113917](https://projects.blender.org/blender/blender/issues/113917),
[\#113932](https://projects.blender.org/blender/blender/issues/113932),
[\#113915](https://projects.blender.org/blender/blender/issues/113915)

  - PR submitted: 4

[PR
\#113831](https://projects.blender.org/blender/blender/pulls/113831):
Fix \#113774: Select objects doesn't work for multiple selected
Collections  
[PR
\#113573](https://projects.blender.org/blender/blender/pulls/113573):
GPv3: Assign material  
[PR
\#113805](https://projects.blender.org/blender/blender/pulls/113805):
Fix \#113778: Skip unlinking a linked collection if parent is linked  
[PR
\#113765](https://projects.blender.org/blender/blender/pulls/113765):
GPv3: Fix problems related to insert/delete keys in edit mode  

  - Review: 2

[PR
\#114006](https://projects.blender.org/blender/blender/pulls/114006):
GPv3: Normalize Stroke operator  
[PR
\#113274](https://projects.blender.org/blender/blender/pulls/113274):
Fix \#112683: Removing the last Annotations layer produces a warning.  

  - Commits: 3

[2a5c499585](https://projects.blender.org/blender/blender/commit/2a5c499585fe890b0c1c8540e1cf1f93509cec2c):
GPv3: Assign material  
[8867cfc563](https://projects.blender.org/blender/blender/commit/8867cfc563bd8432aafb4e28cebb0f67a4b014ca):
Fix \#113778: Skip unlinking a linked collection if parent is linked  
[d03d03402b](https://projects.blender.org/blender/blender/commit/d03d03402bff9d9a683ee62d9fbf5f571625a72b):
GPv3: Fix problems related to insert/delete keys in edit mode  

## Week 126 (Oct 23 - 29)

This week: Triaging. General code contribution. Devtalk moderation.  
It was a short week. I was at Blender conference.  
Next Week: Triaging. Will be in Amsterdam till Wednesday.  
**Total actions on tracker: 85**

  - Confirmed: 3

[\#113994](https://projects.blender.org/blender/blender/issues/113994),
[\#114024](https://projects.blender.org/blender/blender/issues/114024),
[\#114041](https://projects.blender.org/blender/blender/issues/114041)

  - Closed: 24

[\#114124](https://projects.blender.org/blender/blender/issues/114124),
[\#113699](https://projects.blender.org/blender/blender/issues/113699),
[\#114010](https://projects.blender.org/blender/blender/issues/114010),
[\#113112](https://projects.blender.org/blender/blender/issues/113112),
[\#112461](https://projects.blender.org/blender/blender/issues/112461),
[\#113069](https://projects.blender.org/blender/blender/issues/113069),
[\#110105](https://projects.blender.org/blender/blender/issues/110105),
[\#110450](https://projects.blender.org/blender/blender/issues/110450),
[\#110459](https://projects.blender.org/blender/blender/issues/110459),
[\#110625](https://projects.blender.org/blender/blender/issues/110625),
[\#111445](https://projects.blender.org/blender/blender/issues/111445),
[\#111654](https://projects.blender.org/blender/blender/issues/111654),
[\#111720](https://projects.blender.org/blender/blender/issues/111720),
[\#111743](https://projects.blender.org/blender/blender/issues/111743),
[\#112049](https://projects.blender.org/blender/blender/issues/112049),
[\#112050](https://projects.blender.org/blender/blender/issues/112050),
[\#112091](https://projects.blender.org/blender/blender/issues/112091),
[\#112204](https://projects.blender.org/blender/blender/issues/112204),
[\#112207](https://projects.blender.org/blender/blender/issues/112207),
[\#112274](https://projects.blender.org/blender/blender/issues/112274),
[\#113103](https://projects.blender.org/blender/blender/issues/113103),
[\#112301](https://projects.blender.org/blender/blender/issues/112301),
[\#114036](https://projects.blender.org/blender/blender/issues/114036),
[\#114042](https://projects.blender.org/blender/blender/issues/114042)

  - Own Reported: 1

[\#114182](https://projects.blender.org/blender/blender/issues/114182)

  - PR submitted/updated: 2

[PR
\#114057](https://projects.blender.org/blender/blender/pulls/114057):
Fix \#114010: Can't duplicate object data  
[PR
\#113140](https://projects.blender.org/blender/blender/pulls/113140):
Fix \#113112: Loop Cut and Slide from menu is waiting for cursor input  

  - Review: 1

[PR
\#114188](https://projects.blender.org/blender/blender/pulls/114188):
GPv3: Set selected curve material as active material  

  - Commits: 2

[357c213cde](https://projects.blender.org/blender/blender/commit/357c213cdee594cb02cac9132deb63be4be15bb9):
Fix \#114010: Can't duplicate object data  
[b8705d342f](https://projects.blender.org/blender/blender/commit/b8705d342fd7600483e61b2ad1c2ce50d738c178):
Fix \#113112: Loop Cut and Slide from menu is waiting for cursor input  

## Week 127 (Oct 30 - 05)

This week: Triaging. It was a short week. Returned to home on Wednesday
from Amsterdam.  
Next Week: Triaging.  
**Total actions on tracker: 87**

  - Confirmed: 12

[\#114290](https://projects.blender.org/blender/blender/issues/114290),
[\#114435](https://projects.blender.org/blender/blender/issues/114435),
[\#114436](https://projects.blender.org/blender/blender/issues/114436),
[\#114440](https://projects.blender.org/blender/blender/issues/114440),
[\#114438](https://projects.blender.org/blender/blender/issues/114438),
[\#114434](https://projects.blender.org/blender/blender/issues/114434),
[\#114388](https://projects.blender.org/blender/blender/issues/114388),
[\#114389](https://projects.blender.org/blender/blender/issues/114389),
[\#114391](https://projects.blender.org/blender/blender/issues/114391),
[\#114090](https://projects.blender.org/blender/blender/issues/114090),
[\#114217](https://projects.blender.org/blender/blender/issues/114217),
[\#114377](https://projects.blender.org/blender/blender/issues/114377)

  - Closed: 15

[\#114478](https://projects.blender.org/blender/blender/issues/114478),
[\#114446](https://projects.blender.org/blender/blender/issues/114446),
[\#114013](https://projects.blender.org/blender/blender/issues/114013),
[\#114431](https://projects.blender.org/blender/blender/issues/114431),
[\#114405](https://projects.blender.org/blender/blender/issues/114405),
[\#114183](https://projects.blender.org/blender/blender/issues/114183),
[\#114207](https://projects.blender.org/blender/blender/issues/114207),
[\#114392](https://projects.blender.org/blender/blender/issues/114392),
[\#114394](https://projects.blender.org/blender/blender/issues/114394),
[\#114288](https://projects.blender.org/blender/blender/issues/114288),
[\#114239](https://projects.blender.org/blender/blender/issues/114239),
[\#114316](https://projects.blender.org/blender/blender/issues/114316),
[\#114367](https://projects.blender.org/blender/blender/issues/114367),
[\#104937](https://projects.blender.org/blender/blender-addons/issues/104937),
[\#114025](https://projects.blender.org/blender/blender/issues/114025)

  - Needs Information from User: 16

<!-- end list -->

  - PR submitted: 0

<!-- end list -->

  - Review: 2

[PR
\#114348](https://projects.blender.org/blender/blender/pulls/114348):
GPv3: Show and hide layers operators  
[PR
\#114188](https://projects.blender.org/blender/blender/pulls/114188):
GPv3: Set selected curve material as active material  

  - Commits: 1

[9ac87af839](https://projects.blender.org/blender/blender/commit/9ac87af839):
Cleanup: Wrong comment  

## Week 128 (Nov 06 - 12)

This week: Triaging. General code contribution. Devtalk moderation.  
Next Week: Triaging.  
**Total actions on tracker: 200**

  - Confirmed: 27

[\#114432](https://projects.blender.org/blender/blender/issues/114432),
[\#114548](https://projects.blender.org/blender/blender/issues/114548),
[\#114564](https://projects.blender.org/blender/blender/issues/114564),
[\#114657](https://projects.blender.org/blender/blender/issues/114657),
[\#114618](https://projects.blender.org/blender/blender/issues/114618),
[\#114648](https://projects.blender.org/blender/blender/issues/114648),
[\#114655](https://projects.blender.org/blender/blender/issues/114655),
[\#114552](https://projects.blender.org/blender/blender/issues/114552),
[\#114521](https://projects.blender.org/blender/blender/issues/114521),
[\#114685](https://projects.blender.org/blender/blender/issues/114685),
[\#114687](https://projects.blender.org/blender/blender/issues/114687),
[\#114651](https://projects.blender.org/blender/blender/issues/114651),
[\#114481](https://projects.blender.org/blender/blender/issues/114481),
[\#114595](https://projects.blender.org/blender/blender/issues/114595),
[\#114649](https://projects.blender.org/blender/blender/issues/114649),
[\#114468](https://projects.blender.org/blender/blender/issues/114468),
[\#110184](https://projects.blender.org/blender/blender/issues/110184),
[\#114520](https://projects.blender.org/blender/blender/issues/114520),
[\#114479](https://projects.blender.org/blender/blender/issues/114479),
[\#114504](https://projects.blender.org/blender/blender/issues/114504),
[\#114547](https://projects.blender.org/blender/blender/issues/114547),
[\#114599](https://projects.blender.org/blender/blender/issues/114599),
[\#114557](https://projects.blender.org/blender/blender/issues/114557),
[\#114660](https://projects.blender.org/blender/blender/issues/114660),
[\#114338](https://projects.blender.org/blender/blender/issues/114338),
[\#114646](https://projects.blender.org/blender/blender/issues/114646)

  - Closed: 25

[\#114521](https://projects.blender.org/blender/blender/issues/114521),
[\#114687](https://projects.blender.org/blender/blender/issues/114687),
[\#114685](https://projects.blender.org/blender/blender/issues/114685),
[\#114583](https://projects.blender.org/blender/blender/issues/114583),
[\#104965](https://projects.blender.org/blender/blender-addons/issues/104965),
[\#114617](https://projects.blender.org/blender/blender/issues/114617),
[\#104987](https://projects.blender.org/blender/blender-addons/issues/104987),
[\#114730](https://projects.blender.org/blender/blender/issues/114730),
[\#104979](https://projects.blender.org/blender/blender-addons/issues/104979),
[\#114439](https://projects.blender.org/blender/blender/issues/114439),
[\#114521](https://projects.blender.org/blender/blender/issues/114521),
[\#114687](https://projects.blender.org/blender/blender/issues/114687),
[\#114685](https://projects.blender.org/blender/blender/issues/114685),
[\#114583](https://projects.blender.org/blender/blender/issues/114583),
[\#114741](https://projects.blender.org/blender/blender/issues/114741),
[\#114508](https://projects.blender.org/blender/blender/issues/114508),
[\#114563](https://projects.blender.org/blender/blender/issues/114563),
[\#114566](https://projects.blender.org/blender/blender/issues/114566),
[\#114652](https://projects.blender.org/blender/blender/issues/114652),
[\#114556](https://projects.blender.org/blender/blender/issues/114556),
[\#114620](https://projects.blender.org/blender/blender/issues/114620),
[\#114490](https://projects.blender.org/blender/blender/issues/114490),
[\#104977](https://projects.blender.org/blender/blender-addons/issues/104977),
[\#114488](https://projects.blender.org/blender/blender/issues/114488),
[\#104971](https://projects.blender.org/blender/blender-addons/issues/104971),
[\#114616](https://projects.blender.org/blender/blender/issues/114616)

  - Needs Information from User: 28

<!-- end list -->

  - Own Reported: 1

[\#114564](https://projects.blender.org/blender/blender/issues/114564)

  - PR submitted: 4

[PR
\#114698](https://projects.blender.org/blender/blender/pulls/114698):
Fix \#114687: Orbit opposite changes view to perspective  
[PR
\#114615](https://projects.blender.org/blender/blender/pulls/114615):
Fix \#114583: Missing forward axis option in obj export  
[PR
\#114625](https://projects.blender.org/blender/blender/pulls/114625):
UI: Type to search for sequencer add menu  
[PR
\#114526](https://projects.blender.org/blender/blender/pulls/114526):
Fix \#114521: Walk navigation global up does not work  

  - Reviews: 3

[PR
\#114670](https://projects.blender.org/blender/blender/pulls/114670):
UI: Change walk gravity settings UI to reflect the actual behavior  
[PR
\#114348](https://projects.blender.org/blender/blender/pulls/114348):
GPv3: Show and hide layers operators  
[PR
\#104538](https://projects.blender.org/blender/blender-manual/pulls/104538):
Walk Navigation: Add local up/down movement  

  - Commits: 5

[270fc860cf](https://projects.blender.org/blender/blender/commit/270fc860cf):
Fix \#114685: GP time offset modifier doesn't work if the animation is 1
frame long  
[ab2198d77b](https://projects.blender.org/blender/blender/commit/ab2198d77b5161d9e1f5b05a86cdb0fbdc95ace5):
Fix \#114687: Orbit opposite changes view to perspective  
[f7dd40afbf](https://projects.blender.org/blender/blender/commit/f7dd40afbf59b90443f57812904e8c527b047186):
Fix \#114583: Missing forward axis option in obj export  
[2a45027203](https://projects.blender.org/blender/blender/commit/2a45027203854e63023e649b63631f42138b4006):
UI: Type to search for sequencer add menu  
[e5327fa5b7](https://projects.blender.org/blender/blender/commit/e5327fa5b7c871401b75152af697642628795214):
Fix \#114521: Walk navigation global up does not work

## Week 129 (Nov 13 - 19)

This week: Triaging. General code contribution. Devtalk moderation.  
Next Week: Triaging.  
**Total actions on tracker: 193**

  - Confirmed: 25

[\#114781](https://projects.blender.org/blender/blender/issues/114781),
[\#114895](https://projects.blender.org/blender/blender/issues/114895),
[\#115014](https://projects.blender.org/blender/blender/issues/115014),
[\#114962](https://projects.blender.org/blender/blender/issues/114962),
[\#115025](https://projects.blender.org/blender/blender/issues/115025),
[\#114999](https://projects.blender.org/blender/blender/issues/114999),
[\#115037](https://projects.blender.org/blender/blender/issues/115037),
[\#114848](https://projects.blender.org/blender/blender/issues/114848),
[\#114609](https://projects.blender.org/blender/blender/issues/114609),
[\#104980](https://projects.blender.org/blender/blender-addons/issues/104980),
[\#114892](https://projects.blender.org/blender/blender/issues/114892),
[\#114807](https://projects.blender.org/blender/blender/issues/114807),
[\#114818](https://projects.blender.org/blender/blender/issues/114818),
[\#114957](https://projects.blender.org/blender/blender/issues/114957),
[\#114722](https://projects.blender.org/blender/blender/issues/114722),
[\#114653](https://projects.blender.org/blender/blender/issues/114653),
[\#114775](https://projects.blender.org/blender/blender/issues/114775),
[\#114828](https://projects.blender.org/blender/blender/issues/114828),
[\#115029](https://projects.blender.org/blender/blender/issues/115029),
[\#114881](https://projects.blender.org/blender/blender/issues/114881),
[\#114876](https://projects.blender.org/blender/blender/issues/114876),
[\#114481](https://projects.blender.org/blender/blender/issues/114481),
[\#114956](https://projects.blender.org/blender/blender/issues/114956),
[\#114727](https://projects.blender.org/blender/blender/issues/114727),
[\#113966](https://projects.blender.org/blender/blender/issues/113966)

  - Closed: 28

[\#115025](https://projects.blender.org/blender/blender/issues/115025),
[\#114848](https://projects.blender.org/blender/blender/issues/114848),
[\#114758](https://projects.blender.org/blender/blender/issues/114758),
[\#114961](https://projects.blender.org/blender/blender/issues/114961),
[\#114883](https://projects.blender.org/blender/blender/issues/114883),
[\#114035](https://projects.blender.org/blender/blender/issues/114035),
[\#115132](https://projects.blender.org/blender/blender/issues/115132),
[\#114778](https://projects.blender.org/blender/blender/issues/114778),
[\#115025](https://projects.blender.org/blender/blender/issues/115025),
[\#114848](https://projects.blender.org/blender/blender/issues/114848),
[\#115020](https://projects.blender.org/blender/blender/issues/115020),
[\#114820](https://projects.blender.org/blender/blender/issues/114820),
[\#114955](https://projects.blender.org/blender/blender/issues/114955),
[\#114898](https://projects.blender.org/blender/blender/issues/114898),
[\#114904](https://projects.blender.org/blender/blender/issues/114904),
[\#114818](https://projects.blender.org/blender/blender/issues/114818),
[\#113977](https://projects.blender.org/blender/blender/issues/113977),
[\#114740](https://projects.blender.org/blender/blender/issues/114740),
[\#114959](https://projects.blender.org/blender/blender/issues/114959),
[\#115016](https://projects.blender.org/blender/blender/issues/115016),
[\#114974](https://projects.blender.org/blender/blender/issues/114974),
[\#113569](https://projects.blender.org/blender/blender/issues/113569),
[\#113991](https://projects.blender.org/blender/blender/issues/113991),
[\#115019](https://projects.blender.org/blender/blender/issues/115019),
[\#114758](https://projects.blender.org/blender/blender/issues/114758),
[\#114949](https://projects.blender.org/blender/blender/issues/114949),
[\#115008](https://projects.blender.org/blender/blender/issues/115008),
[\#114880](https://projects.blender.org/blender/blender/issues/114880)

  - Needs Information from User: 32

<!-- end list -->

  - PR submitted: 3

[PR
\#115031](https://projects.blender.org/blender/blender/pulls/115031):
Fix \#115025: Transform operations not working in particle edit mode  
[PR
\#114884](https://projects.blender.org/blender/blender/pulls/114884):
Fix \#114848: Crash when adding specular texture slot in texture paint
mode  
[PR
\#114774](https://projects.blender.org/blender/blender/pulls/114774):
Fix \#114758: keep text field empty for global search  

  - Reviews: 0

<!-- end list -->

  - Commits: 3

[bf7d4ef054](https://projects.blender.org/blender/blender/commit/bf7d4ef05430d83fbd2754106d44b67eb6d6f3aa):
Fix \#115025: Transform operations not working in particle edit mode  
[4ee531cf26](https://projects.blender.org/blender/blender/commit/4ee531cf26099099e4e66b740015001b2c9d2117):
Fix \#114848: Crash when adding specular texture slot in texture paint
mode  
[920d9a9165](https://projects.blender.org/blender/blender/commit/920d9a916557aabf7144e8a7c2c3c26547d0128c):
Fix \#114758: keep text field empty for global search  

## Week 130 (Nov 20 - 26)

This week: Triaging. General code contribution. Devtalk moderation.  
Next Week: Triaging. Short week, only working on Monday. Taking holiday
for 4 days.  
**Total actions on tracker: 224**

  - Confirmed: 32

[\#114902](https://projects.blender.org/blender/blender/issues/114902),
[\#115171](https://projects.blender.org/blender/blender/issues/115171),
[\#115192](https://projects.blender.org/blender/blender/issues/115192),
[\#114926](https://projects.blender.org/blender/blender/issues/114926),
[\#115368](https://projects.blender.org/blender/blender/issues/115368),
[\#115255](https://projects.blender.org/blender/blender/issues/115255),
[\#115387](https://projects.blender.org/blender/blender/issues/115387),
[\#115188](https://projects.blender.org/blender/blender/issues/115188),
[\#115155](https://projects.blender.org/blender/blender/issues/115155),
[\#115291](https://projects.blender.org/blender/blender/issues/115291),
[\#115288](https://projects.blender.org/blender/blender/issues/115288),
[\#115089](https://projects.blender.org/blender/blender/issues/115089),
[\#115091](https://projects.blender.org/blender/blender/issues/115091),
[\#115336](https://projects.blender.org/blender/blender/issues/115336),
[\#115170](https://projects.blender.org/blender/blender/issues/115170),
[\#115194](https://projects.blender.org/blender/blender/issues/115194),
[\#115313](https://projects.blender.org/blender/blender/issues/115313),
[\#115199](https://projects.blender.org/blender/blender/issues/115199),
[\#115160](https://projects.blender.org/blender/blender/issues/115160),
[\#115379](https://projects.blender.org/blender/blender/issues/115379),
[\#115382](https://projects.blender.org/blender/blender/issues/115382),
[\#115393](https://projects.blender.org/blender/blender/issues/115393),
[\#115086](https://projects.blender.org/blender/blender/issues/115086),
[\#115226](https://projects.blender.org/blender/blender/issues/115226),
[\#115372](https://projects.blender.org/blender/blender/issues/115372),
[\#115080](https://projects.blender.org/blender/blender/issues/115080),
[\#115282](https://projects.blender.org/blender/blender/issues/115282),
[\#114506](https://projects.blender.org/blender/blender/issues/114506),
[\#115144](https://projects.blender.org/blender/blender/issues/115144),
[\#115176](https://projects.blender.org/blender/blender/issues/115176),
[\#115390](https://projects.blender.org/blender/blender/issues/115390),
[\#115068](https://projects.blender.org/blender/blender/issues/115068)

  - Closed: 33

[\#115171](https://projects.blender.org/blender/blender/issues/115171),
[\#112767](https://projects.blender.org/blender/blender/issues/112767),
[\#115368](https://projects.blender.org/blender/blender/issues/115368),
[\#115118](https://projects.blender.org/blender/blender/issues/115118),
[\#115188](https://projects.blender.org/blender/blender/issues/115188),
[\#114942](https://projects.blender.org/blender/blender/issues/114942),
[\#115386](https://projects.blender.org/blender/blender/issues/115386),
[\#115171](https://projects.blender.org/blender/blender/issues/115171),
[\#115191](https://projects.blender.org/blender/blender/issues/115191),
[\#115157](https://projects.blender.org/blender/blender/issues/115157),
[\#115156](https://projects.blender.org/blender/blender/issues/115156),
[\#115137](https://projects.blender.org/blender/blender/issues/115137),
[\#115149](https://projects.blender.org/blender/blender/issues/115149),
[\#115113](https://projects.blender.org/blender/blender/issues/115113),
[\#115368](https://projects.blender.org/blender/blender/issues/115368),
[\#115222](https://projects.blender.org/blender/blender/issues/115222),
[\#115230](https://projects.blender.org/blender/blender/issues/115230),
[\#115223](https://projects.blender.org/blender/blender/issues/115223),
[\#115212](https://projects.blender.org/blender/blender/issues/115212),
[\#115006](https://projects.blender.org/blender/blender/issues/115006),
[\#115135](https://projects.blender.org/blender/blender/issues/115135),
[\#115225](https://projects.blender.org/blender/blender/issues/115225),
[\#115331](https://projects.blender.org/blender/blender/issues/115331),
[\#115340](https://projects.blender.org/blender/blender/issues/115340),
[\#115134](https://projects.blender.org/blender/blender/issues/115134),
[\#115152](https://projects.blender.org/blender/blender/issues/115152),
[\#115172](https://projects.blender.org/blender/blender/issues/115172),
[\#114953](https://projects.blender.org/blender/blender/issues/114953),
[\#112767](https://projects.blender.org/blender/blender/issues/112767),
[\#115101](https://projects.blender.org/blender/blender/issues/115101),
[\#115392](https://projects.blender.org/blender/blender/issues/115392),
[\#115254](https://projects.blender.org/blender/blender/issues/115254),
[\#115383](https://projects.blender.org/blender/blender/issues/115383)

  - Needs Information from User: 31

<!-- end list -->

  - Own Reported: 1

[\#115328](https://projects.blender.org/blender/blender/issues/115328)

  - PR submitted: 5

[PR
\#115394](https://projects.blender.org/blender/blender/pulls/115394):
Fix \#115368: Auto smooth node is added every time if the file has
linked object  
[PR
\#115332](https://projects.blender.org/blender/blender/pulls/115332):
NLA: Console warnings about missing channel\_index property  
[PR
\#112955](https://projects.blender.org/blender/blender/pulls/112955):
Fix \#112767: Outliner unlink operation fails to unlink from scene  
[PR
\#113831](https://projects.blender.org/blender/blender/pulls/113831):
Fix \#113774: Select objects doesn't work for multiple selected
Collections  
[PR
\#115266](https://projects.blender.org/blender/blender/pulls/115266):
Fix \#115171: Transfer Mode operator tries and fails on linked objects  

  - Reviews: 0

<!-- end list -->

  - Commits: 5

[b0cb58ca0e](https://projects.blender.org/blender/blender/commit/b0cb58ca0e7ba925ab22355e140064965213d2e1):
Fix \#115368: Auto smooth node is added every time if the file has
linked object  
[754c168ed3](https://projects.blender.org/blender/blender/commit/754c168ed3):
Fix: Compiler warning about unused variable  
[3f6d1d098c](https://projects.blender.org/blender/blender/commit/3f6d1d098c9b068ade44ea6b5e640b338a42fc89):
NLA: Console warnings about missing channel\_index property  
[3dc119a440](https://projects.blender.org/blender/blender/commit/3dc119a4409cd2b7414db4de5d4bf7cfbead8c43):
Fix \#112767: Outliner unlink operation fails to unlink from scene  
[a7ae024ea3](https://projects.blender.org/blender/blender/commit/a7ae024ea35868b86b83621935a0dc460b1c5076):
Fix \#115171: Transfer Mode operator tries and fails on linked objects  

## Week 131 (Nov 27 - 03)

This week: Triaging. Short week, only worked on Monday.  
Next Week: Triaging.  
**Total actions on tracker: 59**

  - Confirmed: 6

[\#115434](https://projects.blender.org/blender/blender/issues/115434),
[\#115414](https://projects.blender.org/blender/blender/issues/115414),
[\#115450](https://projects.blender.org/blender/blender/issues/115450),
[\#115581](https://projects.blender.org/blender/blender/issues/115581),
[\#115562](https://projects.blender.org/blender/blender/issues/115562),
[\#115424](https://projects.blender.org/blender/blender/issues/115424)

  - Closed: 8

[\#115441](https://projects.blender.org/blender/blender/issues/115441),
[\#113590](https://projects.blender.org/blender/blender/issues/113590),
[\#115593](https://projects.blender.org/blender/blender/issues/115593),
[\#114045](https://projects.blender.org/blender/blender/issues/114045),
[\#113641](https://projects.blender.org/blender/blender/issues/113641),
[\#115407](https://projects.blender.org/blender/blender/issues/115407),
[\#115426](https://projects.blender.org/blender/blender/issues/115426),
[\#115597](https://projects.blender.org/blender/blender/issues/115597)

  - Needs Information from User: 17

<!-- end list -->

  - PR submitted: 0

<!-- end list -->

  - Review: 0

<!-- end list -->

  - Commits: 1

[b1ac047af4](https://projects.blender.org/blender/blender/commit/b1ac047af47821cb7ad1bca787c484db43f409b5):
Cleanup: to\_bools comment

## Week 132 (Dec 04 - 10)

This week: Triaging. General code contribution. Devtalk moderation.  
Some contribution to GPV3, ported [clean loose points
operator](https://projects.blender.org/blender/blender/commit/2e5d4a87993a01357e4e6346f7bbfb896e7b707c)  
Next Week: Triaging.  
**Total actions on tracker: 222**

  - Confirmed: 17

[\#115925](https://projects.blender.org/blender/blender/issues/115925),
[\#115716](https://projects.blender.org/blender/blender/issues/115716),
[\#115486](https://projects.blender.org/blender/blender/issues/115486),
[\#115871](https://projects.blender.org/blender/blender/issues/115871),
[\#115668](https://projects.blender.org/blender/blender/issues/115668),
[\#115609](https://projects.blender.org/blender/blender/issues/115609),
[\#115655](https://projects.blender.org/blender/blender/issues/115655),
[\#115868](https://projects.blender.org/blender/blender/issues/115868),
[\#115699](https://projects.blender.org/blender/blender/issues/115699),
[\#115821](https://projects.blender.org/blender/blender/issues/115821),
[\#115697](https://projects.blender.org/blender/blender/issues/115697),
[\#115727](https://projects.blender.org/blender/blender/issues/115727),
[\#112340](https://projects.blender.org/blender/blender/issues/112340),
[\#115789](https://projects.blender.org/blender/blender/issues/115789),
[\#115880](https://projects.blender.org/blender/blender/issues/115880),
[\#115869](https://projects.blender.org/blender/blender/issues/115869),
[\#90289](https://projects.blender.org/blender/blender/issues/90289)

  - Closed: 27

[\#115704](https://projects.blender.org/blender/blender/issues/115704),
[\#115909](https://projects.blender.org/blender/blender/issues/115909),
[\#115881](https://projects.blender.org/blender/blender/issues/115881),
[\#115868](https://projects.blender.org/blender/blender/issues/115868),
[\#115649](https://projects.blender.org/blender/blender/issues/115649),
[\#115669](https://projects.blender.org/blender/blender/issues/115669),
[\#115704](https://projects.blender.org/blender/blender/issues/115704),
[\#115874](https://projects.blender.org/blender/blender/issues/115874),
[\#115832](https://projects.blender.org/blender/blender/issues/115832),
[\#115694](https://projects.blender.org/blender/blender/issues/115694),
[\#114952](https://projects.blender.org/blender/blender/issues/114952),
[\#115789](https://projects.blender.org/blender/blender/issues/115789),
[\#115436](https://projects.blender.org/blender/blender/issues/115436),
[\#115800](https://projects.blender.org/blender/blender/issues/115800),
[\#115825](https://projects.blender.org/blender/blender/issues/115825),
[\#114899](https://projects.blender.org/blender/blender/issues/114899),
[\#115834](https://projects.blender.org/blender/blender/issues/115834),
[\#107099](https://projects.blender.org/blender/blender/issues/107099),
[\#115706](https://projects.blender.org/blender/blender/issues/115706),
[\#115876](https://projects.blender.org/blender/blender/issues/115876),
[\#115643](https://projects.blender.org/blender/blender/issues/115643),
[\#115909](https://projects.blender.org/blender/blender/issues/115909),
[\#108563](https://projects.blender.org/blender/blender/issues/108563),
[\#115867](https://projects.blender.org/blender/blender/issues/115867),
[\#115713](https://projects.blender.org/blender/blender/issues/115713),
[\#115401](https://projects.blender.org/blender/blender/issues/115401),
[\#115101](https://projects.blender.org/blender/blender/issues/115101)

  - Needs Information from User: 32

<!-- end list -->

  - PR submitted: 5

[PR
\#115923](https://projects.blender.org/blender/blender/pulls/115923):
GPv3: Clean loose points operator  
[PR
\#115926](https://projects.blender.org/blender/blender/pulls/115926):
Fix \#115909: Walk Navigation Up and Down is interrupted by WASD  
[PR
\#115795](https://projects.blender.org/blender/blender/pulls/115795):
GPv3: Crash switching to draw mode for empty object  
[PR
\#115785](https://projects.blender.org/blender/blender/pulls/115785):
GPv3: Missing add layer option in menu when list is empty  
[PR
\#115753](https://projects.blender.org/blender/blender/pulls/115753):
Fix \#115704: Crash when using transfer mode on empty space  

  - Review: 0

<!-- end list -->

  - Commits: 5

[2e5d4a8799](https://projects.blender.org/blender/blender/commit/2e5d4a87993a01357e4e6346f7bbfb896e7b707c):
GPv3: Clean loose points operator  
[b9cbc5b335](https://projects.blender.org/blender/blender/commit/b9cbc5b335936214625140404fa32e170c6f3e32):
Fix \#115909: Walk Navigation Up and Down is interrupted by WASD  
[5a2b8da619](https://projects.blender.org/blender/blender/commit/5a2b8da6193125f1060664014ae8923e3e09ae5e):
Fix: GPv3: Crash switching to draw mode for empty object  
[31ee0a2a3e](https://projects.blender.org/blender/blender/commit/31ee0a2a3e0c1704137c29e188295957d14ab468):
Fix: GPv3: Missing add layer option in menu when list is empty  
[1020a7f6ee](https://projects.blender.org/blender/blender/commit/1020a7f6ee8a286d1940296ee22e85d0f8037b68):
Fix \#115704: Crash when using transfer mode on empty space  

## Week 133 (Dec 11 - 17)

**Total actions on tracker:**

  - Confirmed:
  - Closed as Archived:
  - Closed as Duplicate:
  - Closed as Resolved:
  - Needs Information from User:
  - Needs Information from Developers:
  - Diff submitted:
