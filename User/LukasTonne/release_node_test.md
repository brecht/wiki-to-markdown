  - **Node panels** feature for node groups and built-in nodes
    ([\#108895](https://projects.blender.org/blender/blender/issues/108895))
      -   
        Panels can be used to group sockets in a collapsible section.
        They can be added to node groups and to built-in nodes.
  - Group sockets are managed in new **UI tree view** together with
    panels. Drag-and-drop support for ordering sockets and inserting
    into panels.
  - Breaking API changes:
      - Node group API moves from
        [\`NodeTree\`](https://docs.blender.org/api/3.6/bpy.types.NodeTree.html)
        to
        [\`NodeTree.interface\`](https://docs.blender.org/api/4.0/bpy.types.NodeTreeInterface.html).

<table>

<tr>

<td>

<b>old</b>

</td>

<td>

<b>new</b>

</td>

</tr>

<tr>

<td>

``` 
    # Make a socket
    tree.inputs.new(name="My Input")
    tree.outputs.new(name="My Output")
    
    
    
    
    
    
    
    
    # Remove a socket
    tree.inputs.remove(socket)
    tree.outputs.remove(socket)
    
    # Move a socket up or down
    tree.inputs.move(from_index=4, to_index=2)
    
        
    
    # Iterate over sockets
    for socket in tree.inputs:
        ...
    for socket in tree.outputs:
        ...
        
        
        
        
        
        
    
```

</td>

<td>

``` 
    # Make a socket
    tree.interface.new_socket(name="My Input", in_out='INPUT')
    tree.interface.new_socket(name="My Output", in_out='OUTPUT')
    # Make pass-through socket
    tree.interface.new_socket(name="My Socket", in_out='BOTH')
    # Make node panel
    tree.interface.new_panel(name="My Panel")
    
    # Copy an existing socket or panel
    tree.interface.copy(socket)
    
    # Remove a socket or panel
    tree.interface.remove(socket)
    tree.interface.remove(panel)
    
    # Move a socket up or down
    tree.interface.move(socket, to_index=2)
    # Move a socket into a panel
    tree.interface.move_to_parent(socket, new_panel, to_index=2)
    
    # Iterate over sockets
    for item in tree.interface.items_tree:
        if item.item_type == 'SOCKET':
            if item.in_out == 'INPUT':
                ...
            elif item.in_out == 'OUTPUT':
                ...
    # Iterate over panels
    for item in tree.interface.items_tree:
        if item.item_type == 'PANEL':
            ...
    
```

</td>

</table>

  - Custom nodes (aka. "python nodes"):
    \`NodeSocket.draw\_color\_simple\` callback becomes the preferred
    color function for custom socket types.
      -   
        It does not take a context or node instance but that is
        sufficient for most use cases. The new callback is used in cases
        where concrete node instance exists, for example drawing node
        group interfaces.

<table>

<tr>

<td>

<b>old</b>

</td>

<td>

<b>new</b>

</td>

<tr>

<td>

``` 
    class MyCustomSocket(NodeSocket):
        def draw_color(self, context, node):
            return (1, 1, 1, 1)
    
```

</td>

<td>

``` 
    class MyCustomSocket(NodeSocket):
        @classmethod
        def draw_color_simple(cls):
            return (1, 1, 1, 1)
    
```

</td>

</tr>

</tr>

</table>
