# 2023

## Calendar Week 10

Mar 6 - Mar 12

### Development

  - PR for matrix nodes
      - <https://projects.blender.org/blender/blender/pulls/105408>
  - Proposal for expandable sockets
      - <https://projects.blender.org/blender/blender/issues/105491>
  - Fixed NaNs in curve editing collision solver
      - <https://projects.blender.org/blender/blender/pulls/105499>
  - Fixed crash on node updates with “undefined” node tree types
      - <https://projects.blender.org/blender/blender/pulls/105564>  
        Regression test is in review to avoid issues like this in
        future  
        <https://projects.blender.org/blender/blender/pulls/104696>
  - Comb brush experiment to make it feel more realistic and
    controllable with "keyhole constraints"
      - <https://projects.blender.org/blender/blender/issues/105722>

### Planning

  - Geometry Nodes & Simulation Workshop
      - <https://devtalk.blender.org/t/2022-03-10-nodes-physics-workshop/28231>

### Next Week

  - Document expected behavior of caches in simulation nodes
      - <https://projects.blender.org/blender/blender/issues/105724>
  - Write detailed design document for "Realtime Physics"
      - <https://projects.blender.org/blender/blender/issues/105732>
  - Working prototype for constraint-based comb brush, determine
    feasibility

## Calendar Week 11

Mar 13 - Mar 19

### Development

  - Comb brush prototype demonstration
      - <https://projects.blender.org/blender/blender/issues/105722>
  - Fixed viewer crash in pinned node trees that are not used by the
    active object
      - <https://projects.blender.org/blender/blender/pulls/105826>
  - Fixed inconsistent viewer node connection cycling
      - <https://projects.blender.org/blender/blender/pulls/105836>

### Code Review

  - <https://projects.blender.org/blender/blender/pulls/105696>

### Design

  - Design doc for caching behavior
      - <https://projects.blender.org/blender/blender/issues/105724>
  - Discussed physics-based animation tools and pros/cons of using
    Bullet with animation module members

### Next Week

  - Clean up new comb brush, get artist feedback, open PR
      - <https://projects.blender.org/blender/blender/pulls/105769>

## Calendar Week 12

Mar 20 - Mar 26

### Development

  - Implemented simplified bending constraint for curve stiffness
      - <https://projects.blender.org/blender/blender/issues/105722#issuecomment-904547>
  - Investigated crash when opening the Node Add menu using keyboard
    shortcuts.
      - Julian takes over after we narrowed it down to memory corruption
        due to a recent asset change
        <https://projects.blender.org/blender/blender/issues/105855>
  - Made a notes page and opened a ToDo issue to plan initial “realtime
    clock” changes:
      - <https://hackmd.io/i0Sa8tNZSUaKsgL8gwG5xQ#>  
        <https://projects.blender.org/blender/blender/issues/105732>
  - Started work on a realtime clock prototype branch.
      - <https://projects.blender.org/LukasTonne/blender/src/branch/geometry-nodes-simulation-realtime-clock>  

## Calendar Week 13

Mar 27 - Apr 2

### Planning

  - Organizing work on realtime clock prototype into several tasks:
      - UI, timer, operators, scene data:
        <https://projects.blender.org/blender/blender/issues/105732>
      - Locking of scene animation time and realtime clock:
        <https://projects.blender.org/blender/blender/issues/106182>
      - Test branch using the realtime clock with simulation nodes:
        <https://projects.blender.org/blender/blender/issues/106178>
  - Started proposal for better top-level building blocks using
    (geometry) nodes:
      - <https://hackmd.io/@lukas-tonne/r1WGoQmWh>

### Development

  - Another small fix for viewer node socket cycling:
      - <https://projects.blender.org/blender/blender/pulls/106318>
  - Finished experimental support for the "realtime clock" in the
    simulation nodes branch:
      - <https://projects.blender.org/blender/blender/issues/106178>
  - Implemented a particle simulation with geometry nodes and tested it
    in combination with animation and the realtime clock:

![Animated cartoon loco with particle sim running on the realtime
clock](../../../images/ParticleSimRealtimeClockDemo.png
"Animated cartoon loco with particle sim running on the realtime clock")

## Calendar Week 14

Apr 3 - Apr 9

  - Attempted fix for
    [\#94235](https://projects.blender.org/blender/blender/issues/94235).
    Partially successful, but multithreading with our Bullet wrapper is
    so unsafe that baking before rendering is the only realistic option
    at this point.
  - [Fix \#103571: Force field absorption for rigid
    bodies](https://projects.blender.org/blender/blender/pulls/106503)
  - Freeze node discussion [Geometry Nodes: Freeze Node
    \#106497](https://projects.blender.org/blender/blender/issues/106497)
  - PR for [adding simulation input/output node pair with a single menu
    entry
    \#106557](https://projects.blender.org/blender/blender/pulls/106557)
  - PR for [removing simulation input/output
    together](https://projects.blender.org/blender/blender/issues/105728)
  - Working on support for copy operators with simulation input/output
    [\#106732](https://projects.blender.org/blender/blender/issues/106732)

## Calendar Week 15

Apr 10 - Apr 16

### Development

  - Copy support for simulation zone nodes
    [\#106812](https://projects.blender.org/blender/blender/pulls/106812)
  - Added task to unify copy operators
    [\#106852](https://projects.blender.org/blender/blender/issues/106852)

### Code Review

  - Node add searches missing context-based poll
    [\#106829](https://projects.blender.org/blender/blender/pulls/106829)
  - Initialize socket value by declaration callback if value is data
    block
    [\#106816](https://projects.blender.org/blender/blender/pulls/106816)

## Calendar Week 16

Apr 17 - Apr 23

  - Postponed copy operator unification. Should be supported by python
    tests and is more work than can be justified atm
    [\#106852](https://projects.blender.org/blender/blender/issues/106852)
  - Finished attribute field type propagation in simulation zones
    [\#107133](https://projects.blender.org/blender/blender/pulls/107133)
  - Finished UI for simulation zone items in the node editor
    [\#106919](https://projects.blender.org/blender/blender/pulls/106919)
  - Started work on attribute propagation support in simulation zones
    [\#107219](https://projects.blender.org/blender/blender/issues/107219)

## Calendar Week 17

Apr 24 - Apr 30

  - Working on baking operators and general cache info panel for
    simulation nodes
    [\#107290](https://projects.blender.org/blender/blender/issues/107290)
  - Vacation Apr 27 - May 2

## Calendar Week 18

May 1 - May 7

  - Moved weekly notes into a yearly subfolder
    [LukasTonne/WeeklyReports/2023](https://wiki.blender.org/wiki/User:LukasTonne/WeeklyReports/2023)
  - Calculate-to-frame operator for setting simulation nodes up to the
    current frame without baking
    [\#107627](https://projects.blender.org/blender/blender/pulls/107627)
  - Fix for crashes due to unsafe simulation node cache access by
    editors
    [\#107621](https://projects.blender.org/blender/blender/pulls/107621)
  - Fix for crash when connecting viewer in a pinned node tree without
    object context
    [\#107737](https://projects.blender.org/blender/blender/pulls/107737)
  - Documenting LOD system in geometry nodes (WIP, further work needed
    to solve normal continuity)
    ![../../../images/LOD\_frustum.jpg](../../../images/LOD_frustum.jpg
    "../../../images/LOD_frustum.jpg")
    ![../../../images/LODgeonodes.jpg](../../../images/LODgeonodes.jpg
    "../../../images/LODgeonodes.jpg")

## Calendar Week 19

May 8 - May 14

  - Added support for simulation zones in node group operators (make
    group, ungroup, insert, separate)
    [\#107807](https://projects.blender.org/blender/blender/pulls/107807)
  - Removed versioning code for older geonodes simulation zones
    [\#107781](https://projects.blender.org/blender/blender/pulls/107781)
  - Wrote proposal for adding "function socket" as a concept: Node
    graphs get wrapped as closures, can be passed around node trees, and
    evaluated at a later stage. This will allow much greater
    customization of node assets without user having to tweak complex
    node trees.
    [\#107842](https://projects.blender.org/blender/blender/pulls/107842)

## Calendar Week 20

May 15 - May 21

  - Mo-We: Workshop for node-based tools and brushes design with Dalai,
    Jacques, Andy, Simon
  - Working on a prototype for a fracturing system in geometry nodes
    ![../../../images/Screenshot\_20230522\_122826.png](../../../images/Screenshot_20230522_122826.png
    "../../../images/Screenshot_20230522_122826.png")

## Calendar Week 21

May 22 - May 28

  - Fix to prevent creation of unnecessary cache folders (simulation
    zone exists but is not in use)
    [\#108278](https://projects.blender.org/blender/blender/pulls/108278)
  - Add user warning to detect when caches might overwrite existing data
    on disk
    [\#108288](https://projects.blender.org/blender/blender/pulls/108288)
  - Updated Embree test branch and started discussion about benefits and
    potential issues of replacing Blender's BVH implementation
    [\#108148](https://projects.blender.org/blender/blender/pulls/108148)

## Calendar Week 22

May 29 - Jun 4

  - Fix for socket shape when copying between different node tree types
    [\#108412](https://projects.blender.org/blender/blender/pulls/108412)
  - Sub-panel support for node group socket categories:

` - Add categories to node tree interfaces for organizing large numbers of inputs`  
` - Support instancing of child panels (remove hidden assumption that child panels are always type-based)`  
` `[`#108565`](https://projects.blender.org/blender/blender/pulls/108565)

## Calendar Week 23

Jun 5 - Jun 11

  - Splitting PR for socket sub-panels into manageable pieces:

` - Category declaration in node trees and sockets `[`#108649`](https://projects.blender.org/blender/blender/pulls/108649)  
` - Return bit arrays as persistent expand flags for panels [8https://projects.blender.org/blender/blender/pulls/108648 #108648]`  
`   _Dropped after changes were postponed until callbacks could support BitVector (c++)_`  
` - Use instanced child panels to display node socket categories `[`#108565`](https://projects.blender.org/blender/blender/pulls/108565)  
`   _(depends on and includes both PRs above)_`

  - Fix for socket display shapes
    [\#108412](https://projects.blender.org/blender/blender/pulls/108412)
  - Bug report: assert failure when working within large asset libraries
    [\#108703](https://projects.blender.org/blender/blender/issues/108703)

## Calendar Week 24

Jun 12 - Jun 18

  - Panel declaration experimental feature
    [\#108649](https://projects.blender.org/blender/blender/pulls/108649)
  - Committed C++ RNA for object, curves, nodes [rna-cpp
    branch](https://projects.blender.org/LukasTonne/blender/src/branch/rna-cpp)

## Calendar Week 25

Jun 19 - Jun 25

  - Published preliminary API design for new group interface data
    structure
  - Discussion on containers, socket/panel ordering, backward
    compatibility

` `[`#109158`](https://projects.blender.org/blender/blender/pulls/109158)

  - Implementing tree view for the new data structure
  - PR for cleaning up color access API of node sockets

` `[`#109288`](https://projects.blender.org/blender/blender/pulls/109288)

  - Finished warnings for bake operator when writing to the same
    directory

` `[`#108263`](https://projects.blender.org/blender/blender/issues/108263)

## Calendar Week 26

Jun 26 - 2 Jul

  - After discussion with Brecht, Jacques, and Hans decided to change
    DNA design of the node group interfaces to a hierarchical structure.
    Takes a bit longer than anticipated ...

` `[`#109158`](https://projects.blender.org/blender/blender/pulls/109158)

  - Review: Serial loop PR by Jacques

` `[`#109164`](https://projects.blender.org/blender/blender/pulls/109164)

## Calendar Week 27

Jun 3 - 9 Jul

  - Vacation 5-9
  - Working on node socket panels

## Calendar Week 28

Jul 10 - 16 Jul

  - Vacation 10-12
  - Working on node socket panels

## Calendar Week 29

Jul 17 - Jul 23

  - Still working on node socket panels

` Discussed how forward compatibility can be implemented, settled on doing write-time versioning in 4.0 so older versions have data to read.`  
` Decided with Brecht to discard the old RNA API to make things easier.`  
` Todo list: `[`#110272`](https://projects.blender.org/blender/blender/pulls/110272)

## Calendar Week 30

Jul 24 - Jul 30

[Node
panels](https://projects.blender.org/blender/blender/pulls/110272):

  - Implemented backward + forward compatibility for node group
    interfaces.
  - Deprecated old \`bNodeTree\` sockets DNA, RNA
  - Replaced uses of old node group interface.
  - Added custom socket type support for new node group interface items.
  - Discovered that the \`interface\_init\_socket\` API method is no
    longer used, this may have been an unintended breaking change
    introduced by node declarations. [-\>
    Discussion](https://projects.blender.org/blender/blender/issues/109135#issuecomment-989637)

## Calendar Week 31

Jul 31 - Aug 6

  - Finished most remaining work on socket panels
    [\#110272](https://projects.blender.org/blender/blender/pulls/110272)
  - Added python tests for the new node group interfaces
  - Started splitting into parts for easier review, part 1:
    [\#110272](https://projects.blender.org/blender/blender/pulls/110272)

## Calendar Week 32

Aug 7 - Aug 13

  - Finished and committed part 1/3 of the node socket panels patch
    [\#110885](https://projects.blender.org/blender/blender/pulls/110885)
  - Vacation from Aug 10

## Calendar Week 33

Aug 14 - Aug 20

  - Vacation until Aug 16
  - Finished and committed part 2/3 of the node socket panels patch
    [\#110952](https://projects.blender.org/blender/blender/pulls/110952)

## Calendar Week 34

Aug 21 - Aug 27

  - Final part 3/3 of the node socket panels patch in review
    [\#111348](https://projects.blender.org/blender/blender/pulls/111348)

` Fixed tests, UI drawing, and other issues found during code review.`

## Calendar Week 35

Aug 28 - Sep 3

Bug fixes for node panels

  - [Fix \#111381: Crash when inserting a node onto a link inside a
    frame](https://projects.blender.org/blender/blender/pulls/111383)
  - [Fix \#111713: Nodes crash when overriding due to item.parent
    nullptr](https://projects.blender.org/blender/blender/pulls/111707)
  - [Fix \#111713: Nodes crash when library
    overriding](https://projects.blender.org/blender/blender/pulls/111782)
  - [Fix: socket hidden in the link drag operator for group input
    nodes](https://projects.blender.org/blender/blender/pulls/111707)
  - [Fix \#111763: Incorrect string copy function for
    default\_attribute\_name](https://projects.blender.org/blender/blender/pulls/111823)
  - [Fix \#111817: Use new interface API in move-to-nodes
    operator](https://projects.blender.org/blender/blender/pulls/111824)
  - [Fix \#111721: Node interface root panel flag
    missing](https://projects.blender.org/blender/blender/pulls/111828)
  - [UI: Add a context argument to tree view item rename
    functions.](https://projects.blender.org/blender/blender/pulls/111522)

Also fixed related issues in Node Wrangler addon caused by breaking API
changes

  - [Node wrangler: Node socket interface class
    changed](https://projects.blender.org/blender/blender-addons/pulls/104849)
  - [Node Wrangler: Fix outdated menu names that break addon loading
    tests](https://projects.blender.org/blender/blender-addons/pulls/104862)

## Calendar Week 36

Sep 4 - Sep 10

More bug fixes for node panels

  - [Fix versioning old 2.6 node groups, causing dangling link
    pointers](https://projects.blender.org/blender/blender/pulls/111704)
  - [Fix \#111950: missing "hide value" and "compact" flags for group
    sockets](https://projects.blender.org/blender/blender/pulls/111956)
  - [Fix \#111963: Copying sockets was not creating unique
    identifiers](https://projects.blender.org/blender/blender/pulls/111972)
  - [Fix \#111804: Remove padding between node sockets when
    hidden](https://projects.blender.org/blender/blender/pulls/111981)
  - [Fix \#111936: Nodes forward compatibility data is not
    freed](https://projects.blender.org/blender/blender/pulls/111989)
  - [Fix \#111966: Node group panels too small when using "both" type
    sockets](https://projects.blender.org/blender/blender/pulls/112013)
  - [Fix \#111786: Node panels header blocks resizing the node
    border](https://projects.blender.org/blender/blender/pulls/112017)
  - [Fix \#112016: Sockets in closed node panels still
    draggable](https://projects.blender.org/blender/blender/pulls/112019)
  - [Fix \#112014: Crash making node group with multiple input
    links](https://projects.blender.org/blender/blender/pulls/112027)
  - [Fix \#111788: Label width needs to use UI scale to match node
    width](https://projects.blender.org/blender/blender/pulls/112047)
  - [Fix \#111963: Followup fix to ensure sockets inside panels have
    unique
    identifiers](https://projects.blender.org/blender/blender/pulls/112074)
  - [Fix \#112073: Add all group outputs first, followed by all
    inputs](https://projects.blender.org/blender/blender/pulls/112076)
  - [Fix \#111969: Remove gaps between panels and fill in the
    background](https://projects.blender.org/blender/blender/pulls/112078)

Added a new test to finally have some checks on complicated node
versioning.

  - [New test for node group
    versioning](https://projects.blender.org/blender/blender/pulls/111800)

## Calendar Week 37

Sep 11 - Sep 17

More bug fixes for node panels

  - [Fix \#112331: Add update tags directly in bNodeTreeInterface API
    methods](https://projects.blender.org/blender/blender/pulls/111741)
  - [Fix node group socket order when making a group from a single
    node](https://projects.blender.org/blender/blender/pulls/112226)
  - [Nodes: Draw node buttons between outputs and inputs for custom
    layouts](https://projects.blender.org/blender/blender/pulls/112228)
  - [Nodes: Support for input/output sockets in same vertical
    space](https://projects.blender.org/blender/blender/pulls/112250)
  - [Fix for node group "make" adding sockets in wrong
    order](https://projects.blender.org/blender/blender/pulls/112281)
  - [Fix \#112247: Node group socket declarations using only 'NONE'
    subtype](https://projects.blender.org/blender/blender/pulls/112286)
  - [Fix \#112233: Panel collapsed state should not hide socket
    links](https://projects.blender.org/blender/blender/pulls/112326)
  - [Fix \#112330: Group sockets not initialized when created by
    link-drag](https://projects.blender.org/blender/blender/pulls/112337)
  - [Node drawing: Fix unused variables in release
    builds](https://projects.blender.org/blender/blender/pulls/112384)

Support for panels in built-in nodes

  - [Nodes: Add PanelDeclarationBuilder for panels in builtin
    nodes](https://projects.blender.org/blender/blender/pulls/111695)

## Calendar Week 38

Sep 18 - Sep 24

Reverted the "inline sockets" feature after it was decided that this
feature would break Blender UI conventions

  - [Nodes: revert the inline (pass-through) socket
    feature](https://projects.blender.org/blender/blender/pulls/112560)

And some final fixes for node panels

  - [Fix \#112462: Panel buttons remain active after
    redrawing](https://projects.blender.org/blender/blender/pulls/112503)
  - [Fix \#112490: Always draw socket icons in "hidden"
    nodes](https://projects.blender.org/blender/blender/pulls/112520)
  - [Fix \#112521: Socket in\_out type specified as a simple
    enum](https://projects.blender.org/blender/blender/pulls/112525)
  - [Nodes: Rename ui\_items property to
    items\_tree](https://projects.blender.org/blender/blender/pulls/112527)
  - [Node Wrangler: ui\_items collection renamed to
    items\_tree](https://projects.blender.org/blender/blender-addons/pulls/104896)
  - [Fix \#112622: draw\_color\_simple is optional for custom
    nodes](https://projects.blender.org/blender/blender/pulls/112658)
  - [Fix incorrect item position returned for items in the root
    panel](https://projects.blender.org/blender/blender/pulls/112714)
  - [Cleanup: Nodes: Simplify drawing
    function](https://projects.blender.org/blender/blender/pulls/112860)
  - [Fix \#112588: Hide node panels when all sockets are
    hidden](https://projects.blender.org/blender/blender/pulls/113003)

## Calendar Week 39

Sep 25 - Oct 1

  - Experimental branch for grids as volume component attributes:
    [volume-component-attributes](https://projects.blender.org/LukasTonne/blender/src/branch/volume-component-attributes)
  - Experimental branch for point cloud storage in
    \`openvdb::PointDataGrid\`:
    [point-cloud-grid](https://projects.blender.org/LukasTonne/blender/src/branch/point-cloud-grid)
  - Simplified node drawing function to have better control over drawing
    state:
    [\#112860](https://projects.blender.org/blender/blender/pulls/112860)
  - Closed
    [\#109288](https://projects.blender.org/blender/blender/pulls/109288)
    since this can be done using the \`draw\_color\_simple\` callback
    now.
  - Worked on [Subpanels for node group
    sockets](https://projects.blender.org/blender/blender/pulls/108565)
  - Reviewed [Store tool group in a separate editor
    property](https://projects.blender.org/blender/blender/pulls/112926)
  - Reviewed [Mesh: Add viewport normals simplify
    option](https://projects.blender.org/HooglyBoogly/blender/pulls/5)
  - [Nodes: ensure consistent outputs..inputs ordering for
    sockets](https://projects.blender.org/blender/blender/issues/113014)
  - [Fix \#112588: Hide node panels when all sockets are
    hidden](https://projects.blender.org/blender/blender/pulls/113003)
  - [Node groups: Add undo steps after drag and drop of interface
    items](https://projects.blender.org/blender/blender/pulls/113061)
  - [Fix \#113013: Make in\_out socket property read-only and remove the
    button](https://projects.blender.org/blender/blender/pulls/113048)
