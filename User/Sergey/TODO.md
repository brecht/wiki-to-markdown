# Personal TODO list

Here's personal my TODO list and list of projects i would like to work
on

## Motion Tracking

Most of the projects from here are also depend on help from Keir

  - Make Follow Track aware of panoramic cameras
  - ~~Weighted tracks (a way to say solver some tracks are not so
    good)~~
  - Relative tracks depth (define which track is in front of others)
  - Variable focal length support
  - Survey data support
  - Multicamera reconstruction
  - Improve scene orientation operators
  - Better stabilization tools (i.e. based on reconstructed camera
    curve)
  - Tracker-based segmentation
  - Edge tracker
  - Automatic reconstruction pipeline
  - Light estimation
  - Support of multilayer EXR sequence as movie clip datablock
  - Rolling shutter compensation
  - Lens estimation
  - Grid calibration
  - Single frame reconstruction
  - Dense surface reconstruction

<!-- end list -->

  - ~~Automatic keyframe selection~~
  - ~~Multithreaded proxies build~~

Smaller tasks:

  - Choose unit for sensor size (mm, inch), requested by some users
  - Different grease pencil strokes displaying in different modes
  - ~~Frames prefetching and multi-threaded load~~
  - Auto keyframe by default

## Cycles

  - ~~Better API integration (support of function calls from C++ RNA,
    check on how Python API could be extended)~~
  - ~~"Persistent" textures in memory (keep textures in GPU in-between
    rendering to save time initialize rendering)~~
  - ~~Movies and packed images support~~
  - ~~Tile size in pixels rather than in number per dimension~~
  - ~~Highlight currently rendering tiles~~
  - Overscan support
  - Rethink preview.blend stuff
  - Write sample render engine demonstrating a way of integrating into
    Blender
  - Lens distortion (also for Blender Internal) -- more realistic VFX
  - Baking
  - ~~Memory usage overview (for GPU and CPU)~~
  - ~~Material preview panel~~

## Color Management

  - ~~Sequencer strips input space~~
  - Color picking color space support
  - Texture painting color space support
  - Better support of configuration for render farm (i.e. packed OCIO
    configuration or so)

## Mask editing

  - Mask editing from sequencer and compositor

## Other areas and smaller projects

  - Wacom Intuos 5 support, fixes and improvements
  - ~~Switch all image blocks to common caching mechanism~~
  - Review FFmpeg presets, make them more clear from interface point of
    view
  - Remove unwanted internal buffers used for video decoding to enhance
    caching policy
  - Delete Lower for multiresolution
  - Make it possible to show sculpting data on multires level 0
  - Automatic image sequence offset guessing
  - Zoom to mouse location in image editor
  - ~~Option to apply taper object on beveled area, taking bevel factor
    into account~~

## Cleanup

  - Bevel Points are confusingly different for 3D and 2D cases. Could be
    nice to make this code unified.
  - Flags CU\_2D for spline and CU\_3D for curve are confusing. Better
    naming would help without damaging current .blend files.
