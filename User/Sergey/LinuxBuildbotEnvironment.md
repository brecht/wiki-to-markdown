In this page I'll try to describe configuration of environment, used for
release builds and for nightly builds making with buildbot. It wouldn't
be a description how to build ideal build environment -- it'll just be
more a description how I configured system to create builds. Of course,
you could easily don't follow it for 100% (i.e. use another Linux
distributive, or don't use virtual machine), but I'll try to describe
all troubles I've meet when was trying to create ideal release build.

### System

I'm using Debian Squeeze amd64 system running under VirtualBox.

<strong>Why virtual machine?</strong>

Virtual machine is needed for easier deploying to another servers
(currently system is running at my home server machine, but it's planned
to be moved to one of machines in studio). Also, it provides snapshoting
functionality. So i could create a snapshot, make some tests inside
build environment and easily revert all changes if this would wrong or
strangely (mainly it happens when you're upgrading used libraries --
it'll be much easier to revert snapshot rather than remembering all
changes you made).

<strong>Why Debian Squeeze?</strong>

This question question could be splitted into two parts: why Debian and
why Squeeze. So, I'm using Debian for \~9 years and it's just convenient
and habitual to work in this Linux distribute. And I prefer using stable
versions of Debian for systems which should be available 24/7 without
worrying about package updates -- there'll be only security updates
which doesn't break stuff.

But unfortunately, Squeeze would be a bad environment for release builds
(I'll write about this a bit later), but using Lenny caused plenty of
problems at my home -- especially when I've been using NFS shares.
Kernel quite often used to catch "kernel oops", which wasn't cool at
all.

<strong>Why AMD64?</strong>

We need to create both of 32 and 64 bit builds, and using one AMD64
platform gives advantage, then 32bit chroot environment could be run
inside it, no need in two virtual machines. And AMD64 is just a bit
faster :)

### Chroot

As I told a bit earlier, Squeeze wound't be a perfect build environment.
Main problem comes from glibc. Our policy is to support
hardware/software for ten years period, and glibc is only one side
compatibility -- you could easily run program linked against earlier
version of glibc, but not in other direction. Static linking against
glibc was a bit problem, which caused segmentation faults for some
cases. I've heard something about posibility to link against older glibc
even if your system has got newer, but I've forgot when and where it
was. If you point me there -- it'll be useful.

So, we need environment with glibc2.7. It's a Debian Lenny\! But as I
wrote, Lenny was too buggy for me to use as host system, so I decided to
use chroot-s. One more advantage -- it makes possible to have one host
system for all kinds of builds (32/64 bits, maybe different versions of
glibc in the future).

<strong>schroot</strong>

Using "raw" chroot isn't very cool -- it requires root privileges and if
you're using schroot you shouldn't even use sudo (after configuration,
of course).

So, I used debootstrap to create two lenny chroot environments and set
them up in schroot config file so I (and some more admins in system)
could easily enter this environment.

<strong>Packages</strong>

First step was is to make Blender compile with "out-of-box"
configuration files. Some development libraries were necessary for this.
I can't remember which exactly, but I could easily publish all packages
installed into chroot environment with versions of packages. But simply,
if you can't compile Blender -- you've missed some libraries :) And I
haven't used any backports site or non-debian official repositories (I'm
using <http://ftp.de.debian.org/> mirror here -- it works much nicer
than others). Btw, it could be very useful to disable installation of
recommended packages -- it saves disk usage.

One note which is necessary to mark here -- I've used python compiled
from official original sources with such flags:

``` bash

./configure --prefix=/opt/python3.2 \
  --enable-ipv6 \
  --enable-loadable-sqlite-extensions \
  --with-dbmliborder=bdb \
  --with-wide-unicode \
  --with-computed-gotos
```

I have also enabled \_md5, \_sha1, \_sha256 and \_sha512 modules in
Modules/Setup. There were some "mistakes" here -- there's no md5.c file
and it should be md5module.c only for \_md5; \_sha should be replaced
with \_sha1 and shamodule with sha1module. I run ./configure script
after this again to get Modules/configure.c re-created with correct
settings. When compilation finished, it told me only modules \_dbm,
\_gdbm and \_tkinte haven't been compiled. If you've got more modules
uncompiled, then some functions could be missed in addons and they could
be unworkable at other platforms.

<strong>Static linking</strong>

After I've been able to compile blender with out-of-box settings, time
cane to make some libraries static. It's needed to prevent versions
conflicts when at some platform it should be libjpeg.so.62 and at others
libjpeg.so.2 (and so on).

Easiest way is to use <strong>scons</strong> for this. There're plenty
of WITH\_<blah-blah>\_STATIC here. But it wouldn't be enough, for
example there's no way to link expat/xml/jpeg/etc libraries staticly.
I've used a small hack for this: I've created special folder where I've
created symlinks to libraries I wanted to link against staticly (there
\_are\_ libjpeg.a and other .a files in /usr/lib) and named them
libxml-static.a, libexpat-static.a and so on. After this I've set
BF\_<FEATURE>\_LIBPATH to folder with this symlinks and set
BF\_<FEATURE>\_LIB to library with -static suffix. Content of this
folder is:

    <nowiki>
    libexpat-static.a
    libgomp.a
    libjpeg.a
    libpcre-static.a
    libpng.a
    libxml2-static.a
    </nowiki>

Actually, not all libraries have got -static suffix. Not sure why
sometimes linking happens against dynamic version and sometimes against
static -- depends on path order, which I'm not sure about, and for cases
when this happens I've used this -static suffix approach.

To find out which libraries I'm linking staticly against and which
features we've got in release builds, you could check rules from
build\_files/buildbot/config.

<strong>OpenCOLLADA</strong>

Maybe I should write about this a bit earlier, but in release builds
we've got COLLADA support. I used such simple command to build
OpenCOLLADA:

``` bash
scons RELEASE=1 NOVALIDATION=1 XMLPARSER=libxmlnative PCRENATIVE=1 SHAREDLIB=0 -j 2
```

I used native libxml and libpcre (from system) because there were some
troubles with using libraries from OpenCOLLADA sources and I haven't got
much time to solve this problems. That's why we need xml/pcre libraries
to be linked against staticly. Can't remember why expat is needed, but
it's not from OpenCOLLADA, it's OpenEXR, IIRC.

To install all libraries and include files into /opt/opencollada I've
used a very stupid script which makes search for .h and .a files in
source folder and copies them to /opt folder (do not forgot to strip
them\!).

<strong>ffmpeg</strong>

We've sticked to version from revision, when ffmpeg was removed from
main source tree of blender. It's caused by quite simply problem: it's
quite difficult to backport very-new version of ffmpeg to Debian Lenny.
And that version, that comes in Lenny's repo (even if you're using
debian-multimedia repo) has got very bad behavior -- it adds unneded
pixelirezationm we don't need this at all. Maybe after 2.57 release I'll
update ffmpeg to the most recent version, but I don't want to to this in
RC state.

<strong>OpenAL</strong>

We've got crash during playback when OpenAL was enabled in user
preferences. We thought it's because of old OpenAL library so we
upgraded it:

``` bash
cmake -DCMAKE_VERBOSE_MAKEFILE=ON \
      -DCMAKE_INSTALL_PREFIX=/opt/openal
      -DCMAKE_BUILD_TYPE:String=Release
      -DALSOFT_CONFIG=ON \
      -DLIBTYPE=STATIC ...
```

It wasn't necessary at all, because crash was caused because of
-lpthread and -static-libgcc flags we've been using, but here it is :)

<strong>blenderplayer</strong>

So, I think all general things I've already described, if not you could
collect knowledge from scons' rules from build\_files/buildbot/config
folder (absolutely the same rules are using for release builds).

We're using different rules for blenderplayer to reduce size of
executable file. Some features are just disabled for player (like
collada).

You could easily learn difference between this two configurations after
comparsion rules form that config/ folder.

<strong>Software GL</strong>

We've got some troubles with 32bit systems and blender-softwaregl script
since 2.5alpha0. It was caused by stupid mistake -- both of 64 and 32
bit builds were using 64bit version of mesa libraries.

The easiest way was to get libraries from 2.49b (they're absolutely the
same version), but plenty of bugs were fixed since that time in Mesa. So
I've decided to upgrade this libs.

It was very difficult to do in Lenny -- the whole X sybsytem should be
backported. Not cool at all, so I've created two more chroot-s with
Debian Sid where I've compiled Mesa.

Here's configuration line for 32bit platform:

``` bash
./configure CC="gcc -Wl,--as-needed" CXX="g++ -Wl,--as-needed" LDFLAGS="-pthread -static-libgcc" --enable-motif --with-driver=xlib --prefix=/opt/mesa --enable-32-bit --build=i486-linux-gnu
```

For 64bit you should simply remove --enable-32-bit
--build=i486-linux-gnu.

One thing here should be noted I think, i've installed libraries listed
in Build-Deps section of mesa package from Debian to create library wit
hse same feature set as Debian uses.

<strong>Build scripts</strong> Scripts for nightly builds are in
build\_files/buildbot folder. I'll ask other admins of build system and
try to share scripts we're using in this BuildBot virtual machine.

### Additional info

You could learn more things about configuration from:

  - build\_files/buildbot/ folder in our main source tree
  - mail me to g.ulairi AT gmail DOT com
  - our irc channel \#blendercoders at irc.freenode.net (my nick there
    is nazg-gul)

Hope this info was useful for you and if I'll notice that I've missed
something in this article -- it'll be updated.

\-Sergey-
