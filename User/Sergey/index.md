  - Work
      - Blender Foundation Work
          - [Year 2011](Foundation/2011.md)
          - [Year 2012](Foundation/2012.md)
          - [Year 2013](Foundation/2013.md)
          - [Year 2014](Foundation/2014.md)
          - [Year 2015](Foundation/2015.md)
          - [Year 2016](Foundation/2016.md)
          - [Year 2017](Foundation/2017.md)
          - [Year 2018](Foundation/2018.md)
          - [Year 2019](Foundation/2019.md)
          - [Year 2020](Foundation/2020.md)
          - [Year 2021](Foundation/2021.md)
          - [Year 2022](Foundation/2022.md)
          - [Year 2023](Foundation/2023.md)

<!-- end list -->

  - Drafts
      - [Draft: Subdivision
        Surface](DraftSubdivisionSurface.md)
      - [Draft: Multiresolution
        Surface](DraftMultiresolutionSurface.md)
      - [Draft: Dependency
        Graph](DraftDependencyGraph.md)
