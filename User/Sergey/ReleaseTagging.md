In this page i'd want to collect all the notes about how release process
happens behind d the scenes, covering all the cases with branching,
tagging, AHOYing and so.

# Process overview

For the final releases and all the release candidates we're using tags.
They are really helpful to grab working tree at a state which fully
corresponds to the state of sources used for the actual release or
release candidate.

However, it was proved that tagging straight away is the wrong strategy.
We are always missing some minor tweaks to the sources after the tag and
before doing actual builds. Git is does not like doing re-tagging. It is
possible, but if someone fetched wrong tag he'll stick to that one. Git
NEVER overrides tags in your local copy because of the security issues.

So we're using a bit different approach. Namely, we create a release
branch and building from this branch. This way it's really easy to
commit stuff if we forgot it, do re-AHOY and don't worry about those
folks who fetched changes from the server.

Branch is only used for blender.git, addons and translations could be
tagged right away. So far we never had issues with this.

# Process itself

Here i'd like to put all the information about naming conventions and
so, so the process keeps all the same with each release.

  - Tag addons and translations

<!-- end list -->

  -   
    For this the following command is used:

` $ git tag -a vX.XX -m "Tagging blender release(release cnadidate) X.XX"`

  -   
    This command i usually invoke from a separate checkout of addons and
    translations, but technically if you're accurate you can run this
    from submodule in blender's working tree.
    In order to push tags to server:

` $ git push --tags`

  - Update submodule hashes. This we do in the master branch, before
    branching. For this you first need to be sure submodules fetched all
    the tags:

` $ git submodule foreach git fetch --tags`

  -   
    After this you nee to checkout submodules to their tags. The easiest
    way to do this is to run

` $ git checkout vX.XX`

  -   
    from inside submodule directory. Currently we only need to do this
    for addons and locales. Contrib addons and scons we don't care
    about.
    In order to commit submodules changes:

` ## Stage the changes`  
` $ git add release/scripts/addons release/datafiles/locale`  
`` ## Actual commit. Need `--allow-empty` because of side-effect``  
` ## of ignoring changes to submodules in .gitmodules.`  
` ## Changes to submodules are not visible, but staged changes to`  
` ## them would be committed.`  
` $ git commit -a --allow-empty -m "Updating submodules to vX.XX"`

  - When we're ready for the first release candidate, we create
    "blender-vX.XX-release" branch.

<!-- end list -->

  -   
    That's correct, it's called "-release" because it'll be used for
    backporting all the changes (which we really better do gradually,
    without saving all the backports to be done at the day of release).

<!-- end list -->

  - When the AHOY happened all fine (builds are up and so) we need to
    tag the release branch:

` $ git tag -a vX.XX[rc1|rc2] -m "Tagging blender release(candidate) X.XX"`

  - Once the final release is done, we do tag and delete the branch

` $ git push origin :blender-vX.XX-release`

# Stuff to be checked

  - Links in readme and slash points to the correct page
  - Subversion is to be bumped
  - Release cycle
  - version char
  - "Release Candidate" label on the splash
