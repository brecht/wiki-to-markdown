We've been asked plenty of times how to update FFmpeg on your system to
be able to build Blender on Linux again.

# Update FFmpeg for Linux

## Use pre-compiled static libs

You can use compiled FFmpeg libraries which are used in our release
build environment. They are available for both of 32 and 64 bit
plaforms. To use this libraries create the lib/ folder near the folder
which contains blender sources:

``` bash
# For 32bit platform
svn co https://svn.blender.org/svnroot/bf-blender/trunk/lib/linux

# For 64bit platform
svn co https://svn.blender.org/svnroot/bf-blender/trunk/lib/linux64
```

So directory structure would look like

``` 
  ./
  |- blender
  |  |- build_files
  |  |- release
  |  |- SConstruct
  |   - ...
  |- lib
     |- linux
        |- ffmpeg
```

After you've checked out libraries, the following changes have to be
done to build configuration.

<strong>SCons</strong>

If you don't have user-config.py, create it in the root of blender
working tree.

After this toy should put the following content into this file:

``` python
# Choose one of this two lines
BF_FFMPEG = "#../lib/linux/ffmpeg"    # for 32bit platform
BF_FFMPEG = "#../lib/linux64/ffmpeg"  # for 64bit platform

# This lines are common for both of 32 and 64 bit platforms
WITH_BF_STATICFFMPEG = True
BF_FFMPEG_LIB_STATIC = '${BF_FFMPEG_LIBPATH}/libavformat.a ${BF_FFMPEG_LIBPATH}/libswscale.a ' + \
    '${BF_FFMPEG_LIBPATH}/libavcodec.a ${BF_FFMPEG_LIBPATH}/libavdevice.a ${BF_FFMPEG_LIBPATH}/libavutil.a ' + \
    '${BF_FFMPEG_LIBPATH}/libxvidcore.a ${BF_FFMPEG_LIBPATH}/libx264.a ${BF_FFMPEG_LIBPATH}/libmp3lame.a ' + \
    '${BF_FFMPEG_LIBPATH}/libvpx.a ${BF_FFMPEG_LIBPATH}/libvorbis.a ${BF_FFMPEG_LIBPATH}/libogg.a ' + \
    '${BF_FFMPEG_LIBPATH}/libvorbisenc.a ${BF_FFMPEG_LIBPATH}/libtheora.a ' + \
    '${BF_FFMPEG_LIBPATH}/libschroedinger-1.0.a ${BF_FFMPEG_LIBPATH}/liborc-0.4.a ${BF_FFMPEG_LIBPATH}/libdirac_encoder.a ' + \
    '${BF_FFMPEG_LIBPATH}/libfaad.a'
```

If you've got already BF\_FFMPEG\* settings in user-config.py -- replace
them with this settings.

<strong>CMake</strong>

The following settings should be added to cmake rules:

    FFMPEG=/home/path/to/cloned/lib/ffmpeg
    FFMPEG_LIBRARIES:STRING="avformat;avcodec;avutil;avdevice;swscale;dirac_encoder;mp3lame;ogg;orc-0.4;schroedinger-1.0;theora;theoraenc;theoradec;vorbis;vorbisenc;vpx;x264;xvidcore;faad;asound;jack"

FFMPEG should point to ffmpeg/ folder inside of checked out from the svn
linux/ folder for 32bit platofrms and for ffmpeg/ inside linux64/ folder
for 64bit platforms.

If you're using cmake, pass this settings as arguments adding -D prefix
to them.

## Use already packaged versions for your distro

You can also search in the (non-)official repositories for your distro,
you might very well find the packages you need. Below is a list a known
repositories:

  - **Debian**

:\*[debian-multimedia.org](http://debian-multimedia.org/)

  - **Fedora**

:\*[atrpms.net](http://atrpms.net/) has a 0.8 package.

:\*[rpmfusion.org](http://rpmfusion.org/) has only a 0.7 package
currently.

  - **Gentoo**  
    0.8.2 already in its official source repository.

## Compile FFmpeg yourself

### Generic distro

First of all you need to download FFmpeg sources:

``` bash
wget -c http://ffmpeg.org/releases/ffmpeg-0.8.2.tar.bz2
```

Then unpack it:

``` bash
tar -xf ffmpeg-0.8.2.tar.bz2
```

After this you can build FFmpeg:

``` bash
cd ffmpeg-0.8.2.tar
./configure --prefix=/opt/ffmpeg-0.8.2 --enable-shared
make
sudo make install
```

This sequence of commands will compile and install fresh FFmpeg to
/opt/ffmpeg-0.8.2 folder. If configuration script fail, install
developer package for library reported as missed and run that sequence
again from ./configure step.

Next thing to do is add new FFmpeg to search path:

``` bash
echo "/opt/ffmpeg-0.8.2/lib" | sudo tee -a /etc/ld.so.conf.d/ffmpeg.conf
sudo ldconfig
```

After this, make the following changes to build rules:

<strong>SCons</strong>

Add the following line to your user-config.py:

``` python
BF_FFMPEG = "/opt/ffmpeg-0.8.2"
```

<strong>CMake</strong>

Use the following parameter when configuring CMake:

    FFMPEG=/opt/ffmpeg-0.8.2

If you're using cmake, it'll be something like this:

``` bash
cmake path/to/sources -DFFMPEG="/opt/ffmpeg-0.8.2"
```
