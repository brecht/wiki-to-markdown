## Blender x USD

### USD Features (Blender LTS)

Full documentation can be found here
<https://docs.blender.org/manual/en/latest/files/import_export/usd.html>

#### Exporting

The following objects can be exported to USD:

  - Meshes (of different kinds, see below).
  - Cameras (perspective cameras only at the moment, not orthogonal
    ones).
  - Light (all types except area lights).
  - Hair (exported as curves, and limited to parent strands).
  - Volume (both static and animated volumes).

When exporting an animation, the final, evaluated mesh is written to
USD. This means that the following meshes can be exported:

  - Static meshes.
  - Deforming meshes; here the topology of the mesh does not change, but
    the locations of the vertices change over time. Examples are
    animated characters or bouncing (but not cracking) objects.
  - Arbitrarily animated meshes; here the topology does change. An
    example is the result of a fluid simulation, where splashes of fluid
    can break off the main body.
  - Metaballs are exported as animated meshes.

Limited support exists for exporting materials.

  - To USD Preview Surface
      - When exporting materials, approximate a Principled BSDF node
        tree to by converting it to USD’s Preview Surface format. If
        disabled, the material is set to the viewport materials of
        meshes. (Not all nodes are supported; currently only Diffuse,
        Principle, Image Textures, and UVMap nodes are support.)
  - Export Textures
      - Export textures referenced by shader nodes to a “textures”
        folder which in the same directory as the USD file.
  - Overwrite Textures
      - Allow overwriting existing texture files when exporting
        textures.

#### Importing

The following USD data types can be imported as Blender objects:

  - Cameras
  - Curves
  - Lights
  - Materials
  - Meshes
  - Volume

The importer supports two types of animation:

  - **Animating transforms**: If a USD primitive has time-varying
    transform data, a Transform Cache constraint will be added to the
    imported Blender object.
  - **Animating geometry**: Animating mesh and curve geometry is
    supported by adding a Mesh Sequence Cache modifier to the imported
    data. Geometry attribute (USD Primvar) animation is currently
    supported only for Color Attributes and UVs. Note that USD file
    sequences (i.e. a unique file per frame) are not yet supported.

Limited support exists for importing materials.

  - If a USD mesh or geometry subset has a bound material, the importer
    will assign to the Blender object a material with the same name as
    the USD material.
  - If the USD material has a USD Preview Surface shader source, the
    Viewport Display color, metallic, and roughness are set to the
    corresponding USD Preview Surface input values.

#### USD Schema Implementation

  - UsdGeom
      - UsdGeomMesh
      - UsdGeomBasisCurves
      - UsdGeomNurbsCurves
      - UsdGeomCamera
  - UsdShade (basic)
  - UsdLux
  - UsdVol

### USD Features (universal-scene-description branch)

The universal-scene-description branch was created to help get features
to users more quickly. Experimental work can be done to give users a
build with more cutting edge features from USD while longer term design
reviews are done to get the work committed to the master branch.

  - Environment map IO
      - Logic to convert between USD dome lights and Blender world
        materials, including environment textures.
  - Unit conversion scene scale
      - Automatically scale the scene for unit conversion on import and
        export (e.g., scale the imported objects based on the USD's
        meters per unit value).
  - Curve export.
  - Armature IO
      - Convert animated skeletons and skinned meshes.
  - Shape Keys IO
      - Convert animated blend shapes.
  - Transform operator options
      - Option to save transforms to USD as the combination of scale,
        rotate and translate operators, where the rotation can be
        expressed as Euler angles or a quaternion.
  - Export to USD Cycles shader nodes
      - Convert Blender shader nodes to a custom USD representation of
        Cycles shaders. (Experimental feature implemented by Tangent
        Animation.)
  - Option to specify a default primitive on export.
  - Option to add a root primitive on export. This option adds a single
    prim as the parent of all exported prims.
  - Proof of concept of a UMM (Universal Material Mapper)
      - Python addon that provides custom hooks for material conversion
  - Instancing import
      - Import USD scene instances as Blender collection instances.
        (Currently being refactored to use geometry nodes.)
  - Instancing export
      - Extend the existing instancing option to support exporting
        arbitrary object hierarchies as USD scene instances. Additional
        support for exporting Blender particle systems as USD point
        instancers.
  - Custom properties IO.
  - Texture scaling options when exporting to USDZ.
  - Export option to apply global axes rotations (e.g., to convert
    Blender's Z-up orientation to Y-up).
  - Option to export primitives as USD overs.
  - Option to triangulate meshes on export.

### Design Discussions

#### Collections for Import & Export

With a long term view to integrating more of USD’s compositional
features, we are beginning to investigate using Blender’s Collections as
a means of representing USD composition.

There are two outstanding design tasks: Collections for Import/Export:
<https://developer.blender.org/T68933> Add Existing Import/Export
Functionality to Collection Properties:
<https://developer.blender.org/T100367>

The first is the highest level design discussion for how collections
might be used to do usd import and export, potentially using libraries
and overrides, and how it might get integrated with Hydra or a node
graph.

The second is a concrete implementation discussion of integrating the
existing USD import/export logic into Collections.

#### USD IO Customization

USD IO Customization Proposal: <https://developer.blender.org/T101283>

This proposal was put forth by CharlesFleche from Ubisoft. They would
like to be able to write their own custom Python code that gets
registered with the importer/exporter to handle custom USD schemas that
aren’t handled by the existing USD spec. The python code would hook in
as callbacks to get run at specific points of the import process (e.g
pre/post open stage, pre/post reading a prim, etc).

This will allow users to write whatever specific python code they need
to help massage their custom data into a form that Blender can
understand, and similarly will allow them to transform data in Blender
into any format they would need to integrate with other tools in a
pipeline.

#### AMD Plugin Integration

USD Layers referencing, MaterialX, and USD Hydra rendering Addon for
Blender: <https://developer.blender.org/T100569>

The guys from AMD are working on getting the functionality of their
plugin integrated into Blender natively. This includes using USD Layers
for scene referencing (which is currently dependent on the design work
involved in the Collections work), integrating MaterialX natively, and
integrating the Hydra rendering layer to allow users to install any
Hydra render delegates they wish.
