# Animation: Weak Areas

<table>
<tbody>
<tr class="odd">
<td><div class="note_title">
<p><strong>Outdated</strong></p>
</div>
<div class="note_content">
<p>This page is outdated, and has been superseeded by <a href="Modules/Animation-Rigging/Weak_Areas" title="wikilink">Modules/Animation-Rigging/Weak_Areas</a>.</p>
</div></td>
</tr>
</tbody>
</table>

This is a personal page where I keep track of (what I feel are) weak
areas in the animation system.

**NOTE:** This is not meant to be an exhaustive list of all known
issues. We have [the issue
tracker](https://developer.blender.org/maniphest/query/hmUqAeH_CAE8/)
for that.

## Parent Inverse Matrix

The way the Parent Inverse matrix works can be confusing. It's not
directly visible in the UI, and its existence is only hinted at in the
parent/unparent operator popups.

## Visibility State and Animatability

  - [T72384 Visibility flags cannot be used as driver
    variables](https://developer.blender.org/T72384)

## Armatures and Negative Scale

  - [T64314 Scaling an edit bone negatively rolls the bone by 90°
    instead of 180°](https://developer.blender.org/T64314)
  - (there were other reports about this as well)

## NLA Strip extrapolation mode

According to \`BKE\_nla\_validate\_state()\` in \`nla.c\`, only the
first NLA Strip can be set to Extrapolation Mode: Hold. However, this
was not documented in [the
manual](https://docs.blender.org/manual/en/dev/editors/nla/properties_modifiers.html#properties),
and this function is only called after moving a strip, which makes its
behaviour appear like a bug. It's also not really intuitive, as one
strip could contain only location animation, and the other only scale
animation, and both could be set to 'Extrapolation: Hold' at the same
time without creating a conflicting situation. This is hard to
implement, though, as it requires a deep analysis of every channel in an
action (and then there could be drivers causing cross-influence), so I
understand why it's implemented the way it is.

  - [T42808 Moving one strip resets extrapolation on other
    strip](https://developer.blender.org/T42808)
  - [T66946 NLA Hold\_Forward holds backwards when orange
    action](https://developer.blender.org/T66946)
  - [T79577 NLA Strips reverting their Extrapolation from "hold" to
    "hold forward"](https://developer.blender.org/T79577)

## Bendy Bones parameters

  - [T64908: In Pose Mode, clearing a bones Scale Transform also clears
    that bone's Bendy Bone Ease in/out values and also Scale in/out
    values](https://developer.blender.org/T64908). It's probably better
    to have specialised 'Clear Transform' operators for Bendy Bone
    parameters.

## Matrix-based constraint system

Blender's constraint system is matrix based. Although this is powerful,
it does have issues with gimbal lock.

  - [T75078 Limit rotation for transform constraint (local)
    bug](https://developer.blender.org/T75078)
