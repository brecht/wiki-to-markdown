# Alembic stuff

**NOTE**: this page may be out of date.

## Alembic-related info / links

Here's what I've gathered from various sources.

  - [Alembic homepage](https://www.alembic.io/)
  - The [Alembic Python
    documentation](http://docs.alembic.io/python/examples.html) is
    actually better than the documentation on Alembic itself.
  - [Alembic Github](https://github.com/alembic/alembic); Python
    examples were removed for some reason in commit
    098e968c6657de00a90c637663966b9913aab07a, so check out
    098e968c6657de00a90c637663966b9913aab07a\~1 to read through them.
  - [Blender 2.8 data
    flow](https://code.blender.org/wp-content/uploads/2016/11/Blender_2.8_Data_Flow.png)
    overview.
  - We're going to keep Y=up for Blender-generated Alembic files for
    now, and add user-specified up/forward axes later.
  - [D1783](https://developer.blender.org/D1783) - the original patch
    introducing Alembic support.
  - [Blender Artists
    discussion](https://blenderartists.org/forum/showthread.php?399763)
    about Alembic support.
  - [Simple Alembic
    pipeline](https://docs.google.com/drawings/d/1mxezo_umafLv70B1hcF3YEfPqKLSm_me7QUBG34RxA8/edit?usp=sharing)
  - "xform" is short for "transform"
  - Alembic requires C++11, unless you pass
    `-DALEMBIC_LIB_USES_BOOST=ON` or `-DALEMBIC_LIB_USES_TR1=ON` to
    CMake.
  - The Alembic Exporter seems to handle modifiers as if it's rendering,
    so only modifiers that are render-enabled will be exported.
  - There is a [Google
    wiki](https://code.google.com/archive/p/alembic/wikis) with more
    info about Alembic.

## Other stuff

  - [Blender 2.8 Scene layer data
    model](https://wiki.blender.org/uploads/3/34/Dev-Blender_2.8_Scene_Layer_data_2.png)
  - Lukas Tönne wrote [a proposal to replace PointCache files with
    Alembic files](https://developer.blender.org/T37578).

## Kévin Dietrich

  - Did a lot of work on Alembic support in Blender.
  - [Private branches on Github](https://github.com/diekev/blender)
    contain more work which he might merge in the coming months.
  - Says "Alembic::IMaterial is \[...\] not for materials but for
    lights", which is good to know.

## Alembic in a future Blender

Integration with the 2.8 branch implies that we can use Collections to
define a mapping between objects in Blender and information in an
Alembic file. Until collections are fully implemented, we use the active
SceneLayer to define this mapping. Since this collection is saved in the
blendfile it should be easy (for an artist) to re-export changed data to
Alembic, or to switch between showing "real" data and loading from
Alembic.

### Possible features

  - Set of objects to export to / import from a file == collection
  - Export multiple collections in parallel, so that the timeline only
    needs to be iterated once (idea by Jason Schleifer).

### User Stories

**Animator**: Hjalti has finished animating the first revision of shot
"07\_03\_B-gun\_kick", consisting of two animated characters Agent and
Boris, an animated camera, and some animated props. He wants to send it
to Andy for shading & lighting. To make the file lighter for Andy to
work with, he exports Boris, the Agent, and the animated props to 3
separate Alembic files. Each file is linked to a collection, so he
performs the following actions:

  - Hjalti selects the collection "Boris"
  - He double-checks that the modifier stack is suitably set up
  - He clicks on "Bake to Alembic".
  - Blender exports the Alembic file, and switches the collection from
    "Live Animation" to "Loaded from Cache" after the export is
    complete.
  - Hjalti repeat those steps for "Agent" and "Props".
  - He can scrub through the currently open blend file to inspect the
    result of the caching.

**Shading & Lighting artist**: Andy gets poked by Hjalti that he has
finished shot "07\_03\_B-gun\_kick".

  - Andy loads the lighting file for that shot.
  - He imports the three Alembic files "Agent", "Boris", and "Props".
  - Blender creates three collections for these files to record the
    relation between Blender objects and Alembic files.
  - Since Alembic fiels do not contain Blender materials, Andy
    re-attaches the materials to the objects.

## Issues with current Alembic export

Based on interview with Andy Goralczyc. Solutions could lie both in
implementations / fixes for 2.79 as well as 2.8.

  - Exporter options are unclear:
      - What does "Use Subdivision Schema" do? What effect does it have
        on the exported mesh?
      - Which subdivision modifiers are disabled when "Apply Subsurf" is
        turned off?
  - Unclear what is taken into account by the exporter and what isn't:
      - Modifier stack (deformations, subdivisions, etc) -- up to which
        modifier is exported?
      - Modifier stack: are viewport-enabled or render-enabled modifiers
        used?
      - Simplify: are viewport or render settings used?
  - It would be nice to be able to configure export options per group
    object / character / Blender 2.8 collection
      - What is exported (i.e. what is in the collection)
      - Which modifiers are baked to the modified mesh
      - Which modifiers have to be applied after importing the Alembic
        file
  - Some things that need to be checked:
      - Shutter Open/Close options need clarification

## Generic brainstomings

Regardless of 2.79 or 2.8.

Export mesh + deformations + hair/particle systems should work reliably
and in a cross-software manner, i.e. adhere to the schemas defined by
Alembic itself. This includes:

  - The mesh itself
  - Hair curves
  - Particles
  - Object transformations

All of the above information is stored per timekey, so the mesh isn't
stored as "mesh + deformation per timekey", but actually as the full
mesh per timekey.

The mesh export may contain Blender-specific information. This would
thus not be understood by other software, unless they explicitly
implement support for it.

  - Multiple UV maps
  - Vertex groups, including weights
  - Material names, possibly as
    "//path/to/matlib.blend/Materials/Materialname"
  - Property overrides? Maybe material names should also be implemented
    as property overrides?

## Idempotence

Blender and Alembic structure data differently:

  - *Blender*: object (contains transformation matrix) has explicit
    pointers to data (like mesh, hair systems, etc.)
  - *Alembic*: Xform object (containts transformation matrix) simply has
    children. Such an Xform could have come from a single Blender
    object-with-data (including multiple children, like mesh + curve
    collection for each hair system), but it could also come from a
    parent relation (for example, an empty, or a rig that wasn't saved
    to Alembic itself).

How an Alembic Xform object is imported into Blender is dependent on its
children, and cannot always be mapped to the same Blender concepts it
was generated from. Here are two examples:

  - Rigged mesh: once a mesh object that is parented to a rig is
    exported, the rig becomes an Xform, the object becomes an Xform, and
    the mesh becomes a PolyMesh. When importing, the former Xform
    becomes an empty, and the other becomes a mesh object.
  - Hair systems: A single mesh object with two hair systems is exported
    as an Xform with three children (1x PolyMesh + 2x Curves). When
    importing, it may become an empty with three children (mesh object
    and two curve objects).

For these reasons, exporting to Alembic and then importing is not an
idempotent operation, and will produce different results. Exporting and
importing again will again produce different results.

## Nice-to-have

Currently the Alembic file is always written with Y=up and X=forward, to
be compatible with other software. In the future this may change, in
which case the artist can choose different orientations. Profiling
should show how important this is, as reading from a file that has the
same coordinate system as Blender itself may be faster and thus
preferable in a Blender-only pipeline. However, this should be weighed
against the burden of additional choices, UI elements, and differences
between files. Alembic does *not* store the used coordinate system (nor
the used units) in the Alembic file; we can choose to define our own
keys to store this information for ourselves, so that at least Blender
can auto-detect the coordinate system and unit sizes of Blender-written
files.
