# Scripting for Artists: User Interfaces

  - After many requests: user interfaces.
  - We have all the ingredients now: create operators, create add-ons,
    we have seen registering classes.
  - As a basic concept, making a UI is the same as we have seen before.
    Create class, register it, and in some functions add your own code.
    For operators this was \`execute\`, for drawing a UI this is
    \`draw\`.
  - Easy to cheat.

## Your Own Panel

  - Simple to add, like operators only requires some properties +
    registration of the class.
  - Cheat by looking at other panels. If no "Edit Source", enable
    "Developer Extras" in the preferences.
  - First look: 3D view, N-panel, View, 3D Cursor.
      - Naming: \`CATEGORY\_PT\_name\`, similar to operators.
      - Uses \`Panel\` instead of \`bpy.types.Panel\`.
      - Scroll to top, see \`from bpy.types import …\`
      - Instead of \`bpy.types.Panel\` you can use \`Panel\`.
      - Be careful with this, only use it if there is no doubt about
        where it comes from.
      - Optimise your code for readability, make it easy to understand.
        You will read it more than you write it, so never do something
        only because it saves writing time. Only do it if it makes
        things easier to understand.

<!-- end list -->

``` Python
class VIEW3D_PT_monkeys(bpy.types.Panel):
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = "Monkeys"
    bl_label = "Grid"

    def draw(self, context):
        pass
```

``` Python
        self.layout.operator('mesh.monkey_grid')
        self.layout.operator('mesh.monkey_grid', text='Grid')
```

  - Second look: Scene Units. Multiple inheritance: "ask mom"

## Appending to Existing Things

  - Menu item
  - Panels
