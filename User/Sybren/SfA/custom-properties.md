# Scripting for Artists: Custom Properties

  - Last video: added existing properties to a panel. Real power comes
    from adding your own properties.
  - Can be added to pretty much any type in \`bpy.types\`, so Object,
    Mesh, Scene, WindowManager, Camera, etc.
  - Different kinds of properties
      - Existing properties, like \`object.location\` or \`scene.name\`.
      - Custom Properties, from the Custom Properties panel.
      - A third kind: your own properties. These are also "custom"
        because they're added by your add-on and not standard in
        Blender. Behind the scenes they also use the same way to store
        their values as Custom Properties. This means that if your
        add-on is not loaded, the values are still stored in the blend
        file.

## Your Own Properties

  - We have seen those when we created an operator: the Monkey Grid
    operator had \`count\_x\`, \`count\_y\`, and \`size\` properties.
  - We created the operator by creating a class, and then putting the
    properties in there. These properties are not limited to our own
    classes. We can also assign them to existing classes.

## Mass import OBJ files and allow re-importing

  - Scene property: directory containing the OBJ files.
  - Object property: name of the OBJ file it came from.
  - Big assumption: one OBJ file corresponds to one object.
  - Draw on panel.

## Temporary Properties on the Window Manager

  - Set properties before we call the Monkey Grid operator.
