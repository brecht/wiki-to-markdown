# Scripting for Artists ideas

This is just a page to collect some ideas for Scripting for Artists. No
promises.

Call for suggestions:
<https://twitter.com/sastuvel/status/1241706841320677376>

## Finished videos

### 6: Working with Collections in 2.8x

  -   
    Are there any changes or new things you can do in 2.8+? Did you
    teach how to create multiple render layers in a specific folder?
    — [pipeliner
    (@cgpipeliner)](https://twitter.com/cgpipeliner/status/1241710636033282051)

[Script](collections-API.md) |
[YouTube](https://www.youtube.com/watch?v=opZy2OJp8co) |
[Cloud](https://cloud.blender.org/p/scripting-for-artists/5e7c941c5afb8fa58fad6718)

**TODO:** excluding collections from the scene via
\`C.view\_layer.layer\_collection.children\['SfA'\].exclude = True\`

### 7: \`for\` vs \`while\`

  -   
    I see you have "For" loops. When is it better to use "While" loops
    instead? That might be a cool topic 🤓
    — [Rodger
    (@RDavis3D)](https://twitter.com/RDavis3D/status/1241740283362017282)

[Script](for-vs-while.md) |
[YouTube](https://www.youtube.com/watch?v=7M8FIXDMKkg) |
[Cloud](https://cloud.blender.org/p/scripting-for-artists/5e7e781c4be12074b19ed6b7)

### 8: Your own Operator

No direct questions about this, but IMO a good next step.

[Script](operators.md) |
[YouTube](https://www.youtube.com/watch?v=xscQ9tcN4GI) |
[Cloud](https://cloud.blender.org/p/scripting-for-artists/5e85f0874cc2c6ed8ef7ba36)

### 9: Turning your code into an add-on

No direct questions about this, but IMO a good next step.

[Script](addon.md) |
[YouTube](https://www.youtube.com/watch?v=nKt6CtMH0no) |
[Cloud](https://cloud.blender.org/p/scripting-for-artists/5e8ed2fb75db67af5c12a538)

### 10: Creating a UI: menus and buttons

  -   
    I would like to know how to create simple ui menus. Also if there’s
    a way to automate rendering multiple cameras from the same scene,
    that would be great for a project I have.
    — [João Morgado
    (@\_MrJomo)](https://twitter.com/_MrJomo/status/1241723419332431872)

<!-- end list -->

  -   
    Is it an idea to show how to build an addon with an UI where the
    user can set values?
    — [nocerasparita
    (@nocerasparita)](https://twitter.com/nocerasparita/status/1241775195486072836)

[Script](user-interface.md) |
[YouTube](https://www.youtube.com/watch?v=bZUTiAJ1Tuc) |
[Cloud](https://cloud.blender.org/p/scripting-for-artists/5e9953ab173f1d99c7826902)

### 11: Custom Properties

[Script](custom-properties.md) |
[YouTube](https://www.youtube.com/watch?v=9fuFDHR-UkE) |
[Cloud](https://cloud.blender.org/p/scripting-for-artists/5ea3fa372236792c34a5f39d)

### 12: Procedurally build a scene

  -   
    A python script that procedurally builds a scene. This script should
    cover creating objects, adding to collections, setting view layers,
    add basic animation. :)
    — [Satish Goda
    (@satishgoda)](https://twitter.com/satishgoda/status/1241748156603232256)

[script](SceneBuilding.md) |
[YouTube](https://www.youtube.com/watch?v=tIg-KOeFxkg) |
[Cloud](https://cloud.blender.org/p/scripting-for-artists/5eabe54d521eafd0953f6d45)

### 13+: Roast my Add-on

Suggested in Pablo's Blender Today livestream.

  - First roast: [call on
    Twitter](https://twitter.com/sastuvel/status/1260200287630962690?s=19)
    | [Script](roast-my-addon.md) |
    [YouTube](https://www.youtube.com/watch?v=_8KsNVE6KJs) |
    [Cloud](https://cloud.blender.org/p/scripting-for-artists/5ebd8583a32a380994b797d9)
  - Second roast. [call on
    Twitter](https://twitter.com/sastuvel/status/1264831715639525376) |
    [YouTube](https://youtu.be/uBDc0Eq70kM)
      - Nature Clicker by OlePost
        [Twitter](https://twitter.com/Ole_Post/status/1264884144896708609)
        | [GitHub](https://github.com/OliverJPost/NatureClicker).
        Started coding recently, plenty to roast.
      - [Their demo video on
        YouTube](https://www.youtube.com/watch?v=_OmCKu4YLDw)

Possible add-ons to roast:

  - Gizmo Size by -L0Lock-
    [Twitter](https://twitter.com/L0Lock/status/1261583094848598016) |
    [GitHub](https://github.com/L0Lock/GizmoSize). Simple, two operators
    to increase/decrease a user preference.
  - Collision Helpers by Matthias Patscheider
    [Twitter](https://twitter.com/weisl12/status/1265016604024344576) |
    [GitHub](https://github.com/Weisl/CollisionHelpers). More complex,
    multi-file and plenty to roast.
  - Blender-Render-Match-VSE-Strips by Patrick W. Crawford
    [Twitter](https://twitter.com/TheDuckCow/status/1265057856522465280)
    |
    [GitHub](https://github.com/TheDuckCow/Blender-Render-Match-VSE-Strips).

## Planned Videos

These are videos that are either currently in production, or that I want
to make in the coming weeks/months. No promises, though\!

### Understandable Code

Cognitive complexity and other things to make your code easier to
understand.

``` C
bool ED_object_modifier_move_down(ReportList *reports, Object *ob, ModifierData *md)
{
  if (md->next) {
    const ModifierTypeInfo *mti = BKE_modifier_get_info(md->type);

    if (mti->flags & eModifierTypeFlag_RequiresOriginalData) {
      const ModifierTypeInfo *nmti = BKE_modifier_get_info(md->next->type);

      if (nmti->type != eModifierTypeType_OnlyDeform) {
        BKE_report(reports, RPT_WARNING, "Cannot move beyond a non-deforming modifier");
        return false;
      }
    }

    BLI_listbase_swaplinks(&ob->modifiers, md, md->next);
  }
  else {
    BKE_report(reports, RPT_WARNING, "Cannot move modifier beyond the end of the list");
    return false;
  }

  return true;
}
```

``` C
static int sequencer_rendersize_exec(bContext *C, wmOperator *UNUSED(op))
{
  int retval = OPERATOR_CANCELLED;
  Scene *scene = CTX_data_scene(C);
  Sequence *active_seq = SEQ_select_active_get(scene);
  StripElem *se = NULL;

  if (active_seq == NULL) {
    return OPERATOR_CANCELLED;
  }

  if (active_seq->strip) {
    switch (active_seq->type) {
      case SEQ_TYPE_IMAGE:
        se = SEQ_render_give_stripelem(active_seq, scene->r.cfra);
        break;
      case SEQ_TYPE_MOVIE:
        se = active_seq->strip->stripdata;
        break;
      case SEQ_TYPE_SCENE:
      case SEQ_TYPE_META:
      case SEQ_TYPE_SOUND_RAM:
      case SEQ_TYPE_SOUND_HD:
      default:
        break;
    }
  }

  if (se) {
    /* Prevent setting the render size if sequence values aren't initialized. */
    if ((se->orig_width > 0) && (se->orig_height > 0)) {
      scene->r.xsch = se->orig_width;
      scene->r.ysch = se->orig_height;
      WM_event_add_notifier(C, NC_SCENE | ND_RENDER_OPTIONS, scene);
      retval = OPERATOR_FINISHED;
    }
  }

  return retval;
}
```

## Other ideas that are not directly planned yet

### Procedurally build a shader node tree

  -   
    A python workflow that shows how to build shader with nodes like
    cycles but using Python\! Can work for Animation Nodes and Sverchok
    as well. What to think when connecting disconnecting nodes etc with
    bpy.
    — [Blender Sushi Guy
    (@jimmygunawanapp)](https://twitter.com/jimmygunawanapp/status/1241825962389430272)

### Automated USD export

  -   
    something like an automated USD exporter maybe
    — [pipeliner
    (@cgpipeliner)](https://twitter.com/cgpipeliner/status/1241710971367895041)

Creating a USD file for every object in the scene, then creating the
layers of USD wrappers that Pixar also have in their example files.
Boring work, really good for scripting.

### Procedurally create objects & meshes

  -   
    I personally would like to know more about making simple addons and
    also anything related to randomization, procedural creation of
    objects...
    — [Lopo Isaac
    (@lopoisaac)](https://twitter.com/lopoisaac/status/1241716283672006657)

<!-- end list -->

  -   
    what about mesh creation from calculated points.
    — \[<https://twitter.com/mudumat/status/1241707618332262401>? Mike
    Webster (@mudumat)\]

### Procedurally draw with Grease Pencil

  -   
    can you explain how to draw greasepencil pictures with python
    — [Momotron2000
    (@momotron2000)](https://twitter.com/momotron2000/status/1241715816581804034)

### Binding Hotkeys to Operators

  -   
    How to add a hotkey for the Studio Light rotation.
    — [3Rton
    (@3Rton93)](https://twitter.com/3Rton93/status/1241722593377386497)
