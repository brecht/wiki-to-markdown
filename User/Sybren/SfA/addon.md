# Scripting for Artists: Creating your own Add-ons

  - So far: code in blend file
  - Absolutely fine if the code should be there. For example one-off
    things for that particular blender scene. Rigify also uses this.
  - Hard to reuse.
  - Hard to distribute.

<!-- end list -->

  - Solution: add-ons.

## What is an add-on?

  - Python code in file on disk, rather than buried inside a blend file.
  - Just a text file, you can open it with any text editor.
  - Blender's text editor works too, but I always get confused like
    that. Are you editing a text file on disk, or a text datablock in
    the blend file? Saving with Ctrl+S or Alt+S.
  - Use a free text editor, like Visual Studio Code, Atom, Notepad++.
  - On Windows, avoid Notepad. Even though Python is text, Notepad
    doesn't support files from Linux and thus you can have trouble
    reading code from Blender itself, or other people's add-ons.
  - PyCharm works for bigger projects, but the smarter your editor the
    more work you need to do to remove all the error markers.
  - Naming: recommended lower case, letters without accents, numbers,
    and underscores only. And end in \`.py\`.

Location of add-ons:

``` python
 
import addon_utils
print(addon_utils.paths())
```

  - More info:
      - Help
      - Manual
      - Advanced
      - Scripting & Extending Blender
      - Lots of info there, including Add-on Tutorial

## Ingredients of a simple Add-on

  - Starts with some metadata in \`bl\_info\`, like name of the add-on,
    the version number, version of Blender that's compatible with it,
    authors, etc.
  - \`register\` and \`unregister\` functions.
  - Then code just like we're used to.
  - Can be enabled & disabled in user preferences.

Minimal add-on:

``` python
bl_info = {
    "name": "My Test Add-on",
    "blender": (2, 83, 0),
    "category": "Object",
}
def register():
    print("Hello World")
def unregister():
    print("Goodbye World")
```

Dictionary: already seen in chapter 5. Basically a lookup table,
\`bl\_info\["name"\]\` gives \`"My Test Add-on"\`.

  - Save in random directory, name \`my\_first\_addon.py\`.
  - Start Blender, user prefs, install, enable & disable add-on and
    check console.
  - Now you know why in the previous chapter we had an \`unregister()\`
    function, even though it wasn't used yet.
  - Was saved by Blender in
    \`$HOME/.config/2.83/scripts/addons/my\_first\_addon.py\`, Blender
    made the right directories for us.
  - Now open the installed file in your editor so you can keep editing
    and reloading. (what happens on reload we'll cover in a few minutes)

## Monkey Grid Add-on

  - Add \`bl\_info\`
  - Copy code from previous chapter
  - Save & reload scripts
  - Done\!

## Accessing add-on from within Blender

``` python
import my_first_addon
print(my_first_addon.__file__)
my_first_addon.register()

# change the print statements for both register & unregister.

my_first_addon.unregister()

import importlib
my_first_addon = importlib.reload(my_first_addon)

my_first_addon.register()
```

## Making code usable by others (maybe skip this part)

  - Operators are glue between menus, buttons, and the code to run.
  - Avoid calling operators from Python code. Sometimes unavoidable, and
    that's fine.
  - Put monkey grid generation code into a function.
  - Call that function from the operator.
  - Call that function from the console.

## Conclusion

  - Code is just text in files.
  - Single-file add-on. Multi-file is not much different, but won't
    cover here. Requires trick with reloading; look at how other add-ons
    are doing it.
  - Add-on requires bl\_info and register/unregister functions.
  - Put one or more operators there.
  - Later video: making a user interface
