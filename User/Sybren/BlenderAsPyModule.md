# Overview

The official **blender.org** embeds a Python interpreter (CPython 3.x).
This makes sense from the perspective of a user, who's primary needs are
to have an application which can be extended with scripts.

However from the perspective of a Python developer, it can be useful to
being Blender into your existing scripts and access its feature set.

The option to build Blender as a Python module is not officially
supported, *in the sense Blender.org isn't distributing it along with
regular releases*, Currently, its a build option you can enable, for
your own use.

# Rationale

This is a build option to be able to import blender into python and
access its modules

Possible uses include:

  - rendering animations.
  - image processing using Blender's compositor.
  - video editing (using Blender's sequencer).
  - importers, exporters (convert 3D file formats).
  - development, accessing `bpy` from Python IDE's and debugging tools
    for example.
  - automation.

This is mainly limited to features which can be usable in background
mode, so you cant for instance do OpenGL preview renders.

# Prerequisites

#### OSX

get Python3.x-framework from Python.org and install it.

# Building

assuming you have a CMake out-of-source build setup, see:
<http://wiki.blender.org/index.php/Dev:Doc/Building_Blender/Linux/Ubuntu/CMake>

Change these CMake options from the defaults:

`WITH_PYTHON_INSTALL=OFF`  
`WITH_PLAYER=OFF`  
`WITH_PYTHON_MODULE=ON`

Everything should build as normal except in the cmake directory you will
have `./bin/bpy.so` instead of `./bin/blender`

# Installation

## Linux

#### System Wide Install

You may want to copy into the module to the systems Python path, eg:

`/usr/lib/python3.6/site-packages`

For a system wide installation:

`WITH_INSTALL_PORTABLE=OFF`

*note, `PYTHON_SITE_PACKAGES` will be used as the target path, but this
is auto detected, nevertheless, you may want to modify.*

Once these optiosn are set, run:

` make install`

#### Local Install

Alternately you might want to use your user Python path (see
<https://www.python.org/dev/peps/pep-0370/>)

`$HOME/.local/lib/python3.6/site-packages`

For a local install use the following options:

`WITH_INSTALL_PORTABLE=ON`  
`CMAKE_INSTALL_PREFIX=$HOME/.local/lib/python3.6/site-packages`

Once these optiosn are set, run:

` make install`

## Windows

`copy bin\bpy.pyd C:\Python36\Lib\site-packages\`  
`copy bin\*.dll C:\Python36\Lib\site-packages\`  
`del C:\Python36\Lib\site-packages\python36.dll`  
`xcopy /E bin\2.79 C:\Python36\`

## OSX

After compiling and "make install", copy needed files to your python
framework

`cp ./bin/bpy.so /Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/`  
`cp -R ./bin/2.79 /Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/`

*Note. Unlike on \*nix C:\\Python36\\2.79 is not in the site packages,
this is because of a difference in how Windows finds the scripts path
and should eventually be fixed.*

# Testing

This allows 'bpy' to be imported from python or other applications/IDE's
which embed python, eg:

`python -c "import bpy ; bpy.ops.render.render(write_still=True)"`

This runs in background mode and has similar restrictions to running a
script:

`blender --background --python test.py`

# Troubleshooting

  - The Python version requirements are the same with building a regular
    blender binary (if Blender us using Python3.6 then there is NO WAY
    to use another version - 2.7/3.2/3.4 will all fail).
  - On Windows, you probably won't want to use a debug build, since this
    requires a debug python installation (python36\_d.dll rather then
    python36.dll), so while it can be made to work, its more trouble.
