# Code Quality Day ideas

## Ideas

  - Finish work on the \`modernize-loop-convert\` Clang-Tidy rule
  - Continue <https://developer.blender.org/T74506>
  - Move auto-keying code into a separate C file, such that the related
    code sits together instead of strewn across multiple C files. Or at
    least document what can be found where.
  - Split \`ed\_screen\_context()\` into separate functions, or at least
    refactor to use early returns.
  - Move keyframe-insertion code from \`editors\` to \`blenkernel\`.
  - Split \`splineik\_evaluate\_bone()\` into separate functions.
  - Rename \`BKE\_scene\_get\_depsgraph()\` to
    \`BKE\_scene\_get\_viewport\_depsgraph()\`
  - Deprecate \`SELECT\` and remove \`\#define SELECT 1\` from various
    places in the code.
  - Split up \`pyrna\_py\_to\_prop\`

### After the new Alembic exporter has been merged into master

  - USD/Alembic: replace \`HierarchyContext &\` with \`HierarchyContext
    \*\`.

## Done

  - Properly describe action baking in [the
    manual](https://docs.blender.org/manual/en/dev/animation/actions.html#bake-action).
  - \`ANIM\_apply\_keyingset()\` has a \`short success\` variable (which
    is strange to begin with, as \`success\` is usually a boolean and
    not a quantity). The value returned by \`return success;\` is then
    later also compared to constants with values \`-1\` and \`-2\`. To
    summarize: \`bool\`, \`short\`, and \`enum\` are all combined into
    one variable. Found this while investigating
    [T73773](https://developer.blender.org/T73773).
  - Document \`bpy.msgbus\`:
      - [the new
        docs](https://docs.blender.org/api/master/bpy.msgbus.html),
      - [original patch D2917](https://developer.blender.org/D2917),
      - [example from Ideasman](https://developer.blender.org/P563), and
      - [Stack Exchange question on the
        topic](https://blender.stackexchange.com/questions/150809/how-to-get-an-event-when-an-object-is-selected).
  - USD/Alembic: refactor code to replace \`std\` containers with
    \`BLI\` ones: not worth the effort as the Alembic library needs
    \`std::vector\` and \`blender::Map\` has slightly different memory
    management than its \`std\` counterpart, causing other issues with
    the Alembic library.
