[thumb|<http://stuvelfoto.nl/>](file:Sybren.jpg)

## Personal Info

  -   
    **Name:** Sybren A. Stüvel
    **Email:** sybren@blender.org
    **WWW:** [stuvel.eu](https://stuvel.eu/) and
    [stuvelfoto.nl](https://stuvelfoto.nl/)
    **Mastodon:** [1](https://mastodon.social/@sybren)
    **Twitter:** [@sastuvel](https://twitter.com/sastuvel)

<!-- end list -->

  -   
    **on developer.blender.org:**
    [sybren](https://developer.blender.org/p/sybren/)
    **on blender.chat:**
    [dr.sybren](https://blender.chat/direct/dr.sybren)

## Sub-pages

  - [weekly reports](Reports/index.md)
  - [User:Sybren/Alembic](Alembic.md)
  - [Weak Areas of the Animation
    System](Animation_Weak_Areas.md)
  - [Code Quality Day
    Ideas](Code_Quality_Day_ideas.md)
  - [Scripting for Artists ideas](SfA/index.md)
  - [Animation & Rigging meeting notes
    template](AR_Meeting_Notes.md)
  - [My approach to code
    review](Code_Review_Approach.md)

## About me

I'm a Blender developer employed by the Blender Institute ([weekly
reports](Reports/index.md)).

I obtained my PhD in 2016 at the Virtual Worlds group, Utrecht
University, on the subject of the animation of virtual characters in
very dense crowds. You can find my thesis and other publications on [my
website](http://stuvel.eu/publications). I obtained my MSc degree in
computer science (Game & Media Technology) with honours, at the Utrecht
University, in 2010. My thesis subject was a solution to the stepping
stone problem: the generation of walking motion from pre-recorded motion
capture data, such that the feet step exactly at the required positions.

Besides my work at the university, I've worked for several companies,
most notably Eyefi Interactive in Amsterdam (2001-2006), and Chess IX in
Haarlem (2006-2011) where I was Senior Software Engineer & Technical
Architect.

Since 2011 I've been attending the Blender conference, and also gave a
workshop "[Blender as research tool: from data to
visualization](http://www.blender.org/conference/2013/presentations#56)"
in 2013 and a talk
"[2](https://www.blender.org/conference/2015/presentations/163)
Simulation of dense crowds of virtual characters\]" in 2015.

Since roughly 1994 I've been programming in C/C++, and since 2005 in
Python. Open Source projects I currently maintain are the [Python Flickr
API implementation](http://stuvel.eu/flickrapi) and [a pure-Python RSA
implementation](http://stuvel.eu/rsa). Blender I've used since 2003, for
graphics, animations, and creating the video clips of my band [The
Soundabout](http://soundabout.nl/). I used it daily for my PhD work, and
I've created various add-ons to support my workflow. Currently I work at
the Blender Institute to work on the Blender Cloud & Blender ID.

## Creative work

Apart from all the computer-related stuff I'm also the drummer in the
Dutch ska & reggae band [The Soundabout](http://soundabout.nl/). I'm
also [a photographer](http://stuvelfoto.nl/), and mostly shoot either
[portraits](http://stuvelfoto.nl/portret) using an old Hasselblad 500C/M
(I do my own development in my dark room) or create full-spherical
[panoramas](http://stuvelfoto.nl/panorama).
