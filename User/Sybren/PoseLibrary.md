# Animation: Pose Library scribbles

This is a **personal** document of [Sybren](index.md) to
jot down some design ideas for a new/improved pose library. Everything
here is unofficial and just some random thought.

## Contents of a Pose Library

  - **Pose:** one frame in an Action, indicated by a Pose Marker. This
    is what is currently the only thing possible to store in the Pose
    Library.
  - **Animation:** more than one consecutive frames in an Action. The
    start is indicated by a Pose Marker, the end is indicated by the
    next Pose Marker or by the end of the Action.
  - **Selection Set:** list of bone names.

The three above will collectively be referred to as a **Library Item**
or just **Item**.

## Supporting Team Collaboration

It must be possible to:

  - **exchange** Library Items between team members,
  - **categorise** Library Items in the following categories:
      - **Approved:** These Items are approved for use by the team in
        the project.
      - **Personal:** Tagged with the name of the animator, this
        category is meant for animators to put in whatever they
        personally want. It should be possible to easily see your own
        personal items, or to look up the personal items of a teammate.
        Personal collections are not private.
      - **Shared:** These Items are not (yet) officially approved, but
        still available in a common pool (so not split up into a pool
        per animator).
  - **organise** Library Items using tags and/or folder-like structures.

The organisation and categorisation of Library Items is independent. For
example, a pose could be organised by storing it as
\`Spring/Face/Smirk\`, and placed into the \`Shared\` category to
indicate it's awaiting approval.

## Technical Unknowns when Storing as Actions

If we keep the approach of using an Action as Pose Library, there are
some technical unknowns to figure out.

  - When do we store poses in one Action, and when do we store them to
    different Actions? In the production of Spring "Spring-Face" and
    "Spring-Hands" were two Actions, each containing multiple poses.
  - How do we map a folder-like organisation onto a flat list of Actions
    each containing a flat list of Pose Markers?
  - Do we store different categories in different blend files? It makes
    sense to store each animator's personal poses into a separate Blend
    file. This could make it harder to move poses between categories,
    though.

## Nice Things

  - When editing a Pose Library, renaming a bone should update the poses
    in that library for that character.

## Workflow / Requirements

  - At least in the first incarnation, the new Pose Library system would
    still require an animator to open the blend file that contains the
    pose library, and then add/update poses.
  - Thumbnails should be auto-generated when adding the pose, by
    rendering (one of) the 3D Viewport(s). Re-rendering should take one
    click.
  - Thumbnails should be stored on the pose itself, and not in an
    external image file.
  - When using the pose library, it would be nice if it were easily
    possible to select a pose and click on an "Edit" button. This button
    would start another Blender instance, opening the file that contains
    that pose. Closing that Blender would then trigger a reload of the
    pose library.

## Wishes for the Asset Browser

It would be nice if the Asset Browser could have custom, possibly
Python-defined, "modes", one of which would be the "Pose Library" mode.
Such a mode would determine:

  - Which filters or navigational structures are available in the
    left-hand panel.
  - Which properties, operations, and panels are shown in the right-hand
    panel.

This would allow the Pose Library to present a hierarchical structure
for the stored poses (f.e. "Victoria/Face/Angry") even when the
underlying blend files are stored in a single directory.

## Existing Pose Library structure vs. Asset Browser

There is a discrepancy between what the Asset Browser considers an
"Asset" and what Blender now considers a "Pose" in the Pose Library.

  - **Asset Browser:** an Asset is always a datablock. Such a datablock
    has asset-metadata, and a preview image.
  - **Pose Library:** a Pose Library is an Action datablock, and
    individual poses are keyframes in that Action, marked by Pose
    Markers.

Because of the above, the Asset Browser could only mark an entire Pose
Library datablock as "asset", and would not allow marking or browsing
individual poses.

Two approaches can be thought up:

  - Change how Blender handles poses, by making each pose a datablock.
    This requires a lot of work to animation editors to still be able to
    provide the desired workflow for animators.
  - Change how the Asset Browser deals with datablocks. This would
    require making pose markers asset-like, with asset-specific
    metadata, previews, etc. This isn't desired by the Asset Browser
    team, who view Blender's current approach (pose markers in an
    action) as hackish.

## Writing to Linked Data

Currently it goes against Blender's design to write to linked-in
datablocks. However, for flexibility in editing and adding to the pose
library, it could be advantageous to still allow this. This would make
it possible for an animator to quickly store poses in a personal
library, for example. Further investigation is necessary.
