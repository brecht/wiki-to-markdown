# Animation & Rigging Meeting Notes template

This is what I copy-paste into a new thread on DevTalk to start new
[Animation & Rigging Module Meeting
notes](https://devtalk.blender.org/tag/animation-rigging).

``` markdown

*The main goal of this meeting will be a status update on ongoing work.*

*The meeting will be be open for everybody interested to join on Google Meet (link below).*

## Links

* [Google Meet](https://meet.google.com/ora-nbxv-evf)
* [Previous meeting notes]()
* [Workboard](https://developer.blender.org/project/board/5/?order=priority)
* [Patches](https://developer.blender.org/project/5/item/view/440/)
* [#animation-module](https://blender.chat/channel/animation-module) chat channel

## Since the Last Meeting / Announcements

## Short-term goals

### From last meeting

### New short-term goals

## Meeting-specific Topic

## Help Needed

## Next Meeting

The next meeting will be on **Thursday 17 December, 16:00 CET/Amsterdam time**. Again it will be open for everybody who's interested. The provisionary meeting agenda will be linked in the [#animation-module](https://blender.chat/channel/animation-module) channel before the meeting.
```
