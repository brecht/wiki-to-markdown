## Future Work

[Proposal](mattoverby_proposal.md)

[Weekly
Reports](https://devtalk.blender.org/t/gsoc-2020-soft-body-sim-weekly-reports/13674)

[Disccussion and
suggestions](https://devtalk.blender.org/t/gsoc-2020-soft-body-sim-discussion-and-suggestions/13765)

### Major tasks

  - Surface attachment constraints:
    <https://doi.org/10.1109/TVCG.2017.2730875>
  - Re-enable MCGS linear solver with collisions
  - Plasticity or cumulative deformation
  - Render lattice in Blender viewport
  - Using Blender's spline/curve editor to define elastic model via
    <https://doi.org/10.1145/2766917>
  - Pin group to exclude certain vertices from simulation
  - Tetrahedralize nested cages as lattice:
    <https://doi.org/10.1145/2816795.2818093>
  - Continuous Collision Detection
  - Friction: DOI 10.1145/3386569.3392396

### Minor tasks

  - Robustify TetGen and support tet mesh collisions
  - Make use of BKE\_report to show errors
  - Use translation matrix on obstacles for substepping instead of
    recomputing SDF
  - Materials: St Venant-Kirchhoff, Spline-based
    (https://doi.org/10.1145/2766917), Stable neo-Hookean
    (https://doi.org/10.1145/3180491)
  - Exact mass computation: <https://doi.org/10.1145/1360612.1360646>
  - Use vertex weight painting to define lattice resolution
  - Vertex mass painting
  - Templated to switch between float/double
  - Unit tests
