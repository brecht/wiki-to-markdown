## Volumetric Soft Body Simulation

### Name & Contact

  - Matthew Overby
  - mattoverby@gmail.com
  - www.mattoverby.net
  - www.github.com/mattoverby
  - Blender ID: mattoverby

### Synopsis

Elastic deformation is an essential component of animation pipelines.
Volumetric soft body simulation can be used to capture the complex
details of muscle and skin, e.g. volume preservation under
collision/compression. In visual effects, such methods must also be
robust to extreme deformation (no numerical blow-ups) and fast enough
for interactive manipulation. Moreover, many of the underlying material
models are nonlinear and computationally expensive, such as
hyper-elastic models for realistic tissue or artistically-defined
deformation. Toward these challenges I propose the implementation of a
volumetric soft body solver in Blender, that is not only rapid and
robust, but is general to hyper-elastic materials. The solver will also
be capable of robustly resolving self-collisions and other constraints.
This would greatly improve the use of physics in Blender for animation
pipelines.

### Benefits

Blender does not have a volumetric soft body solver. The current
placeholder treats soft bodies as a stiff thin shell. This approximation
greatly limits the visual quality and realism of the simulation in a
number of ways: no volume preservation, poor momentum conservation,
inaccurate collision response, and tedious workarounds (if any exist) to
combat the aforementioned problems. By implementing a true soft body
solver in Blender, it will improve the usage of physical simulation in
production environments for deformation tasks. In the future, the solver
may serve artists and animators for a variety of functions. For
instance, modifications of the proposed solver (ADMM-PD, described in
project details) has already found use in the visual effects industry
for geometric modeling \[1\], skin-slide deformation \[2\], and physical
simulation of layered muscle/tissue \[3\].

### Deliverables

I will implement an elastic solver in the Blender code base. This
includes:

  - The solver itself
  - Collision detection, either a custom implementation or adaptation of
    other Blender code
  - Several important (and personal favorite) material models:
    As-Rigid-as-Possible (ARAP) \[4\], some Neo-Hookean-ish energy, and
    Xu-Splines \[5\]
  - End-user documentation, including information about adjustable
    parameters and how they impact the visual result of the simulation

A sophisticated user interface for character rigs is not the focus of
this project. The goal is the implementation of an optimized soft body
solver that is well suited for muscle/skin simulation in character
modeling and animation.

### Project Details

The solver will be based on ADMM-PD \[6\]. In short, the method operates
by minimizing the equations of motion cast as an optimization problem.
It does so through operator splitting, in which momentum and elastic
energies are solved separately in an alternating fashion over a number
of iterations. The approach is robust to large time steps and extreme
deformation, and is accelerated through precomputation and
parallelization.

One minor challenge is that Blender does not have direct support for
tetrahedral meshes. To handle this, I will use an embedding tetrahedral
lattice that is generated as needed. In this approach, the surface
vertices are embedded barycentrically within the invisible lattice, and
move when the lattice deforms. This has some noteworthy benefits,
particularly that the degrees of freedom can be controlled directly (to
balance visual fidelity with run time) and the embedded mesh does not
need to be watertight or even manifold, which greatly increases
usability. Adding tet-mesh input/output or tet-meshing in the future is
possible, as the solver (excepting collision detection) is agnostic to
what type of tet-mesh is used.

### Project Schedule

  - May 18 - June 15: Implementation of the optimized bare-bones soft
    body solver in Blender. This includes the integrator, rudimentary
    lattice generation, and ARAP energy.
  - June 16 - July 13: Improve constraint generation and solver. Add
    support for self-collision and user manipulation.
  - July 14 - August 10: Add additional energy models. Finish any
    uncompleted tasks from previous milestones.
  - August 10 - August 17: Final write up of documentation. Polish code
    for readability, comments, and cleanliness.

### Bio

I am a PhD candidate in Computer Science at the University of Minnesota.
I research methods for physics-based animation with a focus on elastic
deformation, and I am the author of ADMM-PD \[6\]. Some of my previous
work is hosted on my GitHub. Other projects (e.g., microclimate
simulation) and work experience can be found on my home page.

Over the years I have benefited greatly from open source software and
dev libraries. Most of the work I’ve done could not have been completed
without them. It’s been a desire of mine to give back something useful
for a long time now, and GSoC presents a great opportunity. Blender
needs a soft body solver that is well suited for character/creature
defo, and I have been looking for an excuse to port our solver to
Blender.

### Bibliography

\[1\] David Minor. 2018. Making space for cloth simulations using energy
minimization. In ACM SIGGRAPH 2018 Talks (SIGGRAPH ’18). Article 41,
1–2. <DOI:https://doi.org/10.1145/3214745.3214764>

\[2\] Jun Saito and Simon Yuen. 2017. Efficient and robust skin slide
simulation. In Proceedings of the ACM SIGGRAPH Digital Production
Symposium (DigiPro ’17). Article 10, 1–6.
<DOI:https://doi.org/10.1145/3105692.3105701>

\[3\] David Minor and David Corral. 2018. Smeat: ADMM based tools for
character deformation. In SIGGRAPH Asia 2018 Technical Briefs (SA ’18).
Article 2, 1–4. <DOI:https://doi.org/10.1145/3283254.3283279>

\[4\] Olga Sorkine and Marc Alexa. 2007. As-rigid-as-possible surface
modeling. In Proceedings of the fifth Eurographics symposium on Geometry
processing (SGP ’07). Eurographics Association, Goslar, DEU, 109–116.
<https://dl.acm.org/doi/10.5555/1281991.1282006>

\[5\] Hongyi Xu, Funshing Sin, Yufeng Zhu, and Jernej Barbič. 2015.
Nonlinear material design using principal stretches. ACM Trans. Graph.
34, 4, Article 75 (July 2015), 11 pages.
<DOI:https://doi.org/10.1145/2766917>

\[6\] M. Overby, G. E. Brown, J. Li and R. Narain, "ADMM ⊆ Projective
Dynamics: Fast Simulation of Hyperelastic Models with Dynamic
Constraints," in IEEE TVCG, vol. 23, no. 10, pp. 2222-2234, 1 Oct. 2017.
<DOI:https://doi.org/10.1109/TVCG.2017.2730875>
<https://mattoverby.net/pages/admmpd_abstract.html>
