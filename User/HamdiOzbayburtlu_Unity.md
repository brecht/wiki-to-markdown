## Hamdi Ozbayburtlu (HamdiOzbayburtlu\_Unity)

#### Contact

  -   
    **blender.chat:**
    [HamdiOzbayburtlu\_Unity](https://blender.chat/direct/BCDhwPxPjPrbHWD9F)
    **developer.blender.org:**
    [hamdio](https://developer.blender.org/p/hamdio/)
    **devtalk.blender.org:**
    [hamdio](https://devtalk.blender.org/u/hamdio/)
    **Twitter:** [@waLLkie\_](https://twitter.com/waLLkie_)

#### About me

Working as a Senior Software Engineer at Unity. Developed systems and
processing pipelines for Streaming Media and CV Apps before. And I love
being on two wheels
