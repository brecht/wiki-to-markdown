### Week 10 (9th Aug - 15th Aug)

  - Added toggle button for switching on and off thumbnails in video
    editor.
  - Removed progress bar for thumbnail jobs, add transparency to images
    on overlap.
  - Fixed pixel errors on cut thumbnails.
  - Cleanup according to code style, removed unnecessary code.
  - Next is upload patch for review, make final report.

  

### Week 9 (2nd Aug - 8th Aug)

  - Fixed the limits on the caching job, unnecessary images not loaded
    into cache now
  - Removed Global Hash for the sequences. No memory errors
  - Improved speed of entire strip thumbnails.
  - Talked to mentor about final steps before final evaluation.
  - Next week, remove progress bar, add checkbox for thumbnails overlay
    on/off and final cleanup before GSoC final evaluation.

  

### Week 8 (26th july - 1st Aug)

  - Not much done as college projects and tests took up majority of the
    time this week.
  - Improved the job needed/not needed checking code.
  - Removed bug in getting strip start\_frame.
  - Changed code to duplicate strips and add to hash table. Faster
    caching of strips and no need to reopen the same file every time job
    is called
  - Fixed bugs in cropping, SEQ\_TYPE\_IMAGE support.

  

### Week 7 (19th july - 25th july)

  - 19th - Discussed with mentor and worked out the steps for higher
    level strip thumbnail caching job setup.
  - 20th - Started setting up the functions needed for the same.
  - 21st - Finished V1 of the task, some bugs in code.
  - 22nd - V1 done, committed to branch
  - 23rd & 24th - Changed code structure to make it simpler and cleaner.
    V2 ready. Fast drawing and caching possible now.
  - 25th - Bugs fixed. No crashes or memory losses. Pushed to branch.
    Just cleanup / testing next week and maybe try to improve
    performance

  

### Week 6 (12th july - 18th july)

  - Bug fixing - Fixed conditions that started thumbnail job, fixed
    refresh cache and strip slip broken error(crash on both operations)
  - Updated strip slip changes - now works better (prior it used to
    refresh the images)
  - Checked GHash code to add multiple strips to job.
  - Talked to mentor about multiple strips support, different methods
    found - some have good performance improvements.
  - Next week implement multiple strips support. Final step remaining,
    then code cleanup and improving code quality.

  

### Week 5 (5th july - 11th july)

  - Had college exams so not much work was done.
  - Thumbnail Job ready
  - Fixed some bugs in the caching job and made it work for single
    strips. Multiple strips not yet supported.
  - Created task and uploaded diff.
  - Next week also busy with tests so mostly bug fixing, code cleanup
    and checking how to have multiple strips running smoothly together

  

### Week 4 (28st june - 4th july)

  - 28th & 29th - Looked into wmJob code for thumbnail drawing job
  - 30th - Set up the functions needed for Job.
  - 1st & 2nd - First thumbnail Job test done. Found errors.
  - 3rd & 4th - Bug fixing. Final code not ready for commit or update on
    phab. Next week fixing code and update phab.

  

### Week 3 (21st june - 27th june)

  - 21st - Fixed the thumbnail size to max 256 on either x or y axis.
    New functions for thumbnail transform in the preprocessing stage.
  - 22nd - Code cleanup - Shifted code to dedicated functions for
    rendering image and caching after scaling.
  - 23th & 24th - Set a limit for the number of thumbnails cached. fixed
    space used at all time, no unnecessary usage in the case of prefect
    and cache limit reached.
  - 25th & 26th - Fixed the incorrect cropping of thumbnails at the
    ends. Now smooth movement and clean cuts on either edge. Multiple
    methods tested. Fastest implemented in final code.
  - 27st - Cleaned up thumbnail drawing and caching code.

  

### Week 2 (14th june - 20th june)

  - 14th and 15th - Updated the drawing code and removed logical errors
    in drawing thumbnails. Added a limit to the number of images to be
    taken. This provided a performance increase
    [GIF](https://dev-files.blender.org/file/data/vrxm3ptxlye4q2ngdvf5/PHID-FILE-nqep57jlcjj3e67qi4ll/drawing_thumb_perf1.gif).
    Changed the way thumbnails should update on handle movement and
    added ability for strip handle to move over the thumbnail rather
    than push to the side.
  - 16th - Tried finding solution to the incorrect clipping of
    thumbnails. Looked into caching of thumbnails
  - 17th and 18th - Set-up code for the caching of thumbnails done. New
    type and flag added. Added conditions wherever required. Around 10x
    improvement in storage.
  - 19th - talked to mentor about thumbnail caching. Updated code to
    scale to low res and store rather than crop. Discussed with some
    devs about image clipping issues.
  - 20th - Fixed bug of incorrect cache storage on preview and
    sequencer.

  

### Week 1 (7th june - 13th june)

  - 7th and 8th - had college tests.
  - 9th - Read code and understood ibuf drawing functions. created
    branch `soc-2021-vse-strip-thumbnails`.
  - 10th - 12th : thumbnail drawing function done. Small bug in last
    thumbnail drawing to be fixed 14th june.
  - 13th : get the UI looking clean with no overlappings. Uploaded code
    to branch.
  - next week create task in d.b.o and have final drawing function ready
    for review

  

### Bonding Period (May 17th to June 7th)

  - Talked to mentor (Richard Antalík) about the plan and what to do as
    first task
  - Went through the sequencer code and also checked prior work done but
    later abandoned [D4426](https://developer.blender.org/D4426) and
    [D5908](https://developer.blender.org/D5908).
  - Set up devtalk for feedback and weekly reports
  - Interacted with VSE contributors on the \#vse blender.chat.
