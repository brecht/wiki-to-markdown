## GSoC 2021 : Video Sequence Editor Improvements - Individual strip previews and modification indicators

<big> [Proposal](Proposal.md)  
[Devtalk
Discussion](https://devtalk.blender.org/t/gsoc-2021-video-sequence-editor-strip-previews-and-modification-indicators-feedback/19096)  
[Weekly Report](Weekly_Report.md)  
[Final Report](Final_Report.md)  
<big>
