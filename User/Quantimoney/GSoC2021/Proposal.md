## Video Sequence Editor Improvements - Individual strip previews and modification indicators - Proposal

### Name

Aditya Y Jeppu  
\=== Contact === Email : adi.jeppu28@gmail.com  
Blender.chat, d.b.o, devtalk : @quantimoney  
Linkedin : [adityayjeppu](https://www.linkedin.com/in/adityayjeppu/)  
\=== Synopsis === The project focuses on the strips (video and
image/sequence) in the video sequencer. The main goal is to add a
preview of the clip in the corresponding strip of the sequencer to
replace the current flat colour rectangles and, if time permits, to
provide visible icons on the strip representing any strip modifications
made by the user.  

### Benefits

The current look of the strips in the sequencer has no indication of the
clip it represents other than the name of the file. Similar file names
and truncated display text in the smaller strips can lead to confusion
in the orientation of the clips. Each clip cannot be viewed individually
to check the contents. Enabling the ability to see the clip frames in
the strips area will aid the user in basic strip layout tasks. Icons
representing strip modifications for individual strips in the sequencer
will provide a unified view of all changes (modifier wise), that can aid
the user to keep track of the changes made and check the uniformity of
changes across multiple strips.

### Deliverables

The final result is an improved sequencer with the strips (of the video
and the image/sequence) containing additional information. Each strip
has  
\* the preview of the clip

  - the icon representing the modifications applied.

The icons are there to only represent the pre-existing modifications
which are already in blender. No new modifiers are proposed.

### Project Details

Currently, blender uses flat rectangular colour tiles to represent
individual clips in the video sequencer. A preview of the proposed final
video sequencer UI is provided in the figures below **(Figures a,b,c)**.
**Figure (a)** shows a clip with the preview image. Here, the small
rectangle width(vertical) for the strip should be changed to a default
larger size to incorporate a clear enough view of the preview image(s)
when a new movie clip strip is added. The preview image can be cut to
the appropriate length of the strip as shown in **(a)**. When the length
(horizontal) of the strip is wide enough, multiple equally distributed
frames of the video clip can be used as preview, with equal gaps between
each frame, as shown in **Figure (b)**. The size of each frame in this
case is set based on the available width (vertical) of the rectangular
strip and in accordance to the aspect ratio of the actual clip. For long
strip thumbnail views, the number of such frames depend on the length of
the rectangle strip.  

<center>

Image(a) VSE strip thumb.png|Figure (a) Image(b) VSE Strip thumb
longstrip.png|Figure (b) Image(c) VSE modifier indicators.png|Figure (c)

</center>

This involves modifications to `sequencer_draw.c` and `sequencer.c`,
providing a means to generate, store and draw the thumbnails. Drawing
thumbnails might have to be its own operation to avoid slowing down the
UI and the images loaded could be only those which are in view. This is
done only for movie clip strips and image sequences. The display of the
file name, waveforms and the handles on either side also would be
layered over the thumbnails for a clean interface.  
  
For the second half of the project, the icons for each of the modifiers
will be provided in the positions represented by the white squares in
**Figure(c)** . The icons can be shown only when the modifiers are
applied and are arranged in alphabetical order, taking no more space
than required. This should be done to have a uniform ordering of the
icons, even when some icons aren’t needed. New icons for each modifier
need to be designed and will be based on the blender icons design. This
will require discussions with the UI team and done once the strip
previews are working.

### Timing Considerations

Due to the pandemic, my undergraduate college calendar has shifted and I
have my final semester examinations spread across the month of August.
So, I have chosen a relatively easy topic to account for conflicts with
the GSoC coding period. I also have tests during the last week of June
and July. Therefore, my primary goal for the project is the work on clip
previews for the sequencer and I will continue to work on the icons if
time permits. That leaves me around 6 weeks for intensive coding, plus
any additional work that can be done before the coding period,
especially project planning and in depth understanding of the related
portions of the codebase to identify what changes can be made.

### Bio

I am a 2nd year BTech student studying Computer Science and Engineering
at RV College of Engineering, Bangalore, India. I am 20 years old. I
started using Blender in 2013-14 by trying a step by step tutorial on
making the street scene from the Inception movie. This gave me a good
hold of the basics but I predominantly used blender for patent models
and simulations. Over the last year I saw the true power of blender for
art following artists like Ian Hubert and I wanted to contribute to
Blender development. I have used C for over two years now and also have
prior python experience. I have a good understanding of the blender code
especially the sequencer code like adding and drawing strips, background
UI and scroll bar, the overlays for the strips, Image Buffer and
MovieClip structures and how they are used.
