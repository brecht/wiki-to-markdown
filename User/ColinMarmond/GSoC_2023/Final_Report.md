# GSoC 2023: add shader node previews

## Project recap

This project was about implementing a way to view the effect of a node
used in a shader graph. The approach taken was to render a preview scene
with the result of what the shader graph would look if it ended with
this node. Actually a new rectangle area is added with the node so that
the user can have all nodes effect in sight.

A small video can better explain what the purpose exactly is:
![../../../videos/Shader\_nodes\_preview.mp4](../../../videos/Shader_nodes_preview.mp4
"../../../videos/Shader_nodes_preview.mp4")

## What I did during this project

<table>
<tbody>
<tr class="odd">
<td><div class="note_title">
<p><strong>Optimisation introduction</strong></p>
</div>
<div class="note_content">
<p>The task of rendering the preview for every node is expensive so the goal is to re-render the smallest amount of previews while not underestimating the changes. It's all about how much previews we redraw for each change in the node-tree (less false positive as possible).</p>
</div></td>
</tr>
</tbody>
</table>

When the project was active, those links were the threads I used to
communicate:
<https://devtalk.blender.org/t/gsoc-2023-shader-editor-node-preview-weekly-reports>
<https://devtalk.blender.org/t/gsoc-2023-add-a-shader-preview-node-feedback>

### Prepare the ground for the main functionality

In order to start coding the main functionality, I had to update some
older designs existing in Blender before, such as:

  - Move the existing node previews (Compositor nodes) to an overlay.
    [PR
    \#108001](https://projects.blender.org/blender/blender/pulls/108001)
  - Use \`ImBuf\` structure for the compositor nodes. [PR
    \#110064](https://projects.blender.org/blender/blender/pulls/110064)

### Implement this functionality as a whole (MVP: Minimal Viable Product)

In order to be able to merge something one day, I had to start simple
and then add more things later. This first commit took advantage of the
node-tree update functions to launch a new render containing the preview
scene with a ViewLayer for each previewed node. In this patch, a trick
is used to improve efficiency of the non-shader nodes (only textures and
math) previews: we use the AOV pass to generate the previews for those
nodes. This allows the rendered to render all AOVs previews with just
one ray per pixel. [PR
\#110065](https://projects.blender.org/blender/blender/pulls/110065)

### Incrementally improve the MVP

After adding the main functionality, some things were missing and had to
be fixed or improved. It mainly concerned design, and some improvements
in the preview routing. Pull requests: [PR
\#110958](https://projects.blender.org/blender/blender/pulls/110958)[PR
\#110945](https://projects.blender.org/blender/blender/pulls/110945)[PR
\#110949](https://projects.blender.org/blender/blender/pulls/110949)[PR
\#111041](https://projects.blender.org/blender/blender/pulls/111041)

## Curent state of this project and things left to do

The project is nearly complete (functionality wise), but some things are
not yet merged or not finished at this point. Regarding the work in
progress patches, there are:

  - [PR
    \#111040](https://projects.blender.org/blender/blender/pulls/111040):
    UX(User Experience) for the preview area size in the
    shader/compositor node editors
  - [PR
    \#110725](https://projects.blender.org/blender/blender/pulls/110725):
    Use individual node preview refresh (reduce the number of false
    positives)

Those patches are non-blockers for the main functionality, and I
probably finish that after the end of Google Summer of Code. Some things
are still to be coded and improved. Just to list a few:

  - improve performances
  - improve the UX
  - rendering tweaks

Here is a task tracking the progresses of this project (all todos for
future work):
[\#110353](https://projects.blender.org/blender/blender/issues/110353)

## My thoughts

First time GSOC for me, and it has been an extremely interesting
experience. My contribution to the Blender project started few years ago
and it was a pleasure to officially be recognised for this.

I learnt many things in multiple areas, such as:

  - communication
      - learn to exchange with more ease with the community and the
        other developers of Blender
      - manage the successes and fails with my mentor
      - make decisions out of everything people are saying in the chat
        and forum
      - take more time to discuss the designs before coding
      - the whiteboard is the greatest ally
  - work time
      - deal with interrogations, doubts
      - improve the work efficiency
      - improve my comprehension of the review process
      - learn about some C++ functions and alternative uses
  - project management
      - figure out that doing easily maintainable and integrated changes
        is in most cases better than introducing new structures (even if
        they can be more appropriate sometimes)
      - maintain a working environment (git/build folders)

I would thank every developers of the Blender team (Brecht, Jacques,
Hans) and the other ones present in the chat to help. The users
participating in the feedback thread were of an incredible help to
determine designs and alternatives.

Thank so much everyone for the trust, I tried my best this summer and
I'm looking forward to completing other similar projects in the future.
