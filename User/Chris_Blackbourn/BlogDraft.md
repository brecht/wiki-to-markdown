This blog post will spotlight Chris Blackbourn's work on UV editing
tools since mid-2022 and the latest enhancements available now in
[Blender 3.6](https://www.blender.org/download/releases/3-6/).
Furthermore, he'll give us a preview of potential updates to anticipate
in Blender 4.0.

# A New Approach to UV Packing

In Blender 3.6, the UV packing is considerably faster and smarter. The
layouts use less space and are packed more tightly. Let's explore this
new feature.

### The Turbocharged Alpaca

At the heart of the strategy is the new "Alpaca\_turbo" packer. It's
designed for speed, capable of packing millions of islands in just a few
seconds.

Alpaca.png

Here we can see the Alpaca in progress. The islands are already sorted
from largest to smallest, and the light green rectangle on the bottom
left shows islands which are already placed and cannot be moved. Islands
are placed sideways across the top of the existing rectangle until we
get to the edge. Next, we start on the bottom right side, and keep
placing islands until we get to the top.

Then repeat.

Of course, that's where the name comes from, the *Alpaca* is really the
*"L"-Packer*, named after the "L" shaped pattern it uses to place the
islands.

One huge advantage of the Alpaca is the way it combines with other
packers. In Blender, the largest 1000 islands are packed with a
conventional packer into the rectangle in the lower left, and then the
Alpaca\_turbo takes care of all of the rest of the islands.

In this way, by combining other packers with the Alpaca, regardless of
how many islands you're packing, you always get a great result in a
reasonable amount of time.

### The Xatlas Approach

The "xatlas" strategy forms another key part of our approach. It
leverages bitmap occupancy to achieve an incredibly tight pack. With
added tricks to handle both exact shape (concave) and the convex hulls,
xatlas provides high-quality layouts without compromising on speed.
(Blender's implementation of the xatlas strategy is inspired by the
original reference implementation over at
[xatlas](https://github.com/jpcy/xatlas).)

Xatlas sample.png|PNG Version Xatlas.svg|SVG Version

Here we see an example of the "xatlas" UV packing algorithm as
implemented in Blender 3.6.

### Packing, Optimally

![Single islands are now packed optimally. The island touches all four
sides of the unit square.](../../images/SingleOptimal.png
"Single islands are now packed optimally. The island touches all four sides of the unit square.")
![Many layouts consisting of quads are now packed optimally, including
this surprising example with 18 equal sized
squares.](../../images/Pack18.png
"Many layouts consisting of quads are now packed optimally, including this surprising example with 18 equal sized squares.")
For the first time, Blender now includes **optimal** packs. For a few
important types of islands, we can now produce layouts which are
"optimal". While the optimal layout is not unique, we can show
mathematically that there is no tighter pack possible.

### More Packing Features

We've added lots of new options too, for pinned islands, rotation
options, pack to original bounding box and merge overlapping.
![../../images/PackOptions.png](../../images/PackOptions.png
"../../images/PackOptions.png") Check out the [official packing
documentation](https://docs.blender.org/manual/en/latest/modeling/meshes/uv/editing.html#pack-islands)
for all of the details.

### Putting it together

With these improvements to Blender's UV packing, you can now expect to
see a major reduction in wasted texture space, and thanks to the speed
of the Alpaca\_turbo packer, you can pack even very complex models in a
reasonable amount of time.

<hr>

# What else is new in UV Mapping in Blender?

Good UV Mapping starts with a balancing act. We always have to trade off
*distortion* against *seams* against *texel density*.

Lets dive right in by repairing this *seam* with [Unwrap
Selection](https://developer.blender.org/T78394)

|                                                                                                                                     |                                                                                    |
| ----------------------------------------------------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------- |
| ![Close up of UV Editor with default monkey object.](../../images/Untitled.png "Close up of UV Editor with default monkey object.") | ![After UV Unwrap Selected](../../images/Untitled2.png "After UV Unwrap Selected") |

  
![After minimize stretch](../../images/Untitled3.png
"After minimize stretch") Great\! Now we can reduce the *distortion*
with Minimize Stretch. BTW, it now [fixes flipped
faces](https://developer.blender.org/T99781).

  
![After geometry relax brush with pins](../../images/Untitled4.png
"After geometry relax brush with pins") Okay, time for *texel density*.
Lets pin those corners and switch over to the new [geometry relax
brush](https://developer.blender.org/rB74ea0bee9c0a) which uses the
shape of the geometry to drive the shape of the UVs:

<hr>

### Average Islands Scale

![Average Islands Scale (original)](../../images/Untitled5.png
"Average Islands Scale (original)") ![Average Islands Scale, Non uniform
and Shear](../../images/Untitled6.png
"Average Islands Scale, Non uniform and Shear") Lets explore two new
options in [Average Islands Scale](https://developer.blender.org/T99684)
to improve the quality of your unwraps.

The original version evens out texel density between different islands.

  - With the new **Non-Uniform** option, differences between the **U**
    axis and **V** axis are balanced.
  - With the **Shear** option, the **U** axis and **V** axis will be
    balanced back to 90°, even further reducing texture distortion.

### Copy and Paste for UVs

One great new feature in Blender 3.6 is the ability to "Copy" and
"Paste" UVs. This works great for gaming workflows where you might have
repeated geometry for eyes, hands, or even larger pieces like arms and
legs. For hard surface modelling, it works great on rivets, switches,
bevels, spokes, chains etc.

Just select the UVs you want to copy, copy them, and then paste them
onto the UVs. This works between UV layers, and you can even copy UVs
between different .blend files, as long as the topology matches between
the geometry of the source UVs and the topology of your target.

# Blender 4.0

... TODO ...

  - More game developer features.
  - Better UDIM support.
  - Automatic seam placement.
  - Maybe it’s a One-Click “Just Unwrap It For Me” button.
