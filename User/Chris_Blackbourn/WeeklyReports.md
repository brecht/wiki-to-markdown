See also
[WeeklyReports2023](WeeklyReports2023.md)

## Prior Work

... - May 8

*' UV (With Campbell Barton)*'

  - Root cause analysis for [Applying Subdivision/Multires Modifier
    De-merges UV's](https://developer.blender.org/T81065).
  - Investigate [Cube UV projection correct aspect
    option](https://developer.blender.org/T86358)
      - [Fix](https://developer.blender.org/D14852)
  - Root cause analysis for [Angle-based UV distortion with
    quads](https://developer.blender.org/T78101)

## Calendar Week 19

May 9 - May 15

*' UV (With Campbell Barton)*'

  - Debug [UV Constrain to bounds](https://developer.blender.org/T50398)
      - [Fix](https://developer.blender.org/D14882)
      - [Commit](https://developer.blender.org/rBbc256a4507702a28b825d7784c074e040058fb46)
  - Debug [Inaccurate hitbox axis
    gizmo](https://developer.blender.org/T89726)
      - [Fix pending review](https://developer.blender.org/D14930)
  - Debug [UV Projection
    per-face-aspect](https://developer.blender.org/T86358)
      - [Fix](https://developer.blender.org/D14852)
      - [Commit](https://developer.blender.org/rB1c1e8428791fe376ec67ff96b15b5deae8f18296)
  - Debug [UV Transform Tool - broken
    handles](https://developer.blender.org/T83310) Cannot repro,
    resolved.
  - Debug [Python API uv.select\_edge\_ring
    etc](https://developer.blender.org/T89634) Cannot repro, resolved.
  - Investigate [UV Editor
    demerge](https://developer.blender.org/T86754)
      - Most likely a duplicate of [Applying Subdivision/Multires
        Modifier De-merges UV's](https://developer.blender.org/T81065).
      - More info requested.
  - Reported [Individual origins scale
    problem](https://developer.blender.org/T98061)
  - Debug [Live unwrap pin bugs](https://developer.blender.org/T75007)
      - Actually two seperate problems. TODO: Split report Done:
        [T98240](https://developer.blender.org/T98240)
        [T98239](https://developer.blender.org/T98239)
      - Repro both problems and identify both root causes.
      - Waiting on design feedback before fixing.
  - Develop feature [Unwrap
    selected](https://developer.blender.org/T78394)
      - [Abandoned change](https://developer.blender.org/D14933)
      - [Add pin\_unselected](https://developer.blender.org/D14945)
        (Pending review)

*' Blender-Coders*'

  - Confirm faulty BLI\_ASSERT during file handling on macOS.

## Calendar Week 20

May 16 - May 22

*' UV (With Campbell Barton)*'

  - Develop feature [Randomize UV
    islands](https://developer.blender.org/T78406)
      - Waiting on design review.
  - Report and Fix [UV selection crash with wire
    edges](https://developer.blender.org/T98214)
  - Create [Live unwrap is non responsive if mesh has no boundaries and
    no seams.](https://developer.blender.org/T98240)
  - Create [Unwrap and Live Unwrap can merge pinned
    UVs](https://developer.blender.org/T98239)
      - Proof of concept fix.
  - [Cleanup Refactor PHandle -\>
    ParamHandle](https://developer.blender.org/P2964)
  - Develop feature [Unwrap
    selected](https://developer.blender.org/T78394)
      - [Add pin\_unselected](https://developer.blender.org/D14945)
        (Pending review)

## Calendar Week 21

May 23 - May 29

*' UV (With Campbell Barton)*'

  - [Commit Refactor PHandle -\>
    ParamHandle](https://developer.blender.org/rBfdb1a7b5e154)
  - Unable to repro [UV selection
    modes](https://developer.blender.org/T76775), marked as need more
    info.
  - Debug [UV Gizmo hitbox](https://developer.blender.org/T89726)
  - Identify root cause for [UV Align tool
    problem](https://developer.blender.org/T78553)
  - Develop feature [Unwrap
    selected](https://developer.blender.org/T78394)

## Calendar Week 22

May 30 - June 6

*' UV (With Campbell Barton)*'

  - Fix [UV Gizmo hitbox](https://developer.blender.org/T89726)
    [commit](https://developer.blender.org/rB65e7d4993933)
  - Develop feature [Unwrap
    selected](https://developer.blender.org/T78394)
  - Cleanup
    [bools](https://developer.blender.org/rBfb08353f3864c512e20822e802f24bb1b827f28f)
  - Cleanup [remove
    unused](https://developer.blender.org/rBb450a8c85195a609300e7ad0934dadbbb5ac938f)
  - Fix and feature creep on [UV
    Straighten](https://developer.blender.org/T78553)
  - Identify root cause [toggle layer redraw
    problem](https://developer.blender.org/T78815)
  - Debug [UV Unwrap solver
    symmetry](https://developer.blender.org/T78101)
  - Develop feature [Randomize UV
    Islands](https://developer.blender.org/T78406)
  - Discuss with Łukasz Re: [SLIM unwrapping
    algorithm](https://developer.blender.org/T48036)
  - Identify root cause [Unsubdivide produces overlapping
    UVs](https://developer.blender.org/T81065)

## Calendar Week 23

June 7 - June 13

*' UV (With Campbell Barton)*'

  - Fix [UV vis on a globally hidden
    collection](https://developer.blender.org/T78815)
    [commit](https://developer.blender.org/rB38bfa53081e953fd8c9894dd5f057fa8b62c5fc8)
  - Improve circle drawing
    [commit](https://developer.blender.org/rB4c7b0804f83f)
    [commit](https://developer.blender.org/rB9d7e73144409)
    [commit](https://developer.blender.org/rBcc4d46d91ee1)
  - Develop feature [UV Select
    Similar](https://developer.blender.org/D15164)
  - Bug fixing [UV unwrap merge pinned
    verts](https://developer.blender.org/T98239)
  - Develop feature [UV
    Straighten](https://developer.blender.org/T78553)

## Calendar Week 24

June 14 - June 20

*' UV (With Campbell Barton)*'

  - Fix [Wrong face dot color](https://developer.blender.org/T98699)
    [commit](https://developer.blender.org/rBf5dae5844c7ead5788bd4f2a1d89d6a2a97c4169)
  - Fix [Unwrap merges pinned UVs](https://developer.blender.org/T98239)
    [commit](https://developer.blender.org/rBe6e9f1ac5a2dae42dc523784b03395a50671afaa)
  - Debug [KDTree problem](https://developer.blender.org/D15220)

## Calendar Week 25

June 21 - June 27

*' 3D Modeling (With Campbell Barton)*'

  - Fix [Select Similar](https://developer.blender.org/T99033)
  - Fix [Circular gizmo drawing
    in 3D](https://developer.blender.org/T99182)

*' UV (With Campbell Barton)*'

  - Fix [KDTree problem](https://developer.blender.org/D15220)
    [commit](https://developer.blender.org/rB95465606b33c5d1b36f30d007b7ad6d9d6efba4c)
  - Type cleanup [Select
    Similar](https://developer.blender.org/rB4144a85bda45)
  - Add [Select Similar UV](https://developer.blender.org/T47437)
    [commit](https://developer.blender.org/rB1154b4552679)
  - Fix [UV Pins interfere with UV
    seams](https://developer.blender.org/T99156)
  - Debug [Live unwrap problem](https://developer.blender.org/T98240)
  - Fix [Wrong Face dot color](https://developer.blender.org/T98699)
    [commit](https://developer.blender.org/rB9900e534fe4b)
  - Develop feature [Unwrap
    Selected](https://developer.blender.org/T78394)

## Calendar Week 26

June 28 - July 4

*' UV (With Campbell Barton)*'

  - Debug [UV Unwrap symmetry](https://developer.blender.org/T78101) (PM
    me for details.)
  - Commit [Unwrap Selected](https://developer.blender.org/T78394)
  - Develop feature [Align
    rotation](https://developer.blender.org/T78399)
  - Develop feature [UV Randomize
    islands](https://developer.blender.org/D15349)
  - Debug [UV Island with edge
    selection](https://developer.blender.org/T79304)
  - Debug [Live UV unwrap with no
    boundaries](https://developer.blender.org/D15263)
  - Debug [Margin Size during
    pack](https://developer.blender.org/T90782)
  - Fix [UV Straighten](https://developer.blender.org/D15121) (pending
    commit)
  - [Cleanup](https://developer.blender.org/rB77f10fceb2b0)
    [Cleanup](https://developer.blender.org/rBe1dc54c8fc86)
    [Cleanup](https://developer.blender.org/rBf32d7dd0c8ae)
    [Cleanup](https://developer.blender.org/rB3283bc63676c)

## Calendar Week 27

July 5 - July 11

Research week as we're delaying larger changes until the Blender 3.3
(LTS) branch.

Sync and edit documentation repo.

*' UV (With Campbell Barton)*'

  - Implement [UV Island calculation for Edge
    Selection](https://developer.blender.org/D15419) (pending commit)
  - Fix [UV Straighten](https://developer.blender.org/D15121)
    [commit](https://developer.blender.org/rB9dd27a2c87f4)
    [Docs](https://developer.blender.org/rBM9392)
  - Fix [UV Resize problem](https://developer.blender.org/T98061)
    [Pending Review](https://developer.blender.org/D15420)
  - Research [UV Pack Margin](https://developer.blender.org/T90782) and
    [UV Pack convex hull](https://developer.blender.org/T68889)
  - Research [UV Align Rotation](https://developer.blender.org/T78399)
  - Research [Upgrade Average Islands
    Scale](https://developer.blender.org/D15421)

I also researched LSCM unwrapping without using any pins (affects
multiple tasks including <https://developer.blender.org/T78101> etc) by
adding additional constraints on the least squares matrix.

## Calendar Week 28

July 12 - July 18

*' UV (With Campbell Barton)*'

  - Upgrade [Average Island
    Scale](https://developer.blender.org/rB931779197a9ce141eccc8b8c500f9ef726a833eb)
    with new options, Scale UV and Shear
  - Improve [UV Island
    Calculation](https://developer.blender.org/rB8f543a73abc42843fb924fc6d849e3055e3ae011)
    with Hidden faces.
  - Improve [UV Island
    Calculation](https://developer.blender.org/rB178868cf42594bf7eedfa4db93ba8b7f3bf017ce)
    in Edge Select mode.
  - Fix [UV Resize
    problem](https://developer.blender.org/rB4d7c9901800835122413faebd9ead267f1f32285)
    with constrain to bounds and individual origins.
  - Report and Fix [Minimize
    Stretch](https://developer.blender.org/D15475) won't unflip faces.

## Calendar Week 29

July 19 - July 25

*' UV (With Campbell Barton)*'

  - Commit [Minimize
    Stretch](https://developer.blender.org/rB135e530356d09378f153c4cb483a77b3375cc4f2)
    unflips faces.
  - More review on [Slim unwrap](https://developer.blender.org/D15195)
  - Investigate and patch [Missing UV data in corrupt .blend
    file](https://developer.blender.org/rBc171e8b95c00)
  - (Unreported) Investigate convert LSCM unwrapper to C++.
  - (Unreported) Investigate correcting boundaries for Relax brush, to
    unlock boundaries.
  - (Unreported) Investigate cotan formula for Relax brush, to preserve
    mesh geometry and reduce direction changes across edges. [Sneak
    Peek](https://developer.blender.org/D15530)

## Calendar Week 30

July 26 - August 1

Researching packing algorithms.

Debug \`march=native\` problem with building cycles on apple silicon
macOS. (See [build error](https://developer.blender.org/rBcd9ebc816ece))

*' UV (With Campbell Barton)*'

  - Add new options for UV Island select similar: [Face, Area, Area
    UV](https://developer.blender.org/rBa5814607289a)
  - Simplify uv\_parametrizer in preparation for conversion to C++
    [commit](https://developer.blender.org/rBa49b49d934ae)
    [commit](https://developer.blender.org/rBca172677b1a7)

Also reviewed [UV Texture Dimming](https://developer.blender.org/D15566)

## Calendar Week 30

August 2 - August 8

Refactor week\!

Cleanup UV Parametrizer, move to C++\! (pre-req for SLIM algorithm)
[commit](https://developer.blender.org/rBa49b49d934ae)
[commit](https://developer.blender.org/rB1ac956a9f9de)
[commit](https://developer.blender.org/rB2083b9ae5431)
[commit](https://developer.blender.org/rBfcd61d2056f8)

Fix off-by-half errors in UDIM search and add tie-break
[commit](https://developer.blender.org/rB0fcc04e7bfe1)
[commit](https://developer.blender.org/rB68c1eb86a6b8)
[commit](https://developer.blender.org/rB0d62e963b06e)

Improve circle drawing for UV Sculpt
[commit](https://developer.blender.org/rB18377c4f5e47)

Update Relax Brush documentation [Pending
Review](https://developer.blender.org/D15609)

Remove winding for UV Sculpt tool for islands
[commit](https://developer.blender.org/rBdcf50cf04668)

Prepare UVIsland calculation for Python API
[commit](https://developer.blender.org/rBe441e21d74f9)
[commit](https://developer.blender.org/rB64984126a2b6)
[commit](https://developer.blender.org/rBbb8488c62c9a)

## Calendar Week 31

August 9 - August 15

Prepare UVIsland calculation for Python API
[commit](https://developer.blender.org/rB13c5f6e08ff7)
[commit](https://developer.blender.org/rBfb7ef40006b9)
[commit](https://developer.blender.org/rBf35d671f466d)

Attempt to UV unwrap when mesh has no boundary
[Review](https://developer.blender.org/D15681)

Upgrade UV Sculpt tools with [Pin and Boundary
support](https://developer.blender.org/D15669)

Upgrade UV Sculpt tools with [Constrain to
bounds](https://developer.blender.org/D15683)

## Calendar Week 32

August 16 - August 22

UV (with Campbell Barton)

  - Add [Geometry driven relax
    brush](https://developer.blender.org/rB74ea0bee9c0a)
  - UV grab tool supports [live
    unwrap](https://developer.blender.org/rB836c07f29c76)
  - Respect UV Selection in [Spherical, Cylindrical, Cube
    Project](https://developer.blender.org/rBa5c696a0c2b9) and Smart UV.
  - Cleanup \`uvedit\_\*\_select\_\*\`
    [commit](https://developer.blender.org/rBaa82f91c922d)
  - Cleanup: replace uint cd\_loop\_uv\_offset with int
    [commit](https://developer.blender.org/rBcd516d76b640)

First pass, UV Rotation operator supports [Constrain to
bounds](https://developer.blender.org/D15730)

Relax brush documentation
[updated](https://developer.blender.org/D15609)

And debugging of [UV Islands in
Python](https://developer.blender.org/T86484)

## Calendar Week 33

August 23 - August 29

UV (with Campbell Barton)

  - Add new operator [uv randomize
    islands](https://developer.blender.org/rBde570dc87ed1)
  - Add bpy\_extras.bmesh\_utils.bmesh\_linked\_uv\_islands (see above)
  - Resolve [bpy\_extras.mesh\_utils.mesh\_linked\_uv\_islands Creates
    Exception](https://developer.blender.org/T86484)
  - Support constrain-to-bounds for [UV rotation
    operator](https://developer.blender.org/rBd527aa4dd53d4f8bd226477b2d7913abf6424c57)
  - Investigate [UV Face/island
    selection](https://developer.blender.org/D15796)
  - WIP new [align rotation
    operator](https://developer.blender.org/T78399)

Plus packing research (offline)

## Calendar Week 34

August 30 - September 5

Writing a blog post about recent changes in the UV Editor and underlying
code. Currently have outline, first draft of introduction and first
section.

UV (With Campbell Barton)

  - Identify and change face selection changes [Differential,
    parked](https://developer.blender.org/D15796)
  - Add new operator, [UV Align
    Rotation](https://developer.blender.org/D15820) (pending commit)

Preparing for packing updates :

  - Move UVEdit\_Islands to [cpp](https://developer.blender.org/D15870)
  - Add Seam support for UV Island calculation
    [Differential](https://developer.blender.org/D15875)
  - Merge UV Island calculation with UV Element Map (offline, blocked)
  - Literature search, Bin Packing 2D, e.g.
    <https://github.com/topics/2d-bin-packing>

## Calendar Week 35

September 6 - September 12

UV (With Campbell Barton)

  - Fix UV unwrap with degenerate triangles
    [commit](https://developer.blender.org/rB94e211ced914)
  - Add new operator, UV Align Rotation
    [commit](https://developer.blender.org/rB20daaeffce4cf9bfe48ab7c84cb9e2b1d71d2c91)
  - Move UVEdit\_Islands to cpp
    [commit](https://developer.blender.org/rBd9db79dbe5bec8ba541660940bf981de1c7c5c52)
  - Add Seam support for UV Island calculation
    [commit](https://developer.blender.org/rBcaf6225a3d01b3a5d471dc62bb4508477fc4e7df)

Plus packing research and blog writing.

## Calendar Week 36

![Specify margin in UV/pixel units exactly
(Fast\!)](../../images/PackingResearch.png
"Specify margin in UV/pixel units exactly (Fast!)") September 13 -
September 19

UV (With Campbell Barton)

Packing research, explicit margin and using convex hull, speed
improvements for large meshes.

  - Rename "UV Snap To Pixels" -\> "UV Round To Pixels"
    [commit](https://developer.blender.org/rBb5115ed80f197af3f0298f3c2a1e5d177804f9c4)
  - Update [docs](https://developer.blender.org/D15974) for the rename.
  - UV: Extend custom grid sizes to set each axis separately.
    [diff](https://developer.blender.org/D16000)

## Calendar Week 37

September 20 - September 26

UV (With Campbell Barton)

Packing research, explicit margin and using convex hull, "Phi functions"
for concave packing, and speed improvements for large meshes.

  - Fix [T101138](https://developer.blender.org/T101138) by removing
    console spam when hovering over tools in UV editor.
    [commit](https://developer.blender.org/rB7f284f5134ca0cf37c18302975c9632ef13f269d)
  - Add toggle to show UV grid over the top of the image in UV Editor.
    [commit](https://developer.blender.org/rBc50335b359e06751cba81e3d33a4ee0ae71daffb)
  - 2D Convex hull improvements for UV packing.
    [Differential](https://developer.blender.org/D16055)
  - Add option to use Blender 2.8 margin calculation to fix
    [T85978](https://developer.blender.org/T85978).
    [Differential](https://developer.blender.org/D16058)

Coming very soon : Specify exact margin when UV packing.

## Calendar Week 38

September 27 - October 3

Blog writing, see
[draft](https://wiki.blender.org/wiki/User:Chris_Blackbourn/BlogDraft)

UV (With Campbell Barton)

  - 2D Convex hull improvements for UV packing.
    [commit](https://developer.blender.org/rB5c93c37678d1)
  - Fix [T101414](https://developer.blender.org/T101414), packing
    problem in 3d viewport.
    [commit](https://developer.blender.org/rB5beaecb33e74)
  - Simplify storage with UV packing
    [commit](https://developer.blender.org/rB91db47e914e7)
  - Simplify UV packing for non-square materials
    [commit](https://developer.blender.org/rBe5ccbfab09ff)

... and....\!

  - Add option to specify exact margin when packing
    [Differential](https://developer.blender.org/D16121) using a
    combined search/secant root finding custom solver.

## Calendar Week 39

October 4 - October 10

More packing research\!

UV (With Campbell Barton)

  - Iterating on specify exact margin when packing
    [Differential](https://developer.blender.org/D16121).
  - Add grid shape source to the UV editor, and add new "pixel" option
    [Differential](https://developer.blender.org/D16197). See T78391
  - Research on UV Editor Selection Sync
    [T78393](https://developer.blender.org/T78393)

## Calendar Week 40

October 11 - October 17

Implementing [Graph
Isomorphism](https://en.wikipedia.org/wiki/Graph_isomorphism_problem)
for [UV Island Copy/Paste](https://developer.blender.org/T77911)

UV (With Campbell Barton)

  - [Commit](https://developer.blender.org/rBc2256bf7f714bbd41388af3e184b3d84b496fbf3)
    UV Pack option to specify exact margin.
  - ^^^ Also fixes [UV Margin regression for UV
    Unwrap](https://developer.blender.org/T90782)
  - [Commit](https://developer.blender.org/rBb7decab07ef8) Add grid
    shape source to the uv editor, and add new "pixel" option.
  - [Cleanup](https://developer.blender.org/rB4767a8eb4aae) UV Packing
    API for "Paint.Add\_Simple\_UVs"

## Calendar Week 41

October 18 - October 24

More graph isomorphism and packing research.

UV (With Campbell Barton)

  - [Commit](https://developer.blender.org/rB1edebb794b76326e06b527fd0a04eba34d51ab7c)
    Support snapping on non-uniform grids in UV editor.
  - Update [documentation](https://developer.blender.org/rBM9642)
    [+fix](https://developer.blender.org/rBM9643)
  - UV Copy'n'Paste
    [work-in-progress](https://developer.blender.org/D16278)
  - Simplify [UV Packing
    API](https://developer.blender.org/rBe3075f3cf7ce2fae086f3cd09867e4f0455e4115)
    (Fixes margin calculation options for UV.unwrap)

## Calendar Week 42

October 25 - October 31

Graph Isomorphism for UV Copy+Paste. (Turns out was harder than I
thought.)

[work-in-progress](https://developer.blender.org/D16278)

## Calendar Week 43

November 1 - November 7

More UV Packing research.

  - Cleanup: fix translation for rare UV unwrapping with unit scale
    [commit](https://developer.blender.org/rBc3e4fa74044a)
  - Improve precision during UV Packing.
    [Differential](https://developer.blender.org/D16362)

## Calendar Week 44

November 8 - November 14

  - UV Copy and paste
    [commit](https://developer.blender.org/rB721fc9c1c950)
    [commit](https://developer.blender.org/rBf04f9cc3d021)

## Calendar Week 45

November 15 - November 21

  - Debug unwrapping on quads with [reflex
    angles](https://developer.blender.org/T84078)
  - Cleanup asserts in [UV
    Unwrapper](https://developer.blender.org/rBda82d46a5ae6)
  - Show UV Outline in Texture Paint Mode
    [commit](https://developer.blender.org/rB4401c93e452f)
  - Report problems with n-gons with shared vertices on [UV
    unwrapper](https://developer.blender.org/D16521)
  - Investigate [Grid UV Unwrap](https://developer.blender.org/T78402)
  - Updating docs for UV in Blender 3.4
    [work-in-progress](https://developer.blender.org/D16568)

## Calendar Week 46

November 22 - November 28

  - Cleanup UDIM parameters when packing
    [Differential](https://developer.blender.org/D16599)
  - Refactor fix for n-gon problems with shared vertices
    [1](https://developer.blender.org/D16521)
  - Fix unreported, UV Unwrap selection was splitting selection
    [commit](https://developer.blender.org/rB0ce18561bc82)
  - Update UV documentation [Pending
    Differential](https://developer.blender.org/D16568)

## Calendar Week 47

November 29 - December 5

  - Support constrain-to-bounds for the UV Shear operator
    [Commit](https://developer.blender.org/rB3d1594417b0e)
  - Cleanup UV Parametrizer in prep for
    [T78101](https://developer.blender.org/T78101)
    [Commit](https://developer.blender.org/rBb99cdf747224)
    [Commit](https://developer.blender.org/rB5ce72bba7e93)
    [Commit](https://developer.blender.org/rBd602d4f15f5c)
    [Commit](https://developer.blender.org/rBae081b2de1d4)
  - More updating UV documentation [Pending
    Differential](https://developer.blender.org/D16568)
  - Investigate UV Packing performance on
    [T102843](https://developer.blender.org/T102843)
  - Improve performance for candidate [Improve uv unwrapping with n-gons
    and shared vertices](https://developer.blender.org/D16521)

On a more "feeling the vibe" note:

  - Anecdotally, increasing numbers of UV issues are being reported in
    Maniphest. The reported issues are more workflow related and it
    feels like they issues are being found by users using the tools for
    their intended purposes. =\> It feels like the community has noticed
    that when UV issues are reported, they're being actively triaged and
    fixed in a timely manner. Big Success\!

## Calendar Week 48

December 6 - December 12

  - Fix [T102543](https://developer.blender.org/T102543): Broken UV
    unwrap for n-gons with 3 shared vertices
    [commit](https://developer.blender.org/rB644afda7eb6c)
  - Fix [T84078](https://developer.blender.org/T84078): Improve UV
    unwrapping for quads with an internal reflex angle
    [commit](https://developer.blender.org/rBf450d39ada1f)
  - Debug [T102923](https://developer.blender.org/T102923): UV Editor
    Constrain-To-Bounds problem on unusual input file
  - Cleanup: Simplify udim parameters when uv packing
    [commit](https://developer.blender.org/rB0e90896cba37)
  - Improve documentation
    [commit](https://developer.blender.org/rBM9782)
    [commit](https://developer.blender.org/rBM9783)
    [commit](https://developer.blender.org/rBM9784)
    [commit](https://developer.blender.org/rBM9785)
    [commit](https://developer.blender.org/rBM9786)
  - WIP [UV Grid Unwrap](https://developer.blender.org/T78402)

## Calendar Week 49

December 13 - December 19

  - More WIP [UV Grid Unwrap](https://developer.blender.org/T78402)
  - Fix [T103237](https://developer.blender.org/T103237) Prevent UV
    Unwrap from packing hidden UV islands
    [commit](https://developer.blender.org/rB3dcd9992676aba2ff7d15872a9f355fdfb626566)
  - Fix [T102923](https://developer.blender.org/T102923) Strange UV
    Editor Constrain-To-Bounds behavior on an unusual blender file.
    [commit](https://developer.blender.org/rB0079460dc79a6c4798ec41e86550d8f0882ed313)

Plus refactoring UV Packer as a pre-requisite for
[T102843](https://developer.blender.org/T102843) (very slow packing with
large number of islands) and
[T68889](https://developer.blender.org/T68889) UV Packing with convex
hull instead of AABB. (WIP)

## Calendar Week 50

December 20 - December 26

  - More WIP [UV Grid Unwrap](https://developer.blender.org/T78402)
  - More refactoring UV Packer as a pre-requisite for
    [T102843](https://developer.blender.org/T102843) etc

## Calendar Week 51

December 27 - January 2

  - More WIP [UV Grid Unwrap](https://developer.blender.org/T78402)
  - [T103469](https://developer.blender.org/T103469) Improve UV Cylinder
    projection and UV Sphere projection.
    [Differential](https://developer.blender.org/D16869)
  - Improve 3x3 and 4x4 matrix multiply
    [Differential](https://developer.blender.org/D16876)
