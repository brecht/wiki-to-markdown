# AntiFeatures

This page lists features which have been suggested that we think
shouldn't be included in Blender.

## Python Modifiers / Compositing / Sequence Effects

Python is not well suited for fast interactive operations on large data
sets such as pixels or vertices.

Even in cases where computation can be optimized, Python has a global
interpreter lock (GIL), making any operations which use it single
threaded.

Using LLVM may be an option (as we already have for OSL), this would be
long term project which needs to be carefully integrated into Blender's
architecture.

## Pre Selection Highlighting

Selection of faces/edges/vertices/bones/etc in Blender is not “click and
guess” but IMHO one of the best working features of Blender - in which
we beat other programs still. In 3d views you just don’t have singular
selection possible (small elements, overlapping elements, etc). For that
reason Blender has a heuristics that includes the distance to mouse, and
cycles based on this distance (including on Z depth). If this doesn’t
work well, I would call that a bug to solve - and have developers make
it more predictable on single/2nd clicks…

Next to that, one of the key design concepts in Blender is to follow the
non-modal “Select -\> Operate” paradigm. Selection is the state,
operations the user’s toolbox that should be fast accessible. In other
programs (like Maya) they use the “Operator -\> Select” approach; that
means you first set the tool (translate) and then apply that on
selections. In such a modal UI a preselect is more understandable to
use.

See [this
link](https://blenderartists.org/t/gsoc-user-interface-tasklist/542066/581?u=ideasman42)
for original comment.

## Support for Other Scripting Languages

Supporting multiple languages is not something we're currently
interested in.

Embedding a language runtime has a significant maintenance overhead, so
there would need to be very compelling reasons (besides developer
preference) to add support for other languages or to move away from
Python.

## Integrating Native Widget Toolkits

Blender should run on different platforms, but we currently prefer to
use our own Blender based widgets and tools like the file-selector /
file-explorer (which is extended to support library linking, previewing
Blend files, and can be used as non-overlapping editor).

While integrating native widget elements has some advantages, these
widget elements are not portable and adding them locks us into an
operating system's API.

Note that Blender has some low level operating system integration
(through the GHOST library) which takes care of basic window management
and event handling, however this is not the same as integrating buttons,
file menus, dialog boxes .. etc.

A lot more could be said about why we prefer having our own widget
system. What it comes down to is that other toolkits are written to work
with a wide range of applications, while Blender has very specific,
often unusual needs (in UI design and the development process). We don't
expect many benefits from using them (but many problems) and appreciate
that we have our own solution specifically for Blender's needs.

## C/C++ Plugin API

At the moment we are not planning to accept binary plugins in Blender
(which implies a C/C++ API).

*Note: the pros and cons for this are quite involved and not covered in
detail here yet.*
