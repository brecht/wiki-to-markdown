  - [Release Notes](../Release_Notes/index.md)
  - [Frequently Asked Questions (FAQ)](FAQ.md)
  - [Developers: Ask Us Anything
    (Q\&A)](AskUsAnything.md)
  - [Anti-Features](AntiFeatures.md)
  - [UI Paradigms](UIParadigms.md)
  - [Project Policy](ProjectPolicy.md)
