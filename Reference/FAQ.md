# Blender Development FAQ

### What language is Blender written in?

Short answer: C/C++/Python

  - C - for Blender's \[startup/main-loop/shutdown\] and most internal
    functionality, tools, drawing, GUI and editors.
  - C++ - physics, audio, game-engine and Cycles render engine.
  - Python - The interface definitions, addons, most format
    import/export and some tools.

### Why C/C++/Python?

Part of it is historic, part of it is convention and choice of the
current developers.

Early on when Blender was developed, (1990's), C was very common for
developing graphics applications (C++ compilers were expensive, where C
compilers were free).

Python was selected because of its great community.

Nowadays Blender's core is still in C, some libraries are in C++, and
macOS integration even uses Objective-C, Scripting is done in Python.

Currently this works fairly well and we don't have plans to rewrite
large parts of Blender in some other languages.

### What Editors/IDE's do Blender developers use?

These IDE's are known to work well for Blender development where CMake
is used to create project files.

  - XCode on macOS
  - MSVC 2017/2019 on Windows
  - Eclipse on any
  - QtCreator on any
  - Netbeans on any

Some developers use editors such as vim, emacs, geany, notepad++ and
scite.

Note that its common to use a different editor/ide for C/C++ and Python
code.

### Where is the right place to request features in Blender?

There are so many possible features to add to Blender that its difficult
to manage them usefully so we don't accept feature requests on our
tracker as some projects do.

If the feature requests are small, like an option to an existing tool
you could try to contact the developer of that tool, or at least the
maintainer.  
See [Module Owners](Template:ModulesOwners)

If you're suggestion is a larger change, try make this a suggestion on
[Right-Click Select](https://blender.community/c/rightclickselect) to
work out a good proposal. Once you are confident you have a good
proposal you can either contact the respective [Module
Owner](Template:ModulesOwners), or start a thread over at
[Devtalk](https://devtalk.blender.org). In the latter case you can
@-mention [\`jesterking\`](../User/JesterKing/index.md) (Nathan Letwory)
to get started.

### Why did my Blender compile fail?

There may be many reason why it failed, but here are some common causes.
Also remember to look for the first error that is printed, as this is
usually the root cause, further errors may only be a result of this.

##### File not found or Unresolved Symbols

This usually means a library was not installed, not configured in the
build system, or the wrong version of the library was used. Also make
sure to download the lib/ folder on Window and Mac, and to install the
needed development packages on windows. It's also possible to disable
certain libraries when building Blender to avoid the issue. See the
[Building Blender](../Building_Blender/index.md) documentation for
details on dependencies.

##### DNA compile errors

Usually this means you added something to a struct in a header file and
forgot to adjust the amount of padding in the struct. Blender requires
that the structs be multiples of 8, see the [SDNA
Notes](Source/Architecture/SDNA_Notes) for more detail. So if
you add a short, and there is a char pad\[1\]; you change it to char
pad\[7\], to keep it a multiple of eight. Of course if you forget the
rules just keep adding a char pad till it compiles without errors :)
(which is what you will do if it compiles in your 32 bit machine and
others say it is failing in their 64 bit ones... save some time and read
the rules instead, alignment matters too, no matter how much padding you
add).

##### RNA compile errors

This usually means the wrong DNA struct type or property name was
specified.

##### Missing Interface Buttons and Menus

Usually this means you added something to the user interface python code
and made a mistake. Check the console for errors, often this is because
of mixing tabs and spaces. All python scripts in Blender uses spaces for
indentation.

##### Menu or checkbox defaults to the wrong value

Make sure you added a default value in the function that creates the
struct, and that you create a version patch in do\_versions in
readfile.c to initialize values on existing structs.

### Why did my Blender compile with warnings?

While Blender can compile warning-free on some platforms/configurations,
some warnings persist and can't simply be "fixed" or supressed.

If you see warnings while compiling, you can assume Blender developers
are aware of them and they're almost certainly *false-positves*, or at
least not causing real-world problems.

(This goes for static analysis tools too).

However this doesn't mean you should ignore warnings completely, For
code you write, check warnings carefully and resolve them where
appropriate (fix/supress).

### Whats the situation with automated testing?

[Testing in Blender](../Tools/Tests/Setup.md) is a work in
progress, and while we aren't anywhere near full coverage, we have tests
for:

  - Python: some basic tests exist already in: **source/tests/**.
  - GTest: tests for C/C++ code.
  - CTest (from CMake), can be used to call the tests above.

See also the project page: [Automated
Testing](https://projects.blender.org/blender/blender/projects/11)

### How does Blender deal with security?

Blender does not attempt to achieve the same level of security as many
other applications (web browsers for example),

The ability to have .blend files that execute bundled Python scripts
does pose a security risk if you don't know who created the file.

The **Trusted Source** option has been added to the file so you can load
a .blend file without running scripts as a precaution to simple attacks.

However this is no protection against more advanced exploits such as
hand crafting a .blend file which uses buffer overflows to run malicious
code.

For more discussion on this topic:

  - <http://lists.blender.org/pipermail/bf-committers/2013-June/040502.html>
    2013, Jun 4
  - <http://lists.blender.org/pipermail/bf-committers/2010-April/027216.html>
    2010, Apr 28

## Source Code FAQ

### How do I build Blender?

Follow the excellent instructions on
[Building\_Blender](../Building_Blender/index.md).

### What documentation is there?

[Source](../Source/index.md) has a bunch of documentation on the concepts
behind Blender's C code. It is not easy for a newcomer to understand,
there is a lot to read, and it is not completely up to date. But you
should keep coming back to it - the more you understand pieces of
Blender, the more all of this will make sense and be helpful.

### Where does Blender start and what happens next?

The file `source/creator/creator.c` has the `main()` routine in it.

After parsing args, a bunch of initialization, and reading a *.blend
file*, `main()` passes control to `WM_main()` in
`source/blender/windowmanager/intern/wm.c`. That is an infinite loop
that just processes events and notifications and draws updates on the
screen. Events and notifications are things caused either by the user
doing something (e.g., mouse click) or the internal Blender systems
signaling to other parts of Blender that something has changed and that
means something has to be recalculated, redrawn, or something else.

### What is an overview of how the files and subsystems are organized?

Here is a [great picture](http://www.blender.org/bf/codelayout.jpg).

Most of the code you will care about is under the `source/blender/`
subdirectory, though some Python code you may want to look at is under
`release/scripts/`.

### Where is the UI code?

The UI layout is defined in Python but the drawing is done in C, and
some of the UI is defined in C too.  
The Python code is in

  - `release/scripts/modules/bl_ui/`. E.g., `space_view3d.py` in that
    directory is the code for the *3D View* editor type (space).

The C interface drawing code is in:

  - `source/blender/editors/interface/`

The glue code defining the API python uses to create interfaces

  - `source/blender/makesrna/intern/rna_ui_api.c`

The Python UI code mostly just lays out user-interaction widgets that
adjust properties of *operators*. Operators are Blender tools. At some
point you should read
[Source/Architecture/Operators](Source/Architecture/Operators).

### How are Operators connected to C code?

If you hover over a button like *Spin* in the user interface, it shows
you the Python name for the operator that will be invoked if you hit the
button - `bpy.ops.mesh.spin()`. This is implemented in C by an operator
named `MESH_OT_spin` (see the naming convention in
[Source/Architecture/Operators](Source/Architecture/Operators)).
If you look for that name, you'll find it as a function that fills in
various function pointers and flags in a `wmOperatorType` struct. The
important one is the *invoke* function, in this case
`spin_mesh_invoke()`. If you put a breakpoint on that you'll find that
it gets called soon after you push the *Spin* button.

The Python `bpy_operator.c` code knows how to go back and forth between
*module.func* and *MODULE\_OT\_func*.

### What is this DNA and RNA stuff?

The names come from the genetic nucleic acids and hint at their
function: DNA, as in genetics, is the *code* that lets you reproduce
something. In the case of Blender, this is essentially what is written
to and read from disk as a *.blend* file - it has all of the user
settings and mesh, object, scene, etc. data. RNA, as in genetics, is a
helper to transmit the DNA *code* into the rest of the internal Blender
system.

The `.h` files in `source/blender/makesdna/` define the structs, enums,
etc., for all the data that needs to be persisted to disk. These are
usually used in memory in the running system too. For example,
`DNA_mesh_types.h` defines the `Mesh` struct that has enough information
to store a user's mesh on disk. While running, there may be a `Mesh` in
memory, but there will likely be various derived mesh structures that
are more efficient to use during editing, say.

Another aspect of DNA is that it is also a system for helping
compatibility between different versions of Blender. The way this works
is that a coded description of the layout of each struct is also written
to and read from disk. This means that the layout does not have to stay
fixed. The C files in the `makesdna` directory are for generating and
interpreting the structure descriptions. See
[Source/Data\_Structures/DNAStructs](Source/Data_Structures/DNAStructs).

RNA is also something that is partially made by code generation, using
code in `source/blender/makesrna`. It is supposed to be a somewhat more
abstract and nicer interface to the DNA data. For example, where a DNA
struct might use a particular bit in a short for some flag, RNA would
expose that as a bool property. So far, this nicer *Data API* isn't used
too much in the C source code. Where it *is* heavily used is as the
interface to and from Python. Most of the Python access to Blender data
is generated automatically to use the RNA system. See
[Source/Architecture/RNA](../Source/Architecture/RNA.md) for more
information about RNA.

### Where are the icons?

The icons are stored in a file called `blender_icons.svg` is one large
SVG file with almost all of the button icons in a kind of matrix
arrangement in it. There is a shell file to convert that file into 16x16
and 32x32 per icon versions of the SVG file as data files (not user
editable), these are stored in `release/datafiles/blender_icons16`,
`release/datafiles/blender_icons32`.

The brush icons are separate PNG files in the `brushicons` subdirectory.
The `datafiles` directory also has the splash image and some TrueType
fonts.

All of these data files are turned into C files that declare their data
as char arrays at build time, so any modifications to these files will
be visible after rebuilding.

The code for initializing the icons is in
`source/blender/editors/interface/interface_icons.c`, in the
`init_internal_icons()` function. The order of the icons in the image
is, bottom to top, left to right, the order of the `DEF_ICON`
declarations in `source/blender/editors/include/UI_icons.h`.
