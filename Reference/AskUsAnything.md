# Developers: Ask Us Anything\!

Running Blender project there are so many things we're are asked on a
regular basis

*(how and why things are...) and why we don't do it different\!*

While talking to users directly is fine, we often end up repeating
ourselves.

So this page is an informal Q\&A on common user questions relating to
the Blender project.

*Of course developers don't always completely agree,  
this page can at least answer some common questions on generally
accepted practices in the Blender developer community.*

See: [BlenderArtists Thread to ask
questions](http://blenderartists.org/forum/showthread.php?370230-The-Developers-Invite-quot-Ask-us-Anything!-quot)

<table>
<tbody>
<tr class="odd">
<td><div class="note_title">
<p><strong>NOTE</strong></p>
</div>
<div class="note_content">
<p>Questions about specific features in development can't be properly handled on this page.<br />
Planning details change too often and don't fit well with general Q&amp;A.</p>
</div></td>
</tr>
</tbody>
</table>

''For more specific development FAQ see: [Developer
FAQ](FAQ.md)

-----

## Release Cycle

For details on our release cycle, see: [Release
Cycle](../Process/Release_Cycle.md)

-----

### Why not release Blender when it's ready instead of rushing them every few months?

In the years prior to 2.5x development we used this approach, and found
it had a number of problems...

**From the development side:**

  - With active developers maintaining many areas, some volunteers with
    other commitments - we couldn't always rely on specific tasks being
    finished in a fixed timeframe.
  - Since Blender's a very complex application, it became increasingly
    difficult to pick the perfect moment to release for \_all\_ areas.  
    *Consider the wide range of areas: rendering, shading languages,
    modifiers, API's, physics, game engine, file format support,
    painting, hardware such as tablet/3d-mouse support... the list goes
    on\!*
  - The long gaps between releases allowed code to be in an
    unfinished/broken state for periods of time.
  - With over 6 months between some releases, developers would try to
    push changes weeks before the release, even when they weren't quite
    ready...  
    *(otherwise it may not get into a release for another year)*.

**From the user side:**

  - When a bug was fixed, users didn't know how long it would be until
    that got into a stable release.  
    *(assuming it wasn't important enough for a bugfix release).*
  - Users wouldn't have access to features which were ready for
    release... because other areas were still in development.

-----

Instead of frantically rushing out releases, we've largely streamlined
the process, so it's not a determent to regular development.

To support periodic releases, we strive to keep our main branch
(*master*) always stable, developing features in branches, which are
merged into *master* when ready.

This is not to say our release process is perfect, but for now we're
satisfied with the current release cycle and aren't likely to change it
significantly.

-----

**Release-cycle proposal & mailing list discussion:**

Mailing list discussions on this topic *(2011, before migration to
GIT).*

  - [Release Cycle
    Proposal](http://lists.blender.org/pipermail/bf-committers/2011-August/033137.html)
    ([thread view](http://markmail.org/thread/w6hzds2t63r74pkz))
  - [Release Cycle, follow up
    discussion](http://lists.blender.org/pipermail/bf-committers/2011-August/033078.html)
    ([thread view](http://markmail.org/message/nbrrthduqeyzmwf2))

\--[Ideasman42](../User/Ideasman42/index.md) 21:55, 5 May 2015 (UTC)

-----

### Is the release cycle completely frozen to focus exclusively on solving bugs and enhance existing features?

No, we don't have a period in the release cycle specifically for fixes
and enhancements.

Before release (2-3 weeks) we only accept bug fixes, however - we are
rather strict about the kinds of bug-fixes we accept, so it's not a good
time to work on more general improvements.

-----

### How do I find whats been added to daily builds?

**A:** Work in progress release notes are maintained at the same
location as regular release notes, *(current version +1)*.

See top level: [ Release Notes](../Release_Notes/index.md)
page.

## User Community

-----

### How can users help with Blender development

**Q:** What are some things users can do to help with Blender
development?

**A:** There are various ways users can help out.

  - **Report bugs:**  
    If you run into problems, even minor glitches, provide good quality
    bug reports.
  - **Test daily builds:**  
    Try run daily builds, reporting any issues you find in Blender's bug
    tracker is a great start.  
    It's worth checking the development [release
    notes](../Release_Notes/index.md) *(see: the link to the
    current development version)* and test newly added improvements. Try
    to stress test too, check on corner cases or use cases that might
    have been overlooked.  
    Or if you're really keen, check the [commit
    logs](https://projects.blender.org/blender/blender/commits/branch/main)
    *though this does give a lot of detailed information to check over.*
  - **Check new bug reports:** *(also called bug 'triage')*  
    Even testing to see if you can redo a report can help,  
    see the 'Needs triage' and 'Incomplete' categories in Blender's [bug
    tracker](https://projects.blender.org/blender/blender/issues).  
    Sometimes developers don't have access to the systems needed to redo
    a bug, just confirming a bug or providing helpful details can
    contribute significantly to a fix.
  - **Check new patches:**  
    You don't have to be a developer or write code to get Blender
    [building from source](../Building_Blender/index.md), though you
    *will* need to learn a bit about some development tools.  
    Once you have a build environment set up you can test patches which
    are still being reviewed. It's often useful for developers to get
    feedback on their patches from a users perspective.
  - **Provide feedback:**  
    Once you have a login to `projects.blender.org`, you can comment on
    commits and design tasks. If you're experienced in a field and see
    room for improvement, feedback & insights can be helpful.  
    *Note, for this to work well there needs to be developers actively
    working on an area (or have time to investigate and respond to your
    feedback), Otherwise you may spend a lot of time to give feedback
    that can't be acted on, for this reason it's often best to focus on
    an area currently in development, more is written about giving
    feedback in other Q\&A's here.*
  - **Improve Release Notes:**  
    Provide helpful example images for [release
    notes](../Release_Notes/index.md), *you will have to get a
    Wiki login first.*
  - **Improve/Write documentation:**  
    There are still many out-standing documentation TODO's, see the
    [documentation
    project](https://projects.blender.org/blender/documentation).  
    This is an area that often lags behind, and we can definitely use
    help here.

*This isn't a comprehensive list, there are other areas that can use
help but these depend on more specific skill sets (improving/adding
automated tests for example).*

-----

### Why not have a place for users to make requests?

**Q:** I am interested to know what is the best way for community to
voice their opinion on features or general development? (instead of
littering the forums) For instance Autodesk has a prioritized feature
list that is voted up by users which seems very efficient.

**A:** In principle, something like this could work for us (the topic
has come up many times and it's been discussed amongst developers), we
agree the ability to manage user feedback can work well. The issue is,
running such a site successfully isn't 'free' - There has already been
an effort to do this ([<http://www.blenderstorm.org> (web
archive)](http://web.archive.org/web/20140624120237/http://www.blenderstorm.org)),
which is an example of what happens when such a project is attempted
without being moderated/managed.

Getting feedback is not typically a bottleneck for development, we have
so much feedback already from existing communication channels, that
employing a small group of people to manage it *(moderate, reply,
close-duplicate-suggestions, maintain website)*, takes time away from
other areas. Some of this work definitely had to be done by experienced
developers, who usually have deeper insight into why things are the way
they are.

To put it differently: It would be great to have this, but we are
concerned that we wouldn't be able to manage such a task successfully
with the currently available resources.

**See also:** [Best Place to put Feature
Requests?](http://blender.stackexchange.com/questions/1190)

**Update:** Since this answer was written a new community run site has
has started to handle feature requests
[rightclickselect.com](http://rightclickselect.com), the site is
currently in development.

-----

### How can users provide feedback to developers?

**A:** The user/developer ratio makes it difficult to respond to all
feedback, however there are some ways you can communicate with
developers.

  - Use the [bug
    tracker](https://projects.blender.org/blender/blender/issues) if
    it's an error in the software.
  - Use the [\#blender-coders](../Communication/Contact/Chat.md)
    chat room on for more informal communication  
    *(though this can be hit-and-miss if you need to talk to an
    individual developer).*

## Developer Community

-----

### Whats the best way to become a Blender developer?

**A:** Unfortunately there isn't really a good one-size-fits-all answer
to this. It depends so much on your own interests, motivation and
experience level *(also which areas of the software most need of
attention)*.

Developers often give different answers, to this question when asked in
chat too.

**So to stick to some basics:**

  - Get Blender building from source.
  - Become familiar with a single area of the code you find interesting.
  - Start trying to make small changes & improvements.

For general info see: [New Developer
Info](../Developer_Intro/Overview.md)

-----

Links to more opinionated advice *(other devs feel free to link your own
here)*

  - [ Reply To: 'How to get into
    development?'](User:Ideasman42/Reply_Id_Like_To_Develop_Blender)
    --[Ideasman42](../User/Ideasman42/index.md) 21:55, 6 May 2015 (UTC)

-----

### Are there plans to improve docs for new developers?

**Q:** *"I'm thinking of things like overviews/introductions of how
different areas are structured etc (e.g. modifiers have the modifier
implementation file + DNA modifer data etc, you need to make sure you
specify how to write the data in the readfile.c /writefile.c files
etc)."*

**A:** Currently no short term plans, however we know our developer docs
need work, we do keep basics up to date such as building, developer
environments... but as for documenting big picture stuff, this is mostly
older content that needs updating.

As for documenting how to add specific features, while we could do this,
it's not clear if we have the time to maintain docs at this level. (It's
been done in the past but the docs tend to get outdated).

You can always read previous commits of features that were added, but
this isn't always ideal since API's may have changed since.

Another possibility is to have 'template-patches', which we provide that
add common functionality.  
(unofficial project, started but didn't get so far
<https://github.com/ideasman42/blender_patch_templates> )

\--[Ideasman42](../User/Ideasman42/index.md) 10:09, 6 May 2015 (UTC)

## Organization

-----

### Is there a list of currently employed Blender developers?

**A:** not exactly, there isn't a full list of all the currently
employed devs.

However reading over the developer fund you can get an idea of who's
working on Blender:  
<https://www.blender.org/foundation/development-fund/>

Also, developers who do work of Blender maintain a weekly log of their
work.

-----

### Why can't we donate money for specific features?

**A:** While individual developers have accepted donations/payment for
their developments before, this is not something any organizations have
been interested to do this so far.

The main reason is that it's quite risky and complicated to develop very
large features *(the kinds that users may be interested to fund)* with a
high enough chance of success.

Sometimes projects are completed in a branch but eventually abandoned
because the methods used don't work well and simply refining the
approach won't give us something usable. If such projects are part
feature-specific funding, this puts us in an awkward situation.

Along with this risk, it means developers end up being locked into a
plan which we can't modify without possibly upsetting some of the
supporters. *(even when it makes sense to revise plans)*.

All that said, feature specific crowd funding has been shown to work,
and individual developers are free to use online services to accept
donations.

-----

*Note: pros and cons of different funding models is a big topic, this
answer is intentionally a short summary.*

\--[Ideasman42](../User/Ideasman42/index.md) 17:26, 6 May 2015 (UTC)

-----

### Why is patch review taken so serious for a community driven, open project?

**Q:** The barrier for getting into Blender dev is already really high,
why make this even worse by requiring intense review even though it
should be possible for everyone to participate in such an open project?

**A:** While we're all interested in having fun in the development of
the Blender project, we also want to provide a product with a high
quality. Carefully reviewing submitted patches is one of the best
approaches to archive this. Actually, the review progress in Blender can
be split up into 3 different parts:

  - **Feature Review:** This means reviewing the feature itself. Is this
    a feature that fits into Blender at all? Is there a tangible and
    better way to reach the goal of the feature? Does the feature fit
    into the current roadmap (it doesn't make much sense to add features
    to parts of Blender that are planned to be rewritten). *The decision
    over this is up to the module owner of this area*
  - **UI review:** Ideally, the
    \[<https://projects.blender.org/blender/blender/wiki/Module:-User-Interface>|
    UI-team\] would review the UI of each patch before any in-depth
    code-review happens. However, since the UI-team has limited
    recourses, this is currently more a part of the general code-review.
    Nevertheless, checking with them is welcome\!
  - **Code Review:** Usually the most important part of the progress.
    For a project of the size of Blender, it is \*crucial\* to ensure
    added code is good (More info about what good code means and why it
    is so important,
    \[<http://wiki.blender.org/index.php/Dev:Doc/AskUsAnything#Why_are_patches.2Ffeatures_rejected_just_because_.22code_isn.27t_good_enough.22.3F>|
    here\]).

No doubt, this can be a tedious progress, especially for newer devs it
is an additional hurdle and for reviewers it is additional work, but it
also has been proven to help. If we really want to help prospective
developers to get started, we should improve our docs, make code more
readable and improve communication, instead of lowering quality
standards needed to get a change accepted.

-----

## Design Choices

-----

### Feature X in Application Y is Perfect\! Why can't you add it without  
*"re-inventing the wheel"?*

**Short Answer:** Because Blender is a different application\!

**Long Answer:** Blender has its own work-flows, follows different
paradigms.

For any functionality of course we need to consider the options
available - including any existing conventions.  
However we also need to make sure this fits well with Blender which has
its own conventions. It's important to think for ourselves, questioning
the status quo since it may not be a good option for us.  
We can aim to do better too\!

-----

*This is not to suggest when Blender is different it's always 'better'
or that developers always make the best design, only giving some
rationale why simply cloning behavior from other applications isn't
something you should expect developers to do.*

*This topic is subjective, user experiences vary widely\!*

-----

### Why not use an existing GUI toolkit?

**Q:** There are cross platform GUI-toolkits such as Qt, GTK, wxWidgets,
Fox, FLTK, TK ... Why not use one of these?

**A:** The reason is both historic and practical: to move over to a
widget set at this point would be a huge project and it's not even clear
that doing so would be *better* for us. With complete control over our
own toolkit, we have been able to support features not existing in other
toolkits, *(window splitting scheme, grabbing the mouse while operating
sliders, widget control via the 3D-mouse, executing commands on widget
mouse hover, [ multi-number button
editing](Dev:Ref/Release_Notes/2.70/UI#Multi-Number_Button_Editing),
our own data API's integration with the interface, browsing inside
.blend files with the file browser, ...)*.

Another aspect to this is that we don't depend heavily on the
GUI-Toolkit's release schedule. We can fix bugs in the interface code
without depending on a third party toolkit team and its release plans.  
Since Blender was written, widget-toolkits have come and gone.

Other advantages of using OpenGL include remaining lightweight,
portable, uniform GUI across all platforms with the flexibility to
change the GUI behavior as needed.

Of course this adds a maintenance burden, when we want to implement some
features existing in other UI's - we need to do it ourselves.

There are pros & cons obviously it's possible to create 3D software
without writing your own graphical-interface system, however at this
point switching to a GUI-toolkit is a big project, so there would have
to be a very compelling reason to do so.

-----

### Why are patches/features rejected just because "code isn't good enough"?

**Q:** A great patch/feature was rejected because "code wasn't good
enough" or because it "doesn't fit into some code design", even though
many users confirmed that it works nicely. Why is that? What does that
mean exactly?

**A:** Unfortunately, it happens more and more that patches are rejected
(or large changes are requested) because of issues with code, even if
they seem to work exactly as wanted.

'Issues with code' or even 'bad code' doesn't mean that the code doesn't
look nice because of formatting/code style, that the code is in the
wrong place or that we don't like the feature itself.  
In most cases, code that isn't good enough to be accepted is
<i>potentially dangerous</i>. That means, it may break existing
features, it contains known but hidden issues, or it makes future
development difficult.  
In fact, all three reasons happen more often than people think, even
after extensive user testing. Further, the latest one is probably the
most important and most common one. In the past, much of Blender's code
was written without keeping possible future requirements in mind. Today,
Blender's development suffers a lot from such 'bad' code (that's why so
many rewrites/refactors need to be done lately). <i>One can write code
that works, but that doesn't mean that it will also work for the
future.</i>  
Code may also be rejected if it's known that there are better ways
(faster algorithms, better papers, more clean methods, etc) to get a
task done.

Of course, exact reasons vary widely for each case.

-----

<i>For all sides, users, patch authors \*and\* reviewers, this is an at
least unpleasant situation. If this happens, we try to communicate exact
reasons for rejecting well. Finding a proper solution is in the interest
of everyone, so we try to help with that, too.</i>

**Recent Examples:** Fracture modifier
([1](http://developer.blender.org/D817)), Cycles adaptive sampling
([2](http://developer.blender.org/D808)), Cycles shadow-catcher, Alembic
(rejected by Author himself), etc.

\--[Severin](User:Julianeisel) 15:56, 20 August 2015 (UTC)

-----

### Why not support languages besides Python?

**A:** Currently Python is serving our needs quite well as a scripting
language.

Python has its limitations, other language/runtime combinations
undoubtedly have their strengths. But Blender is primarily for artists -
not an SDK/API to support developers preferred technologies.

Another factor is the effort to support/maintain embedding multiple
languages *(supporting Python is already giving us enough work / bugs to
fix)*.

-----

That said, the option is always open to embed other language/runtimes,
for now though it doesn't seem worthwhile.

See more in-depth discussion here:

  - 2009 discussion:
    <http://lists.blender.org/pipermail/bf-committers/2009-February/022669.html>
    ([thread view](http://blender.markmail.org/thread/cjj5u5zivbofbfdo))
  - 2013 discussion (specifically about Lua):
    <http://lists.blender.org/pipermail/bf-gamedev/2013-September/000031.html>
