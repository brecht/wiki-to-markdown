# Blender Compatibility

This page is to document known incompatibilities with software and
hardware.

<table>
<tbody>
<tr class="odd">
<td><div class="note_title">
<p><strong>NOTE</strong></p>
</div>
<div class="note_content">
<p>All items here must link to verified references (bug reports for example) or they will be removed.</p>
</div></td>
</tr>
</tbody>
</table>

Some applications which integrate themselves into your system can cause
problem’s with Blender.

Here are some known compatibility issues listed by platform.

## Linux

...

## Mac OS X

### Mouse Motion Jitters (SmoothMouse)

  - Problem  
    When grabbing an object or orbiting the view, cursor motion is
    jittery. [\#47224](http://developer.blender.org/T47224)

  - Solutions

:\* Uninstall SmoothMouse.

:\* Disable Continuous Grab.

## MS-Windows

### Blender Hangs on Window Duplication (Nahimic for MSI)

  - Problem  
    Accessing *Window -\> Duplicate Window*, hangs Blender, using 100%
    of one core. [\#47224](http://developer.blender.org/T47224)
  - Solution  
    Uninstall Nahimic for MSI
