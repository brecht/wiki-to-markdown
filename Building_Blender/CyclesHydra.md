# Building Cycles as a Hydra Render Delegate

This requires using the Cycles standalone repository.

For build instructions refer to
[BUILDING.md](https://projects.blender.org/blender/cycles/src/branch/main/BUILDING.md)
in the repository.
