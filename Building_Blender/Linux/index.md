# Building Blender on Linux

## Distributions

Instructions to build Blender, based on your distro of choice.

  - [Arch](Arch.md)
  - [Fedora](Fedora.md)
  - [Gentoo](Gentoo.md)
  - [OpenSUSE](OpenSUSE.md)
  - [Ubuntu](Ubuntu.md)

-----

  - [Generic Distro](Generic_Distro.md)

## Release Environment

Setup of official Linux build environment for releases.

  - [Rocky
    8](../Other/Rocky8ReleaseEnvironment.md) for
    Blender 3.5 and newer.
  - [CentOS
    7](Building_Blender/Other/CentOS7ReleaseEnvironment) for
    Blender 3.4 and older.
