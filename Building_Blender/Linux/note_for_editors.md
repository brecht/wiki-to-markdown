## Note for editors

It might seem that editing a certain distro's page won't affect other
distros pages, but this is not the case: the pages above are based on:

  - [Intro](Template:Building_Blender/Linux/intro)
  - [Dependencies](Template:Building_Blender/Linux/dependencies)
  - [CMake](Template:Building_Blender/Linux/cmake)
  - [Troubleshooting](Template:Building_Blender/Linux/Troubleshooting)
