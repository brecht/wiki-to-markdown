# Building Cycles with GPU Binaries

For GPU accelerated rendering with Cycles on various graphics cards,
additional build configuration is needed.

## NVIDIA

### CUDA

Install [CUDA Toolkit 11
or 12](https://developer.nvidia.com/cuda-downloads). Newer versions may
work but are not tested as well, older versions will not work.

Change the [CMake
configuration](index.md#Customization) to enable
building CUDA binaries:

``` text
WITH_CYCLES_CUDA_BINARIES=ON
```

If you will be using the build only on your own computer, you can
compile just the kernel needed for your graphics card, to speed up
building. For that you need to edit \`CYCLES\_CUDA\_BINARIES\_ARCH\` in
the CMake configuration, and leave only the [architecture needed for
your GPU](https://developer.nvidia.com/cuda-gpus).

### OptiX

For rendering with OptiX to take advantage of hardware acceleration in
RTX cards, you need to install the [OptiX
SDK 7.3](https://developer.nvidia.com/designworks/optix/download). This
is supported on Windows and Linux currently.

CUDA must first be set up as described above.

Change the [CMake
configuration](index.md#Customization) to enable
building with OptiX and point to the OptiX installation directory:

``` text
WITH_CYCLES_DEVICE_OPTIX=ON
OPTIX_ROOT_DIR=/path/to/NVIDIA-OptiX-SDK-7.x.x-xxx
```

### Debug Builds

[ASAN](../Tools/Debugging/ASAN_Address_Sanitizer.md) interferes
with CUDA availibility. Disable it to avoid CUDA errors on
initialization.

``` text
WITH_COMPILER_ASAN=OFF
```

## AMD

### Linux

Install the ROCm 5.x with the HIP compiler, use the most recent version
to get important bug fixes. [Instructions by
AMD](https://rocm.docs.amd.com/en/latest/deploy/linux/quick_start.html).
Some Linux distributions may also provide this as packages. Installing
drivers is not required for compiling, only for running.

Change the CMake configuration to enable building CUDA binaries:

``` text
WITH_CYCLES_HIP_BINARIES=ON
```

If you will be using the build only on your own computer, you can
compile just the kernel needed for your graphics card, to speed up
building. For that you need to edit \`CYCLES\_HIP\_BINARIES\_ARCH\` in
the CMake configuration, and leave only the [needed for your
GPU](https://llvm.org/docs/AMDGPUUsage.html#processors) (this list is
incomplete unfortunately, so it may not be trivial to find your GPU).

### Windows

Install the [HIP
SDK](https://www.amd.com/en/developer/rocm-hub/hip-sdk.html) for
windows. By default the installer creates the environment variable
\`HIP\_PATH\` which is used in cmake for locating the SDK location.

To enable building HIP kernels set:

``` text
WITH_CYCLES_HIP_BIANRIES=ON
```

To enable building HIPRT kernels (hardware accelerated ray tracing) set:

``` text
WITH_CYCLES_DEVICE_HIPRT=ON
```

## Intel

OneAPI support is available on Windows & Linux, for Intel Arc GPUs.
Building must be done using precompiled libraries, which includes the
compiler for OneAPI (there is currently no Intel provided SDK that works
with Blender).

Enable it in the build configuration as follows:

``` text
WITH_CYCLES_DEVICE_ONEAPI=ON
```

### Pre-compiled GPU kernels

It is possible to compile GPU kernels in an ahead-of-time manner as a
step of Blender build process. In order to do so the following options
should be enabled:

``` text
WITH_CYCLES_ONEAPI_BINARIES=ON
```

On Windows it is also required to download [Offline Compiler for
OpenCL](https://www.intel.com/content/www/us/en/developer/articles/tool/oneapi-standalone-components.html).
The path to it is either to be provided using \`OCLOC\_INSTALL\_DIR\`
CMake variable, or the ocloc is to be placed to
\`win64\_vc15/dpcpp/lib/ocloc\`.

## Apple

GPU rendering with Metal on Apple requires no extra build steps, the
kernels are compiled at runtime.
