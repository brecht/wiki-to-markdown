# Rocky 8 Release Build Environment

The official Blender builds are done on the Rocky 8 system. This makes
it possible to have a portable build which will work on [VFX reference
platform](https://vfxplatform.com/) compatible systems.

This page describes how the official setup is created. This setup allows
to compile Blender itself, as well as the Blender dependencies.

## Preparing Rocky 8 Environment

The release environment is based on the \`Minimal Install\` of the Rocky
8. This document does not cover the steps needed to get the OS
installed: it is rather straightforward and the default installation
will get to where the systems needs to be.

The
[linux\_rocky8\_setup.sh](https://projects.blender.org/blender/blender/src/branch/main/build_files/build_environment/linux/linux_rocky8_setup.sh)
script is then run as a root on the freshly installed system. It takes
care of installing all dependencies.

<table>
<tbody>
<tr class="odd">
<td><div class="note_title">
<p><strong>Note</strong></p>
</div>
<div class="note_content">
<p>Some system-wide changes are performed, so it is recommended to use a dedicated virtual machine for the release build environment.</p>
</div></td>
</tr>
</tbody>
</table>

Congratulations, the base system is now ready\!

Follow the [Building Cycles with GPU
Binaries](../GPU_Binaries.md) for the instructions
related to installing the GPU compute toolkits.

## Building Blender

Blender is built from sources using the precompiled Rocky 8 libraries.
Follow the Download Sources and Download Libraries sections from the
[Linux Quick Setup](../Linux/Generic_Distro.md).
Note that all packages have already been installed, so that step from
the Quick Setup should be skipped. Also make sure to get precompiled
libraries for Rocky 8.

The build is to be performed using gcc-11 toolchain. It is activated
using the \`scl\` command:

``` bash
scl enable gcc-toolset-11 bash
cd ~/blender-git/blender
make release
```
