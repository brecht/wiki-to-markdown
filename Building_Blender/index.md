# Building Blender

## Instructions

Steps to build Blender from scratch for each operating system.

  - on [Linux](Linux/index.md)
  - on [macOS](Mac.md)
  - on [Windows](Windows.md)

## Resolving Build Failures

Most building problems are not actually errors in Blender's source code,
although you can never fully rule out that possibility. See
[troubleshooting](Troubleshooting.md) page for
possible causes and where to get help.

## Build System

Blender uses the [CMake](http://cmake.org) build system.

### `make` wrapper

For quick setup, the `make` wrapper takes care of setting up CMake
automatically, and has quick commands for various tasks. It must be run
from the Blender repository root directory. The most important commands
are:

``` text
make help    # List all available commands.
make update  # Update source code, add-ons and libraries to latest versions.
make         # Build Blender.
make test    # Run automated tests.
```

### Output

By default, `make` outputs the build and project files to to
`../build_<platform>`. The Blender executable will be located in
`../build_<platform>/bin`.

To change this location, see `make help` or manually run `cmake`. Be
sure to generate the build files in a directory other than the source
directory.

### Customization

Many CMake options can be set to customize the build. See [Build
Options](Options.md) for how to configure them.

## Setup for Developers

For efficient development of the Blender C/C++ code, [install and set up
an Editor or IDE](../Developer_Intro/Environment/index.md) to navigate
and edit the source code.

For efficient debugging and faster builds, see [Build Options for
Developers](Options.md#Setup_For_Developers).

## Library Dependencies

Details on obtaining libraries are included in each platform's build
documentation. See
[dependencies](Dependencies.md) page for
details on how this system works and how to make portable builds.

## Compiler Versions

| Compiler               | Official Release Version | Minimum Supported Version |
| ---------------------- | ------------------------ | ------------------------- |
| Linux GCC:             | 11.2.1                   | 11.0.0                    |
| Linux Clang:           | \-                       | 8.0                       |
| macOS Xcode:           | 14.1                     | 11.0                      |
| Windows Visual Studio: | 2019 (16.11.26)          | 2019 (16.9.16)            |

## Other Build Configurations

  - [Building Blender as a Python
    Module](Other/BlenderAsPyModule.md)
  - [Building Cycles with GPU
    Binaries](GPU_Binaries.md)
