# Debugging blender python with Visual Studio

## Update 2023-01

Visual studio breaks this feature often, it's best to consider this
feature non functional and not sink any time into trying to get it to
work.

The page below is archived in its original state in case a working
version ships in a future Visual Studio version.

Relevant upstream ticket:

<https://github.com/microsoft/PTVS/issues/6712>

## Introduction

Visual Studio 2017 offers integrated debugging of python. This guide
will help you set it up for use with blender.

This guide assumes you already know how to build blender, if you have
not yet done so, please follow the
[Building\_Blender/Windows](../../Building_Blender/Windows.md) guide.

## Prerequisites

### Visual Studio 2017

Version 15.8.7 or later.

  - In the visual studio installer, enable the \`Python Development\`
    workload

` `![`../../images/Vs_python_installer.png`](../../images/Vs_python_installer.png
"../../images/Vs_python_installer.png")

### Visual Studio 2019

  - Enable the Python Development Workload (just like for VS2017)
  - Enable the "Python native development tools" individual component.
    This is not enabled by default with Python Development Workload, but
    is required for the "Python/Native Debugger" option to show up
    later.

## Project Creation

You should already have used \`make.bat\` to build blender before, in
this section we'll use it to generate a Visual Studio project with the
options enabled we need for debugging python.

Open up a command prompt and navigate to the blender source folder and
run <b>`make full 2017 x64 nobuild pydebug`</b>

``` Python numberLines
Microsoft Windows [Version 6.1.7601]
Copyright (c) 2009 Microsoft Corporation.  All rights reserved.

C:\Windows\system32>cd /d k:\BlenderGit\blender

k:\BlenderGit\blender>make 2017 x64 nobuild pydebug
```

<table>
<tbody>
<tr class="odd">
<td><div class="note_title">
<p><strong>Tip:</strong></p>
</div>
<div class="note_content">
<p>Here's what each of the additional parameters mean.</p>
<ul>
<li><b><code>2017</code></b> This explicitly select Visual Studio 2017 in case there are multiple versions of Visual Studio installed.</li>
</ul>
<ul>
<li><b><code>x64</code></b> This explicitly selects a 64 bit build of blender, while 32 bit builds are still supported (use `x86` for that) it is not recommended unless you really need a 32 bit build.</li>
</ul>
<ul>
<li><b><code>nobuild</code></b> This prevents `make.bat` from building blender for you, and will only generate a Visual Studio solution for you that you can later open with the IDE.</li>
</ul>
<ul>
<li><b><code>pydebug</code></b> This sets up the project for python debugging and will cause CMAKE to include all .py scripts from the release folder and all users scripts in sub-projects in the solution.</li>
</ul>
</div></td>
</tr>
</tbody>
</table>

After a few seconds, it should tell you where the project files have
been written.

``` Python numberLines
-- Configuring done
-- Generating done
-- Build files have been written to: K:/BlenderGit/build_windows_Full_x64_vc15_Release
```

## Opening the project in visual studio

Navigate to the location and double click <b>`blender.sln`</b>

## Selecting a configuration

You can build blender in both <b>`Debug`</b> and <b>`Release`</b>
configurations.

<table>
<tbody>
<tr class="odd">
<td><div class="note_title">
<p><strong>Tip:</strong></p>
</div>
<div class="note_content">
<p><b><code>Debug</code></b> is a specially optimized build for debugging code, this build of blender will be bigger and slower, but it will be easier to debug C/C++ code.</p>
<p><b><code>Release</code></b> is a highly optimized version of blender, it will be fast, but it will be problematic to diagnose issues in the C/C++ Code.</p>
</div></td>
</tr>
</tbody>
</table>

You can switch between the builds in the <b>`Solution Configuration`</b>
dropdown menu

![../../images/Vs\_configuration\_dropdown.png](../../images/Vs_configuration_dropdown.png
"../../images/Vs_configuration_dropdown.png")

<table>
<tbody>
<tr class="odd">
<td><div class="note_title">
<p><strong>Tip:</strong></p>
</div>
<div class="note_content">
<p>If you are only going to debug python code select the <b><code>Release</code></b> configuration, if you also need C/C++ debugging select <b><code>Debug</code></b></p>
</div></td>
</tr>
</tbody>
</table>

## Building Blender

First we have to build blender, however just building blender is not
enough, the output folder also needs to be populated with additional
dll's and startup scripts for blender to run properly, so instead of
just clicking build. Expand the <b>`CMakePredefinedTargets`</b> group ,
right click on the <b>`INSTALL`</b> project and select <b>`build`</b>

![../../images/Vs\_blender\_build.png](../../images/Vs_blender_build.png
"../../images/Vs_blender_build.png")

after a while the output window should show something along these lines

``` Python numberLines
========== Build: 149 succeeded, 0 failed, 0 up-to-date, 0 skipped ==========
```

## Selecting the Python Debugger

Select <b>`Python/Native Debugger`</b> in the debugger dropdown.

![../../images/Vs\_debugger\_selection.png](../../images/Vs_debugger_selection.png
"../../images/Vs_debugger_selection.png")

<table>
<tbody>
<tr class="odd">
<td><div class="note_title">
<p><strong>Tip:</strong></p>
</div>
<div class="note_content">
<p>If this option is missing, this is most likely due to the <b><code>Python Development</code></b> workload not being installed, see the Prerequisites section on how to install.</p>
</div></td>
</tr>
</tbody>
</table>

## Setting a breakpoint

In the solution explorer, expand
<b>`scripts/blender_python_system_scripts/addons/io_scene_obj`</b>,
double click on <b>`__init__.py`</b>, and put a breakpoint on the
register function by placing the cursor on the following line and
pressing <b>`F9`</b> or by clicking in the left margin of the line.

``` Python numberLines
def register():
    for cls in classes:
        bpy.utils.register_class(cls)

    bpy.types.TOPBAR_MT_file_import.append(menu_func_import)
    bpy.types.TOPBAR_MT_file_export.append(menu_func_export)
```

## Running blender

Start blender by either pressing the <b>`F5`</b> key or clicking the
play ![../../images/Vs\_play.png](../../images/Vs_play.png
"../../images/Vs_play.png") button.

Blender will now start and after a few moments your breakpoint should
hit.

Happy Debugging\!

![|left](../../images/Vs_blender_breakpoint.png "|left")
