# OpenGL Debugging with Bugle

Some OpenGL bugs can give totally useless backtraces in GDB. (crash
inside the GL driver).

Using bugle source - <http://www.opengl.org/sdk/tools/BuGLe/>

``` bash
scons prefix=/opt/bugle install
```

Copy doc/examples/filters into \~/.bugle/filters

``` bash
BUGLE_CHAIN=validate LD_PRELOAD=/opt/bugle/lib/libbugle.so:/opt/bugle/lib/libbugleutils.so ./blender.bin
```

Then instead of crashing it gives a useful message as to whats going
wrong, like this... \[NOTICE\] checks.error: illegal normal array caught
in glDrawArrays (unreadable memory); call will be ignored.

there are other options for debugging like showfps, trace, wireframe ...
see \~/.bugle/filters for options.

With the "crash" filter I managed to get a valid backtrace from gdb too.

``` bash
BUGLE_CHAIN=crash LD_PRELOAD=/opt/bugle/lib/libbugle.so:/opt/bugle/lib/libbugleutils.so gdb ./blender.bin
```

Thought It might be useful for people looking into VBO bugs for the
first time.
