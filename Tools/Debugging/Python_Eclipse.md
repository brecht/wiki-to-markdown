# Debugging Blender from Eclipse

This is a quick-start tutorial to get you up and running with Eclipse as
a debugging tool for Blender, and assumes that you are either familiar
with Eclipse and PyDev as well as Blender Python scripting, or that you
are prepared to do some research.

## Requirements

  - Eclipse
  - PyDev (Eclipse Python integration)

## Initial Test

Follow this list in order, to minimize any potential snags. '

  - If you're starting Eclipse for the first time, create a new PyDev
    project:  
    **File -\> New -\> Other**,  
    then select one of the **PyDev** project types from the list.
  - Activate the "Debug" perspective:  
    **Window -\> Perspectives -\> Open Perspective -\> Other**,  
    then select **Debug -\> Debug** perspective from the list.
  - Start the PyDev debug server:  
    **PyDev -\> Start Debug Server**
  - Create a new text block in Blender
  - Paste this (suitably edited) at the beginning of the script:

<!-- end list -->

``` python
PYDEV_SOURCE_DIR = "/usr/lib/eclipse/dropins/pydev/eclipse/plugins/org.python.pydev_4.5.4.201601292234/pysrc"

import sys

if PYDEV_SOURCE_DIR not in sys.path:
   sys.path.append(PYDEV_SOURCE_DIR)

import pydevd

pydevd.settrace()

bling = "the parrot has ceased to be"
print(bling)
```

  - Run the script in Blender - at this point Blender will hang (stop
    responding to input).
  - Switch to Eclipse.
  - The file you are debugging should be open, with the line it has
    stopped at highlighted

## How it Works

So how does all this magic work?

When the Debug Server is started in Eclipse, it sits and listen on port
5678 on the loopback interface for Blender to start sending it messages
via the network stack. When this happens, Eclipse grabs then and does
all the debugging by communicating back and forth with Blender.

The script actually runs in Blender, but the debugging takes place in
Eclipse, with Eclipse getting all the necessary information to do this
by sending and receiving messages from Blender.

So, Eclipse if fully equipped to do the debugging at its end. Blender,
on the other hand, needs a little help. First it needs to know where all
the PyDev debugging stuff lives, because this will tell Blender how to
communicate with Eclipse. You can find this path using the Python
Console in eclipse:

First start a Python console in Eclipse:

  - **Window -\> Show View -\> Console**
  - There is a new console icon in the console header with the tool-tip
    **Open Console**  
    Click on this and select **PyDev Console -\> Python Console** in the
    dialog box.
  - Type in
    ``` bash
    sys.path
    ```
    and press Return. A list of paths will be printed, the first one
    will be the pysrc path which Blender needs to know.

An example of the output:

``` python
>>> sys.path
Out[2]: 
['/usr/lib/eclipse/dropins/pydev/eclipse/plugins/org.python.pydev_4.5.4.201601292234/pysrc',
 '/usr/lib/python3.5/site-packages/mathutils-2.76-py3.5-linux-x86_64.egg',
 '/usr/lib/python3.5',
 '/usr/lib/python3.5/site-packages',
 '/usr/lib/python3.5/site-packages/IPython/extensions']
```

Now it’s a matter of adding this path to Pythons system path:

``` python
PYDEV_SOURCE_DIR = "/usr/lib/eclipse/dropins/pydev/eclipse/plugins/org.python.pydev_4.5.4.201601292234/pysrc"

import sys

if PYDEV_SOURCE_DIR not in sys.path:
   sys.path.append(PYDEV_SOURCE_DIR)
```

Now the system knows what to do when it gets this request:

``` python
import pydevd
```

It will scan `sys.path` for `pydevd` in the `PYDEV_SOURCE_DIR` and then
import it.

Now when the script calls:

``` python
pydevd.settrace()
```

it will use `pydevd` to use the magic of the loopback interface and send
and receive the messages to and from Eclipse to do its magic.

Thanks to sc3sc3 on [this
thread](http://blenderartists.org/forum/showthread.php?308260-Eclipse-for-debugging-python-in-Blender&highlight=)
at Blenderartists for his assistance.

## Tips

  - To break for the first time, you will need to call
    `pydevd.settrace()`, after this you can set breakpoints inside
    Eclipse,  
    however on each execution from Blender, `pydevd.settrace()` is
    needed as an entry point to connect to the debugger.
  - Make sure the PyDev remote debugger is started before firing up
    Blender
  - Automatic updates of PyDev creates a new folder for the PyDev python
    code among other things, so you will have to update your
    `PYDEV_SOURCE_DIR` path in your script

For more advanced debugging techniques and in-depth information on the
Blender/Python/Eclipse trinity read Witold Jaworski’s [excellent and
free
e-book](http://www.blendernation.com/2011/08/24/free-e-book-programming-add-ons-for-blender-2-5/).
