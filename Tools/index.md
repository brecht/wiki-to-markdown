# Development Tools

## Services

  - [Pull Requests](Pull_Requests.md): code contribution
    and review
  - [BuildBot](Tools/BuildBot): automated builds at
    [builder.blender.org](http://builder.blender.org)
  - [Git](Git.md): how to download, update and commit code
  - [Subversion](Subversion.md): how to check out prebuild
    libraries and tests
  - [tea](tea.md): command line tool for interacting with
    Gitea

There is an [issue
tracker](https://projects.blender.org/infrastructure/blender-projects-platform/issues)
for bugs and plans in development web services.

## Development Tools

  - [ClangFormat](ClangFormat.md): automatic code
    formatting following Blender style
  - [CMake for
    Developers](../Building_Blender/Options.md#Setup_For_Developers):
    CMake options for development
  - [distcc](distcc.md): distributed building
  - [Unity Builds](Unity_Builds.md): reduce compile times
  - [C/C++ Code Documentation](Doxygen.md): viewing and
    editing with doxygen

## Testing

  - [Setup](Tests/Setup.md): setup to run tests
  - [C/C++ Tests](Tests/GTest.md): GTest tests
  - [Python Tests](Tests/Python.md): Python testing
  - [Performance Tests](Tests/Performance.md):
    benchmarking

## Debugging

### C/C++

  - [GDB](Debugging/GDB.md): debugging on Unix system
  - [Address
    Sanitizer](Debugging/ASAN_Address_Sanitizer.md):
    debugging with GCC/Clang & ASAN
  - [Valgrind](Debugging/Valgrind.md): detecting memory
    errors (linux/osx only)
  - [BuGLe](Debugging/BuGLe.md): OpenGL debugging
  - [Py from C](Debugging/PyFromC.md): run Python scripts
    in C code (test changes without rebuilds).

### Python

  - [Eclipse PyDev](Debugging/Python_Eclipse.md): How to
    debug Python scripts running in Blender from the Eclipse IDE.
  - [Profiling](Debugging/Python_Profile.md): How to
    profile Python function calls.
  - [Tracing](Debugging/Python_Trace.md): How to log
    script execution.

## Guides

  - [Coding Tips](Tips_for_Coding_Blender.md): how to
    navigate the code and debug problems.
  - [Blender Dev Tools](Blender_Tools_Repo.md): how to
    setup Blenders optional developer tools repo.
  - [Git Bisect with Event
    Simulation](GitBisectWithEventSimulation.md):
    automatically find faulty commits
