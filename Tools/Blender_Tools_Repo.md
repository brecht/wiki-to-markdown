## Blender Tools

For developer tools we have a separate [Git
Repository](https://developer.blender.org/diffusion/BDT).

This is to be used to keep tools which are not needed for building
Blender but developers may find useful.

  - Utility scripts for running checks on code for style, spelling.
  - Utility scripts for code-base analysis, creating credits for eg.
  - Configurations for 3rd party applications, IDE's.
  - Any tools we develop specifically for working with Blender.

### Repository Overview

  - \`./check\_source/check\_spelling.py\`: spell checker (*Depends on
    **enchant** Python module*)
  - \`./config/ide\`: style configuration for QtCreator and Eclipse
  - \`./utils/make\_cursor\_gui.py\`: simple paint program to create
    pixmap cursors for \`wm\_cursors.c\`
  - \`./utils/make\_gl\_stipple\_from\_xpm.py\`: Converter from 32x32
    XPM images to a GL-Stipple array, typically for \`glutil.c\`
  - \`./utils\_maintenance/blender\_update\_themes.py\`: run to add new
    fields into existing XML themes
  - \`./utils\_maintenance/trailing\_space\_clean.py\`: Strip trailing
    space and \`BOM\` (byte-order-mark) of source files maintained by in
    our repo

### Setting Up the Repo

The developer tools are "connected" to the main Blender Git repository
as a submodule. The easiest way to ensure the tools are cloned and ready
to use is to run \`make update\` from the Blender's sources root.

Example of running the style checker:

``` bash
python3 source/tools/utils_maintenance/trailing_space_clean.py
```
