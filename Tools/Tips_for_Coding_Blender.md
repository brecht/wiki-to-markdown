# Tips for Coding Blender

by Kent Mein

## Searching Through the Code

One of the biggest questions that gets asked is where do I start, how do
I find blah.... The first thing is a little bit about the directory
structure.

Basically, there are 3 directories in the root directory that you need
to worry about:

  - extern (for external libraries that blender uses), \_not\_
    maintained by Blender.
  - intern (for internal libraries that blender uses), maintained by
    Blender.
  - source (main blender code, breaks down as follows)
      - source/blender (blender specific code)
      - source/creator (should be moved into blender dir some time)

For more info about the directory structure, see this document.

Blender is coded in 3 languages; C, C++ and Python. Python is used as an
internal scripting language for Blender, the majority of the Blender
code is in C and C++, so we will focus on that.

Here are some quick resources on each language:

  - <http://www.freeprogrammingresources.com/ctutor.html>
  - <http://www.mindview.net/Books>
  - <http://www.python.org>

You can also just do a google search on C tutorials, or whichever
language you are interested in.

There are a number of tools that will help you get started tracking
things down. Cscope is a good one. If you run cscope inside of the
Blender directory, you will be able to find all files that contain a
given string, and or lots of other searches. cscope can be found here:
cscope.sourceforge.net.

grep is also a nice tool. Here is a nice tutorial on using grep:
pegasus.rutgers.edu/\~elflord/unix/grep.html

Another good way to get a feel for the files is looking at the Makefiles
and/or just the sub directories. Chances are if the stuff you are
looking for isn't placed well in a dir in blender/source/blender, its
probably in blender/source/blender/src.

## Modifying the Code

Before you dig in and start changing things it is a good idea to take a
look at the coding style guide.

Start with something small. Updating a tool tip or changing something
like that. It's best to take simple steps when starting out.

Before submitting a patch or committing your changes to Git you should
make sure your change compiles correctly and tests are passing.

## Debugging

Debugging is considered an art form by some and can be tricky. There are
a couple of tools and methods that will save you some time though, and
get you headed down the right path. There are lots of things to say
here, lets start with the basics.

The first thing is to look at output from the compiler. A good tool for
logging your make is script. First, type script mylog.txt. Then enter
your build commands. When everything is done type exit then you can look
at mylog.txt and hunt down bugs.

One of the rules of debugging is to start with the first bug you
encounter. (It may be the cause of later bugs). This brings up another
point and that's warnings. Warnings are basically the compiler telling
you "hey, this looks fishy - it might not be what you want to do". It
would be nice to get rid of all of the warnings in Blender but currently
there are quite a few that are harmless. They could be from dated code,
or from lots of other reasons. You may want to save a script of your
initial build so you can filter out warnings that are already there.

Along with this is if you have access to multiple platforms, test your
code on as many platforms as you can. Each of the compilers are
implemented differently and they all report errors differently. The
extra error messages might show you a clearer picture of what is going
on.

Another good tool for debugging is using a text editor with syntax
highlighting. There are a bunch of them and it will save you a lot of
time, (so you don't have to hunt down missing "),};''s or other fun
little programming symbols. There are many different editors such as vi,
emacs, nedit, and so on. Use one you like.

The next tool in your bag should be printf statements - use them as much
as possible. And be sure to terminate all lines with newlines "\\n" so
you do not get buffered info that does not print. (of course for C++ you
can use cout, I'm just being generic)

If your working on a small function, it might make more sense to write a
little wrapper for it so you can just focus on your function. The
wrapper provides the global info you use from the rest of the Blender
source as well as a simple main function that tests your function.

Finally, the next step is to use a debugger. There are a bunch of them
out there. I'll just name a couple I've used quite a bit and like, ddd
and gdb. www.gnu.org/software/ddd has tutorials, download locations and
other info on ddd.

heather.cs.ucdavis.edu/\~matloff/UnixAndC/CLanguage/Debug.html has info
similar to the above site, as well as a basic guide to gdb. It's got a
lot more info than I've provided and even says not to use print
statements like I mentioned. I'll let you make the call on that one ;)

Again, if you're having problems, don't hesitate to ask the community.
If you can, jump on blender.chat and swing by \#blender-coders, if not,
post to the forums. Try to include what files you're having problems
with, any error messages, what platform you're on, etc. The more info
you provide the more likely you're going to get help.

# Related Topics

  - See: interview (audio)
    [http://www.se-radio.net/2009/11/episode-148-software-archaeology-with-dave-thomas/
    Software Archeology with Dave
    Thomas](http://www.se-radio.net/2009/11/episode-148-software-archaeology-with-dave-thomas/_Software_Archeology_with_Dave_Thomas)
    --- general guide for jumping into new code and reading code.
