# ClangFormat

ClangFormat is a tool for automatically formatting code to follow the
Blender code style.

## Policy

Most of the Blender source code is formatted automatically, with only a
few exceptions for externally maintained code.

All developers that commit code to the Blender repository **must use
clang-format before committing**. Otherwise, another developer running
clang-format will end up changing unrelated code.

We highly recommend integrating clang-format into your IDE to
automatically format code on file save. For all other cases, like
committing patches from other developers, run \`make format\` to format
the code.

## Installation

We provide clang-format as part of our precompiled libraries for
Windows, macOS and Linux.

On Linux, clang-format is also installed by the \`install\_deps.sh\`
script. It can also be installed through the package manager, and must
be version 12.0.0 or higher.

## Basic Usage

To apply code formatting to the entire source code, simply run this from
the blender root directory:

``` bash
make format
```

This is a wrapper around \`clang-format\` that checks the appropriate
version is available.

## IDE Integration

We highly recommend integrating clang-format into your IDE to
automatically format code, as this makes formatting convenient and hard
to forget.

Most development environments have native support or extensions to
integrate clang-format. Here are some pointers for commonly used IDEs,
more information is easily found online.

### Visual Studio

Visual Studio has built-in support for formatting and automatically uses
the \`.clang-format\` file.

  - [C/C++ Formatting
    Documentation](https://docs.microsoft.com/en-us/visualstudio/ide/reference/options-text-editor-c-cpp-formatting)
  - [Extension to format on file
    save](https://marketplace.visualstudio.com/items?itemName=mynkow.FormatdocumentonSave)
    (recommended)

### VS Code

[Clang-Format
extension](https://marketplace.visualstudio.com/items?itemName=xaver.clang-format)
is available, an option can be set to format on file save.

### Vim

Multiple plugins are available, as well as a [script bundled with
clang-format](https://clang.llvm.org/docs/ClangFormat.html#vim-integration).

The bundled script can be configured like this to format on file save:

``` bash
function! Formatonsave()
  let l:formatdiff = 1
  py3f <insert path to>/clang-format.py
endfunction
autocmd BufWritePre *.h,*.c,*.cc,*.cpp,*.m,*.mm,*.glsl,*.osl call Formatonsave()
```

### Emacs

See [How to clang-format the current buffer on
save?](https://emacs.stackexchange.com/questions/48500).

### QTCreator

[Beautifying Source Code
Documentation](https://doc.qt.io/qtcreator/creator-beautifier.html)

## Migrating Branches and Patches

The switch to clang-format will cause merge conflicts for effectively
all branches. Fortunately, these can be solved mostly automatically.
Patches should be migrated by creating a branch for them, and then
following the procedure for rebasing branches.

The basic idea is to merge in 3 steps:

  - Merge/rebase the branch up to the commit that introduced
    clang-format.
  - Apply the automatic formatting.
  - Merge/rebase the remaining commits.

This process can be automated by running the following script and
following the instructions:

``` bash
# Equivalent of 'git rebase master'
python3 source/tools/utils/blender_merge_format_changes.py --rebase
# Equivalent of 'git merge master'
python3 source/tools/utils/blender_merge_format_changes.py --merge
```

The `source/tools` submodule must be on the latest master branch for
this.

### Details

The script performs the following steps for merge:

``` bash
git merge <clang-format-commit>^1
... merge any conflicts and commit...

git merge -Xignore-all-space -Xours <clang-format-commit>
make format
git commit --amend

git merge master
... merge any conflicts and commit...
```

And for rebase:

``` bash
git rebase <clang-format-commit>^1
... merge any conflicts and commit...

git rebase -Xignore-all-space -Xours <clang-format-commit>
make format
git commit --amend

git rebase master
... merge any conflicts and commit...
```

For master replace \`<clang-format-commit>\` with
\`e12c08e8d170b7ca40f204a5b0423c23a9fbc2c1\`.
