This is a short guide on how I've been editing the blender-manual with
real-time feedback (WYSIWYG).

This guide relies on vim and the **Instant-rst** system by
[Rykka](https://github.com/Rykka)

## Installation

1\. Go to the Documentation Page and follow the instructions on how to
build it [https://projects.blender.org/blender/documentation
link](https://projects.blender.org/blender/documentation_link)

(for this guide I'm assuming the sources are in **\~/blender-manual**)

2\. Install **InstantRst**: [https://github.com/Rykka/InstantRst
link](https://github.com/Rykka/InstantRst_link) (this is a
plugin for vim)

3\. Install **InstantRst Server**:
[https://github.com/dfelinto/instant-rst.py/tree/blender-manual-static-templates
link](https://github.com/dfelinto/instant-rst.py/tree/blender-manual-static-templates_link)
(this is a patched python plugin)

## Running the Server

``` bash
instantRst -f ~/blender-manual/manual/render/workflows/bake.rst -i ~/blender-manual/manual/images
```

## Editing the Manual

``` bash
vim ~/blender-manual/manual/render/workflows/bake.rst
```

This will open the file in vim, and now you can run the plugin

``` bash
:InstantRst
```

Start editing and look at your browser being updated while you edit it.

## Further Considerations

Some RST syntaxes are not supported by Instanst-Rst server (in
particularly guilabel, which we use a lot). So it's good to build the
docs as you would normally to make sure everything looks fine. In this
case you could either do **make** to build everything, or **make
render** to build only the parts relative to the rendering.

## Advanced Notes

The patched **instant-rst server** has two new additions:

  - A new command-line parameter for the images folder
  - The static files and templates the Blender Manual requires

In the future we will try to change this system so it can be merged back
upstream.
