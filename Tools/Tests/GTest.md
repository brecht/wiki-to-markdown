# C/C++ Tests

[GoogleTest](https://google.github.io/googletest/) is the C++ testing
framework used to test C/C++ code in Blender.

Building of tests must be enabled using the \`WITH\_GTESTS\` CMake build
option, tests can then be run with \`make test\`.

## Test Executables

Unit tests are bundled in one executable per module, for example
\`blenlib\_test\`. Integration tests that depend on other Blender
modules are built as part of the \`blender\_test\` executable.

The tests executables are output to \`bin/tests/\` in the build
directory.

## Test Source Files

Test code is located in \`tests/\` folders in modules, next to the
source files for the module.

Details are described in [the Tests section of the C++ style
guide](../../Style_Guide/C_Cpp.md#Tests).

## Example

The tests for Blender's path utilities:
[\`blenlib/intern/path\_util.c\`](http://developer.blender.org/diffusion/B/browse/master/source/blender/blenlib/intern/path_util.c)

Test file located in:
[\`blenlib/tests/BLI\_path\_util\_test.cc\`](http://developer.blender.org/diffusion/B/browse/master/source/blender/blenlib/tests/BLI_path_util_test.cc)

The test can be executed from the build directory:
\`./bin/tests/blenlib\_test\`

And produces output, for example:

``` text
[==========] Running 3 tests from 1 test case.
[----------] Global test environment set-up.
[----------] 3 tests from path_util
[ RUN      ] path_util.PathUtilClean
[       OK ] path_util.PathUtilClean (0 ms)
[ RUN      ] path_util.PathUtilFrame
[       OK ] path_util.PathUtilFrame (0 ms)
[ RUN      ] path_util.PathUtilSplitDirfile
[       OK ] path_util.PathUtilSplitDirfile (0 ms)
[----------] 3 tests from path_util (0 ms total)

[----------] Global test environment tear-down
[==========] 3 tests from 1 test case ran. (0 ms total)
[  PASSED  ] 3 tests.
```

## Further Reading

  - [List of assertions that can be used in
    tests](https://google.github.io/googletest/reference/assertions.html)
  - [Automated
    Testing](https://projects.blender.org/blender/blender/issues?q=&type=all&sort=&state=open&labels=339&milestone=0&project=0&assignee=0&poster=0)
    issues on projects.blender.org
