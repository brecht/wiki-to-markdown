For tests related to Python and C/C++. For Python Tests, the first 3
links are complementary to each other, none of them being exhaustive in
itself.

  - [Setup](Setup.md)
  - [Python](Python.md)
  - [Adding New Tests](Adding_New_Tests.md)
  - [Adding tests for Geometry
    Nodes](GeometryNodesTests.md)
  - [GTest](GTest.md)
  - [Performance](Performance.md)
