# MediaWiki Documentation

<div class="bd-lead">

Information on how [wiki.blender.org](https://wiki.blender.org) is
configured. Check out [Special:Version](Special:Version) for
the full configuration.

</div>

## Extensions

We run a small number of extensions to improve the user experience:

  - [BacktickCode](https://www.mediawiki.org/wiki/Extension:BacktickCode)
    allows to show text as \<code\> between backticks (\`)
  - [DeleteBatch](https://www.mediawiki.org/wiki/Extension:DeleteBatch)
    Delete a batch of pages

## Bootstrap Skin

We use a
[Bootstrap 4](https://github.com/armadillica/mediawiki-bootstrap) based
skin.

## Templates

We use some custom templates that leverage Bootstrap elements:

  - [Template:Card](Template:Card)
  - [Template:Lead](Template:Lead)

## Development Setup

In order to test out new extensions or contribute to the the Skin, you
can use the following instructions:

  - Set up a [Docker
    Compose](https://docs.docker.com/samples/library/mediawiki/) file
    featuring:

<!-- end list -->

``` 
  mediawiki:
    image:                   mediawiki
    container_name:          mediawiki
    restart: always
    ports:
      - 8085:80
    links:
      - mysql
    # After initial setup uncomment this
    # volumes:
    #  - /data/mediawiki/html/LocalSettings.php:/var/www/html/LocalSettings.php  
    #  - /data/mediawiki/html/skins/Bootstrap:/var/www/html/skins/Bootstrap
```

  - Go through the MediaWiki installation process
  - Move the generated \`LocalSettings.php\` to
    \`/data/mediawiki/html/LocalSettings.php\`
  - Clone the Bootstrap 4 skin in
    \`/data/mediawiki/html/skins/Bootstrap\`
  - Uncomment the volumes in the \`docker-compose.yml\` file
  - Add \`wfLoadSkin( 'Bootstrap' );\` to \`LocalSettings.php\`
  - Restart the wiki container
