# Buildbot: How To Get Builds

## Downloading From Buildbot

Access builds from the [download
page](https://builder.blender.org/download/daily/).

Click on the **Daily | Experimental | Patch | Library** links to view
various pipeline category builds.

Click on the **Buildbot | Builders** links to access Buildbot Home and
Builder pages.

Your current platform will be selected.

**MacOS** now has two architectures **Arm** (Apple Silicon / M1) and
**Intel**

![../images/2021-05-27\_15\_57\_06-Blender\_Builds\_-\_blender.org.png](../images/2021-05-27_15_57_06-Blender_Builds_-_blender.org.png
"../images/2021-05-27_15_57_06-Blender_Builds_-_blender.org.png")

Click the line to download file.

A file hash are now available. Click on the **SHA-256** link to download
the file. ![Hash
file](../images/2021-05-27_16_16_23-Blender_Builds_-_blender.org.png
"Hash file")

### Archived Builds

Archived builds can be accessed by selecting the **All Archived Builds**
link.

![Show
archives](../images/2021-05-27_16_01_39-BlenderBuilds-blender.org.png
"Show archives")

Archived builds are available for a maximum of 25 days, then they are
purged from the system.

### File Naming Convention

File name format is based on [Semantic
Versioning 2.0.0](https://semver.org/)

**Format description:**

`  `  
`<app_id>-<version_id>-<risk_id>+<branch_id>.<commit_hash>-<platform>.<architecture>-<build_configuration>.<file_extension>`

**Example file name:**

`  `  
`blender-3.0.0-alpha+master.ba698f081270-darwin.x86_64-release.dmg`

**Example regular expression for parsing web page download:**

`  `  
``     $regexp = '^(?<app_id>(blender))\-' + ` ``  
``         '(?<version_id>[0-9]+\.[0-9]+\.[0-9]+)\-' + ` ``  
``         '(?<risk_id>[a-z]+)\+' + ` ``  
``         '(?<branch_id>[A-Za-z0-9_\-]+)\.' + ` ``  
``         '(?<commit_hash>[a-fA-F0-9]+)\-' + ` ``  
``         '(?<platform>[A-Za-z0-9_]+)\.' + ` ``  
``         '(?<architecture>[A-Za-z0-9_]+)\-' + ` ``  
``         '(?<build_configuration>(release|debug))\.' + ` ``  
`        '(?<file_extension>[A-Za-z0-9\.]+)'`

<table>
<tbody>
<tr class="odd">
<td><div class="note_title">
<p><strong>NOTE</strong></p>
</div>
<div class="note_content">
<p>The element `<branch_id>` is trimmed of redundant information such as `blender-` and `-release` words from the branch name of all release branches</p>
</div></td>
</tr>
</tbody>
</table>

## Using Steam Test Branches

You can now get Blender daily Alpha, Beta and Candidate builds from
Buildbot via Steam for all supported platforms.

### Switching to daily build branches

Available Blender continuous delivery test branches are:

  - daily-2.83 -\> Candidate
  - daily-2.93 -\> Candidate
  - daily-3.0 -\> Alpha - Version In Development (master)

Right click on **Blender** app and then select **Properties...**

![Select
properties](../images/2021-07-16_11_09_31-Greenshot_image_editor.png
"Select properties")

Select **BETAS** and the branch you require, scroll down the the very
bottom.

![Select
branch](../images/2021-07-16_11_10_20-Greenshot_image_editor.png
"Select branch")

Steam will download new version upon selection.

Close the Properties windows and wait for to date to finish to launch.

![Wait for update](../images/2021-07-16_11_11_02-Steam.png
"Wait for update")

Click on the **Launch** button.

![../images/2021-07-16\_11\_12\_02-Greenshot\_image\_editor.png](../images/2021-07-16_11_12_02-Greenshot_image_editor.png
"../images/2021-07-16_11_12_02-Greenshot_image_editor.png")

### Switching back to default versions

Go back to **Properties.../BETAS** and select \`None\` from branch list.

### Using new Blender launcher

The **vdev** branch now has **new** launcher you can test out.

![../images/2021-07-16\_12\_09\_47-Blender\_-\_Steam.png](../images/2021-07-16_12_09_47-Blender_-_Steam.png
"../images/2021-07-16_12_09_47-Blender_-_Steam.png")

## Using Snap Test Channels

For Linux users, you can now get Blender nightly Alpha, Beta and
Candidate builds from Buildbot via Snap store.

Snap services must be [installed and
running](https://snapcraft.io/docs/installing-snapd) prior to running
these commands.

### First time install - latest stable version

``` bash
$> sudo snap install blender --channel latest/stable --classic
```

### Version in development (vdev) - master branch

``` bash
$> sudo snap refresh blender --channel latest/edge
blender (edge) 3.0.0 from Blender Foundation (blenderfoundation✓) refreshed
```

Testing Blender version

``` bash
$> blender --version
Blender 3.0.0 Alpha
        build date: 2021-07-16
        build time: 00:04:51
        build commit date: 2021-07-15
        build commit time: 20:23
        build hash: 98c4224ff032
        build platform: Linux
        build type: release
        ...
```

### 2.93 LTS - Candidate

``` bash
$> sudo snap refresh blender --channel 2.93lts/candidate
blender (2.93lts/candidate) 2.93.2 from Blender Foundation (blenderfoundation✓) refreshed
```

### 2.83 LTS - Candidate

``` bash
$> sudo snap refresh blender --channel 2.83lts/candidate
blender (2.83lts/candidate) 2.83.17 from Blender Foundation (blenderfoundation✓) refreshed
```
