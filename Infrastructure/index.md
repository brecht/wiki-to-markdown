# Infrastructure

<div class="bd-lead">

The Blender project relies on several systems to aid in the development
effort of the software itself. This section groups information about
these systems.

</div>

  - Gitea (https://projects.blender.org) - hosts:
      - Code Repositories for Blender, its translations and add-ons as
        well as websites such as the Blender Cloud and Blender ID;
      - Bug Tracker and Task Management;
      - Patch submissions, code reviewing and auditing.
  - [Buildbot](BuildBot/index.md) - automated Blender
    builds. Used for the nightly builds and testing different platforms.
  - [HowToGetBlender](HowToGetBlender.md) -
    Various options to to get blender builds
  - [MediaWiki](MediaWiki.md) - this developer
    wiki.
  - [User Manual](https://developer.blender.org/project/manage/53) -
    documentation for users on how to use Blender.
  - Development Forum (https://devtalk.blender.org) - A place for
    discussing Blender development using Discourse.
  - Code Blog (https://code.blender.org) - For communication of new and
    ongoing projects. Made in Wordpress.
  - Official Website (https://blender.org) - presentation of the Blender
    project, including the download page. Made in Wordpress.
  - [OpenData](https://developer.blender.org/tag/blender_open_data) - a
    platform to collect and display the results of hardware and software
    performance tests, analytics, telemetry and more.
