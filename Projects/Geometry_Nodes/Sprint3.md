# Sprint 3

  - **Start**: 2020/11/14
  - **End**: 2020/11/27
  - **Goal**: Get scattering ready for master

## Time

  - Jacques: 6 days
  - Pablo: 1 days
  - Hans: 6 days
  - Simon: 2 days
  - Sebastian: 6 days

**Story points**: 100

## Promises

  - Object Info node + object socket
    [http://developer.blender.org/T82535
    \#82535](http://developer.blender.org/T82535_#82535)
  - Prototype for instancing in node tree
    [http://developer.blender.org/T82534
    \#82534](http://developer.blender.org/T82534_#82534)
  - Better error handling of unsupported node group inputs
    [http://developer.blender.org/T82438
    \#82438](http://developer.blender.org/T82438_#82438)
  - Investigate Support for Enums data type for Node Group Inputs
    [http://developer.blender.org/T82157
    \#82157](http://developer.blender.org/T82157_#82157)
  - Nodes design for the moss use case
    [http://developer.blender.org/T82538
    \#82538](http://developer.blender.org/T82538_#82538)
  - Categories for the existing nodes
    [http://developer.blender.org/T82367
    \#82367](http://developer.blender.org/T82367_#82367)
  - Sample file for trees and flowers
    [http://developer.blender.org/T82365
    \#82365](http://developer.blender.org/T82365_#82365)
  - Nodes design for bark tree use case
    [http://developer.blender.org/T82614
    \#82614](http://developer.blender.org/T82614_#82614)

## Impediments

### Blender Manual

Blender manual is hosted on SVN. Changes to the manual for this project
is done by multiple team members. Normally (during regular Blender
development) a single team member keeps track of the changes to the
manual and push it when ready. For this project we need to work with
multiple members on the same part of the documentation before pushing
it.

**Short term Actions**:

  - In short term we will host the Blender manual externally
    (gitlab/github) to reduce confusion and being able to work with
    multiple members on the same change.
  - When stuff gets in master the bits of the manual can then be
    committed into the b.d.o. SVN repository

**Future Actions**:

  - We need to bring this up with the admins to see how we could deal
    with this issue in the future.

## Sprint Review

### What went well

  - Overall good
  - We got something to show
  - Good and fast feedback loop
  - Super happy to see amazing results
  - Can already use the nodes
  - The created test files are energizing the team

### What didn't go well

  - Progress of scattering node wasn't clear
  - Are the priorities of UI/node editor changes correct.
  - Was the design clear of the node editor context. There was a lot of
    back and forth during implementation.
  - Too many tasks were chosen, and not fully completed.
  - Some tasks are to big, others to small
  - How to leverage feedback from community and developers in the
    process
  - To less time due to release and production work

### Improvements

  - Push WIP code to a public branch
  - Chose a single task and take it to completion
  - Add more fat to Sprint backlog. Not only must haves. These are low
    prio items that could also be handled by community or when devs are
    waiting for input
  - Add UI/Quality of life improvements. These should also be part of
    tasks
  - Prioritize Sample files before fleshing out the node design.
  - Meetings are not effective as updating the workboard is during the
    meeting. We should update the workboard before meetings and during
    the sprint, so meetings can be more effective.
  - Keep meetings short, People should be able to just leave or enter
    meetings. They don't need to sit in the whole time.
