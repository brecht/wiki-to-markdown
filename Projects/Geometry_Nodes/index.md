# Geometry Nodes

**Work board**: [https://developer.blender.org/project/board/121/ Scrum
Work
Board](https://developer.blender.org/project/board/121/_Scrum_Work_Board)

## Artifacts

This part needs to be completed by the development team

### Definition Of Ready

  - User story is clear to every team member, i.e. it has been talked
    through.
  - User story can be estimated.
  - User story is small enough to be achievable in one sprint.
  - There is nothing beyond the teams control that must be done first.
  - User story has a UI mockup (when necessary for the story).
  - A demo file has been planned.
  - For New Nodes it should be clear to what category it belongs.

### Definition Of Done

  - No regression tests have been broken.
  - Technical debt has not been increased, unless it has been decided
    beforehand.
  - Larger code changes have been submitted for code review in
    phabricator. Smaller changes have been reviewed and merged upstream.
  - There is a simple .blend file that uses the feature. This can be
    used by developers to test the feature.
  - There are release notes (those could also be written before the
    sprint?).

## Sprints

Doing online scrum makes it hard to plan that everyone is present during
all meetings. For that the progress meetings are late in the afternoon
at 14:00 CEST time. Sprints start on Monday afternoon and stop on Monday
morning.

### Schedule

| Day/Time           | Meeting           | Comments |
| ------------------ | ----------------- | -------- |
| W1/Mon: 14:00 CEST | Sprint Planning 1 |          |
| W1/Mon: 15:00 CEST | Sprint Planning 2 |          |
| W1/Tue: 14:00 CEST | Standup           |          |
| W1/Wed: 14:00 CEST | Standup           |          |
| W1/Thu: 14:00 CEST | Standup           |          |
| W1/Fri: 14:00 CEST | Standup           |          |
| W2/Mon: 14:00 CEST | Standup           |          |
| W2/Tue: 14:00 CEST | Standup           |          |
| W2/Wed: 14:00 CEST | Standup           |          |
| W2/Thu: 14:00 CEST | Standup           |          |
| W2/Fri: 14:00 CEST | Sprint demo       |          |
| W2/Fri: 15:00 CEST | Retrospective     |          |
|                    |                   |          |

  - [Projects/Geometry Nodes/Sprint Log
    Template](Sprint_Log_Template.md)

  
\=== Cycle \#2=== Period: *1/02/2021 - 26/3/2021*

Theme:

  - Wrap up studio requirements
  - Everything nodes design
  - Attribute workflow
  - Tools nodes

Sprints:

  - [Projects/Geometry
    Nodes/Sprint12](Sprint12.md)
  - [Projects/Geometry
    Nodes/Sprint11](Sprint11.md)
  - [Projects/Geometry
    Nodes/Sprint10](Sprint10.md)
  - [Projects/Geometry
    Nodes/Sprint9](Sprint9.md)
  - [Projects/Geometry
    Nodes/Sprint8](Sprint8.md)
  - [Projects/Geometry
    Nodes/Sprint7](Sprint7.md)

  
\=== Cycle \#1 === Period: *19/10/2020 to 22/01/2021*

Theme:

  - Mesh scattering

Sprints:

  - [Projects/Geometry
    Nodes/Sprint6](Sprint6.md)
  - [Projects/Geometry
    Nodes/Sprint5](Sprint5.md)
  - [Projects/Geometry
    Nodes/Sprint4](Sprint4.md)
  - [Projects/Geometry
    Nodes/Sprint3](Sprint3.md)
  - [Projects/Geometry
    Nodes/Sprint2](Sprint2.md)
  - [Projects/Geometry
    Nodes/Sprint1](Sprint1.md)
