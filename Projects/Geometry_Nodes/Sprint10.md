# Sprint 10

  - **Start**: 2021/02/22
  - **End**: 2021/03/05

Note: *The team will meet in the middle of the sprint (Monday) to check
if the sprint backlog is still valid*.

  - **Goal**: Wrap up attribute Workflow and design for tool system.

## Time

  - Jacques: 8 days
  - Pablo: 1 day
  - Hans: 8 days
  - Simon: 2 day

## Impediments

## Sprint Review

### What went well

  - Code review is working well.

<!-- end list -->

  - Community involvement is increasing.

<!-- end list -->

  - Great to see things that are not strictly necessary being
    prioritized (e.g., attribute search and spreadsheet).

<!-- end list -->

  - Amount of freedom we have to operate is paying off.

<!-- end list -->

  - 2.92 Release shows that the minimal yet complete feature have strong
    foundations.

<!-- end list -->

  - It is nice to see the new features merged.
  - Spreadsheet design process went well even though it should have
    fully preceded the coding stage.

### What didn't go well

  - Sometimes it feels rushed, not leaving time to finishing up to
    completion some tasks.
  - That doesn't leave enough time for too many "good to have" tasks,
    leaving only the "must be done" tasks complete.
  - The spreadsheet building on scale phase started while the final
    design wasn't approved yet.
  - Team is growing disconnected with the tools that we are building.
  - Miscommunication regarding the main goals for this development
    cycle.
  - Spreadsheet was too big of a topic to not formally include other
    modules early in the design process.
  - Dalai found the past 2 weeks a bit overwhelming leading to a drop of
    quality in some of his responsibilities.

### Improvements

  - Hans wants to find time to tackle old standing issues.

<!-- end list -->

  - We could have leveraged the quick prototype as a part of the design.

<!-- end list -->

  - Try to use geometry nodes more.

<!-- end list -->

  - Try to train the process and share the responsibilities for the
    design steps/process more.
