# Sprint 9

  - **Start**: 2021/02/15
  - **End**: 2021/02/19

<!-- end list -->

  - **Goal**: Attribute Workflow initial implementations and missing
    designs.

## Time

  - Jacques: 4 days
  - Pablo: 1 day
  - Hans: 4 days
  - Simon: 1 day

## Impediments

## Sprint Review

### What went well

  - Cutting meetings in smaller sections worked well.

<!-- end list -->

  - Attribute system coming together is proving how good the overall
    design turned out to be.

<!-- end list -->

  - The present approach to design tasks seems to be working.

<!-- end list -->

  - Response by the community seems very positive.

### What didn't go well

  - The may be over-promising the tasks for the sprint.

<!-- end list -->

  - Having other tasks are still getting on the way.

### Improvements

  - Longer sprints (2 weeks) but with a weekly sprint planning revisit.

<!-- end list -->

  - Try to include the needed time to clean up designs for its final
    form as part of their design meetings.
