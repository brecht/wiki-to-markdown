# Sprint 5

  - **Start**: 2020/12/07
  - **End**: 2020/12/18
  - **Goal**: {high level sentence about the goal of this sprint}.

## Time

  - Jacques: 6 days
  - Pablo: 2 days
  - Hans: 6 days
  - Simon: 1 days
  - Sebastian: 0 days

## Promises

  - 
## Impediments

## Sprint Review

### What went well

  - Tasks were all in the same context. The nodes were clustered and
    could be used during testing.
  - Code Review. Normal review and landing to master.
  - Stopping the previous sprint.

### What didn't go well

  - Finishing the poison disk took longer. Tiring and painful getting it
    done.
  - Couldn't advance on the design task this sprint.
  - Crash on undo, got confused, wrong state of mind.

### Improvements

  - Backlog of designs should be cleared. The idea is to ask Pablo and
    Simon to add new designs in the first week of January
  - There was a discussion about Scrum vs 6 week projects vs Kanban. No
    decision made.
  - Try to asses the resources better at the start of the sprint.
  - Use gitsvn for documentation.
  - Share latest test files on the landing page of the module.
  - Make the current channel publicly open. We will monitor how it goes.
