# Sprint 8

  - **Start**: 2021/02/08
  - **End**: 2021/02/12

<!-- end list -->

  - **Goal**: Validate the everything nodes design with the lights of
    geometry nodes:
  - Mapping out the use cases for the other node-based systems.
  - On a high level, design how the different systems work.
  - On a high level, design how the different systems integrate with
    each other.

## Time

  - Jacques: 4 days
  - Pablo: 1 day
  - Hans: 4 days
  - Simon: 1 day

## Impediments

## Sprint Review

### What went well

  - Code review dynamic is working well.
  - Satisfaction from mapping out the existing modifiers to help
    community involvement.
  - More eyes in the same design problem (simulations) help to crack
    bigger problems or share the pain of the constraints of the
    solution.
  - In person meetings about design went well although not strictly
    necessary.
  - Focused, scheduled design time pays off.

### What didn't go well

  - Non-prioritized tasks took precedence over others, leading to a
    smaller throughput of the intended tasks.
  - Meetings agendas was a bit hijacked with other topics, leading to
    important topics not to be addressed.
  - Jacques didn't know how to approach big design so he wasn't so
    involved in the design topics.
  - Mandatory meetings are taking too much time.
  - Lack of past based scrum week combined with unknown design process
    lead to lack of big picture of the progress of the scrum.

### Improvements

  - Leave meetings when feel like it the presence not needed.
