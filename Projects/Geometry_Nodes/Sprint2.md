# Sprint 2

  - **Start**: 2020/11/09
  - **End**: 2020/11/13
  - **Goal**: Preparation to implement pebble user story in the next
    sprint

## Time

  - Jacques: 3 days
  - Pablo: 2 days
  - Hans: 3 days
  - Simon: 1 days

<!-- end list -->

  - Story points: 50

## Promises

  - Object Info node + object socket
    [http://developer.blender.org/T82535
    \#82535](http://developer.blender.org/T82535_#82535)
  - Prototype for instancing in node tree
    [http://developer.blender.org/T82534
    \#82534](http://developer.blender.org/T82534_#82534)
  - Points Distribution node density attribute field
    [http://developer.blender.org/T82376
    \#82376](http://developer.blender.org/T82376_#82376)
  - Better error handling of unsupported node group inputs
    [http://developer.blender.org/T82438
    \#82438](http://developer.blender.org/T82438_#82438)
  - Investigate Support for Enums data type for Node Group Inputs
    [http://developer.blender.org/T82157
    \#82157](http://developer.blender.org/T82157_#82157)
  - Design for the Object Info node [http://developer.blender.org/T82536
    \#82536](http://developer.blender.org/T82536_#82536)
  - Final "modifier name" (currently it is Nodes)
    [http://developer.blender.org/T82537
    \#82537](http://developer.blender.org/T82537_#82537)
  - Nodes design for the moss use case
    [http://developer.blender.org/T82538
    \#82538](http://developer.blender.org/T82538_#82538)
  - Nodes editor context design [http://developer.blender.org/T82366
    \#82366](http://developer.blender.org/T82366_#82366)
  - Final categories for the nodes [http://developer.blender.org/T82367
    \#82367](http://developer.blender.org/T82367_#82367)
  - Sample file for trees and flowers
    [http://developer.blender.org/T82365
    \#82365](http://developer.blender.org/T82365_#82365)

## Burndown chart

| Day | Story points promissed | Story points burned | Expected burn |
| --- | ---------------------- | ------------------- | ------------- |
| 1   | 0                      | 0                   | 0             |
| 2   | 0                      | 0                   | 0             |
| 3   | 0                      | 0                   | 0             |
| 4   | 0                      | 0                   | 0             |
| 5   | 0                      | 0                   | 0             |
| 6   | 0                      | 0                   | 0             |
| 7   | 0                      | 0                   | 0             |
| 8   | 0                      | 0                   | 0             |
| 9   | 0                      | 0                   | 0             |
| 10  | 0                      | 0                   | 0             |

## Impediments

{list containing impediments that happened during the sprint}

  - 
## Sprint Review

Due to the short sprint and not correct availability of the resources a
lot of tasks were not fully finished. Open tasks will be finalized in
the next sprint.

### Improvements

#### Went well

  - Discussion, communication of project and how community is responding
    \[Dalai, Pablo\]
  - Design tasks are ahead \[Pablo, Simon\]
  - Tasks were more clear, less guess time, more structures back log
    \[Jacques\]
  - Recording of the design discussions \[Hans\]

#### Improvements

  - Unclear when devs are available for working, add number of days
    ahead of time to the sprint page \[All\]
  - Be more proactive when tasks are send to ready for testing and ask
    other team members to test, read, give feedback or do code review
    \[All\]
  - Categories
      - DoR: when adding new nodes, it should be clear to what category
        it belongs to
      - Add cleanup task for existing nodes.
  - Add workboard column for bugs that are still not confirmed.
    \[Dalai\]
  - Add EPIC/User Story to the task title/name \[Dalai\]
