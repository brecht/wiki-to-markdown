# Sprint 12

  - **Start**: 2021/03/22
  - **End**: 2021/03/26

<!-- end list -->

  - **Goal**: Finish the documentation of the previous sprints
    (modifiers, everything nodes).

## Time

  - Jacques: 3 days
  - Pablo: 1 day
  - Hans: 4 days
  - Simon: 1 day

## Impediments

\-

## Sprint Review

For this sprint there was no retrospective. Instead the team did a
retrospective for the entire 1st and 2nd cycles of this project.

### What went well

  - Working in a team in a single problem with focus, regular feedback
    sections and shared responsibilities (\#1).
  - Very directed building of a feature (\#1).
      - Validated the design and worked as foundation for further
        development (\#1).
  - Give the proper time it requires to build the right solutions (\#2).
  - Emphasis on re-validation of priorities without overhead/constraints
    of format (\#2).
  - Getting to a better design and focus with the problem well defined
    ahead of time (\#1).
  - Having a cycle to polish the foundation managed to get a lot in
    Blender (\#2).
  - Community involvement, aligning with community, design ahead of
    patch (\#2).
  - Splitting the project in two cycles.
      - More closed/focused and then another with more community
        involvement. (\#1 \#2)
  - Focused-single themed sprints (\#1)
  - Quality of the deliverables was very high and polished (\#2).
  - Cut down the time for meetings that didn’t require everyone (\#2).
  - Kickoffs helped the team feel like working together (\#1, \#2)

### What didn't go well

  - Too many rigid targets (\#2).
  - Too many things that needed to be designed (\#2).
  - Design weeks lacked work ahead of time (\#2).
  - Having a bottleneck like design conflicted with pre-established
    rigid deliverables.
  - Design throughput was a bottleneck (\#2).
  - The footprint of working with daily kickoff and other meetings is
    high (\#1).

### Improvements

  - Cutting down the number of targets (\#1 vs \#2). Separate projects
    into multiple projects if needed, to help focus.
  - Communicate to the world ahead of the project that the project will
    happen, where to find updates (e.g., weekly notes) and how to get
    involved.
  - Dedicate more structured time for design.
  - Make sure the hand-over is planned as part of the project.
      - The project itself can include time for handover.
      - A subsequent project can be proposed only to handle that (e.g.,
        nodes task-force)
      - The project teams should be able to be involved in the module
        after the project.
      - It helps if it is clear what is expected as part of a hand-over.
