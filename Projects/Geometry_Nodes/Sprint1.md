# Sprint 1

  - **Start**: 2020/10/19
  - **End**: 2020/11/02
  - **Goal**: Preparation user stories.

## Time

  - Jacques: 6 days
  - Sebastian: 5 days
  - Pablo: 4 days
  - Hans: 6 days
  - Simon: 2 days

<!-- end list -->

  - Story points: 100

## Promises

  - Break down pebble scattering example file (EPIC) into user stories
    for the next sprint.
  - Basic geom modifier (Geom In and Geom Out)

## Burndown chart

| Day | Story points promissed | Story points burned | Expected burn |
| --- | ---------------------- | ------------------- | ------------- |
| 1   | 0                      | 0                   | 0             |
| 2   | 0                      | 0                   | 0             |
| 3   | 0                      | 0                   | 0             |
| 4   | 0                      | 0                   | 0             |
| 5   | 0                      | 0                   | 0             |
| 6   | 0                      | 0                   | 0             |
| 7   | 0                      | 0                   | 0             |
| 8   | 0                      | 0                   | 0             |
| 9   | 0                      | 0                   | 0             |
| 10  | 0                      | 0                   | 0             |

## Impediments

{list containing impediments that happened during the sprint}

  - 
## Sprint Review

  - During the sprint review several issues were discussed around team
    synchronization, focus, time/resourcing and workflow.
      - Discuss so everyone is on the same page.
      - Focus: Backlog should be more clear, before starting on an
        issue.
      - Focus: Team got derailed and starting on other tasks that
        weren't promised.
      - Miro board: current miro board contains too much, no clear
        overview of what is needed for a sprint.
      - Workflow is confusing as it is new for many of us.
  - Node group selection workflow is missing a design

### Improvements

  - Design should work 1 sprint ahead. \[Everyone\]
      - When you are not working on something that isn't related to your
        task, think again what is going on. Don't derail from the
        promises made unless it was discussed and approved with the
        product owner.
      - Work more as a team. Developers can also help with designs.
  - Create a FAQ page for the project \[Hans\]
  - Miro board should be organized better, no concrete plan, but will
    investigate before the next sprint \[Dalai\]
  - Daily stand-ups will be rescheduled to 2PM AMS time. This leaves
    more room for meetings afterwards \[Jeroen\] (DONE)
