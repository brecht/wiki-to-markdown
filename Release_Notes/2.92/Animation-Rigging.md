# Blender 2.92: Animation & Rigging

## User Interface

Playback Synchronisation has been updated for clarity
([rBf4e8c0c1](https://projects.blender.org/blender/blender/commit/f4e8c0c104f)).

  - Label changed from "Audio" to "Sync" (the "Audio" label has moved
    down one option).
  - Labels of the sync enum changed:
      - No Sync → Play Every Frame
      - AV-sync → Sync to Audio

![../../images/release-notes-292-animation-playback-sync.png](../../images/release-notes-292-animation-playback-sync.png
"../../images/release-notes-292-animation-playback-sync.png")

Group Colors in Animation Channels can be disabled in User Preferences
([ad85256e71](https://projects.blender.org/blender/blender/commit/ad85256e710)).
The old "Show Group Colors" toggle had to be set per editor, and was on
by default. This meant that disabling group colors would require an
action for every file, for every editor. As it is hard to select a color
that works both as bone color in the 3D Viewport (needs to be bright
there) as well as the channel list (needs to be dark there), most
animators turned channel list colors off. This now only has to be done
once, in the User Preferences.
![../../images/release-notes-292-animation-channel-group-colors.png](../../images/release-notes-292-animation-channel-group-colors.png
"../../images/release-notes-292-animation-channel-group-colors.png")

The name for "F-Curve Visibility" in the above panel was changed to
"Unselected Opacity" to reflect the fact that it only affects unselected
F-Curves
([b9e02eace6](https://projects.blender.org/blender/blender/commit/b9e02eace69e)).

Deleting keyframes can now be done without additional confirmation in
the Dopesheet and Graph Editor
([290b6d7ef3](https://projects.blender.org/blender/blender/commit/290b6d7ef327)).
The operator now has a "Confirm" option which can be turned off in the
keymap. By default, deleting with 'X' still brings up the Delete menu
with additional operators, but deleting with 'Del' / 'Backspace' now
does not require confirmation anymore.
![../../images/Delete\_keyframes.png](../../images/Delete_keyframes.png
"../../images/Delete_keyframes.png")

## Euler Filter

The Euler Discontinuity Filter has been improved
([rBe4ca1fc4](https://projects.blender.org/blender/blender/commit/e4ca1fc4ea43)).
This new discontinuity filter performs actions on the Euler X/Y/Z
rotation channels as a whole, rather than only on the individual X/Y/Z
channels. This makes it fix a wider range of discontinuities. The filter
now runs twice on the selected channels, in this order:

1.  Per triplet of X+Y+Z rotation channels: convert to matrix, then back
    to Euler angles in a smart way to minimize jumps.
2.  Per channel: add/remove factors of 360° to minimize jumps.

![../../images/release-notes-292-animation-euler-filter.png](../../images/release-notes-292-animation-euler-filter.png
"../../images/release-notes-292-animation-euler-filter.png")

## Bake Action

Bake Action: The post-bake cleanup of F-Curves is now optional and
disabled by default. Previously Bake Action would do two things:

  - Bake the Action
  - Clean up the FCurves

It is now possible (and even the default) to only perform the baking
operation.

![../../images/release-notes-292-animation-bake-action-clean-curves-optional.png](../../images/release-notes-292-animation-bake-action-clean-curves-optional.png
"../../images/release-notes-292-animation-bake-action-clean-curves-optional.png")

Furthermore, the Bake Action operator now performs better Euler
discontinuity filtering
([rBe5528904](https://projects.blender.org/blender/blender/commit/e5528904f121),
[rB0e85d701](https://projects.blender.org/blender/blender/commit/0e85d701c654)).

![../../images/release-notes-292-animation-bake-action-euler-filter.png](../../images/release-notes-292-animation-bake-action-euler-filter.png
"../../images/release-notes-292-animation-bake-action-euler-filter.png")

## Custom Object Space for Constraints

Constraints can now use **Custom Space** with a custom object to define
the evaluation space, next to the already existing World Space, Local
Space, Pose Space, and Local with Parent.

The Custom Space option uses the Local Space of an other
object/bone/vertex group. If selected on owner or target it will show a
box for object selection. If an armature is selected, then it will also
show a box for bone selection. If a mesh object is selected it will show
the option for using the local space of a vertex group.

![../../images/release-notes-292-animation-constraint-custom-space.png](../../images/release-notes-292-animation-constraint-custom-space.png
"../../images/release-notes-292-animation-constraint-custom-space.png")

## NLA

  - New NLA strips will have Sync Length enabled
    ([rB60746363](https://projects.blender.org/blender/blender/commit/60746363875a67ae15278d8e4629f898d9562c47)),
    allowing animators to keyframe outside of the strip's bounds by
    default. The strip will automatically grow to include the new keys.
  - FCurves with modifiers are now properly drawn anchored to the NLA
    strip
    ([rB30dd31a7](https://projects.blender.org/blender/blender/commit/30dd31a7b3d8)).

## Weight Paint

  - Weight painting with Auto-Normalize will now paint zero instead of
    very small weights when locked deform groups add up to more than
    0.999999.
    ([rB58dae919](https://projects.blender.org/blender/blender/commit/58dae919e5))

## Bone Deformation

Artifacts caused by bone scaling with the Preserve Volume armature
deformation method have been fixed (significantly reduced) for B-Bone
chains.
([b5c3f26cba](https://projects.blender.org/blender/blender/commit/b5c3f26cba))

![../../images/Release-notes-292-animation-bbone-deform.png](../../images/Release-notes-292-animation-bbone-deform.png
"../../images/Release-notes-292-animation-bbone-deform.png")
