# Blender 2.92 Release Notes

Blender 2.92 was released on February 25, 2021.

Check out the final [release notes on
blender.org](https://www.blender.org/download/releases/2-92/).

## [Geometry Nodes](Geometry_Nodes.md)

## [User Interface](User_Interface.md)

## [Modeling](Modeling.md)

## [Sculpt](Sculpt.md)

## [Grease Pencil](Grease_Pencil.md)

## [Cycles](Cycles.md)

## [EEVEE](EEVEE.md)

## [I/O & Overrides](IO.md)

## [Python API](Python_API.md)

## [Physics](Physics.md)

## [Animation & Rigging](Animation-Rigging.md)

## [Add-ons](Add-ons.md)

## [More Features](More_Features.md)
