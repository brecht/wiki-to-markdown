# Blender 2.92: Add-ons

## Rigify

  - Support for 5 bone chains in the \`limbs.paw\` rig type.
    ([rBA46590bb](https://projects.blender.org/blender/blender-addons/commit/46590bb780))
  - New \`limbs.front\_paw\` and \`limbs.rear\_paw\` rig subtypes with
    extra IK automation designed for front and rear limbs of animals.
    ([rBA46590bb](https://projects.blender.org/blender/blender-addons/commit/46590bb780))
  - The horse metarig updated using the new paw features to represent
    the horse anatomy better.
    ([rBA46590bb](https://projects.blender.org/blender/blender-addons/commit/46590bb780))
  - The \`limbs.super\_palm\` rig can now optionally generate a separate
    FK control for every palm bone.
    ([rBA287f844](https://projects.blender.org/blender/blender-addons/commit/287f8443007))
  - The \`basic.super\_copy\` rig will now use the widget assigned to
    the metarig bone as is, and it's possible to choose which built-in
    widget to generate from a list. Human metarigs have better shoulder
    widgets now.
    ([rBAb0361dc](https://projects.blender.org/blender/blender-addons/commit/b0361dcaac))
  - Rigify will now create a separate widget collection for each
    generated rig, using the \`WGTS\_...\` naming.
    ([rBAf8d6489](https://projects.blender.org/blender/blender-addons/commit/f8d6489fb6))

## FBX I/O

  - Exporter has a new option, "Use Space Transform", that allows to
    write a different axis orientation system that default Blender one,
    without actually trying to apply this global transformation to
    objects. It seems to allow Unity to better deal with orientations on
    import
    ([rBA1a9a9d0](https://projects.blender.org/blender/blender-addons/commit/1a9a9d008dea)).

## BlenderKit

  - Texture Resolutions - All textured assets with higher original
    resolutions now have lower resolutions generated.
      - This saves memory, speeds up all interactions, and also helps to
        render bigger scenes.
      - Replace asset resolution command in the Selected model panel or
        in the Asset bar can be used to swap asset resolution.

![The resolution is set before download in import settings and can be
changed after downlod from
menu.](../../videos/Resolutions_feature_in_BlenderKit.mp4
"The resolution is set before download in import settings and can be changed after downlod from menu.")

  - Model thumbnailer was improved, default lighting and angle now look
    much cleaner. Uses a new HDR provided by Mike Radjabov.
  - HDR asset type
  - Scene asset type - BlenderKit scenes are templates to finish certain
    tasks, or usable as environment setups for your models.
  - Particle system support inside Model asset type
      - It's possible to drag-drop particle systems and these will
        automatically be transferred on the target mesh. The count of
        particles is recalculated according to the target area, so the
        particle system looks similar as result.

![Drag-drop of a compatible particle system on a
mesh.](../../videos/Particlesystem.mp4
"Drag-drop of a compatible particle system on a mesh.")

  - Upload was refactored
      - Updating only metadata is now much faster since it happens in
        the background thread.
      - If only the thumbnail is re-uploaded, background blender
        instance doesn't start.
  - New fast metadata editing is possible from the right-click menu in
    asset bar.
  - Snapping was improved and fixed.
      - Snapping ignores objects that are only drawn as bound boxes or
        are part of a particle system(like grass). This is especially
        useful if your scene has a lot of boolean modifier objects for
        purpose of cutting holes.
      - Snapping also works on inverted normals objects by always
        snapping on the visible side of the mesh.
      - Snapping works from inside of orthographic cameras
  - Several improvements have been made to the rating UI, showing
    ratings more prominently and allowing users to rate.

## Collection Manager

### New Features

  - An override checkbox was added to the preferences to override the
    object hiding hotkeys with object disabling hotkeys. Menu items were
    added to the Object-\>Show/Hide menu for the disable object
    operators.
    ([rBA7cc2e4b](https://projects.blender.org/blender/blender-addons/commit/7cc2e4b1))

<!-- end list -->

  - Selected objects' collections can now be either isolated or disabled
    from both the Collection Manager popup and the Quick View Toggles
    menu.
    ([rBA47820f6](https://projects.blender.org/blender/blender-addons/commit/47820f66))
  - Quick View Toggle hotkeys were updated to be more consistent.
    ([rBA6fcfb86](https://projects.blender.org/blender/blender-addons/commit/6fcfb869))
    ([rBA03fd6c5](https://projects.blender.org/blender/blender-addons/commit/03fd6c5a))

### Bug Fixes

  - Fixed a bug where QCD hotkeys would get registered twice when
    toggling the QCD checkbox in the preferences off and then on
    again.([rBA2b1d58f](https://projects.blender.org/blender/blender-addons/commit/2b1d58fe))
