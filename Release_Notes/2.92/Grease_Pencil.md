# Blender 2.92: Grease Pencil

## User Interface

  - Interpolate tools now are visible in Draw mode and not only in Edit
    mode. When the tool is used in Draw mode, the \`only selected
    strokes\` option is disabled.
    ([7bf0682aa9](https://projects.blender.org/blender/blender/commit/7bf0682aa98a))
  - Enabled by default \`Layer Onion Skin\` for new layers.
    ([ba740ad2ab](https://projects.blender.org/blender/blender/commit/ba740ad2abc6))
  - New Material parameter to rotate texture for Dots and Strokes.
    ([e6f61a4a37](https://projects.blender.org/blender/blender/commit/e6f61a4a379b))
    See also the new parameter in Texture modifier below.

![New rotation parameter](../../images/Gpencil_material_rot_param.png
"New rotation parameter")

  - Recalculate Geometry operator added to Cleanup menu. This operator
    was only available using Search and it was used to fix old or
    damaged files.
    ([7567086276](https://projects.blender.org/blender/blender/commit/756708627613))
  - Cleanup menu reorganized.
    ([216880bb47](https://projects.blender.org/blender/blender/commit/216880bb4733))
  - Target object parameter changed in Bake mesh animation and Trace
    image.
    ([e9b955b99c](https://projects.blender.org/blender/blender/commit/e9b955b99cd3))
  - New menu Paint in Vertex Paint mode. These operators existed in
    previous versions, but the menu was removed by error. Also, now all
    operators support multiframe edition.
    ([986955a2d5](https://projects.blender.org/blender/blender/commit/986955a2d53a))

![New Vertex Paint menu](../../images/GPencil_MenuVertex_Paint.png
"New Vertex Paint menu")

  - Added Pin icon for default eraser when using \`Ctrl\` key.
    ([8e1b63d4bd](https://projects.blender.org/blender/blender/commit/8e1b63d4bd5b))

## Operators

  - New option to Bake Animation only for selected frames instead to
    bake all available frames. To bake only selected, select in the
    Dopesheet the keyframes to
    bake.([657e344351](https://projects.blender.org/blender/blender/commit/657e34435174))

![Bake selected frames](../../images/BakeAnimationFrame.png
"Bake selected frames")

  - New option to trace image sequences and convert to grease pencil
    strokes. Now it's possible to trace a single image or a full
    sequence of images from an image sequence or a video. If the trace
    is for a sequence, a new batch job is started and a progress bar is
    displayed during the trace.
    ([a5d237ac85](https://projects.blender.org/blender/blender/commit/a5d237ac85c7))

![`Trace``   ``Sequences`](../../videos/Gpencil_trace_seguences.mp4
"Trace Sequences")

  - Improved Join operator to use the distance between points. Before,
    the join was done using the order of the strokes but not the
    distance between extremes. Now, the algorithm calculates the
    distance and tries to join always the nearest extremes.
    ([21f201e25e](https://projects.blender.org/blender/blender/commit/21f201e25e70))

![`New``   ``Join`` 
 ``algorithm`](../../images/Gpencil_before_after_join.png
"New Join algorithm")

  - Improved method for interpolation. The new method gives better
    results when interpolating between different sized strokes.
    Previously, the longer stroke would be cut off. With the new
    algorithm, the strokes are properly "stretched" to fit the extreme
    shapes exactly.
    ([151e847b87](https://projects.blender.org/blender/blender/commit/151e847b8709))

![New interpolation
method](../../videos/Gpencil_before_after_interpolation_method.mp4
"New interpolation method")

  - New operator in Vertex Paint mode to reset vertex color data. After
    running this operator, all vertex paint data is removed and the
    stroke is reset to default material status.
    ([9081b80d15](https://projects.blender.org/blender/blender/commit/9081b80d15b3))
  - Now Merge Layers respects the old keyframe drawing when the target
    frame was missing. See Task
    [T83705](https://developer.blender.org/T83705) for more details.
    ([57f900e4ef](https://projects.blender.org/blender/blender/commit/57f900e4efda))

## Tools

  - New option to set flat the cut extreme of the stroke after using
    Cutter tool. If the stroke angle is very extreme, parts of the
    stroke can be visible still.
    ([2985a745bb](https://projects.blender.org/blender/blender/commit/2985a745bb01))

![Cutter Caps](../../images/Cutter_Flat_Option.png "Cutter Caps")

  - New option to automerge strokes while drawing.
    ([e9607f45d8](https://projects.blender.org/blender/blender/commit/e9607f45d85d))

## Modifiers and VFX

  - Now Subdivision modifier adds geometry if the stroke is using
    cyclic. This new geometry only refers to number of points, but the
    extremes of the stroke are not smoothed at all in order to preserve
    original extreme points.
    ([e296d58cf2](https://projects.blender.org/blender/blender/commit/e296d58cf220))
  - New parameter to rotate textures in Dots and Strokes in Texture
    modifier. This effect could be also obtained using the new material
    texture rotation parameter.
    ([c80594f57f](https://projects.blender.org/blender/blender/commit/c80594f57f1e))
  - Now the Effects can be linked using \`Ctrl + L\` Link operator.
    ([c8a3d1a1fa](https://projects.blender.org/blender/blender/commit/c8a3d1a1fa65))
  - New Additional menu to Copy Effects.
    ([6327771bc9](https://projects.blender.org/blender/blender/commit/6327771bc968))

![Additional Menu](../../images/Gpencil_Effects_Additional_Menu.png
"Additional Menu")

  - Array modifier now support uniform random scale.
    ([5535b0b887](https://projects.blender.org/blender/blender/commit/5535b0b8878a))

## Curve edit mode

  - The Google Summer of Code project [Editing grease pencil strokes
    using curves](../../User/Filedescriptor/GSoC_2020/Report.md) added
    a new submode in edit mode (similar to multi-frame edit) that allows
    the transformation of strokes using bézier curves.
    ([0be88c7d15](https://projects.blender.org/blender/blender/commit/0be88c7d15d2))

![Curve edit mode](../../videos/Curve_edit_mode_demo.webm
"Curve edit mode")

  - To enable Bezier mode, use the bezier button in the top bar or press
    \`U\` key.

<!-- end list -->

  - Note: all the edit mode operators are supported **except** the ones
    below, which will be added soon:
      - Duplicate
      - Copy & Paste
      - Snap to Cursor & Cursor to selected
      - Flip
      - Simplify & Simplify Fixed
      - Trim
      - Separate
      - Split
