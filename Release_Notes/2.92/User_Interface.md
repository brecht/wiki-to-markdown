# Blender 2.92: User Interface

## Outliner

  - Properties editor sync. Clicking on the icon for outliner data will
    switch to the corresponding properties editor tab for properties
    editors that share a border.
    ([0e47e57eb7](https://projects.blender.org/blender/blender/commit/0e47e57eb77d))
  - New filter to display only selectable objects.
    ([fbf2908674](https://projects.blender.org/blender/blender/commit/fbf29086745f))
  - The object state filter can now be inverted. For example, inverting
    "Selectable" will only display unselectable objects.
    ([2afdb4ba87](https://projects.blender.org/blender/blender/commit/2afdb4ba875))
  - List library overrides in the Outliner. A filter option allows
    hiding them
    ([aa64fd69e7](https://projects.blender.org/blender/blender/commit/aa64fd69e733)).

## Properties Editor

  - Add a new tab for collection properties
    ([3e87d8a431](https://projects.blender.org/blender/blender/commit/3e87d8a4315d)).

## 3D Viewport

  - Use consistent name for select mirror across object modes
    ([1c357a3c5f](https://projects.blender.org/blender/blender/commit/1c357a3c5fc1)).
  - Cycling object selection is now ordered by depth (nearest first)
    ([3e5e204c81](https://projects.blender.org/blender/blender/commit/3e5e204c81a2aa136ad645155494b7ab132db2bf)).

## General Changes

  - Support tooltips for superimposed icons
    ([2250b5cefe](https://projects.blender.org/blender/blender/commit/2250b5cefee7)).
  - Improvements to the alignment and placement of interface items
    ([6bf043ac94](https://projects.blender.org/blender/blender/commit/6bf043ac946f))
    ([b0a9a04f62](https://projects.blender.org/blender/blender/commit/b0a9a04f6201)).
  - Allow theming the alternate row color in the sequencer, similar to
    the file browser and the outliner
    ([f7223d5f72](https://projects.blender.org/blender/blender/commit/f7223d5f722ac4)).
  - When dragging, constrain panels vertically in the property editor,
    preferences, and side-bars vertically
    ([328aad8c98](https://projects.blender.org/blender/blender/commit/328aad8c98c931c3)).
  - When using Weight Paint Sample Weight Tool, using eyedropper cursor
    and display weight in header
    ([64277e8f3a](https://projects.blender.org/blender/blender/commit/64277e8f3a7e))
    ([e3d9241a05](https://projects.blender.org/blender/blender/commit/e3d9241a0532)).
  - New layout for the "About" dialog with full Blender logo
    ([79eeabafb3](https://projects.blender.org/blender/blender/commit/79eeabafb39f)).

![../../images/About\_Dialog.png](../../images/About_Dialog.png
"../../images/About_Dialog.png")

  - New look for the "Startup Script Execution" warning dialog
    ([2e5d9a73f7](https://projects.blender.org/blender/blender/commit/2e5d9a73f7b9)).

![../../images/Security-alert-dialog-box.png](../../images/Security-alert-dialog-box.png
"../../images/Security-alert-dialog-box.png")

  - Improved layout of node group socket lists
    ([1d3b92bdea](https://projects.blender.org/blender/blender/commit/1d3b92bdeabc)).
  - New properties editor popover that contains settings for outliner to
    properties sync
    ([ffacce5be4](https://projects.blender.org/blender/blender/commit/ffacce5be41))
