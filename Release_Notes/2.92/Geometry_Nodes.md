# Blender 2.92: Geometry Nodes

A new node based system for creating and manipulating geometry is
available in Blender 2.92. The first release focuses on the groundwork,
and object scattering and instancing.

  - [User
    Manual](https://docs.blender.org/manual/en/2.92/modeling/geometry_nodes/index.html)
  - [Blog
    Post](https://code.blender.org/2020/12/everything-nodes-and-the-scattered-stone/)

![../../images/Pebble\_scattering.jpg](../../images/Pebble_scattering.jpg
"../../images/Pebble_scattering.jpg") *[Pebble scattering sample
file](https://download.blender.org/demo/geometry-nodes/pebble_scattering.blend)
by Simon Thommes.*

These are all the nodes available in this initial implementation of this
project:

  - Attribute

`Attribute Color Ramp`  
`Attribute Compare`  
`Attribute Fill`  
`Attribute Math`  
`Attribute Mix`  
`Attribute Randomize`  
`Attribute Vector Math`

  - Color

`ColorRamp`  
`Combine RGB`  
`Separate RGB`

  - Geometry

`Join Geometry`  
`Transform`

  - Input

`Object Info`  
`Random Float`  
`Value`  
`Vector`

  - Mesh

`Boolean`  
`Edge Split`  
`Subdivision Surface`  
`Triangulate`

  - Point

`Align Rotation to Vector`  
`Point Distribute`  
`Point Instance`  
`Point Rotate`  
`Point Scale`  
`Point Separate`  
`Point Translate`

  - Utilities

`Boolean Math`  
`Clamp`  
`Float Compare`  
`Map Range`  
`Math`

  - Vector

`Combine XYZ`  
`Separate XYZ`  
`Vector Math`

  - Group

`Make Group`  
`Ungroup`

  - Layout

`Frame`  
`Reroute`

''Example of a scene setup with geometry nodes used to scatter pebble
objects onto a modified plane.
''![600|center](../../images/Pebbles-scattering.jpg "600|center")
