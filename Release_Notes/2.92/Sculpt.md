# Blender 2.92: Sculpting

  - Sculpt: Grab silhouette option
    [383c20a6ab](https://projects.blender.org/blender/blender/commit/383c20a6abd).

![../../videos/2020-10-22\_19-14-59.mp4](../../videos/2020-10-22_19-14-59.mp4
"../../videos/2020-10-22_19-14-59.mp4")

  - Sculpt: Implement plane deformation falloff for Grab
    [c53b6067fa](https://projects.blender.org/blender/blender/commit/c53b6067fad).
  - Sculpt: Face Set Edit delete Geometry operation
    [c15bd1c4e6](https://projects.blender.org/blender/blender/commit/c15bd1c4e65).
  - Sculpt/Paint: Add Paint Studio Light preset
    [d6180dd2f7](https://projects.blender.org/blender/blender/commit/d6180dd2f7e).
  - UI: Add Sculpt Session info to stats
    [ea064133e5](https://projects.blender.org/blender/blender/commit/ea064133e53).
  - Sculpt: Allow inverting the Erase Displacement mesh filter
    [1bc75dfa4a](https://projects.blender.org/blender/blender/commit/1bc75dfa4a9).
  - Sculpt: Elastic deform type for Snake Hook
    [d870a60dd9](https://projects.blender.org/blender/blender/commit/d870a60dd92).
  - Sculpt: Multires Displacement Smear
    [d23894d3ef](https://projects.blender.org/blender/blender/commit/d23894d3ef3).
  - Sculpt: Fair Face Sets operation for Face Set Edit
    [c9b4d75336](https://projects.blender.org/blender/blender/commit/c9b4d753362).
