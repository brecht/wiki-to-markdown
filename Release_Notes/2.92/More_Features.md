# Blender 2.92: More Features

## Compositor

  - Keying node in compositor ensures result is properly premuiltiplied
    ([f68c3d557a](https://projects.blender.org/blender/blender/commit/f68c3d557aa))
    In practice this means that there is no need in manually adding
    Alpha Convert node after the keying one. The existing files might
    need to be adjusted in this regard: the alpha convert node is to be
    removed if it present.

<!-- end list -->

  - The Save as Render option was added to the File Output node. This
    feature allows to disable the use of the scene's color management
    options for each input socket individually.
    ([27b78c9c94](https://projects.blender.org/blender/blender/commit/27b78c9c94ba))

<!-- end list -->

  - New compositor node to adjust the exposure of images.
    ([6538f1e600](https://projects.blender.org/blender/blender/commit/6538f1e600ad))

![../../videos/0001-1322.mp4](../../videos/0001-1322.mp4
"../../videos/0001-1322.mp4") *Demo by [Carlo
Bergonzini](http://www.monorender.com/).*

## Compatibility

  - Pre-2.50.9 multires code has been removed
    ([d11e357824](https://projects.blender.org/blender/blender/commit/d11e357824d))
  - Removed Simple multires subdivision type
    ([17381c7b90](https://projects.blender.org/blender/blender/commit/17381c7b90e))
  - "Camera View Lock" is now in the object relations panel instead of
    the preferences
    ([00374fbde2](https://projects.blender.org/blender/blender/commit/00374fbde2bd))

## Motion tracking

  - Reorganize menus to make them consistent with the rest of Blender
    and expose items previously only visible in panels
    ([6c15b70279](https://projects.blender.org/blender/blender/commit/6c15b702796e))
  - Simplified configuration of intrinsics to refine
    ([0269f0c574](https://projects.blender.org/blender/blender/commit/0269f0c5745))
  - Moved optical center to lens settings panel
    ([2468635174](https://projects.blender.org/blender/blender/commit/24686351746))
  - Solved random crashes tracking multiple tracks
    ([29401d38d1](https://projects.blender.org/blender/blender/commit/29401d38d11))
  - Huge speedup of tracking multiple tracks
    ([5d13082622](https://projects.blender.org/blender/blender/commit/5d130826221))

## Sequencer

  - Media transform redesign
    ([e1665c3d31](https://projects.blender.org/blender/blender/commit/e1665c3d31))
  - Background rectangle option for text strip
    ([rB235c309e](https://projects.blender.org/blender/blender/commit/235c309e5f86))
  - Add Overlay popover panels
    ([rBfad80a95](https://projects.blender.org/blender/blender/commit/fad80a95fd08))
  - Paste strips after playhead by default
    ([rBdd9d12bf](https://projects.blender.org/blender/blender/commit/dd9d12bf45ed))
  - Move remove gaps operator logic to module code
    ([rB9e4a4c2e](https://projects.blender.org/blender/blender/commit/9e4a4c2e996c))
  - Hide cache settings and adjust defaults
    ([rBf448ff2a](https://projects.blender.org/blender/blender/commit/f448ff2afe7a))

## macOS

  - Follow system preference for natural trackpad scroll direction
    automatically, remove manual preference
    ([055ed33](https://projects.blender.org/blender/blender/commit/055ed33))
