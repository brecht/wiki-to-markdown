## Library Overrides

  - Added limited support for simulation caches on override data-blocks
    (caches need to be written on disk for now,
    [rB59910f72](https://projects.blender.org/blender/blender/commit/59910f72),
    [rB7e210e68](https://projects.blender.org/blender/blender/commit/7e210e68),
    [rB50ccf346](https://projects.blender.org/blender/blender/commit/50ccf346)).
  - Added initial support for NLA on override data-blocks (one can add
    new tracks to existing NLA system, and edit some properties of the
    existing tracks and strips
    ([rBc0bd240a](https://projects.blender.org/blender/blender/commit/c0bd240ad0a1)).
  - Some heavy data from override IDs do not get written to disk in the
    local .blend file anymore (mainly mesh geometry, shapekeys data, and
    embedded files currently, see
    [D9810](http://developer.blender.org/D9810),
    [rBf5a019ed](https://projects.blender.org/blender/blender/commit/f5a019ed43ab)).

## glTF 2.0

### Importer

  - Importer can now open Draco encoded files
    ([rBAeb29a12](https://projects.blender.org/blender/blender-addons/commit/eb29a12da48e))
  - Implement occlusion strength
    ([rBAd5c0d4b](https://projects.blender.org/blender/blender-addons/commit/d5c0d4b77c15))
  - Show specific error when buffer resource is missing
    ([rBA285deaf](https://projects.blender.org/blender/blender-addons/commit/285deaf22c27))
  - Removed future deprecated encoding arg to json.loads
    ([rBA48ec56b](https://projects.blender.org/blender/blender-addons/commit/48ec56bde5fc))
  - Create placeholder images for files that can't be loaded
    ([rBA59f6055](https://projects.blender.org/blender/blender-addons/commit/59f60554f18c))
  - Make NLA track order match glTF animation order
    ([rBAd7f8d9a](https://projects.blender.org/blender/blender-addons/commit/d7f8d9a4f919))
  - Use Separate R node for clearcoat textures
    ([rBAebdf186](https://projects.blender.org/blender/blender-addons/commit/ebdf1861dc56))

### Exporter

  - Fix export when texture is used only for alpha
    ([rBA5f2cb88](https://projects.blender.org/blender/blender-addons/commit/5f2cb885abb9))
  - Take care of active output node
    ([rBA5088c8d](https://projects.blender.org/blender/blender-addons/commit/5088c8d9d735))
  - Implement occlusion strength
    ([rBAd5c0d4b](https://projects.blender.org/blender/blender-addons/commit/d5c0d4b77c15))
  - Avoid crash when background exporting (without UI)
    ([rBAc5a84e2](https://projects.blender.org/blender/blender-addons/commit/c5a84e2e1ad0))
  - Fix texture filtering
    ([rBA9ff0d98](https://projects.blender.org/blender/blender-addons/commit/9ff0d983ee49))
  - Roundtrip all texture wrap modes
    ([rBA70efc48](https://projects.blender.org/blender/blender-addons/commit/70efc485eca8))
  - Various Draco encoder fixes
    ([rBAeb29a12](https://projects.blender.org/blender/blender-addons/commit/eb29a12da48e))
  - fix collection offset when rotated or scaled
    ([rBA3e7209f](https://projects.blender.org/blender/blender-addons/commit/3e7209f9f289))
  - More fixes for changing the filename when format changed
    ([rBA3ce41af](https://projects.blender.org/blender/blender-addons/commit/3ce41afdfa56))
