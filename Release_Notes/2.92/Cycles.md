# Blender 2.92: Cycles

## Performance

  - Multithreaded export of geometry, to improves performance in scene
    synchronization when there are many mesh, hair and volume objects.
    ([bb49aa0](https://projects.blender.org/blender/blender/commit/bb49aa0))

## GPU Rendering

  - GPU devices can now take over tiles that are currently being
    rendered by CPU threads to improve hybrid rendering performance.
    ([517ff40b12](https://projects.blender.org/blender/blender/commit/517ff40b124b))
  - NVIDIA OptiX
      - Hybrid rendering with CPU and GPU devices is now supported.
        ([bfb6fce659](https://projects.blender.org/blender/blender/commit/bfb6fce6594e))
      - AO and Bevel shader nodes are now supported.
        ([c10546f5e9](https://projects.blender.org/blender/blender/commit/c10546f5e9fe))
      - NVIDIA driver version must be 450 or newer, due to an upgrade to
        OptiX 7.1. This makes it possible to render more object
        instances.
  - Intel Iris and Xe GPUs can now be enabled for OpenCL rendering.
    ([f762d37790](https://projects.blender.org/blender/blender/commit/f762d37790b55352adc22a1d4c9e2b4953f0eac9))

## Volumes

  - Volume rendering is significantly more memory efficient, by using a
    sparse
    [NanoVDB](https://github.com/AcademySoftwareFoundation/openvdb/tree/feature/nanovdb/nanovdb)
    grid. Results are highly dependent on the volume shape. With one a
    production volume asset memory usage was reduced 3x, at the cost of
    10% longer render time.
    ([3df90de](https://projects.blender.org/blender/blender/commit/3df90de),
    [bd6bfba](https://projects.blender.org/blender/blender/commit/bd6bfba))

## Shaders

  - Attribute Node (new features shared with EEVEE):
      - Alpha output socket that returns the alpha channel of the
        attribute when available.
        ([9bc177d8de](https://projects.blender.org/blender/blender/commit/9bc177d8de))
      - Options to access built-in and custom properties of the object
        or mesh datablock, or of the instancer particle system or
        object. This makes it possible to add user controlled variation
        to materials on instances.
        ([6fdcca8de6](https://projects.blender.org/blender/blender/commit/6fdcca8de6))

## Baking

  - Baking to vertex colors is now supported, in addition to image
    textures.
    ([2221389](https://projects.blender.org/blender/blender/commit/2221389))
  - The baking panel is now enabled when OptiX is active (baking will
    execute with CUDA however).
    ([612b83bbd1](https://projects.blender.org/blender/blender/commit/612b83bbd183))
