# Blender 2.92: Modeling

## Tools

  - Primitive Add: new interactive tool to create primitives with two
    clicks.
    ([122cb1a](https://projects.blender.org/blender/blender/commit/122cb1a))

![Add object
tool](../../images/Add_object_with_objects_in_background.png
"Add object tool")

![Add object tool](../../videos/Add_object.mp4 "Add object tool")

## Modifiers

  - Operator to copy a single modifier to all selected objects
    ([6fbeb6e2e0](https://projects.blender.org/blender/blender/commit/6fbeb6e2e05408af448e))
  - The bevel modifier now uses the angle limit method by default
    ([6b5e4ad589](https://projects.blender.org/blender/blender/commit/6b5e4ad5899d87a183c4a3))
