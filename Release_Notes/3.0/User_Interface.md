# Blender 3.0: User Interface

## Theme and Widgets

  - To celebrate the beginning of a new series, the default theme got a
    refresh.
    ([bfec984cf8](https://projects.blender.org/blender/blender/commit/bfec984cf82a6))
  - Visual style update to panels.
    ([93544b641b](https://projects.blender.org/blender/blender/commit/93544b641bd6))
      - New theme setting for roundness.
      - Added margins to more clearly tell them apart.
      - Replace triangle with chevron icon.
  - *Menu Item* now makes use of *Roundness* theme setting.
    ([5183653951](https://projects.blender.org/blender/blender/commit/518365395152))
  - *List Item* no longer relies on *Regular* buttons style when
    selected.
    ([9e71a07547](https://projects.blender.org/blender/blender/commit/9e71a075473))
  - Active *Tab* text now use region's *Text Highlight* theme color when
    active.
    ([af26720b21](https://projects.blender.org/blender/blender/commit/af26720b2131))
  - Adjust editor's Header color when active (instead of when inactive).
    ([962b17b3ca](https://projects.blender.org/blender/blender/commit/962b17b3ca14))
  - Style drag-drop indicators as tooltips.
    ([499dbb626a](https://projects.blender.org/blender/blender/commit/499dbb626acb))
  - Use arrow icon on context paths.
    ([7c75529333](https://projects.blender.org/blender/blender/commit/7c755293330e))
  - Improve contrast on playhead.
    ([452c78757f](https://projects.blender.org/blender/blender/commit/452c78757f44))
  - In Animation Editors and VSE, align vertical indicators to view.
    ([3ccdee7532](https://projects.blender.org/blender/blender/commit/3ccdee75328b))
  - Use flat colors for NLA strips.
    ([4758a7d357](https://projects.blender.org/blender/blender/commit/4758a7d35750))
  - Remove separator lines between rows in VSE.
    ([b4af70563f](https://projects.blender.org/blender/blender/commit/b4af70563ff1))

## Area Management

  - Corner action zones allow joining any neighbors. Improved Header
    Context Menu.
    ([8b049e4c2a](https://projects.blender.org/blender/blender/commit/8b049e4c2a53),
    [c76141e425](https://projects.blender.org/blender/blender/commit/c76141e425aa)).

![../../images/AreaJoins.gif](../../images/AreaJoins.gif
"../../images/AreaJoins.gif")

  - New 'Area Close' operator.
    ([8b049e4c2a](https://projects.blender.org/blender/blender/commit/8b049e4c2a53),
    [06e62adfb8](https://projects.blender.org/blender/blender/commit/06e62adfb8f2)).

![../../images/CloseOperator.png](../../images/CloseOperator.png
"../../images/CloseOperator.png")

  - Mouse hit size for area resizing increased
    ([84dcf12ceb](https://projects.blender.org/blender/blender/commit/84dcf12ceb7f)).

![../../images/BorderHitSize.png](../../images/BorderHitSize.png
"../../images/BorderHitSize.png")

  - Improved mouse cursor feedback during Area Split and Join at
    unsupported locations.
    ([3b1a16833b](https://projects.blender.org/blender/blender/commit/3b1a16833b58)).
  - Resizing areas now snap (with Ctrl) to more consistent locations.
    ([e5ed9991ea](https://projects.blender.org/blender/blender/commit/e5ed9991eaf0)).

## General

  - The editor header and tool settings region were swapped
    ([4cf4bb2664](https://projects.blender.org/blender/blender/commit/4cf4bb2664ebe)).
  - If saving a file using CloseSave dialog, do not close if there is an
    error doing so, like if read-only.
    ([cfa20ff03b](https://projects.blender.org/blender/blender/commit/cfa20ff03bde)).
  - Capture object thumbnails at an oblique angle for better preview of
    the shapes.
    ([ecc7a83798](https://projects.blender.org/blender/blender/commit/ecc7a837982e)).

![../../images/PreviewOrientation.png](../../images/PreviewOrientation.png
"../../images/PreviewOrientation.png")

  - Do not create thumbnail previews of offline files, to avoid slow
    listings of Windows OneDrive folders.
    ([ee5ad46a0e](https://projects.blender.org/blender/blender/commit/ee5ad46a0ead)).

![../../images/OfflineFiles.png](../../images/OfflineFiles.png
"../../images/OfflineFiles.png")

  - Fix console window briefly flashing when blender starts on windows.
    ([f3944cf503](https://projects.blender.org/blender/blender/commit/f3944cf50396)).
  - Improved positioning of menu mnemonic underlines, especially when
    using custom font.
    ([0fcc063fd9](https://projects.blender.org/blender/blender/commit/0fcc063fd99c),
    [aee04d4960](https://projects.blender.org/blender/blender/commit/aee04d496035)).
  - "Render" Window now top-level (not child) on Windows platform, back
    to behavior prior to 2.93.
    ([bd87ba90e6](https://projects.blender.org/blender/blender/commit/bd87ba90e639)).
  - Improved placement of child windows when using multiple monitors
    with any above any others (Win32).
    ([d75e45d10c](https://projects.blender.org/blender/blender/commit/d75e45d10cbf)).
  - Windows users can now associate current installation with Blend
    files in Preferences.
    ([bcff0ef9ca](https://projects.blender.org/blender/blender/commit/bcff0ef9cabc)).

![../../images/Blend\_File\_Association.png](../../images/Blend_File_Association.png
"../../images/Blend_File_Association.png")

  - 3DView Statistics Overlay now shows Local statistics if you are in
    Local View
    ([c8e331f450](https://projects.blender.org/blender/blender/commit/c8e331f45003)).
  - Improved scaling of File Browser preview images.
    ([cb548329ea](https://projects.blender.org/blender/blender/commit/cb548329eaea)).

![../../images/Scaling2.png](../../images/Scaling2.png
"../../images/Scaling2.png")

  - Do not resize temporary windows (Preferences, Render, etc) if they
    are already open.
    ([643720f8ab](https://projects.blender.org/blender/blender/commit/643720f8abdb)).
  - Win32: Improved placement of windows when using multiple monitors
    that differ in DPI and scale.
    ([999f1f7504](https://projects.blender.org/blender/blender/commit/999f1f75045c)).
  - macOS: support for Japanese, Chinese and Korean input in text fields
    ([83e2f8c](https://projects.blender.org/blender/blender/commit/83e2f8c),
    [0ef794b](https://projects.blender.org/blender/blender/commit/0ef794b))
  - Ctrl+F while hovering UI lists will open the search for the list
    ([rB87c1c811](https://projects.blender.org/blender/blender/commit/87c1c8112fa4)).
  - Text buttons are automatically scrolled into view when editing them
    ([rB0c83ef56](https://projects.blender.org/blender/blender/commit/0c83ef567c50)).
  - Show descriptive display names for Fonts in File Browser instead of
    simple file names.
    ([8aa1c0a326](https://projects.blender.org/blender/blender/commit/8aa1c0a326a8)).

![../../images/FontDisplayNames.png](../../images/FontDisplayNames.png
"../../images/FontDisplayNames.png")

  - Show descriptive display names for 3D Text Object fonts.
    ([b5bfb5f34c](https://projects.blender.org/blender/blender/commit/b5bfb5f34c12)).
  - On Windows when entering Chinese or Japanese characters, do not
    duplicate initial keystroke.
    ([836aeebf70](https://projects.blender.org/blender/blender/commit/836aeebf7077)).
  - Allow use of Wingdings and Symbol fonts in VSE text strips.
    ([ae920d789e](https://projects.blender.org/blender/blender/commit/ae920d789ed3)).
  - Substantial speed increases for interface text drawing.
    ([d5261e973b](https://projects.blender.org/blender/blender/commit/d5261e973b56),
    [0d7aab2375](https://projects.blender.org/blender/blender/commit/0d7aab2375e6))
  - Less jiggling of contents when moving nodes.
    ([400605c3a6](https://projects.blender.org/blender/blender/commit/400605c3a6a8)).
  - Increased resolution of blend file thumbnails.
    ([bf0ac711fd](https://projects.blender.org/blender/blender/commit/bf0ac711fde2)).

![../../images/BlendThumbnailSizeComparison.png](../../images/BlendThumbnailSizeComparison.png
"../../images/BlendThumbnailSizeComparison.png")

  - Improved blend preview thumbs, using screen captures or rendered
    with current current view shading.
    ([58632a7f3c](https://projects.blender.org/blender/blender/commit/58632a7f3c0f),
    [4fa0bbb5ac](https://projects.blender.org/blender/blender/commit/4fa0bbb5ac3d)).

![../../images/BlendPreviews.png](../../images/BlendPreviews.png
"../../images/BlendPreviews.png")

  - Split Output Properties Dimensions panel.
    ([4ddad5a7ee](https://projects.blender.org/blender/blender/commit/4ddad5a7ee5))
  - File Browser "Favorites" section heading changed back to "Bookmarks"
    ([a79c33e8f8](https://projects.blender.org/blender/blender/commit/a79c33e8f8f3))

## Custom Properties

  - Rework the custom property edit operator to make it faster to use
    and more intuitive
    ([bf948b2cef](https://projects.blender.org/blender/blender/commit/bf948b2cef3ba3)).

![Editing metadata from an array property with the new edit custom
property operator
UI.](../../images/Reworked_Custom_Property_Edit_Operator_2.png
"Editing metadata from an array property with the new edit custom property operator UI.")

  - Improve the layout of custom property edit panel
    [972677b25e](https://projects.blender.org/blender/blender/commit/972677b25e1d84)

## 3D Viewport

  - Display indicator in *Text Info* overlay when *Clipping Region* is
    enabled
    ([9c509a7219](https://projects.blender.org/blender/blender/commit/9c509a721949))
  - Z axis lock option for the walk navigation
    ([c749c24682](https://projects.blender.org/blender/blender/commit/c749c24682159))
  - Navigation gizmos no longer hide when using modal operators
    ([917a972b56](https://projects.blender.org/blender/blender/commit/917a972b56af10)).
  - The different outline color for selected instances has been removed,
    improving readability of the active object
    ([aa13c4b386](https://projects.blender.org/blender/blender/commit/aa13c4b386b131)).

## Keymap

  - Holding the \`D\` & dragging now opens the view menu (moved from the
    \`Tilde\` key). The "Switch to Object" operator is now mapped to the
    \`Tilde\` key.
    ([f92f5d1ac6](https://projects.blender.org/blender/blender/commit/f92f5d1ac62c66ceb7a6ac1ff69084fbd5e3614a)).

## Outliner

  - Filter option "All View Layers" shows all the view layers for
    comparing different collection values
    ([bb2648ebf0](https://projects.blender.org/blender/blender/commit/bb2648ebf002)).

![../../images/Outliner\_view\_layers.png](../../images/Outliner_view_layers.png
"../../images/Outliner_view_layers.png")

## Freestyle

A major rework of the UI layout to make the layout align with the
current UI design principles
([6f52ebba19](https://projects.blender.org/blender/blender/commit/6f52ebba192)).

## Preview

  - Sphere material preview updated to have squared UV and better poles
    ([875f24352a](https://projects.blender.org/blender/blender/commit/875f24352a76)).

![Before](../../images/Sphere_preview_-_before.png "Before")
![|thumb|After](../../images/Sphere_preview_-_after.png "|thumb|After")
