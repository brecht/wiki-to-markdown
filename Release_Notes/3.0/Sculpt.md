# Blender 3.0: Sculpt

## Transfer Mode

Transfer mode tool now has a nicer flash animation and a new hotkey,
ALT-Q. It also now works in more modes.

![Demo of transferring sculpt mode between separate objects. Recorded by
Tonatiuh de San Julián](../../videos/Vokoscreen-2021-12-07_10-56-51.mp4
"Demo of transferring sculpt mode between separate objects. Recorded by Tonatiuh de San Julián")

See also:

  - Commit
    [7bc5246156](https://projects.blender.org/blender/blender/commit/7bc5246156e0eba3992317f8ca1b4642de324689)
  - Commit
    [c614eadb47](https://projects.blender.org/blender/blender/commit/c614eadb479e43258586af16deef6bd7d873ac59)

## Other Changes

  - The Voxel Remesher shading mode is now automatic depending on the
    object's flat/smooth shading option.
    [ef7fcaf8e6](https://projects.blender.org/blender/blender/commit/ef7fcaf8e6c6).
  - Texture Paint: better syncing between texture slots and editors.
    ([56005ef499](https://projects.blender.org/blender/blender/commit/56005ef499894bcd48313e3df7ec965fe516c1dc),
    [3b2a01edf6](https://projects.blender.org/blender/blender/commit/3b2a01edf698fb27d9984fe12eb210df5246f16c),
    [b0cb0a7854](https://projects.blender.org/blender/blender/commit/b0cb0a78547582848f5f18f5d5cc85c69f3dbe30))
  - Texture Paint: Stencil now uses the assigned UV Layer properly.
    [f11bcb5a80](https://projects.blender.org/blender/blender/commit/f11bcb5a80eb106560678c8c60be32ad997d6641).
  - Weight Paint Gradient tool now has its falloff exposed.
    [f8a0e102cf](https://projects.blender.org/blender/blender/commit/f8a0e102cf5e33f92447fe6cd6db7838f6051aab).
  - Improved Multires performance
    [482465e18a](https://projects.blender.org/blender/blender/commit/482465e18aa7c9f74fcb90ec1002f157a580e240).
