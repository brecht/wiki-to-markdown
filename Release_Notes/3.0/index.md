# Blender 3.0 Release Notes

Blender 3.0 was released on December 3, 2021.

Check out the final [release notes on
blender.org](https://www.blender.org/download/releases/3-0).

## [Animation & Rigging](Animation_Rigging.md)

## [Asset Browser](Asset_Browser.md)

## [Core](Core.md)

## [EEVEE & Viewport](EEVEE.md)

## [Grease Pencil](Grease_Pencil.md)

## [Modeling](Modeling.md)

## [Nodes & Physics](Nodes_Physics.md)

## [Pipeline, Assets & I/O](Pipeline_Assets_IO.md)

## [Python API & Text Editor](Python_API.md)

## [Render & Cycles](Cycles.md)

## [Sculpt, Paint, Texture](Sculpt.md)

## [User Interface](User_Interface.md)

## [VFX & Video](VFX.md)

## [Virtual Reality](Virtual_Reality.md)

## [Add-ons](Add-ons.md)

## Compatibility

See the sections
[Nodes](Nodes_Physics.md),
[Add-ons](Add-ons.md),
[Cycles](Cycles.md),
[Core](Core.md),
[Animation\_Rigging](Animation_Rigging.md)
and [Python API](Python_API.md).

## [Corrective Releases](Corrective_Releases.md)
