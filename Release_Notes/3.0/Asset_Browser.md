# Blender 3.0: Asset Browser

![../../images/Release\_notes\_asset\_browser\_banner.png](../../images/Release_notes_asset_browser_banner.png
"../../images/Release_notes_asset_browser_banner.png")

Blender 3.0 includes a new editor: The Asset Browser. It makes working
with reusable assets much easier and more visual. Supported asset types
in 3.0 are:

  - Objects
  - Materials
  - Poses
  - Worlds

More asset types will be supported in future.

  

## Quick Start

The following gives a quick overview on how to use asset functionality.

### Regular Data-Blocks

For objects, materials and worlds, the following sections explain the
workflow. For pose assets, the workflow differs a bit and is explained
below.

#### Creating Assets

The *Mark as Asset* operator makes data-blocks available for easy reuse
via the Asset Browser. It can be found in multiple places:

  - Context menu [in the
    Outliner](https://docs.blender.org/manual/en/latest/editors/outliner/editing.html#context-menu).
    Allows marking multiple data-blocks as assets at once.
  - Context menu of the [material
    slots](https://docs.blender.org/manual/en/latest/render/materials/assignment.html#material-slots)
    list.
  - For objects in the 3D View: *Object* \> '' Asset'' \> *Mark as
    Asset*.

**Save the file\!** Otherwise your asset edits won't be available to
other files.

#### Register an Asset Library

![../../images/Release\_notes\_asset\_browser\_library\_preferences.png](../../images/Release_notes_asset_browser_library_preferences.png
"../../images/Release_notes_asset_browser_library_preferences.png")

Save the file into a directory that you want to use as your asset
library. This directory then has to be registered as an asset library in
the Preferences: *Preferences* \> *File Paths* \> *Asset Libraries*.

  

#### Using Assets

Assets can simply be dragged into the 3D View, and in some instances,
into other editors. Objects will snap to the surface under the cursor or
to the grid. Materials will be applied to the material slot under the
cursor.

#### Editing Asset Metadata

![../../images/Release\_notes\_asset\_browser\_sidebar.png](../../images/Release_notes_asset_browser_sidebar.png
"../../images/Release_notes_asset_browser_sidebar.png")

The Asset Browser has an *Asset Details* sidebar in which the metadata
of assets can be edited **if the asset is stored in the current file**.
Such assets are displayed with a special icon:

![Icon indicating that the asset is stored in the current .blend file
and can be
edited.](../../images/Release_notes_asset_browser_current_file_icon.png
"Icon indicating that the asset is stored in the current .blend file and can be edited.")

  

### Poses

The Pose Library system has been revamped to make use of the new Asset
Browser. [Read more in the
manual](https://docs.blender.org/manual/en/3.0/animation/armatures/posing/editing/pose_library.html).

![../../images/Release\_notes\_asset\_browser\_poses.png](../../images/Release_notes_asset_browser_poses.png
"../../images/Release_notes_asset_browser_poses.png")

  

## More Documentation

All Asset Browser related functionality is documented in the
[manual](https://docs.blender.org/manual/en/latest/index.html).

  - [Asset
    Browser](https://docs.blender.org/manual/en/latest/editors/asset_browser.html)
  - [Asset Libraries and Asset
    Catalogs](https://docs.blender.org/manual/en/latest/files/asset_libraries/index.html)
  - [Asset Library
    Registration](https://docs.blender.org/manual/en/latest/editors/preferences/file_paths.html#asset-libraries)
  - [Pose
    Library](https://docs.blender.org/manual/en/latest/animation/armatures/posing/editing/pose_library.html)
    (based on the Asset Browser)
