# Blender 3.0: Core

## Blend file read & Write

<table>
<tbody>
<tr class="odd">
<td><div class="note_title">
<p><strong>Forward Incompatibility Breakage</strong></p>
</div>
<div class="note_content">
<p>Blend files saved in Blender 3.0 with compression enabled won't load in older versions of Blender.</p>
</div></td>
</tr>
</tbody>
</table>

  - The speed when opening a blend file linking many (thousands or more)
    data-blocks from a same library has been greatly improved
    ([\#89194](http://developer.blender.org/T89194),
    [D11757](http://developer.blender.org/D11757),
    [rBdb4fe8e3](https://projects.blender.org/blender/blender/commit/db4fe8e3)).

<!-- end list -->

  - Loading .blend files compressed using the Zstandard algorithm is now
    supported and saving files with compression enabled uses Zstandard
    instead of gzip ([D5799](http://developer.blender.org/D5799),
    [rB2ea66af7](https://projects.blender.org/blender/blender/commit/2ea66af7)).
    This improves loading and saving speed while producing files of
    similar size:

| Saving time  | Uncompressed | gzip   | Zstd  | Change gzip to Zstd |
| ------------ | ------------ | ------ | ----- | ------------------- |
| 2.81 splash  | 0.12s        | 9.30s  | 0.46s | \-95%               |
| 2.92 splash  | 0.12s        | 11.03s | 0.82s | \-94%               |
| Ember Forest | 0.05s        | 2.57s  | 0.19s | \-92%               |
| Mr. Elephant | 0.06s        | 2.19s  | 0.23s | \-89%               |
| Loading time | Uncompressed | gzip   | Zstd  | Change gzip to Zstd |
| 2.81 splash  | 0.23s        | 1.36s  | 0.33s | \-75%               |
| 2.92 splash  | 0.22s        | 2.23s  | 0.82s | \-63%               |
| Ember Forest | 0.09s        | 0.58s  | 0.19s | \-66%               |
| Mr. Elephant | 0.09s        | 0.59s  | 0.19s | \-66%               |
| File size    | Uncompressed | gzip   | Zstd  | Change gzip to Zstd |
| 2.81 splash  | 386M         | 334M   | 334M  | 0%                  |
| 2.92 splash  | 380M         | 291M   | 284M  | \-2%                |
| Ember Forest | 105M         | 71M    | 65M   | \-8%                |
| Mr. Elephant | 112M         | 66M    | 63M   | \-5%                |

  - \`zstandard\` package was added to bundled python to enable scripts
    to manipulate zstd-compressed .blend files
    ([rBa5917175](https://projects.blender.org/blender/blender/commit/a5917175d8)).

## Collections & View Layers

  - The process re-syncing Layer collections in View layers to match
    with current Collections hierarchy in the scene has been enhanced to
    enable re-using as much as possible exiting layers. This often
    avoids losing layer-specific settings (like the Exclude flag) when
    re-organizing the collections, and allows for future improvements
    like adding custom properties to layers
    ([D12016](http://developer.blender.org/D12016),
    [rBb18d0244](https://projects.blender.org/blender/blender/commit/b18d0244),
    [rB3db37075](https://projects.blender.org/blender/blender/commit/3db37075)).

## Library Overrides

  - Some long-standing technical debt was addressed in how insertion of
    local modifiers or collections is handled in local overrides of
    objects. See [\#82160](http://developer.blender.org/T82160) for
    details ([D13222](http://developer.blender.org/D13222),
    [rBfa6a913e](https://projects.blender.org/blender/blender/commit/fa6a913ef19c),
    [rBd6ea881a](https://projects.blender.org/blender/blender/commit/d6ea881a741a),
    [rB33c5e7bc](https://projects.blender.org/blender/blender/commit/33c5e7bcd5e5),
    [rBec71054a](https://projects.blender.org/blender/blender/commit/ec71054a9b7b)).

<table>
<tbody>
<tr class="odd">
<td><div class="note_title">
<p><strong>Forward Incompatibility Breakage</strong></p>
</div>
<div class="note_content">
<p>This means that loading blend files saved in Blender 3.0 in an older version of Blender will loose data (namely, inserted local modifiers, constraints etc. may be lost or misplaced).</p>
</div></td>
</tr>
</tbody>
</table>

  - Basic RNA API has been extended and should now allow all necessary
    operations (see [\#86656](http://developer.blender.org/T86656) for
    details).
  - Lots of polishing and fixes was done, in particular in the
    (auto-)resync process, and to enhance the support of complicated
    features like point caches.

## Proxy Removal

Proxies are being removed from Blender in the 3.x series. For 3.0 this
process includes:

`* Removal of operators to create proxies from linked data-blocks.`  
`* Automatic conversion of proxies to library overrides on file load.`

Proxy evaluation code remains for the time being, and it is still
possible to skip automatic conversion of proxies in existing .blend
files by disabling the \`Proxy to Override Auto Conversion\` user
preferences Experimental setting.

See also [\#91671](http://developer.blender.org/T91671).

## Compatibility

  - Vertex group names are now stored in the meshes directly instead of
    objects.
    ([rB3b6ee8ce](https://projects.blender.org/blender/blender/commit/3b6ee8cee708),
    [rBfc32567c](https://projects.blender.org/blender/blender/commit/fc32567cdaa5))
      - It is impossible to have a different list of names in different
        objects using the same mesh.
      - When opening an older file only one of the available name lists
        would be chosen for each mesh (preferring the longest), and
        others are discarded.
      - When linking a mesh object from 2.93 in 3.0, if the object is
        made local but the mesh remains linked, Blender will not be able
        to read the mesh's vertex groups. See
        [\#93778](http://developer.blender.org/T93778) for more details.
      - A method of using different vertex group names to reference
        different bones on multiple objects that use the same mesh will
        no longer work. See
        [\#T94634](http://developer.blender.org/TT94634) for more
        details.
