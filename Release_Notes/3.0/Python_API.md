# Blender 3.0: Python API

## Bundled Python

  - \`zstandard\` package was added to bundled python to enable scripts
    to manipulate zstd-compressed .blend files
    ([rBa5917175](https://projects.blender.org/blender/blender/commit/a5917175),
    see also the [.blend file read/write release
    notes](Core.md#Blend_file_read_.26_Write)).

## Text Editor

  - The "Register" option to run Python scripts on startup no longer
    requires a \`.py\` extension in the name
    ([3e775a4fc5](https://projects.blender.org/blender/blender/commit/3e775a4fc57cfd48954adcf284354312f34d5412)).

## Other Additions

  - Added multi-dimensional array support for \`bpy.props\` vector types
    (\`FloatVectorProperty\`, \`IntVectorProperty\`,
    \`BoolVectorProperty\`).
    ([bc0a7d3fae](https://projects.blender.org/blender/blender/commit/bc0a7d3fae5cfbe76ff84b76cb0ce48fe46adea5)).

<!-- end list -->

  -   
    Example that defines a 4x4 matrix property:
    ``` python
    bpy.props.FloatVectorProperty(size=(4, 4), subtype='MATRIX')
    ```

<!-- end list -->

  - VSE: Add Sequence.parent\_meta() python API function
    ([eec1ea0ccf](https://projects.blender.org/blender/blender/commit/eec1ea0ccf2))
  - In a File Browser, a list of selected files can be queried via
    \`bpy.context.selected\_files\`
    ([7ec839adfa](https://projects.blender.org/blender/blender/commit/7ec839adfa99)).
  - In a File Browser, \`FileSelectEntry.relative\_path\` gives a file's
    path (including the file name) relative to the currently displayed
    directory. This becomes relevant when the File Browser is set to
    show multiple levels of recursion. Multiple files with the same name
    may be visible then. So do not use the file name to reference file,
    use \`FileSelectEntry.relative\_path\` instead.
    ([79281336c0](https://projects.blender.org/blender/blender/commit/79281336c055))
  - \`mathutils.{Vector/Matrix/Euler/Color/Quaternion}\` now have an
    \`is\_valid\` attribute which can be used to check if their owner is
    still valid
    ([0950cfd9d5](https://projects.blender.org/blender/blender/commit/0950cfd9d53ef666cf820765e83586f72b04e9b6)).
  - Area maintenance "Close" can now be scripted, eg:
    \`bpy.ops.screen.area\_close({"area":
    bpy.context.screen.areas\[0\]})\`.
    ([9290b41381](https://projects.blender.org/blender/blender/commit/9290b41381fd))
  - UI: expose additional mouse cursors \[\`PICK\_AREA\`, \`STOP\`,
    \`COPY\`, \`CROSS\`, \`MUTE\`, \`ZOOM\_IN\`, \`ZOOM\_OUT\`\]
    ([6bf8c95e52](https://projects.blender.org/blender/blender/commit/6bf8c95e521d6effe9e1c426e14efe20dac81175)).
  - UI: new operator setting for \`bl\_options\` called
    \`DEPENDS\_ON\_CURSOR\` to be used so operators wait for input when
    accessed from menus
    ([da2ba40268](https://projects.blender.org/blender/blender/commit/da2ba402689d7ed752b0b37ad49dce1a5851cc77)).
  - UI: support setting the cursor when \`DEPENDS\_ON\_CURSOR\` is used
    via \`OperatorType.bl\_cursor\_pending\`
    ([2a8e5128c1](https://projects.blender.org/blender/blender/commit/2a8e5128c16c17a7b2f6fc5325dc8f5abb4427d4)).

## Other Changes

  - Added functions to ensure and clear an RNA struct's IDProperties: \`
    id\_properties\_clear()\` and \`id\_properties\_ensure()\`
    ([c202d38659](https://projects.blender.org/blender/blender/commit/c202d3865904)).
  - Added \`Operator.poll\_message\_set(message, ...)\`  
    Operators can now set a message for why an operators poll function
    fails
    ([ebe04bd3ca](https://projects.blender.org/blender/blender/commit/ebe04bd3cafaa1f88bd51eee5b3e7bef38ae69bc)).
  - New \`bpy.app.handlers.xr\_session\_start\_pre\` handler that
    executes when starting a virtual reality session. Main use-case is
    to pass a VR controller action map (the XR equivalent of a key-map)
    to the session
    ([rBcb12fb78](https://projects.blender.org/blender/blender/commit/cb12fb78cad4)).
  - New \`XrSessionSettings.use\_absolute\_tracking\` property that,
    when \`true\`, allows the VR tracking origin to be defined
    independently of the headset position
    ([rB36c0649d](https://projects.blender.org/blender/blender/commit/36c0649d32aa)).
  - New \`XrSessionSettings.base\_scale\` property that defines the VR
    viewer's reference scale
    ([rB3434a991](https://projects.blender.org/blender/blender/commit/3434a991ec5b)).
  - New \`XrSessionSettings\` properties for VR overlays
    (\`show\_selection\`, \`show\_controllers\`,
    \`show\_custom\_overlays\`) and controller visualization
    (\`controller\_draw\_style\`)
    ([rB9dda6545](https://projects.blender.org/blender/blender/commit/9dda65455b54)).
  - New \`XrSessionState\` methods for VR actions
    ([e844e9e8f3](https://projects.blender.org/blender/blender/commit/e844e9e8f3bb)).
      - Action creation:\`action\_set\_create()\`, \`action\_create()\`,
        \`action\_binding\_create()\`, \`active\_action\_set\_set()\`,
        \`controller\_pose\_actions\_set()\`
      - Action querying:\`action\_state\_get()\`
      - Haptics:\`haptic\_action\_apply()\`,\`haptic\_action\_stop()\`
      - Controller poses:\`controller\_grip\_location\_get()\`,
        \`controller\_grip\_rotation\_get()\`,
        \`controller\_aim\_location\_get()\`,
        \`controller\_aim\_rotation\_get()\`
  - New \`XrSessionState.actionmaps\` collection property to store
    default VR action maps. Main use-case is to load default action maps
    from a Python script into this collection and then create actions
    for the session during the \`xr\_session\_start\_pre\` handler
    ([e844e9e8f3](https://projects.blender.org/blender/blender/commit/e844e9e8f3bb)).
  - New \`XrSessionState.navigation\_location/rotation/scale\`
    properties that allow the VR viewer to navigate the viewport by
    adding these deltas to the viewer's base pose/scale
    ([rB3434a991](https://projects.blender.org/blender/blender/commit/3434a991ec5b)).
  - New \`XR\_ACTION\` event type and \`Event.xr\` property for
    accessing XR event data such as input states and controller poses
    ([rBcdeb5060](https://projects.blender.org/blender/blender/commit/cdeb506008e9)).
  - New \`XR\` region type for drawing to the VR viewport (headset
    display) and mirror region. Add-ons can use
    \`Space.draw\_handler\_add()\` with region type \`XR\` and draw type
    \`POST\_VIEW\` for VR custom drawing
    ([rB9dda6545](https://projects.blender.org/blender/blender/commit/9dda65455b54)).
  - New \`Matrix.LocRotScale\` constructor for combining all
    transformation channels into a matrix in one call.
    ([rBa86e815d](https://projects.blender.org/blender/blender/commit/a86e815dd86)).

## Breaking Changes

  - Replaced \`PoseBone.custom\_shape\_scale\` scalar with a
    \`PoseBone.custom\_shape\_scale\_xyz\` vector; in addition,
    \`custom\_shape\_translation\` and
    \`custom\_shape\_rotation\_euler\` were added.
    ([rBfc5bf09f](https://projects.blender.org/blender/blender/commit/fc5bf09fd88c))
  - Renamed \`bbone\_curveiny\`/\`bbone\_curveouty\` to
    \`bbone\_curveinz\`/\`bbone\_curveoutz\`. Combined scale channels
    into \`bbone\_scalein.x\`/\`y\`/\`z\` and
    \`bbone\_scaleout.x\`/\`y\`/\`z\` vectors, with original \`y\` also
    becoming \`z\` to make space for an actual Y scale channel.
    ([rB682a74e0](https://projects.blender.org/blender/blender/commit/682a74e0909))
  - Remove unused \`BMesh.from\_object\` deform argument
    ([ead084b6e1](https://projects.blender.org/blender/blender/commit/ead084b6e1a05b9bff943660ea104efbed9b845f)).
  - Remove \`context.active\_base\`
    ([44db4e50b2](https://projects.blender.org/blender/blender/commit/44db4e50b245041ea250bef4e525e658e83403cb)).
  - Remove use of \`wiki\_url\` for add-ons \`bl\_info\` (use "doc\_url"
    instead)
    ([aaa07a3a8f](https://projects.blender.org/blender/blender/commit/aaa07a3a8f6427a01ef1ffacc0733e82921b0a8a)).
  - Remove \`IDPropertyGroup.iteritems\`, now \`keys\`, \`values\` &
    \`items\` all use iterators
    ([B265d97556](https://projects.blender.org/blender/blender/commit/B265d97556aa0f0f2a0e4dd7584e3b8573bbddd54)).
  - Some RNA accessors were **creating** data, which is very bad on
    design level for several reasons, the following were changed:
      - Force fields and collision settings (\`Object.field\`,
        \`Object.collision\`, \`ParticleSettings.force\_field\_1\` and
        \`ParticleSettings.force\_field\_2\`). Object ones can be
        \`None\` and need to be added explicitly now (\`Collision\`
        modifier and \`forcefield\_toggle\` operator), particle ones are
        always generated together with particles settings.
      - ID preview can be \`None\`, can use the new \`preview\_ensure\`
        function first then.
  - Remove \`bpy.app.binary\_path\_python\`
    ([90b0fb135f](https://projects.blender.org/blender/blender/commit/90b0fb135fdd06055d42f22396120e812440122d))
    (use \`sys.executable\` instead)
  - Replace \`UI\_EMBOSS\_NONE\_OR\_STATUS\` with \`NONE\_OR\_STATUS\`
    in \`UILayout.emboss\`
    ([0afe4e81cb](https://projects.blender.org/blender/blender/commit/0afe4e81cbb2)).
  - Remove non-functional \`renderable\_only\` arg from
    \`bpy.ops.wm.alembic\_export\`
    ([834e87af7b](https://projects.blender.org/blender/blender/commit/834e87af7bbf)).
  - Move \`preferences.view.show\_layout\_ui\` to
    \`preferences.apps.show\_corner\_split\`
    ([9fee59a484](https://projects.blender.org/blender/blender/commit/9fee59a4849)).
  - Rename \`bpy.types.TOPBAR\_MT\_app\` and
    \`bpy.types.TOPBAR\_MT\_app\_system\` to
    \`bpy.types.TOPBAR\_MT\_blender\` and
    \`bpy.types.TOPBAR\_MT\_blender\_system\`
    ([58043c0637](https://projects.blender.org/blender/blender/commit/58043c0637f8)).
  - Remove icon \`SMALL\_TRI\_RIGHT\_VEC\` (use
    \`DISCLOSURE\_TRI\_RIGHT\` for a similar icon).
    ([368d794407](https://projects.blender.org/blender/blender/commit/368d7944073e)).

### IDProperty UI Data API

\`IDProperty\` UI data storage was refactored to remove the special
\`\_RNA\_UI\` subgroup, adding a proper manager for a property's UI data
([8b9a3b94fc](https://projects.blender.org/blender/blender/commit/8b9a3b94fc148d)).

<File:IDProperty> UI Data Before 3.0.png|IDProperty UI data before 3.0.
UI data is stored in the "\_RNA\_UI" subgroup IDProperty UI data after
3.0.png|UI data is stored directly in the property now.

  - Python objects with custom properties now have an
    \`id\_properties\_ui(prop\_name)\` method, which returns a manager
    for the UI data, with the following methods:
      - \`update\`: Update UI data values, like \`min=1.0\` or
        \`default="new\_default"\`
      - \`as\_dict\`: Returns a dictionary containing the UI data values
      - \`clear\`: Removes the property's UI data.
      - \`update\_from\`: Copy UI data between properties, even if they
        have different owners.
  - **Note**: code that simply wants to create a property with UI data
    is advised to switch to the \`rna\_idprop\_ui\_create\` utility
    function from the \`rna\_prop\_ui\` module, which has existed since
    2.80 and was transparently updated to preserve compatibility through
    this change to the internals.

**Supported UI Data**

| Property Type | Min | Max | Soft Min | Soft Min | Step | Precision | Default        | Subtype | Description |
| ------------- | --- | --- | -------- | -------- | ---- | --------- | -------------- | ------- | ----------- |
| Float         | X   | X   | X        | X        | X    | X         | X (and arrays) | X       | X           |
| Integer       | X   | X   | X        | X        | X    | X         | X (and arrays) | X       | X           |
| String        |     |     |          |          |      |           | X              | X       | X           |
| ID            |     |     |          |          |      |           |                | X       | X           |

### Geometry Instancing

Previously, objects were the smallest unit of instancing. Now,
object-data, such as meshes, can be instanced without creating an object
with that data first.
([5a9a16334c](https://projects.blender.org/blender/blender/commit/5a9a16334c573c))

The API for accessing instances did not change, one just has to be more
careful now.

``` python
import bpy
depsgraph = bpy.context.view_layer.depsgraph
for object_instance in depsgraph.object_instances:
    if object_instance.is_instance:
        print(object_instance.object.data, object_instance.instance_object.data)
```

Previously, \`.object.data\` and \`.instance\_object.data\` were always
the same. Now they can be different and it is correct to use
\`.object.data\`.

  - \`object\_instance.object\`: A temporary object wrapping the
    instance. References to this object must not be stored between loop
    iterations.
  - \`object\_instance.object.data\`: Actual object-data/geometry of the
    instance. This data should be rendered/exported. It can be stored
    between loop iterations and is still valid after the loop.
  - \`object\_instance.matrix\_world\`: Transformation matrix of the
    instance.
  - \`object\_instance.instance\_object\`: Object that created and owns
    the instance.
  - \`object\_instance.instance\_object.data\`: Data of the object that
    created the instance. This should probably never be used now.

### Use Keyword Only Arguments

  - addon\_utils.module\_bl\_info 2nd arg \`info\_basis\`.
  - addon\_utils.modules 1st \`module\_cache\`, 2nd arg \`refresh\`.
  - addon\_utils.modules\_refresh 1st arg \`module\_cache\`.
  - bl\_app\_template\_utils.activate 1nd arg \`template\_id\`.
  - bl\_app\_template\_utils.import\_from\_id 2nd arg
    \`ignore\_not\_found\`.
  - bl\_app\_template\_utils.import\_from\_path 2nd arg
    \`ignore\_not\_found\`.
  - bl\_keymap\_utils.keymap\_from\_toolbar.generate 2nd & 3rd args
    \`use\_fallback\_keys\` & \`use\_reset\`.
  - bl\_keymap\_utils.platform\_helpers.keyconfig\_data\_oskey\_from\_ctrl
    2nd arg \`filter\_fn\`.
  - bl\_ui\_utils.bug\_report\_url.url\_prefill\_from\_blender 1st arg
    \`addon\_info\`.
  - bmesh.types.BMFace.copy 1st & 2nd args \`verts\`, \`edges\`.
  - bmesh.types.BMesh.calc\_volume 1st arg \`signed\`.
  - bmesh.types.BMesh.from\_mesh 2nd..4th args \`face\_normals\`,
    \`use\_shape\_key\`, \`shape\_key\_index\`.
  - bmesh.types.BMesh.from\_object 3rd & 4th args \`cage\`,
    \`face\_normals\`.
  - bmesh.types.BMesh.transform 2nd arg \`filter\`.
  - bmesh.types.BMesh.update\_edit\_mesh 2nd & 3rd args
    \`loop\_triangles\`, \`destructive\`.
  - bmesh.types.{BMVertSeq,BMEdgeSeq,BMFaceSeq}.sort 1st & 2nd arg
    \`key\`, \`reverse\`.
  - bmesh.utils.face\_split 4th..6th args \`coords\`, \`use\_exist\`,
    \`example\`.
  - bpy.data.libraries.load 2nd..4th args \`link\`, \`relative\`,
    \`assets\_only\`.
  - bpy.data.user\_map 1st..3rd args \`subset\`, \`key\_types\`,
    \`value\_types\`.
  - bpy.msgbus.subscribe\_rna 5th arg \`options\`.
  - bpy.path.abspath 2nd & 3rd args \`start\` & \`library\`.
  - bpy.path.clean\_name 2nd arg \`replace\`.
  - bpy.path.ensure\_ext 3rd arg \`case\_sensitive\`.
  - bpy.path.module\_names 2nd arg \`recursive\`.
  - bpy.path.relpath 2nd arg \`start\`.
  - bpy.types.EditBone.transform 2nd & 3rd arg \`scale\`, \`roll\`.
  - bpy.types.Operator.as\_keywords 1st arg \`ignore\`.
  - bpy.types.Struct.{keyframe\_insert,keyframe\_delete} 2nd..5th args
    \`index\`, \`frame\`, \`group\`, \`options\`.
  - bpy.types.WindowManager.popup\_menu 2nd & 3rd arg \`title\`,
    \`icon\`.
  - bpy.types.WindowManager.popup\_menu\_pie 3rd & 4th arg \`title\`,
    \`icon\`.
  - bpy.utils.app\_template\_paths 1st arg \`subdir\`.
  - bpy.utils.app\_template\_paths 1st arg \`subdir\`.
  - bpy.utils.blend\_paths 1st..3rd args \`absolute\`, \`packed\`,
    \`local\`.
  - bpy.utils.execfile 2nd arg \`mod\`.
  - bpy.utils.keyconfig\_set 2nd arg \`report\`.
  - bpy.utils.load\_scripts 1st & 2nd \`reload\_scripts\` &
    \`refresh\_scripts\`.
  - bpy.utils.preset\_find 3rd & 4th args \`display\_name\`, \`ext\`.
  - bpy.utils.resource\_path 2nd & 3rd arg \`major\`, \`minor\`.
  - bpy.utils.script\_paths 1st..4th args \`subdir\`, \`user\_pref\`,
    \`check\_all\`, \`use\_user\`.
  - bpy.utils.smpte\_from\_frame 2nd & 3rd args \`fps\`, \`fps\_base\`.
  - bpy.utils.smpte\_from\_seconds 2nd & 3rd args \`fps\`,
    \`fps\_base\`.
  - bpy.utils.system\_resource 2nd arg \`subdir\`.
  - bpy.utils.time\_from\_frame 2nd & 3rd args \`fps\`, \`fps\_base\`.
  - bpy.utils.time\_to\_frame 2nd & 3rd args \`fps\`, \`fps\_base\`.
  - bpy.utils.units.to\_string 4th..6th \`precision\`, \`split\_unit\`,
    \`compatible\_unit\`.
  - bpy.utils.units.to\_value 4th arg \`str\_ref\_unit\`.
  - bpy.utils.user\_resource 2nd & 3rd args \`subdir\`, \`create\`
  - bpy\_extras.view3d\_utils.location\_3d\_to\_region\_2d 4th arg
    \`default\`.
  - bpy\_extras.view3d\_utils.region\_2d\_to\_origin\_3d 4th arg
    \`clamp\`.
  - gpu.offscreen.unbind 1st arg \`restore\`.
  - gpu\_extras.batch.batch\_for\_shader 4th arg \`indices\`.
  - gpu\_extras.batch.presets.draw\_circle\_2d 4th arg \`segments\`.
  - gpu\_extras.presets.draw\_circle\_2d 4th arg \`segments\`.
  - imbuf.types.ImBuf.resize 2nd arg \`resize\`.
  - imbuf.write 2nd arg \`filepath\`.
  - mathutils.kdtree.KDTree.find 2nd arg \`filter\`.
  - nodeitems\_utils.NodeCategory 3rd & 4th arg \`descriptions\`,
    \`items\`.
  - nodeitems\_utils.NodeItem 2nd..4th args \`label\`, \`settings\`,
    \`poll\`.
  - nodeitems\_utils.NodeItemCustom 1st & 2nd arg \`poll\`, \`draw\`.
  - rna\_prop\_ui.draw 5th arg \`use\_edit\`.
  - rna\_prop\_ui.rna\_idprop\_ui\_get 2nd arg \`create\`.
  - rna\_prop\_ui.rna\_idprop\_ui\_prop\_clear 3rd arg \`remove\`.
  - rna\_prop\_ui.rna\_idprop\_ui\_prop\_get 3rd arg \`create\`.
  - rna\_xml.xml2rna 2nd arg \`root\_rna\`.
  - rna\_xml.xml\_file\_write 4th arg \`skip\_typemap\`.

See commit
([f29a738e23](https://projects.blender.org/blender/blender/commit/f29a738e23ce487fc9ae759326fc3409b82f8b26)).

### Motion Blur Velocities

Fluid and Alembic modifier properties for vertex velocities were
removed. These were used for motion blur on meshes with changing
topology, where vertex positions can't be matched with other frames.
([128eb6c](https://projects.blender.org/blender/blender/commit/128eb6c))

  - \`FluidDomainSettings.mesh\_vertices\`
  - \`MeshSequenceCacheModifier.vertex\_velocities\`,
    \`MeshSequenceCacheModifier.has\_velocity\`,
    \`MeshSequenceCacheModifier.read\_velocity\`

Instead, renderers and exporters should use the \`velocity\` attribute
on meshes.

``` python
for attribute in mesh.attributes:
    if attribute.name == 'velocity' and attribute.data_type == 'FLOAT_VECTOR':
         print([item.vector for item in attribute.data])
```

The unit of velocity also changed, it is now Blender scene distance
units per second. Previously vertex velocities were relative to frames
instead of seconds.
