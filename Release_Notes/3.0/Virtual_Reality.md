# Blender 3.0: Virtual Reality

## Controller Support

Blender 3.0 offers a brand new set of VR controller-based functionality,
including the ability to visualize controllers (VR, regular 3D viewport)
and the ability to navigate (teleport/fly/grab) one's way through a
scene in VR using controller inputs. This functionality is available via
the updated [VR Scene Inspection
add-on](https://docs.blender.org/manual/en/3.0/addons/3d_view/vr_scene_inspection.html).
!["Classroom" by Christophe Seux
(https://www.blender.org/download/demo-files/)](../../videos/Blender_3.0_virtual_reality.mp4
"\"Classroom\" by Christophe Seux (https://www.blender.org/download/demo-files/)")
!["Race Spaceship" by Alessandro Chiffi / ONdata Studio
(https://www.blender.org/download/demo-files/)](../../images/Xr_controller_support.png
"\"Race Spaceship\" by Alessandro Chiffi / ONdata Studio (https://www.blender.org/download/demo-files/)")

## Other Changes & Additions

  - Session uses stage reference space (user-defined tracking bounds)
    instead of local reference space (position at application launch),
    when available
    ([36c0649d32](https://projects.blender.org/blender/blender/commit/36c0649d32aa)).
  - New "Absolute Tracking" session option which skips eye offsets that
    are normally added for placing users exactly at landmarks. This
    allows the tracking origin to be defined independently of the
    headset position
    ([36c0649d32](https://projects.blender.org/blender/blender/commit/36c0649d32aa)).

![../../images/Xr\_absolute\_tracking.png](../../images/Xr_absolute_tracking.png
"../../images/Xr_absolute_tracking.png")

  - New \`Custom Object\` type for VR landmarks (replaces old \`Custom
    Camera\`), which enables any object to be used as a base pose
    reference for the VR viewer. In addition, a viewer reference scale
    can now be set for landmarks of type \`Custom Object\` or \`Custom
    Pose\`
    ([rBA823910c](https://projects.blender.org/blender/blender-addons/commit/823910c50d1c)).

![../../images/Xr\_landmark.png](../../images/Xr_landmark.png
"../../images/Xr_landmark.png")

  - Higher color depth for VR-displayed images when supported by runtime
    ([eb278f5e12](https://projects.blender.org/blender/blender/commit/eb278f5e12cf)).
  - Support for Varjo quad-view and foveated headsets (Varjo VR-3, XR-3)
    ([07c6af4136](https://projects.blender.org/blender/blender/commit/07c6af413617)).

## Fixes

  - Fixed pink screen issue on Windows when using the SteamVR runtime
    with AMD graphics cards
    ([82ab2c1678](https://projects.blender.org/blender/blender/commit/82ab2c167844)).
  - Fixed render artifacts (geometry occluded for one eye) when using VR
    with EEVEE and viewport denoising
    ([c41b93bda5](https://projects.blender.org/blender/blender/commit/c41b93bda532)).
