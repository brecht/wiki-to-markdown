# Blender 3.0.1

Released on Jan 26th 2022, Blender 3.0.1 features many bug fixes:

  - 3.0 Documentation broken link [http://developer.blender.org/T93773
    \#93773](http://developer.blender.org/T93773_#93773)
  - 3D cursor crash when using shortcut.
    [http://developer.blender.org/T94109
    \#94109](http://developer.blender.org/T94109_#94109)
  - 3D Cursor is not in the right place after Shift + C (Center Cursor
    and Frame All). [http://developer.blender.org/T93971
    \#93971](http://developer.blender.org/T93971_#93971)
  - 3D Cursor surface projection onto hidden faces.
    [http://developer.blender.org/T94392
    \#94392](http://developer.blender.org/T94392_#94392)
  - Asset browser: dropping object on grid in orthogonal view misses the
    floor plane. [http://developer.blender.org/T93388
    \#93388](http://developer.blender.org/T93388_#93388)
  - Asset Browser: Fix incorrect user message.
    [https://projects.blender.org/blender/blender/commit/24a79289b01
    24a79289b0](https://projects.blender.org/blender/blender/commit/24a79289b01_24a79289b0)
  - Auto Depth problem with Cliping Region.
    [http://developer.blender.org/T94728
    \#94728](http://developer.blender.org/T94728_#94728)
  - Blender 3.0 cannot import FBX (containing more than 8 UV layers).
    [http://developer.blender.org/T93541
    \#93541](http://developer.blender.org/T93541_#93541)
  - Blender Crashing While Appending Animation Action.
    [http://developer.blender.org/T94264
    \#94264](http://developer.blender.org/T94264_#94264)
  - Blender freezes when saving with active VR session.
    [http://developer.blender.org/T93649
    \#93649](http://developer.blender.org/T93649_#93649)
  - Boolean Apply Clearing custom data layers (bevel weight Weight /
    Vgroup Data). [http://developer.blender.org/T94197
    \#94197](http://developer.blender.org/T94197_#94197)
  - Change bone name cause vertex group not working until select another
    vertex group. [http://developer.blender.org/T93892
    \#93892](http://developer.blender.org/T93892_#93892)
  - Cleanup: Avoid possible NULL pointer error.
    [https://projects.blender.org/blender/blender/commit/822501d86df
    822501d86d](https://projects.blender.org/blender/blender/commit/822501d86df_822501d86d)
  - Cleanup: Correct order of guard and lock in moviecache\_valfree.
    [https://projects.blender.org/blender/blender/commit/0564b19ff4d
    0564b19ff4](https://projects.blender.org/blender/blender/commit/0564b19ff4d_0564b19ff4)
  - Close Area crashes Blender (3.0.0 and 3.0.1).
    [http://developer.blender.org/T94334
    \#94334](http://developer.blender.org/T94334_#94334)
  - Constraint Shrinkwrap does not applies, it reset all changes.
    [http://developer.blender.org/T94600
    \#94600](http://developer.blender.org/T94600_#94600)
  - Crash in \`nvoglv64.dll\` entering edit mode on curve.
    [http://developer.blender.org/T94454
    \#94454](http://developer.blender.org/T94454_#94454)
  - Crash in Compositing. [http://developer.blender.org/T94506
    \#94506](http://developer.blender.org/T94506_#94506)
  - Crash on changing curve type from Bezier to NURBS in Geometry Nodes.
    [http://developer.blender.org/T94082
    \#94082](http://developer.blender.org/T94082_#94082)
  - Crash on edit mesh with a curve modifier with both a vertex group
    assigned and the edit mode display option enabled.
    [http://developer.blender.org/T93611
    \#93611](http://developer.blender.org/T93611_#93611)
  - crash or error when using \`GPUFrameBuffer.read\_color(...
    data=data)\`. [http://developer.blender.org/T94202
    \#94202](http://developer.blender.org/T94202_#94202)
  - Crash when cutting meta-meta. [http://developer.blender.org/T94280
    \#94280](http://developer.blender.org/T94280_#94280)
  - Crash when exceeding memory\_cache\_limit in the viewport.
    [http://developer.blender.org/T92838
    \#92838](http://developer.blender.org/T92838_#92838)
  - Crash when the Home button is pressed in the VSE.
    [http://developer.blender.org/T94254
    \#94254](http://developer.blender.org/T94254_#94254)
  - Crash when trying to load custom preview in asset browser with user
    library selected. [http://developer.blender.org/T93691
    \#93691](http://developer.blender.org/T93691_#93691)
  - Custom node sockets don't get relinked when dropping a node on an
    existing link. [http://developer.blender.org/T93430
    \#93430](http://developer.blender.org/T93430_#93430)
  - Cycles X broke OptiX memory pooling via NVLink.
    [http://developer.blender.org/T93620
    \#93620](http://developer.blender.org/T93620_#93620)
  - DeltaX does not show how many frames you are moving your keyframes.
    [http://developer.blender.org/T94191
    \#94191](http://developer.blender.org/T94191_#94191)
  - Discontinuous cutting with the knife tool.
    [http://developer.blender.org/T93695
    \#93695](http://developer.blender.org/T93695_#93695)
  - Dragging the NLA strip cause a crash, if the related action is in
    tweakmode. [http://developer.blender.org/T93707
    \#93707](http://developer.blender.org/T93707_#93707)
  - Export to X3D Fails. [http://developer.blender.org/T94122
    \#94122](http://developer.blender.org/T94122_#94122)
  - Extruding first vertex of a spline with GN modifier enabled causes a
    crash. [http://developer.blender.org/T94442
    \#94442](http://developer.blender.org/T94442_#94442)
  - Fast GI Approximation Render Crash.
    [http://developer.blender.org/T93874
    \#93874](http://developer.blender.org/T93874_#93874)
  - Fix Asset Browser properties region toggle not showing open/closed
    state.
    [https://projects.blender.org/blender/blender/commit/2af6cb9dce3
    2af6cb9dce](https://projects.blender.org/blender/blender/commit/2af6cb9dce3_2af6cb9dce)
  - Fix crash caused by exception in Python gizmo target get handler.
    [https://projects.blender.org/blender/blender/commit/40c5786df33
    40c5786df3](https://projects.blender.org/blender/blender/commit/40c5786df33_40c5786df3)
  - Fix Cycles AVX test failure with x86\_64 build running on Arm.
    [https://projects.blender.org/blender/blender/commit/d02eecc
    d02eecc](https://projects.blender.org/blender/blender/commit/d02eecc_d02eecc)
  - Fix meta-ball bound-box calculation reading past buffer bounds.
    [https://projects.blender.org/blender/blender/commit/62ce0c60cd5
    62ce0c60cd](https://projects.blender.org/blender/blender/commit/62ce0c60cd5_62ce0c60cd)
  - Fix some shortcut keys not working on macOS with Japanese input.
    [https://projects.blender.org/blender/blender/commit/8b44b756d85
    8b44b756d8](https://projects.blender.org/blender/blender/commit/8b44b756d85_8b44b756d8)
  - Fix T94563: Cycles standalone build error on with strict
    float/double casting.
    [https://projects.blender.org/blender/blender/commit/7d26cf01f78
    7d26cf01f7](https://projects.blender.org/blender/blender/commit/7d26cf01f78_7d26cf01f7)
  - Fix: Build issue on 32 bit archs.
    [https://projects.blender.org/blender/blender/commit/6514e4c4180
    6514e4c418](https://projects.blender.org/blender/blender/commit/6514e4c4180_6514e4c418)
  - Fix/workaround macOS Rosetta crash running Cycles AVX tests.
    [https://projects.blender.org/blender/blender/commit/e78a21a
    e78a21a](https://projects.blender.org/blender/blender/commit/e78a21a_e78a21a)
  - Geometry Nodes: Random Integer min and max values half as frequent.
    [http://developer.blender.org/T93591
    \#93591](http://developer.blender.org/T93591_#93591)
  - gltf2 not exporting animations in 3.0.
    [http://developer.blender.org/T93704
    \#93704](http://developer.blender.org/T93704_#93704)
  - Grease Pencil: Copying keys doesn't preserve Keyframe Type.
    [http://developer.blender.org/T94903
    \#94903](http://developer.blender.org/T94903_#94903)
  - Grease Pencil: AutoMerge does not work when Draw Strokes On Back is
    enabled. [http://developer.blender.org/T94620
    \#94620](http://developer.blender.org/T94620_#94620)
  - Grease Pencil: Drawing don't Update after paste.
    [http://developer.blender.org/T94089
    \#94089](http://developer.blender.org/T94089_#94089)
  - Grease pencil: hue and tint modifiers don't work with selected
    material. [http://developer.blender.org/T93868
    \#93868](http://developer.blender.org/T93868_#93868)
  - Grease Pencil: Strokes drawn at 0.0 Strength still visible.
    [http://developer.blender.org/T94799
    \#94799](http://developer.blender.org/T94799_#94799)
  - img.has\_data Always returns True since version 3.0.
    [http://developer.blender.org/T93871
    \#93871](http://developer.blender.org/T93871_#93871)
  - Instances of only vertices/edges not properly shown in viewport.
    [http://developer.blender.org/T84710
    \#84710](http://developer.blender.org/T84710_#84710)
  - Knife project fails in orthographic mode.
    [http://developer.blender.org/T94145
    \#94145](http://developer.blender.org/T94145_#94145)
  - Line Art: Per object Override Crease setting does not work.
    [http://developer.blender.org/T94878
    \#94878](http://developer.blender.org/T94878_#94878)
  - Line Art: Correct clamping of out of bound isect index.
    [https://projects.blender.org/blender/blender/commit/69c56d2819a
    69c56d2819](https://projects.blender.org/blender/blender/commit/69c56d2819a_69c56d2819)
  - Line Art: Correct collection intersection mask logic.
    [https://projects.blender.org/blender/blender/commit/7339663bbc3
    7339663bbc](https://projects.blender.org/blender/blender/commit/7339663bbc3_7339663bbc)
  - Measure active tool fails in front view with snap incremental
    enable. [http://developer.blender.org/T93869
    \#93869](http://developer.blender.org/T93869_#93869)
  - Mesh Sequence Cache (ABC) an hair interpolated children distribution
    issue (triangulate modifier seem to fix it).
    [http://developer.blender.org/T92561
    \#92561](http://developer.blender.org/T92561_#92561)
  - Mesh: Add versioning in 3.0 for vertex normal refactor.
    [https://projects.blender.org/blender/blender/commit/add07576a090186dde7858bcb6e68f9cf059f902
    add07576a0](https://projects.blender.org/blender/blender/commit/add07576a090186dde7858bcb6e68f9cf059f902_add07576a0)
  - Multiple volumes using same data can cause crash/freeze.
    [http://developer.blender.org/T94715
    \#94715](http://developer.blender.org/T94715_#94715)
  - New-file causes Python assert with negative reference count.
    [http://developer.blender.org/T94708
    \#94708](http://developer.blender.org/T94708_#94708)
  - Object As Font not working. [http://developer.blender.org/T94624
    \#94624](http://developer.blender.org/T94624_#94624)
  - Outliner: Collection dragging tooltip is not updating.
    [http://developer.blender.org/T94184
    \#94184](http://developer.blender.org/T94184_#94184)
  - Python error when trying to add Grease Pencil brush preset.
    [http://developer.blender.org/T94375
    \#94375](http://developer.blender.org/T94375_#94375)
  - redundant 'falloff' dropdown in weight paint header.
    [http://developer.blender.org/T93169
    \#93169](http://developer.blender.org/T93169_#93169)
  - Regression: Grease Pencil does not show up in render past 2049x2049
    render resolution. [http://developer.blender.org/T94169
    \#94169](http://developer.blender.org/T94169_#94169)
  - Regression: Group Input/Output cannot connect to some custom
    sockets. [http://developer.blender.org/T94827
    \#94827](http://developer.blender.org/T94827_#94827)
  - save\_as\_mainfile will crash if context has no screen.
    [http://developer.blender.org/T93949
    \#93949](http://developer.blender.org/T93949_#93949)
  - Scaling Grease Pencil Strokes in Edit Mode Scales Thickness.
    [http://developer.blender.org/T93163
    \#93163](http://developer.blender.org/T93163_#93163)
  - Selecting current action in Undo History Undoes all the History.
    [http://developer.blender.org/T94115
    \#94115](http://developer.blender.org/T94115_#94115)
  - Selection in the 3d view is broken in build from Xcode 13.
    [http://developer.blender.org/T91680
    \#91680](http://developer.blender.org/T91680_#91680)
  - Set Origin causes unexpected offset on Grease Pencil strokes when
    Curve Editing is enabled. [http://developer.blender.org/T93134
    \#93134](http://developer.blender.org/T93134_#93134)
  - Shadow Catcher - Cuda Error in Viewport Rendering with Optix
    Denoiser. [http://developer.blender.org/T93890
    \#93890](http://developer.blender.org/T93890_#93890)
  - Shadow Terminator Geometry Offset causes artifacts for translucent
    shader (Cycles). [http://developer.blender.org/T93418
    \#93418](http://developer.blender.org/T93418_#93418)
  - Shift+F1 to switch to asset browser randomly crashes.
    [http://developer.blender.org/T93508
    \#93508](http://developer.blender.org/T93508_#93508)
  - Single point NURBS crash in resample node.
    [http://developer.blender.org/T93521
    \#93521](http://developer.blender.org/T93521_#93521)
  - Snap Cursor not working after changing Add Object tool settings.
    [http://developer.blender.org/T93732
    \#93732](http://developer.blender.org/T93732_#93732)
  - Snap performance regression at high poll rate.
    [http://developer.blender.org/T93408
    \#93408](http://developer.blender.org/T93408_#93408)
  - Spare falloff panel. [http://developer.blender.org/T94243
    \#94243](http://developer.blender.org/T94243_#94243)
  - Subdividing with overlapping tri and quad crashes Blender.
    [http://developer.blender.org/T93563
    \#93563](http://developer.blender.org/T93563_#93563)
  - Tool Settings: Drag on Tweak not working.
    [http://developer.blender.org/T92953
    \#92953](http://developer.blender.org/T92953_#92953)
  - Triangulating a mesh with overlapping tri and quad faces can cause
    an assertion failure. [http://developer.blender.org/T93574
    \#93574](http://developer.blender.org/T93574_#93574)
  - Unity x Blender 3.0 Integration.
    [http://developer.blender.org/T95099
    \#95099](http://developer.blender.org/T95099_#95099)
  - Use World background color when rendering pose library previews.
    [http://developer.blender.org/T93467
    \#93467](http://developer.blender.org/T93467_#93467)
  - Video editor thumbnails don't show at the default zoom level.
    [http://developer.blender.org/T93314
    \#93314](http://developer.blender.org/T93314_#93314)
  - Video Sequencer Preview with Prefetch Frames crashes inside Meta
    Strip. [http://developer.blender.org/T94768
    \#94768](http://developer.blender.org/T94768_#94768)
  - Viewport X-Ray is influencing snapping even in material mode.
    [http://developer.blender.org/T93477
    \#93477](http://developer.blender.org/T93477_#93477)
  - VR Add-on: Unintended navigation when using Valve Index.
    [http://developer.blender.org/T93509
    \#93509](http://developer.blender.org/T93509_#93509)
  - VSE: Fix strip with mask modifier not blending.
    [https://projects.blender.org/blender/blender/commit/1fd824345d5
    1fd824345d](https://projects.blender.org/blender/blender/commit/1fd824345d5_1fd824345d)
  - When render with Cycles' Fast GI Approximation, Method "Add" is
    affected by bounces in Method "Replace".
    [http://developer.blender.org/T93498
    \#93498](http://developer.blender.org/T93498_#93498)
  - When separating a grease pencil stroke to its own object it will
    lose all vertex groups. [http://developer.blender.org/T93728
    \#93728](http://developer.blender.org/T93728_#93728)
  - Zstd-compressed \`.blend\` files from external tools aren't
    recognized. [http://developer.blender.org/T93858
    \#93858](http://developer.blender.org/T93858_#93858)
