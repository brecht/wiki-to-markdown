# Blender 3.0: Grease Pencil

## Line Art

  - Speed up tile access.
    ([6ad4b8b7](https://projects.blender.org/blender/blender/commit/6ad4b8b7))
  - Better tolerance to faces perpendicular to view.
    ([5b176b66](https://projects.blender.org/blender/blender/commit/5b176b66))
  - Threaded object loading.
    ([1b07b7a0](https://projects.blender.org/blender/blender/commit/1b07b7a0))
  - Cached calculation for modifiers that are in the same stack.
    ([247abdbf](https://projects.blender.org/blender/blender/commit/247abdbf))
  - Loose edge type and related chaining improvements.
    ([841df831](https://projects.blender.org/blender/blender/commit/841df831))
  - Option to filter feature line using freestyle face mark.
    ([3558bb8e](https://projects.blender.org/blender/blender/commit/3558bb8e))
  - Occlussion effectiveness support for mesh material.
    ([cf21ba37](https://projects.blender.org/blender/blender/commit/cf21ba37))
  - Option to filter feature line with collection intersection masks.
    ([d1e0059e](https://projects.blender.org/blender/blender/commit/d1e0059e))
  - Camera overscan allows line art effective range to go beyond image
    frame, preventing strokes from ending right at the border.
    ([8e9d06f5](https://projects.blender.org/blender/blender/commit/8e9d06f5))
  - Automatic crease with flat/smooth surfaces, which allows crease
    detection to follow smooth/flat shading and auto-smooth
    configuration.
    ([c1cf66bff3](https://projects.blender.org/blender/blender/commit/c1cf66bff3c0))
  - Stroke offset function, allow strokes to be interacting with scene
    depth without showing in-front of
    everything.([c3ef1c15](https://projects.blender.org/blender/blender/commit/c3ef1c15))
  - Custom camera allows line art to compute strokes from other cameras
    instead of the active
    one.([efbd3642](https://projects.blender.org/blender/blender/commit/efbd3642))
  - Trimming edges right at the image border, useful when having
    multiple camera setup where you want the border to be
    clean.([ec831ce5](https://projects.blender.org/blender/blender/commit/ec831ce5))

## Operators

  - New operator to bake GPencil transformed strokes into a new GPencil
    object.
    ([06f86dd4d9](https://projects.blender.org/blender/blender/commit/06f86dd4d9a2))
  - Multiframe support added to Reproject operator.
    ([e1acefd45e](https://projects.blender.org/blender/blender/commit/e1acefd45e23))
  - Multiframe support added to Move to Layer operator.
    ([e4cebec647](https://projects.blender.org/blender/blender/commit/e4cebec647fb))
  - Mask layer list now can be reordered.
    ([8032bd98d8](https://projects.blender.org/blender/blender/commit/8032bd98d820))
  - Now Blank objects include Layer and Material by default.
    ([5cb1e18f72](https://projects.blender.org/blender/blender/commit/5cb1e18f7207))
  - Added support to convert Text to Grease Pencil object.
    ([41820e8a8e](https://projects.blender.org/blender/blender/commit/41820e8a8e70))
  - New Bracket keymaps (\`\[\`, \`\]\`) to increase and decrease brush
    size.
    ([c58bf31aed](https://projects.blender.org/blender/blender/commit/c58bf31aeddd))
  - New operator to Copy materials to other grease pencil object. Can
    copy Active or All materials.
    ([960535ddf3](https://projects.blender.org/blender/blender/commit/960535ddf344)),
    ([f6cb9433d4](https://projects.blender.org/blender/blender/commit/f6cb9433d45a))
  - Layer Copy to Object has been renamed to Copy Layer to Object and
    allows to copy all layers at once.
    ([960535ddf3](https://projects.blender.org/blender/blender/commit/960535ddf344)),
    ([f6cb9433d4](https://projects.blender.org/blender/blender/commit/f6cb9433d45a))
  - New operator to normalize the Thickness or the Opacity of Strokes.
    ([f944121700](https://projects.blender.org/blender/blender/commit/f944121700da))
  - New Select Random operator.
    ([a7aeec2655](https://projects.blender.org/blender/blender/commit/a7aeec26550e))
  - New Convert Mesh to Grease Pencil copy the Vertex Groups with
    weights.
    ([88dc274d05](https://projects.blender.org/blender/blender/commit/88dc274d0533))
  - Automerge when drawing strokes has been improved for better join
    when thickness is very different.
    ([ae334532cf](https://projects.blender.org/blender/blender/commit/ae334532cffb))

## Tools

  - New \`Dilate\` parameter for Fill brush to expand filled area to
    fill small gaps.
    ([f8f6e0b256](https://projects.blender.org/blender/blender/commit/f8f6e0b25621))
  - Annotations: Restore the \`Placement\` parameter in 2D Editors.
    ([6ee14c966d](https://projects.blender.org/blender/blender/commit/6ee14c966d0))

## UI

  - The Topbar \`Leak Size\` parameter has been replaced by new
    \`Dilate\` parameter. \`Leak Size\` has been moved to Advanced
    panel.
    ([f8f6e0b256](https://projects.blender.org/blender/blender/commit/f8f6e0b25621))
  - Removed duplicated \`B\` keymap to insert Blank keyframe in Drawing
    Mode. This was old keymap and it has been replaced with \`I\` menu.
    ([0467979425](https://projects.blender.org/blender/blender/commit/04679794259f))
  - Annotations now display Stabilizer parameters in topbar and also in
    VSE.
    ([0a83f32d79](https://projects.blender.org/blender/blender/commit/0a83f32d79bc))
  - New \`Use Lights\` option now available for when creating line art
    GPencil objects to allow convenient access to desired effect.
    ([45b28c0f88](https://projects.blender.org/blender/blender/commit/45b28c0f8847))

![../../images/GP\_Use\_Lights.png](../../images/GP_Use_Lights.png
"../../images/GP_Use_Lights.png")

  - New option in Topbar to define the type of caps (Rounded/Flat) used
    in new strokes.
    ([c1730ed165](https://projects.blender.org/blender/blender/commit/c1730ed16568))
  - New keymap \`Shift+Ctrl+M\`to merge layer.
    ([27b9cb7a1e](https://projects.blender.org/blender/blender/commit/27b9cb7a1e67))
  - Added buttons to move Up and Down Vertex Groups in the list.
    ([2b64b4d90d](https://projects.blender.org/blender/blender/commit/2b64b4d90d67))
  - Use Scale Thickness option is now enabled by default in 2D template.
    ([6fc92b296f](https://projects.blender.org/blender/blender/commit/6fc92b296fae))
  - List of modifiers reorganized moving some of them to new \`Modify\`
    column.
    ([9cf593f305](https://projects.blender.org/blender/blender/commit/9cf593f30519))

## Modifiers and VFX

  - New Vertex Weight modifiers to generate weights base on Proximity or
    Angle on the fly to be used in any modifier.
    ([29b65f5345](https://projects.blender.org/blender/blender/commit/29b65f534512),
    [368b56c9a1](https://projects.blender.org/blender/blender/commit/368b56c9a132))
  - New Randomize options for Offset modifier.
    ([6a2bc40e01](https://projects.blender.org/blender/blender/commit/6a2bc40e0131))
  - Now Offset modifier uses the weights in the randomize parameters.
    ([b73dc36859](https://projects.blender.org/blender/blender/commit/b73dc36859e0))
  - New Length modifier that allows modification to stroke length in
    percentage to its own length or in geometry space
    ([cd16123761](https://projects.blender.org/blender/blender/commit/cd1612376138)),
    also supports extending strokes based on curvature
    ([25e548c96b](https://projects.blender.org/blender/blender/commit/25e548c96b3d8)).

![../../videos/Length\_Modifier\_Test.mp4](../../videos/Length_Modifier_Test.mp4
"../../videos/Length_Modifier_Test.mp4")

![../../images/GPencil\_Length.png](../../images/GPencil_Length.png
"../../images/GPencil_Length.png")

  - New Dot-dash modifier that allows generating dot-dash lines and
    assign different materials to each segments.
    ([a2c5c2b406](https://projects.blender.org/blender/blender/commit/a2c5c2b4068d))

![../../images/GPencil\_Dash.png](../../images/GPencil_Dash.png
"../../images/GPencil_Dash.png")

## Compositing

  - Added a new option to use masks during view layer render
    ([e459a25e6c](https://projects.blender.org/blender/blender/commit/e459a25e6cbe)).
    This toggle can be found in the

"Layers" -\> "Relations" panel called "Use Masks in Render". The option
is disabled if there is no view layer selected.

|                                                                                                                                                                                                              |                                                                                                                                                                                                 |
| ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| ![../../images/GPencil\_disable\_masks\_viewlayer\_(not\_disabled).png](../../images/GPencil_disable_masks_viewlayer_\(not_disabled\).png "../../images/GPencil_disable_masks_viewlayer_(not_disabled).png") | ![../../images/GPencil\_disable\_masks\_viewlayer\_(disabled).png](../../images/GPencil_disable_masks_viewlayer_\(disabled\).png "../../images/GPencil_disable_masks_viewlayer_(disabled).png") |
