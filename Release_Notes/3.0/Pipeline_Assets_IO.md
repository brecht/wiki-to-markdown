# Blender 3.0: Pipeline, Assets & IO

## Packing

  - Packing linked libraries is now accessible via the File menu
    ([rBee51e733](https://projects.blender.org/blender/blender/commit/ee51e7335552)).
  - Cleanup of the other External Data options
    ([rBee51e733](https://projects.blender.org/blender/blender/commit/ee51e7335552)).

![../../images/External\_Data\_Packed.png](../../images/External_Data_Packed.png
"../../images/External_Data_Packed.png")

## Alembic

  - Animated UV maps are now exported to Alembic
    ([rB3e77f747](https://projects.blender.org/blender/blender/commit/3e77f747c0d1)).
  - Generated mesh vertex coordinates (also known as ORCOs) are now
    exported to and imported from Alembic
    ([rBf9567f6c](https://projects.blender.org/blender/blender/commit/f9567f6c63e7)).
    They're stored in \`.arbGeomParams/Pref\` in the Alembic file.
  - Per vertex UV maps are now imported from Alembic
    ([3385c04598](https://projects.blender.org/blender/blender/commit/3385c04598f)).
    Such UV maps can be defined by other software to reduce file size
    when the mesh is split according to UV islands. Blender, however,
    still stores the UV data per face corner.
  - Non functional "Renderable Objects only" option has been removed
    ([834e87af7b](https://projects.blender.org/blender/blender/commit/834e87af7bbf)),
    has been superseeded by the following:
  - New option to set evaluation mode to Render or Viewport (for
    visibility, modifiers) in
    [8f5a4a2453](https://projects.blender.org/blender/blender/commit/8f5a4a245388)
  - New option to always add a cache reader when importing files
    ([5b97c00e9f](https://projects.blender.org/blender/blender/commit/5b97c00e9fc4e)).
    This simplifies workflows for updating objects after imports if they
    change in the cache.

## USD Importer

USD files can now be imported into Blender
([rBea54cbe1](https://projects.blender.org/blender/blender/commit/ea54cbe1b42e)).
The USD importer works in a similar fashion as the Alembic importer.

Learn more about Importing USD Files in the [Blender
manual](https://docs.blender.org/manual/en/3.0/files/import_export/usd.html#importing-usd-files).

## glTF 2.0

### Importer

  - Import custom properties from default scene
    ([rBA260ca33](https://projects.blender.org/blender/blender-addons/commit/260ca332f88f643a302bcabf140e3c471c8c621b))
  - Fix issue involving flipped bone Z dir
    ([rBAb986a52](https://projects.blender.org/blender/blender-addons/commit/b986a52051d48c2cda5a38159d05fb198cc15db7))
  - Avoid traceback when trying to import some invalid glTF 2.0 files
    ([rBA4d56268](https://projects.blender.org/blender/blender-addons/commit/4d562682c099ce740d705893008a91a30a206710))

### Exporter

  - Add option to keep original texture files
    ([rBA0cdaac6](https://projects.blender.org/blender/blender-addons/commit/0cdaac6f9a3e318b1d5db04ade2838d004cd500d))
  - Do not export glTF internal settings as extras
    ([rBA57ef445](https://projects.blender.org/blender/blender-addons/commit/57ef445314fef1290b155cb182ee7e513bc22008))
  - Remove some channel animation if bone is not animated
    ([rBA1757ec9](https://projects.blender.org/blender/blender-addons/commit/1757ec91e58a23c6dbd31b763267e1d3f5e40026),
    [rBAa8c700d](https://projects.blender.org/blender/blender-addons/commit/a8c700d4ebe6a1a767758a0b79775d48d7fbe085),
    [rBA45132ba](https://projects.blender.org/blender/blender-addons/commit/45132ba0f4410e9b7126bdc0a141ffca8b801c8a))
  - Draco: more explicit error message
    ([rBAeedbe30](https://projects.blender.org/blender/blender-addons/commit/eedbe303799fb8af4e7dafea81a0dd46973fb85b))
  - Fix bug when invalid shapekey driver
    ([rBAac6fe2f](https://projects.blender.org/blender/blender-addons/commit/ac6fe2ff7c9b68b8a08e74ac3b8e350a6eb9d24d),
    [rBAcd70923](https://projects.blender.org/blender/blender-addons/commit/cd7092335690c5644c5c75cea23672510fd58ce3))
  - Fix driver export when shapekey as a dot in name
    ([rBA54d8bdf](https://projects.blender.org/blender/blender-addons/commit/54d8bdf1f15597b9f6e1aaf308b5a0d5df785552))
  - Fix default date type at numpy array creation
    ([rBA97cf910](https://projects.blender.org/blender/blender-addons/commit/97cf91034d0d5255269a0b8c506f2cdc8e81b789))
  - Make sure that addon that changes root gltf are taken into account
    ([rBA0aa618c](https://projects.blender.org/blender/blender-addons/commit/0aa618c849ffef7b11cef81f1712b89e0b0e337b))
  - Don't erase empty dict extras
    ([rBA6c1a227](https://projects.blender.org/blender/blender-addons/commit/6c1a227b3fa329d56bdaf0e4ebd6d72aa92d2bfe))
  - Add merge\_animation\_extensions\_hook
    ([rBA669b89d](https://projects.blender.org/blender/blender-addons/commit/669b89d6a56dfebee45c4b72950d212da04d17df))
  - Do not export empty node when camera or light export is disable
    ([rBA060d0e7](https://projects.blender.org/blender/blender-addons/commit/060d0e76fbb2319e12e8fdf8435debe5267bfc7d))
  - Fix crash when trying to export some muted driver(s)
    ([rBA902b8ba](https://projects.blender.org/blender/blender-addons/commit/902b8ba11efa98006b3a247e4549ca69fb458309))
  - Change order of format option
    ([rBA760b5d3](https://projects.blender.org/blender/blender-addons/commit/760b5d3a46416bd55dabec09aa38385ac3ec2467))
  - Cleanup: use inverted\_safe on matrices
    ([rBA29c9aa3](https://projects.blender.org/blender/blender-addons/commit/29c9aa34754494bc478a08f0be60f38f4844a83d))
  - Validate meshes before export
    ([rBA5bffedc](https://projects.blender.org/blender/blender-addons/commit/5bffedce241f04dc57374ea86f9701e1fddff47f))
  - Better 'selection only' management
    ([rBAe7f2213](https://projects.blender.org/blender/blender-addons/commit/e7f22134350127ac18747c367bb0ad9a1ef2d8a3))
  - Fix animation export for objects parented to bones
    ([rBA8975ba0](https://projects.blender.org/blender/blender-addons/commit/8975ba0a553a6cbe6b9e086a474331bddfb55841))
  - Cleanup object animation curves when animation is constant
    ([rBAba969e8](https://projects.blender.org/blender/blender-addons/commit/ba969e8b536781450d8b43959c95ca07e886b2b6))
  - Change after change on vertex groups data now on mesh
    ([rBA64fcec2](https://projects.blender.org/blender/blender-addons/commit/64fcec250d2d9450ac64d8314af7092a6922a28b))
  - Avoid issue with setting frame with python v \>= 3.10
    ([rBAa0d1647](https://projects.blender.org/blender/blender-addons/commit/a0d1647839180388805be150794a812c44e59053))
