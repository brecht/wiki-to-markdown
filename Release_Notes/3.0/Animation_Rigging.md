# Blender 3.0: Animation & Rigging

  - New keying set: **Location, Rotation, Scale, and Custom Properties**
    ([rB7192ab61](https://projects.blender.org/blender/blender/commit/7192ab614b49)).
    Effectively, it combines the behaviour of *Whole Character (selected
    bones only)* (which keys loc/rot/scale/customprops, but only works
    in pose mode) with *Location, Rotation, and Scale* (which works in
    both object and pose mode, but doesn't key custom properties). This
    makes it possible to use one keying set for animating both
    characters and props.
  - Ctrl+F in animation editors no longer blocks the UI with a popup,
    but simply shows & activates the channel search textbox
    ([rB41a4c62c](https://projects.blender.org/blender/blender/commit/41a4c62c3155)).
  - FCurve and NLA **modifier properties can now be overridden**
    ([rBd2311de2](https://projects.blender.org/blender/blender/commit/d2311de21853)).
    Previously an armature/object with overrides could get new
    FCurve/NLA modifiers, but the properties would be read-only. Now
    they can be edited.
  - Keyframe removal (Default: Alt+I, Industry Compatible: Alt+S) now
    respects the active keying set
    ([rB7fc22051](https://projects.blender.org/blender/blender/commit/7fc220517f87),
    [rB1364f1e3](https://projects.blender.org/blender/blender/commit/1364f1e35c20)).
  - Custom bone shapes now have full translation/rotation/scale options
    ([rBfc5bf09f](https://projects.blender.org/blender/blender/commit/fc5bf09fd88c)).

![../../images/release-notes-300-animation-rigging-custom-bone-transform.png](../../images/release-notes-300-animation-rigging-custom-bone-transform.png
"../../images/release-notes-300-animation-rigging-custom-bone-transform.png")

  - The Asset Browser now supports rendering previews for Action
    datablocks
    ([rB17534e28](https://projects.blender.org/blender/blender/commit/17534e28ff44)).
    This is documented further in the [3.0 Asset
    Browser](Asset_Browser.md)
    release notes.
  - FCurves and all their keys can be selected by box- or
    circle-selecting the curve itself.
    ([rB2246d456](https://projects.blender.org/blender/blender/commit/2246d456aa84))
      - Box selecting a curve selects all the keyframes of the curve.
      - Ctrl + box selecting of the curve deselects all the keyframes of
        the curve.
      - Shift + box selecting of the curve extends the keyframe
        selection, adding all the keyframes of the curves that were just
        selected to the selection.
      - In all cases, if the selection area contains a key, nothing is
        performed on the curves themselves (the action only impacts the
        selected keys).

![../../videos/release-notes-300-animation-rigging-fcurve-select.mp4](../../videos/release-notes-300-animation-rigging-fcurve-select.mp4
"../../videos/release-notes-300-animation-rigging-fcurve-select.mp4")

  - Make Single User: in addition to **object** animation, now **object
    data** (mesh, curve, ...) animation can be made single user as well.
    ([rBd0c5c67e](https://projects.blender.org/blender/blender/commit/d0c5c67e940b))
  - FCurve modifiers are now correctly evaluated in Restrict Range
    Borders
    ([rB9dee0a10](https://projects.blender.org/blender/blender/commit/9dee0a10c81d)).
      - When using FModifier Restrict Frame Range, the resulting
        influence was zero being exactly on Start / End range borders
        (so borders were exclusive). This made it impossible to chain
        FModifers together (forcing the user to specify values slightly
        below the desired border in following FModifiers). This is now
        corrected to be inclusive on Start / End range borders.

[File:Before.png|FCurve](File:Before.png%7CFCurve) modifiers were
evaluated wrong on restrict range borders After.png|FCurve modifiers are
now evaluated correctly

## Motion Paths

New operator: **Update All Visible Motion Paths**
([4de0e2e771](https://projects.blender.org/blender/blender/commit/4de0e2e7717f)).
Besides having this handy new button, there is also a smaller, related
improvement that updating the motion paths for an armature now updates
all the bones motion paths simultaneously.

![../../images/release-notes-300-animation-motion-paths-refresh-all.png](../../images/release-notes-300-animation-motion-paths-refresh-all.png
"../../images/release-notes-300-animation-motion-paths-refresh-all.png")

## Vertex Groups

  - The vertex group names are now stored in meshes directly instead of
    objects, which causes various compatibility issues with files from
    previous versions. See the [compatibility
    section](Core.md#Compatibility)
    in the core release notes for more details.
    ([rB3b6ee8ce](https://projects.blender.org/blender/blender/commit/3b6ee8cee708),
    [rBfc32567c](https://projects.blender.org/blender/blender/commit/fc32567cdaa5))

## Pose Library

A new Pose Library has been added to Blender
([9473c61b36](https://projects.blender.org/blender/blender/commit/9473c61b366),
[28dc07a153](https://projects.blender.org/blender/blender/commit/28dc07a153d),
[f3610a23d1](https://projects.blender.org/blender/blender/commit/f3610a23d12),
[e01b52100f](https://projects.blender.org/blender/blender/commit/e01b52100f)).
It is based on the [Asset
Browser](Asset_Browser.md). For demo
videos & a thorough explanation, see the [Pose Library
v2.0](https://code.blender.org/2021/05/pose-library-v2-0/) blog post.
The pose library is partially implemented as add-on (enabled by default;
[34771cc9f5](https://projects.blender.org/blender/blender/commit/34771cc9f56))
such that studios can create their own alternative implementations.

### Converting poses from the old to the new pose library

![../../images/release-notes-300-animation-pose-library-convert.png](../../images/release-notes-300-animation-pose-library-convert.png
"../../images/release-notes-300-animation-pose-library-convert.png")

To convert an old-style pose library to a new one, follow these steps:

1.  Make sure the active object is the character rig.
2.  In the Action Editor, select the pose library Action.
3.  In the sidebar (press N if not visible), choose "Convert Old-Style
    Pose Library".

This will create an Action asset for each pose in the library.

## Bendy Bones

![../../images/release-notes-300-animation-rigging-bbone-scale.png](../../images/release-notes-300-animation-rigging-bbone-scale.png
"../../images/release-notes-300-animation-rigging-bbone-scale.png")

  - Renamed confusingly named Curve Y and Scale Y channels to Z.
    Animation curves and drivers are automatically updated, but Python
    scripts have to be changed manually.
    ([rB682a74e0](https://projects.blender.org/blender/blender/commit/682a74e0909))
  - Added actual Scale Y channels that produce non-uniform segment
    lengths.
    ([rB638c16f4](https://projects.blender.org/blender/blender/commit/638c16f4101)).
  - Added simple toggles that replace up to 6-8 trivial drivers copying
    handle bone local scale to the corresponding properties.
    ([rBb6030711](https://projects.blender.org/blender/blender/commit/b6030711a24))

<table>
<tbody>
<tr class="odd">
<td><div class="note_title">
<p><strong>Forward Incompatibility Breakage</strong></p>
</div>
<div class="note_content">
<p>Blend files saved in Blender 3.0 with these features can cause unwanted behavior in older versions of Blender, see <a href="http://developer.blender.org/T89621">#89621</a>.</p>
</div></td>
</tr>
</tbody>
</table>

## Constraints

  - New "Local Space (Owner Orientation)" choice for Target Space of
    bone targets, which allows copying local transformation while
    adjusting for the difference in rest pose orientations aiming to
    produce the same global motion.
    ([rB5a693ce9](https://projects.blender.org/blender/blender/commit/5a693ce9e3))

` `![`../../videos/Demo-owner-local.mp4`](../../videos/Demo-owner-local.mp4
"../../videos/Demo-owner-local.mp4")

  - Constraints now have Apply Constraint, Duplicate Constraint, and
    Copy To Selected operators in an "extras" menu, similar to modifiers
    ([rBd6891d9b](https://projects.blender.org/blender/blender/commit/d6891d9bee2)).

` `![`../../images/Release_Notes-Blender-3.0-Constraints-extras-menu.png`](../../images/Release_Notes-Blender-3.0-Constraints-extras-menu.png
"../../images/Release_Notes-Blender-3.0-Constraints-extras-menu.png")

### Limit Rotation

  - The constraint now correctly removes shear before processing, and
    without any limits can be used for that explicit purpose.
    ([rBedaaa2af](https://projects.blender.org/blender/blender/commit/edaaa2afddb2))
  - Added an Euler Order option similar to Copy Rotation.
    ([rBd2dc4523](https://projects.blender.org/blender/blender/commit/d2dc452333a4))

### Copy Transforms

  - New Remove Target Shear option for removing shear from the result of
    the Target Space transformation.
    ([rBbc8ae587](https://projects.blender.org/blender/blender/commit/bc8ae58727))
  - More Mix mode options representing different ways to handle scale
    and location, resulting in a complete set of six "Before/After
    Original (Full/Aligned/Split Channels)" choices in addition to
    Replace.
    ([rBbc8ae587](https://projects.blender.org/blender/blender/commit/bc8ae58727))

` `![`../../videos/Demo-mix-full-aligned-split1.mp4`](../../videos/Demo-mix-full-aligned-split1.mp4
"../../videos/Demo-mix-full-aligned-split1.mp4")

### Action

  - More Mix mode options to complete the same set of six "Before/After
    Original (Full/Aligned/Split Channels)" choices as in Copy
    Transforms.
    ([rBcf10eb54](https://projects.blender.org/blender/blender/commit/cf10eb54cc))
  - For constraints on *bones*, the default Mix mode is now "Before
    Original (Split Channels)".
    ([rBcf10eb54](https://projects.blender.org/blender/blender/commit/cf10eb54cc))

### Stretch To

  - The default Rotation Type for newly created constraints has been
    changed to Swing, which was introduced in 2.82 to replicate the
    behavior of the common Damped Track + Stretch To combination using
    just the Stretch To constraint.
    ([rB8da23fd5](https://projects.blender.org/blender/blender/commit/8da23fd5aaa))

## Pose Sliding / In-Betweens Tools

![../../videos/release-notes-300-animation-rigging-pose-sliding-tools.mp4](../../videos/release-notes-300-animation-rigging-pose-sliding-tools.mp4
"../../videos/release-notes-300-animation-rigging-pose-sliding-tools.mp4")

These "In Betweens" tools have been improved
([rB9797b95f](https://projects.blender.org/blender/blender/commit/9797b95f6175)):

  - Push Pose from Rest Pose
  - Relax Pose to Rest Pose
  - Push Pose from Breakdown
  - Relax Pose to Breakdown
  - Pose Breakdowner

These all now use the same new sliding tool:

  - Actual visual indication of the blending/pushing percentage applied.
  - Mouse wrapping to allow for extrapolation without having to worry
    about the initial placement of the mouse. This also means these
    tools are actually usable when chosen from the menu.
  - Precision mode by holding Shift.
  - Snapping to 10% increments by holding Ctrl.
  - Overshoot protection; by default the tool doesn't allow overshoot
    (lower than 0% or higher than 100%), and it can be enabled by
    pressing the E key.
  - Bones are hidden while sliding, so the pose itself can be seen more
    clearly. This can be toggled by pressing the H key while using the
    tool.

New Pose Sliding Operator - Blend To Neighbour
([f7a492d460](https://projects.blender.org/blender/blender/commit/f7a492d46054)):

  - Nudge the current pose to either the left or the right pose.
  - Useful for dragging parts in an inbetween without losing your
    current pose.
  - Found in Pose Mode under <span class="literal">Pose</span> »
    <span class="literal">In-Betweens</span> »
    <span class="literal">Blend To Neighbour</span>.
  - Also available with the
    <span class="hotkeybg"><span class="hotkey">Alt</span><span class="hotkey">⇧
    Shift</span><span class="hotkey">E</span></span> shortcut.

![../../videos/Blend\_to\_neighbour\_2.mp4](../../videos/Blend_to_neighbour_2.mp4
"../../videos/Blend_to_neighbour_2.mp4")
