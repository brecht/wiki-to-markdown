# Blender 3.0: VFX & Video

## Clip Editor

  - Expose 2D Cursor Location to UI and Python API
    ([2d867426b1](https://projects.blender.org/blender/blender/commit/2d867426b1b7))
  - UI: Move Annotation Panel to new View tab
    ([ce95a2b148](https://projects.blender.org/blender/blender/commit/ce95a2b148e))
  - Sort by start/end frame in the dopesheet
    ([fb820496f5](https://projects.blender.org/blender/blender/commit/fb820496f5b0))

## VFX

  - Updated camera presets
    ([d486ee2dbd](https://projects.blender.org/blender/blender/commit/d486ee2dbdd))

## Sequencer

  - 128 channels are supported now (before it was 32)
    ([8fecc2a852](https://projects.blender.org/blender/blender/commit/8fecc2a85254))
  - The layout of the speed Effect Strip has been reworked
    ([f013e3de81](https://projects.blender.org/blender/blender/commit/f013e3de81da))

VFX-Speed Effect 3.0 Before.png|400xpx|thumb|Before VFX-Speed Effect 3.0
After.png|400px|thumb|After

  - Allow effect inputs to be blended if placed above effect
    ([0b7744f4da](https://projects.blender.org/blender/blender/commit/0b7744f4da66))
  - Bring back select strips at current frame feature
    ([7655cc45eb](https://projects.blender.org/blender/blender/commit/7655cc45eb92))
  - Use own category for metadata panel
    ([3400ba329e](https://projects.blender.org/blender/blender/commit/3400ba329e6b))
  - Add refresh\_all operator to all sequencer regions
    ([3e695a27cd](https://projects.blender.org/blender/blender/commit/3e695a27cdfa))
  - Improve animation evaluation performance
    ([1a5fa2b319](https://projects.blender.org/blender/blender/commit/1a5fa2b319e0))
  - Reduce transform code complexity
    ([e7003bc965](https://projects.blender.org/blender/blender/commit/e7003bc9654b))
  - Improved Snapping
    ([fba9cd019f](https://projects.blender.org/blender/blender/commit/fba9cd019f21))
  - Make pasted strip active
    ([d374469f4c](https://projects.blender.org/blender/blender/commit/d374469f4c04))
  - Add Sequence.split RNA function
    ([53743adc29](https://projects.blender.org/blender/blender/commit/53743adc297f))
  - Draw strips transparent during transform overlap
    ([f5cc348610](https://projects.blender.org/blender/blender/commit/f5cc3486107))
  - Add Sequence.parent\_meta() python API function
    ([eec1ea0ccf](https://projects.blender.org/blender/blender/commit/eec1ea0ccf2))
  - Set default sound and video export format
    ([70f890b510](https://projects.blender.org/blender/blender/commit/70f890b5105))
  - Add transform overwrite mode
    ([59cd9c6da6](https://projects.blender.org/blender/blender/commit/59cd9c6da682))
  - Strip thumbnails
    ([997b5fe45d](https://projects.blender.org/blender/blender/commit/997b5fe45dab))
  - Image transform tools
    ([fa2c1698b0](https://projects.blender.org/blender/blender/commit/fa2c1698b077))
  - Add color tags to strips
    ([5cebcb415e](https://projects.blender.org/blender/blender/commit/5cebcb415e76))
  - Add ASC CDL color correction method
    ([213554f24a](https://projects.blender.org/blender/blender/commit/213554f24a17))
