## Blender 2.82a: Bug Fixes

### Blender

  - Fix [\#74003](http://developer.blender.org/T74003): Autocomplete bug
    with mesh.loop\_triangles. in Blender Python Console.
    ([rB69c58788](https://projects.blender.org/blender/blender/commit/69c587888))
  - Fix [\#73898](http://developer.blender.org/T73898): UDIM crash
    changing form tiled to single
    ([rBd6977b5](https://projects.blender.org/blender/blender/commit/d6977b5))
  - Fix [\#73862](http://developer.blender.org/T73862): Fluid Sim file
    handle resource leak
    ([rB60890cc](https://projects.blender.org/blender/blender/commit/60890cc))
  - Fix [\#74182](http://developer.blender.org/T74182): Crash saving
    images from non-image spaces
    ([rB096936fe](https://projects.blender.org/blender/blender/commit/096936fe1))
  - Fix [\#72903](http://developer.blender.org/T72903): Bone envelope &
    bone size tool functionality swapped
    ([rBdd2cdd43](https://projects.blender.org/blender/blender/commit/dd2cdd436))
  - Fix [\#74278](http://developer.blender.org/T74278): camera border
    gizmo size
    ([rB9cac5fa6](https://projects.blender.org/blender/blender/commit/9cac5fa681),
    [rB360443a4](https://projects.blender.org/blender/blender/commit/360443a48))
  - Fix [\#54270](http://developer.blender.org/T54270): Reset last\_hit
    and last\_location when reading the file
    ([rB2df040ed](https://projects.blender.org/blender/blender/commit/2df040ed58fb))
  - Fix [\#74431](http://developer.blender.org/T74431): crash when
    compiling renderpass shader on some AMD drivers
    ([rB9c4523b1](https://projects.blender.org/blender/blender/commit/9c4523b1fde4)
    +
    [rBe5f98c79](https://projects.blender.org/blender/blender/commit/e5f98c79b0ed))
  - Fix [\#74295](http://developer.blender.org/T74295): Cloth + Internal
    springs crashes on a non-polygonal geometry
    ([rB1648a790](https://projects.blender.org/blender/blender/commit/1648a7903672))
  - Fix Fix (unreported) Separate bones creates empty armature
    ([rB498397f7](https://projects.blender.org/blender/blender/commit/498397f7bd8))
  - Revert "Constraints: remove special meaning of Local Space for
    parentless Objects."
    ([rBf881162f](https://projects.blender.org/blender/blender/commit/f881162f81e))
  - Fix [\#73932](http://developer.blender.org/T73932): modifying
    keyframes in nodes fails when there is an image sequence
    ([rBf0a22f5](https://projects.blender.org/blender/blender/commit/f0a22f5))
  - Fix crash loading .blend file saved with Blender 2.25
    ([rBed8aa15](https://projects.blender.org/blender/blender/commit/ed8aa15))
  - Fix potential crash with Eevee render of missing image textures
    ([rBab18dbb](https://projects.blender.org/blender/blender/commit/ab18dbb))
  - Fix [\#72253](http://developer.blender.org/T72253): Mantaflow
    adaptive domain cuts off
    ([rB32fc22db](https://projects.blender.org/blender/blender/commit/32fc22db5679))
  - Keymap: Add front/back Alt-MMB absolute view axis switching
    ([rBa2009862](https://projects.blender.org/blender/blender/commit/a200986273))
  - Fix [\#72028](http://developer.blender.org/T72028): Crash switching
    to vertex paint
    ([rB31aefdee](https://projects.blender.org/blender/blender/commit/31aefdeec5))
  - Fix bone envelopes displaying wrong when armature is scaled
    ([rBee703494](https://projects.blender.org/blender/blender/commit/ee7034949fd))
  - Fix Vertex weight gradient tool show wrong weight/strength values in
    the UI
    ([rBf38c54d5](https://projects.blender.org/blender/blender/commit/f38c54d56e))
  - Fix [\#74225](http://developer.blender.org/T74225): Image (from
    sequence) cannot be loaded
    ([rB9dbfc7ca](https://projects.blender.org/blender/blender/commit/9dbfc7ca8bf))
  - Fix [\#63892](http://developer.blender.org/T63892): Tools cannot be
    registered into some contexts (e.g. PAINT\_TEXTURE)
    ([rBd95e9c7c](https://projects.blender.org/blender/blender/commit/d95e9c7cf8))
  - Fix [\#73369](http://developer.blender.org/T73369): corner pin &
    sun-beam nodes gizmos are too big
    ([rB212660f4](https://projects.blender.org/blender/blender/commit/212660f467))
  - Fix [\#74425](http://developer.blender.org/T74425): Cannot texture
    paint an images sequence anymore
    ([rBca717f04](https://projects.blender.org/blender/blender/commit/ca717f0489))

### Add-ons

  - glTF: Fix some strange reference error in blender api when exporting
    shapekeys / ApplyModifier
    ([rBA659c121](https://projects.blender.org/blender/blender-addons/commit/659c121a68))
  - glTF: Fix crash using compositor rendering for image generation
    ([rBAaf687f5](https://projects.blender.org/blender/blender-addons/commit/af687f5a041ee))
