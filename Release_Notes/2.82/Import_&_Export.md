# Blender 2.82: Import & Export

## Universal Scene Description

USD file export is now supported, using File → Export → Universal Scene
Description. This is an initial implementation, and USD support will be
improved over time to support more complete scene interchange.
Information can be found in the [USD chapter in the Blender
manual](https://docs.blender.org/manual/en/dev/files/import_export/usd.html).

![USD file exported from Blender and shown in
USDView](../../images/Blender-2.82-USD-usdview-spring.png
"USD file exported from Blender and shown in USDView")

For developers, there is more [USD technical
documentation](../../Source/Architecture/USD.md) about the
implementation.
([ec62413f80](https://projects.blender.org/blender/blender/commit/ec62413f803ee506))

## Alembic

  - Mesh normals are now exported properly to Alembic.
    ([Manual](https://docs.blender.org/manual/en/dev/files/import_export/alembic.html#custom-split-normals-of-meshes)).
    ([b0e7a1d4b4](https://projects.blender.org/blender/blender/commit/b0e7a1d4b492))
  - Changed "Visible Layers" to "Visible Objects" in export options.
    Similarly, the Alembic exporter operator
    \`bpy.ops.wm.alembic\_export()\` changed the keyword argument
    \`visible\_layers\_only\` to \`visible\_objects\_only\`.
    ([d9e61ce195](https://projects.blender.org/blender/blender/commit/d9e61ce1953b))

## glTF

### Importer

  - Multiple files import feature
    ([rBA67e42c7](https://projects.blender.org/blender/blender-addons/commit/67e42c79e5c3))
  - Better names for UVMap and VertexColor
    ([rBA08352b3](https://projects.blender.org/blender/blender-addons/commit/08352b3af770))
  - Size of bone at creation are now based on armature scale
    ([rBA76fc414](https://projects.blender.org/blender/blender-addons/commit/76fc4142b518))
  - Import data from texture samplers + exporter fix
    ([rBA9f1b91c](https://projects.blender.org/blender/blender-addons/commit/9f1b91ca1316))
  - Use vertexcolor node instead of attributenode
    ([rBA74a8fb2](https://projects.blender.org/blender/blender-addons/commit/74a8fb2aa8f9))
  - Set custom props from extras for bones
    ([rBA0141c7f](https://projects.blender.org/blender/blender-addons/commit/0141c7f4cf49))
  - Accessor/buffer decoding enhancements
    ([rBAeb4085f](https://projects.blender.org/blender/blender-addons/commit/eb4085f1dc15),
    [rBA47bef1a](https://projects.blender.org/blender/blender-addons/commit/47bef1a8a535))
  - Import extra data as custom properties
    ([rBAecdaef9](https://projects.blender.org/blender/blender-addons/commit/ecdaef952383))
  - Fix bad vertex color alpha import
    ([rBA6087f4c](https://projects.blender.org/blender/blender-addons/commit/6087f4c26540))
  - Fix avoid crash on invalid glTF file with multiple same target path
    ([rBA9189c45](https://projects.blender.org/blender/blender-addons/commit/9189c4595844))
  - Fix import of bone rest pose of negative scaled bones
    ([rBA2b04a1a](https://projects.blender.org/blender/blender-addons/commit/2b04a1ab6829))
  - Fix bug assigning pose bone custom prop
    ([rBAfe5f339](https://projects.blender.org/blender/blender-addons/commit/fe5f339eb0b4))

### Exporter

  - Option to export only deformation bones
    ([rBAb9b1814](https://projects.blender.org/blender/blender-addons/commit/b9b1814a4c26))
  - Basic SK driver export (driven by armature animation)
    ([rBAf505743](https://projects.blender.org/blender/blender-addons/commit/f505743b2f9f))
  - Performance improvement on image export
    ([rBA289fb2b](https://projects.blender.org/blender/blender-addons/commit/289fb2b8b89e))
  - Define a user extension API
    ([rBA1824626](https://projects.blender.org/blender/blender-addons/commit/18246268e802),
    [rBA73b8594](https://projects.blender.org/blender/blender-addons/commit/73b85949a06c),
    [rBA9733d0f](https://projects.blender.org/blender/blender-addons/commit/9733d0f73654),
    [rBA8304b9b](https://projects.blender.org/blender/blender-addons/commit/8304b9bac7dc))
  - Alphabetically sorting of exported animation
    ([rBA5f1ad50](https://projects.blender.org/blender/blender-addons/commit/5f1ad50707e7))
  - Add option to export textures into a folder
    ([rBA60a11a0](https://projects.blender.org/blender/blender-addons/commit/60a11a0fc442))
  - Keep constant/step interpolation when sampled animation export
    ([rBAb90cbbd](https://projects.blender.org/blender/blender-addons/commit/b90cbbdf4801))
  - Detach last exported animation if needed
    ([rBAbad5957](https://projects.blender.org/blender/blender-addons/commit/bad59573c39e))
  - Add export support for Emission socket of Principled BSDF node
    ([rBA78fe8ec](https://projects.blender.org/blender/blender-addons/commit/78fe8ec049fa))
  - Do not split in primitives if materials are not exported
    ([rBAd709b46](https://projects.blender.org/blender/blender-addons/commit/d709b46340b5))
  - Remove primitive splitting
    ([rBA45c87c1](https://projects.blender.org/blender/blender-addons/commit/45c87c1ae354))
  - Export action custom props as animation extras
    ([rBA334ca37](https://projects.blender.org/blender/blender-addons/commit/334ca375b5e6))
  - Fix exporting area lights that are not supported
    ([rBAc172be1](https://projects.blender.org/blender/blender-addons/commit/c172be1c93a6))
  - Fix exporting instances when apply modifier is disabled
    ([rBAc23d2a7](https://projects.blender.org/blender/blender-addons/commit/c23d2a741e21))
  - Fix crash when armature action is linked to mesh object
    ([rBA7003720](https://projects.blender.org/blender/blender-addons/commit/7003720257c9),
    [rBAa6ce1b1](https://projects.blender.org/blender/blender-addons/commit/a6ce1b121eb7))
  - Fix transforms for mesh parented directly to bones
    ([rBAa96fa1e](https://projects.blender.org/blender/blender-addons/commit/a96fa1e6111a))
  - Fix skinning export when using draco compression
    ([rBA1470f35](https://projects.blender.org/blender/blender-addons/commit/1470f353c650))
  - Fix extension required parameter check
    ([rBA619f3b5](https://projects.blender.org/blender/blender-addons/commit/619f3b5b2d8c))
  - Fix animation that was removed when using both TRS + SK animation
    ([rBA24cc252](https://projects.blender.org/blender/blender-addons/commit/24cc252dc6f6))
  - Fix Draco bug when exporting Blender instances
    ([rBAd504a09](https://projects.blender.org/blender/blender-addons/commit/d504a09ab5ea))
  - Fix buffer size with Draco compression
    ([rBAdc02819](https://projects.blender.org/blender/blender-addons/commit/dc0281955383))
  - Allow draco compression when exporting without normals
    ([rBA5367eba](https://projects.blender.org/blender/blender-addons/commit/5367ebad4813))
  - Fix export of light with shader node
    ([rBA329f2c4](https://projects.blender.org/blender/blender-addons/commit/329f2c4a8b9f))
  - Fix normals when changed by modifiers
    ([rBA6238248](https://projects.blender.org/blender/blender-addons/commit/6238248739dc),
    [rBA3a774be](https://projects.blender.org/blender/blender-addons/commit/3a774beff73d))
  - Fix avoid exporting rotation twice when both euler and quaternion
    ([rBA97b1185](https://projects.blender.org/blender/blender-addons/commit/97b11857d320))

## VFX Reference Platform

Blender is now compatible with the [VFX Reference
Platform](https://vfxplatform.com) 2020 for library dependencies and
Python. This helps improve compatibility between Blender and other VFX
software, especially now that the VFX industry is also switching to
Python 3.
