# Blender 2.82: Animation & Rigging

## Bones

  - New ['Aligned' Inherit
    Scale](https://docs.blender.org/manual/en/dev/animation/armatures/bones/properties/relations.html#bone-relations-inherit-settings)
    option that always applies parent X scale to child X scale and so
    on, ignoring the orientation difference between the child and
    parent.
    ([c23dbcf3](https://projects.blender.org/blender/blender/commit/c23dbcf3))

## Constraints

  - Stretch To: new
    ['Swing'](https://docs.blender.org/manual/en/dev/animation/constraints/tracking/stretch_to.html#options)
    rotation mode that uses a Damped Track style rotation to track the
    target.
    ([aadc90d0](https://projects.blender.org/blender/blender/commit/aadc90d0))
  - Action: new [Mix
    Mode](https://docs.blender.org/manual/en/dev/animation/constraints/relationship/action.html#options)
    option to change the rotation mix order similar to other
    constraints.
    ([33eabb82](https://projects.blender.org/blender/blender/commit/33eabb82))

<!-- end list -->

  - Compatibility: for an object without a parent, Local Space and World
    Space now do the same thing. The old undocumented behavior when
    using Local space on parentless objects was removed
    ([7728bfd4](https://projects.blender.org/blender/blender/commit/7728bfd4)).
    As a result, constraints using Local Space on objects now behave the
    same as on bones in an armature. Previously Local Space of
    parentless objects was interpreted as "World Space but then with the
    object's own rotation also applied".

## Playblast Only Keyframes

In the 3D Viewport, use View → Viewport Render Keyframes for rendering a
playblast with just keyframes.
([7dd6e034f1](https://projects.blender.org/blender/blender/commit/7dd6e034f1ca))

This will render keyframes of selected objects, and for other frames
repeat the last keyframe. This can be used for faster playblast
rendering, or to see just the blocked animation.

## F-Curve Selection and Transform

Selection and transform in the graph editor has been revamped to be more
powerful and intuitive.
([b037ba2665](https://projects.blender.org/blender/blender/commit/b037ba2665f4)).

  - Handles now always move with the key, regardless if they are
    selected or not.
  - Selecting the key doesn't select the handles anymore.
  - Multiple keys and handles can be dragged.
  - Dragging a handle moves all selected handles on the same side.
  - Tweak-dragging any handle can never affect any keyframe location,
    only handles.
  - Changing the handle type with a key selected always applies the
    change to both handles.
  - Box selection with
    <span class="hotkeybg"><span class="hotkey">Ctrl</span><span class="hotkey">Tweak</span></span>
    now allows deselecting handles (used to act on entire triple only).
  - Box selection Include Handles option now only acts on visible
    handles, wasn't the case with Only Selected Keyframes Handles
    enabled.
  - Box selection Include Handles is now enabled by default in all
    bundled keymaps.

## F-Curve Decimation

The new f-curve decimate operator reduces the number of keyframes while
preserving the overall curves as well as possible.
([8bc57e5b91](https://projects.blender.org/blender/blender/commit/8bc57e5b91eb))
([7868db9343](https://projects.blender.org/blender/blender/commit/7868db9343d5))

![../../videos/Decimate\_Fcurve\_Demo.mp4](../../videos/Decimate_Fcurve_Demo.mp4
"../../videos/Decimate_Fcurve_Demo.mp4")
