# Blender 2.82 Release Notes

Blender 2.82 was released on 14 February, 2020.

Check out the final [release notes on
blender.org](https://www.blender.org/download/releases/2-82/).

## [2.82a Corrective Release](a.md)

This corrective release does not include new features and only fixes a
few serious regressions introduced in 2.82 release.

## [Physics](Physics.md)

Mantaflow fluid simulation, cloth internal air pressure, cloth internal
springs.

## [Cycles](Cycles.md)

OptiX denoiser, improvements to shader nodes, custom render passes, and
more.

## [EEVEE](EEVEE.md)

Viewport render passes, and a lot of important fixes.

## [Textures](Textures.md)

UDIM textures for texture painting and rendering in Eevee and Cycles.

## [Sculpt & Paint](Sculpt.md)

Slide/relax tool, multiplane scrape tool, operator improvements, pose
brush and brush updates, paint improvements.

## [Grease Pencil](Grease_Pencil.md)

![../../images/Multiple\_Stroke\_Modifier.png](../../images/Multiple_Stroke_Modifier.png
"../../images/Multiple_Stroke_Modifier.png")

User interface, improved tools, multiple strokes modifier.

  

## [Modeling](Modeling.md)

![../../images/Custom\_Bevel\_Profile\_Release\_Notes.png](../../images/Custom_Bevel_Profile_Release_Notes.png
"../../images/Custom_Bevel_Profile_Release_Notes.png")

Custom profiles for bevel, cutoff vertex mesh, weld modifier and
solidify modifier.

  

## [User Interface](UI.md)

![../../images/Bevel\_gizmo.png](../../images/Bevel_gizmo.png
"../../images/Bevel_gizmo.png")

Many tools now have a gizmo, like bevel and the UV editor.

  

## [Animation & Rigging](Rigging.md)

Improvements to bones, constraints and F-curves.

## [Import & Export](Import_&_Export.md)

![../../images/Blender-2.82-USD-usdview-spring.png](../../images/Blender-2.82-USD-usdview-spring.png
"../../images/Blender-2.82-USD-usdview-spring.png")

New USD exporter, many glTF improvements, Alembic mesh normals export,
VFX platform 2020 compatibility.

  

## [Python API](Python_API.md)

Python API changes, Custom curve profile and widget, changes to handling
of Python environment.

## [Add-ons](Add-ons.md)

Updates to Rigify, Add Mesh Extra objects, BlenderKit, and new add-ons
Import Palettes, PDT, Add Node Presets, Collection manager, Sun position
and Amaranth toolkit.
