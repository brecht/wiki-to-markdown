# Blender 2.82: EEVEE

## Viewport Render Passes

In the 3d viewport an option was added to preview an EEVEE render pass.
Render passes that are supported are

  - Combined
  - Ambient Occlusion
  - Normal
  - Mist
  - Subsurface Direct
  - Subsurface Color

In the future more render passes will be added.

## Volumetric with partial transparency fix

A bug was fixed that changes how Alpha Blended surfaces reacts to
volumetric effect. The old behavior was adding more light if the surface
was partially transparent.

<center>

|                                                                                                                                            |                                                                                                                                               |
| ------------------------------------------------------------------------------------------------------------------------------------------ | --------------------------------------------------------------------------------------------------------------------------------------------- |
| ![Capture\_d’écran\_du\_2020-02-04\_16-13-17.png](Capture_d’écran_du_2020-02-04_16-13-17.png "Capture_d’écran_du_2020-02-04_16-13-17.png") | ![../../images/EEVEE\_282\_volume\_fix\_after.png](../../images/EEVEE_282_volume_fix_after.png "../../images/EEVEE_282_volume_fix_after.png") |

style="caption-side: bottom" | Mr Elephant demo file lamps before and
after the fix

</center>

This fix will alter the appearance of old file, there is no workaround
other than to tweak the affected materials.

([39d5d11e02](https://projects.blender.org/blender/blender/commit/39d5d11e022a))

## Improvements

  - Group node sockets now converts the values as in Cycles.
    ([c2e21b2329](https://projects.blender.org/blender/blender/commit/c2e21b23296))
  - Curve, surface and text object can now use normal mapping without
    workaround.
    ([15350c70be](https://projects.blender.org/blender/blender/commit/15350c70be8)
    [c27d30f3ea](https://projects.blender.org/blender/blender/commit/c27d30f3eaf)
    [bcacf47cbc](https://projects.blender.org/blender/blender/commit/bcacf47cbcf))
  - Disk lights artifact appearing on certain drivers have been fixed.
    ([3d73609832](https://projects.blender.org/blender/blender/commit/3d73609832d))
  - Volumetric objects with zero volume do not fill the scene anymore.
    ([0366c46ec6](https://projects.blender.org/blender/blender/commit/0366c46ec64))
  - "Bake Cubemap Only" operator does not reset the Irradiance Volume
    after file load or if world is updated.
    ([5d5add5de2](https://projects.blender.org/blender/blender/commit/5d5add5de2b))
  - Improved quality of render passes with high samples count
    ([17b63db4e2](https://projects.blender.org/blender/blender/commit/17b63db4e272))
  - Improved performance when calculating render passes
    ([9d7f65630b](https://projects.blender.org/blender/blender/commit/9d7f65630b20))
  - Improved performance during viewport rendering
    ([7959dcd4f6](https://projects.blender.org/blender/blender/commit/7959dcd4f631))
