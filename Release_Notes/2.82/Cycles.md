# Blender 2.82: Cycles

## Denoising

Cycles now supports the AI-Accelerated Denoiser from OptiX. It is built
into the Blender view layer system, supports multiple GPUs (the denoiser
will use the selected OptiX devices also used for rendering) and can
give realistic results with low numbers of samples at very high
performance.

This denoiser is less suited for animations, because not temporarily
stable, but is considerably faster than the existing denoising options
and therefore especially useful to denoise previews or final
single-frame images with high quality.

<center>

|                                                                                                                                                                                |                                                                                                                                                                             |
| ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| ![../../images/Blender2.82\_classroom\_10\_optix\_before.png](../../images/Blender2.82_classroom_10_optix_before.png "../../images/Blender2.82_classroom_10_optix_before.png") | ![../../images/Blender2.82\_classroom\_10\_optix\_after.png](../../images/Blender2.82_classroom_10_optix_after.png "../../images/Blender2.82_classroom_10_optix_after.png") |

style="caption-side: bottom" | Before and after denoising, with 10
samples

</center>

To use this feature you need a NVIDIA RTX GPU and at least driver 441.87
(Windows) or 440.59 (Linux). Currently it can only be used when
rendering with OptiX too.

## Shader Nodes

Shader nodes for Cycles and Eevee have been extended.

  - Map Range node now has interpolation modes : linear, stepped linear,
    [smoothstep](https://en.wikipedia.org/wiki/Smoothstep) and
    [smootherstep](https://en.wikipedia.org/wiki/Smoothstep)
    ([958d0d4236](https://projects.blender.org/blender/blender/commit/958d0d4236b1c))
  - Math node has new operations: trunc, snap, wrap, compare, pingpong,
    sign, radians, degrees, cosh, sinh, tanh, exp, smoothmin and
    inversesqrt.
    ([0406eb1103](https://projects.blender.org/blender/blender/commit/0406eb1103))
  - Noise and Wave texture nodes distortion was improved to distort
    uniformly in all directions, instead of diagonally. This change is
    not strictly backwards compatible, the resulting pattern is a little
    different.
    ([074c00f9d6](https://projects.blender.org/blender/blender/commit/074c00f9d6))
  - Geometry node now has a Random Per Island option, to randomize
    textures or colors for different components within a mesh.
    ([1c2f7b022a](https://projects.blender.org/blender/blender/commit/1c2f7b022a))

## Shader AOVs

Custom render passes are now supported. They are added in the Shader
AOVs panel in the view layer settings, with a name and data type. In
shader nodes, an AOV Output node is then used to output either a value
or color to the pass.

## Improvements

  - The Denoising Albedo pass was improved to work better with the
    OpenImageDenoise compositor node.
    ([4659fa5471](https://projects.blender.org/blender/blender/commit/4659fa5471))
  - The Denoising Normal pass was changed to output in camera space to
    work better with the OptiX denoiser (when "Color + Albedo + Normal"
    is selected as denoiser input).
    ([d5ca72191c](https://projects.blender.org/blender/blender/commit/d5ca72191c36))
  - BVH build time on Windows has been significantly reduced by using a
    better memory allocator, making it similar to Linux and macOS
    performance.
    ([d60a60f0cb](https://projects.blender.org/blender/blender/commit/d60a60f0cb))
  - Direct and indirect light clamping has been changed to clamp per
    light, instead of for the whole path. Clamp values on existing
    scenes will need to be tweaked to get similar results.
    ([3437c9c](https://projects.blender.org/blender/blender/commit/3437c9c))
