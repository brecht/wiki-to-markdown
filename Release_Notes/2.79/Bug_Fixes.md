# Blender 2.79: Bug Fixes

Changes from revision
[27c64e3f46](https://projects.blender.org/blender/blender/commit/27c64e3f46)
to
[f934f54b84](https://projects.blender.org/blender/blender/commit/f934f54b84),
inclusive (*master* branch).

\[RC2\] Changes from revision
[5e9132b3b7](https://projects.blender.org/blender/blender/commit/5e9132b3b7)
to
[7b397cdfc8](https://projects.blender.org/blender/blender/commit/7b397cdfc8),
inclusive (*blender-v2.79-release* branch).

\[Release\] Changes from revision
[beea9421bd](https://projects.blender.org/blender/blender/commit/beea9421bd)
to
[8ef39d5c88](https://projects.blender.org/blender/blender/commit/8ef39d5c88),
inclusive (*blender-v2.79-release* branch).

Total fixed bugs: 789 (416 from tracker, 373 reported/found by other
ways).

'''Note: '''Before RC1 (i.e. during regular development of next version
in master branch), only fixes of issues which already existed in
previous official releases are listed here. Fixes for regressions
introduced since last release, or for new features, are **not** listed
here.  
For following RCs and final release, **all** backported fixes are
listed.

## Objects / Animation / GP

### Animation

  - Fix [\#50026](http://developer.blender.org/T50026): "Only Insert
    Needed" doesn't work when using Trackball rotation
    ([rB8b290595](https://projects.blender.org/blender/blender/commit/8b2905952e)).
  - Fix [\#49816](http://developer.blender.org/T49816): Keyframing NLA
    Strip influence from Python set keyframes in the wrong place
    ([rB3cbe95f6](https://projects.blender.org/blender/blender/commit/3cbe95f683)).
  - Fix [\#50842](http://developer.blender.org/T50842): NLA Influence
    Curve draws out of bounds when it exceeds the 0-1 range
    ([rB91ce13e9](https://projects.blender.org/blender/blender/commit/91ce13e90d)).
  - Fix [\#50904](http://developer.blender.org/T50904): Imprecise
    timeline frame selection using mouse
    ([rB17689f8b](https://projects.blender.org/blender/blender/commit/17689f8bb6)).
  - Fix [\#51867](http://developer.blender.org/T51867): Insert Keyframe
    I - L / I - R / I - S key combos are broken
    ([rB7abed4e4](https://projects.blender.org/blender/blender/commit/7abed4e433)).
  - Fix [\#51792](http://developer.blender.org/T51792): crash calling
    bpy.ops.screen.animation\_cancel while scrubbing
    ([rBf51438ae](https://projects.blender.org/blender/blender/commit/f51438aea0)).
  - Fix [\#50973](http://developer.blender.org/T50973): Directional blur
    node doesn't clamp value if using driver
    ([rB00d47697](https://projects.blender.org/blender/blender/commit/00d476976e)).
  - Fix [\#51879](http://developer.blender.org/T51879): NLA Influence
    can not be autokeyed
    ([rBb4520030](https://projects.blender.org/blender/blender/commit/b452003015)).
  - Fix [\#52009](http://developer.blender.org/T52009): F-Curve "Stepped
    interpolation" modifier "restrict frame-range" IN and OUT parameters
    cannot be edited
    ([rBfdfcbfd0](https://projects.blender.org/blender/blender/commit/fdfcbfd040)).
  - Fix [\#52058](http://developer.blender.org/T52058): Jumping to
    keyframe causes Blender to freeze indefinitely
    ([rB7021aa24](https://projects.blender.org/blender/blender/commit/7021aa245d)).
  - Fix [\#52213](http://developer.blender.org/T52213): Enum drivers no
    longer work
    ([rB87d5e344](https://projects.blender.org/blender/blender/commit/87d5e34453)).
  - \[RC2\] Fix [\#52327](http://developer.blender.org/T52327):
    Entering/Exiting NLA Tweakmode disables Scene -\> Only Keyframes
    from Selected Channels
    ([rB3689be73](https://projects.blender.org/blender/blender/commit/3689be736b)).
  - \[RC2\] Fix [\#52401](http://developer.blender.org/T52401): "Export
    Keying Set" operator generated incorrect ID's for shapekeys
    ([rB7b397cdf](https://projects.blender.org/blender/blender/commit/7b397cdfc8)).
  - \[Release\] Fix [\#52227](http://developer.blender.org/T52227): Time
    Slide tool doesn't take NLA mapping into account
    ([rB73cdf00e](https://projects.blender.org/blender/blender/commit/73cdf00ea8)).

<!-- end list -->

  - Fix unreported: Fixes for pose library change 601ce6a89c4
    ([rB305fec83](https://projects.blender.org/blender/blender/commit/305fec8358)).
  - Fix unreported: Depsgraph: Fix some errors printed to the console
    ([rB630c0559](https://projects.blender.org/blender/blender/commit/630c0559f9)).
  - Fix unreported: Also apply similar fixes to .keyframe\_delete()
    ([rBb1c6ddb1](https://projects.blender.org/blender/blender/commit/b1c6ddb107)).
  - Fix unreported: Fix: NLA "Strip Time" setting cannot be edited
    ([rB65582e75](https://projects.blender.org/blender/blender/commit/65582e75e3)).
  - Fix unreported: fix: redraw dope sheet / action editor when pose
    bone selection changes
    ([rB502c4be5](https://projects.blender.org/blender/blender/commit/502c4be56e)).
  - Fix unreported: A bunch of fixes for Pose Library while checking on
    [\#51607](http://developer.blender.org/T51607)
    ([rB085ed00e](https://projects.blender.org/blender/blender/commit/085ed00e42)).
  - Fix unreported: Fix fcurve color assignment
    ([rB2b7edb77](https://projects.blender.org/blender/blender/commit/2b7edb77c9)).
  - \[Release\] Fix unreported: Fix: GPencil Sequence Interpolation for
    thickness/strength was inverted
    ([rBed0429e7](https://projects.blender.org/blender/blender/commit/ed0429e7e6)).
  - \[Release\] Fix unreported: Fix: Border select for GPencil keyframes
    was including those in the "datablock" channels even though those
    weren't visible
    ([rBf5d02f05](https://projects.blender.org/blender/blender/commit/f5d02f055f)).

### Constraints

  - Fix [\#49981](http://developer.blender.org/T49981): When camera is
    on inactive layer, it does not evaluate constraints
    ([rB24676b53](https://projects.blender.org/blender/blender/commit/24676b535a)).

<!-- end list -->

  - Fix unreported: Fix (unreported) missing update when adding
    constraint from RNA
    ([rBd66d5790](https://projects.blender.org/blender/blender/commit/d66d5790e9)).
  - Fix unreported: Fix/workaround
    [\#50677](http://developer.blender.org/T50677): Shrinkwrap
    constraint don't get updated when target mesh gets modified
    ([rB2342cd0a](https://projects.blender.org/blender/blender/commit/2342cd0a0f)).
  - Fix unreported: Fix another part of
    [\#50565](http://developer.blender.org/T50565): Planar constraints
    were always initialized to accurate transform
    ([rB87f8bb8d](https://projects.blender.org/blender/blender/commit/87f8bb8d1d)).

### Grease Pencil

  - Fix [\#50051](http://developer.blender.org/T50051): Avoid crash when
    render grease pencil from VSE
    ([rBe400f4a5](https://projects.blender.org/blender/blender/commit/e400f4a53e)).
  - Fix [\#50081](http://developer.blender.org/T50081): Grease pencil
    parented rotation problem
    ([rBdd82d70b](https://projects.blender.org/blender/blender/commit/dd82d70bc5)).
  - Fix [\#50123](http://developer.blender.org/T50123): GreasePencil:
    Modifying name of new color in new palette via bpy segfaults
    ([rBe2d22346](https://projects.blender.org/blender/blender/commit/e2d223461e)).
  - Fix [\#50264](http://developer.blender.org/T50264): Stroke is not
    painted when append Grease Pencil block
    ([rB535298ee](https://projects.blender.org/blender/blender/commit/535298eea5)).
  - Fix [\#50184](http://developer.blender.org/T50184): Grease Pencil
    Layer synchronization between Dope Sheet and Properties panel
    ([rBc5338fd1](https://projects.blender.org/blender/blender/commit/c5338fd162)).
  - Fix [\#49617](http://developer.blender.org/T49617): Grease Pencil
    Stroke Placement regression
    ([rB15215652](https://projects.blender.org/blender/blender/commit/15215652e1)).
  - \[Release\] Fix [\#52483](http://developer.blender.org/T52483): Fill
    is incorrect for interpolated strokes
    ([rB27c42e05](https://projects.blender.org/blender/blender/commit/27c42e05c4)).
  - \[Release\] Fix [\#52678](http://developer.blender.org/T52678):
    Crash editing gpencil w/ frame-lock
    ([rB3aaf9087](https://projects.blender.org/blender/blender/commit/3aaf908719)).
  - \[Release\] Fix [\#52650](http://developer.blender.org/T52650):
    Grease pencil selection its not automatically updating in Clip
    Editor
    ([rB87cc8550](https://projects.blender.org/blender/blender/commit/87cc8550e2)).

<!-- end list -->

  - Fix unreported: Fix Grease Pencil render in VSE crashes when no
    strips (\#[\#49975](http://developer.blender.org/T49975))
    ([rB0b9b8ab2](https://projects.blender.org/blender/blender/commit/0b9b8ab2dd)).
  - Fix unreported: Grease Pencil: Fix python errors opening N panel -\>
    GP with empty VSE
    ([rB89c1f9db](https://projects.blender.org/blender/blender/commit/89c1f9db37)).
  - Fix unreported: GPencil: Fix unreported error in animation after
    rename items
    ([rB196520fe](https://projects.blender.org/blender/blender/commit/196520fe7d)).
  - Fix unreported: Fix: Avoid creating redundant frames when erasing
    ([rB7452af0f](https://projects.blender.org/blender/blender/commit/7452af0f86)).
  - Fix unreported: Fix: Make it possible to erase strokes (on other
    layers) even if the active layer doesn't have any frames
    ([rB00edc600](https://projects.blender.org/blender/blender/commit/00edc600b0)).
  - Fix unreported: Fix minor glitches in GP code
    ([rB475d536f](https://projects.blender.org/blender/blender/commit/475d536f72)).
  - Fix unreported: GPencil: Fix unreported animation data missing when
    change palette name
    ([rB520afa29](https://projects.blender.org/blender/blender/commit/520afa2962)).
  - Fix unreported: Fix: GPencil delete operators did not respect color
    locking
    ([rB117d90b3](https://projects.blender.org/blender/blender/commit/117d90b3da)).
  - Fix unreported: Fix missing NULL check in gpencil poll
    ([rBcb6ec44f](https://projects.blender.org/blender/blender/commit/cb6ec44fc7)).
  - Fix unreported: GPencil Copy/Paste Fix: Copying/Pasting strokes
    between datablocks would crash
    ([rB89d4164f](https://projects.blender.org/blender/blender/commit/89d4164f54)).
  - Fix unreported: GP Copy/Paste Fix: Paste button doesn't update after
    copying strokes using Ctrl-C
    ([rB7667040d](https://projects.blender.org/blender/blender/commit/7667040dd0)).
  - Fix unreported: Fix: Pasting GP strokes files between files (or when
    the original colors were deleted) would crash
    ([rB2bd49785](https://projects.blender.org/blender/blender/commit/2bd49785f9)).
  - Fix unreported: Fix: GP Clone brush was not correcting color
    references for pasted strokes either
    ([rBd25ab3fc](https://projects.blender.org/blender/blender/commit/d25ab3fcc4)).
  - Fix unreported: Grease Pencil: Fix hardcoded DKEY for continous
    drawing
    ([rBc2d285f0](https://projects.blender.org/blender/blender/commit/c2d285f0ff)).
  - \[Release\] Fix unreported: Fix: Deleting GPencil keyframes in
    DopeSheet didn't redraw the view
    ([rBa1e8ef26](https://projects.blender.org/blender/blender/commit/a1e8ef264f)).

### Objects

  - Fix [\#49743](http://developer.blender.org/T49743): Adding torus in
    edit mode local mode shows error
    ([rB216a3a38](https://projects.blender.org/blender/blender/commit/216a3a3826)).
  - Fix [\#50008](http://developer.blender.org/T50008): camera DOF
    Distance picking from W key menu not working
    ([rB43703fa4](https://projects.blender.org/blender/blender/commit/43703fa4bf)).
  - Fix [\#50078](http://developer.blender.org/T50078): Vertex Groups
    not copied over when making proxy
    ([rB8c93178c](https://projects.blender.org/blender/blender/commit/8c93178c96)).
  - Fix [\#49718](http://developer.blender.org/T49718): Wrong "Make
    Duplicates Real" behavior with "Keep Hierarchy"
    ([rB99c5c8be](https://projects.blender.org/blender/blender/commit/99c5c8befc)).
  - Fix [\#50369](http://developer.blender.org/T50369): Objects can't be
    deleted from scene when using "link group objects to scene"
    ([rB351a9d08](https://projects.blender.org/blender/blender/commit/351a9d084f)).
  - Fix [\#50084](http://developer.blender.org/T50084): Adding torus
    re-orders UV layers
    ([rB9f67367f](https://projects.blender.org/blender/blender/commit/9f67367f0a)).
  - Fix [\#49860](http://developer.blender.org/T49860): Copying vgroups
    between objects sharing the same obdata was not possible
    ([rBf3a7104a](https://projects.blender.org/blender/blender/commit/f3a7104adb)).
  - Fix [\#51625](http://developer.blender.org/T51625): fix
    impossibility to delete uninstantiated objects from Outliner
    ([rB08b79554](https://projects.blender.org/blender/blender/commit/08b7955415)).
  - Fix [\#51657](http://developer.blender.org/T51657): ID user count
    error when deleting a newly created object with an assigned
    dupli\_group
    ([rBf212bfa3](https://projects.blender.org/blender/blender/commit/f212bfa3cd)).
  - Fix [\#51883](http://developer.blender.org/T51883): Wrong matrix
    computation in "Make Duplicates Real"
    ([rB44397a7a](https://projects.blender.org/blender/blender/commit/44397a7a0a)).

<!-- end list -->

  - Fix unreported: Fix (unreported) asserts in
    \`make\_object\_duplilist\_real()\`
    ([rB17fb504b](https://projects.blender.org/blender/blender/commit/17fb504bcf)).
  - Fix unreported: Fix UV layer bug in
    object\_utils.object\_data\_add()
    ([rBb8710e14](https://projects.blender.org/blender/blender/commit/b8710e1468)).
  - Fix unreported: Fix Torus default UV's offset outside 0-1 bounds
    ([rBd30a0239](https://projects.blender.org/blender/blender/commit/d30a0239a2)).
  - Fix unreported: Fix memory leak when making duplicates real and
    parent had constraints
    ([rBcd5c8533](https://projects.blender.org/blender/blender/commit/cd5c853307)).
  - Fix unreported: Fix race condition invalidating object data's
    bounding box
    ([rBe6954a5a](https://projects.blender.org/blender/blender/commit/e6954a5a7c)).
  - Fix unreported: MakeLocal: fix bad (missing) handling of proxy\_from
    uglyness
    ([rB622ce567](https://projects.blender.org/blender/blender/commit/622ce5672a)).

### Dependency Graph

  - Fix [\#49836](http://developer.blender.org/T49836): Partial Camera
    DOF properties not updating via graph editor
    ([rB42a91f7a](https://projects.blender.org/blender/blender/commit/42a91f7ad8)).
  - Fix [\#49826](http://developer.blender.org/T49826): NEW-DEPSGRAPH -
    Texture is not updated after changing its space color
    ([rB647255db](https://projects.blender.org/blender/blender/commit/647255db93)).
  - Fix [\#49993](http://developer.blender.org/T49993): Indirectly used
    taper/bevel crashes new dependency graph
    ([rB7dda3cf8](https://projects.blender.org/blender/blender/commit/7dda3cf830)).
  - Fix [\#49994](http://developer.blender.org/T49994): Setting
    dupligroup which uses indirect relation will crash
    ([rB7d33d443](https://projects.blender.org/blender/blender/commit/7d33d4439f)).
  - Fix [\#50060](http://developer.blender.org/T50060): New depsgraph
    does not update mask animation
    ([rB1f8762bb](https://projects.blender.org/blender/blender/commit/1f8762bb8e)).
  - Fix [\#49981](http://developer.blender.org/T49981): New Depsgraph -
    When camera is on inactive layer, it does not evaluate constraints
    ([rB25c534f2](https://projects.blender.org/blender/blender/commit/25c534f20a)).
  - Fix [\#50512](http://developer.blender.org/T50512): Linked Backround
    scene with animation not updating with new depsgraph
    ([rBe29a6f73](https://projects.blender.org/blender/blender/commit/e29a6f739d)).
  - Fix [\#50331](http://developer.blender.org/T50331): New Dependency
    Graph - "frame" python driver expression not working
    ([rBa90622ce](https://projects.blender.org/blender/blender/commit/a90622ce93)).
  - Fix [\#50938](http://developer.blender.org/T50938): Cache not being
    reset when changing simulation settings with new depsgraph
    ([rB9ad252d1](https://projects.blender.org/blender/blender/commit/9ad252d157)).
  - Fix [\#51318](http://developer.blender.org/T51318): Non-update of
    preview when switching from world to lamp panel
    ([rB849e77b1](https://projects.blender.org/blender/blender/commit/849e77b1f9)).
  - Fix [\#52134](http://developer.blender.org/T52134): New depsgraph
    crashes when evaluating several psys on from object
    ([rB4d670340](https://projects.blender.org/blender/blender/commit/4d67034076)).
  - \[RC2\] Fix [\#52255](http://developer.blender.org/T52255): New
    Depsgraph - Constraint and Drivers not working together when the
    driver references itself
    ([rBb1d998ec](https://projects.blender.org/blender/blender/commit/b1d998ec5d)).
  - \[Release\] Fix [\#52454](http://developer.blender.org/T52454):
    Crash in DEG\_graph\_on\_visible\_update when activating scene layer
    ([rB63e21e72](https://projects.blender.org/blender/blender/commit/63e21e7218)).
  - \[Release\] Fix [\#51907](http://developer.blender.org/T51907): New
    Depsgraph - Camera constraint is not evaluated properly
    ([rBedded659](https://projects.blender.org/blender/blender/commit/edded659c6)).
  - \[Release\] Fix [\#52209](http://developer.blender.org/T52209): New
    Depsgraph - animated follow curve constraint sometimes freaks out
    when the curve has a parent
    ([rB0bee1269](https://projects.blender.org/blender/blender/commit/0bee126977)).

<!-- end list -->

  - Fix unreported: Depsgraph: Fix race condition writing drivers to
    array property
    ([rB643c5a24](https://projects.blender.org/blender/blender/commit/643c5a24d5)).
  - Fix unreported: Depsgraph: Fix wrong comparison of ID type vs. node
    type
    ([rBfc1b35e4](https://projects.blender.org/blender/blender/commit/fc1b35e44c)).
  - Fix unreported: Depsgraph: Fix wrong relation from IK solver to pole
    target
    ([rBaef66a6b](https://projects.blender.org/blender/blender/commit/aef66a6be0)).
  - Fix unreported: Depsgraph: Fix another issue which seems to be a bug
    ([rBf0d53ac1](https://projects.blender.org/blender/blender/commit/f0d53ac109)).
  - Fix unreported: Depsgraph: Fix wrong relations in array modifier
    ([rB4710fa70](https://projects.blender.org/blender/blender/commit/4710fa700c)).
  - Fix unreported: Depsgraph: Fix missing ID node tag
    ([rB653541ea](https://projects.blender.org/blender/blender/commit/653541ea78)).
  - Fix unreported: Depsgraph: Fix typo in text on curve relation
    builder
    ([rBb1743cda](https://projects.blender.org/blender/blender/commit/b1743cda5a)).
  - Fix unreported: Depsgraph: Fix missing DONE flag in relations
    builder
    ([rB24d89a1f](https://projects.blender.org/blender/blender/commit/24d89a1f77)).
  - Fix unreported: Depsgraph: Fix frash with iTaSC solver
    ([rBaf0e6b31](https://projects.blender.org/blender/blender/commit/af0e6b31a5)).
  - Fix unreported: Depsgrpah: Fix missing animation update in movie
    clips
    ([rBd294509d](https://projects.blender.org/blender/blender/commit/d294509dd8)).
  - Fix unreported: Fix copy/paste typo in new depsgraph object geometry
    builder (found by coverity)
    ([rBb859fef6](https://projects.blender.org/blender/blender/commit/b859fef670)).
  - Fix unreported: Depsgraph: Fix infinite viewport object update in
    CYcles render mode
    ([rB5eab3b07](https://projects.blender.org/blender/blender/commit/5eab3b079f)).
  - Fix unreported: Depsgraph: Fix matrix\_world driver source
    ([rBf2b57c35](https://projects.blender.org/blender/blender/commit/f2b57c3532)).
  - Fix unreported: Fix depsgraph: hair collision is actually enabled,
    so add the relations
    ([rB60dae91d](https://projects.blender.org/blender/blender/commit/60dae91db8)).
  - Fix unreported: Depsgraph: Fix duplicated operation node when two
    objects are sharing same armature
    ([rB605695de](https://projects.blender.org/blender/blender/commit/605695de61)).
  - Fix unreported: Depsgraph: Fixed crash with curve bevel indirect
    dupligroups
    ([rB0434053f](https://projects.blender.org/blender/blender/commit/0434053f13)).
  - Fix unreported: Depsgraph: Fix missing relations update tag when
    typing \#frame
    ([rBdc500764](https://projects.blender.org/blender/blender/commit/dc5007648c)).
  - Fix unreported: Depsgraph: Fix missing updates when in local view
    ([rB843be910](https://projects.blender.org/blender/blender/commit/843be91002)).
  - Fix unreported: Fix missing relation in new DEG between World and
    its NodeTree
    ([rB42c34602](https://projects.blender.org/blender/blender/commit/42c346028f)).
  - Fix unreported: Depsgraph: Fix/workaround crahs when fcu-\>rna\_path
    is NULL
    ([rB8eeb6108](https://projects.blender.org/blender/blender/commit/8eeb610832)).
  - Fix unreported: Depsgraph: Fix object being tagged for data update
    when it shouldn't
    ([rB34cb9343](https://projects.blender.org/blender/blender/commit/34cb934343)).
  - Fix unreported: Depsgraph: Fix missing relations for objects which
    are indirectly linked
    ([rBd1d359b7](https://projects.blender.org/blender/blender/commit/d1d359b792)).

## Data / Geometry

### Armatures

  - Fix [\#50023](http://developer.blender.org/T50023): Inverse
    Kinematics angle limits defaulting to 10313.2403124°
    ([rB63973196](https://projects.blender.org/blender/blender/commit/6397319659)).
  - Fix [\#50393](http://developer.blender.org/T50393): Flip names
    working just on one side when both are selected
    ([rB702bc5ba](https://projects.blender.org/blender/blender/commit/702bc5ba26)).
  - Fix [\#49527](http://developer.blender.org/T49527): Blender stalls
    when changing armature ghosting range with stepsize = 0
    ([rBd2382f78](https://projects.blender.org/blender/blender/commit/d2382f782e)).
  - Fix [\#50932](http://developer.blender.org/T50932): depth picking w/
    pose-bone constraints
    ([rB810982a9](https://projects.blender.org/blender/blender/commit/810982a95c)).
  - Fix [\#51965](http://developer.blender.org/T51965): Custom Bone
    Shape Wireframe setting affects wireframe thickness
    ([rBf86b43e1](https://projects.blender.org/blender/blender/commit/f86b43e130)).
  - Fix [\#51955](http://developer.blender.org/T51955): Changing Auto-IK
    length crashes Blender (with new-depsgraph)
    ([rBa7a5c20f](https://projects.blender.org/blender/blender/commit/a7a5c20fbc)).
  - Fix [\#51971](http://developer.blender.org/T51971): IK non-uniform
    scale regression
    ([rBf05f2f03](https://projects.blender.org/blender/blender/commit/f05f2f0336)).
  - Fix [\#52224](http://developer.blender.org/T52224): auto IK not
    respecting length, after recent bugfix
    ([rBbecb413f](https://projects.blender.org/blender/blender/commit/becb413f29)).

<!-- end list -->

  - Fix unreported: Depsgraph: Fix crash deleting bones in armature edit
    mode
    ([rB4d9562a3](https://projects.blender.org/blender/blender/commit/4d9562a3ae)).
  - Fix unreported: Fix crash happening in some cases with MakeLocal
    operator
    ([rBed957768](https://projects.blender.org/blender/blender/commit/ed957768a0)).
  - Fix unreported: Fix (unreported) crash when drawing armatures' poses
    in some cases
    ([rB1be717d0](https://projects.blender.org/blender/blender/commit/1be717d007)).

### Curve/Text Editing

  - Fix [\#50614](http://developer.blender.org/T50614): Curve doesn't
    restore initial form after deleting all its shapekeys
    ([rB6f1493f6](https://projects.blender.org/blender/blender/commit/6f1493f68f)).
  - Fix [\#50745](http://developer.blender.org/T50745): Shape key
    editing on bezier objects broken with Rendered Viewport Shading
    ([rB5e1d4714](https://projects.blender.org/blender/blender/commit/5e1d4714fe)).
  - Fix [\#51149](http://developer.blender.org/T51149): Joining curves
    allows 2D curves in 3D
    ([rB97c9c6a3](https://projects.blender.org/blender/blender/commit/97c9c6a3f3)).
  - Fix [\#51350](http://developer.blender.org/T51350): 2D curve normals
    flip when deformed
    ([rBf78ba0df](https://projects.blender.org/blender/blender/commit/f78ba0df02)).
  - Fix [\#52007](http://developer.blender.org/T52007): Cancel bezier
    curve transform fails
    ([rB2acd05b2](https://projects.blender.org/blender/blender/commit/2acd05b24c)).
  - Fix [\#51665](http://developer.blender.org/T51665): No orientation
    for nurbs, polygons
    ([rB004a1437](https://projects.blender.org/blender/blender/commit/004a143760)).
  - Fix [\#39925](http://developer.blender.org/T39925): Set Origin to
    Geometry on a new text object moves the origin away from the object
    ([rB06505c52](https://projects.blender.org/blender/blender/commit/06505c5264)).

<!-- end list -->

  - Fix unreported: Fix missing break setting curve auto-handles
    ([rB40639821](https://projects.blender.org/blender/blender/commit/406398213c)).
  - Fix unreported: Fix serious bug in 'curve-to-mesh' conversion code
    ([rB7a80c34f](https://projects.blender.org/blender/blender/commit/7a80c34f52)).

### Mesh Editing

  - Fix [\#50046](http://developer.blender.org/T50046): Segmentation
    fault due to out-of-range VertexGroup.weight() call
    ([rB209bc997](https://projects.blender.org/blender/blender/commit/209bc9977c)).
  - Fix [\#50003](http://developer.blender.org/T50003): Bevel makes
    non-manifold mesh
    ([rB1de79c89](https://projects.blender.org/blender/blender/commit/1de79c8960)).
  - Fix [\#49848](http://developer.blender.org/T49848): bevel of spiral
    gets bigger and bigger widths
    ([rB3d243eb7](https://projects.blender.org/blender/blender/commit/3d243eb710)).
  - Fix [\#49807](http://developer.blender.org/T49807): Inset faces edge
    rail bug
    ([rB1455023e](https://projects.blender.org/blender/blender/commit/1455023e64)).
  - Fix [\#50534](http://developer.blender.org/T50534): Part I, cleanup
    loop normals generated during modifier stack evaluation
    ([rBfb2f95c9](https://projects.blender.org/blender/blender/commit/fb2f95c91a)).
  - Fix [\#50524](http://developer.blender.org/T50524): Basis shapekey
    editing while rendering bug
    ([rB9e2dca93](https://projects.blender.org/blender/blender/commit/9e2dca933e)).
  - Fix [\#50662](http://developer.blender.org/T50662): Auto-split
    affects on smooth mesh when it sohuldn't
    ([rBefbe47f9](https://projects.blender.org/blender/blender/commit/efbe47f9cd)).
  - Fix [\#50855](http://developer.blender.org/T50855): Intersect
    (knife) w/o separate doesn't select
    ([rB3caeb51d](https://projects.blender.org/blender/blender/commit/3caeb51d7f)).
  - Fix [\#50920](http://developer.blender.org/T50920): Adds missing
    edges on return of bisect operator
    ([rB2b3cc243](https://projects.blender.org/blender/blender/commit/2b3cc24388)).
  - Fix [\#50950](http://developer.blender.org/T50950): Converting
    meshes fails w/ boolean
    ([rB750c0dd4](https://projects.blender.org/blender/blender/commit/750c0dd4de)).
  - Fix [\#51038](http://developer.blender.org/T51038):
    \`layerInterp\_mloopcol\` was casting instead of rounding the
    interpolated RGBA channels
    ([rBd23459f5](https://projects.blender.org/blender/blender/commit/d23459f516)).
  - Fix [\#51072](http://developer.blender.org/T51072): The reference of
    a pyobject may be being overwritten in
    \`bm\_mesh\_remap\_cd\_update\`
    ([rBd097c2a1](https://projects.blender.org/blender/blender/commit/d097c2a1b3)).
  - Fix [\#51135](http://developer.blender.org/T51135): Cylinder
    primitive generated bad UVs
    ([rBd426c335](https://projects.blender.org/blender/blender/commit/d426c335c5)).
  - Fix [\#51137](http://developer.blender.org/T51137): Edge Rip Hangs
    ([rBa0799ce3](https://projects.blender.org/blender/blender/commit/a0799ce336)).
  - Fix [\#51184](http://developer.blender.org/T51184): Crash of Blender
    when I try to join an object with one that has booleans modifiers
    ([rB6b815ae5](https://projects.blender.org/blender/blender/commit/6b815ae55d)).
  - Fix [\#51284](http://developer.blender.org/T51284): Mesh not skinned
    ([rB573f6d1e](https://projects.blender.org/blender/blender/commit/573f6d1ec4)).
  - Fix [\#51180](http://developer.blender.org/T51180): BMesh crash
    using calc\_uvs=True
    ([rBdf94f2f3](https://projects.blender.org/blender/blender/commit/df94f2f399)).
  - Fix [\#49467](http://developer.blender.org/T49467): Crash due to
    assert failure in bevel
    ([rB8be9d68d](https://projects.blender.org/blender/blender/commit/8be9d68dd4)).
  - Fix [\#51539](http://developer.blender.org/T51539): BMesh boolean
    crash
    ([rB208462e4](https://projects.blender.org/blender/blender/commit/208462e424)).
  - Fix [\#51520](http://developer.blender.org/T51520): Broken vertex
    weights after two mesh joining
    ([rBe3d63215](https://projects.blender.org/blender/blender/commit/e3d6321530)).
  - Fix [\#48668](http://developer.blender.org/T48668): bevel mistake on
    presumed reflex angle
    ([rB49cc78ef](https://projects.blender.org/blender/blender/commit/49cc78ef18)).
  - Fix [\#51637](http://developer.blender.org/T51637): Mesh Tools -
    Noise Button Crashes
    ([rB275e2fb0](https://projects.blender.org/blender/blender/commit/275e2fb0ff)).
  - Fix [\#48996](http://developer.blender.org/T48996): bevel bad for
    certain in-plane edges
    ([rB9ba7805a](https://projects.blender.org/blender/blender/commit/9ba7805a3b)).
  - Fix [\#51520](http://developer.blender.org/T51520): Broken vertex
    weights after two mesh joining
    ([rB2fb56e71](https://projects.blender.org/blender/blender/commit/2fb56e7157)).
  - Fix [\#50906](http://developer.blender.org/T50906): and
    [\#49361](http://developer.blender.org/T49361), bevel didn't curve
    in plane sometimes
    ([rB538fe7d4](https://projects.blender.org/blender/blender/commit/538fe7d48e)).
  - Fix [\#51733](http://developer.blender.org/T51733): 3d print toolbox
    checks report false positives
    ([rBc71e160c](https://projects.blender.org/blender/blender/commit/c71e160c02)).
  - Fix [\#51648](http://developer.blender.org/T51648): Inconsistent
    edge collapse point depending on orientation
    ([rBab4b7b53](https://projects.blender.org/blender/blender/commit/ab4b7b5368)).
  - Fix [\#51856](http://developer.blender.org/T51856):
    \`BKE\_mesh\_new\_from\_object()\` would often generate default
    'Mesh' named datablock
    ([rBaf354559](https://projects.blender.org/blender/blender/commit/af35455912)).
  - Fix [\#51900](http://developer.blender.org/T51900): Crash after
    pressing "F" multiple times
    ([rB2c0ee61f](https://projects.blender.org/blender/blender/commit/2c0ee61f6d)).
  - Fix [\#51100](http://developer.blender.org/T51100): Vertex pick
    fails after extrude
    ([rBd3f7b04f](https://projects.blender.org/blender/blender/commit/d3f7b04f04)).
  - Fix [\#52066](http://developer.blender.org/T52066): Grid Mesh UV's
    aren't correct
    ([rB49c29dc8](https://projects.blender.org/blender/blender/commit/49c29dc82f)).
  - Fix [\#52176](http://developer.blender.org/T52176): Bevel doesn't
    correctly work with default empty Vgroup
    ([rB66e28a28](https://projects.blender.org/blender/blender/commit/66e28a2827)).
  - \[RC2\] Fix [\#52329](http://developer.blender.org/T52329): Boolean
    with aligned shapes failed
    ([rB0cb38b28](https://projects.blender.org/blender/blender/commit/0cb38b2875)).
  - \[Release\] Fix [\#52434](http://developer.blender.org/T52434):
    Restore mesh center of mass calculation
    ([rBba600ff7](https://projects.blender.org/blender/blender/commit/ba600ff7fa)).
  - \[Release\] Fix [\#52588](http://developer.blender.org/T52588):
    Shape key value driver variables of duplicated object sets refer to
    old objects
    ([rBe9ca9dd5](https://projects.blender.org/blender/blender/commit/e9ca9dd5d7)).
  - \[Release\] Fix [\#52251](http://developer.blender.org/T52251):
    Knife cur displaces surface
    ([rBa8c7f132](https://projects.blender.org/blender/blender/commit/a8c7f1329b)).
  - \[Release\] Fix [\#52701](http://developer.blender.org/T52701): Mesh
    shortest path fails at boundaries
    ([rB904831e6](https://projects.blender.org/blender/blender/commit/904831e62e)).

<!-- end list -->

  - Fix unreported: BMesh: fix edge-rotation selection state bug
    ([rBd3919c22](https://projects.blender.org/blender/blender/commit/d3919c22b0)).
  - Fix unreported: BMesh: fix edge-rotate with w/ flipped faces
    ([rB77ba1ed5](https://projects.blender.org/blender/blender/commit/77ba1ed5db)).
  - Fix unreported: Fix (unreported) looptri array not being
    recalculated in ccgDM and emDM
    ([rB7fe7835d](https://projects.blender.org/blender/blender/commit/7fe7835d13)).
  - Fix unreported: Fix face-creation with existing hidden geometry
    ([rB11187e86](https://projects.blender.org/blender/blender/commit/11187e8628)).
  - Fix unreported: Fix wrong loop normals left after face splitting
    ([rB20283bfa](https://projects.blender.org/blender/blender/commit/20283bfa0b)).
  - Fix unreported: Fix wrong edges created by split faces
    ([rB40e5bc15](https://projects.blender.org/blender/blender/commit/40e5bc15e9)).
  - Fix unreported: Fix more corner cases failing in mesh faces split
    ([rB9b3d415f](https://projects.blender.org/blender/blender/commit/9b3d415f6a)).
  - Fix unreported: Fix useless allocation of edge\_vectors in threaded
    case of loop split generation
    ([rB9d6acc34](https://projects.blender.org/blender/blender/commit/9d6acc34a1)).
  - Fix unreported: BMesh: Fix BM\_face\_loop\_separate\_multi
    ([rB304315d1](https://projects.blender.org/blender/blender/commit/304315d181)).
  - Fix unreported: Fix unreported: inaccuracy of interpolation of
    custom color layers due to float truncation
    ([rBa6f74453](https://projects.blender.org/blender/blender/commit/a6f74453b6)).
  - Fix unreported: Fix skin mark operator
    ([rBdd662c74](https://projects.blender.org/blender/blender/commit/dd662c74ae)).
  - Fix unreported: Fix possible invalid normal use w/ tangent calc
    ([rBd252ac6b](https://projects.blender.org/blender/blender/commit/d252ac6b95)).
  - Fix unreported: Fix error accessing tessface color in edit-mode
    ([rBa033a7be](https://projects.blender.org/blender/blender/commit/a033a7bef9)).
  - Fix unreported: Fix freeing all custom-data layers
    ([rBac66fb19](https://projects.blender.org/blender/blender/commit/ac66fb193f)).
  - Fix unreported: Fix/workaround 'convert object' messing up linked
    data
    ([rB880e96dd](https://projects.blender.org/blender/blender/commit/880e96dd66)).
  - Fix unreported: Fix topology mirror ignoring center verts
    ([rB6e90294e](https://projects.blender.org/blender/blender/commit/6e90294e08)).
  - \[Release\] Fix unreported: Fix minor Mesh -\> BMesh conversion
    issues
    ([rB6fec0692](https://projects.blender.org/blender/blender/commit/6fec06926e)).

### Meta Editing

  - \[RC2\] Fix [\#52324](http://developer.blender.org/T52324): Metaball
    disappears when deleting first metaball object
    ([rBff47118c](https://projects.blender.org/blender/blender/commit/ff47118c73)).

### Modifiers

  - Fix [\#50216](http://developer.blender.org/T50216): Missing checks
    caused data transfer segfault
    ([rB2e15618f](https://projects.blender.org/blender/blender/commit/2e15618f49)).
  - Fix [\#50373](http://developer.blender.org/T50373): lattices should
    not be able to get subsurf modifiers
    ([rB378afc98](https://projects.blender.org/blender/blender/commit/378afc9830)).
  - Fix [\#50534](http://developer.blender.org/T50534): Part II: warn
    user when DataTransfer mod affects custom normals
    ([rB11abb134](https://projects.blender.org/blender/blender/commit/11abb13483)).
  - Fix [\#50830](http://developer.blender.org/T50830): Wrong context
    when calling surfacedeform\_bind
    ([rB85607761](https://projects.blender.org/blender/blender/commit/856077618a)).
  - Fix [\#50838](http://developer.blender.org/T50838): Surface Deform
    DM use after free issue
    ([rB2089a17f](https://projects.blender.org/blender/blender/commit/2089a17f7e)).
  - Fix [\#50899](http://developer.blender.org/T50899): Alternate
    ([rB2fbc50e4](https://projects.blender.org/blender/blender/commit/2fbc50e4c1)).
  - Fix [\#51012](http://developer.blender.org/T51012): Surface modifier
    was not working with curves
    ([rB13d86615](https://projects.blender.org/blender/blender/commit/13d8661503)).
  - Fix [\#50851](http://developer.blender.org/T50851): Array modifier
    generating invalid geometry
    ([rB4d58080e](https://projects.blender.org/blender/blender/commit/4d58080e23)).
  - Fix [\#51890](http://developer.blender.org/T51890): Turning off
    viewport display for array modifier is blocking dupliface in final
    render
    ([rB0210c6b9](https://projects.blender.org/blender/blender/commit/0210c6b9de)).
  - Fix [\#52212](http://developer.blender.org/T52212): Vgroups doesn't
    work after Carve Boolean
    ([rBd41acacc](https://projects.blender.org/blender/blender/commit/d41acacc61)).
  - \[RC2\] Fix [\#51701](http://developer.blender.org/T51701): Alembic
    cache screws up mesh
    ([rB8138082a](https://projects.blender.org/blender/blender/commit/8138082ab8)).
  - \[Release\] Fix [\#52478](http://developer.blender.org/T52478):
    Error report "Shrinkwrap: out of memory" on invisible target
    ([rBa5213924](https://projects.blender.org/blender/blender/commit/a5213924a8)).
  - \[Release\] Fix [\#52149](http://developer.blender.org/T52149):
    LoopTriArray computation was not correctly protected against
    concurrency
    ([rB9ae35faf](https://projects.blender.org/blender/blender/commit/9ae35fafb6)).

<!-- end list -->

  - Fix unreported: Fix potential NULL dereference in mesh sequence
    cache modifier
    ([rB841c4dee](https://projects.blender.org/blender/blender/commit/841c4deed7)).
  - Fix unreported: Fix (IRC reported) DataTransfer modifier affecting
    base mesh in some cases
    ([rBe4e19000](https://projects.blender.org/blender/blender/commit/e4e1900012)).
  - Fix unreported: Fix Surface Deform crash with missing or freed DM
    ([rB0a032ce8](https://projects.blender.org/blender/blender/commit/0a032ce83b)).
  - Fix unreported: Fix Surface Deform not unbinding if target is
    removed
    ([rBca958642](https://projects.blender.org/blender/blender/commit/ca958642fa)).
  - Fix unreported: Fix (unreported) memleak in Warp modifier copying
    ([rB5b683812](https://projects.blender.org/blender/blender/commit/5b6838129f)).
  - Fix unreported: Fix integer overflows in meshcache modifier
    ([rBbddd9d80](https://projects.blender.org/blender/blender/commit/bddd9d809d)).
  - Fix unreported: Fix (unreported) Dynamic Paint modifier not
    increasing ID usercount in copy function
    ([rB0d5c7e5e](https://projects.blender.org/blender/blender/commit/0d5c7e5e36)).
  - Fix unreported: Fix (unreported) bad copying code in Mesh Deform
    modifier
    ([rBe917bc5e](https://projects.blender.org/blender/blender/commit/e917bc5ee0)).
  - Fix unreported: Fix (unreported) bad copying of Ocean modifier
    ([rB1addac8e](https://projects.blender.org/blender/blender/commit/1addac8e0c)).
  - Fix unreported: Fix (unreported) bad copying code of Surface Deform
    modifier
    ([rB24486513](https://projects.blender.org/blender/blender/commit/24486513d5)).
  - Fix unreported: Opensubdiv: Fix compilation error with older
    Opensubdiv versions
    ([rB1dfc4be6](https://projects.blender.org/blender/blender/commit/1dfc4be6ab)).

### Material / Texture

  - Fix [\#50590](http://developer.blender.org/T50590): BI lamp doesn't
    hold a texture in this case
    ([rB66630998](https://projects.blender.org/blender/blender/commit/6663099810)).
  - Fix [\#51913](http://developer.blender.org/T51913): Context tab for
    textures issue
    ([rBf11bcbed](https://projects.blender.org/blender/blender/commit/f11bcbed9d)).
  - Fix [\#51746](http://developer.blender.org/T51746): normal map
    tangents not working correctly when there are no UV maps
    ([rBe3e5ecfa](https://projects.blender.org/blender/blender/commit/e3e5ecfaa8)).
  - Fix [\#51951](http://developer.blender.org/T51951): cell noise
    texture precision issue at unit vertex coordinates
    ([rB3232d8ec](https://projects.blender.org/blender/blender/commit/3232d8ec8f)).
  - Fix [\#51734](http://developer.blender.org/T51734): batch-generate
    previews crashes on some materials
    ([rB0c122d64](https://projects.blender.org/blender/blender/commit/0c122d64d2)).
  - Fix [\#52034](http://developer.blender.org/T52034): cell noise
    renders different
    ([rB5c30bc28](https://projects.blender.org/blender/blender/commit/5c30bc285c)).

<!-- end list -->

  - Fix unreported: Fix reading past bounds removing from color ramp
    ([rB57bcc19b](https://projects.blender.org/blender/blender/commit/57bcc19bb3)).
  - Fix unreported: Fix (unreported) memory leak in Fluid modifier
    copying
    ([rBe9aaf5ed](https://projects.blender.org/blender/blender/commit/e9aaf5ed21)).

## Physics / Simulations / Sculpt / Paint

### Particles

  - Fix [\#51390](http://developer.blender.org/T51390): Blender 2.78c
    will freeze or force close when adding particles at random
    ([rB06ca2c9d](https://projects.blender.org/blender/blender/commit/06ca2c9d03)).
  - Fix [\#51523](http://developer.blender.org/T51523): Lattice modifier
    affecting particles even when disabled
    ([rB56422ff6](https://projects.blender.org/blender/blender/commit/56422ff6c3)).
  - Fix [\#51991](http://developer.blender.org/T51991): Disabled for
    viewport particle system becomes visible after BI render
    ([rB4d4ebc83](https://projects.blender.org/blender/blender/commit/4d4ebc8300)).

### Physics / Hair / Simulations

  - Fix [\#50141](http://developer.blender.org/T50141): Nabla zero
    division on texture force field
    ([rBb18f83bc](https://projects.blender.org/blender/blender/commit/b18f83bcf4)).
  - Fix [\#50350](http://developer.blender.org/T50350): Quick Explode
    time frame problem
    ([rBc0c48cda](https://projects.blender.org/blender/blender/commit/c0c48cdacc)).
  - Fix [\#50056](http://developer.blender.org/T50056): Dyntopo brush
    size shortcut broken using constant detail setting
    ([rB092cbcd1](https://projects.blender.org/blender/blender/commit/092cbcd1d2)).
  - Fix [\#50634](http://developer.blender.org/T50634): Hair Primitive
    as Triangles + Hair shader with a texture = crash
    ([rBf1b21d59](https://projects.blender.org/blender/blender/commit/f1b21d5960)).
  - Fix [\#51176](http://developer.blender.org/T51176): Cache file
    location can be blank and prevent fluid simulation from reading
    baked data
    ([rB46213923](https://projects.blender.org/blender/blender/commit/4621392353)).
  - Fix [\#45350](http://developer.blender.org/T45350): Cache not being
    recomputed with "Bake All Dynamics"
    ([rBee3faddf](https://projects.blender.org/blender/blender/commit/ee3faddfaa)).
  - Fix [\#51297](http://developer.blender.org/T51297): Use A Higher
    Hair Segment Limit
    ([rB46215836](https://projects.blender.org/blender/blender/commit/4621583612)).
  - Fix [\#51524](http://developer.blender.org/T51524): Instantiated
    Hair Object which has dupligroup children and hidden orig objects of
    group crash at render
    ([rBb6f5e8d9](https://projects.blender.org/blender/blender/commit/b6f5e8d9a1)).
  - Fix [\#51774](http://developer.blender.org/T51774): Children
    particles hair interpolation not correct with textures or dp
    ([rB3190eaf1](https://projects.blender.org/blender/blender/commit/3190eaf109)).
  - Fix [\#50887](http://developer.blender.org/T50887): Holes in fluid
    mesh on Windows
    ([rB9c2bbfb6](https://projects.blender.org/blender/blender/commit/9c2bbfb6ce)).
  - Fix [\#50230](http://developer.blender.org/T50230): Rigid Body
    simulation shouldn't step when time is beyond cached area
    ([rB9cd6b031](https://projects.blender.org/blender/blender/commit/9cd6b03187)).
  - Fix [\#51759](http://developer.blender.org/T51759): fluid simulation
    particles not remoevd when colliding with outflow objects
    ([rB405121d6](https://projects.blender.org/blender/blender/commit/405121d613)).
  - Fix [\#51703](http://developer.blender.org/T51703): Rigid body with
    delta transform jumps when transforming
    ([rBd1dfd5fa](https://projects.blender.org/blender/blender/commit/d1dfd5fa26)).
  - Fix [\#51296](http://developer.blender.org/T51296): UVs not working
    for hair emitted from vertices
    ([rBfed853ea](https://projects.blender.org/blender/blender/commit/fed853ea78)).
  - Fix [\#51977](http://developer.blender.org/T51977): New depsgraph
    removes the grass in victor scene
    ([rB1e217782](https://projects.blender.org/blender/blender/commit/1e21778261)).
  - Fix [\#52156](http://developer.blender.org/T52156): Hair dynamics
    broken with density texture
    ([rB9b22dbcc](https://projects.blender.org/blender/blender/commit/9b22dbcc0d)).
  - \[RC2\] Fix [\#52344](http://developer.blender.org/T52344): Softbody
    on Text
    ([rBefa840f9](https://projects.blender.org/blender/blender/commit/efa840f99f)).
  - \[RC2\] Fix [\#52344](http://developer.blender.org/T52344): Softbody
    on Text
    ([rB9a239eea](https://projects.blender.org/blender/blender/commit/9a239eea68)).
  - \[Release\] Fix [\#52439](http://developer.blender.org/T52439):
    Crash after adjusting lenght of hair particles
    ([rBd2f20aed](https://projects.blender.org/blender/blender/commit/d2f20aed04)).
  - \[Release\] Fix [\#52498](http://developer.blender.org/T52498):
    Deleting force field doesn't remove "Surface" from modifier stack
    ([rB82a6889d](https://projects.blender.org/blender/blender/commit/82a6889d83)).
  - \[Release\] Fix [\#52374](http://developer.blender.org/T52374):
    Changes of rigid body related settings during simulation will break
    the simulation
    ([rBd84f5595](https://projects.blender.org/blender/blender/commit/d84f559555)).

<!-- end list -->

  - Fix unreported: Viewport smoke: fix a couple of issues in the new
    display settings
    ([rBae69986b](https://projects.blender.org/blender/blender/commit/ae69986b70)).
  - Fix unreported: Fix memory leak when Blender is build without Bullet
    and files with RB is opened
    ([rB0085001e](https://projects.blender.org/blender/blender/commit/0085001eb0)).
  - Fix unreported: Dynamic Paint: Fix random pixel flooding by absolute
    brush with spread
    ([rBb86042f2](https://projects.blender.org/blender/blender/commit/b86042f21a)).
  - Fix unreported: Fix missing hair after rendering with different
    viewport/render settings
    ([rB03544ecc](https://projects.blender.org/blender/blender/commit/03544eccb4)).
  - Fix unreported: Fix part of
    [\#50634](http://developer.blender.org/T50634): Hair Primitive as
    Triangles + Hair shader with a texture = crash
    ([rB209a6411](https://projects.blender.org/blender/blender/commit/209a64111e)).
  - Fix unreported: Fix hair\_step is a short
    ([rB12651aba](https://projects.blender.org/blender/blender/commit/12651aba03)).
  - Fix unreported: Fix rigid body not resimulating after cache
    invalidation
    ([rB3edc8c1f](https://projects.blender.org/blender/blender/commit/3edc8c1f9b)).
  - \[Release\] Fix unreported: Rigidbody: Fix regression introduced in
    ee3fadd
    ([rBe91f9f66](https://projects.blender.org/blender/blender/commit/e91f9f664d)).

### Sculpting / Painting

  - Fix [\#50035](http://developer.blender.org/T50035): Minor interface
    bug: UV/ImageEditor - Paint Mode - Fill Brush
    ([rB27de0c40](https://projects.blender.org/blender/blender/commit/27de0c40c5)).
  - Fix [\#51761](http://developer.blender.org/T51761): wpaint select
    depth limit fails
    ([rBa247b367](https://projects.blender.org/blender/blender/commit/a247b367b0)).
  - Fix [\#46560](http://developer.blender.org/T46560): 2D paint smear
    and soften brushes not working with alpha
    ([rB0d4fd752](https://projects.blender.org/blender/blender/commit/0d4fd7528f)).
  - Fix [\#50039](http://developer.blender.org/T50039): texture paint
    soften strength not working with float images
    ([rBb379f02f](https://projects.blender.org/blender/blender/commit/b379f02f20)).
  - \[Release\] Fix [\#52639](http://developer.blender.org/T52639):
    Weight paint smooth tool crash
    ([rB300abf24](https://projects.blender.org/blender/blender/commit/300abf241e)).
  - \[Release\] Fix [\#52696](http://developer.blender.org/T52696):
    Sculpt - Brush spacing pressure artifacts
    ([rB9a9e9b1c](https://projects.blender.org/blender/blender/commit/9a9e9b1c4d)).

<!-- end list -->

  - Fix unreported: Fix (unreported) bad handling of brush's fill
    threshold value
    ([rBf6083b7b](https://projects.blender.org/blender/blender/commit/f6083b7bcd)).
  - Fix unreported: Fix brush menu broken before adding uv and texture
    data
    ([rB157a8727](https://projects.blender.org/blender/blender/commit/157a8727b5)).
  - Fix unreported: Fix texture paint crash when painting onto stencil
    ([rB248bba81](https://projects.blender.org/blender/blender/commit/248bba81e7)).

## Image / Video / Render

### Image / UV Editing

  - Fix [\#49343](http://developer.blender.org/T49343):
    ImageFormatSettings were not setting their rna struct path correctly
    ([rBf64aa4e0](https://projects.blender.org/blender/blender/commit/f64aa4e0af)).
  - Fix [\#49391](http://developer.blender.org/T49391): Texture paint is
    not aware of disabled color management
    ([rB075a2175](https://projects.blender.org/blender/blender/commit/075a2175d5)).
  - Fix [\#49815](http://developer.blender.org/T49815): Blender always
    reverts to RGBA when using Save As Image
    ([rBa39ab9cf](https://projects.blender.org/blender/blender/commit/a39ab9cfde)).
  - Fix [\#50071](http://developer.blender.org/T50071): Radience HDR
    fomat does not support alpha at all
    ([rBb6c0edcb](https://projects.blender.org/blender/blender/commit/b6c0edcb09)).
  - Fix [\#50020](http://developer.blender.org/T50020): adding a
    background image does not set image user data
    ([rB8ce6de3b](https://projects.blender.org/blender/blender/commit/8ce6de3bdd)).
  - Fix [\#50122](http://developer.blender.org/T50122): SEGFAULT: OCIO
    configuration typo leads to segfault
    ([rB1b9cae9d](https://projects.blender.org/blender/blender/commit/1b9cae9d04)).
  - Fix [\#50115](http://developer.blender.org/T50115): stereoscopic
    video file memory leak
    ([rBe5482f98](https://projects.blender.org/blender/blender/commit/e5482f986c)).
  - Fix [\#51187](http://developer.blender.org/T51187): Memory leak when
    exporting OpenEXR monochrome animation
    ([rB2836003f](https://projects.blender.org/blender/blender/commit/2836003f6b)).
  - Fix [\#51153](http://developer.blender.org/T51153): Video duration
    is detected wrong after FFmpeg update
    ([rB8ca9fa5f](https://projects.blender.org/blender/blender/commit/8ca9fa5fd3)).
  - Fix [\#50673](http://developer.blender.org/T50673): OpenEXR
    multilayer image truncated on save
    ([rBd59721c2](https://projects.blender.org/blender/blender/commit/d59721c2c3)).
  - Fix [\#51609](http://developer.blender.org/T51609): Bake Texture,
    Margin crashing Blender
    ([rBbf5e717e](https://projects.blender.org/blender/blender/commit/bf5e717ef5)).
  - Fix [\#51587](http://developer.blender.org/T51587): Blender fails to
    interpret a specific layer in OpenEXR multilayer file
    ([rB556942ec](https://projects.blender.org/blender/blender/commit/556942ec07)).
  - \[RC2\] Fix [\#52334](http://developer.blender.org/T52334): images
    with non-color data should not change color space on save
    ([rB35f5d80f](https://projects.blender.org/blender/blender/commit/35f5d80f3a)).

<!-- end list -->

  - Fix unreported: Dynamic Paint: Fix adjacency computations at UV
    seams
    ([rB9a66d0ad](https://projects.blender.org/blender/blender/commit/9a66d0ad1b)).
  - Fix unreported: FFmpeg: Fix off by one error in number of detected
    frames in matroska container
    ([rBa5424250](https://projects.blender.org/blender/blender/commit/a54242503e)).
  - Fix unreported: Fix (unreported) memleak in ImBuf mipmap code in
    some cases
    ([rBa4c65584](https://projects.blender.org/blender/blender/commit/a4c6558481)).
  - Fix unreported: Fix (unreported) crash in 'Match movie length
    operator' in case of lost video texture file
    ([rBb47c912f](https://projects.blender.org/blender/blender/commit/b47c912f4b)).
  - Fix unreported: Image: Fix non-deterministic behavior of image
    sequence loading
    ([rBe76364ad](https://projects.blender.org/blender/blender/commit/e76364adcd)).
  - Fix unreported: Fix (unreported): Crash if a right click is
    performed on an image datablock (open btn f.e.)
    ([rB894513c7](https://projects.blender.org/blender/blender/commit/894513c7fd)).
  - Fix unreported: Fix memory leak when saving OpenEXR files
    ([rB26b23231](https://projects.blender.org/blender/blender/commit/26b2323189)).
  - Fix unreported: Fix crash from freeing of NULL pointer
    ([rB0a6c57d3](https://projects.blender.org/blender/blender/commit/0a6c57d3d2)).
  - Fix unreported: Partial fix of
    [\#51989](http://developer.blender.org/T51989): Don't set image user
    offset for movie files
    ([rB9a4cfc3e](https://projects.blender.org/blender/blender/commit/9a4cfc3e77)).

### Masking

  - Fix [\#50062](http://developer.blender.org/T50062): Mask - Clicking
    in ActivePoint Parent makes Blender crash
    ([rB0c958b9f](https://projects.blender.org/blender/blender/commit/0c958b9f8e)).
  - Fix [\#51388](http://developer.blender.org/T51388): Mask moves when
    zoom is changed in the movie clip editor
    ([rB47f8459e](https://projects.blender.org/blender/blender/commit/47f8459ead)).

<!-- end list -->

  - Fix unreported: Masks: Fix broken animation after adding primitives
    ([rB294ffa0d](https://projects.blender.org/blender/blender/commit/294ffa0d49)).
  - Fix unreported: Fix (unreported) missing Image usercount increase
    when copying UVProject modifier
    ([rBe3b1d562](https://projects.blender.org/blender/blender/commit/e3b1d562a7)).

### Motion Tracking

  - Fix [\#51158](http://developer.blender.org/T51158): Motion Tracking
    Movie clip editor graph
    ([rBa40f15d0](https://projects.blender.org/blender/blender/commit/a40f15d04f)).
  - Fix [\#50908](http://developer.blender.org/T50908): Motion Tracker
    ignored grease pencil mask
    ([rBb0015686](https://projects.blender.org/blender/blender/commit/b0015686e2)).
  - Fix [\#51980](http://developer.blender.org/T51980): Motion Tracking
    - png image files appear in the Blender program directory when using
    refine
    ([rBaaec4ed0](https://projects.blender.org/blender/blender/commit/aaec4ed07d)).
  - Fix [\#51978](http://developer.blender.org/T51978): Setup Tracking
    Scene after Motion Tracking fails the first time
    ([rBd25ccf83](https://projects.blender.org/blender/blender/commit/d25ccf83ad)).

<!-- end list -->

  - Fix unreported: Libmv: Fix copy-paste mistake in camera intrinsic
    parameters
    ([rB356dacea](https://projects.blender.org/blender/blender/commit/356dacea90)).
  - Fix unreported: Libmv: Fix missing virtual destructor in frame
    access sub-class
    ([rB1c34a7f4](https://projects.blender.org/blender/blender/commit/1c34a7f4eb)).
  - Fix unreported: Fix float buffer of tracking image accessed outside
    of check that it has been correctly allocated
    ([rB21e12822](https://projects.blender.org/blender/blender/commit/21e1282265)).
  - Fix unreported: Libmv: Fix crash of keyframe selection on 32bit
    linux
    ([rBa1f8755d](https://projects.blender.org/blender/blender/commit/a1f8755d32)).
  - Fix unreported: Tracking: Fix use-after-free bug
    ([rB58f3b3c6](https://projects.blender.org/blender/blender/commit/58f3b3c6d1)).

### Nodes / Compositor

  - Fix [\#49857](http://developer.blender.org/T49857): Blender crashes
    after adding texture node to compositing tree
    ([rB534f11f7](https://projects.blender.org/blender/blender/commit/534f11f71e)).
  - Fix [\#50736](http://developer.blender.org/T50736): Zero streaks in
    Glare node
    ([rB9dd19471](https://projects.blender.org/blender/blender/commit/9dd194716b)).
  - Fix [\#50656](http://developer.blender.org/T50656): Compositing node
    editor is empty, no nodes can be added
    ([rB9eb647f1](https://projects.blender.org/blender/blender/commit/9eb647f1c8)).
  - Fix [\#50849](http://developer.blender.org/T50849): Transparent
    background produces artifacts in this compositing setup
    ([rB817e975d](https://projects.blender.org/blender/blender/commit/817e975dee)).
  - Fix [\#51455](http://developer.blender.org/T51455): Render Layers in
    compositor from a different scene not working
    ([rBb82954f6](https://projects.blender.org/blender/blender/commit/b82954f6f4)).
  - Fix [\#51449](http://developer.blender.org/T51449): empty node
    editor continuously redrawing and using CPU
    ([rB868678c8](https://projects.blender.org/blender/blender/commit/868678c85f)).
  - Fix [\#51348](http://developer.blender.org/T51348): Node
    highlighting is broken
    ([rB1f96dd2e](https://projects.blender.org/blender/blender/commit/1f96dd2e0b)).
  - Fix [\#51308](http://developer.blender.org/T51308): Bright/Contrast
    Doesn't respect Pre-multiplied Alpha
    ([rB8cc4c3da](https://projects.blender.org/blender/blender/commit/8cc4c3da8c)).
  - Fix [\#51687](http://developer.blender.org/T51687): GPUmat
    evaluation of shader tree would crash uppon unknown/unsupported
    socket types
    ([rB528ae888](https://projects.blender.org/blender/blender/commit/528ae8885e)).
  - Fix [\#51840](http://developer.blender.org/T51840): UI redraw in
    node editor header missing on pointcache bake
    ([rBf158a206](https://projects.blender.org/blender/blender/commit/f158a206f2)).
  - Fix [\#51863](http://developer.blender.org/T51863):
    CompositorNodeSwitchView have the wrong rna API
    ([rBbe4b5551](https://projects.blender.org/blender/blender/commit/be4b5551c7)).
  - Fix [\#52092](http://developer.blender.org/T52092): Crash
    un-grouping nodes
    ([rB3daa641d](https://projects.blender.org/blender/blender/commit/3daa641d7f)).
  - Fix [\#52232](http://developer.blender.org/T52232): Crash in
    RNA\_enum\_from\_value while inspecting compositor image node
    properties in outliner
    ([rBf815aa01](https://projects.blender.org/blender/blender/commit/f815aa01eb)).
  - \[RC2\] Fix [\#52280](http://developer.blender.org/T52280): The
    Image node in Compositing can't read Z buffer of openEXR in 2.79
    ([rBe54df78c](https://projects.blender.org/blender/blender/commit/e54df78c82)).
  - \[Release\] Fix [\#52218](http://developer.blender.org/T52218):
    Missing update when reconnecting node
    ([rBf2aa9bec](https://projects.blender.org/blender/blender/commit/f2aa9bec9d)).
  - \[Release\] Fix [\#52531](http://developer.blender.org/T52531):
    Blender 2D stabilisation node issue when autoscale is selected
    ([rB82466852](https://projects.blender.org/blender/blender/commit/82466852fe)).

<!-- end list -->

  - Fix unreported: Fix Node space ID remap callback not handling node
    trees
    ([rB884693b4](https://projects.blender.org/blender/blender/commit/884693b42a)).
  - Fix unreported: Fix (unreported) nodeRemoveAllSockets() not clearing
    inputs/outputs sockets lists
    ([rBbd6a9fd7](https://projects.blender.org/blender/blender/commit/bd6a9fd734)).
  - Fix unreported: Fix NodeGroup generic verify function crashing if
    node's ID pointer is NULL
    ([rBfa6a62fa](https://projects.blender.org/blender/blender/commit/fa6a62fac2)).
  - Fix unreported: Fix/workaround
    [\#51007](http://developer.blender.org/T51007): Material viewport
    mode crash on node with more than 64 outputs
    ([rB6aa972eb](https://projects.blender.org/blender/blender/commit/6aa972ebd4)).
  - Fix unreported: Fix: [\#50271](http://developer.blender.org/T50271):
    Bilateral/Directional blur's iterations is zero by default
    ([rBa85f4571](https://projects.blender.org/blender/blender/commit/a85f457195)).
  - Fix unreported: Fix error in node flag check
    ([rB6abcd6cf](https://projects.blender.org/blender/blender/commit/6abcd6cfd5)).
  - Fix unreported: Fix/workaround
    [\#51070](http://developer.blender.org/T51070): Cannot scale
    procedural texture in compositor
    ([rBec051f51](https://projects.blender.org/blender/blender/commit/ec051f5103)).
  - Fix unreported: Fix unreported: Copy-pasting nodes crashes when they
    have an undefined type
    ([rBcd8c4662](https://projects.blender.org/blender/blender/commit/cd8c46627f)).
  - Fix unreported: Compositor: Fix compilation error and crash when
    using defocus search
    ([rBe3bddcb2](https://projects.blender.org/blender/blender/commit/e3bddcb215)).
  - Fix unreported: Fix memory leak caused by node clipboard
    ([rBd5d7d453](https://projects.blender.org/blender/blender/commit/d5d7d453a5)).
  - Fix unreported: Fix compositor Glare node with Simpler Star
    resulting in uneven rays
    ([rBb7fb00f5](https://projects.blender.org/blender/blender/commit/b7fb00f512)).

### Render

  - Fix [\#49393](http://developer.blender.org/T49393): Baking ignores
    backfaces
    ([rB8ced4417](https://projects.blender.org/blender/blender/commit/8ced4417f9)).
  - Fix [\#49937](http://developer.blender.org/T49937): Blender is
    crashing because of Lamp Data Node
    ([rB508e2f0d](https://projects.blender.org/blender/blender/commit/508e2f0d69)).
  - Fix [\#50094](http://developer.blender.org/T50094): Crash when
    viewport rendering point density texture
    ([rB1ec5edcc](https://projects.blender.org/blender/blender/commit/1ec5edcc96)).
  - Fix [\#50032](http://developer.blender.org/T50032): Wrong render
    result when same image is used with and without alpha
    ([rBced20b74](https://projects.blender.org/blender/blender/commit/ced20b74e5)).
  - Fix [\#50542](http://developer.blender.org/T50542): Wrong metadata
    frame when using OpenGL render
    ([rB13d31b16](https://projects.blender.org/blender/blender/commit/13d31b1604)).
  - Fix [\#49429](http://developer.blender.org/T49429): incorrect
    Blender internal viewport border render with DrawPixels method
    ([rB6fc7521a](https://projects.blender.org/blender/blender/commit/6fc7521ade)).
  - Fix [\#50109](http://developer.blender.org/T50109): Blender crash
    when a "Render Result" as a Texture
    ([rB1c21e088](https://projects.blender.org/blender/blender/commit/1c21e088f2)).
  - Fix [\#49864](http://developer.blender.org/T49864): EnvMap baking
    crashes 2.78 if 'Full Sample' checked in AA
    ([rBa70a7f9d](https://projects.blender.org/blender/blender/commit/a70a7f9db3)).
  - Fix [\#52011](http://developer.blender.org/T52011): Border render is
    incorrect with multiple scenes
    ([rB440c91b1](https://projects.blender.org/blender/blender/commit/440c91b1f6)).
  - Fix [\#52116](http://developer.blender.org/T52116): Blender internal
    BVH build crash in degenerate cases
    ([rB5376b3ee](https://projects.blender.org/blender/blender/commit/5376b3eeca)).
  - \[Release\] Fix [\#52473](http://developer.blender.org/T52473):
    blender internal Fresnel and Layer Weight only work with linked
    normal
    ([rBdd843244](https://projects.blender.org/blender/blender/commit/dd84324485)).

<!-- end list -->

  - Fix unreported: OpenGL render: Bugfix (unreported)
    after[rB6f92604e](https://projects.blender.org/blender/blender/commit/6f92604e539b2114763150fb1ace60d28e59a889)
    ([rBcade262c](https://projects.blender.org/blender/blender/commit/cade262c47)).
  - Fix unreported: OpenGL render: Fix non-deterministic order of frame
    writes for movies
    ([rBae2471b8](https://projects.blender.org/blender/blender/commit/ae2471b850)).
  - Fix unreported: OpenGL render: Fix missing file output after
    pressing Esc
    ([rB54dad5c4](https://projects.blender.org/blender/blender/commit/54dad5c49f)).
  - Fix unreported: Fix World Space Shading option influence on Fresnel
    node for BI + cleanup
    ([rB84a283d1](https://projects.blender.org/blender/blender/commit/84a283d18c)).
  - Fix unreported: Fix unreported bug in Blender Render: using
    unnormalized normal in normal map node in the same way as in baking
    ([rB27d20a04](https://projects.blender.org/blender/blender/commit/27d20a04b5)).
  - Fix unreported: Fix threading conflicts in multitex\_ext\_safe()
    ([rB25ab3aac](https://projects.blender.org/blender/blender/commit/25ab3aac9d)).
  - Fix unreported: Fix missing render update when building without OCIO
    but having GLSL image draw method
    ([rB717d85fb](https://projects.blender.org/blender/blender/commit/717d85fb1c)).
  - Fix unreported: Fix memory leak in environment
    ([rBf89c6e73](https://projects.blender.org/blender/blender/commit/f89c6e739a)).

### Render: Cycles

  - Fix [\#49630](http://developer.blender.org/T49630): Cycles: Swapped
    shader and bake kernels
    ([rBcd843409](https://projects.blender.org/blender/blender/commit/cd843409d3)).
  - Fix [\#49793](http://developer.blender.org/T49793): Fix enabling
    SSE2 globally for msvc
    ([rB789ea739](https://projects.blender.org/blender/blender/commit/789ea7397f)).
  - Fix [\#49846](http://developer.blender.org/T49846): OpenCL rendering
    compilation failure
    ([rB5aa6a2ec](https://projects.blender.org/blender/blender/commit/5aa6a2ec06)).
  - Fix [\#49901](http://developer.blender.org/T49901): Cycles: OpenCL
    build error after recent light texture coordinate commit
    ([rB9847ad97](https://projects.blender.org/blender/blender/commit/9847ad977a)).
  - Fix [\#49952](http://developer.blender.org/T49952): Cycles: Bad MIS
    sampling of backgrounds with single bright pixels
    ([rBf89fbf58](https://projects.blender.org/blender/blender/commit/f89fbf580e)).
  - Fix [\#49838](http://developer.blender.org/T49838): Noise
    randomization for frame should be done per interframes as well
    ([rBa2d78d7a](https://projects.blender.org/blender/blender/commit/a2d78d7a46)).
  - Fix [\#49985](http://developer.blender.org/T49985): cycles
    standalone using wrong socket names for XML reading
    ([rB188ecee6](https://projects.blender.org/blender/blender/commit/188ecee642)).
  - Fix [\#49985](http://developer.blender.org/T49985): cycles
    standalone XML missing distant lights
    ([rB478e59a0](https://projects.blender.org/blender/blender/commit/478e59a04e)).
  - Fix [\#49904](http://developer.blender.org/T49904): Cycles
    standalone missing default generated texture coordinates
    ([rB111e2f5a](https://projects.blender.org/blender/blender/commit/111e2f5aba)).
  - Fix [\#50001](http://developer.blender.org/T50001): auto tile size
    addon broken after Cycles GPU device changes
    ([rB60409841](https://projects.blender.org/blender/blender/commit/60409841a4)).
  - Fix [\#50100](http://developer.blender.org/T50100): Cycles
    SeparateRGBNode Red socket defined wrong
    ([rBdef365e2](https://projects.blender.org/blender/blender/commit/def365e252)).
  - Fix [\#50104](http://developer.blender.org/T50104): Race condition
    in SVMShaderManager::device\_update\_shader
    ([rB265e5def](https://projects.blender.org/blender/blender/commit/265e5def76)).
  - Fix [\#50116](http://developer.blender.org/T50116): Light threshold
    broke branched path tracer
    ([rBf812b059](https://projects.blender.org/blender/blender/commit/f812b05922)).
  - Fix [\#50460](http://developer.blender.org/T50460): . Greying out
    issue with Cycles culling options
    ([rBff1b8500](https://projects.blender.org/blender/blender/commit/ff1b850081)).
  - Fix [\#50491](http://developer.blender.org/T50491): Cycles UI breaks
    when pushing F8
    ([rBce888917](https://projects.blender.org/blender/blender/commit/ce8889175a)).
  - Fix [\#49405](http://developer.blender.org/T49405): Crash when
    baking with adaptive subdivision
    ([rBa7d5cabd](https://projects.blender.org/blender/blender/commit/a7d5cabd4e)).
  - Fix [\#50517](http://developer.blender.org/T50517): Rendering
    expecting time is negative
    ([rB8ea09252](https://projects.blender.org/blender/blender/commit/8ea09252c8)).
  - Fix [\#50535](http://developer.blender.org/T50535): Cycles render
    segfault when Explode modifier before hair particle modifier + UV
    material
    ([rB86747ff1](https://projects.blender.org/blender/blender/commit/86747ff180)).
  - Fix [\#49253](http://developer.blender.org/T49253): Cycles blackbody
    is wrong on AVX2 CPU on Windows
    ([rB53896d42](https://projects.blender.org/blender/blender/commit/53896d4235)).
  - Fix [\#50655](http://developer.blender.org/T50655): Pointiness is
    too slow to calculate
    ([rB37afa965](https://projects.blender.org/blender/blender/commit/37afa965a4)).
  - Fix [\#50687](http://developer.blender.org/T50687): Cycles baking
    time estimate and progress bar doesn't work / progress when baking
    with high samples
    ([rB306acb7d](https://projects.blender.org/blender/blender/commit/306acb7dda)).
  - Fix [\#50719](http://developer.blender.org/T50719): Memory usage
    won't reset to zero while re-rendering on two video cards
    ([rB333dc8d6](https://projects.blender.org/blender/blender/commit/333dc8d60f)).
  - Fix [\#50718](http://developer.blender.org/T50718): Regression:
    Split Normals Render Problem with Cycles
    ([rB696836af](https://projects.blender.org/blender/blender/commit/696836af1d)).
  - Fix [\#50748](http://developer.blender.org/T50748): Render Time
    incorrect when refreshing rendered preview in GPU mode
    ([rB60592f67](https://projects.blender.org/blender/blender/commit/60592f6778)).
  - Fix [\#50698](http://developer.blender.org/T50698): Cycles baking
    artifacts with transparent surfaces
    ([rB8c5826f5](https://projects.blender.org/blender/blender/commit/8c5826f59a)).
  - Fix [\#49936](http://developer.blender.org/T49936): Cycles point
    density get's it's bounding box from basis shape key
    ([rBa581b658](https://projects.blender.org/blender/blender/commit/a581b65822)).
  - Fix [\#49603](http://developer.blender.org/T49603): Blender/Cycles
    2.78 CUDA error on Jetson-TX1\~
    ([rB05dfe9c3](https://projects.blender.org/blender/blender/commit/05dfe9c318)).
  - Fix [\#50888](http://developer.blender.org/T50888): Numeric overflow
    in split kernel state buffer size calculation
    ([rB96868a39](https://projects.blender.org/blender/blender/commit/96868a3941)).
  - Fix [\#50628](http://developer.blender.org/T50628): gray out cycles
    device menu when no device configured only for GPU Compute
    ([rB68ca973f](https://projects.blender.org/blender/blender/commit/68ca973f7f)).
  - Fix [\#50925](http://developer.blender.org/T50925): Add AO
    approximation to split kernel
    ([rBf169ff8b](https://projects.blender.org/blender/blender/commit/f169ff8b88)).
  - Fix [\#50876](http://developer.blender.org/T50876): Cycles Crash -
    Cycles crashes before sampling when certain meshes have autosmooth
    enabled
    ([rB1410ea04](https://projects.blender.org/blender/blender/commit/1410ea0478)).
  - Fix [\#50968](http://developer.blender.org/T50968): Cycles crashes
    when image datablock points to a directory
    ([rBea3d7a7f](https://projects.blender.org/blender/blender/commit/ea3d7a7f58)).
  - Fix [\#50990](http://developer.blender.org/T50990): Random black
    pixels in Cycles when rendering material with Multiscatter GGX
    ([rB18bf900b](https://projects.blender.org/blender/blender/commit/18bf900b31)).
  - Fix [\#50975](http://developer.blender.org/T50975): Cycles: Light
    sampling threshold inadvertently clamps negative lamps
    ([rBa201b99c](https://projects.blender.org/blender/blender/commit/a201b99c5a)).
  - Fix [\#50268](http://developer.blender.org/T50268): Cycles allows to
    select un supported GPUs for OpenCL
    ([rB3c4df139](https://projects.blender.org/blender/blender/commit/3c4df13924)).
  - Fix [\#50238](http://developer.blender.org/T50238): Cycles:
    difference in texture position between OpenGL and Cycles render
    ([rB467d824f](https://projects.blender.org/blender/blender/commit/467d824f80)).
  - Fix [\#51051](http://developer.blender.org/T51051): Incorrect render
    on 32bit Linux
    ([rBced8fff5](https://projects.blender.org/blender/blender/commit/ced8fff5de)).
  - Fix [\#51115](http://developer.blender.org/T51115): Bump node is
    broken when the displacement socket is used
    ([rBab347c83](https://projects.blender.org/blender/blender/commit/ab347c8380)).
  - Fix [\#51412](http://developer.blender.org/T51412): Instant crash
    with texture plugged into the Displacement output
    ([rBa523dfd2](https://projects.blender.org/blender/blender/commit/a523dfd2fd)).
  - Fix [\#51314](http://developer.blender.org/T51314): crash cancelling
    Cycles bake during scene sync and update
    ([rB890d871b](https://projects.blender.org/blender/blender/commit/890d871bc3)).
  - Fix [\#51501](http://developer.blender.org/T51501): Cycles baking
    cancel affects baking script
    ([rBb60f80e9](https://projects.blender.org/blender/blender/commit/b60f80e9b3)).
  - Fix [\#49324](http://developer.blender.org/T49324): True
    displacement crashes when shader returns NaN
    ([rBef8ad66a](https://projects.blender.org/blender/blender/commit/ef8ad66aa2)).
  - Fix [\#50937](http://developer.blender.org/T50937): baking with
    OpenCL and CPU have slightly different brightness
    ([rB40e6f65e](https://projects.blender.org/blender/blender/commit/40e6f65ea1)).
  - Fix [\#51529](http://developer.blender.org/T51529): Black boxes on a
    denoising render when using a .exr image as a environmental texture
    ([rB1d49205b](https://projects.blender.org/blender/blender/commit/1d49205b1a)).
  - Fix [\#51408](http://developer.blender.org/T51408): Cycles -
    Principled BSDF Shader - Transparency is not working as expected
    ([rB32c9d232](https://projects.blender.org/blender/blender/commit/32c9d2322c)).
  - Fix [\#51555](http://developer.blender.org/T51555): Cycles tile
    count is incorrect when denoising is enabled
    ([rBa21277b9](https://projects.blender.org/blender/blender/commit/a21277b996)).
  - Fix [\#51506](http://developer.blender.org/T51506): Wrong shadow
    catcher color when using selective denoising
    ([rBcf1127f3](https://projects.blender.org/blender/blender/commit/cf1127f380)).
  - Fix [\#51553](http://developer.blender.org/T51553): Cycles Volume
    Emission turns black when strength is 0 or color is black
    ([rB4a04d7ae](https://projects.blender.org/blender/blender/commit/4a04d7ae89)).
  - Fix [\#51537](http://developer.blender.org/T51537): Light passes are
    summed twice for split kernel since denoise commit
    ([rB8e655446](https://projects.blender.org/blender/blender/commit/8e655446d1)).
  - Fix [\#51560](http://developer.blender.org/T51560): Black pixels on
    a denoising render
    ([rB3dee1f07](https://projects.blender.org/blender/blender/commit/3dee1f079f)).
  - Fix [\#51568](http://developer.blender.org/T51568): CUDA error in
    viewport render after fix for for OpenCL
    ([rB34b68989](https://projects.blender.org/blender/blender/commit/34b689892b)).
  - Fix [\#51592](http://developer.blender.org/T51592): Simplify AO
    Cycles setting remains active while Simplify is disabled
    ([rB7add6b89](https://projects.blender.org/blender/blender/commit/7add6b89bc)).
  - Fix [\#51589](http://developer.blender.org/T51589): Principled
    Subsurface Scattering, wrong shadow color
    ([rBe20a33b8](https://projects.blender.org/blender/blender/commit/e20a33b89d)).
  - Fix [\#51652](http://developer.blender.org/T51652): Cycles -
    Persistant Images not storing images
    ([rB9b914764](https://projects.blender.org/blender/blender/commit/9b914764a9)).
  - Fix [\#49570](http://developer.blender.org/T49570): Cycles baking
    can't handle materials with no images
    ([rB2cd7b80c](https://projects.blender.org/blender/blender/commit/2cd7b80cae)).
  - Fix [\#51791](http://developer.blender.org/T51791): Point Density
    doesn't work on GPU
    ([rB6cfa3ecd](https://projects.blender.org/blender/blender/commit/6cfa3ecd4d)).
  - Fix [\#51849](http://developer.blender.org/T51849): change Cycles
    clearcoat gloss to roughness
    ([rB14ea0c5f](https://projects.blender.org/blender/blender/commit/14ea0c5fcc)).
  - Fix [\#51836](http://developer.blender.org/T51836): Cycles: Fix
    incorrect PDF approximations of the MultiGGX closures
    ([rB8cb741a5](https://projects.blender.org/blender/blender/commit/8cb741a598)).
  - Fix [\#51909](http://developer.blender.org/T51909): Cycles:
    Uninitialized closure normals for the Hair BSDF
    ([rB1f3fd8e6](https://projects.blender.org/blender/blender/commit/1f3fd8e60a)).
  - Fix [\#51957](http://developer.blender.org/T51957): principled BSDF
    mismatches in GLSL viewport
    ([rBeb420e6b](https://projects.blender.org/blender/blender/commit/eb420e6b7f)).
  - Fix [\#51956](http://developer.blender.org/T51956): color noise with
    principled sss, radius 0 and branched path
    ([rB29c8c504](https://projects.blender.org/blender/blender/commit/29c8c50442)).
  - Fix [\#51855](http://developer.blender.org/T51855): Cycles emssive
    objects with NaN transform break lighting
    ([rBcda24d08](https://projects.blender.org/blender/blender/commit/cda24d0853)).
  - Fix [\#51950](http://developer.blender.org/T51950): Abnormally long
    Cycles OpenCL GPU render times with certain panoramic camera
    settings
    ([rB15fd758b](https://projects.blender.org/blender/blender/commit/15fd758bd6)).
  - Fix [\#51967](http://developer.blender.org/T51967): OSL crash after
    rendering finished (mainly on Windows)
    ([rB3361f210](https://projects.blender.org/blender/blender/commit/3361f2107b)).
  - Fix [\#52001](http://developer.blender.org/T52001): material draw
    mode principled BSDF artifacts at some angles
    ([rBba256b32](https://projects.blender.org/blender/blender/commit/ba256b32ee)).
  - Fix [\#52027](http://developer.blender.org/T52027): OSL
    getattribute() crash, when optimizer calls it before rendering
    ([rB29ec0b11](https://projects.blender.org/blender/blender/commit/29ec0b1162)).
  - Fix [\#52021](http://developer.blender.org/T52021): Shadow catcher
    renders wrong when catcher object is behind transparent object
    ([rB5f35682f](https://projects.blender.org/blender/blender/commit/5f35682f3a)).
  - Fix [\#52107](http://developer.blender.org/T52107): Color management
    difference when using multiple and different GPUs together
    ([rB4bc6faf9](https://projects.blender.org/blender/blender/commit/4bc6faf9c8)).
  - Fix [\#52125](http://developer.blender.org/T52125): principled BSDF
    missing with macOS OpenCL
    ([rB3b12a719](https://projects.blender.org/blender/blender/commit/3b12a71972)).
  - Fix [\#52135](http://developer.blender.org/T52135): Cycles should
    not keep generated/packed images in memory after render
    ([rB2b132fc3](https://projects.blender.org/blender/blender/commit/2b132fc3f7)).
  - Fix [\#52152](http://developer.blender.org/T52152): allow zero
    roughness for Cycles principled BSDF, don't clamp
    ([rBe982ebd6](https://projects.blender.org/blender/blender/commit/e982ebd6d4)).
  - Fix [\#51450](http://developer.blender.org/T51450): viewport render
    time keeps increasing after render is done
    ([rBe9380431](https://projects.blender.org/blender/blender/commit/e93804318f)).
  - \[Release\] Fix [\#51805](http://developer.blender.org/T51805):
    Overlapping volumes renders incorrect on AMD GPU
    ([rB6825439b](https://projects.blender.org/blender/blender/commit/6825439b36)).
  - \[Release\] Fix [\#52533](http://developer.blender.org/T52533):
    Blender shuts down when rendering duplicated smoke domain
    ([rBc9d653e5](https://projects.blender.org/blender/blender/commit/c9d653e560)).

<!-- end list -->

  - Fix unreported: Cycles: Fix another OpenCL logging issue
    ([rBf7ce4823](https://projects.blender.org/blender/blender/commit/f7ce482385)).
  - Fix unreported: Cycles: Fix OpenCL compilation with the new brick
    texture
    ([rB2e9dd120](https://projects.blender.org/blender/blender/commit/2e9dd1200f)).
  - Fix unreported: Cycles: Fix OpenCL build error caused by light
    termination commit
    ([rBf800794b](https://projects.blender.org/blender/blender/commit/f800794b97)).
  - Fix unreported: Fix Brick Texture GLSL, broken after Mortar Smooth
    addition
    ([rB28639a22](https://projects.blender.org/blender/blender/commit/28639a22bc)).
  - Fix unreported: Fix Cycles OSL compilation based on modified time
    not working
    ([rBb5a58507](https://projects.blender.org/blender/blender/commit/b5a58507f2)).
  - Fix unreported: Cycles: Fix different noise pattern from fix in
    [\#49838](http://developer.blender.org/T49838)
    ([rB2a2eb0c4](https://projects.blender.org/blender/blender/commit/2a2eb0c463)).
  - Fix unreported: Fix Cycles device backwards compatibility error if
    device type is unavailable
    ([rB411836d9](https://projects.blender.org/blender/blender/commit/411836d97c)).
  - Fix unreported: Cycles: Fix correlation issues in certain cases
    ([rB9d50175b](https://projects.blender.org/blender/blender/commit/9d50175b6c)).
  - Fix unreported: Fix emissive volumes generates unexpected fireflies
    around intersections
    ([rBdd58390d](https://projects.blender.org/blender/blender/commit/dd58390d71)).
  - Fix unreported: Cycles: Fix wrong motion blur when combining
    deformation motion blur with autosplit
    ([rB394fa07d](https://projects.blender.org/blender/blender/commit/394fa07d41)).
  - Fix unreported: Cycles: Fix wrong scaling of traversed instances
    debug pass
    ([rB789fdab8](https://projects.blender.org/blender/blender/commit/789fdab825)).
  - Fix unreported: Cycles: Fix wrong transparent shadows for motion
    blur hair
    ([rBe5a665fe](https://projects.blender.org/blender/blender/commit/e5a665fe24)).
  - Fix unreported: Cycles: Fix amount of rendered samples not being
    shown while rendering the last tile on CPU
    ([rB4a191122](https://projects.blender.org/blender/blender/commit/4a19112277)).
  - Fix unreported: Cycles: Fix typo in the panel name
    ([rB77982e15](https://projects.blender.org/blender/blender/commit/77982e159c)).
  - Fix unreported: Cycles: Fix rng\_state initialization when using
    resumable rendering
    ([rBfa19940d](https://projects.blender.org/blender/blender/commit/fa19940dc6)).
  - Fix unreported: Cycles: Fix regression with transparent shadows in
    volume
    ([rBb16fd220](https://projects.blender.org/blender/blender/commit/b16fd22018)).
  - Fix unreported: Cycles: Fix pointiness attribute giving wrong
    results with autosplit
    ([rBfd7e9f79](https://projects.blender.org/blender/blender/commit/fd7e9f7974)).
  - Fix unreported: Cycles: Fix wrong shading on GPU when background has
    NaN pixels and MIS enabled
    ([rB581c8190](https://projects.blender.org/blender/blender/commit/581c819013)).
  - Fix unreported: Cycles: Fix wrong transparent shadows with CUDA
    ([rB21dbfb78](https://projects.blender.org/blender/blender/commit/21dbfb7828)).
  - Fix unreported: Cycles: Fix wrong pointiness caused by precision
    issues
    ([rB5723aa8c](https://projects.blender.org/blender/blender/commit/5723aa8c02)).
  - Fix unreported: Cycles: Fix missing initialization of triangle BVH
    steps
    ([rB088c6a17](https://projects.blender.org/blender/blender/commit/088c6a17ba)).
  - Fix unreported: Cycles: Fix wrong hair render results when using BVH
    motion steps
    ([rBdc7bbd73](https://projects.blender.org/blender/blender/commit/dc7bbd731a)).
  - Fix unreported: Cycles: Fix shading with autosmooth and custom
    normals
    ([rB36c4fc1e](https://projects.blender.org/blender/blender/commit/36c4fc1ea9)).
  - Fix unreported: Fix Cycles still saving render output when error
    happened
    ([rB75cc33fa](https://projects.blender.org/blender/blender/commit/75cc33fa20)).
  - Fix unreported: Cycles: Fix wrong render results with texture limit
    and half-float textures
    ([rB4e12113b](https://projects.blender.org/blender/blender/commit/4e12113bea)).
  - Fix unreported: Cycles: Fix non-zero exit status when rendering
    animation from CLI and running out of memory
    ([rBf49e28ba](https://projects.blender.org/blender/blender/commit/f49e28bae7)).
  - Fix unreported: Fix/workaround
    [\#48549](http://developer.blender.org/T48549): Crash baking
    high-to-low-poly normal map in cycles
    ([rBefe78d82](https://projects.blender.org/blender/blender/commit/efe78d824e)).
  - Fix unreported: Cycles: Fix division by zero in volume code which
    was producing -nan
    ([rB87f236cd](https://projects.blender.org/blender/blender/commit/87f236cd10)).
  - Fix unreported: Cycles: Fix possibly uninitialized variable
    ([rB810d7d46](https://projects.blender.org/blender/blender/commit/810d7d4694)).
  - Fix unreported: Cycles: Fix crash after failed kernel build
    ([rB997e345b](https://projects.blender.org/blender/blender/commit/997e345bd2)).
  - Fix unreported: Cycles: Fix CUDA build error for some compilers
    ([rBc837bd5e](https://projects.blender.org/blender/blender/commit/c837bd5ea5)).
  - Fix unreported: Cycles: Fix handling of barriers
    ([rB60a344b4](https://projects.blender.org/blender/blender/commit/60a344b43d)).
  - Fix unreported: Cycles: Fix wrong vector allocation in the mesh sync
    code
    ([rBeb1a57b1](https://projects.blender.org/blender/blender/commit/eb1a57b12c)).
  - Fix unreported: Fix/workaround
    [\#50533](http://developer.blender.org/T50533): Transparency shader
    doesn't cast shadows with curve segments
    ([rB2b44db4c](https://projects.blender.org/blender/blender/commit/2b44db4cfc)).
  - Fix unreported: Cycles: Fix speed regression on GPU
    ([rBa1348dde](https://projects.blender.org/blender/blender/commit/a1348dde2e)).
  - Fix unreported: Cycles: Fix uninitialized memory access when
    comparing curve mapping nodes
    ([rB5ce95df2](https://projects.blender.org/blender/blender/commit/5ce95df2c6)).
  - Fix unreported: Cycles: Fix race condition in attributes creation
    during SVM compilation
    ([rB52029e68](https://projects.blender.org/blender/blender/commit/52029e689c)).
  - Fix unreported: Cycles: Fix corrupted mesh render when topology
    differs at the next frame
    ([rB9706bfd2](https://projects.blender.org/blender/blender/commit/9706bfd25d)).
  - Fix unreported: Cycles: Fix access of NULL pointer as array
    ([rBfd085706](https://projects.blender.org/blender/blender/commit/fd08570665)).
  - Fix unreported: Cycles: Fix race condition in shader attribute for
    real now
    ([rBc8e764cc](https://projects.blender.org/blender/blender/commit/c8e764ccbf)).
  - Fix unreported: Cycles: Fix the AO replacement option in the split
    kernel
    ([rBef816f9c](https://projects.blender.org/blender/blender/commit/ef816f9cff)).
  - Fix unreported: Cycles: Fix over-allocation of triangles storage for
    triangle primitive hair
    ([rBbe60e9b8](https://projects.blender.org/blender/blender/commit/be60e9b8c5)).
  - Fix unreported: Cycles: Fix missing type declaration in OpenCL image
    ([rB48461840](https://projects.blender.org/blender/blender/commit/4846184095)).
  - Fix unreported: Cycles: Fix crash when assigning KernelGlobals
    ([rBed688e48](https://projects.blender.org/blender/blender/commit/ed688e4843)).
  - Fix unreported: Cycles: Fix access array index of -1 in SSS and
    volume split kernels
    ([rB2eb906e1](https://projects.blender.org/blender/blender/commit/2eb906e1b4)).
  - Fix unreported: \[Cycles\] Fix math problems in safe\_logf
    ([rBc9451f1c](https://projects.blender.org/blender/blender/commit/c9451f1cff)).
  - Fix unreported: Cycles: Fix transform addressing in the denoiser
    code
    ([rBe518ea9b](https://projects.blender.org/blender/blender/commit/e518ea9b5e)).
  - Fix unreported: Cycles: Fix occasional black pixels from denoising
    with excessive radii
    ([rB58a0c275](https://projects.blender.org/blender/blender/commit/58a0c27546)).
  - Fix unreported: Cycles: fix AO approximation for split kernel
    ([rB90b94678](https://projects.blender.org/blender/blender/commit/90b9467861)).
  - Fix unreported: Cycles Denoising: Fix wrong order of denoising
    feature passes
    ([rBb3a3459e](https://projects.blender.org/blender/blender/commit/b3a3459e1a)).
  - Fix unreported: Cycles: Fix random noise pattern seen with
    multiscatter bsdf and split kernel
    ([rB29f4a851](https://projects.blender.org/blender/blender/commit/29f4a8510c)).
  - Fix unreported: Cycles: Fix denoising passes being written when
    they're not actually generated
    ([rBc73206ac](https://projects.blender.org/blender/blender/commit/c73206acc5)).
  - Fix unreported: Cycles: Fix race condition happening in progress
    utility
    ([rB794311c9](https://projects.blender.org/blender/blender/commit/794311c92b)).
  - Fix unreported: Cycles: Fix excessive sampling weight of glossy
    Principled BSDF components
    ([rB19791760](https://projects.blender.org/blender/blender/commit/1979176088)).
  - Fix unreported: Fix principled BSDF incorrectly missing subsurface
    component with base color black
    ([rB52b9516e](https://projects.blender.org/blender/blender/commit/52b9516e03)).
  - Fix unreported: Cycles: Fix comparison in principled BSDF
    ([rB1f933c94](https://projects.blender.org/blender/blender/commit/1f933c94a7)).
  - Fix unreported: Fix potential memory leak in Cycles loading of
    packed/generated images
    ([rBa4cd7b72](https://projects.blender.org/blender/blender/commit/a4cd7b7297)).
  - Fix unreported: Fix Cycles denoising NaNs with a 1 sample renders
    ([rBec831ee7](https://projects.blender.org/blender/blender/commit/ec831ee7d1)).
  - Fix unreported: Fix Cycles multi scatter GGX different render
    results with Clang and GCC
    ([rB9e929c91](https://projects.blender.org/blender/blender/commit/9e929c911e)).
  - \[RC2\] Fix unreported: Cycles: Fixed broken camera motion blur when
    motion was not set to center on frame
    ([rB10764d31](https://projects.blender.org/blender/blender/commit/10764d31d4)).
  - \[Release\] Fix unreported: Fix threading conflict when doing Cycles
    background render
    ([rB9997f5ca](https://projects.blender.org/blender/blender/commit/9997f5ca91)).
  - \[Release\] Fix unreported: Cycles: Fix stack overflow during
    traversal caused by floating overflow
    ([rB75e392ae](https://projects.blender.org/blender/blender/commit/75e392ae9f)).
  - \[Release\] Fix unreported: Cycles: FIx issue with -0 being
    considered a non-finite value
    ([rB95d871c1](https://projects.blender.org/blender/blender/commit/95d871c1a2)).
  - \[Release\] Fix unreported: Cycles Bake: Fix overflow when using
    hundreds of images
    ([rB9ca3d4cb](https://projects.blender.org/blender/blender/commit/9ca3d4cbbd)).
  - \[Release\] Fix unreported: Cycles: Safer fix for infinite recursion
    ([rBfbb4be06](https://projects.blender.org/blender/blender/commit/fbb4be061c)).

### Render: Freestyle

  - Fix [\#49479](http://developer.blender.org/T49479): Freestyle
    inconsistent line drawing with large geometry dimension
    ([rB7f262acb](https://projects.blender.org/blender/blender/commit/7f262acb92)).

<!-- end list -->

  - Fix unreported: Fix unfreed memory after cleaning render layers
    ([rB18cf3e1a](https://projects.blender.org/blender/blender/commit/18cf3e1a38)).
  - Fix unreported: Freestyle: Fix (unreported) wrong distance
    calculation in the Fill Range by Selection operator
    ([rBcdff6590](https://projects.blender.org/blender/blender/commit/cdff659036)).
  - Fix unreported: Freestyle: Fix (unreported) wrong distance
    calculation in the Fill Range by Selection operator
    ([rBcdff6590](https://projects.blender.org/blender/blender/commit/cdff659036)).
  - Fix unreported: Fix freestyle lineset panels being animatable
    ([rBb0ee9aaa](https://projects.blender.org/blender/blender/commit/b0ee9aaa5d)).

### Sequencer

  - Fix [\#49996](http://developer.blender.org/T49996): VSE opengl
    render crash with grease pencil if current frame is empty
    ([rB674c3bf8](https://projects.blender.org/blender/blender/commit/674c3bf894)).
  - Fix [\#49893](http://developer.blender.org/T49893): Crash in Video
    Sequence Editor with 'drop' effect
    ([rB8f29503b](https://projects.blender.org/blender/blender/commit/8f29503b52)).
  - Fix [\#51556](http://developer.blender.org/T51556): Sequencer -
    White Balance Modifier - Masking is not honored
    ([rBa5c73129](https://projects.blender.org/blender/blender/commit/a5c73129c5)).
  - Fix [\#50112](http://developer.blender.org/T50112): Sequencer crash
    w/ missing proxy data
    ([rB2580c3cb](https://projects.blender.org/blender/blender/commit/2580c3cb82)).
  - Fix [\#51661](http://developer.blender.org/T51661): Swaping strips
    does not refresh sequencer
    ([rBa51dccc6](https://projects.blender.org/blender/blender/commit/a51dccc6f3)).
  - Fix [\#51947](http://developer.blender.org/T51947): failure setting
    sequence.use\_proxy/crop/translation to False
    ([rB79f27fc8](https://projects.blender.org/blender/blender/commit/79f27fc8d3)).
  - Fix [\#51898](http://developer.blender.org/T51898): missing sequence
    strip color space validation on load
    ([rB00f3ab2f](https://projects.blender.org/blender/blender/commit/00f3ab2fb2)).
  - \[Release\] Fix [\#52522](http://developer.blender.org/T52522): VSE
    renders with alpha transparent PNG image incorrectly
    ([rBa8bd08ff](https://projects.blender.org/blender/blender/commit/a8bd08ffdd)).

<!-- end list -->

  - Fix unreported: Fix (unreported) Sequencer Drop effect: wrong
    initial offset in second input buffer
    ([rBfc4a51e3](https://projects.blender.org/blender/blender/commit/fc4a51e3fa)).
  - Fix unreported: Sequencer: Fix broken interface script since 415ff74
    ([rB5e82981f](https://projects.blender.org/blender/blender/commit/5e82981f47)).
  - Fix unreported: Fix byte-to-float conversion when using scene strips
    in sequencer with identical color spaces
    ([rB06ac6ded](https://projects.blender.org/blender/blender/commit/06ac6ded66)).
  - Fix unreported: Fix crash opening really old files with compositor
    ([rB0e46da76](https://projects.blender.org/blender/blender/commit/0e46da76b7)).
  - Fix unreported: Better fix for sequencer crash when text strip
    doesn't have effect data
    ([rBaea44561](https://projects.blender.org/blender/blender/commit/aea4456101)).

## UI / Spaces / Transform

### 3D View

  - Fix [\#49408](http://developer.blender.org/T49408): OpenGL light
    widget breaks viewport shading
    ([rB78c0bc52](https://projects.blender.org/blender/blender/commit/78c0bc52de)).
  - Fix [\#49804](http://developer.blender.org/T49804): Display grid
    Scale/Subdivision are sometimes disabled in View3D when they should
    not
    ([rBb5187443](https://projects.blender.org/blender/blender/commit/b51874437d)).
  - Fix [\#49872](http://developer.blender.org/T49872): 3D cursor places
    with camera shift in ortographic mode
    ([rB0a26904a](https://projects.blender.org/blender/blender/commit/0a26904a75)).
  - Fix [\#49861](http://developer.blender.org/T49861): Interlace stereo
    is broken in 2.78
    ([rB070f22c4](https://projects.blender.org/blender/blender/commit/070f22c440)).
  - Fix [\#50564](http://developer.blender.org/T50564): 3D view panning
    with scroll wheel inconsistent with dragging
    ([rB3f5b2e26](https://projects.blender.org/blender/blender/commit/3f5b2e2682)).
  - Fix [\#51216](http://developer.blender.org/T51216): SSAO attenuation
    not being scale invariant
    ([rBe280c70a](https://projects.blender.org/blender/blender/commit/e280c70aa9)).
  - Fix [\#51324](http://developer.blender.org/T51324): Auto-Depth fails
    rotating out of camera
    ([rBaf3f7db4](https://projects.blender.org/blender/blender/commit/af3f7db4ec)).
  - Fix [\#51434](http://developer.blender.org/T51434): Module math
    operation is wrong in GLSL shading
    ([rBffc95a33](https://projects.blender.org/blender/blender/commit/ffc95a33b6)).
  - Fix [\#51354](http://developer.blender.org/T51354): Final take on
    multi-view (single view) issues
    ([rB62aa925c](https://projects.blender.org/blender/blender/commit/62aa925c11)).
  - Fix [\#51538](http://developer.blender.org/T51538): Weight-paint
    circle select w/ clipping
    ([rB102394a3](https://projects.blender.org/blender/blender/commit/102394a323)).
  - Fix [\#51629](http://developer.blender.org/T51629): Select w/ object
    lock fails
    ([rB4badf677](https://projects.blender.org/blender/blender/commit/4badf67739)).
  - Fix [\#51862](http://developer.blender.org/T51862): principled
    shader GLSL artifacts in ortho mode
    ([rB968093ec](https://projects.blender.org/blender/blender/commit/968093ec2f)).
  - Fix [\#51834](http://developer.blender.org/T51834): Active Object
    and Groups color difference imperceptible
    ([rB4d124418](https://projects.blender.org/blender/blender/commit/4d124418b7)).
  - \[Release\] Fix [\#52663](http://developer.blender.org/T52663):
    Remap used invalid local-view data
    ([rBb895c733](https://projects.blender.org/blender/blender/commit/b895c7337e)).

<!-- end list -->

  - Fix unreported: fix for potential pitfall with glMatrixMode
    ([rBa3d258bf](https://projects.blender.org/blender/blender/commit/a3d258bfb4)).
  - Fix unreported: Viewport SSAO: Fix normals not normalized
    ([rB0507b3e4](https://projects.blender.org/blender/blender/commit/0507b3e4c4)).
  - Fix unreported: Fix possible crash in various 3D View operators
    ([rB7359cc10](https://projects.blender.org/blender/blender/commit/7359cc1060)).
  - Fix unreported: Rigid body: fix viewport not updating on properties
    change
    ([rB15fa8061](https://projects.blender.org/blender/blender/commit/15fa806160)).
  - Fix unreported: Fix stereoscopic camera volume drawing
    ([rB2ad11243](https://projects.blender.org/blender/blender/commit/2ad1124372)).
  - Fix unreported: Partial fix to Multi-View single eye issues in
    viewport
    ([rBd2f1f80a](https://projects.blender.org/blender/blender/commit/d2f1f80a6f)).
  - Fix unreported: Fixup for multi-view single eye viewport issues
    ([rB195d0fba](https://projects.blender.org/blender/blender/commit/195d0fbae3)).
  - Fix unreported: Fix GPencil depth checks
    ([rBb5a976ec](https://projects.blender.org/blender/blender/commit/b5a976ec19)).
  - Fix unreported: Fix bad index use drawing deformed face centers
    ([rBe2b1c70b](https://projects.blender.org/blender/blender/commit/e2b1c70b48)).

### Input (NDOF / 3D Mouse)

  - Fix [\#48911](http://developer.blender.org/T48911): Fix
    [\#48847](http://developer.blender.org/T48847): Issues with some
    shortcuts on non-US latin keyboards, and with non-first layouts
    ([rB16cb9391](https://projects.blender.org/blender/blender/commit/16cb939163)).
  - Fix [\#49303](http://developer.blender.org/T49303): Fix
    [\#49314](http://developer.blender.org/T49314): More issues with new
    handling of X11 shortcuts
    ([rB037df2aa](https://projects.blender.org/blender/blender/commit/037df2aaa6)).
  - Fix [\#51948](http://developer.blender.org/T51948): pen pressure not
    detected with some Wacom tablets
    ([rBc3c0495b](https://projects.blender.org/blender/blender/commit/c3c0495b30)).

<!-- end list -->

  - Fix unreported: GHOST X11 keyboard: Attempt to fix issues with
    modifier keys on some systems
    ([rB87b3faf5](https://projects.blender.org/blender/blender/commit/87b3faf557)).
  - Fix unreported: Fix Windows mouse wheel scroll speed
    ([rB26d7d995](https://projects.blender.org/blender/blender/commit/26d7d995db)).

### Outliner

  - Fix [\#51680](http://developer.blender.org/T51680): 'Delete Group'
    from Group view of Outliner does not work
    ([rBf783efd1](https://projects.blender.org/blender/blender/commit/f783efd127)).
  - Fix [\#51926](http://developer.blender.org/T51926): Selecting pose
    icon under expanded group in outliner causes crash
    ([rBa57a7975](https://projects.blender.org/blender/blender/commit/a57a7975a1)).
  - \[Release\] Fix [\#52538](http://developer.blender.org/T52538):
    Outliner crash when displaying groups and using Show Active on
    editmode bone not in any groups
    ([rB42760d92](https://projects.blender.org/blender/blender/commit/42760d922e)).

<!-- end list -->

  - Fix unreported: Fix outliner contextual menu allowing to delete
    indirect libraries
    ([rBfa9bd044](https://projects.blender.org/blender/blender/commit/fa9bd04483)).
  - Fix unreported: Fix missing undo pushes in outliner's new datablock
    management operations
    ([rB1f65ab60](https://projects.blender.org/blender/blender/commit/1f65ab606b)).

### Text Editor

  - Fix [\#50900](http://developer.blender.org/T50900): Text-Blocks
    created from "Edit Source" have zero users
    ([rB15eb83c8](https://projects.blender.org/blender/blender/commit/15eb83c8b3)).

### Transform

  - Fix [\#50486](http://developer.blender.org/T50486): Don't always do
    the \`ray\_start\_correction\` in the ortho view
    ([rB88b0b229](https://projects.blender.org/blender/blender/commit/88b0b22914)).
  - Fix [\#49632](http://developer.blender.org/T49632): Grease pencil in
    "Edit Strokes" mode: Snap tool did not snap points to active object
    A simple confusion between enums: \~SNAP\_NOT\_ACTIVE\~
    ([rB997a210b](https://projects.blender.org/blender/blender/commit/997a210b08)).
  - Fix [\#49494](http://developer.blender.org/T49494):
    snap\_align\_rotation should use a local pivot to make the
    transformation
    ([rBddf99214](https://projects.blender.org/blender/blender/commit/ddf99214dc)).
  - Fix [\#50125](http://developer.blender.org/T50125): Shortcut keys
    missing in menus for Clear Location, Rotation, and Scale
    ([rB52696a0d](https://projects.blender.org/blender/blender/commit/52696a0d3f)).
  - Fix [\#46892](http://developer.blender.org/T46892): snap to closest
    point now works with Individual Origins
    ([rB21f37678](https://projects.blender.org/blender/blender/commit/21f3767809)).
  - Fix [\#50592](http://developer.blender.org/T50592): Scene.raycast
    not working
    ([rB47caf343](https://projects.blender.org/blender/blender/commit/47caf343c0)).
  - Fix [\#50602](http://developer.blender.org/T50602): Avoid crash when
    executing \`transform\_snap\_context\_project\_view3d\_mixed\` with
    \`dist\_px\` NULL
    ([rB22156d95](https://projects.blender.org/blender/blender/commit/22156d951d)).
  - Fix [\#50565](http://developer.blender.org/T50565): Planar
    constraints don't work properly with non-Blender key configurations
    ([rB278fce11](https://projects.blender.org/blender/blender/commit/278fce1170)).
  - Fix [\#47690](http://developer.blender.org/T47690): Connected PET w/
    individual origins
    ([rB12e68190](https://projects.blender.org/blender/blender/commit/12e681909f)).
  - Fix [\#50899](http://developer.blender.org/T50899): Even though the
    Shrinkwrap options hide the possibility of using a non-mesh target,
    you can still circumvent this... Causing Crash
    ([rBa81ea408](https://projects.blender.org/blender/blender/commit/a81ea40836)).
  - Fix [\#51169](http://developer.blender.org/T51169): Push/pull fails
    w/ local lock axis
    ([rB97374016](https://projects.blender.org/blender/blender/commit/9737401688)).
  - Fix [\#51651](http://developer.blender.org/T51651): translate w/
    individual origins fails
    ([rB3be07380](https://projects.blender.org/blender/blender/commit/3be073807b)).
  - Fix [\#51691](http://developer.blender.org/T51691): Shear cursor
    input scales w/ zoom
    ([rB863f0434](https://projects.blender.org/blender/blender/commit/863f0434ec)).
  - Fix [\#51756](http://developer.blender.org/T51756): Fix crash when
    transforming vertices in edit mode
    ([rBd583af00](https://projects.blender.org/blender/blender/commit/d583af0026)).
  - \[Release\] Fix [\#52490](http://developer.blender.org/T52490): NDOF
    orbit doesn't lock in ortho view
    ([rB0ed5605b](https://projects.blender.org/blender/blender/commit/0ed5605bd5)).

<!-- end list -->

  - Fix unreported: Fix unreported bug: parameter ray\_start repeated
    ([rB318ee2e8](https://projects.blender.org/blender/blender/commit/318ee2e8c1)).
  - Fix unreported: Fix bug not reported: Ruler/Protractor: Snap to
    vertices and edges was not considering the depth variation
    ([rBd07e2416](https://projects.blender.org/blender/blender/commit/d07e2416db)).
  - Fix unreported: Fix (unreported) crash in new snap code
    ([rBa2c469ed](https://projects.blender.org/blender/blender/commit/a2c469edc2)).
  - Fix unreported: Fix unreported bug: Ensure you have the correct
    array directory even after the \`dm-\>release(dm)\`
    ([rB631ecbc4](https://projects.blender.org/blender/blender/commit/631ecbc4ca)).
  - Fix unreported: Fix second part
    [\#50565](http://developer.blender.org/T50565): Using planar
    transform once makes it enabled by default
    ([rB499faa8b](https://projects.blender.org/blender/blender/commit/499faa8b11)).
  - Fix unreported: Fix another part of
    [\#50565](http://developer.blender.org/T50565): Planar constraints
    were always initialized to accurate transform
    ([rB87f8bb8d](https://projects.blender.org/blender/blender/commit/87f8bb8d1d)).
  - Fix unreported: Fix buffer read error w/ 2 pass select queries
    ([rB24623202](https://projects.blender.org/blender/blender/commit/2462320210)).
  - Fix unreported: Fix unpredictable trackball rotation
    ([rB9210a4fa](https://projects.blender.org/blender/blender/commit/9210a4faf3)).
  - Fix unreported: Fix [http://developer.blender.org/T51595
    \#51595](http://developer.blender.org/T51595_#51595):
    Snap to edge does not work with high zoom level
    ([rB3f39719b](https://projects.blender.org/blender/blender/commit/3f39719b5d)).
  - Fix unreported: Snap System: fix rename \`ob\` to \`obj\`
    ([rBc6ddef73](https://projects.blender.org/blender/blender/commit/c6ddef7359)).
  - Fix unreported: Snap System: Fixed index of objects used to make
    \`snap to volume\`
    ([rB80095645](https://projects.blender.org/blender/blender/commit/8009564503)).
  - \[Release\] Fix unreported: Correction to last fix
    ([rB7997646c](https://projects.blender.org/blender/blender/commit/7997646c2d)).
  - \[Release\] Fix unreported: Fix transform snap code using
    'allocated' flags to get verts/edges/etc. arrays again from DM
    ([rB9cc7e32f](https://projects.blender.org/blender/blender/commit/9cc7e32f39)).

### User Interface

  - Fix [\#49383](http://developer.blender.org/T49383): Color pickers
    are available if the color is locked
    ([rB09925d52](https://projects.blender.org/blender/blender/commit/09925d52f5)).
  - Fix [\#49997](http://developer.blender.org/T49997): don't flip
    texture users menu in texture properties
    ([rBcc8132b0](https://projects.blender.org/blender/blender/commit/cc8132b0c8)).
  - Fix [\#50022](http://developer.blender.org/T50022): "Mirror" in
    Dopesheet Crashes Blender
    ([rB0cd1b5ef](https://projects.blender.org/blender/blender/commit/0cd1b5ef85)).
  - Fix [\#50063](http://developer.blender.org/T50063): Editing driver's
    expression eliminates "Zero" number
    ([rB3fb11061](https://projects.blender.org/blender/blender/commit/3fb11061ba)).
  - Fix [\#50386](http://developer.blender.org/T50386): Crash when
    spawning pie menus
    ([rB8a6c689f](https://projects.blender.org/blender/blender/commit/8a6c689f30)).
  - Fix [\#50570](http://developer.blender.org/T50570): pressing pgup or
    pgdn in any scrollable area irreversably alters scrolling speed
    ([rBfb61711b](https://projects.blender.org/blender/blender/commit/fb61711b1a)).
  - Fix [\#50497](http://developer.blender.org/T50497): prop\_search not
    correctly drew in UI (D2473)
    ([rBfeb58806](https://projects.blender.org/blender/blender/commit/feb588060a)).
  - Fix [\#50629](http://developer.blender.org/T50629): Add remove
    doubles to the cleanup menu
    ([rBbb1367cd](https://projects.blender.org/blender/blender/commit/bb1367cdaf)).
  - Fix [\#51068](http://developer.blender.org/T51068): Place props in
    their own row
    ([rB93426cb2](https://projects.blender.org/blender/blender/commit/93426cb295)).
  - Fix [\#51248](http://developer.blender.org/T51248): user preferences
    window size not adapted to DPI
    ([rB6c26911c](https://projects.blender.org/blender/blender/commit/6c26911c3d)).
  - Fix [\#50775](http://developer.blender.org/T50775): Missing
    parenthesis on fluid bake button
    ([rB76c97aaf](https://projects.blender.org/blender/blender/commit/76c97aaff9)).
  - Fix [\#51737](http://developer.blender.org/T51737): Material
    properties error
    ([rB5ccaef6d](https://projects.blender.org/blender/blender/commit/5ccaef6d67)).
  - Fix [\#51845](http://developer.blender.org/T51845): UI Scale cause
    double width vertical borders
    ([rBbddb4de4](https://projects.blender.org/blender/blender/commit/bddb4de47c)).
  - Fix [\#51772](http://developer.blender.org/T51772): double undo
    entry for color picker editing
    ([rBeb1532a8](https://projects.blender.org/blender/blender/commit/eb1532a860)).
  - Fix [\#49034](http://developer.blender.org/T49034): multi-drag
    crashes when UI forces exit
    ([rBd415a1cb](https://projects.blender.org/blender/blender/commit/d415a1cbd6)).
  - Fix [\#49498](http://developer.blender.org/T49498): continuous grab
    issues on macOS, particularly with gaming mouses
    ([rB8b2785bd](https://projects.blender.org/blender/blender/commit/8b2785bda5)).
  - Fix [\#51776](http://developer.blender.org/T51776): Make sure button
    icons are updated on Ctrl-ScrollWheel
    ([rBf1d6bad4](https://projects.blender.org/blender/blender/commit/f1d6bad4b6)).
  - Fix [\#52208](http://developer.blender.org/T52208): Using
    UI\_BUT\_REDALERT flag for UI\_BTYPE\_KEY\_EVENT buttons crashes
    Blender
    ([rB304e5541](https://projects.blender.org/blender/blender/commit/304e5541cb)).
  - \[RC2\] Fix [\#52250](http://developer.blender.org/T52250): Glitch
    in UI in the addon panel regression
    ([rBade9fc92](https://projects.blender.org/blender/blender/commit/ade9fc9245)).
  - \[RC2\] Fix [\#52263](http://developer.blender.org/T52263): Crash
    When Splitting and Merging Areas with Header Text Set
    ([rBfb73cee1](https://projects.blender.org/blender/blender/commit/fb73cee1ec)).
  - \[Release\] Fix [\#52466](http://developer.blender.org/T52466):
    Silence search for button\_context menu type
    ([rB8cb21706](https://projects.blender.org/blender/blender/commit/8cb217069e)).
  - \[Release\] Fix [\#51400](http://developer.blender.org/T51400):
    Pasting hex code fails
    ([rB8193e50f](https://projects.blender.org/blender/blender/commit/8193e50fd3)).

<!-- end list -->

  - Fix unreported: Fix menu drawing printing 'unknown operator' warning
    when building without WITH\_BULLET
    ([rBc126a517](https://projects.blender.org/blender/blender/commit/c126a5179f)).
  - Fix unreported: Fix vertical scrollbar adding to region width in
    graph editor
    ([rBdf3394a3](https://projects.blender.org/blender/blender/commit/df3394a386)).
  - Fix unreported: Fix jumping view when expanding graph editor channel
    over view bounds
    ([rB4b390699](https://projects.blender.org/blender/blender/commit/4b39069908)).
  - Fix unreported: UI: Fix crash using drag-toggle over window bounds
    with button callback
    ([rBc5326958](https://projects.blender.org/blender/blender/commit/c5326958a5)).
  - Fix unreported: Fix crash in space context cycling when leaving
    window bounds
    ([rB2476faeb](https://projects.blender.org/blender/blender/commit/2476faebd7)).
  - Fix unreported: GPencil: Fix interpolate stroke keymap conflict with
    sculpt
    ([rBb8595571](https://projects.blender.org/blender/blender/commit/b859557115)).
  - Fix unreported: More fixes for keyframe theme options
    ([rB2a53e097](https://projects.blender.org/blender/blender/commit/2a53e0975c)).
  - Fix unreported: Fix unlikely uninitialized pointer usage
    ([rB0b749d57](https://projects.blender.org/blender/blender/commit/0b749d57ee)).
  - Fix unreported: Fix Make Vertex Parent operator missing from
    vertex/curve/lattice menus
    ([rB4151f127](https://projects.blender.org/blender/blender/commit/4151f12713)).
  - Fix unreported: Fix menu inconsistencies
    ([rBcb117f28](https://projects.blender.org/blender/blender/commit/cb117f283b)).
  - Fix unreported: Fix prefs UI when built w/o Cycles
    ([rB403f00e5](https://projects.blender.org/blender/blender/commit/403f00e558)).
  - Fix unreported: Fix expanding enum property in sub-layout of pie
    menus
    ([rBd5708fda](https://projects.blender.org/blender/blender/commit/d5708fdad6)).
  - Fix unreported: Fix drawing enum property with icon only flag
    ([rB524ab962](https://projects.blender.org/blender/blender/commit/524ab96245)).
  - Fix unreported: UI Layout: fix some cases mixing fixed and
    expandable sizes
    ([rBda026249](https://projects.blender.org/blender/blender/commit/da026249ab)).
  - Fix unreported: Fix blurry icons
    ([rB15b253c0](https://projects.blender.org/blender/blender/commit/15b253c082)).
  - Fix unreported: Fix Drawing nested box layouts (D2508)
    ([rB3622074b](https://projects.blender.org/blender/blender/commit/3622074bf7)).
  - Fix unreported: Fix rows with fixed last item (D2524)
    ([rB94ca09e0](https://projects.blender.org/blender/blender/commit/94ca09e01c)).
  - Fix unreported: Fix text and icon positioning issues
    ([rB32c5f3d7](https://projects.blender.org/blender/blender/commit/32c5f3d772)).
  - Fix unreported: Fix width calculation for split layouts
    ([rBf1c764fd](https://projects.blender.org/blender/blender/commit/f1c764fd8f)).
  - Fix unreported: Fix icon alignment for pie buttons
    ([rB76015f98](https://projects.blender.org/blender/blender/commit/76015f98ae)).
  - Fix unreported: Fix for splash not opening centered
    ([rB253281f9](https://projects.blender.org/blender/blender/commit/253281f9d6)).
  - Fix unreported: Fix various i18n ambiguous issues reported in
    [\#43295](http://developer.blender.org/T43295)
    ([rBa7f16c17](https://projects.blender.org/blender/blender/commit/a7f16c17c2)).
  - Fix unreported: Fix: Button's label can be NULL
    ([rB001fce16](https://projects.blender.org/blender/blender/commit/001fce167a)).
  - Fix unreported: Fix: Use "round" instead of "floor" in snapping UI
    to pixels
    ([rBfa63515c](https://projects.blender.org/blender/blender/commit/fa63515c37)).
  - Fix unreported: Fix: Ignore min flag for rows that require all
    available width
    ([rB4bdb2d48](https://projects.blender.org/blender/blender/commit/4bdb2d4885)).
  - Fix unreported: Fix padding and align calculation for box layouts
    ([rB505b3b73](https://projects.blender.org/blender/blender/commit/505b3b7328)).
  - Fix unreported: Fix columns with fixed width
    ([rB5ce120b8](https://projects.blender.org/blender/blender/commit/5ce120b865)).
  - Fix unreported: Fix: Icon offset for pie buttons
    ([rB59bb4ca1](https://projects.blender.org/blender/blender/commit/59bb4ca1b0)).
  - Fix unreported: Fix: width of UILayout.prop\_enum() buttons
    ([rB31bdb31e](https://projects.blender.org/blender/blender/commit/31bdb31ecf)).
  - Fix unreported: Fix UI: double separator in Movie Clip Editor's view
    menu
    ([rB8d78df31](https://projects.blender.org/blender/blender/commit/8d78df315c)).
  - Fix unreported: Fix 'API defined' ID properties still having
    'remove' button in UI
    ([rB42c8d93c](https://projects.blender.org/blender/blender/commit/42c8d93c5f)).
  - Fix unreported: UI: Fix some small ui inconsistencies
    ([rBf0bbb67e](https://projects.blender.org/blender/blender/commit/f0bbb67e8a)).
  - Fix unreported: Fix: Icon alignment for scaled pie buttons with no
    text
    ([rB9e08019b](https://projects.blender.org/blender/blender/commit/9e08019b74)).
  - Fix unreported: Fix node UI not using translation context correctly
    ([rBb5696f27](https://projects.blender.org/blender/blender/commit/b5696f2799)).
  - Fix unreported: Fix bad loss of precision when manually editing
    values in numbuttons
    ([rBecb5b55d](https://projects.blender.org/blender/blender/commit/ecb5b55d7f)).
  - Fix unreported: Fix: use click style if a pie was spawned by release
    or click event
    ([rB3e8b2288](https://projects.blender.org/blender/blender/commit/3e8b2288f5)).
  - Fix unreported: Fix Label colors in popups
    ([rBec228090](https://projects.blender.org/blender/blender/commit/ec22809025)).
  - Fix unreported: Fix button text overlapping with shortcut text in
    popups
    ([rB920bff52](https://projects.blender.org/blender/blender/commit/920bff5224)).
  - Fix unreported: Fix potential 'divide-by-zero' in our UI fitting
    code
    ([rB38eabcb8](https://projects.blender.org/blender/blender/commit/38eabcb858)).
  - \[RC2\] Fix unreported: Fix width estimation for empty layouts in
    pie menus
    ([rB20520236](https://projects.blender.org/blender/blender/commit/205202361c)).
  - \[RC2\] Fix unreported: Fix fixed width box layouts
    ([rB909320e3](https://projects.blender.org/blender/blender/commit/909320e3ff)).
  - \[RC2\] Fix unreported: Fix width estimation for buttons with short
    labels in pie menus
    ([rB686b8e8f](https://projects.blender.org/blender/blender/commit/686b8e8fed)).
  - \[Release\] Fix unreported: UI: fix memory leak when
    copy-to-selected failed
    ([rB9da09853](https://projects.blender.org/blender/blender/commit/9da098536d)).

## Game Engine

  - Fix [\#50098](http://developer.blender.org/T50098): BGE: Crash when
    useding ImageMirror
    ([rB741c4082](https://projects.blender.org/blender/blender/commit/741c4082d8)).

<!-- end list -->

  - Fix unreported: BGE: Fix silly typo that invalidates negative
    scaling camera feature
    ([rB89b1805d](https://projects.blender.org/blender/blender/commit/89b1805df6)).

## System / Misc

### Audio

  - Fix [\#49657](http://developer.blender.org/T49657): Audio backend
    "Jack" should be named "JACK"
    ([rB3055ae50](https://projects.blender.org/blender/blender/commit/3055ae5092)).
  - Fix [\#49657](http://developer.blender.org/T49657): Audio backend
    "Jack" should be named "JACK"
    ([rB132478d4](https://projects.blender.org/blender/blender/commit/132478d4b8)).
  - Fix [\#50065](http://developer.blender.org/T50065): Audaspace: some
    values of the lower limit of Factory.limit causes the factory not to
    play
    ([rB3340acd4](https://projects.blender.org/blender/blender/commit/3340acd46b)).
  - Fix [\#50240](http://developer.blender.org/T50240): Rendering
    crashes when synced to JACK Transport
    ([rBd874b40a](https://projects.blender.org/blender/blender/commit/d874b40a55)).
  - Fix [\#50843](http://developer.blender.org/T50843): Pitched Audio
    renders incorrectly in VSE
    ([rBf75b52ec](https://projects.blender.org/blender/blender/commit/f75b52eca1)).
  - \[Release\] Fix [\#52472](http://developer.blender.org/T52472): VSE
    Audio Volume not set immediately
    ([rB022b9676](https://projects.blender.org/blender/blender/commit/022b9676b0)).

<!-- end list -->

  - Fix unreported: Fix: setting an audio callback before audio device
    initialization
    ([rBe9689e1a](https://projects.blender.org/blender/blender/commit/e9689e1a20)).
  - Fix unreported: Fix: Audio plays back incorrectly after rendering to
    a video file
    ([rBe713009e](https://projects.blender.org/blender/blender/commit/e713009e9b)).
  - Fix unreported: Fix potential memory leak in Sequencer sound strip
    creation code
    ([rBb488988a](https://projects.blender.org/blender/blender/commit/b488988ab1)).

### Collada

  - Fix [\#50004](http://developer.blender.org/T50004): Removed check
    for empty mesh and adjusted the vertex import function to accept
    meshes without vertices as well
    ([rB447fc7c4](https://projects.blender.org/blender/blender/commit/447fc7c4ce)).
  - Fix [\#50118](http://developer.blender.org/T50118): Added missing
    assignment of Bone Roll
    ([rBd464a7c4](https://projects.blender.org/blender/blender/commit/d464a7c441)).
  - Fix [\#50923](http://developer.blender.org/T50923): Inconsistent
    default values and wrong order of parameters in api call
    ([rBb759d3c9](https://projects.blender.org/blender/blender/commit/b759d3c9c5)).
  - Fix [\#52065](http://developer.blender.org/T52065): Joint ID was
    generated wrong for bone animation exports
    ([rB9feeb14e](https://projects.blender.org/blender/blender/commit/9feeb14e91)).

<!-- end list -->

  - Fix unreported: fix D2489: Collada exporter broke edit data when
    exporting Armature while in Armature edit mode
    ([rBc64c9015](https://projects.blender.org/blender/blender/commit/c64c901535)).
  - Fix unreported: fix D2489: Collada exporter broke edit data when
    exporting Armature while in Armature edit mode
    ([rBba116c8e](https://projects.blender.org/blender/blender/commit/ba116c8e9c)).
  - Fix unreported: fix D2552: Collada - use unique id for bones with
    same name but in different armatures. Co-authored-by: Gaia
    \<gaia.clary@machiniamtrix.org\>
    ([rBda6cd776](https://projects.blender.org/blender/blender/commit/da6cd77628)).
  - Fix unreported: fix: collada - Connected bones get their tails set
    to wrong location when fix leaf nodes option is enabled
    ([rBec398944](https://projects.blender.org/blender/blender/commit/ec3989441f)).
  - Fix unreported: fix: [\#50412](http://developer.blender.org/T50412)
    - collada: Replaced precision local limit function by blender's own
    implementation
    ([rB3bf0026b](https://projects.blender.org/blender/blender/commit/3bf0026bec)).
  - Fix unreported: fix: collada - do proper conversion from int to bool
    (as with other nearby parameters)
    ([rBf65d6ea9](https://projects.blender.org/blender/blender/commit/f65d6ea954)).
  - Fix unreported: Fix collada importer doing own handling of
    usercount/freeing
    ([rB7853ebc2](https://projects.blender.org/blender/blender/commit/7853ebc204)).
  - Fix unreported: Collada: Fix: Geometry exporter did not create all
    polylist when meshes are only partially textured
    ([rBbdacb60a](https://projects.blender.org/blender/blender/commit/bdacb60a92)).
  - Fix unreported: fix: adjusted collada declaration after changes in
    collada module. @campbell Barton: Why is this declaration needed at
    all in stubs.c? Further up the file collada.h is imported and that
    already decalres the function and results in a duplicate declaration
    ([rBd17786b1](https://projects.blender.org/blender/blender/commit/d17786b107)).
  - Fix unreported: fix: collada: removed unnecessary extra
    qualification
    ([rB1340c7bc](https://projects.blender.org/blender/blender/commit/1340c7bcdc)).
  - Fix unreported: fix: Collada fprintf needs std::string be converted
    to char \*
    ([rBfcb87619](https://projects.blender.org/blender/blender/commit/fcb8761966)).
  - Fix unreported: fix: [\#51622](http://developer.blender.org/T51622)
    The exporter now exports meshes as <Triangles> when all contained
    polygons are tris
    ([rBc9b95c28](https://projects.blender.org/blender/blender/commit/c9b95c28f6)).

### File I/O

  - Fix [\#49352](http://developer.blender.org/T49352): Blender's file
    browser do not display previews
    ([rB51e8c167](https://projects.blender.org/blender/blender/commit/51e8c167f4)).
  - Fix [\#49369](http://developer.blender.org/T49369): Blender
    crashes/closes down application at alembic export of any object
    ([rB04bfea0d](https://projects.blender.org/blender/blender/commit/04bfea0d67)).
  - Fix [\#49878](http://developer.blender.org/T49878): Alembic crash
    with long object name
    ([rBd3b0977a](https://projects.blender.org/blender/blender/commit/d3b0977a35)).
  - Fix [\#49918](http://developer.blender.org/T49918): Make duplicates
    real crash on clicking operator toggles
    ([rB4e5d251c](https://projects.blender.org/blender/blender/commit/4e5d251ccb)).
  - Fix [\#50013](http://developer.blender.org/T50013): Blender 2.78a
    Link/Append Crash
    ([rB1b1d6ce1](https://projects.blender.org/blender/blender/commit/1b1d6ce131)).
  - Fix [\#49813](http://developer.blender.org/T49813): crash after
    changing Alembic cache topology
    ([rB66a36719](https://projects.blender.org/blender/blender/commit/66a3671904)).
  - Fix [\#50334](http://developer.blender.org/T50334): Also select
    indirectly imported objects when linking/appending
    ([rB934b3f36](https://projects.blender.org/blender/blender/commit/934b3f3682)).
  - Fix [\#50287](http://developer.blender.org/T50287): Blender crashes
    when open a blend that contains an alembic file
    ([rBb91edd61](https://projects.blender.org/blender/blender/commit/b91edd61d0)).
  - Fix [\#49249](http://developer.blender.org/T49249): Alembic export
    with multiple hair systems crash blender
    ([rB8cda364d](https://projects.blender.org/blender/blender/commit/8cda364d6f)).
  - Fix [\#50757](http://developer.blender.org/T50757): Alembic, assign
    imported materials to the object data instead of to the object
    itself
    ([rBcaaf5f0a](https://projects.blender.org/blender/blender/commit/caaf5f0a09)).
  - Fix [\#50227](http://developer.blender.org/T50227): Alembic uv
    export/load issue
    ([rB699a3e24](https://projects.blender.org/blender/blender/commit/699a3e2498)).
  - Fix [\#51262](http://developer.blender.org/T51262): Blender CRASH
    with alembic file
    ([rB3128600a](https://projects.blender.org/blender/blender/commit/3128600a8a)).
  - Fix [\#51292](http://developer.blender.org/T51292): Alembic import,
    show notification when trying to load HDF5
    ([rB9dadd5ff](https://projects.blender.org/blender/blender/commit/9dadd5ff93)).
  - Fix [\#51280](http://developer.blender.org/T51280): Alembic: Crash
    when removing cache modifier
    ([rBff1f1157](https://projects.blender.org/blender/blender/commit/ff1f115706)).
  - Fix [\#51432](http://developer.blender.org/T51432): Find Files case
    sensitive on win32
    ([rB1cfc4819](https://projects.blender.org/blender/blender/commit/1cfc48192c)).
  - Fix [\#51336](http://developer.blender.org/T51336): Crash on broken
    file opening
    ([rBbaf788d7](https://projects.blender.org/blender/blender/commit/baf788d7cd)).
  - Fix [\#51319](http://developer.blender.org/T51319): Alembic export
    crash w/simple child particles if Display value \< 100%
    ([rB8d26f2c2](https://projects.blender.org/blender/blender/commit/8d26f2c222)).
  - Fix [\#51534](http://developer.blender.org/T51534): Alembic: added
    support for face-varying vertex colours
    ([rB7b25ffb6](https://projects.blender.org/blender/blender/commit/7b25ffb618)).
  - Fix [\#51586](http://developer.blender.org/T51586): Regression:
    Alembic containing animated curves / hair no longer working
    ([rBad27e97e](https://projects.blender.org/blender/blender/commit/ad27e97ee7)).
  - Fix [\#51820](http://developer.blender.org/T51820): Alembic: for
    sequence files not loading properly
    ([rB0900914d](https://projects.blender.org/blender/blender/commit/0900914d96)).
  - Fix [\#51015](http://developer.blender.org/T51015): Pack all into
    blend automatically leaves checkbox enabled
    ([rBbdd814d8](https://projects.blender.org/blender/blender/commit/bdd814d87d)).
  - Fix [\#51052](http://developer.blender.org/T51052): CacheFile Open
    crashes from Python
    ([rB2c10e8a3](https://projects.blender.org/blender/blender/commit/2c10e8a3cf)).
  - Fix [\#52022](http://developer.blender.org/T52022): Alembic Inherits
    transform not taken into account
    ([rB32edfd53](https://projects.blender.org/blender/blender/commit/32edfd53d9)).
  - Fix [\#52109](http://developer.blender.org/T52109): Folder search
    won't work when selecting animation output folder
    ([rB3cfb248b](https://projects.blender.org/blender/blender/commit/3cfb248bb6)).
  - \[RC2\] Fix [\#52240](http://developer.blender.org/T52240): Alembic
    Not Transferring Materials Per Frame
    ([rBf21020f4](https://projects.blender.org/blender/blender/commit/f21020f45f)).
  - \[Release\] Fix [\#52481](http://developer.blender.org/T52481):
    After making all local, local proxies of linked data get broken
    after file save and reload
    ([rBfc26280b](https://projects.blender.org/blender/blender/commit/fc26280bcb)).
  - \[Release\] Fix [\#52579](http://developer.blender.org/T52579):
    Alembic: crash when replacing slightly different alembic files
    ([rB4b90830c](https://projects.blender.org/blender/blender/commit/4b90830cac)).

<!-- end list -->

  - Fix unreported: CacheFile: fix missing depsgraph update
    ([rB65c481e1](https://projects.blender.org/blender/blender/commit/65c481e145)).
  - Fix unreported: Alembic export: fix frame range values being reset
    at every update, draw call
    ([rB0c137924](https://projects.blender.org/blender/blender/commit/0c13792437)).
  - Fix unreported: Fix forward-compat Nodes write code being executed
    also for undo steps writing
    ([rB5a6534a5](https://projects.blender.org/blender/blender/commit/5a6534a5bb)).
  - Fix unreported: Fix crash when opening a Blender file containing
    Alembic data
    ([rB62a2ed97](https://projects.blender.org/blender/blender/commit/62a2ed97ba)).
  - Fix unreported: Fix (unreported) linked datablocks going through
    do\_versions several times
    ([rBbd429873](https://projects.blender.org/blender/blender/commit/bd42987399)).
  - Fix unreported: Fix (unreported) crash when file browser attempts to
    show preview of some defective font
    ([rBf14e1da5](https://projects.blender.org/blender/blender/commit/f14e1da5aa)).
  - Fix unreported: Alembic/CacheFile: fix crash de-referencing NULL
    pointer
    ([rB4580ace4](https://projects.blender.org/blender/blender/commit/4580ace4c1)).
  - Fix unreported: Fix (unreported) Object previews being written even
    for skipped objects
    ([rBf7eaaf35](https://projects.blender.org/blender/blender/commit/f7eaaf35b4)).
  - Fix unreported: Alembic: fixed mistake in bounding box computation
    ([rBb929eef8](https://projects.blender.org/blender/blender/commit/b929eef8c5)).
  - Fix unreported: Alembic: fix naming of imported transforms
    ([rB54102ab3](https://projects.blender.org/blender/blender/commit/54102ab36e)).
  - Fix unreported: Fix lib\_link\_cachefile
    ([rBe1909958](https://projects.blender.org/blender/blender/commit/e1909958d9)).
  - Fix unreported: Alembic: fixed importer
    ([rB02d6df80](https://projects.blender.org/blender/blender/commit/02d6df80aa)).
  - Fix unreported: Alembic import: fixed bug where local matrix from
    Alembic was used as object matrix
    ([rBd1696622](https://projects.blender.org/blender/blender/commit/d1696622b7)).
  - Fix unreported: Alembic import: fixed crash on more complex model
    ([rBbc55c198](https://projects.blender.org/blender/blender/commit/bc55c19807)).
  - Fix unreported: Alembic export: fixed exporting as "flat"
    ([rB642728b3](https://projects.blender.org/blender/blender/commit/642728b339)).
  - Fix unreported: Alembic import: fixed off-by-one error in start/end
    frame
    ([rB0706b908](https://projects.blender.org/blender/blender/commit/0706b908db)).
  - Fix unreported: Alembic export: fixed flattened dupligroup import
    ([rB4d117f2f](https://projects.blender.org/blender/blender/commit/4d117f2fd2)).
  - Fix unreported: Alembic import: fixed dupligroup export when the
    dupli-empty has a parent
    ([rB5fa4f397](https://projects.blender.org/blender/blender/commit/5fa4f397c2)).
  - Fix unreported: Alembic export: fixed curve type and order
    ([rBd24578b6](https://projects.blender.org/blender/blender/commit/d24578b676)).
  - Fix unreported: Alembic: fixed refcount issue when duplicating
    imported objects
    ([rB20621d46](https://projects.blender.org/blender/blender/commit/20621d46d1)).
  - Fix unreported: Proper fix for crash loading old files with
    compositor
    ([rB3de9db96](https://projects.blender.org/blender/blender/commit/3de9db9650)).
  - Fix unreported: Alembic import: fixed bug interpolating between
    frames
    ([rB24a0b332](https://projects.blender.org/blender/blender/commit/24a0b332e2)).
  - Fix unreported: Fix (unreported) seldom crash when using previews in
    filebrowser
    ([rB73adf3e2](https://projects.blender.org/blender/blender/commit/73adf3e27d)).
  - \[RC2\] Fix unreported: Alembic import: fix crash when face color
    index is out of bounds
    ([rB2307ea88](https://projects.blender.org/blender/blender/commit/2307ea88b5)).

### Other

  - Fix [\#49899](http://developer.blender.org/T49899): Add
    EIGEN\_MAKE\_ALIGNED\_OPERATOR\_NEW to classes that use eigen's data
    types , to force aligned on 16 byte boundaries
    ([rB0de157a3](https://projects.blender.org/blender/blender/commit/0de157a320)).
  - Fix [\#50305](http://developer.blender.org/T50305): Blender
    truncates a long multibyte character object's name to an invalid
    utf-8 string
    ([rB752a783f](https://projects.blender.org/blender/blender/commit/752a783fa4)).
  - Fix [\#51024](http://developer.blender.org/T51024): Switch
    install\_deps to set OSL\_ROOT\_DIR instead of CYCLES\_OSL
    ([rB797b1d50](https://projects.blender.org/blender/blender/commit/797b1d5053)).
  - Fix [\#51624](http://developer.blender.org/T51624): Scene Full Copy
    ignores shader node links
    ([rBbe31582e](https://projects.blender.org/blender/blender/commit/be31582e3d)).
  - Fix [\#51889](http://developer.blender.org/T51889): broken UI after
    File \> New without a userpref.blend
    ([rB46c7d45f](https://projects.blender.org/blender/blender/commit/46c7d45f77)).
  - Fix [\#51998](http://developer.blender.org/T51998): Anim player uses
    100% CPU
    ([rB910ab141](https://projects.blender.org/blender/blender/commit/910ab14145)).
  - \[Release\] Fix [\#52396](http://developer.blender.org/T52396):
    Crash loading template w/o config dir
    ([rB76e8dcaf](https://projects.blender.org/blender/blender/commit/76e8dcafa3)).

<!-- end list -->

  - Fix unreported: API doc update script: Fix generated zipfile name,
    was broken in 'release' case
    ([rB56c798ee](https://projects.blender.org/blender/blender/commit/56c798ee62)).
  - Fix unreported: Fix (unreported) some RNA func definitions setting
    flags of other func parameters\!
    ([rB7415b9ff](https://projects.blender.org/blender/blender/commit/7415b9ffa3)).
  - Fix unreported: Fix (unreported) \`--threads\` option no more
    respected by main task scheduler
    ([rB8db2f729](https://projects.blender.org/blender/blender/commit/8db2f72997)).
  - Fix unreported: Fix (unreported) fully broken 'sanitize utf-8'
    helper
    ([rBadadfaad](https://projects.blender.org/blender/blender/commit/adadfaad88)).
  - Fix unreported: Fix/cleanup stupid check on array of char being
    non-NULL pointer
    ([rBa97ec403](https://projects.blender.org/blender/blender/commit/a97ec403c2)).
  - Fix unreported: Fix 'public' global 'g\_atexit' var in Blender
    ([rBac8348d0](https://projects.blender.org/blender/blender/commit/ac8348d033)).
  - Fix unreported: Fix some more minor issue with updated py doc
    generation
    ([rB29859d0d](https://projects.blender.org/blender/blender/commit/29859d0d5e)).
  - Fix unreported: Fix bug on Blender version string
    ([rB81dc8dd4](https://projects.blender.org/blender/blender/commit/81dc8dd42a)).
  - Fix unreported: Install deps: Fix compilation error of Alembic
    ([rB358def15](https://projects.blender.org/blender/blender/commit/358def15a3)).
  - Fix unreported: Fix: Object.raycast: error to free treedata
    ([rB34ea8058](https://projects.blender.org/blender/blender/commit/34ea8058b9)).
  - Fix unreported: Docs: Fix <file:line> links in generated API docs
    ([rBb94a433c](https://projects.blender.org/blender/blender/commit/b94a433ca3)).
  - \[RC2\] Fix unreported: Fix broken API doc generation: Partially
    revert[rBa372638a](https://projects.blender.org/blender/blender/commit/a372638a76e0)
    ([rB8ae6e359](https://projects.blender.org/blender/blender/commit/8ae6e35923)).

### Python

  - Fix [\#49829](http://developer.blender.org/T49829): Removal of
    custom icon previews during add-on unregister crashes Blender
    ([rB5f0933f0](https://projects.blender.org/blender/blender/commit/5f0933f07a)).
  - Fix [\#49856](http://developer.blender.org/T49856): Blender 2.78
    crashes after loading data from a blendfile
    ([rB1ee43c5a](https://projects.blender.org/blender/blender/commit/1ee43c5aef)).
  - Fix [\#50007](http://developer.blender.org/T50007): blender offline
    python documentation in zipped HTML files, not shown correctly
    ([rB7e8bf9db](https://projects.blender.org/blender/blender/commit/7e8bf9dbd6)).
  - Fix [\#50052](http://developer.blender.org/T50052):
    bpy.utils.unregister\_module doesn't unregister classes of
    submodules in reload scenario
    ([rB8f0dc3ce](https://projects.blender.org/blender/blender/commit/8f0dc3cef6)).
  - Fix [\#50029](http://developer.blender.org/T50029):
    BVHTree.FromPolygons memory leak
    ([rB3b467b35](https://projects.blender.org/blender/blender/commit/3b467b35a8)).
  - Fix [\#50926](http://developer.blender.org/T50926): python crashes
    with path containing utf8 characters
    ([rB43f7d564](https://projects.blender.org/blender/blender/commit/43f7d5643f)).
  - Fix [\#51287](http://developer.blender.org/T51287): Matrix.lerp
    fails w/ shear
    ([rB98df7d77](https://projects.blender.org/blender/blender/commit/98df7d778f)).
  - Fix [\#51810](http://developer.blender.org/T51810): Add minimal
    example of usage of translation API for non-official addons
    ([rBfc4154f8](https://projects.blender.org/blender/blender/commit/fc4154f857)).
  - Fix [\#52195](http://developer.blender.org/T52195): Sculpt from
    Python fails
    ([rBf3782c0a](https://projects.blender.org/blender/blender/commit/f3782c0a9e)).
  - \[RC2\] Fix [\#46329](http://developer.blender.org/T46329):
    scene\_update\_{pre,post} doc needs clarification
    ([rB6bc962d7](https://projects.blender.org/blender/blender/commit/6bc962d7bc)).
  - \[Release\] Fix [\#52515](http://developer.blender.org/T52515):
    Crash on BMesh.to\_mesh()
    ([rB5fd4eca8](https://projects.blender.org/blender/blender/commit/5fd4eca8c0)).

<!-- end list -->

  - Fix unreported: Fix objects added via py being on the wrong layer
    when viewport is decoupled from scene
    ([rB17603b9f](https://projects.blender.org/blender/blender/commit/17603b9f01)).
  - Fix unreported: API: Fix Links
    ([rBcf9a6b41](https://projects.blender.org/blender/blender/commit/cf9a6b416c)).
  - Fix unreported: Fix custom props not being handled correctly by
    manual/pyref UI menu entries
    ([rBe17b92f5](https://projects.blender.org/blender/blender/commit/e17b92f535)).
  - Fix unreported: API: Fix double slashes in URLs
    ([rB911544c7](https://projects.blender.org/blender/blender/commit/911544c70c)).
  - Fix unreported: API: Fix redirect in bgl page
    ([rB42e4b095](https://projects.blender.org/blender/blender/commit/42e4b0955c)).
  - Fix unreported: API: Fix rst syntax
    ([rB13174df5](https://projects.blender.org/blender/blender/commit/13174df534)).
  - Fix unreported: Fix missing uniform type for python GPU uniform
    export
    ([rBd4e0557c](https://projects.blender.org/blender/blender/commit/d4e0557cf1)).
  - Fix unreported: Fix incorrect spot lamp blend in python GPU uniform
    export
    ([rB7f10a889](https://projects.blender.org/blender/blender/commit/7f10a889e3)).
  - Fix unreported: Fix memory leak re-registering operators
    ([rBd808557d](https://projects.blender.org/blender/blender/commit/d808557d15)).
  - Fix unreported: Fix 'bl\_app\_override' wrapping multiple times
    ([rB4f69dca5](https://projects.blender.org/blender/blender/commit/4f69dca547)).
  - Fix unreported: Fix PyAPI crash assigning/deleting id-properties
    ([rB5cdd94a5](https://projects.blender.org/blender/blender/commit/5cdd94a58e)).
  - Fix unreported: Property path generation fixes
    ([rB09eac015](https://projects.blender.org/blender/blender/commit/09eac0159d)).
  - \[RC2\] Fix unreported: PyAPI: Fix memory leak w/ empty, allocated
    enums
    ([rB9bc55492](https://projects.blender.org/blender/blender/commit/9bc5549222)).
  - \[Release\] Fix unreported: Fix bpy library load: invalid function
    signature
    ([rBfd0fbf25](https://projects.blender.org/blender/blender/commit/fd0fbf2564)).
  - \[Release\] Fix unreported: Fix error in PointerProperty argument
    list
    ([rB6692af39](https://projects.blender.org/blender/blender/commit/6692af3997)).
  - \[Release\] Fix unreported: PyAPI: Fix mathutils freeze allowing
    owned data
    ([rB1a7dda04](https://projects.blender.org/blender/blender/commit/1a7dda046b)).

### System

  - Fix [\#49385](http://developer.blender.org/T49385): Copy buffer is
    not from pose mode. Report Error
    ([rB25ee0942](https://projects.blender.org/blender/blender/commit/25ee094226)).
  - Fix [\#49903](http://developer.blender.org/T49903): Blender crashes
    -\> Append Group incl. Object using boolean modifier
    ([rB4a68ff15](https://projects.blender.org/blender/blender/commit/4a68ff150f)).
  - Fix [\#49905](http://developer.blender.org/T49905): Segfault when
    copying object data of linked object
    ([rBf0ac661a](https://projects.blender.org/blender/blender/commit/f0ac661aa8)).
  - Fix [\#49991](http://developer.blender.org/T49991): reloading
    librairies doesn't update node groups
    ([rB369872a2](https://projects.blender.org/blender/blender/commit/369872a2c5)).
  - Fix [\#50034](http://developer.blender.org/T50034): Blender changes
    processor affinity unauthorized
    ([rB751573ce](https://projects.blender.org/blender/blender/commit/751573ce6f)).
  - Fix [\#46795](http://developer.blender.org/T46795): Reset
    GWLP\_USERDATA to NULL at window destruction so any future events
    will not try to reference this deleted class
    ([rB05b181fb](https://projects.blender.org/blender/blender/commit/05b181fbc5)).
  - Fix [\#50305](http://developer.blender.org/T50305): When adding new
    ID with same name as existing, Blender could generate invalid utf-8
    ID name
    ([rB7924c84c](https://projects.blender.org/blender/blender/commit/7924c84c44)).
  - Fix [\#50385](http://developer.blender.org/T50385): Deadlock in
    BKE\_libblock\_remap\_locked
    ([rB9c756162](https://projects.blender.org/blender/blender/commit/9c756162ae)).
  - Fix [\#50676](http://developer.blender.org/T50676): Crash on closing
    while frameserver rendering
    ([rB9062c086](https://projects.blender.org/blender/blender/commit/9062c086b4)).
  - Fix [\#49655](http://developer.blender.org/T49655): Reloading
    library breaks proxies
    ([rBdf88d542](https://projects.blender.org/blender/blender/commit/df88d54284)).
  - Fix [\#50886](http://developer.blender.org/T50886): Blender crashes
    on render
    ([rBa095611e](https://projects.blender.org/blender/blender/commit/a095611eb8)).
  - Fix [\#50788](http://developer.blender.org/T50788): blender startup
    crash on macOS with some types of volumes available
    ([rB3f948369](https://projects.blender.org/blender/blender/commit/3f94836922)).
  - Fix [\#51150](http://developer.blender.org/T51150): user\_remap on
    itself changes users to 0
    ([rB4d0d1b59](https://projects.blender.org/blender/blender/commit/4d0d1b5936)).
  - Fix [\#51243](http://developer.blender.org/T51243): Delete Globally
    won't work with Datablock ID Properties
    ([rB1873ea33](https://projects.blender.org/blender/blender/commit/1873ea337c)).
  - Fix [\#50856](http://developer.blender.org/T50856): crash when
    minimizing window on AMD / Windows
    ([rBce531ed1](https://projects.blender.org/blender/blender/commit/ce531ed1a1)).
  - Fix [\#51902](http://developer.blender.org/T51902): Severe problem
    with relocating linked libraries when using proxies
    ([rBd4ca2ec9](https://projects.blender.org/blender/blender/commit/d4ca2ec9d5)).
  - Fix [\#51889](http://developer.blender.org/T51889): new file or load
    factory settings results in broken UI
    ([rB7dc3ad22](https://projects.blender.org/blender/blender/commit/7dc3ad2287)).
  - Fix [\#51959](http://developer.blender.org/T51959): Windows + Intel
    GPU offset between UI drawing and mouse
    ([rB0584c5ba](https://projects.blender.org/blender/blender/commit/0584c5ba8e)).
  - Fix [\#52148](http://developer.blender.org/T52148): Point Density
    Texture ID User decrement error related to the Object field
    ([rB05f37780](https://projects.blender.org/blender/blender/commit/05f377805b)).
  - \[RC2\] Fix [\#52260](http://developer.blender.org/T52260): Blender
    2.79 Objects made duplicates real still refer armature proxy
    ([rBfa34f864](https://projects.blender.org/blender/blender/commit/fa34f864a2)).
  - \[RC2\] Fix [\#52315](http://developer.blender.org/T52315): Crash on
    duplicating Scene without world
    ([rB0146ab2f](https://projects.blender.org/blender/blender/commit/0146ab2fd5)).
  - \[RC2\] Fix [\#52278](http://developer.blender.org/T52278):
    'Default' application template fails
    ([rBa52ec530](https://projects.blender.org/blender/blender/commit/a52ec5308f)).

<!-- end list -->

  - Fix unreported: Fix some assert when making local (due to infamous
    PITA ShapeKey ID)
    ([rB18be39ff](https://projects.blender.org/blender/blender/commit/18be39ff17)).
  - Fix unreported: Fix \`BKE\_library\_make\_local()\` trying to also
    make local proxified objects
    ([rBe316636f](https://projects.blender.org/blender/blender/commit/e316636fa8)).
  - Fix unreported: Fix two very bad issues in new ID.make\_local RNA
    function
    ([rBb97c567c](https://projects.blender.org/blender/blender/commit/b97c567c1d)).
  - Fix unreported: Fix (IRC reported) bad handling of Text data-block
    user count
    ([rBa9163f7d](https://projects.blender.org/blender/blender/commit/a9163f7d22)).
  - Fix unreported: Fix missing user when opening text from ID UI widget
    ([rB646aa40c](https://projects.blender.org/blender/blender/commit/646aa40cf7)).
  - Fix unreported: \[Cycles/MSVC/Testing\] Fix broken test code
    ([rB64f5afdb](https://projects.blender.org/blender/blender/commit/64f5afdb89)).
  - Fix unreported: Fix missing non-ID nodetrees in ID relationships
    built from library\_query.c
    ([rBfbd28d37](https://projects.blender.org/blender/blender/commit/fbd28d375a)).
  - Fix unreported: Fix ugly mistake in BLI\_task - freeing while some
    tasks are still being processed
    ([rB18c2a443](https://projects.blender.org/blender/blender/commit/18c2a44333)).
  - Fix unreported: Fix GHOST crash on X11 with recent DPI changes on
    some systems
    ([rB393efccb](https://projects.blender.org/blender/blender/commit/393efccb19)).
  - Fix unreported: Fix crash closing window in background mode
    ([rBa7ca9918](https://projects.blender.org/blender/blender/commit/a7ca991841)).
  - Fix unreported: Fix missing protection of
    \`RNA\_pointer\_as\_string()\` against NULL pointers
    ([rB9170e492](https://projects.blender.org/blender/blender/commit/9170e49250)).
  - Fix unreported: Fix (unreported) missing handling of GPencil Layer's
    parent Object pointer in BKE\_library\_query
    ([rB1c28e124](https://projects.blender.org/blender/blender/commit/1c28e12414)).
  - Fix unreported: Fix use after free of new render layer ID properites
    after copying scene
    ([rBffa31a84](https://projects.blender.org/blender/blender/commit/ffa31a8421)).
  - Fix unreported: Fix missing usercount update of poselib when copying
    Object
    ([rBb180900e](https://projects.blender.org/blender/blender/commit/b180900e52)).
  - Fix unreported: Fix bad handling of 'extra' user for groups at their
    creation
    ([rB8b0f968a](https://projects.blender.org/blender/blender/commit/8b0f968a31)).
  - Fix unreported: Fix crash when making local object+obdata with
    linked armature
    ([rBf097e73a](https://projects.blender.org/blender/blender/commit/f097e73a2a)).
  - Fix unreported: Fix three obvious mistakes in brush/mask/cachefile
    ID copying
    ([rB31437b0d](https://projects.blender.org/blender/blender/commit/31437b0d4d)).
  - Fix unreported: Fix potentially dnagerous code in doversionning of
    brush
    ([rB25c0666b](https://projects.blender.org/blender/blender/commit/25c0666b90)).
  - Fix unreported: Fix dangerous code when deleting Scene
    ([rBee5ed2ae](https://projects.blender.org/blender/blender/commit/ee5ed2ae26)).
  - Fix unreported: Guarded allocator: Fix type in macro definition
    ([rB8bf108dd](https://projects.blender.org/blender/blender/commit/8bf108dd48)).
  - Fix unreported: Fix ED\_OT\_undo\_redo operator
    ([rB5b2b5a42](https://projects.blender.org/blender/blender/commit/5b2b5a4258)).
  - Fix unreported: Fix (unreported) Scene copying doing very stupid
    things with World and LineStyle usercounts
    ([rB806bc4b4](https://projects.blender.org/blender/blender/commit/806bc4b433)).
  - Fix unreported: Fix (unreported) Scene's copying toolsettings' clone
    and canvas, and particles' scene and object pointers
    ([rB665288cc](https://projects.blender.org/blender/blender/commit/665288ccd7)).
  - Fix unreported: Fix (unreported) broken uvsculpt in Scene's
    toolsettings' copying
    ([rB8677c76f](https://projects.blender.org/blender/blender/commit/8677c76f13)).
  - Fix unreported: Fix factory setup using user scripts path still
    ([rB40034094](https://projects.blender.org/blender/blender/commit/4003409430)).
  - \[RC2\] Fix unreported: Fix OSX duplicate path in Python's sys.path
    ([rB6e7d9621](https://projects.blender.org/blender/blender/commit/6e7d962118)).

<hr/>

## RC2

For RC2, 25 bugs were fixed:

  - Fix [\#52327](http://developer.blender.org/T52327): Entering/Exiting
    NLA Tweakmode disables Scene -\> Only Keyframes from Selected
    Channels
    ([rB3689be73](https://projects.blender.org/blender/blender/commit/3689be736b)).
  - Fix [\#52401](http://developer.blender.org/T52401): "Export Keying
    Set" operator generated incorrect ID's for shapekeys
    ([rB7b397cdf](https://projects.blender.org/blender/blender/commit/7b397cdfc8)).
  - Fix [\#52255](http://developer.blender.org/T52255): New Depsgraph -
    Constraint and Drivers not working together when the driver
    references itself
    ([rBb1d998ec](https://projects.blender.org/blender/blender/commit/b1d998ec5d)).
  - Fix [\#52329](http://developer.blender.org/T52329): Boolean with
    aligned shapes failed
    ([rB0cb38b28](https://projects.blender.org/blender/blender/commit/0cb38b2875)).
  - Fix [\#52324](http://developer.blender.org/T52324): Metaball
    disappears when deleting first metaball object
    ([rBff47118c](https://projects.blender.org/blender/blender/commit/ff47118c73)).
  - Fix [\#51701](http://developer.blender.org/T51701): Alembic cache
    screws up mesh
    ([rB8138082a](https://projects.blender.org/blender/blender/commit/8138082ab8)).
  - Fix [\#52344](http://developer.blender.org/T52344): Softbody on Text
    ([rBefa840f9](https://projects.blender.org/blender/blender/commit/efa840f99f)).
  - Fix [\#52344](http://developer.blender.org/T52344): Softbody on Text
    ([rB9a239eea](https://projects.blender.org/blender/blender/commit/9a239eea68)).
  - Fix [\#52334](http://developer.blender.org/T52334): images with
    non-color data should not change color space on save
    ([rB35f5d80f](https://projects.blender.org/blender/blender/commit/35f5d80f3a)).
  - Fix [\#52280](http://developer.blender.org/T52280): The Image node
    in Compositing can't read Z buffer of openEXR in 2.79
    ([rBe54df78c](https://projects.blender.org/blender/blender/commit/e54df78c82)).
  - Fix unreported: Cycles: Fixed broken camera motion blur when motion
    was not set to center on frame
    ([rB10764d31](https://projects.blender.org/blender/blender/commit/10764d31d4)).
  - Fix [\#52250](http://developer.blender.org/T52250): Glitch in UI in
    the addon panel regression
    ([rBade9fc92](https://projects.blender.org/blender/blender/commit/ade9fc9245)).
  - Fix [\#52263](http://developer.blender.org/T52263): Crash When
    Splitting and Merging Areas with Header Text Set
    ([rBfb73cee1](https://projects.blender.org/blender/blender/commit/fb73cee1ec)).
  - Fix unreported: Fix width estimation for empty layouts in pie menus
    ([rB20520236](https://projects.blender.org/blender/blender/commit/205202361c)).
  - Fix unreported: Fix fixed width box layouts
    ([rB909320e3](https://projects.blender.org/blender/blender/commit/909320e3ff)).
  - Fix unreported: Fix width estimation for buttons with short labels
    in pie menus
    ([rB686b8e8f](https://projects.blender.org/blender/blender/commit/686b8e8fed)).
  - Fix [\#52240](http://developer.blender.org/T52240): Alembic Not
    Transferring Materials Per Frame
    ([rBf21020f4](https://projects.blender.org/blender/blender/commit/f21020f45f)).
  - Fix unreported: Alembic import: fix crash when face color index is
    out of bounds
    ([rB2307ea88](https://projects.blender.org/blender/blender/commit/2307ea88b5)).
  - Fix unreported: Fix broken API doc generation: Partially
    revert[rBa372638a](https://projects.blender.org/blender/blender/commit/a372638a76e0)
    ([rB8ae6e359](https://projects.blender.org/blender/blender/commit/8ae6e35923)).
  - Fix [\#46329](http://developer.blender.org/T46329):
    scene\_update\_{pre,post} doc needs clarification
    ([rB6bc962d7](https://projects.blender.org/blender/blender/commit/6bc962d7bc)).
  - Fix unreported: PyAPI: Fix memory leak w/ empty, allocated enums
    ([rB9bc55492](https://projects.blender.org/blender/blender/commit/9bc5549222)).
  - Fix [\#52260](http://developer.blender.org/T52260): Blender 2.79
    Objects made duplicates real still refer armature proxy
    ([rBfa34f864](https://projects.blender.org/blender/blender/commit/fa34f864a2)).
  - Fix [\#52315](http://developer.blender.org/T52315): Crash on
    duplicating Scene without world
    ([rB0146ab2f](https://projects.blender.org/blender/blender/commit/0146ab2fd5)).
  - Fix [\#52278](http://developer.blender.org/T52278): 'Default'
    application template fails
    ([rBa52ec530](https://projects.blender.org/blender/blender/commit/a52ec5308f)).
  - Fix unreported: Fix OSX duplicate path in Python's sys.path
    ([rB6e7d9621](https://projects.blender.org/blender/blender/commit/6e7d962118)).

## Release

For Release, 50 bugs were fixed:

  - Fix unreported: Fix bpy library load: invalid function signature
    ([rBfd0fbf25](https://projects.blender.org/blender/blender/commit/fd0fbf2564)).
  - Fix [\#52434](http://developer.blender.org/T52434): Restore mesh
    center of mass calculation
    ([rBba600ff7](https://projects.blender.org/blender/blender/commit/ba600ff7fa)).
  - Fix unreported: Fix error in PointerProperty argument list
    ([rB6692af39](https://projects.blender.org/blender/blender/commit/6692af3997)).
  - Fix [\#52439](http://developer.blender.org/T52439): Crash after
    adjusting lenght of hair particles
    ([rBd2f20aed](https://projects.blender.org/blender/blender/commit/d2f20aed04)).
  - Fix [\#52473](http://developer.blender.org/T52473): blender internal
    Fresnel and Layer Weight only work with linked normal
    ([rBdd843244](https://projects.blender.org/blender/blender/commit/dd84324485)).
  - Fix [\#52454](http://developer.blender.org/T52454): Crash in
    DEG\_graph\_on\_visible\_update when activating scene layer
    ([rB63e21e72](https://projects.blender.org/blender/blender/commit/63e21e7218)).
  - Fix unreported: Fix threading conflict when doing Cycles background
    render
    ([rB9997f5ca](https://projects.blender.org/blender/blender/commit/9997f5ca91)).
  - Fix [\#52466](http://developer.blender.org/T52466): Silence search
    for button\_context menu type
    ([rB8cb21706](https://projects.blender.org/blender/blender/commit/8cb217069e)).
  - Fix [\#52218](http://developer.blender.org/T52218): Missing update
    when reconnecting node
    ([rBf2aa9bec](https://projects.blender.org/blender/blender/commit/f2aa9bec9d)).
  - Fix [\#51805](http://developer.blender.org/T51805): Overlapping
    volumes renders incorrect on AMD GPU
    ([rB6825439b](https://projects.blender.org/blender/blender/commit/6825439b36)).
  - Fix unreported: Cycles: Fix stack overflow during traversal caused
    by floating overflow
    ([rB75e392ae](https://projects.blender.org/blender/blender/commit/75e392ae9f)).
  - Fix [\#52481](http://developer.blender.org/T52481): After making all
    local, local proxies of linked data get broken after file save and
    reload
    ([rBfc26280b](https://projects.blender.org/blender/blender/commit/fc26280bcb)).
  - Fix [\#52538](http://developer.blender.org/T52538): Outliner crash
    when displaying groups and using Show Active on editmode bone not in
    any groups
    ([rB42760d92](https://projects.blender.org/blender/blender/commit/42760d922e)).
  - Fix [\#52478](http://developer.blender.org/T52478): Error report
    "Shrinkwrap: out of memory" on invisible target
    ([rBa5213924](https://projects.blender.org/blender/blender/commit/a5213924a8)).
  - Fix [\#52498](http://developer.blender.org/T52498): Deleting force
    field doesn't remove "Surface" from modifier stack
    ([rB82a6889d](https://projects.blender.org/blender/blender/commit/82a6889d83)).
  - Fix [\#52588](http://developer.blender.org/T52588): Shape key value
    driver variables of duplicated object sets refer to old objects
    ([rBe9ca9dd5](https://projects.blender.org/blender/blender/commit/e9ca9dd5d7)).
  - Fix [\#52472](http://developer.blender.org/T52472): VSE Audio Volume
    not set immediately
    ([rB022b9676](https://projects.blender.org/blender/blender/commit/022b9676b0)).
  - Fix [\#52227](http://developer.blender.org/T52227): Time Slide tool
    doesn't take NLA mapping into account
    ([rB73cdf00e](https://projects.blender.org/blender/blender/commit/73cdf00ea8)).
  - Fix [\#52396](http://developer.blender.org/T52396): Crash loading
    template w/o config dir
    ([rB76e8dcaf](https://projects.blender.org/blender/blender/commit/76e8dcafa3)).
  - Fix [\#52490](http://developer.blender.org/T52490): NDOF orbit
    doesn't lock in ortho view
    ([rB0ed5605b](https://projects.blender.org/blender/blender/commit/0ed5605bd5)).
  - Fix unreported: Correction to last fix
    ([rB7997646c](https://projects.blender.org/blender/blender/commit/7997646c2d)).
  - Fix unreported: Fix minor Mesh -\> BMesh conversion issues
    ([rB6fec0692](https://projects.blender.org/blender/blender/commit/6fec06926e)).
  - Fix [\#52515](http://developer.blender.org/T52515): Crash on
    BMesh.to\_mesh()
    ([rB5fd4eca8](https://projects.blender.org/blender/blender/commit/5fd4eca8c0)).
  - Fix [\#51400](http://developer.blender.org/T51400): Pasting hex code
    fails
    ([rB8193e50f](https://projects.blender.org/blender/blender/commit/8193e50fd3)).
  - Fix [\#52483](http://developer.blender.org/T52483): Fill is
    incorrect for interpolated strokes
    ([rB27c42e05](https://projects.blender.org/blender/blender/commit/27c42e05c4)).
  - Fix unreported: Fix: GPencil Sequence Interpolation for
    thickness/strength was inverted
    ([rBed0429e7](https://projects.blender.org/blender/blender/commit/ed0429e7e6)).
  - Fix unreported: Fix: Border select for GPencil keyframes was
    including those in the "datablock" channels even though those
    weren't visible
    ([rBf5d02f05](https://projects.blender.org/blender/blender/commit/f5d02f055f)).
  - Fix unreported: Fix: Deleting GPencil keyframes in DopeSheet didn't
    redraw the view
    ([rBa1e8ef26](https://projects.blender.org/blender/blender/commit/a1e8ef264f)).
  - Fix [\#52579](http://developer.blender.org/T52579): Alembic: crash
    when replacing slightly different alembic files
    ([rB4b90830c](https://projects.blender.org/blender/blender/commit/4b90830cac)).
  - Fix unreported: Cycles: FIx issue with -0 being considered a
    non-finite value
    ([rB95d871c1](https://projects.blender.org/blender/blender/commit/95d871c1a2)).
  - Fix [\#51907](http://developer.blender.org/T51907): New Depsgraph -
    Camera constraint is not evaluated properly
    ([rBedded659](https://projects.blender.org/blender/blender/commit/edded659c6)).
  - Fix [\#52533](http://developer.blender.org/T52533): Blender shuts
    down when rendering duplicated smoke domain
    ([rBc9d653e5](https://projects.blender.org/blender/blender/commit/c9d653e560)).
  - Fix [\#52209](http://developer.blender.org/T52209): New Depsgraph -
    animated follow curve constraint sometimes freaks out when the curve
    has a parent
    ([rB0bee1269](https://projects.blender.org/blender/blender/commit/0bee126977)).
  - Fix unreported: Cycles Bake: Fix overflow when using hundreds of
    images
    ([rB9ca3d4cb](https://projects.blender.org/blender/blender/commit/9ca3d4cbbd)).
  - Fix [\#52251](http://developer.blender.org/T52251): Knife cut
    displaces surface
    ([rBa8c7f132](https://projects.blender.org/blender/blender/commit/a8c7f1329b)).
  - Fix [\#52374](http://developer.blender.org/T52374): Changes of rigid
    body related settings during simulation will break the simulation
    ([rBd84f5595](https://projects.blender.org/blender/blender/commit/d84f559555)).
  - Fix unreported: Rigidbody: Fix regression introduced in ee3fadd
    ([rBe91f9f66](https://projects.blender.org/blender/blender/commit/e91f9f664d)).
  - Fix [\#52522](http://developer.blender.org/T52522): VSE renders with
    alpha transparent PNG image incorrectly
    ([rBa8bd08ff](https://projects.blender.org/blender/blender/commit/a8bd08ffdd)).
  - Fix [\#52663](http://developer.blender.org/T52663): Remap used
    invalid local-view data
    ([rBb895c733](https://projects.blender.org/blender/blender/commit/b895c7337e)).
  - Fix [\#52678](http://developer.blender.org/T52678): Crash editing
    gpencil w/ frame-lock
    ([rB3aaf9087](https://projects.blender.org/blender/blender/commit/3aaf908719)).
  - Fix unreported: UI: fix memory leak when copy-to-selected failed
    ([rB9da09853](https://projects.blender.org/blender/blender/commit/9da098536d)).
  - Fix [\#52639](http://developer.blender.org/T52639): Weight paint
    smooth tool crash
    ([rB300abf24](https://projects.blender.org/blender/blender/commit/300abf241e)).
  - Fix unreported: Fix transform snap code using 'allocated' flags to
    get verts/edges/etc. arrays again from DM
    ([rB9cc7e32f](https://projects.blender.org/blender/blender/commit/9cc7e32f39)).
  - Fix [\#52149](http://developer.blender.org/T52149): LoopTriArray
    computation was not correctly protected against concurrency
    ([rB9ae35faf](https://projects.blender.org/blender/blender/commit/9ae35fafb6)).
  - Fix [\#52650](http://developer.blender.org/T52650): Grease pencil
    selection its not automatically updating in Clip Editor
    ([rB87cc8550](https://projects.blender.org/blender/blender/commit/87cc8550e2)).
  - Fix unreported: Cycles: Safer fix for infinite recursion
    ([rBfbb4be06](https://projects.blender.org/blender/blender/commit/fbb4be061c)).
  - Fix [\#52531](http://developer.blender.org/T52531): Blender 2D
    stabilisation node issue when autoscale is selected
    ([rB82466852](https://projects.blender.org/blender/blender/commit/82466852fe)).
  - Fix unreported: PyAPI: Fix mathutils freeze allowing owned data
    ([rB1a7dda04](https://projects.blender.org/blender/blender/commit/1a7dda046b)).
  - Fix [\#52701](http://developer.blender.org/T52701): Mesh shortest
    path fails at boundaries
    ([rB904831e6](https://projects.blender.org/blender/blender/commit/904831e62e)).
  - Fix [\#52696](http://developer.blender.org/T52696): Sculpt - Brush
    spacing pressure artifacts
    ([rB9a9e9b1c](https://projects.blender.org/blender/blender/commit/9a9e9b1c4d)).
