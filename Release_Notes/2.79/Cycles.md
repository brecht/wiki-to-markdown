# Blender 2.79: Cycles

## Denoising

Denoising filters the resulting image using information (known as
feature passes) gathered during rendering to get rid of noise while
preserving visual detail as well as possible.
([43b374e](https://projects.blender.org/blender/blender/commit/43b374e))

To use the option, enable it in the render layers tab of the properties
editor. On rendering, it will denoise tile by tile once all the
surrounding tiles are finished rendering. The default settings fit a
wide range of scenes, but the user can tweak individual settings to
control the trade-off between a noise-free image, image details, and
calculation time.

  - *Radius:* Size of the image area that's used to denoise a pixel.
    Higher values are smoother, but might lose detail and are slower.
  - *Strength:* Controls how different the area around a neighbor pixel
    can look compared to the center pixel before it's no longer used for
    denoising. Lower values preserve more detail, but aren't as smooth.
  - *Feature Strength:* Controls removal of noisy and redundant image
    feature passes before the actual denoising. This is required in some
    cases like DoF or MoBlur to avoid splotchy results, but might cause
    fine texture/geometrical detail to be lost. Lower values preserve
    more detail, but aren't as smooth.
  - *Relative Filter:* When removing features that don't carry
    information, decide which to keep based on the total amount of
    information in the features. This can help to reduce artifacts, but
    might cause detail loss around edges.

<center>

|                                                                                         |                                                                                                                                                                                                                    |
| --------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| ![Before denoising](../../images/Cycles2.79_denoise_room_before.jpg "Before denoising") | ![After denoising ([render by The Pixelary](http://blog.thepixelary.com/post/160451378592/denoising-in-cycles-tested))](../../images/Cycles2.79_denoise_room_after.jpg "After denoising (render by The Pixelary)") |

</center>

#### Notes and issues

The denoiser will change in the future and some features are not
implemented yet. If denoising fails to produce good results, more
samples or clamping will often resolve the issue.

  - Setting the radius too high is generally not advisable. It increases
    denoising time a lot and, while the result might be smoother, it is
    not more accurate since there isn't any additional info coming out
    of the renderer. Beyond a radius of \~15, the additional rendering
    time is probably better spent on increasing the amount of samples.
  - Denoising can not be used for baking yet. This might be added in the
    future, but will not be part of 2.79.
  - For animation denoising can be used, however it still requires high
    sample counts for good results. With low sample counts low frequency
    (blurry) noise can be visible in animation, even if it is not
    immediately apparent in still images. Cross-frame denoising to
    produce flicker-free and smoother results is planned for a future
    release.
  - When using GPU rendering, the denoising process may use a
    significant amount of vRAM. If the GPU runs out of memory but
    renders fine without denoising, try reducing the tile size. Also,
    using left-to-right or right-to-left as the tile order can sometimes
    help because fewer tiles are kept in memory that way.

## OpenCL

![OpenCL render time comparison with previous
release](../../images/Blender_278_opencl.png
"OpenCL render time comparison with previous release")

  - The amount of work that the compute device does at once is now
    determined automatically to fill available memory, independent of
    tile size. This reduces render times by up to 50% in certain cases,
    without the need to adjust the tile size for every scene or system
    configuration.
    ([230c00d](https://projects.blender.org/blender/blender/commit/230c00d))
  - Tiles can now be seen updating while rendering.
    ([230c00d](https://projects.blender.org/blender/blender/commit/230c00d))
  - Fixed shading artifacts sometimes seen especially at low samples.
    ([223f458](https://projects.blender.org/blender/blender/commit/223f458))
  - Fixes for most common cases of hanging and crashing.
    ([365a423](https://projects.blender.org/blender/blender/commit/365a423))
  - Added support of SSS and Volume rendering
    ([57e2662](https://projects.blender.org/blender/blender/commit/57e2662))
  - Optimized transparent shadows
    ([e8b5a5b](https://projects.blender.org/blender/blender/commit/e8b5a5b))
  - Due to various issues with drivers for AMD GCN 1.0 architecture,
    those cards are no longer supported for Cycles rendering
    ([1f0998b](https://projects.blender.org/blender/blender/commit/1f0998b))

  

## GPU Rendering test

Below are the timings for the official [Benchmark
Files](https://code.blender.org/2016/02/new-cycles-benchmark/), using
reference systems in Blender Institute. AMD and Nvidia GPUs are now
giving comparable performance.

![OpenCL and CUDA render time for 2.79
release](../../images/GPU279-1.png
"OpenCL and CUDA render time for 2.79 release")

![OpenCL and CUDA render time for 2.79
release](../../images/GPU279-nrs.png
"OpenCL and CUDA render time for 2.79 release")

## Shadow Catcher

Shadow catcher support was added to Cycles
([d14e396](https://projects.blender.org/blender/blender/commit/d14e396)).
This feature makes it really easy to combine CGI elements into a
real-life footage. Any object in the scene can be a shadow catcher and
this is controlled by Shadow Catcher option in the object settings as
shown below.

This option will make object to only receive shadows in a way that it
could be alpha-overed onto another image.

It is to be noted that shadow catcher objects will interact with other
CG objects via indirect light interaction. For example, shadow catcher
will be reflected in the glossy surfaces.

<center>

|                                                                                                         |                                                                                                      |
| ------------------------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------- |
| ![Shadow catcher example](../../images/Blender_279_shadow_catcher_example.png "Shadow catcher example") | ![Shadow catcher option](../../images/Blender_278_shadow_catcher_option.png "Shadow catcher option") |

</center>

## Principled BSDF

The principled BSDF is new surface shader node, combining multiple
layers into a single easy to use node. It is based on the [Disney
principled
model](https://disney-animation.s3.amazonaws.com/library/s2012_pbs_disney_brdf_notes_v2.pdf)
also known as the "PBR" shader, making it compatible with other software
such as Substance Painter, Unreal Engine and Pixar's Renderman. Image
textures painted or baked from software like Substance Painter may be
directly linked to the corresponding parameters in this shader.

![Texture compatibility with Substance Painter (render by [Julian
Perez](https://julperado.wordpress.com/2017/06/26/principled-shader-o-disney-shader-en-blender-cycles/))](../../images/Cycles2.79_principled_bsdf.jpg
"Texture compatibility with Substance Painter (render by Julian Perez)")

This shader includes multiple layers to create a wide variety of
materials. The base layer is a user controlled mix between diffuse,
metal, subsurface scattering and transmission. On top of that there is a
specular layer, sheen layer and clearcoat layer. The following
parameters controls these:

  - *Base Color:* diffuse or metal surface color.
  - *Subsurface:* mix between diffuse and subsurface scattering.
  - *Subsurface Radius:* average scattering distance for R, G and B
    channels.
  - *Subsurface Color:* subsurface scattering base color.
  - *Metallic:* mix between dielectric (diffuse and specular) and
    metallic (fully specular with complex fresnel).
  - *Specular:* amount of specular reflection.
  - *Specular Tint:* mix between white (typical for dielectric surfaces)
    and using base color for specular reflection.
  - *Roughness:* specular reflection rough from perfect sharp reflection
    at zero to almost diffuse at one.
  - *Anisotropic:* amount of anisotropy for specular reflection.
  - *Anisotropic Rotation:* rotate the anisotropic reflection.
  - *Sheen:* amount of soft velvet like reflection near edges, for
    simulating materials such as cloth.
  - *Sheen Tint:* mix between white and using base color for sheen
    reflection.
  - *Clearcoat:* extra white specular layer on top of others.
  - *Clearcoat Roughness:* roughness of clear coat specular, from
    perfect sharp reflection at zero to almost diffuse at one.
  - *IOR:* index of refraction for transmission.
  - *Transmission:* mix between fully opaque surface at zero and fully
    glass like transmission at one.
  - *Normal*, *Clearcoat Normal* and *Tangent:* control normals and
    tangents for the layers.

![Principled BSDF
Parameters](../../images/Cycles2.79_principled_bsdf_parameters.jpg
"Principled BSDF Parameters")

Development of this shader was supported by Adidas.

## Filmic Color Management

A new Filmic view transform has been added, for more photorealistic
results and better handling of high dynamic range. Contrast can be
adjusted by changing the look for the Filmic view transform.
Additionally a new False Color view transform shows a heat map of image
intensities, to visualize the dynamic range.

![Comparison between default sRGB and Filmic ([demonstration by The
Pixelary](http://blog.thepixelary.com/post/160247878572/filmic-colors-in-blender-and-light-linearity))](../../images/Cycles2.79_filmic_comparison.jpg
"Comparison between default sRGB and Filmic (demonstration by The Pixelary)")

Lighting and materials may need to be tweaked if they were originally
created for the default sRGB view transform. In general a view transform
should be chosen at the start of a project and used for viewing while
working on the assets.

For more details see the original [Filmic Blender
Configuration](https://sobotka.github.io/filmic-blender/), which can
still be used as an alternative to the builtin Blender configuration.

## Optimizations

  - 10-20% speedups for CPUs that support AVX2
    ([42aeb60](https://projects.blender.org/blender/blender/commit/42aeb60),
    [e588106d](https://projects.blender.org/blender/blender/commit/e588106d),
    [22cdf4410](https://projects.blender.org/blender/blender/commit/22cdf4410))
  - Light sampling optimizations
    ([b459d9f46c](https://projects.blender.org/blender/blender/commit/b459d9f46c),
    [1558f5b](https://projects.blender.org/blender/blender/commit/1558f5b),
    [aae2cea](https://projects.blender.org/blender/blender/commit/aae2cea))
  - Multithreaded shader nodes compilation
    ([0de69e56b4](https://projects.blender.org/blender/blender/commit/0de69e56b4))

## Other Features

![Water caustics with a textured
spotlight](../../images/Cycles2_79_lamp_coordinates_caustics.jpeg
"Water caustics with a textured spotlight")

  - Panorama camera now uses near clipping.
    ([7fec7eee20](https://projects.blender.org/blender/blender/commit/7fec7eee2070))
  - Light sampling threshold
    ([26bf230](https://projects.blender.org/blender/blender/commit/26bf230))
  - Brick texture smoothing
    ([b2974d7](https://projects.blender.org/blender/blender/commit/b2974d7))
  - Texture coordinates for point, spot and area lamps
    ([1272ee4](https://projects.blender.org/blender/blender/commit/1272ee4))
  - Camera Distance culling. When used individually, this will cull any
    objects further from the camera than a given distance. When used in
    combination with camera frustum culling, this can be used to avoid
    culling nearby objects that are outside the camera frustum but still
    visible in reflections.
    ([e8641d4474](https://projects.blender.org/blender/blender/commit/e8641d44740e))
  - Individual selection of GPU devices to use when rendering with
    multiple GPUs. Also in Blender 2.78c.
    ([dd92123](https://projects.blender.org/blender/blender/commit/dd92123))
