# Blender 2.79a: Bug Fixes

Changes from revision
[rBe8e40b17](https://projects.blender.org/blender/blender/commit/e8e40b171b)
to
[rB4c1bed0a](https://projects.blender.org/blender/blender/commit/4c1bed0a12),
inclusive.

Total fixed bugs: 202 (117 from tracker, 85 reported/found by other
ways).

**Note:** Nearly all commits from 2.79 are listed here (over 75% of
total), only really technical/non-user affecting ones have been skipped.

## Objects / Animation / GP

### Animation

  - Fix [\#52932](http://developer.blender.org/T52932): Driver with
    target of custom property from scene fails to update
    ([rBad834085](https://projects.blender.org/blender/blender/commit/ad834085b7)).
  - Fix [\#52835](http://developer.blender.org/T52835): When driven IK
    influence change, ik animation have 1 frame delay
    ([rB89de073e](https://projects.blender.org/blender/blender/commit/89de073ea5)).

<!-- end list -->

  - Fix unreported: Fix missing ID remapping in Action editor callback
    ([rB9f6d5d67](https://projects.blender.org/blender/blender/commit/9f6d5d679e)).
  - Fix unreported: [\#50354](http://developer.blender.org/T50354):
    Action length calculation added unnecessary padding if some F-Curves
    only contained a single key (on the last real frame of the action)
    ([rBf887b8b2](https://projects.blender.org/blender/blender/commit/f887b8b230)).
  - Fix unreported: Fix: Undo pushes were missing for Add/Remove Driver
    Variable buttons, and Remove Driver button
    ([rB08e16e0b](https://projects.blender.org/blender/blender/commit/08e16e0bd1)).

### Grease Pencil

  - Fix unreported: Fix: When transforming GP strokes in "Local" mode,
    the strokes would get obscured by the transform constraint lines
    ([rB99e4c819](https://projects.blender.org/blender/blender/commit/99e4c819f7)).

### Objects

  - Fix [\#52729](http://developer.blender.org/T52729): Decimals not
    showing over 100m or 100 feet
    ([rB5384b2a6](https://projects.blender.org/blender/blender/commit/5384b2a6e2)).
  - Fix [\#52140](http://developer.blender.org/T52140): Align objects
    centers using origin for text
    ([rB3a0f199a](https://projects.blender.org/blender/blender/commit/3a0f199aa7)).

### Dependency Graph

  - Fix [\#52432](http://developer.blender.org/T52432): Blender crashes
    while using Ghost (new depsgraph)
    ([rB2213153c](https://projects.blender.org/blender/blender/commit/2213153c27)).
  - Fix [\#52741](http://developer.blender.org/T52741): Follow track
    with depth object crashes Blender with new depsgraph
    ([rBd5dbe0c5](https://projects.blender.org/blender/blender/commit/d5dbe0c566)).
  - Fix [\#53547](http://developer.blender.org/T53547): Metaballs as
    dupli objects are not updated with the new Depsgraph
    ([rB8cdda3d2](https://projects.blender.org/blender/blender/commit/8cdda3d2ad)).

<!-- end list -->

  - Fix unreported: Transform: Enable recursion dependency check for new
    depsgraph
    ([rBef04aa4a](https://projects.blender.org/blender/blender/commit/ef04aa4a43)).
  - Fix unreported: Depsgraph: Fix relations for metaballs
    ([rB7103c6ef](https://projects.blender.org/blender/blender/commit/7103c6ef3b)).

## Data / Geometry

### Armatures

  - Fix [\#53300](http://developer.blender.org/T53300): Bone Extrude via
    Ctrl + Click is not done from active bone tail
    ([rB96919606](https://projects.blender.org/blender/blender/commit/969196069a)).
  - Fix [\#53054](http://developer.blender.org/T53054): Parentless bone
    + IK crashes
    ([rBc63e0886](https://projects.blender.org/blender/blender/commit/c63e08863d)).

### Curve/Text Editing

  - Fix [\#52860](http://developer.blender.org/T52860): 3D Text crashes
    w/ Ctrl Backspace
    ([rBb969f7eb](https://projects.blender.org/blender/blender/commit/b969f7eb55)).
  - Fix [\#53410](http://developer.blender.org/T53410): 3D Text always
    recalculated
    ([rB480b0b5d](https://projects.blender.org/blender/blender/commit/480b0b5dfc)).
  - Fix [\#53586](http://developer.blender.org/T53586): Surfaces
    collapse when joined
    ([rB1611177a](https://projects.blender.org/blender/blender/commit/1611177ac9)).

<!-- end list -->

  - Fix unreported: Curves: Fix wrong bitset being checked against
    CYCLIC bit flag
    ([rB2edc2b49](https://projects.blender.org/blender/blender/commit/2edc2b4912)).

### Mesh Editing

  - Fix [\#53145](http://developer.blender.org/T53145): bevel tool fails
    when used a second time
    ([rB36f324fc](https://projects.blender.org/blender/blender/commit/36f324fc55)).
  - Fix [\#53145](http://developer.blender.org/T53145): bevel tool does
    not start with amount at zero
    ([rBf1a4e130](https://projects.blender.org/blender/blender/commit/f1a4e130b3)).
  - Fix [\#52723](http://developer.blender.org/T52723): Reset UV layers
    failed
    ([rB06410697](https://projects.blender.org/blender/blender/commit/0641069778)).
  - Fix [\#52748](http://developer.blender.org/T52748): Select shortest
    face path fails
    ([rB60bfa969](https://projects.blender.org/blender/blender/commit/60bfa969e2)).
  - Fix [\#52384](http://developer.blender.org/T52384): Bridge pair
    result depends on other loops
    ([rB495aa77b](https://projects.blender.org/blender/blender/commit/495aa77b53)).
  - Fix [\#53131](http://developer.blender.org/T53131): Incorrect
    vert-edge angle calculation
    ([rB60e8d86f](https://projects.blender.org/blender/blender/commit/60e8d86fb1)).
  - Fix [\#53441](http://developer.blender.org/T53441): Inset doesn't
    start at zero
    ([rB19b27d84](https://projects.blender.org/blender/blender/commit/19b27d84ab)).
  - Fix [\#53529](http://developer.blender.org/T53529): Rip crashes w/
    wire edge
    ([rB52a5daa4](https://projects.blender.org/blender/blender/commit/52a5daa404)).
  - Fix [\#53343](http://developer.blender.org/T53343): Custom Normal
    Data Transfer Crashes when some vertexes have no faces
    ([rBff8c9c59](https://projects.blender.org/blender/blender/commit/ff8c9c5931)).
  - Fix [\#53420](http://developer.blender.org/T53420): Vertex Groups:
    The "-" button gets a hidden function
    ([rB50ca70b2](https://projects.blender.org/blender/blender/commit/50ca70b275)).
  - Fix [\#52733](http://developer.blender.org/T52733): Percent mode for
    Bevel sometimes had nans
    ([rB48079e1f](https://projects.blender.org/blender/blender/commit/48079e1f11)).
  - Fix [\#52871](http://developer.blender.org/T52871):
    \`BLI\_polyfill\_beautify\_quad\_rotate\_calc\_ex\` was mistakenly
    considering the state as degenerated
    ([rB9298d99b](https://projects.blender.org/blender/blender/commit/9298d99b77)).
  - Fix [\#52871](http://developer.blender.org/T52871): beauty fill
    error
    ([rB9d501d23](https://projects.blender.org/blender/blender/commit/9d501d23cb)).
  - Fix [\#53143](http://developer.blender.org/T53143): Knife Crash
    after Grid Fill
    ([rB4f247ed0](https://projects.blender.org/blender/blender/commit/4f247ed07a)).
  - Fix [\#53713](http://developer.blender.org/T53713): User remap
    failed w/ texface images
    ([rB26ffade5](https://projects.blender.org/blender/blender/commit/26ffade5c1)).
  - Fix [\#52818](http://developer.blender.org/T52818): Tangent space
    calculation is really slow for high-density mesh with degenerated
    topology
    ([rB9a5320ae](https://projects.blender.org/blender/blender/commit/9a5320aea3)).
  - Fix [\#53678](http://developer.blender.org/T53678): Smart Project UV
    margin ignores units
    ([rB42e207b5](https://projects.blender.org/blender/blender/commit/42e207b599)).

<!-- end list -->

  - Fix unreported: Fix for inset when accessed from spacebar search
    ([rBdb3e3f9c](https://projects.blender.org/blender/blender/commit/db3e3f9c24)).
  - Fix unreported: Edit Mesh: click extrude, ensure inverse matrix
    ([rB3e3a27d0](https://projects.blender.org/blender/blender/commit/3e3a27d089)).
  - Fix unreported: Polyfill Beautify: option to rotate out of
    degenerate state
    ([rB0834eefa](https://projects.blender.org/blender/blender/commit/0834eefae0)).
  - Fix unreported: BMesh: use less involved check for edge rotation
    ([rB58ab62ed](https://projects.blender.org/blender/blender/commit/58ab62ed6d)).
  - Fix unreported: Avoid bias when calculating quad split direction
    ([rB569d2df6](https://projects.blender.org/blender/blender/commit/569d2df634)).
  - Fix unreported: Beauty fill was skipping small faces
    ([rB1b613053](https://projects.blender.org/blender/blender/commit/1b6130533f)).
  - Fix unreported: Polyfill Beautify: half-edge optimization
    ([rB4a457d4f](https://projects.blender.org/blender/blender/commit/4a457d4f1e)).
  - Fix unreported: Use BLI\_heap\_reinsert for decimate and beautify
    ([rBeaeb0a00](https://projects.blender.org/blender/blender/commit/eaeb0a002e)).
  - Fix unreported: BMesh: move bridge tools stepping logic into macro
    ([rB02b20678](https://projects.blender.org/blender/blender/commit/02b206780e)).
  - Fix unreported: Subsurf: Avoid global lock for loops and orig index
    layers
    ([rB83b06030](https://projects.blender.org/blender/blender/commit/83b0603061)).
  - Fix unreported: Subsurf: Avoid possible use of partially initialized
    edge hash
    ([rB72151f3e](https://projects.blender.org/blender/blender/commit/72151f3e36)).
  - Fix unreported: Fix scalability issue in threaded code of Mesh
    normals computation
    ([rB71e0894e](https://projects.blender.org/blender/blender/commit/71e0894e0d)).

### Modifiers

  - Fix [\#51074](http://developer.blender.org/T51074): Boolean modifier
    inverts operation
    ([rBd07011da](https://projects.blender.org/blender/blender/commit/d07011da71)).
  - Fix [\#52291](http://developer.blender.org/T52291): Boolean fails w/
    co-linear edged ngons
    ([rB78cc3c82](https://projects.blender.org/blender/blender/commit/78cc3c828f)).
  - Fix [\#52763](http://developer.blender.org/T52763): Boolean problem
    with vertex group
    ([rB00d80975](https://projects.blender.org/blender/blender/commit/00d8097510)).
  - Fix [\#52823](http://developer.blender.org/T52823): New Depsgraph -
    Shrinkwrap crashes blender
    ([rB754630ce](https://projects.blender.org/blender/blender/commit/754630cee4)).
  - Fix [\#53007](http://developer.blender.org/T53007): OpenSubdiv +
    transparency = artefact/crashes
    ([rBba40d8f3](https://projects.blender.org/blender/blender/commit/ba40d8f331)).
  - Fix [\#53398](http://developer.blender.org/T53398): Surface deform
    modifier says that convex polygons are concave for big faces
    ([rB4c46f693](https://projects.blender.org/blender/blender/commit/4c46f69376)).

<!-- end list -->

  - Fix unreported: Fix (irc-reported by @sergey) invalid precision
    value in a float RNA property
    ([rB8e43a9e9](https://projects.blender.org/blender/blender/commit/8e43a9e9cc)).

### Material / Texture

  - Fix [\#53116](http://developer.blender.org/T53116): default texture
    coordinates for volume materials are blank
    ([rB576899b9](https://projects.blender.org/blender/blender/commit/576899b90a)).
  - Fix [\#52953](http://developer.blender.org/T52953): Crash removing
    material
    ([rB243b961c](https://projects.blender.org/blender/blender/commit/243b961c29)).
  - Fix [\#53509](http://developer.blender.org/T53509): Datablock ID
    Properties attached to bpy.types.Material are not loaded
    ([rB0c365472](https://projects.blender.org/blender/blender/commit/0c365472b6)).

<!-- end list -->

  - Fix unreported: Fix logic for pinning textures users from context
    ([rB8f9d9ba1](https://projects.blender.org/blender/blender/commit/8f9d9ba14e)).

## Physics / Simulations / Sculpt / Paint

### Particles

  - Fix [\#53552](http://developer.blender.org/T53552): Unneeded
    particle cache reset on frame change
    ([rB6f19787e](https://projects.blender.org/blender/blender/commit/6f19787e52)).
  - Fix [\#52732](http://developer.blender.org/T52732): Particle system
    volume grid particles out of volume
    ([rB17c00d22](https://projects.blender.org/blender/blender/commit/17c00d222f)).
  - Fix [\#53513](http://developer.blender.org/T53513): Particle size
    showing in multiple places
    ([rBc70a4502](https://projects.blender.org/blender/blender/commit/c70a45027d)).
  - Fix [\#53832](http://developer.blender.org/T53832): Particle weight
    paint crash
    ([rB5b3538e0](https://projects.blender.org/blender/blender/commit/5b3538e02a)).
  - Fix [\#53823](http://developer.blender.org/T53823): Particle weight
    brush crash
    ([rBb6481cbb](https://projects.blender.org/blender/blender/commit/b6481cbbe5)).

<!-- end list -->

  - Fix unreported: Fix missing update for particles w/ fluids
    ([rB84361a07](https://projects.blender.org/blender/blender/commit/84361a0709)).

### Physics / Hair / Simulations

  - Fix [\#53650](http://developer.blender.org/T53650): remove hard
    limits on force field size and max distance
    ([rB2a9abc0f](https://projects.blender.org/blender/blender/commit/2a9abc0f5e)).

<!-- end list -->

  - Fix unreported: Fix error copying smoke modifier uv layer
    ([rB9f032c38](https://projects.blender.org/blender/blender/commit/9f032c3867)).
  - Fix unreported: Depsgraph: Don't make non-dynamic hair dependent on
    time
    ([rB76032b13](https://projects.blender.org/blender/blender/commit/76032b133c)).

### Sculpting / Painting

  - Fix [\#53577](http://developer.blender.org/T53577): Rake
    sculpt/paint wrong on first step
    ([rB413817a3](https://projects.blender.org/blender/blender/commit/413817a3d2)).
  - Fix [\#52537](http://developer.blender.org/T52537): Dyntopo "detail
    flood fill" doesn't work in some cases
    ([rB1b8e8326](https://projects.blender.org/blender/blender/commit/1b8e8326b4)).
  - Fix [\#53593](http://developer.blender.org/T53593): sculpt brush
    rake spacing bug after recent bugfix
    ([rB4c1bed0a](https://projects.blender.org/blender/blender/commit/4c1bed0a12)).

<!-- end list -->

  - Fix unreported: Fix brush reset (missing notifier)
    ([rB98dc9072](https://projects.blender.org/blender/blender/commit/98dc9072e5)).
  - Fix unreported: Fix sculpt secondary color missing some brushes
    ([rB6058b651](https://projects.blender.org/blender/blender/commit/6058b651c4)).

## Image / Video / Render

### Image / UV Editing

  - Fix [\#53092](http://developer.blender.org/T53092): errors reading
    EXR files with different data/display window
    ([rBf901bf6f](https://projects.blender.org/blender/blender/commit/f901bf6f47)).
  - Fix [\#52739](http://developer.blender.org/T52739): Crash loading
    corrupted video files
    ([rB9c39f021](https://projects.blender.org/blender/blender/commit/9c39f021ad)).
  - Fix [\#52811](http://developer.blender.org/T52811): At any framerate
    selected, video exported with 1000fps
    ([rB3163d0e2](https://projects.blender.org/blender/blender/commit/3163d0e205)).
  - Fix [\#52920](http://developer.blender.org/T52920): Saving Tiff
    Files type Blender crashes
    ([rBd305c101](https://projects.blender.org/blender/blender/commit/d305c10104)).
  - Fix [\#53499](http://developer.blender.org/T53499): Cannot load DPX
    files
    ([rBb8bdca8c](https://projects.blender.org/blender/blender/commit/b8bdca8c0a)).

<!-- end list -->

  - Fix unreported: Fix writing Iris images w/ invalid header
    ([rBacae901a](https://projects.blender.org/blender/blender/commit/acae901a10)).

### Masking

  - Fix [\#52840](http://developer.blender.org/T52840): New Depsgraph -
    Mask editor not working correctly
    ([rB69062cdd](https://projects.blender.org/blender/blender/commit/69062cdd8d)).
  - Fix [\#52749](http://developer.blender.org/T52749): New Depsgraph -
    Render View Mask is not initialized correctly
    ([rB53818251](https://projects.blender.org/blender/blender/commit/538182511a)).
  - Fix [\#53419](http://developer.blender.org/T53419): Masking "Add"
    menu is not present in Image editor, but shortcut is
    ([rBc8f95c78](https://projects.blender.org/blender/blender/commit/c8f95c7829)).

### Motion Tracking

  - Fix [\#52851](http://developer.blender.org/T52851): Per-frame
    traking is broken when sequence doesn't start at frame 1
    ([rBf0743b56](https://projects.blender.org/blender/blender/commit/f0743b56ec)).
  - Fix [\#53612](http://developer.blender.org/T53612): Blender crashes
    on CleanTracks with 'DELETE\_SEGMENTS' and a disabled marker
    ([rBa1d05ac2](https://projects.blender.org/blender/blender/commit/a1d05ac2a1)).

<!-- end list -->

  - Fix unreported: Tracking: Fix crash when tracking failed
    ([rB350f5c68](https://projects.blender.org/blender/blender/commit/350f5c6894)).
  - Fix unreported: Tracking: Create mesh from selected tracks only
    ([rB333ef6e6](https://projects.blender.org/blender/blender/commit/333ef6e6f7)).

### Nodes / Compositor

  - Fix [\#53360](http://developer.blender.org/T53360): crash with GLSL
    bump mapping and missing group output node
    ([rBbb897596](https://projects.blender.org/blender/blender/commit/bb89759624)).
  - Fix [\#52299](http://developer.blender.org/T52299): X resolution of
    4 causes nodes to collapse
    ([rBcdc35e63](https://projects.blender.org/blender/blender/commit/cdc35e63bd)).
  - Fix [\#53371](http://developer.blender.org/T53371): Keying Node
    fails with values above 1
    ([rB1c7657be](https://projects.blender.org/blender/blender/commit/1c7657befb)).
  - Fix [\#52113](http://developer.blender.org/T52113): Compositor
    doesnt mix unrendered render layers well
    ([rB31823366](https://projects.blender.org/blender/blender/commit/3182336666)).
  - Fix [\#53263](http://developer.blender.org/T53263): Proposed Blender
    crashes when rendering with Stabilizer 2D node without movie
    selected
    ([rBce35151f](https://projects.blender.org/blender/blender/commit/ce35151f38)).
  - Fix [\#53696](http://developer.blender.org/T53696): Compositor HSV
    limits changed
    ([rB528e00da](https://projects.blender.org/blender/blender/commit/528e00dae0)).
  - Fix [\#52927](http://developer.blender.org/T52927): Compositor wrong
    scale when scale size input is connected to complex node
    ([rB259e9ad0](https://projects.blender.org/blender/blender/commit/259e9ad00d)).
  - Fix [\#47212](http://developer.blender.org/T47212): Luminance Key
    not working with HDR and out-of-gamut ranges
    ([rB13973a5b](https://projects.blender.org/blender/blender/commit/13973a5bbf)).

<!-- end list -->

  - Fix unreported: Fix compositor node links getting lost on file load
    for custom render passes
    ([rB6bd1189e](https://projects.blender.org/blender/blender/commit/6bd1189e5c)).
  - Fix unreported: Node selection: Stop operator when mouse selection
    selected a node
    ([rBbf58ec92](https://projects.blender.org/blender/blender/commit/bf58ec9265)).
  - Fix unreported: Compositor: Ensured 16 byte alignment for variables
    accessed by SSE instructions
    ([rB02bac54f](https://projects.blender.org/blender/blender/commit/02bac54fa9)).
  - Fix unreported: (Nodes) Display image name if any in the Image and
    Texture Image node title
    ([rB010cf35e](https://projects.blender.org/blender/blender/commit/010cf35e7e)).
  - Fix unreported: Fix old files with changed node socket type not
    loading correctly
    ([rB79563d2a](https://projects.blender.org/blender/blender/commit/79563d2a33)).

### Render

  - Fix [\#52679](http://developer.blender.org/T52679): Hole in bake
    normal
    ([rB759af7f1](https://projects.blender.org/blender/blender/commit/759af7f1ee)).
  - Fix [\#53185](http://developer.blender.org/T53185): After rendering
    an animation (Ctrl-F12), pressing F12 no longer renders single
    frames only
    ([rB6cfc5edb](https://projects.blender.org/blender/blender/commit/6cfc5edb86)).
  - Fix [\#53810](http://developer.blender.org/T53810): Crash removing a
    scene used in render
    ([rB5ddcad43](https://projects.blender.org/blender/blender/commit/5ddcad4377)).

<!-- end list -->

  - Fix unreported: Fix incorrect color management when saving JPG
    previews for EXR
    ([rB1dbdcfe9](https://projects.blender.org/blender/blender/commit/1dbdcfe9a9)).
  - Fix unreported: Fix leak when rendering OpenGL animations
    ([rBe4dce3b3](https://projects.blender.org/blender/blender/commit/e4dce3b3d7)).

### Render: Cycles

  - Fix [\#53129](http://developer.blender.org/T53129): Cycles missing
    update when changing image auto refresh
    ([rBa6e55581](https://projects.blender.org/blender/blender/commit/a6e5558194)).
  - Fix [\#53217](http://developer.blender.org/T53217): GLSL principled
    BSDF black with zero clearcoat roughness
    ([rBd564beda](https://projects.blender.org/blender/blender/commit/d564beda44)).
  - Fix [\#52368](http://developer.blender.org/T52368): Cycles OSL
    trace() failing on Windows 32 bit
    ([rB2bc667ec](https://projects.blender.org/blender/blender/commit/2bc667ec79)).
  - Fix [\#53348](http://developer.blender.org/T53348): Cycles
    difference between gradient texture on CPU and GPU
    ([rB198fd0be](https://projects.blender.org/blender/blender/commit/198fd0be43)).
  - Fix [\#53171](http://developer.blender.org/T53171): lamp specials
    strength tweak fails with renamed emission nodes
    ([rB5b2d5f90](https://projects.blender.org/blender/blender/commit/5b2d5f9077)).
  - Fix [\#52573](http://developer.blender.org/T52573): Cycles baking
    artifacts
    ([rBb0c55d5c](https://projects.blender.org/blender/blender/commit/b0c55d5c94)).
  - Fix [\#51416](http://developer.blender.org/T51416): Blender Crashes
    while moving Sliders
    ([rBaafe528c](https://projects.blender.org/blender/blender/commit/aafe528c7d)).
  - Fix [\#53048](http://developer.blender.org/T53048): OSL Volume is
    broken in Blender 2.79
    ([rBad738542](https://projects.blender.org/blender/blender/commit/ad7385422e)).
  - Fix [\#53559](http://developer.blender.org/T53559): Auto texture
    space for text and font is wrong in Cycles
    ([rBe7aad8fd](https://projects.blender.org/blender/blender/commit/e7aad8fd0e)).
  - Fix [\#53600](http://developer.blender.org/T53600): Cycles shader
    mixing issue with principled BSDF and zero weights
    ([rB836a1ccf](https://projects.blender.org/blender/blender/commit/836a1ccf72)).
  - Fix [\#52801](http://developer.blender.org/T52801): reload scripts
    causes Cycles viewport render crash
    ([rBf1ee24a2](https://projects.blender.org/blender/blender/commit/f1ee24a284)).
  - Fix [\#53017](http://developer.blender.org/T53017): Cycles not
    detecting AMD GPU when there is an NVidia GPU too
    ([rB1f50f067](https://projects.blender.org/blender/blender/commit/1f50f0676a)).
  - Fix [\#53755](http://developer.blender.org/T53755): Cycles OpenCL
    lamp shaders have incorrect normal
    ([rB2ca933f4](https://projects.blender.org/blender/blender/commit/2ca933f457)).
  - Fix [\#53692](http://developer.blender.org/T53692): OpenCL multi GPU
    rendering not using all GPUs
    ([rB30a0459f](https://projects.blender.org/blender/blender/commit/30a0459f2c)).
  - Fix [\#53567](http://developer.blender.org/T53567): Negative pixel
    values causing artifacts with denoising
    ([rB824c0392](https://projects.blender.org/blender/blender/commit/824c039230)).
  - Fix [\#53012](http://developer.blender.org/T53012): Shadow catcher
    creates artifacts on contact area
    ([rBc3237cdc](https://projects.blender.org/blender/blender/commit/c3237cdc13)).

<!-- end list -->

  - Fix unreported: OpenVDB: Fix compilation error against OpenVDB 4
    ([rB0cdfe887](https://projects.blender.org/blender/blender/commit/0cdfe887b4)).
  - Fix unreported: Cycles: Fix possible race condition when generating
    Beckmann table
    ([rBb5c629e6](https://projects.blender.org/blender/blender/commit/b5c629e604)).
  - Fix unreported: Fix Cycles bug in RR termination, probability should
    never be \> 1.0
    ([rBdf1af9b3](https://projects.blender.org/blender/blender/commit/df1af9b349)).
  - Fix unreported: CMake: support CUDA 9 toolkit, and automatically
    disable sm\_2x binaries
    ([rBcd9c68f0](https://projects.blender.org/blender/blender/commit/cd9c68f0c5)).
  - Fix unreported: Fix part of
    [\#53038](http://developer.blender.org/T53038): principled BSDF
    clearcoat weight has no effect with 0 roughness
    ([rB440d647c](https://projects.blender.org/blender/blender/commit/440d647cc1)).
  - Fix unreported: Fix incorrect MIS with principled BSDF and specular
    roughness 0
    ([rB866be342](https://projects.blender.org/blender/blender/commit/866be3423c)).
  - Fix unreported: Cycles: Fix possible race condition when
    initializing devices list
    ([rB93d711ce](https://projects.blender.org/blender/blender/commit/93d711ce55)).
  - Fix unreported: Cycles: Fix wrong shading when some mesh triangle
    has non-finite coordinate
    ([rB075950ad](https://projects.blender.org/blender/blender/commit/075950ad66)).
  - Fix unreported: Cycles: Fix compilation error with latest OIIO
    ([rB49f57e53](https://projects.blender.org/blender/blender/commit/49f57e5346)).
  - Fix unreported: Cycles: Fix compilation error with OIIO compiled
    against system PugiXML
    ([rBb6f3fec2](https://projects.blender.org/blender/blender/commit/b6f3fec259)).
  - Fix unreported: Cycles: Fix compilation error of standalone
    application
    ([rB391f7cc4](https://projects.blender.org/blender/blender/commit/391f7cc406)).
  - Fix unreported: Cycles: Workaround for performance loss with the
    CUDA 9.0 SDK
    ([rBb3adce77](https://projects.blender.org/blender/blender/commit/b3adce7766)).
  - Fix unreported: Cycles: Fix difference in image Clip extension
    method between CPU and GPU
    ([rBa50c381f](https://projects.blender.org/blender/blender/commit/a50c381fac)).
  - Fix unreported: Cycles: Fix crash opening user preferences after
    adding extra GPU
    ([rBa3616980](https://projects.blender.org/blender/blender/commit/a3616980c6)).
  - Fix unreported: Cycles: Fix bug in user preferences with factory
    startup
    ([rB2f5a027b](https://projects.blender.org/blender/blender/commit/2f5a027b94)).

### Sequencer

  - Fix [\#52890](http://developer.blender.org/T52890): Crash unlinking
    sequencer sound
    ([rBeaba111b](https://projects.blender.org/blender/blender/commit/eaba111bd5)).
  - Fix [\#53430](http://developer.blender.org/T53430): Cut at the strip
    end fails w/ endstill
    ([rB5ab1897d](https://projects.blender.org/blender/blender/commit/5ab1897de7)).
  - Fix [\#52940](http://developer.blender.org/T52940): VSE Glow Effect
    Strip on transparent images has no blur
    ([rB3ad84309](https://projects.blender.org/blender/blender/commit/3ad84309df)).
  - Fix [\#53639](http://developer.blender.org/T53639): text sequence
    strips no stamped into render
    ([rB8f7030e5](https://projects.blender.org/blender/blender/commit/8f7030e5f3)).

## UI / Spaces / Transform

### 3D View

  - Fix [\#52959](http://developer.blender.org/T52959): Local view
    looses clip range on exit
    ([rB7f6e9c59](https://projects.blender.org/blender/blender/commit/7f6e9c595b)).
  - Fix [\#53850](http://developer.blender.org/T53850): Lock to Cursor
    breaks 3D manipulators
    ([rBb90f3928](https://projects.blender.org/blender/blender/commit/b90f3928a3)).

<!-- end list -->

  - Fix unreported: Fix ruler access from search pop-up
    ([rBb0fbe95a](https://projects.blender.org/blender/blender/commit/b0fbe95a1b)).
  - Fix unreported: 3D View: use shortest angle between quaternions
    ([rB2895cb22](https://projects.blender.org/blender/blender/commit/2895cb22a7)).

### Input (NDOF / 3D Mouse)

  - Fix [\#52998](http://developer.blender.org/T52998): disabled menu
    entries responding to key shortcuts
    ([rB20c96cce](https://projects.blender.org/blender/blender/commit/20c96cce86)).
  - Fix [\#52861](http://developer.blender.org/T52861): Keymap editor
    filter doesn't show shortcuts using "+"
    ([rBca236408](https://projects.blender.org/blender/blender/commit/ca236408f3)).

### Outliner

  - Fix [\#53342](http://developer.blender.org/T53342): Outliner 'select
    hierarchy' broken
    ([rB7153cee3](https://projects.blender.org/blender/blender/commit/7153cee305)).

### Transform

  - Fix [\#53463](http://developer.blender.org/T53463): Rotation
    numerical input shows instable behaviour
    ([rB8a9d64b5](https://projects.blender.org/blender/blender/commit/8a9d64b578)).
  - Fix [\#53309](http://developer.blender.org/T53309): Remove default
    'Clear loc/rot/scale delta transform' shortcuts
    ([rBc4f8d924](https://projects.blender.org/blender/blender/commit/c4f8d924e1)).
  - Fix [\#52086](http://developer.blender.org/T52086): Graph editor
    "normalize" drag errors for integers
    ([rB9f916baa](https://projects.blender.org/blender/blender/commit/9f916baa70)).
  - Fix [\#53311](http://developer.blender.org/T53311): transform
    edge/normal orientation
    ([rB57b11d8b](https://projects.blender.org/blender/blender/commit/57b11d8b4e)).

### User Interface

  - Fix [\#52800](http://developer.blender.org/T52800): fix UI
    flickering with Mesa on Linux
    ([rBc85a305c](https://projects.blender.org/blender/blender/commit/c85a305c09)).
  - Fix [\#53630](http://developer.blender.org/T53630): Effect strips
    not disFix [\#52977](http://developer.blender.org/T52977): playing
    Input data. Parent bone name disappeared in the UI in pose mode
    ([rB4bc89d81](https://projects.blender.org/blender/blender/commit/4bc89d815c)).

<!-- end list -->

  - Fix unreported: Fix failure in our UI code that could allow search
    button without search callbacks, leading to crash
    ([rBe8e40b17](https://projects.blender.org/blender/blender/commit/e8e40b171b)).
  - Fix unreported: Add some security checks against future bad float
    UIprecision values
    ([rB7e4be7d3](https://projects.blender.org/blender/blender/commit/7e4be7d348)).
  - Fix unreported: UI: avoid int cast before clamping number input
    ([rBa437b9bb](https://projects.blender.org/blender/blender/commit/a437b9bbbe)).
  - Fix unreported: UI: fullstop at end of tooltips
    ([rB4c3e2518](https://projects.blender.org/blender/blender/commit/4c3e2518c8)).
  - Fix unreported: Fix incorrect allocation size
    ([rB6b2d1f63](https://projects.blender.org/blender/blender/commit/6b2d1f63db)).

## Game Engine

  - Fix unreported: Fix BGE sound actuator property access
    ([rBd2d20777](https://projects.blender.org/blender/blender/commit/d2d207773e)).

## System / Misc

### Collada

  - Fix [\#53322](http://developer.blender.org/T53322): Collada export
    crash w/ shape keys
    ([rB869e4a34](https://projects.blender.org/blender/blender/commit/869e4a3420)).
  - Fix [\#53230](http://developer.blender.org/T53230): avoid
    Nullpointer problems in Collada Exporter
    ([rBeaf14b1e](https://projects.blender.org/blender/blender/commit/eaf14b1ebf)).
  - Fix [\#52831](http://developer.blender.org/T52831): removed
    enforcement of matrix decomposition when animations are exported
    ([rBa123dafd](https://projects.blender.org/blender/blender/commit/a123dafdc0)).

### File I/O

  - Fix [\#52816](http://developer.blender.org/T52816): regression can't
    open file in 2.79 (crash)
    ([rBa4116673](https://projects.blender.org/blender/blender/commit/a4116673c7)).
  - Fix [\#53002](http://developer.blender.org/T53002): Batch-Generate
    Previews generate empty or none image for large objects
    ([rB47a388b2](https://projects.blender.org/blender/blender/commit/47a388b2a9)).
  - Fix [\#52514](http://developer.blender.org/T52514): don't clear
    filename when dropping directory path in file browser
    ([rB625b2f5d](https://projects.blender.org/blender/blender/commit/625b2f5dab)).
  - Fix [\#53250](http://developer.blender.org/T53250): Crash when
    linking/appending a scene to a blend when another linked scene in
    this blend is currently open/active
    ([rB06df30a4](https://projects.blender.org/blender/blender/commit/06df30a415)).
  - Fix [\#53572](http://developer.blender.org/T53572): Alembic imports
    UV maps incorrectly
    ([rB0f841e24](https://projects.blender.org/blender/blender/commit/0f841e24b0)).

<!-- end list -->

  - Fix unreported: Alembic import: fixed mesh corruption when changing
    topology
    ([rBbae796f0](https://projects.blender.org/blender/blender/commit/bae796f0d5)).

### Other

  - Fix [\#53274](http://developer.blender.org/T53274): Saving template
    prefs overwrites default prefs
    ([rB38fdfe75](https://projects.blender.org/blender/blender/commit/38fdfe757d)).
  - Fix [\#53637](http://developer.blender.org/T53637): Keymap from
    app-template ignored
    ([rB8c484d0b](https://projects.blender.org/blender/blender/commit/8c484d0bda)).

<!-- end list -->

  - Fix unreported: Fix potential string buffer overruns
    ([rB30f53d56](https://projects.blender.org/blender/blender/commit/30f53d56b6)).
  - Fix unreported: Fix build with OSL 1.9.x, automatically aligns to 16
    bytes now
    ([rBf968268c](https://projects.blender.org/blender/blender/commit/f968268c1e)).
  - Fix unreported: WM: minor correction to user-pref writing
    ([rB75aec5ee](https://projects.blender.org/blender/blender/commit/75aec5eeaa)).
  - Fix unreported: WM: don't load preferences on 'File -\> New'
    ([rB571e801b](https://projects.blender.org/blender/blender/commit/571e801b27)).
  - Fix unreported: WM: load UI for new file, even when pref disabled
    ([rB9f0ebb09](https://projects.blender.org/blender/blender/commit/9f0ebb0941)).
  - Fix unreported: Fix MSVSC2017 error
    ([rB405874bd](https://projects.blender.org/blender/blender/commit/405874bd79)).
  - Fix unreported: Fix SGI foramt reader CVE-2017-2901
    ([rB7b8b621c](https://projects.blender.org/blender/blender/commit/7b8b621c1d)).
  - Fix unreported: Memory: add MEM\_malloc\_arrayN() function to
    protect against overflow
    ([rBa9727298](https://projects.blender.org/blender/blender/commit/a972729895)).
  - Fix unreported: Fix buffer overflows in TIFF, PNG, IRIS, DPX, HDR
    and AVI loading
    ([rB16718fe4](https://projects.blender.org/blender/blender/commit/16718fe4ea)).
  - Fix unreported: Fix buffer overflow vulernability in thumbnail file
    reading
    ([rB04c51312](https://projects.blender.org/blender/blender/commit/04c5131281)).
  - Fix unreported: Fix buffer overflow vulnerabilities in mesh code
    ([rB9287434f](https://projects.blender.org/blender/blender/commit/9287434fa1)).
  - Fix unreported: Fix buffer overflow vulnerability in curve, font,
    particles code
    ([rB8dbd5ea4](https://projects.blender.org/blender/blender/commit/8dbd5ea4c8)).
  - Fix unreported: Fix manual lookups (data is now lowercase)
    ([rBcae8c68c](https://projects.blender.org/blender/blender/commit/cae8c68ca6)).

### Python

  - Fix [\#53273](http://developer.blender.org/T53273): render bake
    settings properties not showing correct Python path
    ([rB825aecae](https://projects.blender.org/blender/blender/commit/825aecaee3)).
  - Fix [\#52442](http://developer.blender.org/T52442):
    bl\_app\_templates\_system not working
    ([rBd8935315](https://projects.blender.org/blender/blender/commit/d89353159f)).
  - Fix [\#53294](http://developer.blender.org/T53294):
    bpy.ops.image.open crash
    ([rB8ea3f643](https://projects.blender.org/blender/blender/commit/8ea3f6438d)).
  - Fix [\#53191](http://developer.blender.org/T53191): Python API
    Reference link wrong in splash screen
    ([rB5f2307d1](https://projects.blender.org/blender/blender/commit/5f2307d1a7)).
  - Fix [\#52982](http://developer.blender.org/T52982): Join operator
    with context override crashes Blender 2.79
    ([rB09c38726](https://projects.blender.org/blender/blender/commit/09c387269a)).
  - Fix [\#53772](http://developer.blender.org/T53772): Presets don't
    support colons
    ([rB91ce2957](https://projects.blender.org/blender/blender/commit/91ce295796)).

<!-- end list -->

  - Fix unreported: Fix bpy.utils.resource\_path('SYSTEM') output
    ([rB21ecdacd](https://projects.blender.org/blender/blender/commit/21ecdacdf1)).
  - Fix unreported: Fix setting the operator name in Py operator API
    ([rB8c9bebfe](https://projects.blender.org/blender/blender/commit/8c9bebfe28)).
  - Fix unreported: Docs: add note for bmesh face\_split\_edgenet
    ([rBa253b1b7](https://projects.blender.org/blender/blender/commit/a253b1b7bc)).
  - Fix unreported: Fix edge-split bmesh operator giving empty result
    ([rBce6e30b3](https://projects.blender.org/blender/blender/commit/ce6e30b3dc)).
  - Fix unreported: Fix BMesh PyAPI internal flag clearing logic
    ([rB6639350f](https://projects.blender.org/blender/blender/commit/6639350f92)).
  - Fix unreported: Docs: clarify return value for BVH API
    ([rB0c456eb9](https://projects.blender.org/blender/blender/commit/0c456eb90a)).
  - Fix unreported: bl\_app\_override: support empty UI layout items
    ([rB6d640504](https://projects.blender.org/blender/blender/commit/6d640504e8)).
  - Fix unreported: Fix bad 'poll' prop callback API doc
    ([rB3193045c](https://projects.blender.org/blender/blender/commit/3193045c59)).
  - Fix unreported: Fix background\_job template
    ([rB38357cd0](https://projects.blender.org/blender/blender/commit/38357cd004)).
  - Fix unreported: Fix bmesh.utils.face\_join arg parsing
    ([rB20cccb15](https://projects.blender.org/blender/blender/commit/20cccb1561)).

### System

  - Fix [\#53004](http://developer.blender.org/T53004): XWayland ignores
    cursor-warp calls
    ([rB1ab828d4](https://projects.blender.org/blender/blender/commit/1ab828d4bb)).
  - Fix [\#53068](http://developer.blender.org/T53068): AMD Threadripper
    not working well with Blender
    ([rBdfed7c48](https://projects.blender.org/blender/blender/commit/dfed7c48ac)).

<!-- end list -->

  - Fix unreported: GPU: Fix memory corruption in GPU\_debug on GTX1080
    ([rB3a1ade22](https://projects.blender.org/blender/blender/commit/3a1ade22e5)).
  - Fix unreported: Task scheduler: Start with suspended pool to avoid
    threading overhead on push
    ([rB8553449c](https://projects.blender.org/blender/blender/commit/8553449cf2)).

<hr/>
