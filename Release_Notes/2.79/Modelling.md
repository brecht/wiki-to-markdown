# Blender 2.79: Mesh Modeling

## Modifiers

![Surface Deform example: Cloth simulation copied to an arbitrary mesh
with rings as
duplifaces](../../images/Cycles2.79_surface_deformer_example.png
"Surface Deform example: Cloth simulation copied to an arbitrary mesh with rings as duplifaces")

  - New Surface Deform modifier: allow an arbitrary mesh surface to
    control the deformation of another, essentially transferring its
    motion/deformation. One great use for this is to have a proxy mesh
    for cloth simulation, which will in turn drive the motion of your
    final and more detailed mesh, which would otherwise not be suitable
    for simulation. See the [surface deform
    documentation](https://docs.blender.org/manual/en/dev/modeling/modifiers/deform/surface_deform.html)
    for details.

![Surface Deform Modifier User
Interface](../../images/Blender_Surface_Deform_Modifier.jpg
"Surface Deform Modifier User Interface")

  - Displace modifier with direction X/Y/Z/XYZ can now work in local or
    global space.
    ([e0a34e963f](https://projects.blender.org/blender/blender/commit/e0a34e963fff))
  - Displace modifier support for multi-threading.
  - Mirror Modifier: add offsets for mirrored UVs.

  

## Other Improvements

  - Delete Unlocked Vertex Groups in vertex group list menu.
    ([cf8f6d1dbc](https://projects.blender.org/blender/blender/commit/cf8f6d1dbcfc))
  - Set custom normals from selected faces in the Normals section of
    Shading tools, Set Normals from Faces operator.
    ([dd6fa94dc6](https://projects.blender.org/blender/blender/commit/dd6fa94dc6))
  - Improved center of mass calculation for mesh centers.
    ([37bc3850ce](https://projects.blender.org/blender/blender/commit/37bc3850cee))
  - Sculpt dynamic topology constant detail is now a resolution value.
    ([6271410](https://projects.blender.org/blender/blender/commit/6271410))
  - Mesh intersect has a new Cut separate mode, keeping each side of the
    intersection separate without splitting faces in half.
    ([a461216885](https://projects.blender.org/blender/blender/commit/a461216885))
  - The Monkey primitive now has default UVs.
    ([a070a5befa](https://projects.blender.org/blender/blender/commit/a070a5befa11))
  - Improved default UVs for UV Sphere and Icosphere.
    ([a070a5befa](https://projects.blender.org/blender/blender/commit/a070a5befa11))
  - Screw Modifier now has remove doubles option (useful for closing off
    end-points)
    ([584523e0ad](https://projects.blender.org/blender/blender/commit/584523e0adeb2663077602953f0d3288c4c60fe4))
