# Blender 2.79: Animation

## Pose library

![Poses are only keyed for bone
selections](../../images/blender_279_poselib_add_selected.png
"Poses are only keyed for bone selections")

  - Poses in a pose library can now be ordered.
    ([19268fbad](https://projects.blender.org/blender/blender/commit/19268fbad))
  - Adding a pose in a pose library only stores keys for selected bones.
    If no bones are selected, all bones are keyed. This restores a
    feature lost in version 2.57.
    ([601ce6a89c](https://projects.blender.org/blender/blender/commit/601ce6a89c4f7ecc))

  

## Frame Change Undo

The animation workflow is more smooth now that all frame change related
operators are grouped together. Let's say the animator tries a new pose,
and then do a few frame changes (scrub a few frames, go to a marker, go
to the previous keyframe). Previously if the animator wanted to roll
back the pose, she would need to undo multiple times, while nervously
risking facing a filled undo stack. With this change undo will quickly
bring back the previous pose, bypassing all but the first frame change
operation.([8f2e6f7e](https://projects.blender.org/blender/blender/commit/8f2e6f7e))

## Other Improvements

  - Add animation channel option to make it always visible regardless of
    selection.
    ([98c7e75897](https://projects.blender.org/blender/blender/commit/98c7e7589754))
  - The Breakdowner tool can now be constrainted to work on specific
    transforms and axes, by pressing the following keys while the tool
    is active: **G** (translate), **S** (scale), **R** (rotate), **B**
    (bendy bones), **C** custom properties, and **X** **Y** **Z** for
    the corresponding axes.
    ([c374e9f1f5](https://projects.blender.org/blender/blender/commit/c374e9f1f5b9))
  - Motion paths now have options to customize the line thickness and
    color.
    ([404e59c842](https://projects.blender.org/blender/blender/commit/404e59c842fe))
  - Add support for Rotational Difference drivers on objects instead of
    only bones.
    ([98045648ab](https://projects.blender.org/blender/blender/commit/98045648ab49))
