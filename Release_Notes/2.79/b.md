# Blender 2.79b: Bug Fixes

Changes from revision
[rB9accd885](https://projects.blender.org/blender/blender/commit/9accd8852b11)
to
[rBf4dc9f9d](https://projects.blender.org/blender/blender/commit/f4dc9f9d68bd),
inclusive.

Total fixed bugs: 12 (7 from tracker, 5 reported/found by other ways).

**Note:** Nearly all commits since 2.79a are listed here, only really
technical/non-user affecting ones have been skipped.

  - Fix [\#54003](http://developer.blender.org/T54003): Particles - Size
    and random size not present in "physics" tab
    ([rBa3409d3f](https://projects.blender.org/blender/blender/commit/a3409d3f53f1d)).
  - Fix [\#53478](http://developer.blender.org/T53478),
    [\#53430](http://developer.blender.org/T53430): Sequencer cut edge
    case fails
    ([rBf3018322](https://projects.blender.org/blender/blender/commit/f3018322c097f)).
  - Fix [\#54204](http://developer.blender.org/T54204): Wrong selection
    on Clean Tracks (Motion Tracking)
    ([rBcaa0b0fa](https://projects.blender.org/blender/blender/commit/caa0b0fadf928)).
  - Fix [\#54206](http://developer.blender.org/T54206): bevel and inset
    operations repeat did not remember offset
    ([rB596f33f8](https://projects.blender.org/blender/blender/commit/596f33f801f8)).
  - Fix [\#54348](http://developer.blender.org/T54348): Bone dissolve
    gives invalid hierarchy
    ([rB865fbe34](https://projects.blender.org/blender/blender/commit/865fbe343a906)).
  - Fix [\#54234](http://developer.blender.org/T54234): add
    BLENDER\_VERSION\_CHAR to .plist
    ([rBb5b52604](https://projects.blender.org/blender/blender/commit/b5b5260464f)).
  - Fix [\#54360](http://developer.blender.org/T54360): FFMPEG bitrate
    not editable for all codecs
    ([rBf4dc9f9d](https://projects.blender.org/blender/blender/commit/f4dc9f9d68b)).

<!-- end list -->

  - Fix (unreported): Fix Error passing context arg to marker menu
    ([rB80766374](https://projects.blender.org/blender/blender/commit/807663742b7bc)).
  - Fix (unreported): Fixed: cache clearing when using multiple Alembic
    files
    ([rB09c88fed](https://projects.blender.org/blender/blender/commit/09c88fed1f5)).
  - Fix (unreported): Fix bone dissolve using wrong envelope radius
    ([rBce51066e](https://projects.blender.org/blender/blender/commit/ce51066e47175)).
  - Fix (unreported): Tracking: Warn when no tracks are selected when
    creating mesh
    ([rB658fb7f4](https://projects.blender.org/blender/blender/commit/658fb7f4536)).
  - Fix (unreported): Tracking: Make object created form tracks active
    and selected
    ([rB23ffd4ec](https://projects.blender.org/blender/blender/commit/23ffd4ec394)).

## Addons:

Fix [\#53766](http://developer.blender.org/T53766): MeasureIt
measurements not appearing on all layers an object exists
([rBAdc6704a](https://projects.blender.org/blender/blender-addons/commit/dc6704ab4231)).
