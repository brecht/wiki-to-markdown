# Blender 2.79: User Interface

## Application Templates

Application templates are a feature that allows you to define a
re-usable configuration that can be selected to replace the default
configuration, without requiring a separate Blender installation or
overwriting your personal settings.
([f68145011f](https://projects.blender.org/blender/blender/commit/f68145011fd46))

For more details, see the [application templates
documentation](https://docs.blender.org/manual/en/dev/advanced/app_templates.html).

## Shortcut Keys

  - Support for customizing shortcuts of button interactions. Shortcut
    editing has been enabled for:
      - Eyedropper operator (Default shortcut: **E**)
      - Keyframe operators (Default shortcuts: **I** to insert,
        **Alt+I** to delete, **Shift+Alt+I** to clear)
      - Driver operators (Default shortcuts: **Ctrl+D** to insert,
        **Ctrl+Alt+D** to delete)
      - Keying Set operators (Default shortcuts: **K** to add to Keying
        Set, **Alt+K** to remove from Keying Set)

<!-- end list -->

  - NDOF navigation can now be used during border and circle select.
  - Transform manipulator now allows pressing **Shift** before selecting
    the axis.
  - Cycling through tabs with **Ctrl+Tab** and **Ctrl+Shift+Tab** is now
    possible in the properties editor, tabbed tool shelves and user
    preferences
    ([718bf8fd9d](https://projects.blender.org/blender/blender/commit/718bf8fd9d1))
  - An entire vector or color can now be copied and pasted using
    **Ctrl+Alt+C**/**Ctrl+Alt+V**, instead of just a single value.
    ([rB4e95a906](https://projects.blender.org/blender/blender/commit/4e95a9069e)).

## Automatic DPI

High DPI displays on Windows 8.1+ and X11 are now better supported. The
interface is now automatically scaled with the system DPI. On Windows
multiple monitors with different DPI are supported as well. For X11, the
DPI is based on Xft.dpi or xrandr --dpi, which is set for most desktop
environments.
([fe3fb23697](https://projects.blender.org/blender/blender/commit/fe3fb236970c))

The user preferences now have a Display Scale setting in the Interface
tab, to adjust the size of fonts and buttons relative to the
automatically detected DPI.

## Other Improvements

![Uv Editing tools](../../images/Release_notes_2.79_uv-tools.png
"Uv Editing tools")

  - For disabled buttons, more tooltips show the reason why the button
    is disabled now.
    ([6f80604509](https://projects.blender.org/blender/blender/commit/6f8060450934))
  - A Modifiers tab has been added to the NLA & VSE editors to match the
    behavior of the Graph editor.
    ([65c8937f7e](https://projects.blender.org/blender/blender/commit/65c8937f7ed8))
  - Sequencer properties shelf has been cleaned up to better use space.
    ([415ff7467c](https://projects.blender.org/blender/blender/commit/415ff7467ca9))
  - UV editing tools have been added to the toolshelf in the UV/Image
    editor.
    ([8b0fbb909b](https://projects.blender.org/blender/blender/commit/8b0fbb909b15))
  - 3D view now support using both orbit selected and orbit depth at the
    same time, using cursor depth if nothing is selected.
    ([a1164eb3dd](https://projects.blender.org/blender/blender/commit/a1164eb3ddb))
  - Image empties now support multi-view stereo.
    ([fffb1a4](https://projects.blender.org/blender/blender/commit/fffb1a4))
  - Add-on duplicates warning now includes paths in the User Interface.
    ([112e4de885](https://projects.blender.org/blender/blender/commit/112e4de8855a))

![Example of a disabled property
tooltip](../../images/Rna_prop_editableinfo.png
"Example of a disabled property tooltip")
