# Blender 2.90: User Interface

## Search

Operator search will now search through menu entries, to better show
relevant operators and where to find them in menus. Adding operators to
quick favorites or assigning shortcuts is now easy by right clicking on
search results.

For developers that need more low-level search, Developer Extras can be
enabled after which raw operators will appear in search results as well.
Add-ons that expose operators only through search [need to be updated to
expose them through a
menu](Python_API.md).

## Layout

  - Checkbox placements was reworked to place text on the right hand
    side, group together related settings and make layouts more compact.
    ([219049bb3b](https://projects.blender.org/blender/blender/commit/219049bb3b76),
    [7fc60bff14](https://projects.blender.org/blender/blender/commit/7fc60bff14a6))

![../../images/Release\_notes\_properties\_checkbox\_split\_layout.png](../../images/Release_notes_properties_checkbox_split_layout.png
"../../images/Release_notes_properties_checkbox_split_layout.png")
![../../images/Release\_notes\_properties\_checkbox\_subpanel.png](../../images/Release_notes_properties_checkbox_subpanel.png
"../../images/Release_notes_properties_checkbox_subpanel.png")

  - Shader nodes in the material properties editor now show socket
    colors matching the shader editor, and are positioned on the left
    side.
    ([7ef2dd8424](https://projects.blender.org/blender/blender/commit/7ef2dd84246c),
    [675d42dfc3](https://projects.blender.org/blender/blender/commit/675d42dfc33d))  

![../../images/Release\_notes\_node\_socket\_icons.png](../../images/Release_notes_node_socket_icons.png
"../../images/Release_notes_node_socket_icons.png")

## Statistics

  - Scene statistics available in 3D viewport with an (optional)
    "Statistics" Viewport Overlay.
    ([fd10ac9aca](https://projects.blender.org/blender/blender/commit/fd10ac9acaa0)).

![Statistics Overlay|600px](../../images/Statistics_Overlay.png
"Statistics Overlay|600px")

  - Status Bar now showing only version by default, but context menu can
    enable statistics, memory usage, etc.
    ([c08d847488](https://projects.blender.org/blender/blender/commit/c08d84748804)).

![Status Bar Context
Menu|600px](../../images/Status_Bar_Context_Menu.png
"Status Bar Context Menu|600px")

## Modifiers

Modifiers and other stacks now support drag and drop for reordering.

Their layouts were completely rewritten to make use of subpanels and the
current user interface guidelines.

Where they apply, the operation buttons ("Apply", "Copy" (now
"Duplicate"), and "Apply as Shape Key") are now in a menu on the right
side of the panel headers, along with new "Move to First" and "Move to
Last" buttons.

[File:New-modifier-stack.png|Mesh](File:New-modifier-stack.png%7CMesh)
Modifiers () <File:Constraint> UI 2.90.png|Constraints ()

There are now shortcuts for common modifier operations.
([1fa40c9f8a](https://projects.blender.org/blender/blender/commit/1fa40c9f8a81))
Enabled by default are:

  - Remove: X, Delete
  - Apply: Ctrl A
  - Duplicate: Shift D

## About

  - About Blender dialog added to the App menu to contain information
    like build, branch, and hash values. This declutters the Splash
    Screen and highlights only the release version.
    ([5b86fe6f33](https://projects.blender.org/blender/blender/commit/5b86fe6f3350)).

![../../images/About18.png](../../images/About18.png
"../../images/About18.png")

  

## Miscellaneous

  - Area borders given much larger hit area to make them easier to
    resize.
    ([4e8693ffcd](https://projects.blender.org/blender/blender/commit/4e8693ffcddb))

![Increased Border Hit Area|600px](../../images/Border_Hit_Size.png
"Increased Border Hit Area|600px")

  - Support for Windows Shell Links (shortcuts) in File Browser.
    Extended Mac Alias usage. Better display of linked items.
    ([1f223b9a1f](https://projects.blender.org/blender/blender/commit/1f223b9a1fce))

![Shortcuts and Aliases|600px](../../images/Shortcuts.png
"Shortcuts and Aliases|600px")

  - Pie menus support for A-Z accelerator keys, for quickly selecting
    items with the keyboard.
  - New "Instance Empty Size" setting in user preferences
    ([e0b5a20231](https://projects.blender.org/blender/blender/commit/e0b5a202311)).
  - Text field buttons now support local undo and redo while being
    edited.
    ([1e12468b84](https://projects.blender.org/blender/blender/commit/1e12468b84d2)).
  - Outliner Delete operator added to delete all selected objects and
    collections using either the keymap or context menu.
    ([ae98a033c8](https://projects.blender.org/blender/blender/commit/ae98a033c856)).
    Replaces the Delete Object and Delete Collection operators.
  - Outliner Delete Hierarchy now operates on all selected objects and
    collections. Additionally, the menu option is shown for objects in
    the View Layer display mode.
    ([26c0ca3aa7](https://projects.blender.org/blender/blender/commit/26c0ca3aa7f4)).
  - Dragging outliner elements to an edge will scroll the view.
    ([7dbfc864e6](https://projects.blender.org/blender/blender/commit/7dbfc864e6f8))
  - Dragging panels to the edge of a region will scroll it up or down.
    ([859505a3da](https://projects.blender.org/blender/blender/commit/859505a3dae8))
  - Move a few FFmpeg render properties from the scene properties to the
    audio output settings.
    ([a55eac5107](https://projects.blender.org/blender/blender/commit/a55eac5107ed))
  - Update the terminology used in some properties.
    ([1278657cc2](https://projects.blender.org/blender/blender/commit/1278657cc2d)),
    ([3ea302cf8e](https://projects.blender.org/blender/blender/commit/3ea302cf8ef)),
    ([ef0ec01461](https://projects.blender.org/blender/blender/commit/ef0ec014611))
  - 3D Viewport: Add Edge Loopcut Slide to edge menu.
    ([0fdb79fe58](https://projects.blender.org/blender/blender/commit/0fdb79fe584))
  - The interface for many operators was updated to be clearer and more
    consistent with the rest of Blender.
    ([17ebbdf1c1](https://projects.blender.org/blender/blender/commit/17ebbdf1c17d))
  - Option to print text in bold or italics style, synthesized from a
    single base UI font.
    ([b74cc23dc4](https://projects.blender.org/blender/blender/commit/b74cc23dc478))
