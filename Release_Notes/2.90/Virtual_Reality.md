# Blender 2.90: Virtual Reality

Only smaller updates were done for this release while the second
milestone is still being worked on.

## Landmarks

In the 2.83 release, landmarks were already available, but had limited
usefulness. With 2.90, landmarks should be much more practicable.

  - Tweaks to how landmarks are used to establish the viewer pose:
      - Changing to a landmark moves the view exactly to it, rather than
        keeping the current position offset.
      - Disabling positional tracking moves the viewer back to the exact
        landmark position.
  - New landmark type: *Custom Pose*. These allow specifying a position
    and rotation to use as base pose, without a camera.
  - New landmark operators:
    ![../../images/Release\_notes\_290\_vr\_landmarks\_operators.png](../../images/Release_notes_290_vr_landmarks_operators.png
    "../../images/Release_notes_290_vr_landmarks_operators.png")
      - *Add VR Landmark from Session*  
        Add a new custom pose landmark, based on the current position
        and rotation of the viewer.
      - *Add VR Landmark from Camera*  
        If a camera object is active, this adds a landmark with a
        position and rotation based on the camera.
      - *Update Custom VR Landmark*  
        Updates the selected landmark's position and rotation based on
        the current viewer pose.
      - *Cursor to VR Landmark*
      - *Scene Camera to VR Landmark*  
        Copy the position and rotation of the selected landmark to the
        scene camera.
      - *New Camera from VR Landmark*  
        Add a camera with the position and rotation of the selected
        landmark.
  - *Show Landmarks* 3D View option, available in the *Viewport
    Feedback* panel. Enables visual indicators for positions and
    rotation of landmarks.

([607d745a79](https://projects.blender.org/blender/blender/commit/607d745a79e0),
[rBAfe21f93](https://projects.blender.org/blender/blender-addons/commit/fe21f93ae98d),
[rBA362200b](https://projects.blender.org/blender/blender-addons/commit/362200bfeed6))

## Miscellaneous

  - A warning is displayed in the *Viewport Feedback*, indicating that
    the feedback options may have a significant performance impact.
