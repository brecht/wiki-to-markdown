# Blender 2.90: Modeling

## Tools

  - Extrude Manifold: new tool to automatically split and remove
    adjacent faces when extruding inwards.
    ([b79a5bdd5a](https://projects.blender.org/blender/blender/commit/b79a5bdd5ae0)).
  - Ruler: snapping support for ruler tool.
    ([1f7a791](https://projects.blender.org/blender/blender/commit/1f7a791))
  - Topology cleanup oriented tools Merge (including By Distance aka
    Remove Doubles), Rip, Delete, Dissolve, Connect Path, Knife and Tris
    To Quads now correctly preserve custom normal vectors.
    ([93c8955a7](https://projects.blender.org/blender/blender/commit/93c8955a7))
  - The custom profile for the bevel modifier and tool now supports
    bezier curve handle types.
    ([baff05ad1c](https://projects.blender.org/blender/blender/commit/baff05ad1c15))

![Free, aligned, vector, and automatic handle
types](../../images/Curve_Profile_Handles_2.90.png
"Free, aligned, vector, and automatic handle types")

  - The bevel tool and modifier have a new 'Absolute' mode for
    interpreting the 'amount' value. This mode is like Percent, but
    measures absolute distance along adjacent edges instead of a
    percentage.
    [466e716495](https://projects.blender.org/blender/blender/commit/466e716495ff))
  - The bevel tool and modifier use a new method to decide which
    material and UV values to use for the center polygons in odd-segment
    bevels. It should give more consistency in different parts of the
    model compared to the seemingly random choice made up til now.
    [e3a420c477](https://projects.blender.org/blender/blender/commit/e3a420c4773a)

## UV Editor

  - The new mesh editing options **Correct Face Attributes** and **Keep
    Connected** adjust UV's and vertex colors when moving mesh
    components. The option is activated in the top right corner of the
    3d viewport in the mesh editing menu
    ([rB4387aff](https://projects.blender.org/blender/blender/commit/4387aff))

![../../images/Correct\_Face\_Attributes\_and\_Keep\_Connected.png](../../images/Correct_Face_Attributes_and_Keep_Connected.png
"../../images/Correct_Face_Attributes_and_Keep_Connected.png")
![../../videos/Correct\_face\_attributes\_uv\_video.mp4](../../videos/Correct_face_attributes_uv_video.mp4
"../../videos/Correct_face_attributes_uv_video.mp4")

  - **Edge Ring Select**: matching edit-mesh functionality of the same
    name using \`Ctrl-Alt-LeftMouse\`
    ([1dd381828f](https://projects.blender.org/blender/blender/commit/1dd381828fa3f)).

<!-- end list -->

  - **Pick Shortest Path**: hold down \`CTRL\` and select UV components
    with \`left mouse\` to select the shortest path between the UV
    components.
    ([ea5fe7abc1](https://projects.blender.org/blender/blender/commit/ea5fe7abc183))

![../../videos/UV\_pick\_shortest\_path.mp4](../../videos/UV_pick_shortest_path.mp4
"../../videos/UV_pick_shortest_path.mp4")

  - **Pick Shortest Path**: grid type selection if you activate **fill
    region** or use \`Ctrl-Shift-LeftMouse\` when selecting UV
    components

![../../videos/Pick\_shortest\_path\_fill\_region.mp4](../../videos/Pick_shortest_path_fill_region.mp4
"../../videos/Pick_shortest_path_fill_region.mp4")

  - **UV Rip**: separates UV components (vertices, edges, faces) from
    connected components. The operator is run by pressing the \`V\`
    hotkey. The components are ripped in the direction of the mouse
    pointer position.
    ([rB89cb41fa](https://projects.blender.org/blender/blender/commit/89cb41faa0596d898f850f68c46e2b39c25e6452))

![../../videos/UV\_Rip.mp4](../../videos/UV_Rip.mp4
"../../videos/UV_Rip.mp4")

  - Option to change the opacity of the UV overlay.
    ([90d3fe1e4d](https://projects.blender.org/blender/blender/commit/90d3fe1e4d)).

<!-- end list -->

  - Remove selection threshold for nearby coordinates, only select when
    UV coordinates are identical.
    ([b88dd3b8e7](https://projects.blender.org/blender/blender/commit/b88dd3b8e7b))

## Curves

  - Curve Normals are disabled by default.
    ([0bd7e202fb](https://projects.blender.org/blender/blender/commit/0bd7e202fbd6))
  - Curve Handles now can be Hidden/Selected or All.
    ([49f59092e7](https://projects.blender.org/blender/blender/commit/49f59092e7c8))

The new default for Handles is \`Selected\`. For Nurb Curves, the
handlers are displayed always as it was in previous version.

![New Handle Display](../../images/GifCurvesAll.gif
"New Handle Display")

## Transform

  - Edge and Vert Slide now supports the other snapping types.
    ([e2fc9a88bc](https://projects.blender.org/blender/blender/commit/e2fc9a88bcb3),
    [9335daac2a](https://projects.blender.org/blender/blender/commit/9335daac2a36))
  - The snap can now be made to the intersection between constraint and
    geometry.
    ([d8206602fe](https://projects.blender.org/blender/blender/commit/d8206602feed))

![New behavior of the snap with
constraint](../../images/Snap_with_Constraint.gif
"New behavior of the snap with constraint")

## Modifiers

  - The ocean modifier can now generate maps for spray direction.
    ([17b89f6dac](https://projects.blender.org/blender/blender/commit/17b89f6dacba))

![full|center](../../images/Wave-modifier.jpg "full|center")

  - Applying a modifier as a shapekey is now allowed even when the
    object has linked duplicates.
    ([01c8aa12a1](https://projects.blender.org/blender/blender/commit/01c8aa12a1))
  - New menu option to Save As Shapekey, which applies the modifier as a
    shape key without removing it.
    ([01c8aa12a1](https://projects.blender.org/blender/blender/commit/01c8aa12a1))
