# Blender 2.90: Grease Pencil

## User Interface

  - Onion skinning support for annotations in the video sequencer.
    ([fda754145a](https://projects.blender.org/blender/blender/commit/fda754145ae5))
  - Layer Mask and Use Lights properties are animatable.
    ([221a7002a9](https://projects.blender.org/blender/blender/commit/221a7002a981))
  - New Material selector in Context menu and new Material menu using
    \`U\` key.
    ([6a7e9f2b76](https://projects.blender.org/blender/blender/commit/6a7e9f2b7647))

![Material selector context menu](../../images/Material_context_menu.png
"Material selector context menu")

  - New option to put Canvas Grid in front of any object.
    ([b369d46eb7](https://projects.blender.org/blender/blender/commit/b369d46eb775))
  - First keyframe is displayed only if the current frame is equals or
    greater
    ([11ba9eec70](https://projects.blender.org/blender/blender/commit/11ba9eec7017))

\`This change can produce changes in old animations. Review your
drawings and set the keyframe number to be displayed at the right time.
Usually, The fix is as simple as move or duplicate the first keyframe to
frame 1.\`

  - Keyframe type now is accessible using python.
    ([326db1b7cd](https://projects.blender.org/blender/blender/commit/326db1b7cd4e))
  - Default Vertex Color mode changed to \`Stroke and Fill\`.
    ([e079bf6996](https://projects.blender.org/blender/blender/commit/e079bf6996fc))
  - Added missing UV Fill property to RNA.
    ([f382109f38](https://projects.blender.org/blender/blender/commit/f382109f3885))

## Operators

  - Reproject stroke now allows to keep original stroke.
    ([37d68871b7](https://projects.blender.org/blender/blender/commit/37d68871b7c5))
  - New Bevel Depth and Resolution parameters converting strokes to
    curves.
    ([146473f083](https://projects.blender.org/blender/blender/commit/146473f08335))
  - New Convert mesh and Bake animation to grease pencil.
    ([bc7a4b126a](https://projects.blender.org/blender/blender/commit/bc7a4b126afb))

## Tools

  - Annotate Line tool now supports different types of arrows.
    ([668dd146f6](https://projects.blender.org/blender/blender/commit/668dd146f647))

![Annotate arrows types](../../images/Annotate_arrows.png
"Annotate arrows types")

  - Annotate tool now supports mouse stabilizer.
    ([9a7c4e2d44](https://projects.blender.org/blender/blender/commit/9a7c4e2d444f))
  - New Brush options and curves to randomize effects.
    ([b571516237](https://projects.blender.org/blender/blender/commit/b571516237a9))
  - When Using the Eraser tool a new frame is created if Additive
    Drawing is enabled.
    ([29afadcb15](https://projects.blender.org/blender/blender/commit/29afadcb15f7))
  - Improved selection of strokes in the fill area
    ([244ed645e0](https://projects.blender.org/blender/blender/commit/244ed645e08e))

![Fill Area Select](../../images/GifSelectAfter.gif "Fill Area Select")

  - Improved fill paint in vertex color mode and Tint tool.
    ([abeda01ac6](https://projects.blender.org/blender/blender/commit/abeda01ac6da))

## Modifiers and VFX

  - New Texture Mapping modifier to manipulate UV data.
    ([a39a6517af](https://projects.blender.org/blender/blender/commit/a39a6517af66))
  - New Restrict Visible Points Build parameter to define the visibility
    of stroke.
    ([a1593fa05b](https://projects.blender.org/blender/blender/commit/a1593fa05baf))
  - New Antialiasing parameter for Pixel effect.
    ([11a390e85e](https://projects.blender.org/blender/blender/commit/11a390e85e2b))
