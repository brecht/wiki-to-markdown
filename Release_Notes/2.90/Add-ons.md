# Blender 2.90: Add-ons

# Add-ons Updates

## BlenderKit

  - Perpendicular snap (in import settings) improves snapping to sloped
    surfaces or ceilings. Useful for trees on slopes/chandeliers under
    curved ceilings and similar.
  - A new fast rating operator was added. Accessible under F shortcut in
    asset bar.
  - Ui was reorganized to subpanels, with many small useful tweaks.
  - Asset menu is available also in the Selected Model panel. Enables
    'search similar' and other commands for objects that are already in
    the scene.
  - More info on [the
    webpage](https://www.blenderkit.com/articles/updates-blender-290/)
    of authors

![Snap objects perpendiculary, for trees or other things on sloped
surfaces/ceilings](../../videos/Perpendicular_snap.mp4
"Snap objects perpendiculary, for trees or other things on sloped surfaces/ceilings")

## Collection Manager

![Collection Manager popup with RTOs aligned to the
right.](../../images/Collection_Manager_Blender_2.90.png
"Collection Manager popup with RTOs aligned to the right.")

### New Features

  - Added an option to align RTOs to the right.
    ([rBA2aa4745](https://projects.blender.org/blender/blender-addons/commit/2aa47457))
  - Renamed the Filter Restriction Toggles popover to Display Options.
    ([rBA2aa4745](https://projects.blender.org/blender/blender-addons/commit/2aa47457))
  - Added line separators to the tree view to better differentiate rows,
    especially when RTOs are aligned to the right.
    ([rBA2aa4745](https://projects.blender.org/blender/blender-addons/commit/2aa47457))
  - Added a new Specials menu with items to Remove Empty Collections and
    Purge All Collections Without Objects.
    ([rBA711efc3](https://projects.blender.org/blender/blender-addons/commit/711efc3e))
  - Added a new Apply Phantom Mode button, this will apply the changes
    made to RTOs and quit Phantom Mode.
    ([rBAcee1751](https://projects.blender.org/blender/blender-addons/commit/cee17513))
  - Added a linear renumbering option and a constrain to branch option
    to the Renumber QCD Slots Operator; all options can now be combined
    with each other.
    ([rBA09133c5](https://projects.blender.org/blender/blender-addons/commit/09133c5a))
  - Added menu items for the Collection Manager popup and the QCD Move
    widget to the Object-\>Collection menu.
    ([rBA52edc5f](https://projects.blender.org/blender/blender-addons/commit/52edc5f4))

### Bug Fixes

  - Fixed removing collections not preserving the RTOs of their
    children.
    ([rBA969e77e](https://projects.blender.org/blender/blender-addons/commit/969e77ed))
  - Fixed an error when removing a collection with a child that is
    already linked to the parent collection.
    ([rBA0657e99](https://projects.blender.org/blender/blender-addons/commit/0657e99e))
  - Fixed the QCD Move Widget not accounting for the 3D View bounds when
    first called.
    ([rBA2c9bc1e](https://projects.blender.org/blender/blender-addons/commit/2c9bc1e6))
  - Vastly increased the performance when there are a large number of
    selected objects.
    ([rBAadac42a](https://projects.blender.org/blender/blender-addons/commit/adac42a4))
