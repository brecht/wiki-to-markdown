# Blender 2.90: EEVEE

## Motion Blur Improvement

The motion blur has been done from scratch and now support mesh
deformation, hair, and sub-frame accumulation for even better precision.
For more details [check out the
manual](https://docs.blender.org/manual/en/dev/render/eevee/render_settings/motion_blur.html#example).
([f84414d6e1](https://projects.blender.org/blender/blender/commit/f84414d6e14c)
[439b40e601](https://projects.blender.org/blender/blender/commit/439b40e601f8))

![../../images/Mb\_32step\_fx.png](../../images/Mb_32step_fx.png
"../../images/Mb_32step_fx.png")

## Improvements

  - Fix texture node repeat & filter mode to match Cycles better.
    ([b2dcff4c21](https://projects.blender.org/blender/blender/commit/b2dcff4c21a6))

## Sky Texture

  - Port of Preetham and Hosek/Wilkie sky models.
    ([9de09220fc](https://projects.blender.org/blender/blender/commit/9de09220fc5f))

![../../images/Hosek\_Eevee.png](../../images/Hosek_Eevee.png
"../../images/Hosek_Eevee.png")
