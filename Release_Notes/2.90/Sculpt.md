# Blender 2.90: Sculpting

## Multires Modifier

Multiresolution sculpting is now fully supported. It is now possible to
select the subdivision level to sculpt on and switch between levels,
with displacement information smoothly propagated between them.
([d4c547b7bd](https://projects.blender.org/blender/blender/commit/d4c547b7bd8))

![Multires Modifier UI](../../images/Multires_UI.png
"Multires Modifier UI")

### Unsubdivide and Rebuild subdivisions

The Multires Modifier can now rebuild lower subdivisions levels and
extract its displacement. This can be used to import models from any
subdivision based sculting software and rebuild all subdivisions levels
to be editable inside the modifier.
([f28875a998](https://projects.blender.org/blender/blender/commit/f28875a998d))

![Unsubdivide different meshes with triangles, ngons and non-manifold
geometry](../../videos/Unsubdivide_demo.mp4
"Unsubdivide different meshes with triangles, ngons and non-manifold geometry")

### Subdivision Modes

Multires can now create smooth, linear and simple subdivisions. This
means that any subdivision type can be created at any time without
changing the subdivision type of the modifier. Changing the subdivision
type of the modifier is now and advance option only used to support
legacy files.
([134619fabb](https://projects.blender.org/blender/blender/commit/134619fabbc))

![All subdivision modes available in Multires](../../images/Subdivs.png
"All subdivision modes available in Multires")

## Tools

  - The Cloth filter runs a cloth simulation in the entire mesh. It has
    3 force modes and the area were the force is applied can be defined
    by a Face Set.
    ([1d4bae8566](https://projects.blender.org/blender/blender/commit/1d4bae85669))

![Cloth filter applying forces to different Face
Sets](../../videos/Cloth_filter_demo.mp4
"Cloth filter applying forces to different Face Sets")

  - The Face Set edit operator allows to modify a Face Set. The
    supported operations are Grow (Ctrl + W) and Shrink (Ctrl + Alt +
    W).
    ([cb9de95d61](https://projects.blender.org/blender/blender/commit/cb9de95d61b))

## Brushes

  - The Pose Brush includes two more deformation modes: Scale/translate
    and Squash/stretch. They can be selected using the deformation
    property. Pressing Ctrl before starting the stroke selects the
    secondary deformation mode.
    ([77789a1904](https://projects.blender.org/blender/blender/commit/77789a19049))

<!-- end list -->

  - The Pose Brush has a Face Set FK mode, which deforms the mesh using
    the Face Sets to simulate and FK rig.
    ([3778f168f6](https://projects.blender.org/blender/blender/commit/3778f168f68))

![Pose Brush with FK/IK modes](../../videos/Pose_fk_290_demo.mp4
"Pose Brush with FK/IK modes")

  - The Pose Brush has a Connected only property. By disabling it, the
    brush can affect multiple disconnected parts of the same mesh. The
    distance to consider a part as connected can be tweaked with the Max
    Element Distance property.
    [438bd82371](https://projects.blender.org/blender/blender/commit/438bd823714)

<!-- end list -->

  - Clay strips now has more predictable pressure/size and
    pressure/strength curves. Its deformation algorithm was modified to
    prevent geometry artifacts in large deformations.
    ([560a73610b](https://projects.blender.org/blender/blender/commit/560a73610b7))
    ([0a1fbfee2b](https://projects.blender.org/blender/blender/commit/0a1fbfee2ba))

![Clay strips 2.83/2.90 artifacts
comparison](../../videos/Demo_clay_strips_290.mp4
"Clay strips 2.83/2.90 artifacts comparison")

  - The Topology Slide/Relax mode now has two more deformation modes for
    Slide: Pinch and Expand. They can be selected using the Deformation
    property.
    ([878d191bae](https://projects.blender.org/blender/blender/commit/878d191baee))

## Miscellaneous

  - The Automasking system was redesigned, so brush will not have and
    start delay when automasking options are enabled in a high poly
    mesh.
    ([e06a346458](https://projects.blender.org/blender/blender/commit/e06a346458f))

<!-- end list -->

  - Boundary mesh detection was redesigned. Now mesh boundaries are
    properly detected by the automasking system in both meshes and
    Multires levels. This means that the smooth brush does not longer
    automasks boundary vertices by default and its meshes and multires
    behavior now matches.
    ([e06a346458](https://projects.blender.org/blender/blender/commit/e06a346458f))

<!-- end list -->

  - Pen pressure modulation is now supported for the hardness property.
    The modulation can be inverted for achieving higher hardness with
    lower pressure.
    ([69afdf6970](https://projects.blender.org/blender/blender/commit/69afdf69702))
