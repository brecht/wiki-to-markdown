# Blender 2.90 Release Notes

Blender 2.90 was released on August 31, 2020.

Check out the final [release notes on
blender.org](https://www.blender.org/download/releases/2-90/). List of
[corrective
releases](Corrective_Releases.md).

## [User Interface](User_Interface.md)

## [Modeling](Modeling.md)

## [Sculpt](Sculpt.md)

## [Grease Pencil](Grease_Pencil.md)

## [Cycles](Cycles.md)

## [EEVEE](EEVEE.md)

## [Import & Export](IO.md)

## [Python API](Python_API.md)

## [Physics](Physics.md)

## [Animation & Rigging](Animation-Rigging.md)

## [Virtual Reality](Virtual_Reality.md)

## [More Features](More_Features.md)

## [Add-ons](Add-ons.md)

## Compatibility

  - The minimum required macOS version was changed from 10.12 Sierra to
    10.13 High Sierra. Apple extended support for Sierra ended in
    October 2019.

## [Corrective Releases](Corrective_Releases.md)
