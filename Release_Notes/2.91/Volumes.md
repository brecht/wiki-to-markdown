# Blender 2.91: Volume Object

## Modifiers

Volume objects now have initial support for modifiers, opening new
possibilities for procedural modeling.

  - Volume Displace modifier: displace a volume using a procedural
    texture. This may be used to add detail or patterns to a volume.
    ([1f50beb9f2](https://projects.blender.org/blender/blender/commit/1f50beb9f28edd)).
  - Mesh to Volume modifier: uses a mesh to generate a volume, either
    filling the mesh interior or on the mesh boundary. Together with a
    Volume Displace modifier, this can for example be used to module
    clouds using a mesh as the base shape.
    ([5845c06a63](https://projects.blender.org/blender/blender/commit/5845c06a63a6)).
  - Volume to Mesh modifier: generate a mesh from a volume object. This
    may for example be used to perform volumetric displacement on a
    mesh, by converting a mesh to a volume, applying displacement, and
    converting back.
    ([f7832b1583](https://projects.blender.org/blender/blender/commit/f7832b1583cd)).

Converting between volumes and meshes currently requires multiple
objects in the scene. We recommend parenting the source object to the
object with the modifiers. This ensures the generated volume or mesh
follows along when transforming the object with the modifier.

<center>

|                                                                                                                                              |                                                                                                |
| -------------------------------------------------------------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------- |
| ![Volume displacement with a wave texture](../../images/Volume_to_mesh_and_displace_modifiers.jpg "Volume displacement with a wave texture") | ![Volume to mesh modifier](../../images/Volume_to_Mesh_modifier.png "Volume to mesh modifier") |

</center>

## Viewport

  - Volume objects can now be selected in the viewport, and display a
    selection outline when selected.
    ([e12767a035](https://projects.blender.org/blender/blender/commit/e12767a0352a9e)).
  - Scene simplify settings now have a Volume Resolution setting in the
    viewport. This reduces the resolution of all volume objects in the
    viewport, both those read from .vdb files and dynamically generated
    using the mesh to volume modifier.
    ([365bf103d1](https://projects.blender.org/blender/blender/commit/365bf103d1f66),
    [63a9f24b55](https://projects.blender.org/blender/blender/commit/63a9f24b55d0b5)).
