# Blender 2.91: Modeling

## Boolean

The Boolean modifier and Boolean tool have a new *Exact* solver option
that handles overlapping geometry better
([9e09b5c418](https://projects.blender.org/blender/blender/commit/9e09b5c418c0)).

![The Fast (old) mode](../../videos/Boolean_modifier_fast.mp4
"The Fast (old) mode") ![The Exact (new)
mode](../../videos/Boolean_modifier_exact.mp4 "The Exact (new) mode")

The old BMesh solver remains as the *Fast* option, as it is still quite
a bit faster. The Exact solver uses exact multi-precision arithmetic and
a new algorithm (taken from [Mesh Arrangements for Solid
Geometry](http://www.cs.columbia.edu/cg/mesh-arrangements/mesh-arrangements-for-solid-geometry-siggraph-2016-zhou-et-al.pdf),
by Zhou, Grinspun, Zorin, and Jacobson). Together these mean that:

  -   - Overlapping geometry - coplanar faces, overlapping edges - is
        handled properly.
      - There should be no issues (other than speed) when intersecting
        very dense meshes.
      - Confusion about what is *inside* and what is *outside*, which
        determines what parts survive and what parts are deleted in a
        Boolean, should be eliminated. At least, in the case where the
        input meshes describe closed volumes.
      - A new *Self* option for the *Exact* solver will properly handle
        cases when one or both operands have self-intersections
        ([de21ab418d](https://projects.blender.org/blender/blender/commit/de21ab418d69)).

<!-- end list -->

  - More examples, showing difference with coplanar faces:

![The Fast (old) mode](../../videos/Boolean_modifier_coplanar_fast.mp4
"The Fast (old) mode") ![The Exact (new)
mode](../../videos/Boolean_modifier_coplanar_exact.mp4
"The Exact (new) mode")

  - The Boolean modifier can now take a Collection as an alternative to
    an Object as the "other operand" of the Boolean
    ([ab7608af1b](https://projects.blender.org/blender/blender/commit/ab7608af1bd4)).
    To use it, change the *Operand Type* from *Object* to *Collection*,
    and then pick a Collection name. With the *Fast* solver, this will
    just apply the Boolean as many times as there are Mesh Objects in
    the Collection. With the *Exact* solver, the whole Boolean is done
    in one operation. Also, the new *Self* option described above is
    always on for the *Exact* solver when the operand type is
    *Collection*, and you can leave the collection empty to just get
    self-intersections removed. Due to the way the *Fast* method is
    implemented, it will not work for an *Intersect* operation, but the
    *Exact* solver can do an *Intersect* operation.

![The Boolean modifier in 2.91 with the new "Exact" solver and
collection operand type](../../images/Boolean_Modifier_2.91.png
"The Boolean modifier in 2.91 with the new \"Exact\" solver and collection operand type")

## Tools

  - Intersect (Knife) and Intersect (Boolean): a new *Exact* solver
    option handles overlapping geometry better (see update on Boolean
    for more details).
    ([9e09b5c418](https://projects.blender.org/blender/blender/commit/9e09b5c418c0))

## UV Editor

  - Loop select at boundaries now supports cycling between partial and
    full boundaries, matching the behavior for mesh-editing
    ([6a10e69d27](https://projects.blender.org/blender/blender/commit/6a10e69d270bc6866dd0000ffc0b81f6205ba2e9)).

## Modifiers

  - The Mesh Sequence Cache modifier can now import Alembic velocities
    for rendering motion blur
    ([b5dcf74636](https://projects.blender.org/blender/blender/commit/b5dcf746369)).
    This is controlled by 3 new properties on the modifier : Velocity
    Attribute, Velocity Unit, and Velocity Scale. The first two are
    global for the Alembic cache, so every object imported will use
    those same properties for resolving the velocity attribute.

![Visualization of the new controls for importing velocity
data.](../../images/Abc_velocities_modifier_highlight.png
"Visualization of the new controls for importing velocity data.")

  - The ocean modifier now has separate values for viewport and render
    resolution
    ([a44299ccd1](https://projects.blender.org/blender/blender/commit/a44299ccd117)).

![../../images/Ocean\_resolution\_settings.png](../../images/Ocean_resolution_settings.png
"../../images/Ocean_resolution_settings.png")

![Ocean modifier resolution viewport vs
render](../../images/Ocean_resolution_smaller.jpg
"Ocean modifier resolution viewport vs render")

  - The Vertex Weight Proximity modifier now can use a custom mapping
    curve to control its falloff
    ([D9594](http://developer.blender.org/D9594),
    [rBe4204a39](https://projects.blender.org/blender/blender/commit/e4204a3979c4)).

## Subdivision Surfaces

For compatibility with other software, a few options were added to the
Subdivision Surface modifier.
([53f20b9](https://projects.blender.org/blender/blender/commit/53f20b9),
[6070f92](https://projects.blender.org/blender/blender/commit/6070f92),
[443e42d](https://projects.blender.org/blender/blender/commit/443e42d))

  - Option to match iterative subdivision and shrinking, by disabling
    the limit surface.
  - Option to smooth only boundary edges and keep boundary corners
    sharp.
  - Option to smooth entire boundary for UV coordinates, also including
    corners.

Boundary and UV smooth options are also available in the Multiresolution
modifier.

## Curves

  - Allow building flat curve caps for all bevel types
    ([a6a0cbcd74](https://projects.blender.org/blender/blender/commit/a6a0cbcd7492)).

![Fill caps on curve object](../../images/Fill_caps.png
"Fill caps on curve object")

  - Bevels on curves (and text and 2D curves) now have support for the
    same custom profiles as the bevel modifier. The new "Bevel Mode"
    property decides which type of bevel to build, with the "Profile"
    option for a quarter circle custom profile.
    ([60fa80de0b](https://projects.blender.org/blender/blender/commit/60fa80de0b2c))

![../../images/Mirror\_image\_01.png](../../images/Mirror_image_01.png
"../../images/Mirror_image_01.png")

![Curve bevel profile](../../videos/Custom_curve_bevel_profile_.mp4
"Curve bevel profile")
