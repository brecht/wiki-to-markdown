# Blender 2.91: Animation & Rigging

## FCurves

  - Animation curves can now become much snappier, allowing for more
    sudden changes with fewer keyframes required
    ([da95d1d851](https://projects.blender.org/blender/blender/commit/da95d1d851b)).

![../../images/release-notes-291-animation-fcurve-extremes.png](../../images/release-notes-291-animation-fcurve-extremes.png
"../../images/release-notes-291-animation-fcurve-extremes.png")
![Snappier, more extreme
FCurves](../../videos/release-notes-291-animation-fcurve-extremes.mp4
"Snappier, more extreme FCurves")

  - All Keyframes types can now be inserted without changing the FCurve
    shape
    ([8b72d9cc15](https://projects.blender.org/blender/blender/commit/8b72d9cc1530)).

![Insert keyframes without changing the curve shape. The blue line in
the top image is the original FCurve before the middle key was
inserted.](../../images/release-notes-291-animation-fcurve-insert-keyframes.png
"Insert keyframes without changing the curve shape. The blue line in the top image is the original FCurve before the middle key was inserted.")

  - FCurves in the keyframe now have an active keyframe, similar to the
    active vertex in object edit mode. This makes it much easier to tell
    which keyframe the "Active Keyframe" panel corresponds to
    ([983ad4210b](https://projects.blender.org/blender/blender/commit/983ad4210b9e)).

![The active keyframe is drawn in
white](../../images/Graph_Editor_Active_Keyframe.png
"The active keyframe is drawn in white")

  - The Snap menu in the graph editor now can snap the full 2D Cursor
    and just the cursor value
    ([5ebdbcafcb](https://projects.blender.org/blender/blender/commit/5ebdbcafcb27)).

![Graph Editor Snap Pie
Menu](../../images/release-notes-291-animation-graph-editor-snap.png
"Graph Editor Snap Pie Menu")

## Keyframes

  - "Copy To Selected" support for keyframes
    ([rB0a66436f](https://projects.blender.org/blender/blender/commit/0a66436fe5f2)):

![../../videos/Keyframe\_Copy\_To\_Selected.mp4](../../videos/Keyframe_Copy_To_Selected.mp4
"../../videos/Keyframe_Copy_To_Selected.mp4")

## Non-Linear Animation (NLA) Editor

  - "Sync Length" now no longer moves animation keys around
    ([50919413fe](https://projects.blender.org/blender/blender/commit/50919413fe7)).

![Move keyframes then sync strip length. New version preserves global
frame timing.](../../videos/Sync_move_keys.mp4
"Move keyframes then sync strip length. New version preserves global frame timing.")

  - The NLA system now evaluates the strip based on the action bounds if
    it's flagged for syncing
    ([89ee260ef2](https://projects.blender.org/blender/blender/commit/89ee260ef22)).
    Now the animator can freely insert and modify keys outside of the
    strip bounds. They will never have to touch the strip bounds
    directly. Changing the evaluation bounds is a simple as moving keys
    around.

![Ability to keyframe outside strips that have Sync Length toggle
active.](../../videos/Keyframe_outside_strip_bounds.mp4
"Ability to keyframe outside strips that have Sync Length toggle active.")

  - Non-active strips no longer gets hidden when entering edit mode
    ([d067c13a9d](https://projects.blender.org/blender/blender/commit/d067c13a9d07)):

![Deactivated upper strip, "noise", still shown while in tweak
mode.](../../images/Show_All_Strips1.png
"Deactivated upper strip, \"noise\", still shown while in tweak mode.")

## Constraints

  - The Child Of constraint now sets the inverse matrix when it's
    created
    ([ad70d4b095](https://projects.blender.org/blender/blender/commit/ad70d4b0956f5f)).
    This means that the object no longer jumps around when the Child Of
    constraint is added.

<!-- end list -->

  - Add an optional Evaluation Time slide to the Action Constraints.
    This makes it possible to specify the playback position of the
    action without using any objects
    ([c9c0f89307](https://projects.blender.org/blender/blender/commit/c9c0f8930733)):

![../../images/Action\_constraint\_slider.png](../../images/Action_constraint_slider.png
"../../images/Action_constraint_slider.png")

## Playback

  - If using the "Allow Negative Frames" option, The Playback "Set Start
    Frame" / "Set Start Frame" operators allowed for setting a negative
    rendering range, which is not supported. This was corrected in
    [ca38bce46a](https://projects.blender.org/blender/blender/commit/ca38bce46aa2).
    A negative range can still be used if "Use Preview Range" option is
    used.
