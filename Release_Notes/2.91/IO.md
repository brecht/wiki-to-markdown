# Blender 2.91: IO & Overrides

## Alembic & USD

### Alembic & USD Export: Invisible Objects

The Alembic and USD exporters now support **exporting invisible
objects** (Alembic:
[a95f863596](https://projects.blender.org/blender/blender/commit/a95f8635967),
USD:
[108f3284a7](https://projects.blender.org/blender/blender/commit/108f3284a73)).
Both exporters now write a 'visibility' property that reflects whether
the object is visible or invisible. Note that this uses the [Global
Viewport Visibility (USD) and Render Visibility (USD &
Alembic)](https://docs.blender.org/manual/en/dev/editors/outliner.html#restriction-columns)
properties, and not the 3D Viewport visibility. The location of the
written property in the exported file is different for Alembic and USD,
due to file format differences:

  - Alembic supports values "hidden", "inherited", and "visible".
    Blender will only write "hidden" or "visible", depending on the
    visibility of the object in render mode. The property is written to
    the XForm in the Alembic file.
  - USD only supports values "hidden" and "inherited"; children of a
    hidden object are always also hidden. Because Blender does support
    hiding a parent without hiding the children, the visibility property
    is written to the geometry node in the USD file, and not to the
    transform node.

### Alembic Export: Instances

The Alembic exporter is now writing duplicated object data as **Alembic
instances**
([b3759cc0d6](https://projects.blender.org/blender/blender/commit/b3759cc0d67)).
This is used when the objects are instances, for example when duplicated
by a particle system or instanced by the duplication system
(collection-duplicating empties, vertex/face duplis, etc.). This
instancing support is still limited, in the sense that only object data
is instanced and all transforms are still written explicitly. Blender's
Alembic importer has no understanding of these Alembic instances yet,
and will thus happily duplicate the data on import. [USD's Alembic
plugin](https://graphics.pixar.com/usd/docs/Alembic-USD-Plugin.html)
seems to have problems understanding the instancing, and there might
also be other software with similar issues. Because of this, instancing
can be turned off in the exporter (it's on by default).

### Alembic Export: Custom Properties

The Alembic exporter now can optionally export Blender's **custom
properties**
([ee97add4c4](https://projects.blender.org/blender/blender/commit/ee97add4c40)).
The export of custom properties is enabled by default, and can be
disabled from the export options. The following custom property types
are supported:

  - Numbers (\`int\`, \`float\`) and strings. These are exported as
    arrays of a single element, so \`47\` will be exported as \`\[47\]\`
    to Alembic, and \`"Agent"\` to \`\["Agent"\]\`. This matches the
    behaviour of many other DCCs.
  - Lists of numbers and strings. These are exported as-is, so \`\[327,
    47\]\` is exported as \`\[327, 47\]\`.
  - Matrices and nested arrays of numbers. These are flattened into one
    long list, so a 3x2 matrix of numbers will become a list of 6
    numbers. Similarly, nested lists \`\[\[1, 2, 3\], \[4, 5\],
    \[6\]\]\` will be exported as \`\[1, 2, 3, 4, 5, 6\]\`.
  - Numbers can be animated as well.

Here is an example file that contains the above, both animated and
non-animated:
![../../images/Blender2.91\_alembic\_custom\_properties.abc.zip](../../images/Blender2.91_alembic_custom_properties.abc.zip
"../../images/Blender2.91_alembic_custom_properties.abc.zip")

### Alembic Import: Vertex Interpolation

![../../images/Blender2.91\_alembic\_import\_vertex\_interpolation.png](../../images/Blender2.91_alembic_import_vertex_interpolation.png
"../../images/Blender2.91_alembic_import_vertex_interpolation.png")

Alembic stores mesh samples at specific time keys; when a frame in
Blender maps to a timecode between two samples, Blender will interpolate
the mesh vertex positions. This interpolation only happens when the mesh
has a constant topology, but sometimes this was not detected properly
when the vertices change order, but the number of mesh elements remains
the same. This would result in a mesh with jumbled up vertices
([\#71981](http://developer.blender.org/T71981)). It is now possible to
**disable vertex interpolation** per object.

### Alembic Import: Animated Vertex Colors on static mesh

If the mesh was constant, no check was done if there were animated
vertex colors and thus creation of a MeshSequenceCache modifier was
skipped, this is now supported
([\#81330](http://developer.blender.org/T81330),
[rB449e6124](https://projects.blender.org/blender/blender/commit/449e6124b5f7))

## glTF

### Importer

  - Manage KHR\_materials\_unlit extension
    ([rBA0dca80f](https://projects.blender.org/blender/blender-addons/commit/0dca80fdc4e8))
  - Permit missing texture.source
    ([rBAf969687](https://projects.blender.org/blender/blender-addons/commit/f9696870783d))
  - Select all (and only) the imported objects
    ([rBAee912f6](https://projects.blender.org/blender/blender-addons/commit/ee912f6fd22b))
  - Convert glTF meters to the units of the Blender scene
    ([rBAfc7cf58](https://projects.blender.org/blender/blender-addons/commit/fc7cf5847a37))
  - Import grayscale emissiveFactor as Emission Strength (new principled
    socket)
    ([rBAfeca8c5](https://projects.blender.org/blender/blender-addons/commit/feca8c528979))
  - Use bpy.utils.escape\_identifier to escape names for action paths
    ([rBA6cef5e0](https://projects.blender.org/blender/blender-addons/commit/6cef5e013e54))
  - Fix for material\_index/material slots
    ([rBA77aada8](https://projects.blender.org/blender/blender-addons/commit/77aada8057d9))
  - Restore setting extra to mesh custom properties
    ([rBA9e7404c](https://projects.blender.org/blender/blender-addons/commit/9e7404ce614d))
  - Better error messages when loading files
    ([rBAf713ed8](https://projects.blender.org/blender/blender-addons/commit/f713ed806347))
  - Avoid traceback on non valid file with empty primitive tab
    ([rBAa927d3e](https://projects.blender.org/blender/blender-addons/commit/a927d3e1bca8))
  - Handle limit for vertex color and uv layers in importer
    ([rBAa0e8b8b](https://projects.blender.org/blender/blender-addons/commit/a0e8b8b7875b))
  - Refactoring
    ([rBA5ebb907](https://projects.blender.org/blender/blender-addons/commit/5ebb907bf7e2),
    [rBAb0b94bf](https://projects.blender.org/blender/blender-addons/commit/b0b94bf49e8b))
  - Performance
    ([rBA78be556](https://projects.blender.org/blender/blender-addons/commit/78be5568d974),
    [rBA5118d31](https://projects.blender.org/blender/blender-addons/commit/5118d312b7a5),
    [rBA0c2ff8f](https://projects.blender.org/blender/blender-addons/commit/0c2ff8fa7c72),
    [rBAf8e313f](https://projects.blender.org/blender/blender-addons/commit/f8e313f62677))

### Exporter

  - Manage KHR\_materials\_unlit extension
    ([rBA0dca80f](https://projects.blender.org/blender/blender-addons/commit/0dca80fdc4e8),
    [rBA1f04368](https://projects.blender.org/blender/blender-addons/commit/1f043682f956))
  - Always export texture samplers
    ([rBA9867270](https://projects.blender.org/blender/blender-addons/commit/98672709fc4d))
  - Allow user extension in asset
    ([rBA1096320](https://projects.blender.org/blender/blender-addons/commit/109632093bd5))
  - Materials: export factors from MULTIPLY nodes
    ([rBA3a5aaa9](https://projects.blender.org/blender/blender-addons/commit/3a5aaa9b9999))
  - Detect when a file will be overwritten
    ([rBA4c4502f](https://projects.blender.org/blender/blender-addons/commit/4c4502ff1c8f))
  - Ignore muted node when checking tree
    ([rBAd7d2bad](https://projects.blender.org/blender/blender-addons/commit/d7d2bad09f8c))
  - Manage basic color when shader tree is not used
    ([rBA59787f0](https://projects.blender.org/blender/blender-addons/commit/59787f0f2f74))
  - Manage emission strength in materials
    ([rBAf3cf3a1](https://projects.blender.org/blender/blender-addons/commit/f3cf3a16e989))
  - Avoid zero normals when degenerate tris
    ([rBA051770b](https://projects.blender.org/blender/blender-addons/commit/051770b36de9))
  - Fix UVMap export when ORM are using different maps
    ([rBA9db2451](https://projects.blender.org/blender/blender-addons/commit/9db2451888a5))
  - Option to export placeholder of materials
    ([rBA6541699](https://projects.blender.org/blender/blender-addons/commit/654169969408))
  - Don't mess with normals when doing Apply Modifiers
    ([rBAd3100f5](https://projects.blender.org/blender/blender-addons/commit/d3100f5f71ce))
  - Fix for material\_index/material slots
    ([rBA77aada8](https://projects.blender.org/blender/blender-addons/commit/77aada8057d9))
  - Filtering of exported nodes
    ([rBA33eae7d](https://projects.blender.org/blender/blender-addons/commit/33eae7da675d))
  - Refactoring
    ([rBAd37be8f](https://projects.blender.org/blender/blender-addons/commit/d37be8f89230))
  - Performance
    ([rBA566a68e](https://projects.blender.org/blender/blender-addons/commit/566a68e2083a),
    [rBA495dd7b](https://projects.blender.org/blender/blender-addons/commit/495dd7bd07bd))

## Library Overrides

  - 'Resync' operation was added to the Outliner (to re-generate an
    override when relationships between IDs are changed in library
    .blend file),
    [rBaeaf2b0d](https://projects.blender.org/blender/blender/commit/aeaf2b0dd437f1a).
  - 'Delete' and re-link to linked data operation was added to the
    Outliner (to fully get rid of an override hierarchy and relink its
    usages to the lib-linked data-blocks instead),
    [rB4aa04b64](https://projects.blender.org/blender/blender/commit/4aa04b64904b).
  - Support insertion of items into py-defined IDProp-based RNA
    collections ([\#79562](http://developer.blender.org/T79562),
    [rBdb314ee7](https://projects.blender.org/blender/blender/commit/db314ee7a472),
    [rB9756c667](https://projects.blender.org/blender/blender/commit/9756c6672046),
    [rB86c5d1f4](https://projects.blender.org/blender/blender/commit/86c5d1f4aa07)).
  - An operator was added to convert proxies to library overrides
    ([rBc3a0618f](https://projects.blender.org/blender/blender/commit/c3a0618fbfff)).
  - More data has been made overridable:
      - Armature data.

## Library Linking

  - Link and Append now create object instances when linking object-data
    directly such as meshes, curves... etc.
    [rB748deced](https://projects.blender.org/blender/blender/commit/748deced1c7b)

## Handling of shading in add-ons importers

These changes affect potentially all add-ons using the node shader
wrapper featured by Blender to help conversion between old 'fixed'
shading type and modern nodal one (including OBJ and FBX IO).

  - Textures used as transparency input are now linked to the BSDF
    shader through their Alpha output, and not their Color output
    anymore
    ([rB0696eaa3](https://projects.blender.org/blender/blender/commit/0696eaa3e84e93)).

## File Loading

  - Faster loading of large old files
    ([rB6374644f](https://projects.blender.org/blender/blender/commit/6374644fd117)).
