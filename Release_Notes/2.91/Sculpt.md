# Blender 2.91: Sculpting

## Sculpting Workflow with multiple objects

  - XYZ symmetry is now a per mesh settings which is shared between all
    painting and editing modes.
    [5502517c3c](https://projects.blender.org/blender/blender/commit/5502517c3c1)
  - Fade Inactive Geometry Overlay that fades with the background color
    objects that are not editable in the current mode. This makes
    possible to identify which object is enabled for sculpting.
    [ea6cd1c8f0](https://projects.blender.org/blender/blender/commit/ea6cd1c8f05)

## Viewport

  - The HDRI rotation can be locked to the view in LookDev mode to use
    EEVEE materials as Matcaps
    [5855f317a7](https://projects.blender.org/blender/blender/commit/5855f317a70)

![../../videos/Eevee\_lock\_rotation.mp4](../../videos/Eevee_lock_rotation.mp4
"../../videos/Eevee_lock_rotation.mp4")

  - Sculpt Mode can now renders its overlays (Face Sets and Masks) in
    objects with constructive modifiers enabled.
    [6c9ec1c893](https://projects.blender.org/blender/blender/commit/6c9ec1c893f)

## Sculpt Gestures

New gesture tools to manipulate different data in the sculpt mesh.

  - Box and Lasso Face Set create a new Face Set in the area affected by
    the gesture
    [c05715b03f](https://projects.blender.org/blender/blender/commit/c05715b03fe).
  - Box and Lasso Trim cut the mesh using a boolean operation
    [675c964442](https://projects.blender.org/blender/blender/commit/675c9644420).
  - Box mask its now its own operator, separated from selection
    [6faa765af8](https://projects.blender.org/blender/blender/commit/6faa765af89).
  - Sculpt: Line Project Gesture tool
    [e0bfd3968c](https://projects.blender.org/blender/blender/commit/e0bfd3968c6).
  - Sculpt: Line gestures and Mask Line tool
    [8c81b3fb8b](https://projects.blender.org/blender/blender/commit/8c81b3fb8b9).
  - Sculpt: Union and Join mode for trim tools
    [2b72860ff4](https://projects.blender.org/blender/blender/commit/2b72860ff44).
  - Sculpt: Use cursor depth in trimming gestures
    [da7ace00d5](https://projects.blender.org/blender/blender/commit/da7ace00d5f).

## Cloth Sculpting

  - The Cloth Brush and Filter now support collisions with scene
    colliders
    [675700d948](https://projects.blender.org/blender/blender/commit/675700d9489).

![../../videos/Cloth\_Collisions.mp4](../../videos/Cloth_Collisions.mp4
"../../videos/Cloth_Collisions.mp4")

  - Cloth Brush applies gravity in the entire simulated area
    [54a2fcc0f3](https://projects.blender.org/blender/blender/commit/54a2fcc0f33).
  - Sculpt: Cloth Brush Soft Body Plasticity property
    [ec9edd36ca](https://projects.blender.org/blender/blender/commit/ec9edd36ca1).

![../../videos/Plasticity.mov](../../videos/Plasticity.mov
"../../videos/Plasticity.mov")

  - Sculpt: Enable persistent base for the cloth brush
    [49c1359b55](https://projects.blender.org/blender/blender/commit/49c1359b556).
  - New Cloth Grab Brush that uses constraints
    [2e33c5ca15](https://projects.blender.org/blender/blender/commit/2e33c5ca15c).
  - New Cloth Snake Hook Brush
    [2e33c5ca15](https://projects.blender.org/blender/blender/commit/2e33c5ca15c).

![../../videos/Cloth\_Snake\_Hook.mp4](../../videos/Cloth_Snake_Hook.mp4
"../../videos/Cloth_Snake_Hook.mp4")

  - Sculpt: Cloth Brush simulation area property
    [d693d77fed](https://projects.blender.org/blender/blender/commit/d693d77fed9).
  - Sculpt: Cloth Simulation Dynamic area mode
    [8ef353fa50](https://projects.blender.org/blender/blender/commit/8ef353fa506).

![../../videos/Dynamic\_simulation\_area.mp4](../../videos/Dynamic_simulation_area.mp4
"../../videos/Dynamic_simulation_area.mp4")

  - Cloth brush Pin Simulation Boundary property
    [bf65820782](https://projects.blender.org/blender/blender/commit/bf65820782a).
  - Sculpt: Option to limit the forces axis in the Cloth Filter
    [4814836120](https://projects.blender.org/blender/blender/commit/4814836120e).
  - Sculpt: Enable Cloth Simulation Target for Pose and Boundary
    [c2f0522760](https://projects.blender.org/blender/blender/commit/c2f0522760b).
  - Sculpt: Add orientation modes to the Cloth Filter
    [b3bd121dd4](https://projects.blender.org/blender/blender/commit/b3bd121dd4c).
  - Sculpt: Scale Cloth Filter
    [5d728d38b9](https://projects.blender.org/blender/blender/commit/5d728d38b94).

![../../videos/Simulation\_Target.mp4](../../videos/Simulation_Target.mp4
"../../videos/Simulation_Target.mp4")

## Multires

  - Multires: Base Mesh Sculpting
    [976f0113e0](https://projects.blender.org/blender/blender/commit/976f0113e00).

## Tools

### Boundary Brush

This brush includes a set of deformation modes designed to deform and
control the shape of the mesh boundaries, which are really hard to do
with regular sculpt brushes (and even in edit mode). This is useful for
creating cloth assets and hard surface base meshes.
[ed9c0464ba](https://projects.blender.org/blender/blender/commit/ed9c0464bae).
![../../videos/Boundary\_Brush.mp4](../../videos/Boundary_Brush.mp4
"../../videos/Boundary_Brush.mp4")

The boundary brush supports different modes to apply the falloff which
can be used to create repeating trim patterns
[c77bf95221](https://projects.blender.org/blender/blender/commit/c77bf952210).
![../../videos/Boundary\_brush\_falloff.mp4](../../videos/Boundary_brush_falloff.mp4
"../../videos/Boundary_brush_falloff.mp4")

### Multires Displacement Eraser

  - This brush deletes the displacement of the Multires Modifier and
    moves the vertices towards the base mesh limit surface.
    [478ea4c898](https://projects.blender.org/blender/blender/commit/478ea4c8988).

![../../videos/Displacement\_eraser.mp4](../../videos/Displacement_eraser.mp4
"../../videos/Displacement_eraser.mp4")

### Sculpt Filters

  - Sculpt: Sharpen Mesh Filter curvature smoothing and intensify
    details
    [3570173d0f](https://projects.blender.org/blender/blender/commit/3570173d0f5).
  - Sculpt: Sculpt Filter Orientation Options
    [89a374cd96](https://projects.blender.org/blender/blender/commit/89a374cd964).
  - Sculpt: Enhance Details Mesh Filter
    [0957189d4a](https://projects.blender.org/blender/blender/commit/0957189d4a3).
  - Sculpt: Erase Displacement Mesh Filter
    [bedd6f90ca](https://projects.blender.org/blender/blender/commit/bedd6f90ca7).

### Other Tools

  - Sculpt: Add global automasking settings support in filters
    [6fe3521481](https://projects.blender.org/blender/blender/commit/6fe3521481b).

<!-- end list -->

  - Sculpt: Invert Smooth to Enhance Details
    [3e5431fdf4](https://projects.blender.org/blender/blender/commit/3e5431fdf43).

![../../videos/Enhance\_details.mp4](../../videos/Enhance_details.mp4
"../../videos/Enhance_details.mp4")

  - Sculpt: Option to mask front faces only using Lasso and Box Mask
    [af77bf1f0f](https://projects.blender.org/blender/blender/commit/af77bf1f0f9).
  - Sculpt: Option to lock the rotation in the Pose Brush scale deform
    mode
    [9ea77f5232](https://projects.blender.org/blender/blender/commit/9ea77f5232c).
  - Sculpt: Face Set Extract Operator
    [afb43b881c](https://projects.blender.org/blender/blender/commit/afb43b881cc).
  - Edit Face Set is now a tool with all options exposed in the UI
    [77e40708c2](https://projects.blender.org/blender/blender/commit/77e40708c2c)
    [48c0a82f7a](https://projects.blender.org/blender/blender/commit/48c0a82f7a3).
  - Sculpt: Experimental Pen Tilt Support
    [0d5ec990a9](https://projects.blender.org/blender/blender/commit/0d5ec990a98).
