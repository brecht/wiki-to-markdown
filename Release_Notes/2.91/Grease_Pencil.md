# Blender 2.91: Grease Pencil

## User Interface

  - Renamed Boundary Fill \`Default\` to \`All\`.
    ([fff12be945](https://projects.blender.org/blender/blender/commit/fff12be9456c))
  - Boundary strokes are now visible, except in render mode. This was
    removed by error in the refactor.
    ([ebf5ff8afb](https://projects.blender.org/blender/blender/commit/ebf5ff8afb9a))
  - Brush Advanced panel now has \`Ignore Transparent\` and
    \`Threshold\` parameters related.
    ([9e644b98a6](https://projects.blender.org/blender/blender/commit/9e644b98a6f7))
  - Set Onion Keyframe mode to \`All\` by default. Also renamed option
    from \`All Types\` to \`All\`.
    ([3e56dd8fd9](https://projects.blender.org/blender/blender/commit/3e56dd8fd921))
  - Replace Interpolate shortcut to \`Ctrl+E\` instead of
    \`Ctrl+Alt+E\`.
    ([ee49ce482a](https://projects.blender.org/blender/blender/commit/ee49ce482a79))
  - New Subdivision parameter for Primitives in Topbar.
    ([90baead979](https://projects.blender.org/blender/blender/commit/90baead9792b))

## Operators

  - New Cleanup Frames operator.
    ([382b9007f8](https://projects.blender.org/blender/blender/commit/382b9007f8f3))
  - New Step parameter for Interpolate Sequence.
    ([97871e16ff](https://projects.blender.org/blender/blender/commit/97871e16ff45))
  - The convert mesh to strokes and bake mesh animation, now create
    layers and materials using the original object name. Also fixed
    problems when convert or bake animation for several objects at the
    same time.
    ([256b59b76f](https://projects.blender.org/blender/blender/commit/256b59b76f0a))
  - New material option to set as Holdout. This allows to open holes in
    filled areas. This option only works when the stroke uses Material
    mode.
    ([0335c1fd21](https://projects.blender.org/blender/blender/commit/0335c1fd21dd))

![Holdout Material](../../videos/Gpencil_holdout.mp4 "Holdout Material")

  - New Trace Images operator using Potrace library.
    ([4d62bb8fe5](https://projects.blender.org/blender/blender/commit/4d62bb8fe57c))

![Trace Image](../../videos/TraceGP.mp4 "Trace Image")

This functionality is designed to trace a black and white image into
grease pencil strokes. If the image is not B/W, the trace try to convert
to bitone image. For better results, use manually converted to black and
white images and try to keep the resolution of the image small. High
resolutions could produce very dense strokes.

## Tools

  - Draw Brushes: New predefined mode.
    ([7becd283cc](https://projects.blender.org/blender/blender/commit/7becd283cc27))
  - Fill Brush: New Layer modes.
    ([8fc7c3539a](https://projects.blender.org/blender/blender/commit/8fc7c3539ade))
  - Fill Brush: Fill inverted pressing \`Ctrl\` key. Also added new
    direction buttons for Fill in topbar.
    ([4ada290956](https://projects.blender.org/blender/blender/commit/4ada2909566c))

## Modifiers and VFX

  - Offset modifier scale thickness when modify scale.
    ([a0b0a47d81](https://projects.blender.org/blender/blender/commit/a0b0a47d8104))
