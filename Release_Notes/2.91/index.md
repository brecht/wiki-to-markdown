# Blender 2.91 Release Notes

Blender 2.91 was released on November 25, 2020.

Check out the final [release notes on
blender.org](https://www.blender.org/download/releases/2-91/).

## [User Interface](User_Interface.md)

## [Modeling](Modeling.md)

## [Sculpt](Sculpt.md)

## [Grease Pencil](Grease_Pencil.md)

## [Volume Object](Volumes.md)

## [EEVEE](EEVEE.md)

## [I/O & Overrides](IO.md)

## [Python API](Python_API.md)

## [Physics](Physics.md)

## [Animation & Rigging](Animation-Rigging.md)

## [Add-ons](Add-ons.md)

## [More Features](More_Features.md)
