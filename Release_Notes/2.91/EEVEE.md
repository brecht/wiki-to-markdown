# Blender 2.91: EEVEE

## Multi-Scatter GGX

The principle BSDF and Glossy BSDF nodes have been improved to support a
multi-scattering approximation, removing the energy loss on rough
surfaces.

![Rough metals with GGX (top) and GGX with multi-scattering
approximation (bottom) ](../../images/Eevee_ggx_multiscatter.png
"Rough metals with GGX (top) and GGX with multi-scattering approximation (bottom) ")

## Motion Blur

Shutter start position can now be adjusted to make motion trail effects
just like Cycles.
[b7afcdff7b](https://projects.blender.org/blender/blender/commit/b7afcdff7b06)

## Bug Fixes

  - Screen Space Reflection now are more robust with less self
    intersections for rough reflections and less gaps at intersecting
    geometries.
    [b55c44e3ed](https://projects.blender.org/blender/blender/commit/b55c44e3eda0)
    [27137c479c](https://projects.blender.org/blender/blender/commit/27137c479cc3)
  - Fixed Camera Motion Blur is not blending steps properly
    [aae60f0fec](https://projects.blender.org/blender/blender/commit/aae60f0fecf).
  - Fixed light probe baking ignoring indirect bounces from SSS
    [568dc2665e](https://projects.blender.org/blender/blender/commit/568dc2665e8).
  - Fixed Alpha Clip shadows actually using Alpha Hashed shadows
    [96e8dadda0](https://projects.blender.org/blender/blender/commit/96e8dadda0b).
  - Fixed Environmental light leak with baked indirect lighting and
    refraction
    [dede4aac5b](https://projects.blender.org/blender/blender/commit/dede4aac5be9).
