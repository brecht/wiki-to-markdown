# Blender 2.91: User Interface

## Outliner

  - Collection colors: object collections now have a color tag, set from
    the right-click menu.
    ([33d7b36cec](https://projects.blender.org/blender/blender/commit/33d7b36cecd4))
    ([16f625ee65](https://projects.blender.org/blender/blender/commit/16f625ee654))
    ([7b3d38a72d](https://projects.blender.org/blender/blender/commit/7b3d38a72d3))

![../../images/Outliner-colors.png](../../images/Outliner-colors.png
"../../images/Outliner-colors.png")

  - Move mode toggling to the left column. When in non-object modes,
    icon buttons are shown in the left column for toggling modes.
    Clicking a dot icon will switch that object to become the new active
    object, ctrl-click adds the object into the current mode.
    ([2110af20f5](https://projects.blender.org/blender/blender/commit/2110af20f5e6))

<!-- end list -->

  - Add left and right walk navigation. When the active element is
    expanded, walking right will select the first child element. When
    the active is closed, walking left will select the parent element.
    ([70151e41dc](https://projects.blender.org/blender/blender/commit/70151e41dc02))
  - The collection exclude checkbox was moved to the right with the
    other restriction toggles.
    ([8bce181b71](https://projects.blender.org/blender/blender/commit/8bce181b71545))
  - Use shift-click on restrict icons for setting visibility and
    selectability of bone children to be consistent with objects and
    collections.
    ([9de18c361b](https://projects.blender.org/blender/blender/commit/9de18c361bb4))
  - Drag & drop for modifiers, constraints and grease pencil effects.
    ([1572da858d](https://projects.blender.org/blender/blender/commit/1572da858df))
      - Drag within the object or bone to reorder
      - Drag to another object or bone to copy
      - Drag the parent element (Modifiers, Constraints, or Effects) to
        link all to the target object or bone.
  - Use the right-click target element to determine the type of context
    menu. For example, a right-click on a modifier will show the
    modifier menu even if objects are selected.
    ([b0741e1dcb](https://projects.blender.org/blender/blender/commit/b0741e1dcbc5))
  - Display grease pencil modifiers and shader effects in the tree
    ([2c34e09b08](https://projects.blender.org/blender/blender/commit/2c34e09b08fb))
  - Object data (can now be dropped into the 3D Viewport to create
    object instances), especially useful for orphan meshes, curves..
    etc.
    ([e56ff76db5](https://projects.blender.org/blender/blender/commit/e56ff76db5b44ae6784a26ff6daf9864b2387712))
  - Rename "Sequence" Display Mode to "Video Sequencer"
    ([af843be301](https://projects.blender.org/blender/blender/commit/af843be3012))

## Property Search

Search is now possible in the property editor, with automatically
expanding and collapsing panels. \`Ctrl-F\` starts a search, and
\`alt-F\` clears the search field.
([bedbd8655e](https://projects.blender.org/blender/blender/commit/bedbd8655ed1),
[8bcdcab659](https://projects.blender.org/blender/blender/commit/8bcdcab659fb),
[7c633686e9](https://projects.blender.org/blender/blender/commit/7c633686e995))

Tabs without search results will appear grayed out in the tab bar on the
left. If a result is not found in the current tab, the search switches
to the next one with a search result.

![Searching across all tabs in the property
editor](../../videos/Property_Search_2.91_Video.mp4
"Searching across all tabs in the property editor")

## Other

  - Masking: Update the mask menu to be more consistent with other
    editors by rearranging and adding missing operators.
    ([c598e939ad](https://projects.blender.org/blender/blender/commit/c598e939ad2)),
    ([4155d77026](https://projects.blender.org/blender/blender/commit/4155d77026d)).
  - Menus: Aesthetic tweaks to Select All by Type operator
    ([8f4f9275ce](https://projects.blender.org/blender/blender/commit/8f4f9275cea))
  - UV Editor: Add missing Unwrapping tools to UV menu
    ([4e3cb95638](https://projects.blender.org/blender/blender/commit/4e3cb956385))
  - 3D Viewport: Move Live Unwrap from UV menu to toolbar
    ([637699e78a](https://projects.blender.org/blender/blender/commit/637699e78ab))
  - Many searches now support fuzzy and prefix matching.
    ([98eb89be5d](https://projects.blender.org/blender/blender/commit/98eb89be5dd08f3))
  - Add temperature units option with support for Kelvin, Celsius and
    Fahrenheit.
    ([36aeb0ec1e](https://projects.blender.org/blender/blender/commit/36aeb0ec1e2e))
      - Note that math operations are not supported for non-Kelvin
        temperature units, and will lead to incorrect results.
  - Move curve geometry "start and end mapping" to a new subpanel.
    ([4e667ecef9](https://projects.blender.org/blender/blender/commit/4e667ecef92f))
  - Auto keyframing settings are moved to their own popover in the
    timeline header.
    ([099ce95ef3](https://projects.blender.org/blender/blender/commit/099ce95ef36d))

![../../images/Auto\_Keyframing\_Popover\_2.91.png](../../images/Auto_Keyframing_Popover_2.91.png
"../../images/Auto_Keyframing_Popover_2.91.png")

  - Option to invert filter in Dopesheet, Timeline and Curve Editor.
    ([fecb276ef7](https://projects.blender.org/blender/blender/commit/fecb276ef7b3))
  - Change the invert list filter icon to be consistent with other
    areas.
    ([459618d860](https://projects.blender.org/blender/blender/commit/459618d8606e))
  - "Reset to Default Value" in button right click menus now works in
    many more places, including modifiers and particle settings
    ([Reference](https://developer.blender.org/T80164#1034116))
  - More information is now shown in tooltips in the "Open Recent Files"
    menu: file path, size, and modification date
    ([53792e32e7](https://projects.blender.org/blender/blender/commit/53792e32e7dc)).

![../../images/Recent\_Details.png](../../images/Recent_Details.png
"../../images/Recent_Details.png")

  - Changes to "Quit" and "About" dialogs with change to monochrome
    versions of the large alert icons (and blender logo).
    ([dc71ad0624](https://projects.blender.org/blender/blender/commit/dc71ad062408)).

![../../images/Monochrome\_Alerts.png](../../images/Monochrome_Alerts.png
"../../images/Monochrome_Alerts.png")

  - Float inputs should never show the confusing value of negative zero.
    ([c0eb6faa47](https://projects.blender.org/blender/blender/commit/c0eb6faa47ec)).
  - Categorized List Menus get slight change in format with nicer
    headings.
    ([aa244a7a68](https://projects.blender.org/blender/blender/commit/aa244a7a68db)).

![../../images/Categorized\_Menu.png](../../images/Categorized_Menu.png
"../../images/Categorized_Menu.png")
