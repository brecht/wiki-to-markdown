# Nodes & Physics

## Nodes

  - The new **Split to Instances** node allows separating a geometry
    into multiple pieces based on a group ID
    ([5bee6bcedc](https://projects.blender.org/blender/blender/commit/5bee6bcedc86b7f8fa2d451186eff75a721bd62d)).
  - The new **Index Switch** node allows choosing between an arbitrary
    number of inputs with an index
    ([8d5aa6eed4](https://projects.blender.org/blender/blender/commit/8d5aa6eed4b50027002670e3e36a5d3457ade65c)).
  - The **Fill Curve** node now has a "Group ID" input to limit filling
    to specific curve groups
    ([90de0368cd](https://projects.blender.org/blender/blender/commit/90de0368cdf2bcbd8b096f6069ccaef5455b0f0c)).
  - Node tools are now supported in object mode
    ([3fcd9c9425](https://projects.blender.org/blender/blender/commit/3fcd9c94255f28c3a9d704b2dc1985682d07c419)).
  - Five nodes have been changed to use the rotation socket introduced
    in the last release
    ([600a133521](https://projects.blender.org/blender/blender/commit/600a133521813939973fb9ac4be2634de884fb17),
    [49087a84d0](https://projects.blender.org/blender/blender/commit/49087a84d02d6a6c1ebcda072d643960e3785378)).
      - *Distribute Points on Faces*
      - *Instance on Points*
      - *Rotate Instances*
      - *Transform Geometry*
      - *Object Info*
      - *Instance Rotation*
  - The **Active Camera** input node gives the scene's current camera
    object
    ([75f160ee96](https://projects.blender.org/blender/blender/commit/75f160ee96b93b7438c4e5900e93d7332fa1d323)).
  - The **Musgrave Texture** node was [replaced by an extended Noise
    Texture
    node](Rendering.md#Musgrave_Texture).

## User Interface

  - The *Ungroup* operator now ungroups all selected group nodes instead
    of just the active one
    ([f3cd25370c](https://projects.blender.org/blender/blender/commit/f3cd25370c5a5eac4e4bf20c330caeba5d2d2d84)).
  - Socket picking when creating node links has been improved to reduce
    the number of mis-clicks
    ([74dd1e044b](https://projects.blender.org/blender/blender/commit/74dd1e044b55d10f6567697be2a30eda8995f697)).

## Auto Smooth

  - The mesh "Auto Smooth" option has been replaced by a modifier node
    group asset
    ([89e3ba4e25](https://projects.blender.org/blender/blender/commit/89e3ba4e25c9ff921b2584c294cbc38c3d344c34)).
      - This means geometry nodes now has the ability to set edge
        sharpness and create split normals without the need for an
        "original" mesh with the auto smooth option.
          - The behavior of sharp handles in the \*Curve to Mesh\* node
            is also controllable, and possible to create from scratch.
      - Blender automatically chooses whether to calculate vertex, face,
        or face corner normals given the presence of custom normals and
        the mix of sharp/smooth edges and faces.
      - Face corner "split" normals are calculated when there is a mix
        of sharp and smooth elements.
      - For more information on the impact to modeling, see the
        [Modeling](Modeling.md)
        section.
      - For more information on the Python API changes, see the [Python
        API](Python_API.md#Breaking_changes)
        section.

![A node group asset to replace the behavior of the "Auto Smooth"
option](../../images/Smooth_by_Angle_Modifier.png
"A node group asset to replace the behavior of the \"Auto Smooth\" option")
