# Rendering

## Shader Nodes

### Musgrave Texture

The Musgrave Texture node was replaced by the Noise Texture node.
Existing shader node setups are converted automatically, and the
resulting renders are identical.
([0b11c591ec](https://projects.blender.org/blender/blender/commit/0b11c591ec62d4ff405108c453f4a34bf3eaee91))

  - The Dimension input is replaced by a Roughness input, where
    \`Roughness = power(Lacunarity, -Dimension)\`.
  - The Detail input value must be subtracted by 1 compared to the old
    Musgrave Texture node.

By being part of the Noise Texture node, the Musgrave modes gain support
for the Distortion input and Color output.
