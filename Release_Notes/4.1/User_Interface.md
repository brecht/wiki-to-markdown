# User Interface

## General

  - Input Placeholders to show a hint about the expected value of an
    input.
    ([b0515e34f9](https://projects.blender.org/blender/blender/commit/b0515e34f9)).

![../../images/Placeholders.png](../../images/Placeholders.png
"../../images/Placeholders.png")

  - Cryptomatte picking can now occur between separate windows.
    ([9ee5de05c0](https://projects.blender.org/blender/blender/commit/9ee5de05c0)).
  - The UI interface font can now be shown in any weight.
    ([0bde01eab5](https://projects.blender.org/blender/blender/commit/0bde01eab5)).

![../../images/UIFontweight.png](../../images/UIFontweight.png
"../../images/UIFontweight.png")

  - Khmer font added to support a new translation of that Cambodian
    language.
    ([3f5654b491](https://projects.blender.org/blender/blender/commit/3f5654b491)).
  - New icons added to represent area splitting, joining, and swapping.
    ([153dd76d22](https://projects.blender.org/blender/blender/commit/153dd76d22),
    [8933284518](https://projects.blender.org/blender/blender/commit/8933284518)).
  - Wide Enum lists will now collapse to a single column if not enough
    space.
    ([83ce3ef0db](https://projects.blender.org/blender/blender/commit/83ce3ef0db)).
  - Changing UI font in Preferences will now start in your OS Fonts
    folder.
    ([d3a2673cb8](https://projects.blender.org/blender/blender/commit/d3a2673cb8)).
  - File Browser List View removes columns and reformats as width is
    decreased.
    ([07820b0703](https://projects.blender.org/blender/blender/commit/07820b0703)).
  - Improved Color Picker cursor indication and feedback.
    ([c11d5b4180](https://projects.blender.org/blender/blender/commit/c11d5b4180)).

![../../images/Colorpicker.png](../../images/Colorpicker.png
"../../images/Colorpicker.png")

  - Outliner Context Menu "Show Hierarchy" and "Expand/Collapse All".
    ([4793b4592b](https://projects.blender.org/blender/blender/commit/4793b4592b),
    [f815484e7d](https://projects.blender.org/blender/blender/commit/f815484e7d)).
  - Text Object fonts now look in the fallback stack when characters are
    not found.
    ([604ee2d036](https://projects.blender.org/blender/blender/commit/604ee2d036)).

![../../images/Textobjectfallback.png](../../images/Textobjectfallback.png
"../../images/Textobjectfallback.png")

  - Animation marker drawing improvements.
    ([0370feb1bf](https://projects.blender.org/blender/blender/commit/0370feb1bf)).
  - Improved corner rounding for menus and popup blocks.
    ([42ddc13033](https://projects.blender.org/blender/blender/commit/42ddc13033)).

![../../images/Menurounding.png](../../images/Menurounding.png
"../../images/Menurounding.png")

  - Improved quality of menu and popup block shadows.
    ([0335b6a3b7](https://projects.blender.org/blender/blender/commit/0335b6a3b7)).
  - Improved initial display of compositor node trees.
    ([ff083c1595](https://projects.blender.org/blender/blender/commit/ff083c1595)).
  - New Text Objects will use translated "Text" as default.
    ([5e38f7faf0](https://projects.blender.org/blender/blender/commit/5e38f7faf0)).

![../../images/TranslatedText.png](../../images/TranslatedText.png
"../../images/TranslatedText.png")

## Viewport

  - Walk mode now supports relative up/down (using R/F keys)
    ([c62009a6ac](https://projects.blender.org/blender/blender/commit/c62009a6ac961e199285dee0d7b5037132a067a7)).
  - Improved Mesh Edge Highlighting.
    ([dfd1b63cc7](https://projects.blender.org/blender/blender/commit/dfd1b63cc7)).
