# VFX & Video

## Compositor

\- The *Keying Screen* node was changed to use a *Gaussian Radial Basis
Function Interpolation*, which produces smoother temporally stable
keying screens. The node now also have smoothness parameter.

![../../images/KeyingScreenTriangulationVsRBF.png](../../images/KeyingScreenTriangulationVsRBF.png
"../../images/KeyingScreenTriangulationVsRBF.png")

\- The Kuwahara node now allows variable sizing by providing a Size
input.

## Sequencer

  - Timeline user interface repaints 3x-4x faster for complex timelines.
    ([df16f4931e](https://projects.blender.org/blender/blender/commit/df16f4931e00))
  - Luma Waveform display calculation is 8x-15x faster.
    ([93ca2788ff](https://projects.blender.org/blender/blender/commit/93ca2788ff01f))
  - Various Sequencer effects and transitions were optimized:
      - Glow is 6x-10x faster.
        ([fc64f48682](https://projects.blender.org/blender/blender/commit/fc64f4868230))
      - Wipe is 6x-20x faster.
        ([06370b5fd6](https://projects.blender.org/blender/blender/commit/06370b5fd685))
      - Gamma Cross is 4x faster.
        ([9cbc96194e](https://projects.blender.org/blender/blender/commit/9cbc96194ea))
