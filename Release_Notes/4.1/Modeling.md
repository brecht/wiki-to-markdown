# Modeling & UV

  - The "Auto Smooth" option has been replaced by a modifier node group
    asset
    ([89e3ba4e25](https://projects.blender.org/blender/blender/commit/89e3ba4e25c9ff921b2584c294cbc38c3d344c34)).
      - Turning "Auto Smooth" on and setting the angle to 180 degrees is
        no longer required to use custom normals
      - Blender automatically chooses whether to calculate vertex, face,
        or face corner normals given the presence of custom normals and
        the mix of sharp/smooth edges and faces.
      - Face corner "split" normals are calculated when there is a mix
        of sharp and smooth elements.
      - In edit mode, "Select Sharp Edges" and "Mark Sharp" can be used
        to set sharpness, or "Shade Smooth by Angle" in other modes.
      - For more information on the procedural smoothing modifier, see
        the [Nodes &
        Physics](Nodes_Physics.md)
        section.
      - For more information on the Python API changes, see the [Python
        API](Python_API.md#Breaking_changes)
        section.
