# Python API & Text Editor

## Breaking changes

### Light Probes

  - The Lightprobe type items have been renamed. It affects
    \`bpy.types.LightProbe.type\`,\` bpy.types.BlendDataProbes.new()\`
    and \`bpy.ops.object.lightprobe\_add()\`.
      - \`CUBEMAP\` -\> \`SPHERE\`
      - \`PLANAR\` -\> \`PLANE\`
      - \`GRID\` -\> \`VOLUME\`

<!-- end list -->

  - \`show\_data\` has been deprecated. Use \`use\_data\_display\`
    instead.
  - Each \`LightProbe\` now has its own \`data\_display\_size\`
    property.

### Mesh

  - Sculpt mask values are stored in a generic attribute
    ([f2bcd73bd2](https://projects.blender.org/blender/blender/commit/f2bcd73bd25625d6b5c1194a4199e96fc5fafa2e)).
      - The name is ".sculpt\_mask", with the \`FLOAT\` type.
      - Accessing, adding, and removing masks is done in a simpler way:
          - The \`Mesh.vertex\_paint\_mask\` property returns the
            attribute directly, rather than a collection.
          - The \`Mesh.vertex\_paint\_mask\_ensure()\` and
            \`Mesh.vertex\_paint\_mask\_remove()\` functions add and
            remove the attribute.
  - The \`Mesh\` \`auto\_smooth\` property has been replaced by a
    modifier node group asset
    ([89e3ba4e25](https://projects.blender.org/blender/blender/commit/89e3ba4e25c9ff921b2584c294cbc38c3d344c34)).
      - \`use\_auto\_smooth\` is removed. Face corner normals are now
        used automatically if there are mixed smooth vs. not smooth
        tags. Meshes now always use custom normals if they exist.
      - \`auto\_smooth\_angle\` is removed. Replaced by a modifier (or
        operator) controlling the \`"sharp\_edge"\` attribute. This
        means the mesh itself (without an object) doesn't know anything
        about automatically smoothing by angle anymore.
      - \`create\_normals\_split\`, \`calc\_normals\_split\`, and
        \`free\_normals\_split\` are removed, and are replaced by the
        simpler \`Mesh.corner\_normals\` collection property. Since it
        gives access to the normals cache, it is automatically updated
        when relevant data changes.
      - \`MeshLoop.normal\` is now a read-only property. Custom normals
        should be created by \`normals\_split\_custom\_set\` or
        \`normals\_split\_custom\_set\_from\_vertices\`.

### Material

  - The \`displacement\_method\` property has moved from
    \`cycles.properties.CyclesMaterialSettings\` to
    \`bpy.types.Material\`
    [a001cf9f2b](https://projects.blender.org/blender/blender/commit/a001cf9f2b08a67aa3b20f1857eac5c915f5ef33)
