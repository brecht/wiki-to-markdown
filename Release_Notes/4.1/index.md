# Blender 4.1 Release Notes

Blender 4.1 is currently in **Alpha**. Phase **Bcon1** until December
20, 2023. [See
schedule](https://projects.blender.org/blender/blender/milestone/18).

Under development in
[\`main\`](https://projects.blender.org/blender/blender/src/branch/main).

## [Animation & Rigging](Animation_Rigging.md)

## [Core](Core.md)

## [EEVEE & Viewport](EEVEE.md)

## [Grease Pencil](Grease_Pencil.md)

## [Modeling & UV](Modeling.md)

## [Nodes & Physics](Nodes_Physics.md)

## [Pipeline, Assets & I/O](Pipeline_Assets_IO.md)

## [Python API & Text Editor](Python_API.md)

## [Rendering](Rendering.md)

## [Cycles](Cycles.md)

## [Sculpt, Paint, Texture](Sculpt.md)

## [User Interface](User_Interface.md)

## [Keymap](Reference/Release_Notes/4.1/Keymap)

## [VFX & Video](VFX.md)

## [Add-ons](Add-ons.md)

## [Asset Bundles](Reference/Release_Notes/4.1/Asset_Bundles)

## Compatibility
