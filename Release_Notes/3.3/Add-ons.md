# Add-ons

## glTF 2.0

### Importer

  - Add missing image name
    ([rBAa2650c0](https://projects.blender.org/blender/blender-addons/commit/a2650c0158ee))
  - **Manage some official Khronos Extensions about Materials**
    ([rBA042fbef](https://projects.blender.org/blender/blender-addons/commit/042fbefac686))

### Exporter

  - **Manage some official Khronos Extensions about Materials**
    ([rBA042fbef](https://projects.blender.org/blender/blender-addons/commit/042fbefac686))
  - Replace Separate RGB shader node by new Separate Color node
    ([rBAc28c92c](https://projects.blender.org/blender/blender-addons/commit/c28c92c1494c))
  - Allow custom name for merged animation
    ([rBA501c976](https://projects.blender.org/blender/blender-addons/commit/501c97628f15))
  - Export deformation bones only, without animation, is now possible
    ([rBA807a64c](https://projects.blender.org/blender/blender-addons/commit/807a64cdfc50))
  - Draco: Directly look at path specified by environment variable
    ([rBA11bc851](https://projects.blender.org/blender/blender-addons/commit/11bc851e16ae))
  - Fix camera & light export when Yup is off
    ([rBA849e719](https://projects.blender.org/blender/blender-addons/commit/849e7196eb4e))
  - Export all actions of a single armature is now under an export
    option
    ([rBA09d752e](https://projects.blender.org/blender/blender-addons/commit/09d752e84534))
  - Fixed bezier control points to cubic spline tangents conversion
    ([rBA08d13c0](https://projects.blender.org/blender/blender-addons/commit/08d13c024f60))
  - **Manage all 4 types of Vertex Colors (corner/point - byte/float)**
    ([rBA2fcac97](https://projects.blender.org/blender/blender-addons/commit/2fcac97522de))
  - Performance: cache parent/children data to avoid bad \`.children\`
    performance when used a lot
    ([rBA5b2953a](https://projects.blender.org/blender/blender-addons/commit/5b2953ad0883))
  - Avoid crash when all drivers are invalids
    ([rBA69b9725](https://projects.blender.org/blender/blender-addons/commit/69b9725ba0f8))
  - Add bufferView Target at export
    ([rBAef0027e](https://projects.blender.org/blender/blender-addons/commit/ef0027e72d29))

## Amaranth

  - Support listing images used by Empty objects in Debug tool
    ([rBA8d609fc](https://projects.blender.org/blender/blender-addons/commit/8d609fc7e68d))
  - Add check for unsaved images in Save/Reload operator
    ([rBA3c6a16f](https://projects.blender.org/blender/blender-addons/commit/3c6a16fcbe5e))
  - Fix Debug feature to list users of image datablocks
    ([rBA4a522d3](https://projects.blender.org/blender/blender-addons/commit/4a522d353a58))

## Collection Manager

  - Fix duplicate keymap entries for QVT operators.
    ([rBA1cc09e2](https://projects.blender.org/blender/blender-addons/commit/1cc09e24c516))
  - Fix duplicate QCD widgets being added when pressing backspace while
    hovering over the QCD 3D Viewport Header Widget checkbox in the
    preferences.
    ([rBAaf8d747](https://projects.blender.org/blender/blender-addons/commit/af8d747d00ac))
  - Fix adding/removing objects from nested collections to/from the
    current selection.
    ([rBA3570274](https://projects.blender.org/blender/blender-addons/commit/3570274fe75f))
