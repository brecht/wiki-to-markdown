# Pipeline, Assets & I/O

## General

  - Improved performance when importing USD, Alembic, OBJ files with
    massive amounts of objects
    ([rB230f7234](https://projects.blender.org/blender/blender/commit/230f72347a),
    [rB5b5811c9](https://projects.blender.org/blender/blender/commit/5b5811c97b),
    [rB94323bb4](https://projects.blender.org/blender/blender/commit/94323bb42),
    [rB7f8d0513](https://projects.blender.org/blender/blender/commit/7f8d05131a),
    [rB092732d1](https://projects.blender.org/blender/blender/commit/092732d11)).
    Importing a production USD scene with 260 thousand objects went from
    3.5 hours down to 1.5 minutes.

## Alembic

  - The export operator can now have presets
    ([rB1d668b63](https://projects.blender.org/blender/blender/commit/1d668b635632))

## OBJ

  - Importer and exporter support .obj vertex colors now
    ([rB1b4f35f6](https://projects.blender.org/blender/blender/commit/1b4f35f6a588)).
    Importer can import both "xyzrgb" and "\#MRGB" vertex color formats;
    exporter optionally writes "xyzrgb" format.
  - The Python based OBJ importer/exporter is marked as "legacy" in the
    menu item now; the new C++ based one got "experimental" label
    removed. Addons using \`bpy.ops.import\_scene.obj\` and
    \`bpy.ops.export\_scene.obj\` APIs are strongly encouraged to switch
    to \`bpy.ops.wm.obj\_import\` and \`bpy.ops.wm.obj\_export\`.

## STL

  - New experimental STL (.stl) importer
    ([rB7c511f1b](https://projects.blender.org/blender/blender/commit/7c511f1b47d8)).
    The new importer is written in C++ and is about 8 times faster than
    the Python importer.

## USD

  - OpenVDB volumes can now be exported to USD
    ([rBce3dd123](https://projects.blender.org/blender/blender/commit/ce3dd12371f5)).

## glTF 2.0

glTF I/O is an python addon, change logs can be retrieved on [Addon
page](Add-ons.md)
