# Modeling

## General

  - New property in the *Shade Smooth* operator to enable *Auto Smooth*.
    Exposed as **Shade Auto Smooth** in the 3D Viewport's *Object* menu
    and *Object Context Menu*.
    ([eef98e66](https://projects.blender.org/blender/blender/commit/eef98e66))
  - New snapping method that snaps transformed geometry to *Nearest
    Face*. Expanded snapping options to give more control over possible
    snapping targets.
    ([011327224e](https://projects.blender.org/blender/blender/commit/011327224ece))

## Modifiers

  - Surface deform modifier can be used when target mesh increases
    number of vertices.
    ([5f8f436dca](https://projects.blender.org/blender/blender/commit/5f8f436dca9))

## UV

![Note the flipped and overlapping UVs around the nose, and the shear
and non-uniform scale for the ears and eyes.](../../images/Uv_before.png
"Note the flipped and overlapping UVs around the nose, and the shear and non-uniform scale for the ears and eyes.")
![After minimize stretch, the flipped faces are smoothed away. After
Average Island Scale with Shear and Scale UV, the ears and eyes now have
a nice even balance between the U coordinate and V
coordinate.](../../images/Uv_after.png
"After minimize stretch, the flipped faces are smoothed away. After Average Island Scale with Shear and Scale UV, the ears and eyes now have a nice even balance between the U coordinate and V coordinate.")

Four changes help with workflow and improve final UV quality.

  - When using UV Unwrap in the UV editor, only the selected UVs will be
    unwrapped.
    ([c0e4532331](https://projects.blender.org/blender/blender/commit/c0e453233132))
  - Minimize Stretch will now unflip faces.
    ([135e530356](https://projects.blender.org/blender/blender/commit/135e530356d0))
  - Upgrade Average Island Scale with new options, Scale UV and Shear.
    ([931779197a](https://projects.blender.org/blender/blender/commit/931779197a9c))
  - Add new operator, "UV Select Similar".
    ([1154b45526](https://projects.blender.org/blender/blender/commit/1154b4552679))

In addition, many smaller bugs have been fixed or resolved including:

  - Snapping to UV vertices is now done if the cursor reaches a minimum
    distance.
    ([d2271cf939](https://projects.blender.org/blender/blender/commit/d2271cf93926))
  - Improved constrain-to-image-bounds when scaling UVs.
    ([bc256a4507](https://projects.blender.org/blender/blender/commit/bc256a450770),
    [4d7c990180](https://projects.blender.org/blender/blender/commit/4d7c99018008))
  - Use per-face aspect correction when applying Cube Projection,
    Spherical Projection, Cylinder Projection and Project From View.
    ([1c1e842879](https://projects.blender.org/blender/blender/commit/1c1e8428791f))
  - Update size of hitbox for UV axis gizmo so it matches hitbox size in
    the 3D viewport.
    ([65e7d49939](https://projects.blender.org/blender/blender/commit/65e7d4993933))
  - During UV unwrap, pinned vertices will not be merged.
    ([e6e9f1ac5a](https://projects.blender.org/blender/blender/commit/e6e9f1ac5a2d))
  - Improved UDIM support.
