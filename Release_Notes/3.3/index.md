# Blender 3.3 Release Notes

Blender 3.3 was released on September 7, 2022.

Check out out the final [release notes on
blender.org](https://www.blender.org/download/releases/3-3/).

This release includes long-term support, see the [LTS
page](https://www.blender.org/download/lts/) for a list of bugfixes
included in the latest version.

## [Animation & Rigging](Animation_Rigging.md)

## [Core](Core.md)

## [Grease Pencil](Grease_Pencil.md)

## [Modeling](Modeling.md)

## [Nodes & Physics](Nodes_Physics.md)

## [Pipeline, Assets & I/O](Pipeline_Assets_IO.md)

## [Python API & Text Editor](Python_API.md)

## [Render & Cycles](Cycles.md)

## [Sculpt, Paint, Texture](Sculpt.md)

## [User Interface](User_Interface.md)

## [VFX & Video](VFX.md)

## [Add-ons](Add-ons.md)

## [More Features](More_Features.md)
