# Sculpt, Paint, Texture

## UDIM

Packing UDIM texture sets into .blend files is now supported.
([rB578771ae](https://projects.blender.org/blender/blender/commit/578771ae4dc))

## UI

  - Use constant size for voxel remesh size edit by default
    ([rB5946ea93](https://projects.blender.org/blender/blender/commit/5946ea938a0)).
  - Add operator to duplicate the active color attribute layer
    ([rB9c28f0eb](https://projects.blender.org/blender/blender/commit/9c28f0eb37a))

<!-- end list -->

  - There are new icons for the trimming tools
    ([17fc8db104](https://projects.blender.org/blender/blender/commit/17fc8db104f)).

![../../images/Trim\_icons\_temp.png](../../images/Trim_icons_temp.png
"../../images/Trim_icons_temp.png") *The old icon was too similar to
existing selection tool icons. We updated the icon to avoid confusion,
especially with planned future features.*

## New Features

  - Add elastic mode to transform tools
    ([rBe90ba74d](https://projects.blender.org/blender/blender/commit/e90ba74d3eb826abab4ec65b82fe0176ce3b7d1b)).

## Performance

  - Drastically improve drawing performance when sculpting with EEVEE
    enabled
    ([rB285a68b7](https://projects.blender.org/blender/blender/commit/285a68b7bbf)).
