# Render & Cycles

## GPU Rendering

### Intel

Support for rendering on the latest Intel GPUs has been added using
oneAPI.
([rBa02992f1](https://projects.blender.org/blender/blender/commit/a02992f1313))

This requires an Intel® Arc™ GPU or Intel Data Center GPU. The
implementation is primarily focused on this architecture and future
Intel GPUs.

  - Supported on **Windows** with driver version **101.3430** or newer.
    Some stability issues exists, which will be addressed in future
    Intel driver updates.
  - Supported on **Linux** with driver version **22.26.23904** or newer.

It is recommended to use [Intel Arc
Beta](https://www.intel.com/content/www/us/en/download/729157/intel-arc-graphics-windows-dch-driver-beta.html)
drivers.

![Render time per sample on an Intel® Arc™ A770
GPU|thumb|center](../../images/Cycles_3.3_Intel_A770.png
"Render time per sample on an Intel® Arc™ A770 GPU|thumb|center")

Going forward we can expect more great things from Intel’s Blender
community collaborations. Development is underway to add Intel® Embree
Ray Tracing GPU hardware acceleration support and Intel® Open Image
Denoise AI GPU acceleration in Cycles for Intel GPUs.

### AMD

AMD GPU Rendering for Vega generation graphics cards has been enabled,
on Windows and Linux. Both discrete GPUs and APUs are supported.
([abfa097](https://projects.blender.org/blender/blender/commit/abfa097))

This includes GPUs such as Radeon VII, Radeon RX Vega Series and Radeon
Pro WX 9100.

### Apple

Metal GPU rendering on Apple Silicon received optimizations for memory
access locality and intersection kernels.
([4b1d315](https://projects.blender.org/blender/blender/commit/4b1d315),
[da4ef05](https://projects.blender.org/blender/blender/commit/da4ef05))

## Changes

  - OpenVDB volumes are now rendered with half float instead of full
    float precision by default. This significantly reduces memory usage.
    The Volume datablock render settings has a new setting to choose
    half float, full float or variable precision encoding.
    ([a8c81ff](https://projects.blender.org/blender/blender/commit/a8c81ff))
  - A new Filmic sRGB colorspace was added for images. This may be used
    for compositing background plates into a render that uses a Filmic
    view transform, without changing the look of the background plate.
    Using Filmic sRGB will convert 0..1 range colors into HDR colors in
    the scene linear color space.
    ([2b80bfe](https://projects.blender.org/blender/blender/commit/2b80bfe),
    [33f5e8f](https://projects.blender.org/blender/blender/commit/33f5e8f))
  - Camera depth of field now supports armature bones as target.
    ([2e70d5c](https://projects.blender.org/blender/blender/commit/2e70d5c))
  - OptiX denoiser update performance when rendering with multiple GPUs
    was improved.
    ([rB79787bf8](https://projects.blender.org/blender/blender/commit/79787bf8e1e1))
