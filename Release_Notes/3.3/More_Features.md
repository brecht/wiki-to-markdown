# More Features

## Collection Instance Offset

The Collections property panel now has operators to control the Instance
Offset parameter
([rB2935b6a2](https://projects.blender.org/blender/blender/commit/2935b6a2acb5)):

![../../images/D14966-collections-instance-offset.png](../../images/D14966-collections-instance-offset.png
"../../images/D14966-collections-instance-offset.png")

  - Instance Offset from Cursor: This one was already available from the
    Object properties panel, and is now in the Collections properties as
    well.
  - Instance Offset from Object: Performs the same operation as above,
    but then based on the active object's evaluated world position.
  - Instance Offset to Cursor: Performs the opposite of the first
    operator: it moves the 3D cursor to the collection's instance
    offset.

The first two operators make it easier to set the instance offset. The
last operator makes it possible to actually see the offset in the 3D
viewport.

## Miscellaneous

  - [Make Instance
    Face](https://docs.blender.org/manual/en/3.3/scene_layout/object/properties/instancing/faces.html)
    now supports instanced collections
    ([rB15b41200](https://projects.blender.org/blender/blender/commit/15b4120064f1)).
