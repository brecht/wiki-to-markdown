# VFX & Video

## Tracking

Implemented "Image from Plane Marker" operator which creates or updates
image data-block from pixels which the plane marker "sees"
([e4bf58e285](https://projects.blender.org/blender/blender/commit/e4bf58e2852)).

This allows creating unwarped texture from a billboard from footage. The
intent is to allow this image to be touched up and re-projected back to
the footage with an updated content.

<youtube>PDphO-w2SsA</youtube>

Implemented motion tracking data pre-fill for compositor nodes
([da85245704](https://projects.blender.org/blender/blender/commit/da852457040),
[c76e1ecac6](https://projects.blender.org/blender/blender/commit/c76e1ecac6d))

## Clip Editor

  - Default to descending average sorting
    ([2e6cd70473](https://projects.blender.org/blender/blender/commit/2e6cd704739))

## Mask Editor

  - Add mask blending factor for combined overlay
    ([eca0c95d51](https://projects.blender.org/blender/blender/commit/eca0c95d51a))
  - Add mask spline visibility overlay option
    ([df2ab4e758](https://projects.blender.org/blender/blender/commit/df2ab4e758c))
  - Masks are always drawn smooth
    ([c1ffea157c](https://projects.blender.org/blender/blender/commit/c1ffea157c5))

## Sequencer

  - Add filter method to strip transform
    ([1c5f2e49b7](https://projects.blender.org/blender/blender/commit/1c5f2e49b7bf))
  - Now change menu (\`C\` key) supports change scene.
    ([205c6d8d08](https://projects.blender.org/blender/blender/commit/205c6d8d0853))
  - Add Scene and Strip at the same time in one action.
    ([be84fe4ce1](https://projects.blender.org/blender/blender/commit/be84fe4ce1d8))
  - Delete Strip and Scene in one step.
    ([8c4bd02b06](https://projects.blender.org/blender/blender/commit/8c4bd02b067a))
  - Added new retiming system. Sped up strips can be edited as normal
    strips. Also movie playback speed is adjusted to match scene
    framerate.
    ([302b04a5a3](https://projects.blender.org/blender/blender/commit/302b04a5a3fc))
  - Add API function to select displayed meta strip.
    ([6b35d9e6fb](https://projects.blender.org/blender/blender/commit/6b35d9e6fbef))

## FFmpeg

  - Playback of VFR movies is now correct
    ([f0a3d2beb2](https://projects.blender.org/blender/blender/commit/f0a3d2beb23e))
