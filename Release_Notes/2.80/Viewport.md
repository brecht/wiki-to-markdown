# Blender 2.80: Viewport

The 3D viewport was completely rewritten, to optimize it for modern
graphics cards and add powerful new features.

## Workbench

A new [Workbench render
engine](https://docs.blender.org/manual/en/2.80/render/workbench/index.html)
was designed for getting work done in the viewport, for tasks like scene
layout, modeling and sculpting. This now powers the wireframe and solid
shading modes.

<center>

|                                                                                     |                                                                               |
| ----------------------------------------------------------------------------------- | ----------------------------------------------------------------------------- |
| ![Matcaps & Cavity](../../images/Blender2.8_viewport_cavity.png "Matcaps & Cavity") | ![Random Colors](../../images/Blender2.8_viewport_random.png "Random Colors") |

</center>

<center>

|                                                                   |                                                             |
| ----------------------------------------------------------------- | ----------------------------------------------------------- |
| ![Shadows](../../images/Blender2.8_viewport_shadow.png "Shadows") | ![X-Ray](../../images/Blender2.8_viewport_xray.png "X-Ray") |

</center>

## Overlays

[Overlays](https://docs.blender.org/manual/en/2.80/editors/3dview/controls/overlays.html)
provide fine control over which utilities are visible on top of the
render. These also work on top of Eevee and Cycles render previews, so
you can edit and paint the scene with full shading.

## LookDev

LookDev is a new shading mode powered by Eevee render engine. In this
mode, multiple lighting conditions (HDRIs) can be tested without
affecting the scene settings. This mode also works as a render preview
for Cycles. Metallic and diffuse shaders spheres can be displayed as a
reference for lighting.

## Volume Preview

Smoke and fire simulation preview has been overhauled to be closer to
what would be rendered by a physically based renderer. It is also more
optimized for newer hardware.
