# Blender 2.80: Tools & Gizmos

The 3D viewport and UV editor have new interactive tools and gizmos,
along with a new toolbar. These make it easier to for new users to start
using Blender, and for existing users to discover and use tools that
previously required obscure key combinations.

![Toolbar in compact and wide
configurations](../../images/Blender2.8_toolbar_config.png
"Toolbar in compact and wide configurations")

## Toolbar

A new toolbar is displayed on the side of the 3D viewport and UV editor.
When activating a tool in the toolbar, it stays active and shows
relevant gizmos in the viewport. This is different than commands found
in menus, which have a more immediate effect and whose settings can be
adjusted after completing the operation.

For operations like moving or extruding, the user can choose to use
either existing fast commands and keyboard shortcuts, or to use the
toolbar instead. The manipulator system has been replaced by move,
rotate and scale gizmos from the corresponding tools in the toolbar. For
combining Move, Rotate or Scale gizmos, we now have the Transform tool.

Some examples of the new interactive tools in action:

<center>

|                                                                                                                                                                 |
| --------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| ![../../images/Blender2.8\_tools\_edit\_mode\_tools.gif](../../images/Blender2.8_tools_edit_mode_tools.gif "../../images/Blender2.8_tools_edit_mode_tools.gif") |
| ![../../images/Blender2.8\_tools\_rotate\_tool\_.gif](../../images/Blender2.8_tools_rotate_tool_.gif "../../images/Blender2.8_tools_rotate_tool_.gif")          |
| ![../../images/Blender2.8\_tools\_spin\_tool.gif](../../images/Blender2.8_tools_spin_tool.gif "../../images/Blender2.8_tools_spin_tool.gif")                    |
| ![../../images/Blender2.8\_tools\_spin\_tool\_2.gif](../../images/Blender2.8_tools_spin_tool_2.gif "../../images/Blender2.8_tools_spin_tool_2.gif")             |

</center>

## Gizmos

Besides gizmos for tools, various elements like lights, camera, and the
compositing backdrop image now have handles to adjust their shape or
other attributes.

![Spot light gizmo](../../images/Blender2.8_tools_lamp_widget.gif
"Spot light gizmo")
