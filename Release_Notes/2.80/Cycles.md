# Blender 2.80: Cycles

## Principled Hair BSDF

Rendering physically based hair and fur is now easy with the [Principled
Hair
BSDF](https://docs.blender.org/manual/en/2.80/render/shader_nodes/shader/hair_principled.html),
no longer is setting up a complex shader network required. Hair color
can be specified with Melanin to match natural human hair colors, or
tinted to simulated dyed hair. Fine roughness control make it possible
to simulate various types of animal fur, and randomization controls help
to add realism.

<center>

|                                                                                                                                                |                                                                                                                                                |
| ---------------------------------------------------------------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------- |
| ![../../images/Cycles2.80\_principled\_hair1.png](../../images/Cycles2.80_principled_hair1.png "../../images/Cycles2.80_principled_hair1.png") | ![../../images/Cycles2.80\_principled\_hair5.png](../../images/Cycles2.80_principled_hair5.png "../../images/Cycles2.80_principled_hair5.png") |
| ![../../images/Cycles2.80\_principled\_hair3.png](../../images/Cycles2.80_principled_hair3.png "../../images/Cycles2.80_principled_hair3.png") | ![../../images/Cycles2.80\_principled\_hair4.png](../../images/Cycles2.80_principled_hair4.png "../../images/Cycles2.80_principled_hair4.png") |

style="caption-side: bottom" | Melanin variations (model and groom by
Chris Chuipka)

</center>

## Principled Volume Shader

The new [Principled
Volume](https://docs.blender.org/manual/en/2.80/render/shader_nodes/shader/volume_principled.html)
shader makes it easier to set up volume materials. Volumes like smoke
and fire can be rendered with a single shader node, which includes
scattering, absorption and blackbody emission.

![Principled Volume with blackbody
emission](../../images/Cycles2.8_principled_volume_blackbody.png
"Principled Volume with blackbody emission")

<center>

|                                                                                                                                                                   |                                                                                                                                                                   |                                                                                                                                                                      |
| ----------------------------------------------------------------------------------------------------------------------------------------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| ![../../images/Cycles2.8\_principled\_volume\_cloud.png](../../images/Cycles2.8_principled_volume_cloud.png "../../images/Cycles2.8_principled_volume_cloud.png") | ![../../images/Cycles2.8\_principled\_volume\_green.png](../../images/Cycles2.8_principled_volume_green.png "../../images/Cycles2.8_principled_volume_green.png") | ![../../images/Cycles2.8\_principled\_volume\_colors.png](../../images/Cycles2.8_principled_volume_colors.png "../../images/Cycles2.8_principled_volume_colors.png") |

</center>

## Cryptomatte

[Cryptomatte](https://github.com/Psyop/Cryptomatte) is a standard to
efficiently create mattes for compositing. Cycles outputs the required
render passes, which can then be used in the Blender compositor or
another compositor with Cryptomatte support to create masks for
specified objects.

It is effectively a more powerful version of the Object and Material ID
passes with two advantages:

  - The objects to create a mask for do not need to be specified in
    advance, rather they can be chosen while compositing.
  - Motion blur, depth of field, transparency, and antialiasing are
    better supported.

The typical workflow is:

  - Enable Cryptomatte Object render pass in the Passes panel, and
    render.
  - In the compositing nodes, create a Cryptomatte node and link the
    Render Layer matching Image and Crypto passes to it.
  - Attach a Viewer node to the Pick output of the Cryptomatte node.
  - Use the Cryptomatte Add/Remove button to sample objects in the Pick
    Viewer node.
  - Use the Matte output of the Cryptomatte node to get the alpha mask.

![Compositor nodes setup with two viewer nodes to pick objects and view
the resulting matte](../../images/cryptomatte_nodes.png
"Compositor nodes setup with two viewer nodes to pick objects and view the resulting matte")

<center>

|                                                        |                                                                            |
| ------------------------------------------------------ | -------------------------------------------------------------------------- |
| ![Render](../../images/cryptomatte_input.png "Render") | ![Picking View](../../images/cryptomatte_pick.png "Picking View")          |
| ![Matte](../../images/cryptomatte_mask.png "Matte")    | ![Isolated Objects](../../images/cryptomatte_matte.png "Isolated Objects") |

style="caption-side: bottom" | Render, Picking View, Matte, and Isolated
Objects (model by Pokedstudio)

</center>

## Random Walk Subsurface Scattering

The new [Random Walk subsurface
scattering](https://docs.blender.org/manual/en/2.80/render/shader_nodes/shader/sss.html)
method provides more accurate results for thin and curved objects. This
comes at the cost of increased render time or noise for more dense media
like skin, but also better geometry detail preservation. It is available
in the Subsurface Scattering and Principled BSDF nodes.
([0df9b2c](https://projects.blender.org/blender/blender/commit/0df9b2c))

<center>

|                                                                                        |                                                                                                                                                                |
| -------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| ![Random Walk SSS](../../images/Cycles2.80_random_walk_bressant.jpg "Random Walk SSS") | ![Random Walk SSS (model by [Three D Scans](http://threedscans.com))](../../images/Cycles2.80_random_walk_crab.jpg "Random Walk SSS (model by Three D Scans)") |

</center>

Random Walk uses true volumetric scattering inside the mesh, which means
that it works best for closed meshes. Overlapping faces and holes in the
mesh can cause problems.

## Subdivision and Displacement

New offscreen dicing scale helps to significantly reduce memory usage,
by reducing the dicing rate for objects the further they are outside of
the camera view. To avoid popping artifacts in animation, a separate
dicing camera can be specified now, to keep the geometry fixed.
Typically the dicing camera would be positioned to cover the entire area
seen by the actual camera for an entire shot.
([cce280d](https://projects.blender.org/blender/blender/commit/cce280d))

![Memory usage in this scene was reduced 5x with no noticeable visual
difference, by using the offscreen dicing
scale.](../../images/Cycles_Displacement.jpg
"Memory usage in this scene was reduced 5x with no noticeable visual difference, by using the offscreen dicing scale.")

When using displacement in materials, a new Displacement node must now
be used. This node will be automatically inserted into existing .blend
files. The reason for this change is to support vector displacement, and
to make it easier to tweak the displacement scale and midlevel.
([4a5ee1a](https://projects.blender.org/blender/blender/commit/4a5ee1a))
([b129ea8](https://projects.blender.org/blender/blender/commit/b129ea8))

![New displacement node
setup.](../../images/Cycles2.80_displacement_node.png
"New displacement node setup.")

Vector Displacement is now supported through a new node. Regular
displacement maps only displace the surface along the normal, while
vector displacement maps work in any direction and can be used to more
accurately bake high resolution meshes from other software. Tangent
space maps can be used for meshes that will be deformed, like animated
characters, so the displacement follows the deformation. Object space
maps work for static meshes, and will render a bit faster with less
memory usage.
([f9ea097](https://projects.blender.org/blender/blender/commit/f9ea097))

![Regular and exaggerated vector displacement on a smooth base mesh.
(Model by [Three D
Scans](http://threedscans.com))](../../images/Cycles2.80_vector_displacement.png
"Regular and exaggerated vector displacement on a smooth base mesh. (Model by Three D Scans)")

## Bevel Shader

A bevel shader was added for rendering rounded corners. Like bump
mapping, this does not modify the actual geometry, only the shading is
affected. Slight rounding on edges helps to capture specular highlights
that you would also see in the real world.
([26f39e6](https://projects.blender.org/blender/blender/commit/26f39e6))

<center>

|                                                          |                                                                                               |
| -------------------------------------------------------- | --------------------------------------------------------------------------------------------- |
| ![Before](../../images/Cycles_bevel_before.jpg "Before") | ![After (model by Juri Unt)](../../images/Cycles_bevel_after.jpg "After (model by Juri Unt)") |

</center>

Note that this is a very expensive shader, and may slow down renders by
20% even if there is a lot of other complexity in the scene. For that
reason, we suggest to mainly use this for baking or still frame renders
where render time is not as much of an issue. The bevel modifier is a
faster option when it works, but sometimes fails on complex or messy
geometry.  

## Ambient Occlusion Shader

The Ambient Occlusion shader can now output a color or a float, for
procedural texturing. It can for example be used to add weathering
effects to corners only.

Similar to the Bevel shader, this is an expensive shader and can slow
down renders significantly. If render time is a concern, using
Pointiness from the Geometry node or baking Ambient Occlusion will
render faster.

New settings on this shader are:

  - *Samples*: number of samples to use, keep as low as possible best
    performance.
  - *Inside*: detect convex rather than concave shapes, by computing
    occlusion inside the mesh.
  - *Only Local*: only detect occlusion from the object itself, and not
    others.
  - *Distance*: distance to trace rays and detect occlusion.

![Rust blended into corners based on AO (model by Emiliano
Colantoni)](../../images/Cycles2.80_ao.jpg
"Rust blended into corners based on AO (model by Emiliano Colantoni)")

## Shader Nodes

  - Hair Info and Particle Info nodes now have a Random output, which is
    a random per hair or particle value in the range 0..1. This can for
    example be used in combination with a color ramp, to randomize the
    hair or particle color.
    ([f6107af](https://projects.blender.org/blender/blender/commit/f6107af))
  - Normal map baking now supports baking of bump nodes plugged into
    BSDFs instead of the displacement output.
    ([b5f8063](https://projects.blender.org/blender/blender/commit/b5f8063))
  - Normal map baking now uses the antialiased baking and AA samples
    when there is a bump or bevel shader.
    ([d0af56f](https://projects.blender.org/blender/blender/commit/d0af56f))
  - Principled BSDF: new Alpha and Emission inputs for easy transparency
    and emission.
  - Math node: add Arctan2 (convert from Cartesion to polar
    coordinates), Floor (round down), Ceil (round up), Fract (value
    after decimal), Square Root.
    ([6862762](https://projects.blender.org/blender/blender/commit/6862762))
  - Voronoi Texture node: add feature and distance settings to output
    different features and give the cells different shapes, similar to
    what was available in Blender Internal.
    ([83a4e1a](https://projects.blender.org/blender/blender/commit/83a4e1a))
  - IES Texture node: match real-world lights based on IES light
    distribution files.
    ([48155c2](https://projects.blender.org/blender/blender/commit/48155c2))
  - Linking color and value sockets to shader sockets now automatically
    treats them as emission. Links from shader to other sockets are now
    highlighted in red, to indicate that these are not valid links.
  - Color management: Cycles now fully follows Blender's OpenColorIO
    configuration. Image texture nodes now use the color space of
    images, shading nodes can work in arbitrary scene linear color
    spaces.

## More Features

  - Area lights now support disk and ellipse shapes.
    ([5505ba8](https://projects.blender.org/blender/blender/commit/5505ba8))
  - Glass can now be rendered as if it was transparent, for compositing
    on top of a background. This can be enabled in the Film panel, with
    an option to control the transmisison roughness below which glass
    becomes transparent.
    ([322f022](https://projects.blender.org/blender/blender/commit/322f022))
  - Holdout setting for objects, this works the same as a Holdout shader
    but for the entire object.
    ([e4b54f44c](https://projects.blender.org/blender/blender/commit/e4b54f44c))
  - Roughness baking support was added.
    ([23ccf57](https://projects.blender.org/blender/blender/commit/23ccf57))
  - Motion blur now supports animated object scale.
    ([25b794a](https://projects.blender.org/blender/blender/commit/25b794a))
  - Camera and object motion blur now support an arbitrary number of
    motion steps.
    ([78c2063](https://projects.blender.org/blender/blender/commit/78c2063),
    [db333d9](https://projects.blender.org/blender/blender/commit/db333d9))

![Long exposure render with many motion
steps](../../images/Cycles2.80_bmw_many_motion_steps.png
"Long exposure render with many motion steps")

## Optimizations

  - More efficient Russian roulette termination, on various scenes
    resulting in 10%-30% faster renders for similar noise. The minimum
    bounces options have been removed and are now automatically
    determined. Number of samples and clamp indirect may need to be
    tweaked to match previous results.
    ([cd023b6cec](https://projects.blender.org/blender/blender/commit/cd023b6cecb7))
  - Better mesh light sampling near geometry and in volumes.
    ([8141eac](https://projects.blender.org/blender/blender/commit/8141eac))
  - The Performance panel contains a new Pixel Size option to control
    the resolution for viewport rendering. Preview renders on high DPI
    displays now render at lower resolution by default.
    ([66c1b23aa1](https://projects.blender.org/blender/blender/commit/66c1b23aa10d))
  - AO bounces in the Simplify panel now renders more glossy and
    transmission bounces than diffuse bounces. This makes it possible to
    get good looking results with low AO bounces settings, making it
    useful to speed up interior renders for example. The AO factor also
    controls the intensity now.
    ([659ba012](https://projects.blender.org/blender/blender/commit/659ba012),
    [171c4e9](https://projects.blender.org/blender/blender/commit/171c4e9))
  - Auto detect importance sampling resolution for environment textures.
    Previously users would have to manually set an appropriate
    resolution to reduce noise, now it’s automatic for the typical
    cases.
  - Removed transparent shadows option, it is always enabled now that it
    has been optimized for all compute devices.
    ([5e4bad2c00](https://projects.blender.org/blender/blender/commit/5e4bad2c00e))
  - Filter glossy and indirect light clamping are now enabled by
    default.
    ([8a7c207](https://projects.blender.org/blender/blender/commit/8a7c207))

## GPU rendering

  - Combined CPU and GPU rendering support for final renders. The number
    of CPU cores for rendering will be automatically reduced to ensure
    GPU rendering does not slow down.
    ([dc9eb82](https://projects.blender.org/blender/blender/commit/dc9eb82))
  - CUDA renders no longer need to use large tiles. In many cases
    rendering with a tile size of for example 32x32 will actually be
    faster now. When using denoising it may still be somewhat faster to
    user large tiles, but this comes with a high memory usage cost.
    ([6da6f8d](https://projects.blender.org/blender/blender/commit/6da6f8d))
  - CUDA rendering now supports rendering scenes that don't fit in GPU
    memory, but can be kept in CPU memory. This feature is automatic but
    comes at a performance cost that depends on the scene and hardware.
    When image textures do not fit in GPU memory, we have measured
    slowdowns of 20-30% in our benchmark scenes. When other scene data
    does not fit on the GPU either, rendering can be a lot slower, to
    the point that it is better to render on the CPU.
    ([c621832](https://projects.blender.org/blender/blender/commit/c621832))
  - OpenCL kernel compile time has been significantly reduced, renders
    now start much faster. Complex scenes also render faster.
  - CUDA and OpenCL rendering support for cubic interpolation with
    images and volumes.
    ([2d92988](https://projects.blender.org/blender/blender/commit/2d92988),
    [f61c340](https://projects.blender.org/blender/blender/commit/f61c340))
  - CUDA GPU rendering no longer supports graphics cards with compute
    capability 2.0 and 2.1, which includes the GTX 480 and 580.
    Unfortunately the latest NVIDIA development tools have dropped
    support for these GPUs, and in order to support newer Volta GPUs and
    faster CPU rendering we needed to upgrade. CUDA is also longer
    available in 32 bit builds, for similar reasons.
  - OpenCL support for half float textures.
    ([df30b50f2f](https://projects.blender.org/blender/blender/commit/df30b50f2f))
  - Due to OpenCL compiler bugs and discontinuation of OpenCL by Apple
    it was disabled on macOS platform. Other platforms still support
    OpenCL
    ([bb0d812d98](https://projects.blender.org/blender/blender/commit/bb0d812d98)

## Fixes

  - Bump and normal maps with high strength no longer lead to black
    spots.
    ([d6e769d](https://projects.blender.org/blender/blender/commit/d6e769d))
  - Shadow catcher objects no longer cast shadows on each other, since
    those would already exist in the real footage.
    ([8b73c9a](https://projects.blender.org/blender/blender/commit/8b73c9a))
  - Image texture box mapping was changed to avoid textures being
    flipped.
    ([dd8016f7](https://projects.blender.org/blender/blender/commit/dd8016f7))
  - Russian roulette termination fix, that makes some objects render
    brighter than before.
    ([d454a44e9](https://projects.blender.org/blender/blender/commit/d454a44e9))
  - Anisotropic, Glossy, Glass and Refraction BSDFs now use a squared
    roughness mapping, consistent with the Principled BSDF and other
    software.
    ([7613ffc](https://projects.blender.org/blender/blender/commit/7613ffc))
