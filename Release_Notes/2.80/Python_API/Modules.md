# Blender 2.80: Helper Modules

Various helper Python API modules have been added, removed or changed.

## Shader Node Import / Export

With the removal of Blender Internal, all materials are now node based.
However shaders found in external files usually have a fixed set of
parameters. To ease import and export of such shaders
`bpy_extras.node_shader_utils` was added, replacing the earlier
`cycles_shader_compat`.

Differences compared to the earlier system:

  - Both import and export are supported, instead of only import.
  - Based on Principled BSDF, instead of a custom shader setup.
      - Custom shader setups not using the Principled BSDF are ignored,
        and default values are exported instead.
  - Value conversion and clamping is left to each IO add-on.
  - Based on accessors to support:
      - Lazy creation of only the required nodes and links on first
        access.
      - Write checking to avoid modification when exporting shaders.

Example code:

``` Python
import bpy

from bpy_extras.node_shader_utils import PrincipledBSDFWrapper
from bpy_extras.image_utils import load_image

# Import
mat = bpy.data.materials.new(name="Material")
mat.use_nodes = True
principled = PrincipledBSDFWrapper(mat, is_readonly=False)
principled.base_color = (0.8, 0.8, 0.5)
principled.specular_texture.image = load_image("/path/to/image.png")

# Export
principled = PrincipledBSDFWrapper(mat, is_readonly=True)
base_color = principled.base_color
specular_texture = principled.specular_texture
if specular_texture and specular_texture.image:
    specular_texture_filepath = principled.specular_texture.image.filepath
```

For a more complex code example, see bundled addons like the OBJ or the
FBX importers/exporters. Import this [free OBJ cat
model](https://free3d.com/3d-model/cat-95027.html) gives a shader setup
like this:

![../../../images/Cat\_OBJ\_MTL\_shader\_in\_Blender\_2.8.png](../../../images/Cat_OBJ_MTL_shader_in_Blender_2.8.png
"../../../images/Cat_OBJ_MTL_shader_in_Blender_2.8.png")

  

## Noise Module

#### Noise and Metric Enumerators

Enumerators in 2.7x were implemented as module level constants. These
have been changed to string enumerators:

**2.7x**

``` Python
noise.noise(position, noise_basis=noise.types.BLENDER)
```

**2.8x**

``` Python
noise.noise(position, noise_basis='BLENDER')
```

#### Noise Type Naming

When transitioning to string enumerators, the following changes apply:

    noise.types.BLENDER         -> 'BLENDER'
    noise.types.STDPERLIN       -> 'PERLIN_ORIGINAL'
    noise.types.NEWPERLIN       -> 'PERLIN_NEW'
    noise.types.VORONOI_F1      -> 'VORONOI_F1'
    noise.types.VORONOI_F2      -> 'VORONOI_F2'
    noise.types.VORONOI_F3      -> 'VORONOI_F3'
    noise.types.VORONOI_F4      -> 'VORONOI_F4'
    noise.types.VORONOI_F2F1    -> 'VORONOI_F2F1'
    noise.types.VORONOI_CRACKLE -> 'VORONOI_CRACKLE'
    noise.types.CELLNOISE       -> 'CELLNOISE'

## Orientation Helper

The `orientation_helper_factory` type generator (used to create a base
class with custom orientation defaults, and common properties that are
used by the axis helpers in code) has been removed. Instead, a more
pythonic `orientation_helper` decorator has been added.

**2.7x:**

``` Python
from bpy_extras.io_utils import (
        ImportHelper,
        orientation_helper_factory,
        )

IOFBXOrientationHelper = orientation_helper_factory("IOFBXOrientationHelper", axis_forward='-Z', axis_up='Y')

class ImportFBX(bpy.types.Operator, ImportHelper, IOFBXOrientationHelper):
    pass
```

**2.8x:**

``` Python
from bpy_extras.io_utils import (
        ImportHelper,
        orientation_helper,
        )

@orientation_helper(axis_forward='-Z', axis_up='Y')
class ImportFBX(bpy.types.Operator, ImportHelper):
    pass
```

## Removed Modules

  - The *extensions\_framework* module was removed.
