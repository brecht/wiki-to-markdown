# Blender 2.80: Animation & Rigging API

## Fix in PoseBone.bone Behavior

The \`PoseBone.bone\` property now correctly sets \`id\_data\` of its
result to the Armature ID, instead of keeping it pointing at the
armature Object
([1c106e18](https://projects.blender.org/blender/blender/commit/1c106e18)).

**2.7x**

``` Python
>>> bpy.data.objects[0].pose.bones[0].bone
bpy.data.objects['Armature'].pose.bones["Bone"].bone
```

**2.8x**

``` Python
>>> bpy.data.objects[0].pose.bones[0].bone
bpy.data.armatures['Armature'].bones["Bone"]
```

The main consequence is that now
\`object.pose.bones\[...\].bone.driver\_add()\` correctly adds the
driver to \`object.data.animation\_data\` instead of
\`object.animation\_data\`.

## Bone Matrix Utilities

  - Bone and PoseBone have new utility methods for bone roll math and
    B-Bone shape.
    ([a58f0eea](https://projects.blender.org/blender/blender/commit/a58f0eea))
      - \`Bone.MatrixFromAxisRoll()\`
      - \`Bone.AxisRollFromMatrix()\`
      - \`pose\_bone.bbone\_segment\_matrix()\`
      - \`pose\_bone.compute\_bbone\_handles()\`

<!-- end list -->

  - Bone provides access to the parent-child transformation math with
    arbitrary matrices.
    ([48a3f97b](https://projects.blender.org/blender/blender/commit/48a3f97b))
      - \`bone.convert\_local\_to\_pose()\`

## Keyframing Utilities

  - Added method for conversion between strip and scene time for NLA
    tweak mode.
    ([27d097e9](https://projects.blender.org/blender/blender/commit/27d097e9))
      - \`anim\_data.nla\_tweak\_strip\_time\_to\_scene()\`
  - Added more flags for use in \`object.keyframe\_insert()\`.
    ([ebc44aae](https://projects.blender.org/blender/blender/commit/ebc44aae))
      - \`INSERTKEY\_REPLACE\`, \`INSERTKEY\_AVAILABLE\`,
        \`INSERTKEY\_CYCLE\_AWARE\`
  - Added a new \`fcurve.is\_empty\` property for easily detecting when
    the curve contributes no animation due to lack of keyframes or
    useful modifiers, and should be deleted.
    ([b05038fe](https://projects.blender.org/blender/blender/commit/b05038fe))
      - Such curves are now ignored by animation evaluation code, so
        this cleanup can be safely postponed until the end of a complex
        editing operator, even if it changes the current frame or
        otherwise triggers evaluation.

## Misc

  - Hook Modifier now provides direct access to the bound vertex index
    array.
    ([3f788eac](https://projects.blender.org/blender/blender/commit/3f788eac))
      - \`hook.vertex\_indices\`, \`hook.vertex\_indices\_set()\`

<!-- end list -->

  - New method to delete all Shape Keys of an object at once.
    ([c7ec6bb7](https://projects.blender.org/blender/blender/commit/c7ec6bb7))
      - \`object.shape\_key\_clear()\`

<!-- end list -->

  - New utility for creating a custom property with default value,
    limits and other UI settings in one call.
    ([40dd9156](https://projects.blender.org/blender/blender/commit/40dd9156))
      - \`from rna\_prop\_ui import rna\_idprop\_ui\_create\`
