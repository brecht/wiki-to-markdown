  - Image: Flip image operator
    [7d874b0343](https://projects.blender.org/blender/blender/commit/7d874b03433).
  - Sculpt: Expand Operator
    [82e7032477](https://projects.blender.org/blender/blender/commit/82e70324772).
  - Sculpt: Mask Init operator
    [74052a9f02](https://projects.blender.org/blender/blender/commit/74052a9f02a).
  - Sculpt: Init Face Sets by Face Sets boundaries
    [e5c1e13ef0](https://projects.blender.org/blender/blender/commit/e5c1e13ef09).
  - The Transfer Mode operator allows to switch objects while in Sculpt
    Mode by pressing the D key
    [86915d04ee](https://projects.blender.org/blender/blender/commit/86915d04ee38)
