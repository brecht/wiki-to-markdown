# Blender 2.93: Pipeline, Assets & I/O

## glTF 2.0

### Importer

  - Fix morph target import when named 'basis'
    ([rBA7521a4e](https://projects.blender.org/blender/blender-addons/commit/7521a4e27be9))
  - Fix shapekey import with negative weight
    ([rBA0dc2141](https://projects.blender.org/blender/blender-addons/commit/0dc2141207df))
  - Better extra error management
    ([rBA30012da](https://projects.blender.org/blender/blender-addons/commit/30012da83522))

### Exporter

  - New feature: export 'Loose Points' and 'Loose Edges'
    ([rBA8d24537](https://projects.blender.org/blender/blender-addons/commit/8d24537a6e9c))
  - New option to export only visible/renderable/active collection
    ([rBA91f57b4](https://projects.blender.org/blender/blender-addons/commit/91f57b489943))
  - Draco : Increase maximal compression limit
    ([rBA55ec469](https://projects.blender.org/blender/blender-addons/commit/55ec46913b99))
  - Inverse matrix only when there is a parent
    ([rBA8adb0dc](https://projects.blender.org/blender/blender-addons/commit/8adb0dce852d))
  - Hook entire glTF data for extensions
    ([rBA4e1ab4a](https://projects.blender.org/blender/blender-addons/commit/4e1ab4a386d5))
  - Fix shapekey export in some cases
    ([rBA5ef60cb](https://projects.blender.org/blender/blender-addons/commit/5ef60cb44a0c))
  - Fix exporting with draco when duplicate geometry
    ([rBA3d7d5ce](https://projects.blender.org/blender/blender-addons/commit/3d7d5cead543))
  - Fix crash when mesh is not created, without crash
    ([rBAf172f77](https://projects.blender.org/blender/blender-addons/commit/f172f7724749))
