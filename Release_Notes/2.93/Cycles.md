# Blender 2.93: Cycles

## OpenColorIO 2.0

OpenColorIO has been upgraded to
[version 2.0](https://opencolorio.readthedocs.io/en/latest/upgrading_v2/how_to.html).
This brings support for reading new OpenColorIO v2 configurations, and
improved color management accuracy in the 3D viewport.
([1b4961b](https://projects.blender.org/blender/blender/commit/1b4961b))

## OpenImageDenoise 1.3

OpenImageDenoise 1.3 improves the sharpness of fine details and gives
less blurry results.

## Subsurface Scattering

Random Walk subsurface scattering now uses Dwivedi guiding to reduce
noise.
([2f6d62b](https://projects.blender.org/blender/blender/commit/2f6d62b))

![Cycles Random Walk SSS comparison by Metin Seven. Download the scene
from the [Blender Demo
Files](https://www.blender.org/download/demo-files/)
page|thumb|center](../../images/Cycles_SSS_demo_by_Metin_Seven_-_Blender_renderings_comparison.jpg
"Cycles Random Walk SSS comparison by Metin Seven. Download the scene from the Blender Demo Files page|thumb|center")

## Light Spread Angle

Area lights now have a Spread angle, to simulate the effect of a
honeycomb or grid placed in front of a softbox. At 180° the area light
is a diffuse emitter as before, at lower angles the light is more
focused.
([a4260ac219](https://projects.blender.org/blender/blender/commit/a4260ac219))

<center>

|                                                           |                                                        |                                                        |                                                     |
| --------------------------------------------------------- | ------------------------------------------------------ | ------------------------------------------------------ | --------------------------------------------------- |
| ![180°](../../images/Cycles-spread-angle-180.jpeg "180°") | ![90°](../../images/Cycles-spread-angle-90.jpeg "90°") | ![45°](../../images/Cycles-spread-angle-45.jpeg "45°") | ![1°](../../images/Cycles-spread-angle-1.jpeg "1°") |

</center>

## Other

  - Access geometry node attributes on meshes, using the Attribute
    shader node.
    ([3a6d6299d](https://projects.blender.org/blender/blender/commit/3a6d6299d)).
  - New Persistent Data option, to speed up animation renders and
    re-renders, at the cost of increased memory usage. If this option is
    enabled, the Cycles render data is cached between animation frames
    and re-renders. This replaces the Persistent Images option, which
    was removed. In scene with expensive modifiers, heavy geometry, many
    images, this can significantly speed up overall rendering time, but
    one must be careful in heavy scenes to avoid running out of memory.
    ([50782df42](https://projects.blender.org/blender/blender/commit/50782df42))
  - New Light Paths \> Fast GI Approximation panel and setting, with
    convenient access to the AO bounces feature. This was previously
    part of the Simplify panel and somewhat difficult to find. This
    feature approximates indirect light with ambient occlusion after a
    specified number of bounces, which is particularly useful for
    viewport render previews or final renders where some reduced quality
    is acceptable.
    ([edd2f51b4e](https://projects.blender.org/blender/blender/commit/edd2f51b4e9))
