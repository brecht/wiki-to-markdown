# Blender 2.93: User Interface

## Outliner

  - The render visibility toggles are now shown by default.
    ([510db9512f](https://projects.blender.org/blender/blender/commit/510db9512fc6))
  - When showing folded item, more sub-items types are gathered as a
    single icon + counter (all bones, bone groups, and vertex groups,
    [rB92dfc8f2](https://projects.blender.org/blender/blender/commit/92dfc8f2673e)).

## File Browser

  - "Frame Selected" operator to scroll to the selected files
    ([5a67407d5a](https://projects.blender.org/blender/blender/commit/5a67407d5aa880)).
    This has shortcut \`Numpad .\` in Blender's default keymap, and
    \`F\` in the Industry Compatible keymap.

## Window Management

#### All Platforms

  - Temporary windows play nicer with others. Opening a new window can
    no longer take over the space of a different window.
    ([492e64c7bc](https://projects.blender.org/blender/blender/commit/492e64c7bcbd)).
  - Window / "New Window" now opens with a simpler layout, with just a
    single editor (the largest of the parent).
    ([be9842f65b](https://projects.blender.org/blender/blender/commit/be9842f65b85)).

#### Windows Platform

  - Child windows are shown on top of parent windows. You can now work
    with overlapping and floating windows.
    ([261fa052ac](https://projects.blender.org/blender/blender/commit/261fa052ac42)).
  - Improved positioning of windows on multiple monitors.
    ([d447bd3e4a](https://projects.blender.org/blender/blender/commit/d447bd3e4a9a))
    ([97fabc3c1c](https://projects.blender.org/blender/blender/commit/97fabc3c1c92)).
  - Improvements to Full-screen support
    ([12193035ed](https://projects.blender.org/blender/blender/commit/12193035ed12))
    ([9b87d3f029](https://projects.blender.org/blender/blender/commit/9b87d3f02962)).

## General Changes

  - Transform arrow cursor improvements in shape and in response to line
    width changes.
    ([b4b02eb4ff](https://projects.blender.org/blender/blender/commit/b4b02eb4ff77)).

![../../images/TransformArrowComparison.png](../../images/TransformArrowComparison.png
"../../images/TransformArrowComparison.png")

  - Navigation Gizmo changes. Better indication of negative axes,
    consistent use of color and size to indicate orientation, ability to
    be resized.
    ([ded9484925](https://projects.blender.org/blender/blender/commit/ded9484925ed)).

![../../images/NavigateGizmoHighlight.png](../../images/NavigateGizmoHighlight.png
"../../images/NavigateGizmoHighlight.png")

  - Position Gizmo tooltips below their bounds so as to not obscure
    themselves.
    ([cf6d17a6aa](https://projects.blender.org/blender/blender/commit/cf6d17a6aa42)).

![../../images/GizmoTooltips.png](../../images/GizmoTooltips.png
"../../images/GizmoTooltips.png")

  - Use ellipsis character when truncating strings no matter how narrow.
    ([de3f369b30](https://projects.blender.org/blender/blender/commit/de3f369b30e5)).
  - Windows OS - Allow any input language, regardless of translated
    display language.
    ([f2781e1c7c](https://projects.blender.org/blender/blender/commit/f2781e1c7c82)).
  - Improved contrast for Status Bar report messages.
    ([694bc4d040](https://projects.blender.org/blender/blender/commit/694bc4d040a8)).

![../../images/Status\_Bar\_Reports.png](../../images/Status_Bar_Reports.png
"../../images/Status_Bar_Reports.png")

  - WM: Add 'Confirm On Release' option for \`WM\_OT\_radial\_control\`
    ([631cc5d56e](https://projects.blender.org/blender/blender/commit/631cc5d56ee9445ece0a9982ed58b1de1dcbbe5b)).
  - Update layouts for graph editor and NLA editor FModifiers
    ([1f5647c07d](https://projects.blender.org/blender/blender/commit/1f5647c07d15d2)).
  - Blender thumbnails now saved without "Camera View"-style
    Passepartout.
    ([9c395d6275](https://projects.blender.org/blender/blender/commit/9c395d6275a0)).
  - Virtual node sockets (unconnected node group sockets) are now more
    visible
    ([b54b56fcbe](https://projects.blender.org/blender/blender/commit/b54b56fcbea62b)).
  - Add Add Copy Full Data Path to RMB menu
    ([85623f6a55](https://projects.blender.org/blender/blender/commit/85623f6a5590)).
  - Dynamically increase width of data-block and search-menu name
    buttons for long names where there is enough space
    ([2dd040a349](https://projects.blender.org/blender/blender/commit/2dd040a34953)).
  - Allow translation of strings even when in 'en\_US' locale, useful
    for non-English add-ons
    ([7a05ebf84b](https://projects.blender.org/blender/blender/commit/7a05ebf84b35)).
