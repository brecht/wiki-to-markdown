# Blender 2.93: VFX & Video

## FFmpeg

  - Improve scrubbing performance
    ([88604b79b7](https://projects.blender.org/blender/blender/commit/88604b79b7d1))
  - Improve proxy building performance
    ([e4f3477833](https://projects.blender.org/blender/blender/commit/e4f347783320))
  - Improve threading settings
    ([1614795ae2](https://projects.blender.org/blender/blender/commit/1614795ae2bf))
  - Adjust default proxy settings
    ([b9207fb43d](https://projects.blender.org/blender/blender/commit/b9207fb43d6f))

## Motion tracking

  - Add track averaging operator
    ([f17b26bbed](https://projects.blender.org/blender/blender/commit/f17b26bbed3))
  - Resolved old-standing issue with Lock-to-View causing jumps when
    changing selection
    ([66f8835f9c](https://projects.blender.org/blender/blender/commit/66f8835f9c03)).
    The fix also avoids jump when toggling lock-to-selection option.

## Sequencer

  - Inherit blend mode with single input effects
    ([d6bbcc4f66](https://projects.blender.org/blender/blender/commit/d6bbcc4f666b))
  - Automatic proxy building
    ([rB04e1feb8](https://projects.blender.org/blender/blender/commit/04e1feb83051))
  - Simplify proxy settings
    ([rB91561629](https://projects.blender.org/blender/blender/commit/91561629cd0b))
  - Preview images when moving strip handles
    ([rB3d9ee83d](https://projects.blender.org/blender/blender/commit/3d9ee83d8818))
  - Text strip improvements
    ([rB1c095203](https://projects.blender.org/blender/blender/commit/1c095203c275))
  - Add bold and italic option for text strip
    ([rB913b71bb](https://projects.blender.org/blender/blender/commit/913b71bb8be9))
  - Add new\_meta RNA API function
    ([rBbfad8deb](https://projects.blender.org/blender/blender/commit/bfad8deb0be0))
  - Add move\_to\_meta RNA API function
    ([rB4158405c](https://projects.blender.org/blender/blender/commit/4158405c0b9d))
  - Disk cache path was changed to include \`\_seq\_cache\` suffix
    ([5368859a66](https://projects.blender.org/blender/blender/commit/5368859a669d))

## Compositor

**Redesigned Cryptomatte Workflow**
[d49e7b82da](https://projects.blender.org/blender/blender/commit/d49e7b82da67885aac5933e094ee217ff777ac03)

In the redesigned workflow a RenderLayer or Image can be selected
directly from the cryptomatte node. The cryptomatte layers are extracted
from the meta data. No need anymore to connect the sockets from the
render layer node anymore.

![mediumpx|frameless|center|Sampling a matte using the Cryptomatte
node](../../images/20210413_cryptomatte-workflow.png
"mediumpx|frameless|center|Sampling a matte using the Cryptomatte node")

When picking a cryptomatte id the mouse shows a hint what is underneath
the mouse cursor. There is no need anymore to switch to the pick socket.
You can even pick from the image editor or movie clip editor.

The matte id's are stored as readable text in the node. It is even
possible to alter the text.

**Anti Aliasing Node**
[805d947810](https://projects.blender.org/blender/blender/commit/805d9478109e)

The Anti-Aliasing node removes distortion artifacts around edges known
as aliasing. The implementation is based on Enhanced Subpixel
Morphological Antialiasing.

![mediumpx|frame|center|Anti aliasing
node](../../images/20210413_anti-aliasing-node.png
"mediumpx|frame|center|Anti aliasing node")

## Animation Player

  - Improved display performance using GLSL to convert the color-space
    instead of the CPU for float images
    [fd3e44492e](https://projects.blender.org/blender/blender/commit/fd3e44492e7606a741a6d2c42b31687598c7d09a).
  - Added support for limiting cache using a memory limit instead of a
    fixed number of frames.  
    This uses the memory cache limit system-preference when launching
    the player from Blender, otherwise it can be accessed with via a new
    command line argument \`-c <memory_limit_mb>\`.
    [3ee49c8711](https://projects.blender.org/blender/blender/commit/3ee49c8711ca72b03687574a98adec904ec1699c)
  - Image cache is now filled at startup to avoid frame-skipping during
    playback as frames load
    [3ee49c8711](https://projects.blender.org/blender/blender/commit/3ee49c8711ca72b03687574a98adec904ec1699c)
