## Library Overrides

### Resync Process Improvements

Resyncing a library override is needed when the source linked data gets
changes in the relationships between its data-blocks.

This process has been made way easier, and most of the time fully
automatic:

  - With newly created files/overrides, needed resync are detected and
    performed automatically when loading the .blend file.
  - However, in some cases, especially with older files that were saved
    with 'broken' (non-hierarchy-matching) overrides, resync itself
    cannot rebuild properly the override as expected (some objects might
    go missing e.g.). For that case, a new operation in the Outliner
    right-click menu was added, \`Resync Library Override Hierarchy
    Enforce\`, that essentially performs a more aggressive resync, at
    the cost of a potential loss of some overrides on ID pointers
    properties.
  - An option (under \`Experimental\` area of user Preferences) was also
    added to allow disabling this auto-resync feature on load, since
    some complex production files may need proper manual processing
    first.

<table>
<tbody>
<tr class="odd">
<td><div class="note_title">
<p><strong>Note</strong></p>
</div>
<div class="note_content">
<p>While this new Resync Library Override Hierarchy Enforce operator may cause some loos of override information, it will be much less sever than the `Reset` + `Resync` alternative process.</p>
</div></td>
</tr>
</tbody>
</table>

Auto-resync detects and keeps around data-blocks no more needed by the
override, but that were previously edited by the user, and for objects
or collections, it put them in a dedicated collection,
\`OVERRIDE\_RESYNC\_LEFTOVERS\`.

### New Override View in the Outliner

A basic informative view of current library overrides has been added to
the outliner
([rBe1ae5bd4](https://projects.blender.org/blender/blender/commit/e1ae5bd45fd0)).

![mediumpx|frameless|center|Override view in the
outliner](../../images/20210413_outliner-override-view.png
"mediumpx|frameless|center|Override view in the outliner")

System-generated overrides (like the overrides of pointers to other
override data-blocks which ensures the override hierarchy) are hidden by
default, and can be shown through a filter option.

Potential info or warning messages from (auto-)resync process are also
shown, as an icon + tooltip next to the affected items.

## Blenloader

  - Added support for using memory-mapped IO in order to speed up
    uncompressed .blend file loading
    ([rB0f2ae614](https://projects.blender.org/blender/blender/commit/0f2ae614a17a)).

## Data Management

  - The Purge operator to delete unused data-blocks has been improved,
    it can now operate on only linked or local data, and can recursively
    delete all 'indirectly' unused data-blocks at once
    ([rB2718ea80](https://projects.blender.org/blender/blender/commit/2718ea80d262)
    and related). new behavior only exposed from \`File -\> Clean Up\`
    menu currently.

## Linked Data

  - Missing libraries and linked data-blocks now are reported as a
    single warning in the UI when loading a .blend file with broken
    libraries. Full info remains available in the console or Info editor
    ([rB53d13b6f](https://projects.blender.org/blender/blender/commit/53d13b6f5387c68)).
