# Blender 2.93: EEVEE

## Depth of field

EEVEE's implementation of depth of field has been rewritten from scratch
to improve performance and quality. It also features a new high quality
mode that matches the raytraced ground truth if using a high enough
sample count.
([000a340afa](https://projects.blender.org/blender/blender/commit/000a340afa67))

<center>

|  |
|  |
|  |

</center>

<center>

|                                                                                                       |
| ----------------------------------------------------------------------------------------------------- |
| ![../../images/Depth-of-field.jpg](../../images/Depth-of-field.jpg "../../images/Depth-of-field.jpg") |
| ![Previous implementation](../../images/Eevee-dof-kitchen-old.jpeg "Previous implementation")         |

</center>

## Volumetrics

A few improvements have been made to the volumetric lighting. It is now
easier to work with and has less limitations.

<center>

|                                                                                                            |                                                                                                                              |
| ---------------------------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------- |
| ![Previous implementation](../../images/1_EEVEE_293_previous_implementation.png "Previous implementation") | ![New (bug-fixed) volumetric shadowing](../../images/2_EEVEE_293_fixed_shadowing.png "New (bug-fixed) volumetric shadowing") |
| ![Area Light Support](../../images/3_EEVEE_293_area_light.png "Area Light Support")                        | ![Soft Shadows Support](../../images/4_EEVEE_293_soft_shadows.png "Soft Shadows Support")                                    |

</center>

  - Added a new Volume and Diffuse slider light contribution. This
    addition was motivated by the current lack of support of light
    node-tree and visibility flag.
    ([884f934a85](https://projects.blender.org/blender/blender/commit/884f934a853f))
  - Area lights are now correctly supported inside volumetric lighting.
    The solution is not physically based and does not match cycles
    perfectly.
    ([355f884b2f](https://projects.blender.org/blender/blender/commit/355f884b2f09))
  - Volumetric Lights Shadowing had bugs fixed that may change the look
    of certain scenes.
    ([3a29c19b2b](https://projects.blender.org/blender/blender/commit/3a29c19b2bff)
    [54f52cac7c](https://projects.blender.org/blender/blender/commit/54f52cac7ccd))
  - Volume lighting now uses a softer attenuation function, reducing
    flickering of lights center.
    ([884f934a85](https://projects.blender.org/blender/blender/commit/884f934a853f))
  - Volume light clamp was changed to fit the new volumetric lighting.
    ([b96acd0663](https://projects.blender.org/blender/blender/commit/b96acd0663b5))
  - Volume shadowing will now be soft if using the soft shadow option in
    the render property panel. Light needs to have shadows enabled for
    this to work.
    ([89ef0da551](https://projects.blender.org/blender/blender/commit/89ef0da5513a))

## Ambient Occlusion

A complete rewrite was done to fix over-darkening artifacts and
precision issues. A better specular occlusion has been implemented to
reduce light leaking from light-probes.
([64d96f68d6](https://projects.blender.org/blender/blender/commit/64d96f68d6ef))

Thanks to this, the ambient occlusion node now supports variable
distance and an approximation of the inverted option.
([dee94afd03](https://projects.blender.org/blender/blender/commit/dee94afd039d))

![Inverted AO node with textured distance
input](../../images/EEVEE_293_ao.png
"Inverted AO node with textured distance input")

## Improvements

  - Glass BSDF have been updated and have less visible interpolation
    artifacts when IOR is less than 1.
    ([83ac8628c4](https://projects.blender.org/blender/blender/commit/83ac8628c490))
  - Fresnel effect of glossy surfaces now follows Cycles more closely.
    ([06492fd619](https://projects.blender.org/blender/blender/commit/06492fd61984))
  - Reflection Cubemaps probes have been improved for low roughness. Old
    scene now require a rebake.
    ([aaf1650b09](https://projects.blender.org/blender/blender/commit/aaf1650b0993))
  - Normal Maps and smoothed normals do not produce strange reflections
    when reflection is below the surface.
    ([1c22b551d0](https://projects.blender.org/blender/blender/commit/1c22b551d0c1))
  - Sub-Surface Scattering do not leak light onto nearby surfaces.
    ([cd9a6a0f93](https://projects.blender.org/blender/blender/commit/cd9a6a0f9389))
  - Screen-Space Raytracing (Reflections, Refraction, Shadows) have been
    improved to reduce the amount of noise and convergence time.
    ([267a9e14f5](https://projects.blender.org/blender/blender/commit/267a9e14f5a7)
    [bbc5e26051](https://projects.blender.org/blender/blender/commit/bbc5e2605199)
    [b79f209041](https://projects.blender.org/blender/blender/commit/b79f20904170))
  - Contact shadow distance fading was removed for performance and
    reliability reasons.
    ([6842c549bb](https://projects.blender.org/blender/blender/commit/6842c549bb3f))
  - Faster animation rendering by reusing scene data between frames.
    ([50782df42](https://projects.blender.org/blender/blender/commit/50782df42))
