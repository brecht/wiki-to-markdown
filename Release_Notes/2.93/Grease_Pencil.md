# Blender 2.93: Grease Pencil

## LineArt Modifier

New Line Art modifier is now included to provide functionality of
generating stylized lines from geometries in the scene.
([3e87d8a431](https://projects.blender.org/blender/blender/commit/3e87d8a4315d))

![Line art demonstration render of a 727 cut-away
drawing.](../../images/Cutaway_0401_bw_3k.png
"Line art demonstration render of a 727 cut-away drawing.")

  - Produce accurate occlusion result from any geometry.
  - Intersection lines can be generated and selected separately for
    styling.
  - Chaining allows strokes to be connected thus allow smooth display of
    grease pencil styles and textures.
  - Vertex weight in the mesh can be transferred into grease pencil to
    allow further style control of generated strokes.
  - Strokes can be baked to allow further edits in the viewport.

## Import/Export

  - New operators to Import SVG to grease pencil objects and export
    grease pencil objects in SVG and PDF format.
    ([a8a92cd15a](https://projects.blender.org/blender/blender/commit/a8a92cd15a52))
  - SVG Importer use vertex color for colors.

<div align="center">

<youtube align="center">BMm0KeMJsI4</youtube>

</div>

## User Interface

  - **IMPORTANT CHANGE:** Now \`Autokey\` button must be enabled to
    generate new keyframes in Draw, Edit or Sculpt mode. If the Autokey
    button is disabled, the last available frame is used. This is more
    aligned with how Blender works and with other 2D softwares. Also, 2D
    template has now Autokey button enabled by default.
    ([6a662ffda8](https://projects.blender.org/blender/blender/commit/6a662ffda836))
      - Note: Old files are not changed. Enable \`Autokey\` button
        manually to get the old behaviour.

<!-- end list -->

  - Now the multiframe is disabled when play animation. Before, it was
    impossible to see the animation because all frames were visible.
    ([e7c36ce43c](https://projects.blender.org/blender/blender/commit/e7c36ce43cf6))
  - Swap positions of Pie menu options for Vertex Paint and Weight
    Paint.
    ([20d0bb43af](https://projects.blender.org/blender/blender/commit/20d0bb43afc8))
  - New multiframe display mode. Keyframes before or after of the active
    frame are displayed using onion colors. This can be disable using
    Onion overlay options.
  - Dopesheet now display a bar between keyframes to indicate that
    keyframe is still used.
    ([e44b2ada3e](https://projects.blender.org/blender/blender/commit/e44b2ada3e4a))
    ([296984b40e](https://projects.blender.org/blender/blender/commit/296984b40e03))
  - Dopesheet context menu has been reorganized and some options added.
    ([15a4ed3aff](https://projects.blender.org/blender/blender/commit/15a4ed3aff40))
  - Materials \`Options\` panel renamed to \`Settings\` to keep
    consinstency.
    ([51c7ff9222](https://projects.blender.org/blender/blender/commit/51c7ff9222c3))
  - Annotations now has a \`Opacity\` parameter (this was removed during
    the refactor and never was added again).
    ([c62e60f01f](https://projects.blender.org/blender/blender/commit/c62e60f01f2a))

## Operators

  - \`Resolution\` parameter for Fill Tool has been renamed to
    \`Precision\` and allows values below 1. This is very handy for
    storyboarding.
    ([6704372f04](https://projects.blender.org/blender/blender/commit/6704372f047c))
  - New operator to Duplicate Layers but with empty Keyframes. This
    operator is very useful to duplicate the keyframes but don't copy
    the strokes to be used in cleanup or painting works.
    ([cfd54ad7d7](https://projects.blender.org/blender/blender/commit/cfd54ad7d78e))
  - New option to Trace images starting in the current frame and not
    always 1.
    ([e747c07156](https://projects.blender.org/blender/blender/commit/e747c07156cc))

## Tools

  - New Layer transform parameters.
    ([e02d84eb3b](https://projects.blender.org/blender/blender/commit/e02d84eb3bea))
  - New Multiframe support in Draw mode (included in fill tool refactor
    commit).

### Fill Tool

A refactor of the fill tool has been done inorder to solve several
problems we had since the first version of the tool.
([1352d81b17](https://projects.blender.org/blender/blender/commit/1352d81b1747))

Changes:

  - The filling speed has been improved for each step of the process
    with the optimization of each algorithm/function.
  - New \`AutoFit\` option to fill areas outside of the viewport. When
    enable, the total size of the frame is calculated to fit the filling
    area.
  - New support multiframe filling. Now it is possible to fill multiple
    similar frames in one go.
  - New \`Stroke Extension\` option to create temporary closing strokes.
    These strokes can be displayed and adjusted dynamically using wheel
    mouse or PageUp/Down keys. Use \`Wheel + Shift\` key for precise
    adjustments.
  - Parameter \`Resolution\` now is named \`Precision\` and has been
    moved to topbar.
  - \`Resolution\` now has decimals and can be lower than 1 to allow
    quick filling in storyboarding workflows. Maximum value has been set
    as 5.
  - Parameter \`Simplify\` has been moved to Advanced panel.
  - Improved fill outline detection. In some cases, the outline
    penetrated the area to be filled with unexpected results.
  - Fill is canceled if the filled area is not closed. This avoids ugly
    full screen fill.
    ([c489bb7c01](https://projects.blender.org/blender/blender/commit/c489bb7c016f))
  - Fixes some corner case bugs with infinite loops.

<div align="center">

<youtube align="center">Jtp0vF3PlbE</youtube>

</div>

### Interpolate Tools

Following with the changes included to interpolate strokes of different
number of points, a full review has been done in the interpolation
tools.
([6bba830589](https://projects.blender.org/blender/blender/commit/6bba83058986))

  - Interpolate now is a tool and not an operator. It was not logic to
    have this tool as a button.
  - Interpolate tool parameters have been moved to topbar.
  - Interpolate popover has been removed from topbar and interpolate
    \`Sequence\` operator has been moved to grease pencil menu.
  - Interpolate Sequence now include a Redo panel.
  - Interpolate tool now allows to select the strokes by pairs. This
    allows to interpolate any stroke with any stroke and not as before
    that it was only possible by drawing order. If no stroke is
    selected, the interpolation is done as before.
  - Now is possible interpolate again if a previous keyframe exist.
    Before, it was impossible to interpolate two times in same frame and
    this made impossible to do the interpolation by groups of frames.
  - New automatic option to \`Flip strokes\` if the initial stroke and
    end stroke are not in the right position because direction is
    different. Also the flip can be set manually for corner cases.
  - Cleanup of menus related to interpolate.
  - Fixed some bugs and removed parameters from scene because now all
    are tool or operator contained.
  - Some code cleanup and function renames.

<div align="center">

<youtube align="center">88FsF6ontPk</youtube>

</div>

This commit also includes the some codebase to future implementation of
the concept \`Vertex Active\` that now does not exist in grease pencil.

## Modifiers and VFX

  - \`Seed\` parameter moved outside Randomize panel in Noise Modifier.
    ([3191b76eb5](https://projects.blender.org/blender/blender/commit/3191b76eb534))
  - New Noise Offset parameter in Noise Modifier.
    ([7a4bdc3a4f](https://projects.blender.org/blender/blender/commit/7a4bdc3a4feb))
  - Remove limitation to use only one Lattice modifier.
    ([670c1fdf64](https://projects.blender.org/blender/blender/commit/670c1fdf64d1))

## Python

  - New API to force the stroke recalculation. In some scripts, the
    coordinates of the points are manipulated and there was no way to
    update the stroke.
    ([faf45091b4](https://projects.blender.org/blender/blender/commit/faf45091b454))
  - New API to manipulate weight points. The old RNA prop was wrong and
    never was usable.
    ([71e63153eb](https://projects.blender.org/blender/blender/commit/71e63153ebf3))
