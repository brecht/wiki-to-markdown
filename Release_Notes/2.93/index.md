# Blender 2.93 Release Notes

Blender 2.93 was released on June 2, 2021.

Check out the final [release notes on
blender.org](https://www.blender.org/download/releases/2-93/).

This release includes long-term support, see the [LTS
page](https://www.blender.org/download/lts/) for a list of bugfixes
included in the latest version.

## [Geometry Nodes](Geometry_Nodes.md)

## [User Interface](User_Interface.md)

## [Modeling](Modeling.md)

## [Sculpt, Paint, Texture](Sculpt.md)

## [Animation & Rigging](Animation-Rigging.md)

## [Grease Pencil](Grease_Pencil.md)

## [EEVEE & Viewport](EEVEE.md)

## [Render & Cycles](Cycles.md)

## [Data Management, Linking, Overrides](Core.md)

## [Pipeline, Assets & I/O](IO.md)

## [Python API & Text Editor](Python_API.md)

## [VFX & Video](VFX.md)

## [Nodes & Physics](Physics.md)

## [Add-ons](Add-ons.md)

## [More Features](More_Features.md)

## Compatibility

Windows 7 is no longer supported. Windows 8.1 or newer is required.
[Microsoft discontinued Windows 7 support in
January 2020](https://support.microsoft.com/en-us/windows/windows-7-support-ended-on-january-14-2020-b75d4580-2cc7-895a-2c9c-1466d9a53962).
