# Blender 2.93: Animation & Rigging

  - Update layouts for graph editor and NLA editor FModifiers
    ([1f5647c07d](https://projects.blender.org/blender/blender/commit/1f5647c07d15d2)).

FCurve Modifier 2.93 Before.png|400px|thumb|Before FCurve Modifier 2.93
After.png|400px|thumb|After

  - Armature bone axes can now be shown anywhere on the bone (previously
    only on the tip),
    ([rB74d5a93b](https://projects.blender.org/blender/blender/commit/74d5a93b2bf7)).
    Existing files still have the old behaviour, while new armatures
    will default to showing the axes at the root (head) of the bone.

![../../images/release-notes-293-animation-bone-axes.png](../../images/release-notes-293-animation-bone-axes.png
"../../images/release-notes-293-animation-bone-axes.png")

  - Weight paint symmetry options has now been tweaked so you can't have
    vertex group mirroring and brush mirroring on at the same time:

![Only X axis mirroring is supported with vertex groups
mirroring|center](../../images/Weight_paint_symmetry_options.png
"Only X axis mirroring is supported with vertex groups mirroring|center")

  - The Cycle-Aware Keying option was moved back to the Keying popover
    since it's not limited to auto-keying.
    ([rBcaf92a64](https://projects.blender.org/blender/blender/commit/caf92a647f76))

<!-- end list -->

  - The "follow path constraint" and "follow parented curve" are no
    longer clamped and can extend beyond the start/end point of the
    curve
    ([rBcf2baa58](https://projects.blender.org/blender/blender/commit/cf2baa585cc8)
    [rB6a0906c0](https://projects.blender.org/blender/blender/commit/6a0906c09a26)).

<!-- end list -->

  - It is now possible to alt click to get a list of bones under the
    cursor.
    ([rB5e77ff79](https://projects.blender.org/blender/blender/commit/5e77ff79ccda))

![../../images/Bone\_menu\_selection.png](../../images/Bone_menu_selection.png
"../../images/Bone_menu_selection.png")
