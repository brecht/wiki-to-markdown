# Geometry Nodes

The geometry nodes project was expanded to build on the attribute
system, allow sampling textures, support volume data, improve usablity,
and much more.

![[Flower scattering sample
file](https://download.blender.org/demo/geometry-nodes/flower_scattering.blend)
by [Blender
Studio](https://cloud.blender.org/).](../../images/Flower_scattering.jpg
"Flower scattering sample file by Blender Studio.")

## New Nodes

![All of the new nodes added in
2.93](../../images/New_Geometry_Nodes_2.93.png
"All of the new nodes added in 2.93")

  - Attribute Proximity
    ([4d39a0f8eb](https://projects.blender.org/blender/blender/commit/4d39a0f8eb1103),
    [f7933d0744](https://projects.blender.org/blender/blender/commit/f7933d0744df44))
  - Attribute Sample Texture
    ([425e706921](https://projects.blender.org/blender/blender/commit/425e706921a8f9))
  - Attribute Remove
    ([60f7275f7f](https://projects.blender.org/blender/blender/commit/60f7275f7f3cbf))
  - Attribute Convert
    ([670453d1ec](https://projects.blender.org/blender/blender/commit/670453d1ecdbb4))
  - Attribute Clamp
    [43455f3857](https://projects.blender.org/blender/blender/commit/43455f385709ed)
  - Attribute Map Range
    ([dda02a448a](https://projects.blender.org/blender/blender/commit/dda02a448a38))
  - Bounding Box
    [e0a1a2f49d](https://projects.blender.org/blender/blender/commit/e0a1a2f49dab57)
  - Collection Info
    ([894cc9c915](https://projects.blender.org/blender/blender/commit/894cc9c9159204))
  - Is Viewport
    ([e7af04db07](https://projects.blender.org/blender/blender/commit/e7af04db0793b6))
  - Points to Volume
    ([ff7a557c67](https://projects.blender.org/blender/blender/commit/ff7a557c67096f))
  - Volume to Mesh
    ([16abe9343a](https://projects.blender.org/blender/blender/commit/16abe9343a5e01))
  - Attribute Separate XYZ, Combine XYZ
    ([a2ba37e5b6](https://projects.blender.org/blender/blender/commit/a2ba37e5b6310b))
  - Subdivide (simple)
    ([4891d4b3d1](https://projects.blender.org/blender/blender/commit/4891d4b3d1f38b))
  - String Input
    ([a961a2189c](https://projects.blender.org/blender/blender/commit/a961a2189cb38f))
  - Mesh Primitive Nodes
    ([9a56a3865c](https://projects.blender.org/blender/blender/commit/9a56a3865c06b472a))
      - Cone
      - Cylinder
      - Circle
      - Cube
      - UV Sphere
      - Ico Sphere
      - Line
      - Grid

## New Attributes Available from Mesh Data

  - UV layers
    ([33a558bf21](https://projects.blender.org/blender/blender/commit/33a558bf21579b))
  - Vertex Colors
    ([e3f0b6d5cb](https://projects.blender.org/blender/blender/commit/e3f0b6d5cbe5d3))
  - Material Index
    ([53bf04f284](https://projects.blender.org/blender/blender/commit/53bf04f2844b64))
  - Face Normals
    ([ba3a0dc9ba](https://projects.blender.org/blender/blender/commit/ba3a0dc9ba91))
  - Shade Smooth
    ([0700441578](https://projects.blender.org/blender/blender/commit/0700441578c9bb))
  - Edge Crease
    ([3618948df8](https://projects.blender.org/blender/blender/commit/3618948df85f))

## Spreadsheet Editor

![The new spreadsheet editor helps to visualize attributes to make it
clearer what the nodes are
doing](../../images/Spreadsheet_Editor_2.93.png
"The new spreadsheet editor helps to visualize attributes to make it clearer what the nodes are doing")

  - A spreadsheet editor was added to display the values of attributes
    for mesh, point cloud, and instance data
    ([3dab6f8b7b](https://projects.blender.org/blender/blender/commit/3dab6f8b7b89),
    [17a5db7303](https://projects.blender.org/blender/blender/commit/17a5db7303df)).
  - The value from a geometry at a certain point in the node tree can be
    displayed by clicking on the icon in the node header
    ([c6ff722a1f](https://projects.blender.org/blender/blender/commit/c6ff722a1fcc)).
  - Data from a specific node can be pinned to a spreadsheet editor, to
    allow displaying data from more than one node at a time
    ([3810bcc160](https://projects.blender.org/blender/blender/commit/3810bcc16047)).

## Usability Improvements

  - There is now a default workspace for geometry nodes
    ([ac90c8a774](https://projects.blender.org/blender/blender/commit/ac90c8a7743f6d)).
  - Error messages are now displayed for some problems, like when an
    input attribute doesn't yet exist
    ([461d4fc1aa](https://projects.blender.org/blender/blender/commit/461d4fc1aae200)).
  - Clicking on an attribute field now exposes a list of available
    attributes for that node, cached from the latest evaluation
    ([461d4fc1aa](https://projects.blender.org/blender/blender/commit/461d4fc1aae200)).
      - The domain and data type of each attribute are also displayed in
        the search list
        ([71eaf872c2](https://projects.blender.org/blender/blender/commit/71eaf872c2db37)).

![../../images/Attribute\_Search\_2.93.png](../../images/Attribute_Search_2.93.png
"../../images/Attribute_Search_2.93.png")

## Rendering Attributes

  - Rendering attributes created in geometry nodes is possible with
    Cycles
    ([3a6d6299d7](https://projects.blender.org/blender/blender/commit/3a6d6299d7a0f2))
