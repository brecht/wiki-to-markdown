# Blender 2.81: Animation & Rigging

## Animation Curves

  - The default automatic handle [smoothing
    mode](https://docs.blender.org/manual/en/dev/editors/graph_editor/fcurves/properties.html#graph-editor-auto-handle-smoothing)
    for new F-Curves can now be changed via User Preferences -\>
    Animation.
    ([aabd8701](https://projects.blender.org/blender/blender/commit/aabd8701))

## Bones

  - The Inherit Scale checkbox is replaced with a dropdown providing
    [more
    options](https://docs.blender.org/manual/en/dev/animation/armatures/bones/properties/relations.html#transformations),
    e.g. Inherit Average Scale.
    ([fcf2a712](https://projects.blender.org/blender/blender/commit/fcf2a712))

## Drivers

  - New [Copy As New
    Driver](https://docs.blender.org/manual/en/dev/animation/drivers/usage.html#copy-as-new-driver)
    context menu option to streamline creating drivers that copy the
    value of a different property.
    ([47335b4e](https://projects.blender.org/blender/blender/commit/47335b4e))
  - Support overriding the Euler order and access Quaternion values in
    rotational Transform Channel driver variables.
    ([82ef1edc](https://projects.blender.org/blender/blender/commit/82ef1edc),
    [88370639](https://projects.blender.org/blender/blender/commit/88370639))
  - [Swing +
    Twist](https://docs.blender.org/manual/en/dev/animation/drivers/drivers_panel.html#rotation-channel-modes)
    rotation decomposition to Transform Channel driver variables.
    ([e91ea20e](https://projects.blender.org/blender/blender/commit/e91ea20e))

## Constraints

### Copy Scale

  - [New
    option](https://docs.blender.org/manual/en/dev/animation/constraints/transform/copy_scale.html#options)
    to force the copied scale to be uniform.
    ([49729785](https://projects.blender.org/blender/blender/commit/49729785))

### Copy Rotation

  - Option to explicitly specify the Euler order used during copy.
    ([18d4ad5a](https://projects.blender.org/blender/blender/commit/18d4ad5a))
  - Replaced the Offset checkbox with a [Mix
    dropdown](https://docs.blender.org/manual/en/dev/animation/constraints/transform/copy_rotation.html#options)
    providing better combining options.
    ([f4056e9e](https://projects.blender.org/blender/blender/commit/f4056e9e))

### Copy Transforms

  - Added a new [Mix
    dropdown](https://docs.blender.org/manual/en/dev/animation/constraints/transform/copy_transforms.html#options)
    option that allows combining the original and copied transforms.
    ([f8362836](https://projects.blender.org/blender/blender/commit/f8362836))

### Transformation

  - Options to explicitly specify the input and output Euler order.
    ([18d4ad5a](https://projects.blender.org/blender/blender/commit/18d4ad5a))
  - Swing + Twist rotation decomposition as a choice for rotation [input
    mode](https://docs.blender.org/manual/en/dev/animation/constraints/transform/transformation.html#source).
    ([e91ea20e](https://projects.blender.org/blender/blender/commit/e91ea20e))
  - [Mix
    dropdown](https://docs.blender.org/manual/en/dev/animation/constraints/transform/transformation.html#destination)
    option that allows changing how the transformation is combined with
    the original.
    ([e858d21a](https://projects.blender.org/blender/blender/commit/e858d21a))

## Custom Properties

  - [Custom
    property](https://docs.blender.org/manual/en/dev/files/data_blocks.html#custom-properties)
    UI now supports short arrays, useful for storing vectors or colors
    as a single property.
    ([aef08fda](https://projects.blender.org/blender/blender/commit/aef08fda))
  - Array custom properties can be set to appear as a standard color
    picker button in the UI.
    ([55c38f47](https://projects.blender.org/blender/blender/commit/55c38f47))
