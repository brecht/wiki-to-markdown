# Blender 2.81: Python API

## Python Upgrade

Python has been upgraded from 3.7.0 to version, 3.7.4.

## API Changes

  - \`IDMaterials.clear()\` and \`IDMaterials.pop()\` no longer have an
    \`update\_data\` parameter. It now always behave as if
    \`update\_data=True\` was passed.
    ([c70f975d5c](https://projects.blender.org/blender/blender/commit/c70f975d5c3b961e0c))
  - \`Bone.use\_inherit\_scale\` and \`EditBone.use\_inherit\_scale\`
    are deprecated in favor of the new \`inherit\_scale\` enum property.
    ([fcf2a712](https://projects.blender.org/blender/blender/commit/fcf2a712))
  - \`CopyRotationConstraint.use\_offset\` is deprecated in favor of the
    new \`mix\_mode\` enum.
    ([f4056e9e](https://projects.blender.org/blender/blender/commit/f4056e9e))
  - \`CyclesShaderWrapper\` has been removed developers should be using
    \`PrincipledBSDFWrapper\` now.
    ([rBA19819fb1ce7e](https://developer.blender.org/rBA19819fb1ce7e3a3a603a32e6eac7f548cfbcf866))
  - \`ShaderNodeMapping\` was rewritten to have dynamic inputs, so
    direct access to \`translation\`, \`rotation\` and \`scale\` has
    been removed and is now done via \`inputs\['Location'\]\`,
    \`inputs\['Rotation'\]\` and \`inputs\['Scale'\]\`. Also
    \`min\`/\`max\` was removed entirely and should be replaced by
    access to the \`Extension\` parameter of the Texture node
    ([1](https://developer.blender.org/rBbaaa89a0bc54),
    [2](https://developer.blender.org/rB87d0033ea9fe))
  - \`Bone\` properties related to its position and orientation have
    been marked read only, because modifying them was never expected by
    the code and resulted in inconsistent state. The rest pose should
    only be modified through \`EditBone\`.
    ([d4f8bc80](https://projects.blender.org/blender/blender/commit/d4f8bc80))

## Mesh Replacement

Meshes can now be cleared of all geometry by calling
\`mesh.clear\_geometry()\`
([6d2f9b1dfa](https://projects.blender.org/blender/blender/commit/6d2f9b1dfa98)).
This allows the reuse of existing mesh datablocks, by clearing with
\`mesh.clear\_geometry()\` and subsequently recreating with, for
example, \`mesh.from\_pydata()\`. Not only is it faster than creating a
new mesh and swapping out the old one, but this also makes it easier for
exporters to understand this actually represents the same logical mesh
(instead of exporting a different static mesh for every frame).

Shape keys are not freed by this function. Freeing those would require
tagging the depsgraph for relations update, which is an expensive
operation. They should be removed explicitly if necessary.

Material slots are also not cleared by this function, in the same way
that they are not cleared when manually removing all geometry from a
mesh.

## Nodes

  - Support for displaying node sockets with square and diamond shapes,
    using the new \`NodeSocket.display\_shape\` property.
    ([2ba233a](https://projects.blender.org/blender/blender/commit/2ba233a))

## Rigging

  - Constraint collections of objects and bones now have a \`move()\`
    method for reordering the stack.
    ([36e23c95](https://projects.blender.org/blender/blender/commit/36e23c95))
  - The \`length\` property of bones is now implemented in C and
    available for use in drivers and UI.
    ([d4f8bc80](https://projects.blender.org/blender/blender/commit/d4f8bc80))

## EEVEE

Shadow map refactor removed some properties from the API.
([d8aaf25c23](https://projects.blender.org/blender/blender/commit/d8aaf25c23fa))

  - \`shadow\_buffer\_soft\`, \`shadow\_buffer\_exp\`,
    \`shadow\_buffer\_bleed\_bias\` and \`contact\_shadow\_soft\_size\`
    have been removed for all light data types.
  - \`SunLight.shadow\_buffer\_clip\_start\` and
    \`SunLight.shadow\_buffer\_clip\_end\` have been removed due to
    sunlight now having auto clip distances.
  - \`SceneEEVEE.use\_sss\_separate\_albedo\` and
    \`SceneEEVEE.shadow\_method\` have also been removed.

## Operators

Operators can now have a dynamic tooltip that changes based on the
context and operator parameters.
([9ecbd67d](https://projects.blender.org/blender/blender/commit/9ecbd67d))

With an operator defined as (irrelevant parts omitted):

``` Python
class WM_OT_tooltip_test(bpy.types.Operator):
    arg: bpy.props.StringProperty()

    @classmethod
    def description(cls, context, properties):
        return "Arg is: " + properties.arg
```

The following button would have a tooltip that says "Arg is: FOO".

``` Python
layout.operator('wm.tooltip_test').arg = 'FOO'
```

This is mainly intended for cases when multiple similar but distinct
operations are implemented for technical reasons as one operator with a
mode switch parameter.

## Handlers

The \`depsgraph\_update\_post\` and \`frame\_change\_post\` handlers are
now receiving an optional \`depsgraph\` argument which allows to access
evaluated datablocks within corresponding dependency graph. This can be
used, for example, to alter scene prior it gets rendered.

The new function signatures are:

``` Python
def depsgraph_update_post_handler(scene, depsgraph):
    pass

def frame_change_post_handler(scene, depsgraph):
    pass
```

NOTE: The \`depsgraph\` argument is optional and can be left out. This
keeps compatibility with existing scripts.

Details about access of evaluated datablocks can be found in the [API
documentation](https://docs.blender.org/api/master/bpy.types.Depsgraph.html?dependency-graph-evaluated-id-example).

## Mathutils - Geometry

A new \`mathutils.geometry.delaunay\_2d\_cdt(vert\_coords, edges, faces,
output\_type, epsilon)\` function will do a Constrained Delaunay
Triangulation of a set of 2d points, edges, and faces. Such a
triangulation makes nice triangles (not long and skinny) while making
sure that the input edges and faces are in the triangulation (perhaps
divided into pieces). As a side effect, all of the input geometry is
intersected with itself and other input geometry, so this routine can
also be used for doing general 2d intersection, handling all of the
degenerate cases gracefully.
([b91643c711](https://projects.blender.org/blender/blender/commit/b91643c71135))
