## Blender 2.81: Bug Fixes

### Blender

  - Fix [\#71147](http://developer.blender.org/T71147): Eevee stops
    rendering after selection attempt
    ([rB4e42a98e](https://projects.blender.org/blender/blender/commit/4e42a98edd8))
  - Fix [\#71213](http://developer.blender.org/T71213): Mask or Mirror
    before Armature breaks weight paint
    ([rBf1ac6492](https://projects.blender.org/blender/blender/commit/f1ac64921b4)
    )
  - Fix [\#71612](http://developer.blender.org/T71612): Viewport rotate
    doesn't work
    ([rBaadbb794](https://projects.blender.org/blender/blender/commit/aadbb794cd6e))
  - Fix [\#71741](http://developer.blender.org/T71741): Crash showing
    the object relations menu
    ([rB8cb55f8d](https://projects.blender.org/blender/blender/commit/8cb55f8d1672))
  - Fix [\#71558](http://developer.blender.org/T71558): Hair particles:
    Brush effect not occluded by emitter geometry
    ([rBa8d29ad6](https://projects.blender.org/blender/blender/commit/a8d29ad6e06))
  - Fix segfault when polling \`MESH\_OT\_paint\_mask\_extract\`
    ([rB73ce35d3](https://projects.blender.org/blender/blender/commit/73ce35d3325))
  - Fix [\#71864](http://developer.blender.org/T71864): Broken 'Select'
    \> 'More' in face mode in UV Editor
    ([rBbdfcee34](https://projects.blender.org/blender/blender/commit/bdfcee347eb))
  - Fix [\#69332](http://developer.blender.org/T69332): 'Reset to
    Default Value' on a custom string property crashes
    ([rB60e81769](https://projects.blender.org/blender/blender/commit/60e817693ce))
  - Fix [\#72071](http://developer.blender.org/T72071): Crash on snap to
    edge
    ([rB4a440ecb](https://projects.blender.org/blender/blender/commit/4a440ecb99d))
  - Fix crash exiting edit-mode with an active basis shape key
    ([34902f2008](https://projects.blender.org/blender/blender/commit/34902f20089))

### Addons

  - Fix [\#71774](http://developer.blender.org/T71774): SVG import error
    on specific files
    ([rBAee818b2](https://projects.blender.org/blender/blender-addons/commit/ee818b2a))
  - Fix [\#71678](http://developer.blender.org/T71678): Rigify crash if
    the Advanced mode is set to New.
    ([rBAed5b81f](https://projects.blender.org/blender/blender-addons/commit/ed5b81f5))
  - Fix [\#72145](http://developer.blender.org/T72145): STL crash
    exporting object without data
    ([rBA2f425cc](https://projects.blender.org/blender/blender-addons/commit/2f425cc1))
