# Blender 2.81: Cycles

## Denoising

A new Denoise node was added in the compositor, to denoise renders using
[OpenImageDenoise](https://openimagedenoise.github.io/). It needs Albedo
and Normal passes, which Cycles can render by enabling Denoising Data
passes.

Compared to the existing denoiser, it works better with more complex
materials like glass, and suffers less from splotchy artifacts. It also
gives better results with very low numbers of samples, which can be used
for quick previews.

<center>

|                                                                                                                                                                       |                                                                                                                                                                    |
| --------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| ![../../images/Blender2.81\_pabellon\_4\_oidn\_before.png](../../images/Blender2.81_pabellon_4_oidn_before.png "../../images/Blender2.81_pabellon_4_oidn_before.png") | ![../../images/Blender2.81\_pabellon\_4\_oidn\_after.png](../../images/Blender2.81_pabellon_4_oidn_after.png "../../images/Blender2.81_pabellon_4_oidn_after.png") |

style="caption-side: bottom" | Before before and after denoising, with 4
samples

</center>

<center>

|                                                                                                                                                                             |                                                                                                                                                                          |
| --------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| ![../../images/Blender2.81\_pabellon\_128\_oidn\_before.png](../../images/Blender2.81_pabellon_128_oidn_before.png "../../images/Blender2.81_pabellon_128_oidn_before.png") | ![../../images/Blender2.81\_pabellon\_128\_oidn\_after.png](../../images/Blender2.81_pabellon_128_oidn_after.png "../../images/Blender2.81_pabellon_128_oidn_after.png") |

style="caption-side: bottom" | Before and after denoising, with 128
samples

</center>

This feature requires a CPU with SSE4.1, available in Intel and AMD CPUs
from the last 10 years.

## Shaders

Shader nodes for Cycles and Eevee have been extended.

  - New White Noise Texture node, producing a random number based on
    input values.
    ([133dfdd7](https://projects.blender.org/blender/blender/commit/133dfdd7))
  - Noise Texture node: support 1D, 2D, and 4D noise.
    ([23564583](https://projects.blender.org/blender/blender/commit/23564583))
  - Musgrave Texture node: support 1D, 2D, and 4D musgrave.
    ([f2176b3f](https://projects.blender.org/blender/blender/commit/f2176b3f))
  - Voronoi Texture node: support 1D, 2D, and 4D voronoi. Add more
    feature types.
    ([613b37bc](https://projects.blender.org/blender/blender/commit/613b37bc))
  - New Volume Info node, for convenient access to Color, Density,
    Flame, and Temperature attributes of smoke domains.
    ([e83f0922](https://projects.blender.org/blender/blender/commit/e83f0922))
  - Object Info node: new Object Color output.
    ([08ab3cbc](https://projects.blender.org/blender/blender/commit/08ab3cbc))
  - New Map Range node. Linearly remap a value from one range to
    another.
    ([71641ab5](https://projects.blender.org/blender/blender/commit/71641ab5))
    ([7a7eadaf](https://projects.blender.org/blender/blender/commit/7a7eadaf))
  - New Clamp node. Clamps a value between a maximum and a minimum
    values.
    ([313b7892](https://projects.blender.org/blender/blender/commit/313b7892))
  - Math node: shows one or two inputs depending on how many are needed
    by the operation.
    ([e5618725](https://projects.blender.org/blender/blender/commit/e5618725))
  - Vector Math node: new operations, and show number of inputs
    depending on the operation.
    ([7f4a2fc4](https://projects.blender.org/blender/blender/commit/7f4a2fc4))
  - Mapping node: location, rotation and scale are now node inputs that
    can be linked to other nodes.
    ([baaa89a0bc](https://projects.blender.org/blender/blender/commit/baaa89a0bc5))
  - New Vertex Color node, for convenient access to Vertex Colors and
    their alpha channel.
    ([2ea82e86](https://projects.blender.org/blender/blender/commit/2ea82e86))

![New voronoi texture nodes](../../images/Blender2.81_voronoi.png
"New voronoi texture nodes")

## NVIDIA RTX

Cycles now has experimental support for rendering with
hardware-accelerated raytracing on NVIDIA RTX graphics cards. To use,
enable OptiX in Preferences \> System \> Cycles Render Devices.

Most, but not all features of the CUDA backend are supported yet:
Currently still missing are baking, branched path tracing, ambient
occlusion and bevel shader nodes and combined CPU + GPU rendering. So
certain scenes may need small tweaks for optimal rendering performance
with OptiX. When rendering the first time, the OptiX kernel needs to be
compiled. This can take up to a few minutes. Subsequent runs will be
faster.

OptiX requires [recent NVIDIA
drivers](https://www.nvidia.com/Download/index.aspx) and is supported on
both Windows and Linux.

  - Linux: driver version 435.12 or newer
  - Windows: driver version 435.80 or newer

![OptiX performance compared to CPU and
CUDA](../../images/Blender2.81_cycles-rtx-performance-1.png
"OptiX performance compared to CPU and CUDA")

The OptiX backend was [contributed by
NVIDIA](https://code.blender.org/2019/07/accelerating-cycles-using-nvidia-rtx/).

## Adaptive Subdivision

Cycles adaptive subdivision now stitches faces to avoid cracks between
edges, that may be caused by displacement or different materials.

![../../images/Cycles\_displacement\_cracks.jpg](../../images/Cycles_displacement_cracks.jpg
"../../images/Cycles_displacement_cracks.jpg") ![Adaptive subdivision
with and without cracks](../../images/Cycles_multi_material_cracks.jpg
"Adaptive subdivision with and without cracks")

## Other

  - Reduced shadow terminator artifacts for bump mapping with diffuse
    BSDFs.

<center>

|                                                              |                                                           |
| ------------------------------------------------------------ | --------------------------------------------------------- |
| ![Before](../../images/Blender2.81_bump_before.png "Before") | ![After](../../images/Blender2.81_bump_after.png "After") |

</center>

  - Viewport support to use HDRI lighting instead of scene lighting, for
    look development.
  - Viewport option to display a render pass, instead of the combined
    pass.
