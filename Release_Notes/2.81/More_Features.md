# Blender 2.81: More Features

## Alembic

  - Face-varying normals are now imported from Alembic.
    ([e9c149d911](https://projects.blender.org/blender/blender/commit/e9c149d911c)).
  - Importing is now possible with relative paths, and respects the
    'Relative Paths' user preference.
    ([34143e4510](https://projects.blender.org/blender/blender/commit/34143e45104b)).
  - The Auto Smooth flag can be used to toggle flat/smooth shading on an
    imported mesh. This only works when the Alembic mesh doesn't contain
    face normals (if it does contain normals, those are used as-is).
    ([dffe702d78](https://projects.blender.org/blender/blender/commit/dffe702d782)).

## Audio & Video Output

  - Opus audio codec support.
    ([2ddfd51810](https://projects.blender.org/blender/blender/commit/2ddfd51810e0))
  - WebM video container support.
    ([ca0b5529d0](https://projects.blender.org/blender/blender/commit/ca0b5529d09c))
  - Alpha channel support for WebM/VP9 video.
    ([43b7512a59](https://projects.blender.org/blender/blender/commit/43b7512a59c2))
  - RGB mode (contrary to RGBA) support for various video formats, like
    Quicktime RLE/Animation. Previously these always included an alpha
    channel.
    ([08a6321501](https://projects.blender.org/blender/blender/commit/08a63215018)).

## Library Management

  - Only directly linked objects are now added to the scene when
    appending. Indirectly linked objects will be used by the directly
    linked objects but not visible in the scene.
    ([rB37b4384b](https://projects.blender.org/blender/blender/commit/37b4384b59c0968)).

## Sequencer

  - Prefetching support, to automatically fill the cache with frames
    after the current frame in the background. This can be enabled in
    the sequencer Preview, in the View Settings in the sidebar
    ([rBab3a9dc1](https://projects.blender.org/blender/blender/commit/ab3a9dc1ed28)).
  - New operator to add and remove fades for all strips(volume or
    opacity property)
    ([rB2ec025d7](https://projects.blender.org/blender/blender/commit/2ec025d7be3c))
