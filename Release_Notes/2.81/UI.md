# Blender 2.81: User Interface

## Outliner

![Active and selected items are synced and drawn
highlighted](../../images/Outliner_2.81_Selection.jpg
"Active and selected items are synced and drawn highlighted")

![A short demonstration of the outliner
improvements](../../videos/Blender_Outliner_2.81_gsoc.mp4
"A short demonstration of the outliner improvements")

The outliner got many usability improvements.

  - Selection is now synced between the 3D viewport and outliner by
    default, in both directions.
  - Walk navigation with up and down keys.
  - Expand and collapse with left and right keys, hold shift to apply
    recursively.
  - Box select with click and drag.
  - Range selection with shift click, extend selection with ctrl click,
    and range select without deselecting with ctrl shift click.
  - Expand arrow click and drag to expand or collapse multiple.
  - Sub elements shown as icon row can now be selected.
  - Object selection with eyedropper now works in the outliner.
  - Filter option to show hidden objects.
  - F2 to rename active outliner element.
  - Parent drag and drop for multiple objects.
  - Icons for constraints, vertex groups and sequencer.

  

## File Browser

![New File Browser design in Vertical List
view](../../images/2_81_file_browser_vertical_list.png
"New File Browser design in Vertical List view") ![Thumbnail
view](../../images/2_81_file_browser_thumbnails.png "Thumbnail view")

The file browser was redesigned.

  - Opens as floating window with more standard layout.
  - Popovers for view and filter options.
  - File operation options display in the sidebar. Depending on the
    operation the File Browser will execute, the sidebar is opened or
    closed by default.
  - Vertical list view with interactive column header.
  - Reworked display of date and file size text.
  - New and updated icons.
  - Deleting files will now move them to the system trash.
  - Some settings of the dialog are saved in the preferences, so
    reopening it will have them remembered.
  - Preferences option allows using the File Browser in full-screen
    layout.

Mouse and keyboard interaction is also more consistent and powerful. For
details, see
[section](https://wiki.blender.org/wiki/Reference/Release_Notes/2.81/UI#Keymap%7Ckeymap).

  

## Batch Rename

![../../images/2\_81\_batch\_rename.png](../../images/2_81_batch_rename.png
"../../images/2_81_batch_rename.png")

Where you could previously rename the active item with F2, you can now
also batch rename all selected items with Ctrl F2.

  - Find & replace, with regular expressions
  - Set, prefix or suffix
  - Strip characters
  - Change capitalization

  

## Preferences

  - Option to use OS-Key for emulate middle mouse button.
    ([d596a6368](https://projects.blender.org/blender/blender/commit/d596a6368))
  - Option to show temporary File Browser in full-screen layout
    (<span class="literal">Interface</span> »
    <span class="literal">Temporary Windows</span> »
    <span class="literal">File Browser</span>).
    ([f5bbaf55ac](https://projects.blender.org/blender/blender/commit/f5bbaf55ac3d))
  - Option for the render result location was moved from the scene, to
    the Preferences (<span class="literal">Interface</span> »
    <span class="literal">Temporary Windows</span> »
    <span class="literal">Render In</span>).
    ([cac756a92a](https://projects.blender.org/blender/blender/commit/cac756a92aae))
  - Option for view rotation sensitivity.
    ([e82b7f1527](https://projects.blender.org/blender/blender/commit/e82b7f15270e))
  - Option for Auto Handle Smoothing mode.
    ([aabd8701e9](https://projects.blender.org/blender/blender/commit/aabd8701e985))
  - Annotation Simplify option got removed.
    ([317033a1be](https://projects.blender.org/blender/blender/commit/317033a1be2a))
  - "Enabled Add-ons Only" option
    [078d02f557](https://projects.blender.org/blender/blender/commit/078d02f55743)
  - Auto-saving:
      - Option is disabled when factory settings have been loaded.
      - "\*" suffix is added to Save Preferences button when changes
        were made.
  - Themes:
      - File Browser Custom Folder Color

## Keymap

### Default Keymap

3D View:

  - <span class="hotkeybg"><span class="hotkey">Ctrl</span><span class="hotkey">.</span></span>:
    Toggle origin editing
  - Poly Build Tool:
      - <span class="hotkeybg"><span class="hotkey" style="padding-left: 3px;">LMB
        ![../../images/Template-LMB.png](../../images/Template-LMB.png
        "../../images/Template-LMB.png")</span><span class="hotkey">Drag</span></span>:
        On boundary edge, extrude quad and move with cursor; on vertices
        tweak position
      - <span class="hotkeybg"><span class="hotkey">Ctrl</span><span class="hotkey" style="padding-left: 3px;">LMB
        ![../../images/Template-LMB.png](../../images/Template-LMB.png
        "../../images/Template-LMB.png")</span></span>: Add geometry
      - <span class="hotkeybg"><span class="hotkey">⇧
        Shift</span><span class="hotkey" style="padding-left: 3px;">LMB
        ![../../images/Template-LMB.png](../../images/Template-LMB.png
        "../../images/Template-LMB.png")</span></span>: Delete elements
  - Shear Tool:
      - <span class="hotkeybg"><span class="hotkey" style="padding-left: 3px;">LMB
        ![../../images/Template-LMB.png](../../images/Template-LMB.png
        "../../images/Template-LMB.png")</span><span class="hotkey">Tweak
        north/south</span></span>: Y axis shear (else X axis shear)
  - Sculpt Mode:
      - <span class="hotkeybg"><span class="hotkey">A</span></span>:
        Mask edit pie menu
      - <span class="hotkeybg"><span class="hotkey">⇧
        Shift</span><span class="hotkey">A</span></span>: Mask expand
        from active index
      - <span class="hotkeybg"><span class="hotkey">⇧
        Shift</span><span class="hotkey">Alt</span><span class="hotkey">A</span></span>:
        Mask expand from active index, curvature mode
      - <span class="hotkeybg"><span class="hotkey">Ctrl</span><span class="hotkey">R</span></span>:
        Voxel remesh
      - <span class="hotkeybg"><span class="hotkey">Ctrl</span><span class="hotkey">Alt</span><span class="hotkey">R</span></span>:
        Quadriflow remesh

File Browser:

  - <span class="hotkeybg"><span class="hotkey">N</span></span>: Toggle
    operator options sidebar
  - <span class="hotkeybg"><span class="hotkey" style="padding-left: 3px;">LMB
    ![../../images/Template-LMB.png](../../images/Template-LMB.png
    "../../images/Template-LMB.png")</span></span>: Select item, or
    deselect on empty space
  - <span class="hotkeybg"><span class="hotkey" style="padding-left: 3px;">LMB
    ![../../images/Template-LMB.png](../../images/Template-LMB.png
    "../../images/Template-LMB.png")</span><span class="hotkey">Double-click</span></span>:
    Open
  - <span class="hotkeybg"><span class="hotkey">Ctrl</span><span class="hotkey" style="padding-left: 3px;">LMB
    ![../../images/Template-LMB.png](../../images/Template-LMB.png
    "../../images/Template-LMB.png")</span></span>: Add to selection
  - <span class="hotkeybg"><span class="hotkey">⇧
    Shift</span><span class="hotkey" style="padding-left: 3px;">LMB
    ![../../images/Template-LMB.png](../../images/Template-LMB.png
    "../../images/Template-LMB.png")</span></span>: Add to selection and
    fill in-between items
  - <span class="hotkeybg"><span class="hotkey" style="padding-left: 3px;">RMB
    ![../../images/Template-RMB.png](../../images/Template-RMB.png
    "../../images/Template-RMB.png")</span></span>: Context Menu
  - <span class="hotkeybg"><span class="hotkey" style="padding-left: 3px;">LMB
    ![../../images/Template-LMB.png](../../images/Template-LMB.png
    "../../images/Template-LMB.png")</span><span class="hotkey">Tweak</span></span>:
    Box-select
  - <span class="hotkeybg"><span class="hotkey">F2</span></span>: Rename

Grease Pencil:

  - <span class="hotkeybg"><span class="hotkey">⇧
    Shift</span><span class="hotkey">I</span></span>: Add blank frame
  - <span class="hotkeybg"><span class="hotkey">⇧
    Shift</span><span class="hotkey">X</span></span>,
    <span class="hotkeybg"><span class="hotkey">⇧
    Shift</span><span class="hotkey">Del</span></span>: Delete all
    active frames
  - <span class="hotkeybg"><span class="hotkey">Ctrl</span><span class="hotkey">Alt</span><span class="hotkey">E</span></span>:
    Interpolate
  - <span class="hotkeybg"><span class="hotkey">⇧
    Shift</span><span class="hotkey">Ctrl</span><span class="hotkey">E</span></span>:
    Interpolate Sequence
  - <span class="hotkeybg"><span class="hotkey">Alt</span><span class="hotkey">H</span></span>:
    Unhide
  - <span class="hotkeybg"><span class="hotkey">H</span></span>: Hide
    selected
  - <span class="hotkeybg"><span class="hotkey">⇧
    Shift</span><span class="hotkey">H</span></span>: Hide unselected
  - <span class="hotkeybg"><span class="hotkey" style="padding-left: 3px;">LMB
    ![../../images/Template-LMB.png](../../images/Template-LMB.png
    "../../images/Template-LMB.png")</span></span>: Create stroke
    material
  - <span class="hotkeybg"><span class="hotkey">⇧
    Shift</span><span class="hotkey" style="padding-left: 3px;">LMB
    ![../../images/Template-LMB.png](../../images/Template-LMB.png
    "../../images/Template-LMB.png")</span></span>: Create fill material
  - <span class="hotkeybg"><span class="hotkey">Ctrl</span><span class="hotkey" style="padding-left: 3px;">LMB
    ![../../images/Template-LMB.png](../../images/Template-LMB.png
    "../../images/Template-LMB.png")</span></span>: Create stroke and
    fill material

Text Editor:

  - <span class="hotkeybg"><span class="hotkey">Ctrl</span><span class="hotkey">/</span></span>:
    Toggle comments
  - <span class="hotkeybg"><span class="hotkey">Ctrl</span><span class="hotkey">G</span></span>:
    Find selected text

Video Sequencer:

  - <span class="hotkeybg"><span class="hotkey">2 NumPad</span></span>,
    <span class="hotkeybg"><span class="hotkey">4 NumPad</span></span>,
    <span class="hotkeybg"><span class="hotkey">8 NumPad</span></span>:
    Zoom 1:2, 1:4, 1:8 respectively
  - <span class="hotkeybg"><span class="hotkey">Ctrl</span><span class="hotkey">2
    NumPad</span></span>,
    <span class="hotkeybg"><span class="hotkey">Ctrl</span><span class="hotkey">4
    NumPad</span></span>,
    <span class="hotkeybg"><span class="hotkey">Ctrl</span><span class="hotkey">8
    NumPad</span></span>: Zoom 2:1, 4:1, 8:1 respectively
  - <span class="hotkeybg"><span class="hotkey">\[</span></span>,
    <span class="hotkeybg"><span class="hotkey">\]</span></span>: Select
    left/right of Playhead.

### Industry Compatible Keymap

For this keymap, an issue was fixed that made element mode keys break
when customising the keymap with additional shortcuts.

## Miscellaneous

  - Context Menus for Info and Console Editors.
    ([41f8f08e51](https://projects.blender.org/blender/blender/commit/41f8f08e5188))
  - Only hide locked transform manipulator axes for matching spaces.
    ([1857aa32bd](https://projects.blender.org/blender/blender/commit/1857aa32bd3b))
  - Changed the default drawing method of Vector sockets. They are now
    drawn expanded using a column layout.
    ([26911ba1](https://projects.blender.org/blender/blender/commit/26911ba1))
  - All selected elements are now properly moved when dragging keys and
    markers in the Dopesheet & Timeline, as well as strips in the Video
    Sequence Editor & NLA Editor. Previously this only worked in the
    node editors.  
    The Graph Editor is still missing this functionality, but will be
    added in the next release.
