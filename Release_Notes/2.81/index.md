# Blender 2.81 Release Notes

Blender 2.81 was released on November 21, 2019.

Check out the final [Release Notes on
blender.org](https://www.blender.org/download/releases/2-81/)

  

## [2.81a Corrective Release](a.md)

This corrective release does not include new features and only fixes a
few serious regressions introduced in 2.81 release.

## [Cycles](Cycles.md)

![../../images/Cycles\_displacement\_cracks.jpg](../../images/Cycles_displacement_cracks.jpg
"../../images/Cycles_displacement_cracks.jpg")

New shaders for texturing, denoising with OpenImageDenoise, and NVIDIA
RTX support.

  

## [Eevee](Eevee.md)

![../../images/Eevee2.81\_transparent\_bsdf1.png](../../images/Eevee2.81_transparent_bsdf1.png
"../../images/Eevee2.81_transparent_bsdf1.png")

Shadows, transparency and bump mapping redesigned for easier setup and
better quality.

  

## [Viewport](Viewport.md)

![../../images/Demonstrating\_Multi-layered\_Matcaps\_(Image\_from\_Pablo\_Dobarro).png](../../images/Demonstrating_Multi-layered_Matcaps_\(Image_from_Pablo_Dobarro\).png
"../../images/Demonstrating_Multi-layered_Matcaps_(Image_from_Pablo_Dobarro).png")

New options for look development with Cycles and Eevee.

  

## [Grease Pencil](Grease_Pencil.md)

![../../images/New\_Brushes.png](../../images/New_Brushes.png
"../../images/New_Brushes.png")

User interface, tools, operators, modifiers, new brushes and presets and
material self overlap.

  

## [Sculpt & Retopology](Sculpt.md)

New sculpt tools, poly build tool, voxel remesh and quad remesh.

## [Transform & Snapping](Transform.md)

![../../images/Snap\_Perpendicular.png](../../images/Snap_Perpendicular.png
"../../images/Snap_Perpendicular.png")

Transform origins, new snapping, mirroring and auto merge options.

  

## [User Interface](UI.md)

![../../images/2\_81\_file\_browser\_vertical\_list.png](../../images/2_81_file_browser_vertical_list.png
"../../images/2_81_file_browser_vertical_list.png")

Outliner improvements, new file browser and batch rename.

  

## [Library Overrides](Library_Overrides.md)

A new system to replace proxies, to make local overrides to linked
characters and other data types.

## [Animation & Rigging](Rigging.md)

Finer control over rotations and scale in bones, constraints and
drivers.

## [More Features](More_Features.md)

Alembic, audio & video output, library management and video sequencer
editor.

## [Python API](Python_API.md)

Python version upgrade, dynamic tooltip for operators, new handlers and
other API changes.

## [Add-ons](Add-ons.md)

Enabled add-ons only, glTF, FBX, import images as planes, blenderkit,
rigify among others.
