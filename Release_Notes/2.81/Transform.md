# Blender 2.81: Transform & Snapping

## Transform

### Affect Origins

It's now possible to move object origins directly.

This is an alternative method of adjusting the origin, previously only
possible placing the cursor and setting an objects origins to the
cursor.

This only works for objects with data which can be transformed.

This is accessible from the Options panel.

### Affect Parents (Skip Children)

It's now possible to transform parents without affecting their children.

This is accessible from the Options panel.

### Mirror

Mirroring over the Y and Z axis is now supported, along with the
existing X axis option. Multiple axes can also be enabled
simultaneously.
([3bd4f22](https://projects.blender.org/blender/blender/commit/3bd4f22))

![Mirror now has X, Y and Z options](../../images/Transform_mirror.png
"Mirror now has X, Y and Z options")

## Snap

### Edge Snapping Options

Two new snap options have been added:

  - Edge Center, to snap to the middle of an edge.
    ([5834124](https://projects.blender.org/blender/blender/commit/5834124));
  - Edge Perpendicular, to snap to the nearest point on an edge.
    ([dd08d68](https://projects.blender.org/blender/blender/commit/dd08d68));

<center>

|                                                                                                                                             |                                                                                                   |
| ------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------- |
| ![Snap modes Edge Center and Perpendicular](../../images/Snap_Edge_Center_and_Perpendicular.png "Snap modes Edge Center and Perpendicular") | ![Edge Perpendicular snapping](../../images/Snap_Perpendicular.png "Edge Perpendicular snapping") |

</center>

### Auto Merge

When auto merging vertices, adjacent edges and faces can now be
automatically split to avoid overlapping geometry. This is controlled by
the new "Split Edges & Faces" option for Auto Merge.
([6b189d2](https://projects.blender.org/blender/blender/commit/6b189d2))

![Auto merge and split
faces](../../images/Blender2.81_auto_merge_split.png
"Auto merge and split faces")
