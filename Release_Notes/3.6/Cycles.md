# Cycles

## GPU Rendering

### AMD

Experimental support for AMD hardware ray-tracing acceleration on
Windows, using [HIP RT](https://gpuopen.com/hiprt/).
([557a245](https://projects.blender.org/blender/blender/commit/557a245))

  - This improves performance on GPUs with ray tracing acceleration - RX
    6000 and RX 7000 series, as well as W6000 and W7000 series
    workstation GPUs.
  - Driver version [22.40.51.06 for Blender 3.6
    Beta](https://www.amd.com/en/support/kb/release-notes/rn-rad-win-22-40-51-06-blender-3-6-beta)
    needs to be installed, along with enabling HIP RT in Preferences \>
    System.
  - Known limitations:
      - No Linux support, as HIP RT is Windows only still.
      - Degenerate triangles may causes crashes or poor performance.
      - Shadows in hair are not rendering accurately.

AMD GPUs now also support light trees.
([d5757a0](https://projects.blender.org/blender/blender/commit/d5757a0))

### Intel

Hardware ray-tracing acceleration for Intel® Arc™ and Data Center GPUs,
using [Embree 4](https://www.embree.org/).
([3f8c995109](https://projects.blender.org/blender/blender/commit/3f8c995109))

<table>
<tbody>
<tr class="odd">
<td><figure>
<img src="../../images/Cycles_3.6_Intel_A770.png" title="Render time per sample on an Intel® Arc™ A770 GPU|thumb|center" alt="Render time per sample on an Intel® Arc™ A770 GPU|thumb|center" width="800" /><figcaption>Render time per sample on an Intel® Arc™ A770 GPU|thumb|center</figcaption>
</figure></td>
<td><table>
<thead>
<tr class="header">
<th><p>Scene</p></th>
<th><p>With HW RT</p></th>
<th><p>Without HW RT</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>barbershop_interior</p></td>
<td><p>0.192991</p></td>
<td><p>0.223141</p></td>
</tr>
<tr class="even">
<td><p>bmw27</p></td>
<td><p>0.013019</p></td>
<td><p>0.015585</p></td>
</tr>
<tr class="odd">
<td><p>classroom</p></td>
<td><p>0.112879</p></td>
<td><p>0.139789</p></td>
</tr>
<tr class="even">
<td><p>fishy_cat</p></td>
<td><p>0.014602</p></td>
<td><p>0.024707</p></td>
</tr>
<tr class="odd">
<td><p>junkshop</p></td>
<td><p>0.101691</p></td>
<td><p>0.119027</p></td>
</tr>
<tr class="even">
<td><p>monster</p></td>
<td><p>0.057748</p></td>
<td><p>0.067487</p></td>
</tr>
<tr class="odd">
<td><p>pabellon</p></td>
<td><p>0.045524</p></td>
<td><p>0.066214</p></td>
</tr>
<tr class="even">
<td><p>sponza</p></td>
<td><p>0.023778</p></td>
<td><p>0.031841</p></td>
</tr>
</tbody>
</table></td>
</tr>
</tbody>
</table>

  - Known limitations:
      - During their first use with oneAPI device, Ambient Occlusion and
        Bevel nodes will trigger a GPU binaries recompilation that will
        use around 9GB of memory and take several minutes. Improvements
        on this may come from future GPU drivers.
      - On Windows, when using drivers \< 101.4644, embree on GPU may
        crash when rendering duration goes beyond 3 minutes.

### Apple

Apple Silicon GPUs now support NanoVDB for Metal, reducing memory usage
of volumes.
([02c2970983](https://projects.blender.org/blender/blender/commit/02c2970983))

## Performance

  - Light trees now use less memory and are faster to build, through
    instancing and multi-threading.
    ([bfd1836861](https://projects.blender.org/blender/blender/commit/bfd1836861),
    [23c5e06932](https://projects.blender.org/blender/blender/commit/23c5e0693253516a51906588a361bb637ffd391b))
  - Loading large geometries into Cycles is much faster, meaning
    rendering can start more quickly after geometry changes or switching
    to rendered view.
      - Loading large meshes 4-6x faster, and mesh attributes are copied
        up to 10x faster (or even 60x faster for an extreme example with
        UV maps)
        ([4bcd59d644](https://projects.blender.org/blender/blender/commit/4bcd59d644c4cc50692e60899403e3f60ac409de),
        [8d0920ec6d](https://projects.blender.org/blender/blender/commit/8d0920ec6dcd3fc7fc6b3d7dd76872801dcefa29)).
      - Loading point clouds can be 9x faster
        ([aef0e72e5a](https://projects.blender.org/blender/blender/commit/aef0e72e5acccf5f86cabe17dc49d7af473901e9)).
      - Loading curves can be 10x faster
        ([ae017b3ab7](https://projects.blender.org/blender/blender/commit/ae017b3ab7fb119d8a3327f880f74ce2c31a57f9)).

## Other Improvements

  - OSL: support for new standard microfacet closures from MaterialX
    (\`dielectric\_bsdf\`, \`conductor\_bsdf\`,
    \`generalized\_schlick\_bsdf\`).
  - Byte color attributes are now supported for point clouds and curves
    ([5a86c4cc88](https://projects.blender.org/blender/blender/commit/5a86c4cc886ef25bf6f1fd61b5b9c706bba93de0)).
  - Improved Fresnel handling of the Glass BSDF for better energy
    preservation and accuracy of results at high roughness.
    ([D17149](http://developer.blender.org/D17149))
  - Bump mapping for diffuse surfaces was improved. ([PR
    \#105776](https://projects.blender.org/blender/blender/pulls/105776))
