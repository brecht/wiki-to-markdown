# Asset Bundles

## Human Base Meshes

Download the bundle
[here](https://www.blender.org/download/demo-files/#assets).

These are various human base meshes that are provided by the Blender
community and the Blender Studio. They can be used as a base for various
tasks (sculpting, modeling, texturing, rigging, simulation, etc) and
include:

  - Quad topology for subdividing
  - Closed volumes for voxel remeshing
  - Face sets
  - UV maps (including UDIMs)
  - Multi-resoluton details for realistic assets
  - Creased edges and Subdiv modifiers for planar assets

![../../images/Thumbnail.png](../../images/Thumbnail.png
"../../images/Thumbnail.png")
