# Add-ons

## Storypencil

  - It is now possible to create many scene strips at once.
    [27970e4b8b](https://projects.blender.org/blender/blender-addons/commit/27970e4b8b556d99cf3476e52c52d2d00628092d)
  - Support to render meta strip as one video. Previously, a video was
    generated per scene in the meta strip.

## New: VDM Brush Baker

A new add-on to easily create VDM brushes has been added. Previously
available as part of [a demo
file](https://www.blender.org/download/demo-files/#sculpting).

![../../images/Vdm\_brush\_baker\_screenshot.png](../../images/Vdm_brush_baker_screenshot.png
"../../images/Vdm_brush_baker_screenshot.png")

## New: 3DS I/O

Support for importing and exporting the legacy \`3ds\` format is back
since it is still used by many applications and was missed by many
Blender users. ([PR
\#104493](https://projects.blender.org/blender/blender-addons/pulls/104493))

Animations and almost all 3ds definitions can be imported and exported.
([2dc2c1c3e4](https://projects.blender.org/blender/blender-addons/commit/2dc2c1c3e4),
[6525a57a32](https://projects.blender.org/blender/blender-addons/commit/6525a57a32),
[febc4b4433](https://projects.blender.org/blender/blender-addons/commit/febc4b4433),
[859a2c1b9a](https://projects.blender.org/blender/blender-addons/commit/859a2c1b9a))

All backporting changes are listed in the [commit
details](https://projects.blender.org/blender/blender-addons/commit/c7a4b16aed).

## Collection Manager

Ported the QCD Move Widget to solely use the \`gpu\` API for drawing,
replacing the deprecated \`bgl\` API.
([be6cbe0222](https://projects.blender.org/blender/blender-addons/commit/be6cbe0222))

### Bug Fixes

  - Fixed the QCD Move Widget not dragging.
    ([14595effe5](https://projects.blender.org/blender/blender-addons/commit/14595effe5))
  - Fixed an error when toggling off the only QCD slot that contains the
    active object.
    ([bee6a356c6](https://projects.blender.org/blender/blender-addons/commit/bee6a356c6))
  - Fixed an error when restoring disabled objects.
    ([06328e8aea](https://projects.blender.org/blender/blender-addons/commit/06328e8aea))
