# Sculpt, Paint, Texture

## Features

  - Add support for "Adjust Last Operation" panel to mesh & color
    filters
    ([c352eeb213](https://projects.blender.org/blender/blender/commit/c352eeb2134))
    ([b4ee936627](https://projects.blender.org/blender/blender/commit/b4ee9366278a8159af5e2b97bfc18ccc29354aa3))

![../../images/Filter\_repeat\_last.png](../../images/Filter_repeat_last.png
"../../images/Filter_repeat_last.png")

  - Transform, Trim, Project, Fairing and Filter operations are now also
    available in the header menu
    ([da65b21e2e](https://projects.blender.org/blender/blender/commit/da65b21e2e0))

This makes it possible to easily assign these operation a shortcut or
add them to the Quick Favorites menu for faster access.

Repeated use is possible with Repeat Last (\`Shift+R\`).

![../../images/Sculpt\_menu\_update.png](../../images/Sculpt_menu_update.png
"../../images/Sculpt_menu_update.png")

  - Added trim orientation to Box Trim UI. Previously only available in
    the Lasso Trim tool
    ([a843a9c9bb](https://projects.blender.org/blender/blender/commit/a843a9c9bb8a25f123752fcd6ca68f4694974a44))
