# Pipeline, Assets & I/O

## Assets

  - It is now possible to use relative path when importing assets.
  - The option exists in the Preferences per asset library
    ([5d0595fded](https://projects.blender.org/blender/blender/commit/5d0595fded69)).

![../../images/Asset\_library\_relative\_path.jpg](../../images/Asset_library_relative_path.jpg
"../../images/Asset_library_relative_path.jpg")

## Stanford PLY

  - New C++ based PLY importer and exporter
    ([43e9c900](https://projects.blender.org/blender/blender/commit/43e9c900),
    [6c6edaf5](https://projects.blender.org/blender/blender/commit/6c6edaf5),
    [48496f14](https://projects.blender.org/blender/blender/commit/48496f14)).
    About 4x-20x faster export, 8x-30x faster import compared to the
    Python addon. It also fixes and improves some things:
      - Importing point clouds with vertex colors now works
      - Importing PLY files with non standard line endings
      - Exporting multiple objects (previous exporter didn't take the
        vertex indices into account)
      - The importer has the option to merge vertices
      - The exporter supports exporting loose edges and vertices along
        with UV map data
      - Vertex colors can be treated as either sRGB or Linear color
        space.
      - Importer supports models that use "tristrips" element instead of
        "face".

## USD

  - \`Path Mask\` import option now supports multiple primitive paths
    ([239af1705d](https://projects.blender.org/blender/blender/commit/239af1705d)).
  - Author \`opacityThreshold\` \`USD Preview Surface\` material
    attribute when exporting USD
    ([3c74575dac](https://projects.blender.org/blender/blender/commit/3c74575dac)).
  - Fixed incorrect texture alpha mapping when exporting \`USD Preview
    Surface\` materials
    ([109c1b92cd](https://projects.blender.org/blender/blender/commit/109c1b92cd)).
  - New \`Prim Path\` USD export option to add a root transform
    primitive to the stage
    ([42342124f8](https://projects.blender.org/blender/blender/commit/42342124f8)).
  - Set \`emissiveColor\` \`USD Preview Surface\` material input when
    exporting USD
    ([ce83b26a33](https://projects.blender.org/blender/blender/commit/ce83b26a33)).
  - Fixed bug creating duplicate shader nodes when importing USD
    ([74b5e62d2a](https://projects.blender.org/blender/blender/commit/74b5e62d2ae)).
  - New Curves/Hair Export Support
    ([1164976dd8](https://projects.blender.org/blender/blender/commit/1164976dd8)).

## FBX

  - Geometry export performance was made faster by using Python's numpy
    ([994c4d91](https://projects.blender.org/blender/blender-addons/commit/994c4d91),
    [6054d1be](https://projects.blender.org/blender/blender-addons/commit/6054d1be),
    [aa7d1d71](https://projects.blender.org/blender/blender-addons/commit/aa7d1d71),
    [4269b4ad](https://projects.blender.org/blender/blender-addons/commit/4269b4ad),
    [d1556507](https://projects.blender.org/blender/blender-addons/commit/d1556507),
    [3e783620](https://projects.blender.org/blender/blender-addons/commit/3e783620),
    [58740ec8](https://projects.blender.org/blender/blender-addons/commit/58740ec8),
    [2a0791a4](https://projects.blender.org/blender/blender-addons/commit/2a0791a4)).
    E.g. exporting Snow (without any animation) is about twice as fast
    as before.
  - Geometry import performance was made faster by using Python's numpy
    ([47da0ad56b](https://projects.blender.org/blender/blender-addons/commit/47da0ad56b),
    [5da3d41c27](https://projects.blender.org/blender/blender-addons/commit/5da3d41c27),
    [66390ced12](https://projects.blender.org/blender/blender-addons/commit/66390ced12),
    [9859e253b5](https://projects.blender.org/blender/blender-addons/commit/9859e253b5),
    [2438ae5c27](https://projects.blender.org/blender/blender-addons/commit/2438ae5c27)).
  - It is now possible to export the active color layer in first
    position, in case other software only import one of these
    ([4c397ede](https://projects.blender.org/blender/blender-addons/commit/4c397ede)).

## glTF 2.0

### Import

  - Fix zero sum weights for skinning
    ([83357ab3b9](https://projects.blender.org/blender/blender-addons/commit/83357ab3b923a9b5bfe343959fe61bf2bb726f80))
  - Fix empty shapekey names
    ([6ac724926a](https://projects.blender.org/blender/blender-addons/commit/6ac724926a7508955fe9ee879fcd870518660e6e))
  - Fix custom attribute import when vertices are merged or shared
    accessors
    ([2403f4c6b9](https://projects.blender.org/blender/blender-addons/commit/2403f4c6b90c64c894ab1d210b73ef6fea93e318),
    [45bfb99ffe](https://projects.blender.org/blender/blender-addons/commit/45bfb99ffe4ab5bbc78c0464011d84eed1e37c0b))

### Export

  - Big animation refactoring
    ([8dfe73800d](https://projects.blender.org/blender/blender-addons/commit/8dfe73800d5cb6cb27e2c078c4e5f4e27f684034))
  - Export right materials when changed by modifiers
    ([8b57a74629](https://projects.blender.org/blender/blender-addons/commit/8b57a74629333852f3f7cbbafee63d747ff0c989))
  - Fix normal normalization
    ([0920b3e329](https://projects.blender.org/blender/blender-addons/commit/0920b3e329073ed09ec1e28995300a9633840bf5))
  - Reset sk values when switching sk action
    ([0086ce9d67](https://projects.blender.org/blender/blender-addons/commit/0086ce9d67048900bcc09e05202bd574961d7b85))
  - Add hook to change primitive attribute order
    ([874240f275](https://projects.blender.org/blender/blender-addons/commit/874240f2753eaad4a2b2105bba120d395dde6f27))
  - Fix uri encoded, only for uri, not filename
    ([8c77651799](https://projects.blender.org/blender/blender-addons/commit/8c776517997c9ef07d3d4dc2ad87e7402d1f2677))
  - Convert light option was not saved
    ([17f1e4d848](https://projects.blender.org/blender/blender-addons/commit/17f1e4d8481cd7ce1d35aad9654308f535fca4f0))
  - Avoid crash when sequence image (not managed by glTF)
    ([789a200cd7](https://projects.blender.org/blender/blender-addons/commit/789a200cd7eaf7247b83bc91fffe402385d9a90f))
  - Fix exporting children of instance collections
    ([932ea1d719](https://projects.blender.org/blender/blender-addons/commit/932ea1d71928ddba2d026a3b32a19f5d56239808))
  - Avoid crash when collision name in attributes
    ([e2b556cbe8](https://projects.blender.org/blender/blender-addons/commit/e2b556cbe8dfbe279fb9ef3ae41591709213e942))
  - Initialize filter when user saved export preferences
    ([776b4a3a3f](https://projects.blender.org/blender/blender-addons/commit/776b4a3a3f4119108489c852e231479ff89227ec))
  - Add a hook for node name
    ([f12999046e](https://projects.blender.org/blender/blender-addons/commit/f12999046e9823cff9ea88fbc12fdbf20ee4bf3d))
