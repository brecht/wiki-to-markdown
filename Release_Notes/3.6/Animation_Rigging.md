# Animation & Rigging

## General

  - Slider Operators are now available with hotkeys. ALT+S for smoothing
    operators and ALT+D for blending operators. [PR
    \#107866](https://projects.blender.org/blender/blender/pulls/107866)
    Animation: change Slider Popup menu hotkeys
  - The iTaSC IK solver now supports keeping root bones (i.e. without
    parent) positioned where they are, instead of always moving them to
    the armature's origin. For backward compatibility the old behavior
    is retained; new armatures will get the new behavior. This can be
    controlled via the new "Translate Roots" checkbox.
    ([51c2a0f816](https://projects.blender.org/blender/blender/commit/51c2a0f81606d2920146bf3fd3e753148d7c7aca))
  - \`Ctrl-LMB\` shortcut for channel renaming is removed. It is now
    used for Extend channel selection
    ([6c4da68ad2](https://projects.blender.org/blender/blender/commit/6c4da68ad2320608798e6418b5bdd5222053686d)).

### Extend Paint Mask Selection

Add the option to grow/shrink the selection just as in Edit Mode. [PR
\#105633](https://projects.blender.org/blender/blender/pulls/105633) [PR
\#105607](https://projects.blender.org/blender/blender/pulls/105607)
![../../videos/Extend\_secletion\_demo.mp4](../../videos/Extend_secletion_demo.mp4
"../../videos/Extend_secletion_demo.mp4")

### Duration Display in the Status Bar

It is now possible to display the scene duration in the status bar. The
format is the following: \`Duration: <timecode> (Frame
<current frame>/<total frames>)\` The timecode format follows the user
preferences. [PR
\#104882](https://projects.blender.org/blender/blender/pulls/104882)

![Scene Duration Display](../../images/Scene_duration.png
"Scene Duration Display")

### Frame Channels Operator

Allows to frame onto the contents of a channel, or a selection of
channels. Works in the Graph Editor and the Dope Sheet. [PR
\#104523](https://projects.blender.org/blender/blender/pulls/104523) It
is called with "." on the numpad or right click the channels and choose
the "Frame Selected Channels" option. Alternatively press ALT+MMB to
frame the channel under the cursor.

![../../videos/Blender-3-6-frame-channel-demo.mp4](../../videos/Blender-3-6-frame-channel-demo.mp4
"../../videos/Blender-3-6-frame-channel-demo.mp4")

### Copy Global Transform

The Copy Global Transform add-on can now also mirror the transform
([606e4b35f2](https://projects.blender.org/blender/blender-addons/commit/606e4b35f2)).

![Options of Copy Global
Transform](../../images/Blender-36-copy-global-transform-mirror.png
"Options of Copy Global Transform")

  - Object but no bone name: the transform will be mirrored relative to
    the object
  - Object and bone name: the transform will be mirrored relative to
    that specific bone of that specific armature
  - Bone name but no object: the transform will be mirrored relative to
    the named bone of the current armature

### Bone Relation Lines

Instead of always drawing the relationship line from the head of the
child to the *tail* of the parent, there is now choice for the parent
side of the line. This can be toggled between the *head* and the *tail*
of the bone, where the tail is Blender's original behaviour and is
retained as default value
([45c1deac4f](https://projects.blender.org/blender/blender/commit/45c1deac4f61af39f58678d96cfa7447cea4e4a4)).

![Options of bone relation
lines](../../images/Blender-36-bone-relation-lines-options.png
"Options of bone relation lines")

![Different bone relation
lines](../../images/Blender-36-bone-relation-lines.png
"Different bone relation lines")

### Context properties

[PR \#105132](https://projects.blender.org/blender/blender/pulls/105132)
Drivers: Introduce the Context Properties

A special type for driver variable has been added: the Context Property.

The property is implicitly referring to either a scene or a view layer
of the currently evaluating animation system. This is a weak reference
which does not lead to the scene or view layer referenced from the
driver to be linked when linking animation data.

An example when such properties comes in play is referring to a
transformation of the active camera. It is possible to setup driver in a
character file, and make the driver use the set camera when the
character is linked into a set.

### New Parent Space transformation

[PR \#104724](https://projects.blender.org/blender/blender/pulls/104724)
Animation: New parent space transformation aligns child objects /
armature bones to parent space.

![Child object using parent space transformation
gizmo](../../images/Parent_space_object.png
"Child object using parent space transformation gizmo")

![Child bone using parent space transformation
gizmo](../../images/Blender-36-parent-space-bone.png
"Child bone using parent space transformation gizmo")

## Graph Editor

### Settings moved to the User Preferences

Two view options in the Graph Editor have been moved to the User
Preferences (Only Selected Curve Keyframes, Use High Quality Display).
([PR
\#104532](https://projects.blender.org/blender/blender/pulls/104532)
Animation: Move Graph Editor settings to User Preferences)

![../../images/Moved\_Graph\_Editor\_Settings.png](../../images/Moved_Graph_Editor_Settings.png
"../../images/Moved_Graph_Editor_Settings.png")

### Key menu cleaned up

The "Key" menu in the Graph Editor has been cleaned up. Things that have
to do with channels were moved into the "Channels" menu. The rest has
been grouped into sub-menus where possible. [PR
\#106113](https://projects.blender.org/blender/blender/pulls/106113)
Animation: Clean up "Key" menu in Graph Editor

![../../images/Key\_menu.png](../../images/Key_menu.png
"../../images/Key_menu.png")

### Gaussian Smooth Operator

[PR \#105635](https://projects.blender.org/blender/blender/pulls/105635)
A new operator to smooth keyframe data. It improves on the current
implementation in the following ways.

  - Supports modal operations
  - Is independent of key density
  - More options in the redo panel
  - Smooths sudden spikes more predictable

![Demo of the Gaussian Smooth operator](../../videos/Gaussian_smooth.mp4
"Demo of the Gaussian Smooth operator")

### Keyframes

  - Add option to insert keyframes only on the active F-Curve. [PR
    \#106307](https://projects.blender.org/blender/blender/pulls/106307)
  - Pasting keys in the Graph Editor now has value offset options. [PR
    \#104512](https://projects.blender.org/blender/blender/pulls/104512)

### Small Tweaks

  - When hitting normalize, the y extents of visible F-Curves are frame
    automatically. [PR
    \#105857](https://projects.blender.org/blender/blender/pulls/105857)
  - When the view is normalized, the area outside of -1/1 is drawn
    slightly darker. [PR
    \#106302](https://projects.blender.org/blender/blender/pulls/106302)
  - Ignore hidden curves when jumping to keyframes in the Graph Editor.
    [PR
    \#108549](https://projects.blender.org/blender/blender/pulls/108549)

## Dope Sheet

  - Added the pin icon to the channels in the Dope Sheet
    [rB49ad91b5ab7b](http://projects.blender.org/scm/viewvc.php?view=rev&root=bf-blender&revision=B49ad91b5ab7b)
  - Clamp the viewport in the Dope Sheet so channels cannot go off
    screen. [PR
    \#104516](https://projects.blender.org/blender/blender/pulls/104516)

## NLA

  - Clicking the NLA/Graph Editor search box no longer scrolls the items
    below it.
    ([d1219b727c](https://projects.blender.org/blender/blender/commit/d1219b727c)).

` `![`NLA``   ``solo``   ``button``   ``now``   ``right``   ``aligned`` 
 `](../../images/NLA_solo_button_change.png
"NLA solo button now right aligned ")

  - NLA track Solo button has been moved on the to be right aligned to
    group it logically with Mute, Lock track
    buttons.([d8024135a5](https://projects.blender.org/blender/blender/commit/d8024135a5)).
  - NLA strips can now be dragged horizontal through other strips on the
    same track
    ([8833f5dbf9](https://projects.blender.org/blender/blender/commit/8833f5dbf9)).

` `![`NLA``   ``horizontal``   ``drag``   ``and`` 
 ``drop`](../../videos/NLA_horizontal_drag_and_drop.mp4
"NLA horizontal drag and drop")

## Python API

### FCurve key deduplication + exposing the innards of \`fcurve.update()\`

New functions on \`fcurve.keyframe\_points\`
([85ed2e8c36](https://projects.blender.org/blender/blender/commit/85ed2e8c368bb624166d5082df79faa9d983573e)):

\- \`deduplicate()\`: Removes duplicate keys (i.e. when there are
multiple keys on the same point in time) by retaining the last key of
those duplicates based on their index in the \`fcurve.keyframe\_points\`
array. - \`sort()\`: Ensure the keys are chronologically sorted. -
\`handles\_recalc()\`: Recalculate the handles, to update 'auto'-type
handles.

Calling \`fcurve.update()\` actually performs \`sort()\` and then
\`handles\_recalc()\`, and now those two steps are exposed as individual
functions as well.

Note that in Blender 4.0 \`fcurve.update()\` will also be calling the
\`deduplicate()\` function.
