# Nodes & Physics

## Geometry Nodes

### Simulation Nodes

  - Geometry nodes now has support for simulations
    ([0de54b84c6](https://projects.blender.org/blender/blender/commit/0de54b84c6f0e64e145db2356048fea63a734da9)).
      - Simulations are created with the new **Simulation Input** and
        **Simulation Output** nodes.
      - On the first frame, the inputs of the *Simulation Input* node
        are evaluated. In later frames the inputs aren't evaluated
        anymore. On later frames, the node outputs the result of the
        previous frame.
      - The *Simulation Output* node saves the state for the next frame.
      - The simulation's results can be cached or baked to storage,
        which is controlled with a new panel in the physics tab in the
        property editor and visualized in the timeline.
          - All baked data is stored on disk in a folder next to the
            .blend file.
          - The timeline indicates which frames are currently cached,
            baked or cached but invalidated by user-changes.
  - [Simulation Nodes and Index of Nearest demo
    file](https://www.blender.org/download/demo/geometry-nodes/index_of_nearest.blend).

![Index of nearest sample file, CC-BY Sean
Christofferson|800px](../../images/Index_of_Nearest_Sample_File.jpg
"Index of nearest sample file, CC-BY Sean Christofferson|800px")

### General

  - A new **Index of Nearest** node gives index of the closest
    "non-self" element
    ([15f9e42c4f](https://projects.blender.org/blender/blender/commit/15f9e42c4f687d31f31b84dadcb7decdb4883498),
    [d8f4387ac9](https://projects.blender.org/blender/blender/commit/d8f4387ac92),
    [4346314351](https://projects.blender.org/blender/blender/commit/4346314351d)).
  - The legacy read-only "normal" attribute has been removed from the
    spreadsheet
    ([300c673a64](https://projects.blender.org/blender/blender/commit/300c673a6494b887e)).
  - Confusing units have been removed on some inputs to the *String to
    Curves* node
    ([4f08eeae9c](https://projects.blender.org/blender/blender/commit/4f08eeae9cb)).

### Performance

  - Blender and geometry nodes make use of a new system to avoid copying
    large data chunks, called "implicit sharing"
    ([7eee378ecc](https://projects.blender.org/blender/blender/commit/7eee378eccc),
    [dcb3b1c1f9](https://projects.blender.org/blender/blender/commit/dcb3b1c1f9c)).
      - Generally copying geometry to change part of it is much faster,
        and overal memory usage can be significantly better as well (at
        least 25% in simple situations).
      - Avoiding copies when converting geometry types can make the
        *Instance on Points*, *Instances to Points*, *Points to
        Vertices* and *Mesh to Points* nodes at least 10x faster
        ([e45ed69349](https://projects.blender.org/blender/blender/commit/e45ed69349a)).
      - Copies can also be skipped when duplicating attributes with the
        *Store Named Attribute* and *Capture Attribute* nodes
        ([b54398c16c](https://projects.blender.org/blender/blender/commit/b54398c16cf)).
  - Blender now caches loose edges and loose vertices for meshes, making
    drawing large meshes in the viewport and other operations faster
    after some node setups.
      - The *Subdivision Surface* node tags meshes with no loose
        edges/vertices
        ([54072154c5](https://projects.blender.org/blender/blender/commit/54072154c5b)).
      - Primitive nodes, the *Realize Instances* node, and the *Curve to
        Mesh* node now use precomputed this data too, saving hundreds of
        milliseconds for large setups
        ([8e967cfeaf](https://projects.blender.org/blender/blender/commit/8e967cfeaf4),
        [00bb30c0e9](https://projects.blender.org/blender/blender/commit/00bb30c0e96),
        [63689e4756](https://projects.blender.org/blender/blender/commit/63689e47562)).
  - The mesh bounding box is pre-calculated for primitive nodes, saving
    time calculating it later
    ([a1f52a02a8](https://projects.blender.org/blender/blender/commit/a1f52a02a87)).
      - Recomputing bounds can be skipped after translating a mesh
        ([59c0e19db2](https://projects.blender.org/blender/blender/commit/59c0e19db26))
  - Drawing curves selection data in edit mode is up to 3.8x faster
    ([70d854538b](https://projects.blender.org/blender/blender/commit/70d854538b7)).
  - Improvements to the *Mesh to Curve* node improved FPS in a test by
    10%
    ([98ccee78fe](https://projects.blender.org/blender/blender/commit/98ccee78fe8)).
  - The *Curve to Mesh* node is a few milliseconds faster in a test with
    1 million curves
    ([52eced3eef](https://projects.blender.org/blender/blender/commit/52eced3eef4)).

## Node Editor

  - A shortcut to the Online Manual is now included in the node context
    menu
    ([e95ba8a70e](https://projects.blender.org/blender/blender/commit/e95ba8a70e1922fc8a)).
  - A new dropdown allows select group socket subtypes
    ([e7f395dd20](https://projects.blender.org/blender/blender/commit/e7f395dd206dcd46ff9d8e89682b33cd9e95abe7)).

` `![`../../images/3.6_Node_Group_Socket_Subtype.png`](../../images/3.6_Node_Group_Socket_Subtype.png
"../../images/3.6_Node_Group_Socket_Subtype.png")

  - Link drag search can now move data-block default values when
    creating group inputs and the *Image* node
    ([9726e4a0ad](https://projects.blender.org/blender/blender/commit/9726e4a0adb30e452327866aae1af7316ac0e0ab)).
  - Link drag search can copy values of basic socket types like vectors
    as well
    ([71e4f48180](https://projects.blender.org/blender/blender/commit/71e4f48180b)).
