# Animation & Rigging

## General

  - The old (pre-3.0) pose library has been removed
    ([rB48b5dcdb](https://projects.blender.org/blender/blender/commit/48b5dcdbe857)).
    They are now seen by Blender as simply Actions with named markers.
    It is recommended to [convert them to pose
    assets](https://docs.blender.org/manual/en/3.5/animation/armatures/posing/editing/pose_library.html#converting-old-pose-libraries).
  - Fixed an old bug where the effect of the Invert Vertex Group toggle
    of the Armature modifier was inverted when Multi-Modifier was
    active. Old tutorials explaining the usage of the Multi-Modifier
    option will need updating.
    ([rBea1c31a2](https://projects.blender.org/blender/blender/commit/ea1c31a24438))
  - Added the pin icon to the Dope Sheet to pin channels.
    ([rB49ad91b5](https://projects.blender.org/blender/blender/commit/49ad91b5ab7b))
  - Added "Select Linked Vertices" to weight paint mode.
    ([rB04aab7d5](https://projects.blender.org/blender/blender/commit/04aab7d51620))
  - Take subframes into account when jumping to next/previous keyframes.
    ([rB5eab813f](https://projects.blender.org/blender/blender/commit/5eab813fc078))
  - Motion paths have a new frame range option "Manual Range" to ensure
    the frame range is never changed on update
    ([rBd72c7eef](https://projects.blender.org/blender/blender/commit/d72c7eefd1c5)).
    In other modes ("All Keys", "Selected Keys", "Scene Frame Range")
    the start/end frame are still editable, but greyed out.
  - Adding F-Curve modifiers to multiple channels at once is now easier
    to access
    ([rB0f51b5b5](https://projects.blender.org/blender/blender/commit/0f51b5b599bb)).

![Motion Paths frame range
options](../../images/Blender-3.5-anim-motion-paths.webp
"Motion Paths frame range options")

## Asset Browser and Pose Library

The Pose Library went through a few usability changes to unify the
experience of the other asset types
([rBAc164c5d](https://projects.blender.org/blender/blender-addons/commit/c164c5d86655)).

![../../images/Poselib\_assetbrowser.png](../../images/Poselib_assetbrowser.png
"../../images/Poselib_assetbrowser.png")

The Pose Library functionality in the asset browser were moved to a new
Asset menu, as well as the pose asset context menu.

![../../images/Poselib\_contextmenu.png](../../images/Poselib_contextmenu.png
"../../images/Poselib_contextmenu.png")

In the viewport, a few options were removed:

  - The option to Create Pose Assets is no longer there - use instead
    the Action Editor or the Asset Browser.
  - The Flip Pose check-box is gone - flipped poses can be applied
    directly via the context menu. When blending, keep CTRL pressed to
    blend the flipped pose.
  - The \`poselib.apply\_pose\_asset\_for\_keymap\` and
    \`poselib.blend\_pose\_asset\_for\_keymap\` operators are gone. If
    you have assigned these in your keymap, use the regular apply/blend
    operators (\`poselib.blend\_pose\_asset\` and
    \`poselib.apply\_pose\_asset\`) instead.

Other improvements are:

  - A pose asset can now be "subtracted" while blending. Drag to the
    right to blend as usual, **drag to the left to subtract the pose**
    ([rc82311bb8efd24abec6f50a18256c636b78ef626](http://projects.blender.org/scm/viewvc.php?view=rev&root=bf-blender&revision=c82311bb8efd24abec6f50a18256c636b78ef626)).
  - While blending, keep Ctrl pressed to flip the pose
    ([rBbd36c712](https://projects.blender.org/blender/blender/commit/bd36c712b928f522)).
  - Blending can now also exaggerate a pose, by pressing E (for
    Extrapolate) and applying a pose for more than 100%
    [rB74c4977a](https://projects.blender.org/blender/blender/commit/74c4977aeaa3).

![../../videos/Blender-3.5-poselib-bidirectional-blending.mp4](../../videos/Blender-3.5-poselib-bidirectional-blending.mp4
"../../videos/Blender-3.5-poselib-bidirectional-blending.mp4")

## Graph Editor

#### Ease Operator

The Graph Editor got a new Operator, "Ease"
([76a68649c1](https://projects.blender.org/blender/blender/commit/76a68649c1c1)),
that can align keys on an exponential curve. This is useful for quickly
making an easing transition on multiple keys. It can be found under
<span class="literal">Key</span> » <span class="literal">Slider
Operators</span>.

![../../videos/Graph\_Editor\_Ease\_Operator.mp4](../../videos/Graph_Editor_Ease_Operator.mp4
"../../videos/Graph_Editor_Ease_Operator.mp4")

## Pose Mode

#### Propagate Pose

The Propagate Pose Operator has been updated
([200a114e15](https://projects.blender.org/blender/blender/commit/200a114e1596)).
Previously it used to evaluate each FCurve individually to find the
target key. That resulted in undesired behavior when the keys where not
on the same frame. Now it finds the frame first by checking all FCurves
in question and then propagates the pose to it. It will add a keyframe
to it in case it doesn't exist. The discussion can be found here
<https://developer.blender.org/T87548>

![../../videos/Pose\_Propagate\_Demo.mp4](../../videos/Pose_Propagate_Demo.mp4
"../../videos/Pose_Propagate_Demo.mp4")
