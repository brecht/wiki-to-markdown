# Python API

## Breaking changes

### BGL Deprecation

BGL is a direct wrapper around OpenGL calls. The BGL module is
deprecated and since Blender 3.4 has been replaced with the \`gpu\`
module. From this release it will not work when using the Metal backend.
Add-ons and scripts that uses the bgl module are warned about the
deprecation and should use the [gpu
module](https://docs.blender.org/api/3.5/gpu.html) in stead.

### Class Registration

Registering classes with the names matching existing built-in types now
raises an error
([52f521dec4](https://projects.blender.org/blender/blender/commit/52f521dec4491feb746b458e120d0f3e8d05b19a)).

### Node Group Sockets

Virtual sockets are the extra sockets added to group input and output
nodes. Previously connecting a link to them would create an input or
output in the corresponding node group. That feature was meant as a
UI/UX feature though, and is now implemented differently in Blender and
doesn't happen automatically. The correct way to add a group input from
Python is to use \`node\_tree.inputs.new(...)\`
([7026096099](https://projects.blender.org/blender/blender/commit/70260960994d)).

### Grease Pencil

Auto-masking settings were moved from brush to tool settings, for
details see
[4c182aef7c](https://projects.blender.org/blender/blender/commit/4c182aef7ce0).

### Motion Tracking

The internal storage of the optical center (principal point) has been
changed in
[7dea18b3aa](https://projects.blender.org/blender/blender/commit/7dea18b3aa)
to a normalized space.

The normalized space refers to coordinate \`(0, 0)\` corresponding to
the frame center, \`(-1, -1)\` the left bottom frame corner, \`(1, 1)\`
is the top right frame corner. This notation is available via
\`clip.tracking.camera.principal\_point\` property.

For some algorithms it is convenient to operate in the pixel space. For
those usecases the new \`clip.tracking.camera.principal\_point\_pixels\`
property has been added.

The old \`clip.tracking.camera.principal\` has been removed to avoid
ambiguous naming in the API.

### Armature Modifier

An old bug where the effect of the Invert Vertex Group toggle of the
Armature modifier was inverted when Multi-Modifier was active was fixed.
([rBea1c31a2](https://projects.blender.org/blender/blender/commit/ea1c31a24438))

Scripts creating complex armature modifier setups need updating to check
the exact blender version:

``` Python
modifier.use_multi_modifier = True
if bpy.app.version_file < (3, 5, 8):  # Blender bug T103074
    modifier.invert_vertex_group = True
```

### Data Transer

For \`bpy.ops.object.datalayout\_transfer operator\`, \`VCOL\` has been
split into \`COLOR\_VERTEX\` and \`COLOR\_CORNER\` for different color
domains
([93d84e87b2](https://projects.blender.org/blender/blender/commit/93d84e87b26c),
[eae36be372](https://projects.blender.org/blender/blender/commit/eae36be372a6)).

## Internal Mesh Format

The mesh data structure refactoring from earlier releases has continued
in 3.5. See the similar section in the [3.4 release
notes](../3.4/Python_API.md#Internal_Mesh_Format).

  - The active and default (for render) color attributes are now stored
    as strings, making it easier to change them and access the correct
    attributes
    ([rB6514bb05](https://projects.blender.org/blender/blender/commit/6514bb05ea5a)).
  - Mesh vertex positions are now accessible via the builtin
    \`position\` attribute
    ([rB1af62cb3](https://projects.blender.org/blender/blender/commit/1af62cb3bf46)).
  - UV layers are now stored as generic 2D vector attributes on the face
    corner domain
    ([rB6c774feb](https://projects.blender.org/blender/blender/commit/6c774feba2c9))
  - The \`MeshUVLoop\` Python type is deprecated and will be removed in
    4.0. Its \`data\` collection member is also deprecated, replaced
    with separate collection members named \`uv\`,
    \`vertex\_selection\`, \`edge\_selection\`, and \`pin\`. Accessing
    the \`data\` member is emulated for backwards compatibility for now,
    but the emulation comes with a performance penalty
    ([rBa82c12ae](https://projects.blender.org/blender/blender/commit/a82c12ae32d9)).
  - The sharp edge property is now stored as a generic attribute,
    accessible with the \`sharp\_edge\` name
    ([rBdd9e1ede](https://projects.blender.org/blender/blender/commit/dd9e1eded0d4)).
  - Loose edge status is stored separately internally
    ([rB1ea169d9](https://projects.blender.org/blender/blender/commit/1ea169d90e39)).
      - The \`MeshEdge.is\_loose\` property is no longer editable.
      - Loose edge status can be recalculated with
        \`Mesh.update(calc\_edges\_loose=True)\`
  - Data access is generally faster than before when accessed with the
    attribute API (i.e. \`mesh.attributes\["sharp\_edge"\]), but
    slightly slower than before when accessed with the old properties.

## Bundled Libraries

Python bindings for the following libraries are now bundled with
Blender, and available to use by add-ons.

  - [USD](https://graphics.pixar.com/usd/release/index.html)
  - [OpenVDB](https://www.openvdb.org/documentation/doxygen/python.html)
  - [OpenImageIO](https://openimageio.readthedocs.io/en/latest/pythonbindings.html)
  - [OpenColorIO](https://opencolorio.readthedocs.io/en/latest/)
  - [MaterialX](https://materialx.org/docs/api/index.html)

## Other Changes

  - The users site-packages are now available by default without having
    to use \`--python-use-system-env\`
    ([72c012ab4a](https://projects.blender.org/blender/blender/commit/72c012ab4a3d2a7f7f59334f4912402338c82e3c))
  - New \`ModifierData.execution\_time\` property that can be used for
    profiling and optimization. It's not exposed in the UI yet
    ([8adebaeb7c](https://projects.blender.org/blender/blender/commit/8adebaeb7c3c663ec775fda239fdfe5ddb654b06)).
  - New \`object.modifiers.move()\` method for reordering modifiers
    without using operators.
    ([rBf7dd7d54](https://projects.blender.org/blender/blender/commit/f7dd7d545472))
  - The active catalog of Asset Browsers can be changed via
    \`bpy.types.FileAssetSelectParams.catalog\_id\`, it's no longer
    read-only.
    ([rB80249ce6](https://projects.blender.org/blender/blender/commit/80249ce6e4f9))
  - Custom node tree now have a default name set to
    \`NodeTree.bl\_label\`.
    ([59ce7bf](https://projects.blender.org/blender/blender/commit/59ce7bf))
