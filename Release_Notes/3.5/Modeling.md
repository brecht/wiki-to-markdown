# Modeling

  - A new **Set Attribute** operator allows basic control over generic
    attribute values in edit mode
    ([6661342dc5](https://projects.blender.org/blender/blender/commit/6661342dc549a8492)).
  - A new **Flip Quad Tessellation** operator allows control over the
    tessellation orientation of quads
    ([328772f2](https://projects.blender.org/blender/blender/commit/328772f2)).

![../../videos/Flip\_Quad\_Tessellation.webm](../../videos/Flip_Quad_Tessellation.webm
"../../videos/Flip_Quad_Tessellation.webm")

  - Edit mesh conversion to evaluated meshes has been parallelized and
    can be over twice as fast depending on the mesh and CPU
    ([ebe8f8ce71](https://projects.blender.org/blender/blender/commit/ebe8f8ce7197)).

## UV Editing

#### Copy/Paste

The most anticipated UV feature in this release is **UV Copy** and **UV
Paste**.

For low poly modeling and game assets, you can now select connected
faces, select from the menu *"UV \> Copy UVs"*, this will make a copy of
the UVs in a separate clipboard.

Next, select different faces that have the same topology. They can be on
a different mesh, a different UV channel, or even a different .blend
file. Choose "*Paste UVs*", and the UVs will be pasted over.
([721fc9c1c9](https://projects.blender.org/blender/blender/commit/721fc9c1c950))

#### Improvements

The **Constrain to Image Bounds** feature received fixes for the **Shear
Operator**, and to improve robustness on unusual inputs. With this
change, **all** of the UV operators should now correctly support
**Constrain to Image Bounds** and it can now be considered feature
complete.
([3d1594417b](https://projects.blender.org/blender/blender/commit/3d1594417b0e)
and
[0079460dc7](https://projects.blender.org/blender/blender/commit/0079460dc79a))

UV **Sphere Projection** and UV **Cylinder Projection** now have better
handling of UVs at the poles. In addition, you can now specify whether
to "**Fan**" or "**Pinch**" the UVs. All the projection options are now
fully supported in all modes.
([280502e630](https://projects.blender.org/blender/blender/commit/280502e630e9))

#### Fixes

  - Bring back the missing UV overlay in **Texture Paint Mode**.
    ([4401c93e45](https://projects.blender.org/blender/blender/commit/4401c93e452f))
  - **Select Similar** now has support for **Similar Object** and
    **Similar Winding**
    ([2b4bafeac6](https://projects.blender.org/blender/blender/commit/2b4bafeac68e))
  - Fix **UV Unwrap** for quads with **Reflex Angles**.
    ([f450d39ada](https://projects.blender.org/blender/blender/commit/f450d39ada1f))
  - Fix **UV Unwrap** broken for n-gons with 3 shared vertices.
    ([644afda7eb](https://projects.blender.org/blender/blender/commit/644afda7eb6c))
