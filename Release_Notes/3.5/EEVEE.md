# EEVEE & Viewport

## Viewport Compositor

A new **[Viewport
Compositor](https://code.blender.org/2022/07/real-time-compositor/)**
shading option was added that applies the result of the Compositor
Editor node-tree directly in the 3D Viewport. This feature is a work in
progress and not all compositor setups are supported. Refer to
[\#99210](https://projects.blender.org/blender/blender/issues/99210) for
the status of the project.

![Viewport Compositor is
disabled.](../../images/RealtimeCompositorDisabled.png
"Viewport Compositor is disabled.")

![Viewport Compositor is
enabled.](../../images/RealtimeCompositorEnabled.png
"Viewport Compositor is enabled.")

## Metal Viewport

When running on a MacOS device Blender will now use the Metal back-end
for the user interface and 3D viewport. Metal provides improved
performance, better stability and on par compatibility compared with
other platforms.

Improved performance affects rendering as well as animation playback.

The scenes tested are
[Wanderer](https://www.blender.org/download/demo-files/#eevee) and ["Hi,
my name is
Amy"](https://www.blender.org/download/demo-files/#animation).

| Platform            | Scene    | Blender        | Playback (FPS) | Render time (seconds) |
| ------------------- | -------- | -------------- | -------------- | --------------------- |
| Mac Studio - Ultra  | Wanderer | 3.4.1 / OpenGL | 16.8           | 6.5                   |
|                     |          | 3.5 / OpenGL   | 17.0           | 5.7                   |
|                     |          | 3.5 / Metal    | **45.0**       | **5.3**               |
|                     | Amy      | 3.4.1 / OpenGL | 12.7           | 12.2                  |
|                     |          | 3.5 / OpenGL   | 17.7           | 9.8                   |
|                     |          | 3.5 / Metal    | **34**         | **8.8**               |
| Mac Mini - Intel i5 | Wanderer | 3.4.1 / OpenGL | 5.4\*          | 17.5\*                |
|                     |          | 3.5 / OpenGL   | 5.9\*          | 16.9\*                |
|                     |          | 3.5 / Metal    | **6.8**        | **20.4**              |
|                     | Amy      | 3.4.1 / OpenGL | 6.3            | 65                    |
|                     |          | 3.5 / OpenGL   | 6.5            | 64                    |
|                     |          | 3.5 / Metal    | **9.7**        | **55**                |

(\*) = Fails to compile certain materials at the moment.

Check [Python API](Python_API.md) to
learn more about the deprecation of the \`bgl\` module. Users of
add-ons/scripts still using this module should inform the add-on/script
developers to update their scripts. Users can switch back to the OpenGL
back-end in Preferences → System, in order to keep using these scripts.
