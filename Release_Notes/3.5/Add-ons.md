# Add-ons

## Rigify

  - Ported the ability to generate Action Constraint layers from Cloud
    Rig ([D16336](http://developer.blender.org/D16336),
    [rBAdddf346](https://projects.blender.org/blender/blender-addons/commit/dddf346f1adf)).

## Storypencil

  - UI Refactor: New Mode selector and settings for more discoverability
    of the add-on two main modes (Switch and New Window).
  - In Switch Mode the audio file in the video sequencer is copied
    temporarily to the source scene allowing to keep audio in sync. This
    can be disabled in the Storypencil settings panel.
  - IN/OUT strip range markers ar now showed in Switch Mode.
  - TAB key can be used now to edit the strip scene under the timeline
    cursor.
  - Bugs Fixed.
    ([rBA6fcd157](https://projects.blender.org/blender/blender-addons/commit/6fcd157f2497d9ba4ba82191cb2abf3de11a0394))

![../../images/Storypencil\_panel.png](../../images/Storypencil_panel.png
"../../images/Storypencil_panel.png")

## Sun Position

  - Add Show Surface and Show Analemmas options. These are useful to
    visualize the trajectory of the Sun in the sky during the year, for
    each hour of the day.
    ([rBA29a6735](https://projects.blender.org/blender/blender-addons/commit/29a67357a059))

## glTF 2.0

### Importer

  - Custom primitive attributes are now imported as attributes
    ([rBA4f8ccce](https://projects.blender.org/blender/blender-addons/commit/4f8ccce4a5357974804dcecce88deddd67dcb8f3))
  - Better uri management
    ([rBAfc8bd7d](https://projects.blender.org/blender/blender-addons/commit/fc8bd7d6d4b7e0b34fb008ac328e839cca813094))

### Exporter

  - Add an export identifier, usefull for glTF exporter called by
    another addon
    ([rBA665a82e](https://projects.blender.org/blender/blender-addons/commit/665a82e17100e4560ea0ef55a51f8721c670c936))
  - Fix regression on Vertex Color export (face corner domain)
    ([rBAd18a07f](https://projects.blender.org/blender/blender-addons/commit/d18a07f51404ff155de0c21ac1ea32e839e74d88))
  - Fix when custom shader node has no type
    ([rBA23661bd](https://projects.blender.org/blender/blender-addons/commit/23661bdd40144efb1f681396947c38cdecbe0163))
  - Various fixed in shader node tree export
    ([rBA5a774a6](https://projects.blender.org/blender/blender-addons/commit/5a774a6dad4378f173ec7fdcdcd406048fcc9a29))
  - Add option to choose jpeg quality
    ([rBAbcc7923](https://projects.blender.org/blender/blender-addons/commit/bcc792388887d15bbed6f87842cf8fe6079f8323))
  - Manage future deprecation in numpy
    ([rBAa131db6](https://projects.blender.org/blender/blender-addons/commit/a131db64b9482cb6de52dc561b60f8decbe307a7))
  - Export custom attribute only when start with underscore, to avoid
    exporing built-in attributes
    ([rBAd573201](https://projects.blender.org/blender/blender-addons/commit/d5732014b103c7dd28e79bdc8e4bffc8201ba260))
  - Fix division by 0 when trans+spec
    ([rBA2207539](https://projects.blender.org/blender/blender-addons/commit/2207539e32c34609c9c8ca48c640f91259e71326))
  - Better uri management
    ([rBAfc8bd7d](https://projects.blender.org/blender/blender-addons/commit/fc8bd7d6d4b7e0b34fb008ac328e839cca813094))
  - For glTF separated mode, use tab as indent (save space)
    ([rBA07157fb](https://projects.blender.org/blender/blender-addons/commit/07157fbb8fe590b868ab2e65af04a5d723d4a45f))
  - Better parent check, when bone parent is still set, but user moved
    to "OBJECT" parent type, without resetting bone parent
    ([rBAbddbd5e](https://projects.blender.org/blender/blender-addons/commit/bddbd5ed5fa6af76b50b4f9ac202cc2903977248))
  - Update file filter in UI when user change the export mode
    (\*.glb/\*.gltf)
    ([rBA38890b5](https://projects.blender.org/blender/blender-addons/commit/38890b51d3c262cba7b8df1063a33ce89e558550))
  - Fix check for node tree
    ([rBA0554223](https://projects.blender.org/blender/blender-addons/commit/0554223a844549ec2e18462fa76e2c34fafe8f19))
  - Round normals to avoid vertex split
    ([rBA48b114c](https://projects.blender.org/blender/blender-addons/commit/48b114c17881a752d5dde1db68e77b49de063f2f))
  - Fix variable name collision that leads to crash
    ([rBAc6c45a8](https://projects.blender.org/blender/blender-addons/commit/c6c45a843e761045ba755c12c6394437564485c8))
  - Fix error after reloading scripts
    ([rBA599f2c4](https://projects.blender.org/blender/blender-addons/commit/599f2c4fdac28c9cbb15704062b17e7b3358a57c))
  - Manage new mirror texture option
    ([rBA69beaa0](https://projects.blender.org/blender/blender-addons/commit/69beaa0e43b244f8e74d5e53ce0fe34731ac0bb9))
  - Fix object parented to bone TRS
    ([rBAd31fa71](https://projects.blender.org/blender/blender-addons/commit/d31fa71a6b27bcb168b46c44d22dfb075482591a))
  - Fix typo in tangent morph export
    ([rBAafed066](https://projects.blender.org/blender/blender-addons/commit/afed066115d8102b41059df6ba9a2914ea3f4f70))
  - Add hook to change attribute
    ([rBAd936a94](https://projects.blender.org/blender/blender-addons/commit/d936a948e877dcb79817ab9f214514514540f6d5))

## Contrib repository

  - Contrib add-ons are now already excluded in beta, to better
    communicate that releases aren't shipping with them. Also improve
    the UI to only show the relevant categories for releases.
    ([rBe8c78666](https://projects.blender.org/blender/blender/commit/e8c7866608bb))
