# Grease Pencil

## Operators

  - Sculpt Auto-masking moved to Global setting.
    ([4c182aef7c](https://projects.blender.org/blender/blender/commit/4c182aef7ce0))

![../../images/Auto-masking.png](../../images/Auto-masking.png
"../../images/Auto-masking.png")

  - Added new Auto-masking pie menu using \`Shift+Alt+A\`.
    ([4c182aef7c](https://projects.blender.org/blender/blender/commit/4c182aef7ce0))

![../../images/Auto-masking\_radial.png](../../images/Auto-masking_radial.png
"../../images/Auto-masking_radial.png")

  - Interpolate Sequence by default can now use all different keyframes
    types as extremes and a new option \`Exclude Breakdowns\` was added
    to allows users exclude this type of keyframes from the
    interpolation as with the old behavior.
    ([56ae4089eb](https://projects.blender.org/blender/blender/commit/56ae4089eb35))

![../../images/Interpolation.png](../../images/Interpolation.png
"../../images/Interpolation.png")

  - Copy & Paste now works in multiframe mode.
    ([a5d4c63e86](https://projects.blender.org/blender/blender/commit/a5d4c63e867e))

## UI

  - \`Vertex Opacity\` parameter now is visible in Overlay panel in
    Sculpt mode.
    ([1aff91b1a7](https://projects.blender.org/blender/blender/commit/1aff91b1a707))

![../../images/Vertex\_Opacity\_Sculpt\_Mode.png](../../images/Vertex_Opacity_Sculpt_Mode.png
"../../images/Vertex_Opacity_Sculpt_Mode.png")

  - New option to show the brush size in Draw tool cursor.
    ([a44c128482](https://projects.blender.org/blender/blender/commit/a44c1284823a))
    ([a2cf9a8647](https://projects.blender.org/blender/blender/commit/a2cf9a8647fd))

![../../images/Cursor.png](../../images/Cursor.png
"../../images/Cursor.png")

  - Radial control now displays the correct size when resizing brush
    size.
    ([0fb12a9c2e](https://projects.blender.org/blender/blender/commit/0fb12a9c2ebc))
  - Create new layer using \`Y\` menu in Drawing mode allows to enter
    new layer name.
    ([f53bb93af9](https://projects.blender.org/blender/blender/commit/f53bb93af937))

![../../images/New\_Layer.png](../../images/New_Layer.png
"../../images/New_Layer.png")

  - Material popover now display Fill color for Fill materials.
    ([e144af1f7c](https://projects.blender.org/blender/blender/commit/e144af1f7cd3))

## Modifiers

  - Add offset (Location, Rotation, Scale) by Layer, Stroke and Material
    to the \`Offset Modifier\`.
    ([7d712dcd0b](https://projects.blender.org/blender/blender/commit/7d712dcd0bbd))

![../../images/GP\_Offset\_Modifier.png](../../images/GP_Offset_Modifier.png
"../../images/GP_Offset_Modifier.png")

  - New \`Natural Drawing Speed\` mode in \`Build Modifier\` timing
    property that replay drawings using the recorded speed of the
    stylus, giving it a more natural feel
    ([250eda36b8](https://projects.blender.org/blender/blender/commit/250eda36b8f9))

![../../images/GP\_Build\_Modifier.png](../../images/GP_Build_Modifier.png
"../../images/GP_Build_Modifier.png")
