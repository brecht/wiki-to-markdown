# Blender 3.5 Release Notes

Blender 3.5 was released on March 29, 2023.

Check out the [release notes on
blender.org](https://www.blender.org/download/releases/3-5/).

## [Animation & Rigging](Animation_Rigging.md)

## [Core](Core.md)

## [EEVEE & Viewport](EEVEE.md)

## [Grease Pencil](Grease_Pencil.md)

## [Modeling](Modeling.md)

## [Nodes & Physics](Nodes_Physics.md)

## [Pipeline, Assets & I/O](Pipeline_Assets_IO.md)

## [Python API](Python_API.md)

## [Cycles](Cycles.md)

## [Sculpt, Paint, Texture](Sculpt.md)

## [User Interface](User_Interface.md)

## [VFX & Video](VFX.md)

## [Add-ons](Add-ons.md)

## Compatibility

Blender 3.5 is compatible with [VFX Reference
Platform 2023](https://vfxplatform.com/)

New minimum requirements:

  - **macOS**
      - **macOS 10.15** (Catalina) for **Intel** devices
      - macOS 11.0 (Big Sur) for Apple Silicon (same requirements as
        previous releases)
  - **Linux** distribution with **glibc 2.28**, including:
      - **Ubuntu 18.10**
      - **Debian 10** (Buster)
      - **Fedora 29**
      - **RHEL 8** and derivatives CentOS, Rocky Linux and AlmaLinux

## [Corrective Releases](Corrective_Releases.md)
