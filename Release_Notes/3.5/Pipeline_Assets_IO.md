# Pipeline, Assets & I/O

## USD

  - Support for import
    ([cdef135f6f](https://projects.blender.org/blender/blender/commit/cdef135f6f))
    and export
    ([0c67a90e4f](https://projects.blender.org/blender/blender/commit/0c67a90e4f))
    of USDZ (compressed version of USD) was added.
  - USD export changes to pass \`usdchecker\`
    ([b67b84bd5d](https://projects.blender.org/blender/blender/commit/b67b84bd5d)).
  - Add scale and bias for exported \`USD Preview Surface\` normal maps
    ([c79b55fc05](https://projects.blender.org/blender/blender/commit/c79b55fc05)).
  - Convert USD camera properties to mm from USD units
    ([f359a39d11](https://projects.blender.org/blender/blender/commit/f359a39d11)).
  - Support importing USD Shapes
    ([72a85d976a](https://projects.blender.org/blender/blender/commit/72a85d976a)).
  - Author extents on exported \`UsdGeomMesh\`
    ([5040c39d1a](https://projects.blender.org/blender/blender/commit/5040c39d1a)).

## OBJ

  - New OBJ importer got "Split by Objects" and "Split by Groups" import
    settings, just like the old Python based addon had
    ([rBb5998204](https://projects.blender.org/blender/blender/commit/b59982041)).
  - New OBJ importer can now import polylines with more than 2 vertices
    ([rBcfe828b4](https://projects.blender.org/blender/blender/commit/cfe828b452e6)).
  - Removing incorrect edge recalculation made the OBJ exporter up to
    1.6x faster
    ([rB455d195d](https://projects.blender.org/blender/blender/commit/455d195d5504))

## Assets

  - New "All" asset library
    ([35e54b52e6](https://projects.blender.org/blender/blender/commit/35e54b52e6)).
    Displays contents from all other asset libraries. This asset library
    is shown in Asset Browsers by default
    ([7ba59c8a62](https://projects.blender.org/blender/blender/commit/7ba59c8a62)).
  - New "Essentials" asset library
    ([b3fb73f325](https://projects.blender.org/blender/blender/commit/b3fb73f325))
    containing a number of assets that are now shipped with Blender. It
    includes a number of hair node groups for use with geometry nodes.  
    ![../../images/Release\_notes\_essentials\_asset\_library.png](../../images/Release_notes_essentials_asset_library.png
    "../../images/Release_notes_essentials_asset_library.png")  

![../../images/3.5\_release\_notes\_asset\_library\_Preferences\_UI.png](../../images/3.5_release_notes_asset_library_Preferences_UI.png
"../../images/3.5_release_notes_asset_library_Preferences_UI.png")

  - The user interface for setting up asset libraries in the Preferences
    has been revamped.
    ([0d798ef57c](https://projects.blender.org/blender/blender/commit/0d798ef57c))
  - A default import method (*Append*, *Append (Reuse Data)*, *Link*)
    can now be set per asset library in the Preferences.
    ([ae84a2956e](https://projects.blender.org/blender/blender/commit/ae84a2956e)).
    Asset Browsers follow this setting by default, but there's an option
    to override it.  
  - *Import Type* has been renamed to *Import Method*.
    ([972f58c482](https://projects.blender.org/blender/blender/commit/972f58c482))

## glTF 2.0

Find the changelog in the
[Add-ons](Add-ons.md) section.
