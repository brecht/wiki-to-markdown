# Sculpt, Paint, Texture

## New Features

![../../images/Sculpt-paint\_sculpt\_vdm\_example.png](../../images/Sculpt-paint_sculpt_vdm_example.png
"../../images/Sculpt-paint_sculpt_vdm_example.png")

  - Support for **Vector Displacement Map** (VDM) brushes for the Draw
    brush
    ([39f63c8c08](https://projects.blender.org/blender/blender/commit/39f63c8c086dd9dbe286924909e400d0d47b186c))

<!-- end list -->

  - Added **Extrude Mode** for Trim tools. See manual for [Box
    Trim](https://docs.blender.org/manual/en/3.5/sculpt_paint/sculpting/tools/box_trim.html),
    [Lasso
    Trim](https://docs.blender.org/manual/en/3.5/sculpt_paint/sculpting/tools/lasso_trim.html).
    ([rB88e98265](https://projects.blender.org/blender/blender/commit/88e9826529d1))

## User Manual

Visit the user manual at
[blender.org/manual](https://docs.blender.org/manual/en/3.5/sculpt_paint/sculpting/introduction/general.html)

  - The user manual got a major rewrite of many pages.
    ([rBM9824](https://ro.developer.blender.org/rBM9824))
  - Individual tool pages were rewritten and restructured.
    ([rBM9839](https://ro.developer.blender.org/rBM9839))
  - Additions and rewrites for Editing pages
    ([rBM9885](https://ro.developer.blender.org/rBM9885))

This adds:

  - A multi-page Introduction section to sculpting
  - User oriented sorting and more visual examples
  - Fixing various out of date, missing or false information
  - A new page for Expand to explain many use cases

![../../images/Sculpt\_user\_manual\_intro.png](../../images/Sculpt_user_manual_intro.png
"../../images/Sculpt_user_manual_intro.png")

## Shortcut Changes

  - The shortcuts \`Shift R\` and \`Shift D\` to define density in
    sculpt mode have been remapped to \`R\`
    ([rB3e903909](https://projects.blender.org/blender/blender/commit/3e9039091870))

This change was done to prevent shortcut conflicts with Redo. Based on
community feedback, not only this improves consistency across Blender,
but the benefits of being able to redo certain operations outweigh the
muscle memory adjustment.

This change affects the Voxel Remesher, Dynamic Topology and the Hair
Density brush.
