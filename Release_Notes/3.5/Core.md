# Core

### Custom Properties

Custom properties can now have a boolean type
([rBef68a37e](https://projects.blender.org/blender/blender/commit/ef68a37e5d55)).

### Datablocks

'Fake User' on linked data will now always enforce that linked
data-block to be considered as 'directly linked', i.e. never
automatically cleared by Blender on file save. See
[\#105786](https://projects.blender.org/blender/blender/issues/105786)
and
[133dde41bb](https://projects.blender.org/blender/blender/commit/133dde41bb)
for details.
