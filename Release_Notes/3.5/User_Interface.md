# User Interface

## General

  - The icon preview shown in the material slots and material search
    menu will no longer be rendered for linked in materials. This would
    cause slowdowns and UI freezes every time the file is reloaded,
    since the preview is never stored in files.
    ([rB571f3731](https://projects.blender.org/blender/blender/commit/571f373155cb))
  - More menus align their item labels when some items include an icon
    and some don't
    ([58752ad93c](https://projects.blender.org/blender/blender/commit/58752ad93c)).
  - Font previews now differentiate better between Korean, Japanese,
    Simplified & Traditional Chinese
    ([rB485ab420](https://projects.blender.org/blender/blender/commit/485ab420757b))

![../../images/FontPreviewsCJK.png](../../images/FontPreviewsCJK.png
"../../images/FontPreviewsCJK.png")

## Viewport

  - Cancelling viewport orbit/pan/zoom & dolly is now supported (by
    pressing RMB), this allows orbiting out of camera or orthographic
    views temporarily.
    ([rB0b85d6a0](https://projects.blender.org/blender/blender/commit/0b85d6a030a3fc5be10cce9b5df7a542a78b4503))

## macOS

  - User notifications for finishing renders and compiling shaders were
    removed.
    ([3590e263e0](https://projects.blender.org/blender/blender/commit/3590e263e09d))
