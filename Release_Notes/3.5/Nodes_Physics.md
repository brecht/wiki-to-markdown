# Nodes & Physics

## Geometry Nodes

### General

  - A new **Image Info** node allows retrieving various information
    about an image
    ([rBefcd587b](https://projects.blender.org/blender/blender/commit/efcd587bc28d)).
  - There is a new **Image** input node
    ([rBa3251e66](https://projects.blender.org/blender/blender/commit/a3251e66a723)).
  - The *Named Attribute* input node now has an "Exists" output to tell
    whether the attribute exists
    ([rB0d3a33e4](https://projects.blender.org/blender/blender/commit/0d3a33e45eb3)).
  - A new **Blur Attribute** nodes allows mixing attribute values of
    neighboring elements
    ([rBd68c47ff](https://projects.blender.org/blender/blender/commit/d68c47ff347bbb3824)).
  - The *Store Named Attribute* node can now store 2D vector attributes
    ([rBf0dc4d67](https://projects.blender.org/blender/blender/commit/f0dc4d67e56dbdd13)).
  - The *Image Texture* node has a new mirror extension type
    ([rBa501a2db](https://projects.blender.org/blender/blender/commit/a501a2dbff797829)).
  - Field utility nodes have been renamed
    ([rB4961e5f9](https://projects.blender.org/blender/blender/commit/4961e5f91d98)).
      - *Interpolate Domain* -\> *Evaluate on Domain*
      - *Field at Index* -\> *Evaluate at Index*
  - The modifier user interface has been improved in various ways
      - Exposed properties from node groups no longer have hard min and
        max values
        ([rBcb92ff7b](https://projects.blender.org/blender/blender/commit/cb92ff7b2d50)).
      - Checkboxes are used for exposed boolean sockets
        ([rB2ea47e0d](https://projects.blender.org/blender/blender/commit/2ea47e0def6a)).
      - The "Use Attribute" toggles have moved to the right edge of the
        panel
        ([rB68625431](https://projects.blender.org/blender/blender/commit/68625431d5d0)).

![The technology for checkboxes was finally invented\!
](../../images/Boolean_inputs_for_the_modifier.png
"The technology for checkboxes was finally invented! ")

  - Drag and drop is supported for geometry node group assets in the
    viewport
    ([bfa7f9db0e](https://projects.blender.org/blender/blender/commit/bfa7f9db0e797a1fc5d06aa0a38fbbfd046aac44)).
  - A new operator "Move to Nodes" creates a new node group wrapping the
    modifier's group
    ([5ca65001ea](https://projects.blender.org/blender/blender/commit/5ca65001ea9e90c4d)).

### Curves

  - New **Interpolate Curves** node allows generating child curves
    between a set of guides
    ([rB85908e9e](https://projects.blender.org/blender/blender/commit/85908e9edf3df)).
  - The *Trim Curves* node now has a selection input
    ([rB11f6c65e](https://projects.blender.org/blender/blender/commit/11f6c65e61a2)).
  - Making simple procedural changes can be much faster in some cases,
    and non-topology changing sculpt brushes are slightly faster
    ([rB7f958217](https://projects.blender.org/blender/blender/commit/7f958217ada2)).

### Mesh

  - The new **Edges to Face Groups** node finds groups of faces
    surrounded by selected edges
    ([50dfd5f501d3e07](https://projects.blender.org/blender/blender/commit/50dfd5f501d3e07915d1e9ad8656a52b3902b6ab))
  - The mesh primitive nodes now output a UV map (previously that was
    stored as named attribute with a hard-coded name
    ([rBf879c20f](https://projects.blender.org/blender/blender/commit/f879c20f72d9ce58a2e0)).
  - The *Split Edges* node is over two times faster
    ([rBe83f46ea](https://projects.blender.org/blender/blender/commit/e83f46ea7630)).
  - For mesh objects, applying modifier will now give an error message
    if the geometry it creates doesn't contain a mesh
    ([rBb1494bce](https://projects.blender.org/blender/blender/commit/b1494bcea7b6)).

### Instances

  - Caching of geometry bounds can make viewport display of many
    geometry instances 2-3x faster
    ([rBe8f40106](https://projects.blender.org/blender/blender/commit/e8f4010611e7)).

## Node Editor

### User Interface

  - The context menu has been significantly improved, giving quick
    access to more features
    ([rB2c096f17](https://projects.blender.org/blender/blender/commit/2c096f17a690)).
  - Automatically attaching nodes after adding or moving them can be
    disabled by holding \`Alt\`
    ([rBae886596](https://projects.blender.org/blender/blender/commit/ae886596a0f1)).
  - For Copy & Paste, nodes are placed at the mouse position rather than
    their original locations
    ([rB7355d64f](https://projects.blender.org/blender/blender/commit/7355d64f2be5)).
  - The Geometry Nodes add menu is reorganized to make it easier to
    explore
    ([rBd4e638ba](https://projects.blender.org/blender/blender/commit/d4e638baac43),
    [rB789e549d](https://projects.blender.org/blender/blender/commit/789e549dbae8)).

![../../videos/2\_geonodes\_menu\_after.mp4](../../videos/2_geonodes_menu_after.mp4
"../../videos/2_geonodes_menu_after.mp4")

  - Node links can be swapped between sockets by holding \`Alt\` while
    connecting them
    ([rB89aae4ac](https://projects.blender.org/blender/blender/commit/89aae4ac82e0)).
    This replaces the auto-swapping behavior.

![Hold \`Alt\` to swap links.](../../videos/node_link_swapping.mp4
"Hold \`Alt\` to swap links.")

## Cloth Simulation

  - Self-collision was optimized with a 25% overall fps gain on some
    collision-heavy tests
    ([rB0796210c](https://projects.blender.org/blender/blender/commit/0796210c8df3),
    [rBa3ac91da](https://projects.blender.org/blender/blender/commit/a3ac91da27dd),
    [rBe1df731c](https://projects.blender.org/blender/blender/commit/e1df731c91bc)).
