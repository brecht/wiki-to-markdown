# Cycles

## Light Sampling

Cycles now uses a light tree to more effectively sample scenes with many
lights. This can significantly reduce noise, at the cost of a somewhat
longer render time per sample.

  - Light tree sampling is enabled by default in new scenes. It can be
    disabled in the Sampling \> Lights panel.
  - If the light tree yields different render results, the most likely
    cause is light clamping. Clamping is a biased method that depends on
    the sampling strategy. Generally if the light tree works well, there
    will be less clamping and the render will be closer to the unbiased
    result.
  - The light tree works best in scenes with physically correct
    lighting, that is no custom falloff or ray visibility tricks. These
    may interfere with heuristics used in the light tree.
  - Materials with emission now have a new Emission Sampling setting,
    replacing the previous Multiple Importance Sample toggle. Auto is
    the new default, and uses a heuristic to estimate the emitted light
    intensity to determine of the mesh should be considered as a light
    for sampling. For single sided emitters or closed meshes, setting
    this to Front faces only can reduce noise with the light tree.

The light tree is currently disabled for AMD GPUs on Windows, Linux and
macOS. We hope to resolve this in the next release.

<center>

|                                                                                                                           |                                                                                                                                                 |
| ------------------------------------------------------------------------------------------------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------- |
| ![../../images/Cycles3.5\_bistro\_ref.png](../../images/Cycles3.5_bistro_ref.png "../../images/Cycles3.5_bistro_ref.png") | ![../../images/Cycles3.5\_bistro\_light\_tree.png](../../images/Cycles3.5_bistro_light_tree.png "../../images/Cycles3.5_bistro_light_tree.png") |

style="caption-side: bottom" | Equal time render without and with light
tree, scene CC-BY 4.0, Amazon Lumberyard

</center>

<center>

|                                                                                                                        |                                                                                                                                              |
| ---------------------------------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------- |
| ![../../images/Cycles3.5\_attic\_ref.png](../../images/Cycles3.5_attic_ref.png "../../images/Cycles3.5_attic_ref.png") | ![../../images/Cycles3.5\_attic\_light\_tree.png](../../images/Cycles3.5_attic_light_tree.png "../../images/Cycles3.5_attic_light_tree.png") |

style="caption-side: bottom" | Equal time render without and with light
tree, scene CC-BY 4.0, NVIDIA

</center>

## Open Shading Language

OSL can now be used with OptiX on the GPU, in addition to existing
support with CPU rendering.

There are some limitations currently, in particular for image texture,
AO and bevel nodes. See the [OSL
documentation](https://docs.blender.org/manual/en/3.5/render/shader_nodes/osl.html)
for details.

## Apple Metal

GPU rendering performance on Apple devices was improved, without various
incremental changes and tunings.

The big change is kernel specialization for shaders. This works
transparently: rendering starts quickly with a generic shader and
optimized shaders are compiled in the background and used when ready.
This optimization can be configured in the preferences.

## Changes

  - Improved adaptive sampling for overexposed scenes, reducing render
    time by taking into account absolute light intensity.
    ([857bb1b](https://projects.blender.org/blender/blender/commit/857bb1b))  
    This can negatively affect some scenes where compositing or color
    management are used to make the scene much darker or lighter. For
    best results adjust the Film \> Exposure setting to bring the
    intensity into a good range, and then do further compositing and
    color management on top of that. Note that this setting is not the
    same as Color Management \> Exposure.
  - Spot lights support for non-uniform object scale.
    ([b454416](https://projects.blender.org/blender/blender/commit/b454416))
  - Area lights with ellipse shape and spread have much reduced noise.
    ([53ef52f](https://projects.blender.org/blender/blender/commit/53ef52f))
  - Fix Anisotropic BSDF with Beckmann roughness using isotropic
    sampling
    ([eb657633ed](https://projects.blender.org/blender/blender/commit/eb657633ed85))

![Anisotropic Beckmann before
fix](../../images/Aniso_beckmann_before.png
"Anisotropic Beckmann before fix") ![Anisotropic Beckmann after
fix](../../images/Aniso_beckmann.png "Anisotropic Beckmann after fix")
![Anisotropic GGX for reference](../../images/Aniso_ggx.png
"Anisotropic GGX for reference")

  - The shadow render pass was removed, as its design was incompatible
    with the light tree. In many case shadow catcher passes can be used
    instead.
    ([0731d78d](https://projects.blender.org/blender/blender/commit/0731d78d))
  - The Diffuse and Glossy Color passes now account for the color of
    additional closures (e.g. the Principled BSDF's metallic and sheen
    components), ensuring that the contribution is split into a color
    component (in the Color pass) and a color-independent light
    component (in the Direct/Indirect passes) as expected
    ([73000c792d](https://projects.blender.org/blender/blender/commit/73000c792d))
