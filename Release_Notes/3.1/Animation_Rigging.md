# Blender 3.1: Animation & Rigging

## Armatures

  - It is now possible to reduce the opacity of bones drawn as
    'wireframe' in Pose, Edit and Weight Paint modes via an option in
    the overlay popover. This is anticipated to be most useful for
    reducing visual clutter during Weight Paint.
    ([rB1785286e](https://projects.blender.org/blender/blender/commit/1785286ecc9))

## Action Frame Range

![../../images/Action\_manual\_frame\_range.png](../../images/Action_manual_frame_range.png
"../../images/Action_manual_frame_range.png")

It is now possible to manually specify the intended usable frame range
of an action, and whether it is intended to be a cycle.

The settings are accessible via a panel within the Dope Sheet, Action
Editor and NLA Editor. The range is also displayed as a diagonal hash
highlight in the background. The frame range will be used when adding an
action strip to NLA instead of the actual range of the keys in the
action. It can also be used by exporters to define the export range,
e.g. FBX.
([rB5d59b386](https://projects.blender.org/blender/blender/commit/5d59b38605d61b)).

The cyclic setting does not actually make the animation cycle by itself,
but the [Cycle-Aware
Keying](https://docs.blender.org/manual/en/dev/editors/timeline.html#timeline-keying)
option will use it to automatically make curves newly inserted into this
action cyclic with the right period
([rB72acce43](https://projects.blender.org/blender/blender/commit/72acce43bc34e5)).

## Copy Global Transform

New add-on for animators, which is documented in the [add-ons
section](Add-ons.md#Copy_Global_Transform)
of the release notes.

## Motion Path Interface

It is possible to clean all paths from a bone or object that has no
motion path: the "X" button was moved to a different line and is visible
at all times.

![Blender 3.1 Motion Path User
Interface](../../images/Motion_Path_UI_3.1.png
"Blender 3.1 Motion Path User Interface")

## Equalize Handles

The graph editor now has an "Equalize Handles" operator (in the Keys →
Snap menu,
[rB17b0c069](https://projects.blender.org/blender/blender/commit/17b0c06946be)).
It equalizes the handle length of all selected keyframes, optionally
making them horizontal as well. The length itself defaults to 5.0 and
can be adjusted in the redo panel.

## Graph Editor Slider Operators

The graph editor now has a "Breakdown"
[f7ddb1ed8a](https://projects.blender.org/blender/blender/commit/f7ddb1ed8a2a)
and a "Blend To Neighbor"
[9085b4a731](https://projects.blender.org/blender/blender/commit/9085b4a731fd)
operator like the pose mode. It allows to modify selected keys based on
their surrounding keys. The operators can be found under
<span class="literal">Key</span> » <span class="literal">Slider
Operators</span>.
![../../videos/Graph\_slide\_breakdown.mov](../../videos/Graph_slide_breakdown.mov
"../../videos/Graph_slide_breakdown.mov")
