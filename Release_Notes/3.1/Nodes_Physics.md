# Geometry Nodes

Geometry nodes has been extended with large performance improvements,
more possibilities with many new mesh nodes, and various other utilities
and features.

In **bold** you can find the names of 19 newly added nodes.

### Performance Improvements

  - The *Set Position* node has been optimized for meshes, with up to a
    4x improvement for a simple field
    ([602ecbdf9a](https://projects.blender.org/blender/blender/commit/602ecbdf9aef58)).
  - The evaluation system used for fields has been improved in many ways
      - In large fields with many nested values, memory usage can be
        reduced by up to 100x, which can also improve performance by
        3-4x
        ([1686979747](https://projects.blender.org/blender/blender/commit/1686979747c3b5)).
      - Improvements to multi-threading with medium loads (around 10,000
        elements) can improve field evaluation performance by up to 10x
        ([658fd8df0b](https://projects.blender.org/blender/blender/commit/658fd8df0bd2427c)).
      - Memory use and performance have been improved, especially when
        few elements are processed by many nodes, up to a 20%
        improvement in some cases
        ([92237f11eb](https://projects.blender.org/blender/blender/commit/92237f11eba44d)).
      - Processing single values with field nodes is more efficient now,
        which can make a 2-3x difference in very extreme situations
        ([47276b8470](https://projects.blender.org/blender/blender/commit/47276b84701727c2f18)).
  - The *Realize Instances* node is now multi-threaded, which can give
    at least a 4x improvement in some common cases
    ([f5ce243a56](https://projects.blender.org/blender/blender/commit/f5ce243a56a22d)).
  - Mesh vertex and face normals have been refactored, improving access
    speed in geometry nodes by up to 40%
    ([cfa53e0fbe](https://projects.blender.org/blender/blender/commit/cfa53e0fbeed71)).
      - Other areas like normal calculation, primitive nodes, and more
        complex files also have significant improvements.
  - Domain interpolation now only calculates necessary values, for some
    interpolations to faces and face corners
    ([796e9d442c](https://projects.blender.org/blender/blender/commit/796e9d442cf8f9)).
      - When selections are used, the improvements can be significant; a
        6x improvement was measured using the position attribute to a
        single face.
  - Displaying large node trees with many nodes in the node editor can
    be almost twice as fast
    ([0129178376](https://projects.blender.org/blender/blender/commit/012917837649676e9)).
  - The *Set Spline Type* node is now multi-threaded, making it faster
    when there are many splines
    ([d2f4fb68f5](https://projects.blender.org/blender/blender/commit/d2f4fb68f5d1747d1827)).
  - The *Cube* mesh primitive node can be about 75% faster at high
    resolutions
    ([aa6c922d99](https://projects.blender.org/blender/blender/commit/aa6c922d99f7ae170)).
  - The *Grid* mesh primitive is now multi-threaded, with a 2.5x
    performance improvement observed on a 4096x4096 vertex grid
    ([cb96435047](https://projects.blender.org/blender/blender/commit/cb96435047506c)).
  - The internals of the *Bounding Box* node have been multi-threaded
    for meshes and point clouds, with observed 4.4x and 5x performance
    improvements
    ([6a71b2af66](https://projects.blender.org/blender/blender/commit/6a71b2af66cf10),
    [6d7dbdbb44](https://projects.blender.org/blender/blender/commit/6d7dbdbb44f379)).
  - The overhead for multi-threading a series of nodes has been lowered,
    for a few percent improvement in some cases
    ([e206a0ae96](https://projects.blender.org/blender/blender/commit/e206a0ae960c2c62d)).

![All of the new geometry nodes added in Blender
3.1](../../images/New_Geometry_Nodes_3.1.png
"All of the new geometry nodes added in Blender 3.1")

### General

  - The result of a field at a certain index can be calculated with the
    **Field at Index** node
    ([b88a37a490](https://projects.blender.org/blender/blender/commit/b88a37a490fa9e)).
  - The new **Accumulate Field** node is useful as a building block for
    more complex setups, building sums of values inside multiple groups
    ([a836ded990](https://projects.blender.org/blender/blender/commit/a836ded9902d67)).
  - It's no longer necessary to add a \`\#frame\` driver to get the
    scene's current animation time, the **Scene Time** node does that
    instead
    ([bd3bd776c8](https://projects.blender.org/blender/blender/commit/bd3bd776c8938d)).
  - The *Compare Floats* node has been upgraded to a generalized
    **Compare** node
    ([1757840843](https://projects.blender.org/blender/blender/commit/17578408434faf)).
      - The new node can compare most data types, not just floats, even
        strings.
      - Special modes are included for 3D Vectors, to compare directions
        dot products, lengths, and more.
  - Close points on meshes and point clouds can be merged with the
    **Merge by Distance** node
    ([ec1b0c2014](https://projects.blender.org/blender/blender/commit/ec1b0c2014a8b9),
    [0ec94d5359](https://projects.blender.org/blender/blender/commit/0ec94d5359d732)).
  - The *Map Range* node now has a vector data type option
    ([5b61737a8f](https://projects.blender.org/blender/blender/commit/5b61737a8f4168)).
  - The *Boolean Math* node has been expanded with more operations
    ([14f6afb090](https://projects.blender.org/blender/blender/commit/14f6afb0900390)).

### Attributes

  - The **Domain Size** node gives the size of any attribute domain,
    like the number of vertices in a mesh
    ([35124acd19](https://projects.blender.org/blender/blender/commit/35124acd197372)).
  - The *Attribute Statistic* node now has a selection input
    ([1a833dbdb9](https://projects.blender.org/blender/blender/commit/1a833dbdb9c4ec)).
  - To make naming more intuitive, the *Transfer Attribute* node
    geometry input has been renamed from "Target" to "Source"
    ([6a16a9e661](https://projects.blender.org/blender/blender/commit/6a16a9e661f134)).
  - An new operator on meshes allows converting generic attributes to UV
    layers or vertex groups, or other generic data types
    ([f6888b530a](https://projects.blender.org/blender/blender/commit/f6888b530ac81a)).
      - The operator also makes it possible to change the domain of an
        attribute on original geometry.
      - *The operator only works on original geometry, so the geometry
        nodes modifier must be applied*

### Instances

  - Instances now support dynamic attributes
    ([97533eede4](https://projects.blender.org/blender/blender/commit/97533eede444217bd28),
    [15ecd47b96](https://projects.blender.org/blender/blender/commit/15ecd47b9685dffdf59),
    [ba8dd0f24f](https://projects.blender.org/blender/blender/commit/ba8dd0f24ff16433c)).
      - Attributes are propagated in the *Realize instances* node,
        including special handling for the \`id\` attribute
        ([f5ce243a56](https://projects.blender.org/blender/blender/commit/f5ce243a56a22d)).
      - Instances attributes are propagated in the *Instance on Points*
        and *Instances to Points* nodes
        ([221b7b27fc](https://projects.blender.org/blender/blender/commit/221b7b27fce393fb3d),
        [386b112f76](https://projects.blender.org/blender/blender/commit/386b112f7652f321f2f)).
      - *Geometry Nodes instance attributes are not usable in shader
        nodes in 3.1.*
  - Real geometry can be converted to an instance with the **Geometry to
    Instance** node
    ([565b33c0ad](https://projects.blender.org/blender/blender/commit/565b33c0ad3196)).
  - Instances can be removed directly in the *Delete Geometry* node
    ([a94d80716e](https://projects.blender.org/blender/blender/commit/a94d80716e688a)).

![Abandoned House Generator by
[Sozap](https://blenderartists.org/t/procedural-abandoned-house-with-geometry-nodes/1363024)](../../images/Sozap_Abandoned_House_Generator.jpg
"Abandoned House Generator by Sozap")

### Meshes

  - The **Dual Mesh** node transforms a mesh's faces into vertices and
    vertices into faces
    ([d54a08c8af](https://projects.blender.org/blender/blender/commit/d54a08c8af1251)).
  - The **Extrude Mesh** node makes it possible to extrude vertices,
    edges, and faces
    ([95981c9876](https://projects.blender.org/blender/blender/commit/95981c98764832)).
  - Various nodes have been added to give basic access to mesh topology
    information.
      - **Vertex Neighbors** gives access to the number of vertices and
        faces connected to each vertex
        ([2814740f5b](https://projects.blender.org/blender/blender/commit/2814740f5be86f)).
      - **Face Neighbors** gives access to each face's vertex count and
        the number of faces connected by an edge
        ([2814740f5b](https://projects.blender.org/blender/blender/commit/2814740f5be86f)).
      - **Edge Vertices** gives access to edge vertex indices and their
        positions
        ([2814740f5b](https://projects.blender.org/blender/blender/commit/2814740f5be86f)).
      - **Edge Neighbors** tells how many faces use each edge
        ([c4cee2e221](https://projects.blender.org/blender/blender/commit/c4cee2e22180e8)).
  - More new nodes give useful data derived from meshes.
      - The **Face Area** node provides access to the surface area of
        each of a mesh's faces
        ([2814740f5b](https://projects.blender.org/blender/blender/commit/2814740f5be86f)).
      - The **Mesh Island** node outputs a separate index for each piece
        of connected vertices in a mesh
        ([069d63561a](https://projects.blender.org/blender/blender/commit/069d63561a4c),
        [4251455dcd](https://projects.blender.org/blender/blender/commit/4251455dcd739b)).
          - There is also an "Island Count" output for the total number
            of islands in the mesh
            ([87c5423c5e](https://projects.blender.org/blender/blender/commit/87c5423c5e1e7b)).
      - The **Edge Angle** node gives the angle between the normals of
        two manifold faces
        ([b7ad58b945](https://projects.blender.org/blender/blender/commit/b7ad58b945c377)).
          - The node can output the signed angle, to tell when an edge
            is convex or concave
            ([4d5c08b938](https://projects.blender.org/blender/blender/commit/4d5c08b9387c5d)).
  - The **Flip Faces** node reverses the normals of selected faces by
    reversing the winding order of their vertices and edges
    ([c39d514a4e](https://projects.blender.org/blender/blender/commit/c39d514a4eacd4)).
  - The **Scale Elements** node changes the size of selected groups of
    faces or edges
    ([d034b85f33](https://projects.blender.org/blender/blender/commit/d034b85f332537)).
  - The *Triangulate* node now has a selection input to limit the
    operation to only some faces
    ([abf30007ab](https://projects.blender.org/blender/blender/commit/abf30007abdac2)).

### Curves

  - The *String to Curves* node has two new outputs
    ([0e86c60c28](https://projects.blender.org/blender/blender/commit/0e86c60c28b62f)):
      - "Line" contains the index of the line in the text layout for
        every character instance.
      - "Pivot Point" gives a position relative to the bounding box of
        each character.
  - The *Resample Curve* node now has a selection input for only
    affecting certain splines
    ([a7672caeb2](https://projects.blender.org/blender/blender/commit/a7672caeb255e3)).
  - The *Curve Parameter* node is renamed to *Spline Parameter* and now
    has a length output in addition to "Factor"
    ([0c6b815855](https://projects.blender.org/blender/blender/commit/0c6b8158554463)).
  - The *Set Handle Position* node has a new "Offset" input like the
    *Set Position* node
    ([3fe735d371](https://projects.blender.org/blender/blender/commit/3fe735d371f4acd6)).
  - The number of control points in a spline is now included in the
    *Spline Length* node
    ([88e9e97ee9](https://projects.blender.org/blender/blender/commit/88e9e97ee907b8)).
  - The *Set Spline Type* node has a better conversion from NURBS to
    Bézier splines
    ([a84621347d](https://projects.blender.org/blender/blender/commit/a84621347d93bc)).
  - The *Curve Handle Positions* node now has a toggle to output the
    position relative to the corresponding control point
    ([38c7378949](https://projects.blender.org/blender/blender/commit/38c7378949b332)).

### Primitives

  - A new **Arc** curve primitive is similar to the *Curve Circle*
    primitive, but can create an incomplete circle
    ([cc1a48e395](https://projects.blender.org/blender/blender/commit/cc1a48e39514a3)).
  - The *Cone* and *Cylinder* mesh primitives now have selection outputs
    for the top, side, and bottom sections
    ([431524aebc](https://projects.blender.org/blender/blender/commit/431524aebc5aa9)).
  - The *Star* curve primitive node has a selection output for the outer
    points
    ([3fe735d371](https://projects.blender.org/blender/blender/commit/3fe735d371f4)).

### Spreadsheet Editor

  - Volume grids are now listed in the spreadsheet, including the grid
    name, data type, and class
    ([ccead2ed9c](https://projects.blender.org/blender/blender/commit/ccead2ed9c6121)).

# Node Editor

The nodes editor has a new context-aware search menu when dragging
links, and significant performance improvements for large node trees,
with fewer unnecessary updates than before.

![The [node timings
overlay](https://docs.blender.org/manual/en/3.1/modeling/geometry_nodes/inspection.html#node-timings-overlay)
shows approximately how long each node took to
execute.](../../images/Node_Timings_Overlay.png
"The node timings overlay shows approximately how long each node took to execute.")

  - When dragging a node link over empty space, a new search menu allows
    quickly connecting a node with a compatible socket
    ([11be151d58](https://projects.blender.org/blender/blender/commit/11be151d58ec0c)).
  - The execution time for nodes can be displayed above nodes (Geometry
    nodes only)
    ([e4986f92f3](https://projects.blender.org/blender/blender/commit/e4986f92f32b096a)).
  - Performance for large node trees has been improved, and unnecessary
    updates have been removed.
      - Editing a node tree causes fewer unnecessary updates than before
        ([7e712b2d6a](https://projects.blender.org/blender/blender/commit/7e712b2d6a0d25)).
          - Inserting reroutes and changing nodes that aren't connected
            to the output don't trigger updates.
          - *Some unnecessary updates still happen when a driver is
            used.*
      - Displaying large node trees with many nodes in the node editor
        can be almost twice as fast
        ([0129178376](https://projects.blender.org/blender/blender/commit/012917837649676e9)).
      - Editing and undo with large node trees should be generally
        faster
        ([dbbf0e7f66](https://projects.blender.org/blender/blender/commit/dbbf0e7f66524a),
        [bdbd0cffda](https://projects.blender.org/blender/blender/commit/bdbd0cffda697e)).
  - Node selection inside Frame nodes has been fixed/improved
    ([1995aae6e3](https://projects.blender.org/blender/blender/commit/1995aae6e3bf)).
