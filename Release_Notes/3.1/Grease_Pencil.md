# Blender 3.1: Grease Pencil

## Line Art

![../../images/Line\_Art\_collection\_inverse.gif](../../images/Line_Art_collection_inverse.gif
"../../images/Line_Art_collection_inverse.gif")

  - New option to toggle collection inverse selection. This allows
    conveniently exclude a collection and select "the rest of" the
    scene.
    ([40c8e23d48](https://projects.blender.org/blender/blender/commit/40c8e23d481c))
      - Differences between collection line art usage flag:
        1.  Collection line art usage is global, used to specify e.g. if
            a collection should appear in calculation at all.
        2.  Invert collection selection is used to easily select a
            collection and "everything else other than that collection",
            making it convenient to set up such cases.

![../../images/Line\_Art\_Facemark\_Contour\_Preserving.png](../../images/Line_Art_Facemark_Contour_Preserving.png
"../../images/Line_Art_Facemark_Contour_Preserving.png")

  - New option to preserve contour lines when using face mark filtering.
    ([dde997086c](https://projects.blender.org/blender/blender/commit/dde997086ce2))

![../../images/Line\_art\_noise\_tolerant\_chaining.png](../../images/Line_art_noise_tolerant_chaining.png
"../../images/Line_art_noise_tolerant_chaining.png")

  - Noise tolerant chaining. This feature takes advantage of original
    line art chain which is a continuous long chain, instead of
    splitting it at each occlusion change, this function tolerates short
    segments of "zig-zag" occlusion incoherence and don't split the
    chain at these points, thus creates a much smoother result.
    ([5ae76fae90](https://projects.blender.org/blender/blender/commit/5ae76fae90da))

<!-- end list -->

  - Back face culling option.
    ([579e8ebe79](https://projects.blender.org/blender/blender/commit/579e8ebe79a1))

## Operators

  - New Merge All Layers option in Merge operator.
    ([556c71a84a](https://projects.blender.org/blender/blender/commit/556c71a84ac1))
  - New option in PDF export to export full scene frame range.
    ([e1c4e5df22](https://projects.blender.org/blender/blender/commit/e1c4e5df225d))

## Tools

  - Now Fill tool allows to use a Dilate negative value to contract the
    filled area and create a gap between fill and stroke.
    ([3b14224881](https://projects.blender.org/blender/blender/commit/3b1422488195))

![../../images/Fill\_Dilate-Contract\_sample.png](../../images/Fill_Dilate-Contract_sample.png
"../../images/Fill_Dilate-Contract_sample.png")

## Modifiers and VFX

  - New Shrinkwrap modifier.
    ([459af75d1e](https://projects.blender.org/blender/blender/commit/459af75d1ed5))
  - New Randomize parameter in Length modifier.
    ([a90c356467](https://projects.blender.org/blender/blender/commit/a90c35646766))
