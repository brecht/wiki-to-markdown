# Blender 3.1.2

Released on Apr 1st 2022, and a followup to 3.1.1 \[which unfortunately
introduced a regression\], Blender 3.1.2 features more bug fixes:

  - Regression: The location of "Viewport Gizmos" is not correct in POSE
    mode (multi-object/armature editing, multiple bones selected).
    [http://developer.blender.org/T96347
    \#96347](http://developer.blender.org/T96347_#96347)
  - Compositor: Missing output UI for Normal node.
    [http://developer.blender.org/T96860
    \#96860](http://developer.blender.org/T96860_#96860)
  - GPU subdivision: Crash on select in Edit Mode with X-Ray on and
    Mirror Modifier. [http://developer.blender.org/T96344
    \#96344](http://developer.blender.org/T96344_#96344)
  - GPU subdivision modifier: Smooth Shade doesn't work.
    [http://developer.blender.org/T96915
    \#96915](http://developer.blender.org/T96915_#96915)
  - OBJ export: mark the new 3.1+ exporter as experimental, and
    reintroduce menu item for the old exporter.
    \[[2cfca7d910](https://projects.blender.org/blender/blender/commit/2cfca7d9101),
    [A19337ef72](https://projects.blender.org/blender/blender/commit/A19337ef729)\]

# Blender 3.1.1

Released on Mar 30th 2022, Blender 3.1.1 features many bug fixes:

  - Fix memory leak evaluating PyDrivers.
    [https://projects.blender.org/blender/blender/commit/eff7e9aa45a
    eff7e9aa45](https://projects.blender.org/blender/blender/commit/eff7e9aa45a_eff7e9aa45)
  - PyAPI: ID property group returns wrong type with iter().
    [http://developer.blender.org/T94121
    \#94121](http://developer.blender.org/T94121_#94121)
  - Grease Pencil \> Stroke \> Normalize Thickness Causes Crash.
    [http://developer.blender.org/T96352
    \#96352](http://developer.blender.org/T96352_#96352)
  - GPencil: Fix unreported select error in Normalize operator.
    [https://projects.blender.org/blender/blender/commit/af6b88c629d
    af6b88c629](https://projects.blender.org/blender/blender/commit/af6b88c629d_af6b88c629)
  - Unable to set active material output node using Python.
    [http://developer.blender.org/T96292
    \#96292](http://developer.blender.org/T96292_#96292)
  - is\_active\_output stopped working correctly.
    [http://developer.blender.org/T96396
    \#96396](http://developer.blender.org/T96396_#96396)
  - Nodegroups don't update the shaders anymore in 3.1.
    [http://developer.blender.org/T96402
    \#96402](http://developer.blender.org/T96402_#96402)
  - Crash with Close Area menu 3D view.
    [http://developer.blender.org/T96416
    \#96416](http://developer.blender.org/T96416_#96416)
  - Issue clicking on stem of arrow gizmos to scale on axis.
    [http://developer.blender.org/T96357
    \#96357](http://developer.blender.org/T96357_#96357)
  - Armature corrupted after undo. [http://developer.blender.org/T96452
    \#96452](http://developer.blender.org/T96452_#96452)
  - Overlapping Volumes broken in Cycles, GPU only (possible
    regression). [http://developer.blender.org/T96381
    \#96381](http://developer.blender.org/T96381_#96381)
  - Multiscatter GGX regression with non-zero roughness and a bump
    texture on a non-manifold object.
    [http://developer.blender.org/T96417
    \#96417](http://developer.blender.org/T96417_#96417)
  - Crash when changing shader to nodegroup in properties.
    [http://developer.blender.org/T96386
    \#96386](http://developer.blender.org/T96386_#96386)
  - GPencil: Fill freezes when use invert and click inside areas.
    [http://developer.blender.org/T96518
    \#96518](http://developer.blender.org/T96518_#96518)
  - Fix object centers & geometry selecting meta-elements in edit-mode.
    [https://projects.blender.org/blender/blender/commit/6aa45434623
    6aa4543462](https://projects.blender.org/blender/blender/commit/6aa45434623_6aa4543462)
  - Stereoscopy with Motion Blur crashes in Eevee.
    [http://developer.blender.org/T96572
    \#96572](http://developer.blender.org/T96572_#96572)
  - C3D Import. [http://developer.blender.org/T14392
    \#14392](http://developer.blender.org/T14392_#14392)
  - Crash when a curve object got an array modifier with a mesh object
    containing a vertex group set as cap.
    [http://developer.blender.org/T96494
    \#96494](http://developer.blender.org/T96494_#96494)
  - Bake normals for multi-resolution object is broken.
    [http://developer.blender.org/T96401
    \#96401](http://developer.blender.org/T96401_#96401)
  - Set ID for Instances not work in Blender 3.1.
    [http://developer.blender.org/T96420
    \#96420](http://developer.blender.org/T96420_#96420)
  - Regression: Can not translate after selecting with Select Circle.
    [http://developer.blender.org/T96515
    \#96515](http://developer.blender.org/T96515_#96515)
  - Regression: NLA crash when reordering tracks if no object is
    selected. [http://developer.blender.org/T96624
    \#96624](http://developer.blender.org/T96624_#96624)
  - Crash on Geometry Nodes Edit Mode tap Tab.
    [http://developer.blender.org/T96316
    \#96316](http://developer.blender.org/T96316_#96316)
  - Regression: Texture Mapping properties of texture nodes are not
    updated in 3D Viewport. [http://developer.blender.org/T96361
    \#96361](http://developer.blender.org/T96361_#96361)
  - Regression: Crash when pressing F3 outside a Blender window if
    Developer extras is on. [http://developer.blender.org/T96705
    \#96705](http://developer.blender.org/T96705_#96705)
  - snap does not work properly. [http://developer.blender.org/T96711
    \#96711](http://developer.blender.org/T96711_#96711)
  - UV Editor doesn't work when GPU Subdivision in the Viewport is
    enabled. [http://developer.blender.org/T96372
    \#96372](http://developer.blender.org/T96372_#96372)
  - Fix missing updates for external render engines rendering tiles.
    [https://projects.blender.org/blender/blender/commit/c91d39e4735
    c91d39e473](https://projects.blender.org/blender/blender/commit/c91d39e4735_c91d39e473)
  - Add Curve Extra Objects addon fails when adding Curvy Curve.
    [http://developer.blender.org/T96342
    \#96342](http://developer.blender.org/T96342_#96342)
  - Regression: Script using bmesh.ops.wireframe and bmesh.ops.bevel
    does not work the same in 3.1 and in 3.01.
    [http://developer.blender.org/T96308
    \#96308](http://developer.blender.org/T96308_#96308)
  - Regression: Crash when executing
    bpy.types.ShapeKey.normals\_vertex\_get.
    [http://developer.blender.org/T96294
    \#96294](http://developer.blender.org/T96294_#96294)
  - Fix text editor failure to move the cursor for syntax errors.
    [https://projects.blender.org/blender/blender/commit/694e20b1415
    694e20b141](https://projects.blender.org/blender/blender/commit/694e20b1415_694e20b141)
  - Gpencil: Inverted Fill makes extra stroke at origin (0,0,0).
    [http://developer.blender.org/T96790
    \#96790](http://developer.blender.org/T96790_#96790)
  - Image editor: not updating after image operation.
    [https://projects.blender.org/blender/blender/commit/f6e2c9bc97d
    f6e2c9bc97](https://projects.blender.org/blender/blender/commit/f6e2c9bc97d_f6e2c9bc97)
  - Regression: Blender 3.1 bake from multires not reflected in the
    Image Editor. [http://developer.blender.org/T96670
    \#96670](http://developer.blender.org/T96670_#96670)
  - Regression: GPencil primitives handlers not working.
    [http://developer.blender.org/T96828
    \#96828](http://developer.blender.org/T96828_#96828)
  - Regression: Snapping is broken with proportional editing.
    [http://developer.blender.org/T96812
    \#96812](http://developer.blender.org/T96812_#96812)
  - Regression: Cutting a strip with keyframes in the VSE deletes the
    keyframes from the original (left) strip.
    [http://developer.blender.org/T96699
    \#96699](http://developer.blender.org/T96699_#96699)
  - Regression: VSE Wipe transition does not work.
    [http://developer.blender.org/T96582
    \#96582](http://developer.blender.org/T96582_#96582)
  - Heap corruption in file\_browse\_exec.
    [http://developer.blender.org/T96691
    \#96691](http://developer.blender.org/T96691_#96691)
  - GPencil: Scripting weight\_get cannot retrieve weights of certain
    vertex groups. [http://developer.blender.org/T96799
    \#96799](http://developer.blender.org/T96799_#96799)
  - vertex paint face selction display bug gpu subdivision.
    [http://developer.blender.org/T96356
    \#96356](http://developer.blender.org/T96356_#96356)
  - New OBJ exporter fixes:
      - Reintroduce setting presets and an option to skip modifiers.
        [http://developer.blender.org/T96303
        \#96303](http://developer.blender.org/T96303_#96303)
      - Fix wrong normals on non-uniformly scaled objects.
        [http://developer.blender.org/T96430
        \#96430](http://developer.blender.org/T96430_#96430)
      - Fix scaling factor not being applied correctly.
        [http://developer.blender.org/T96415
        \#96415](http://developer.blender.org/T96415_#96415)
      - Fix export of "material groups" option.
        [http://developer.blender.org/T96470
        \#96470](http://developer.blender.org/T96470_#96470)
      - Fix issues with some apps (e.g. Procreate) with multiple
        materials; the new exporter was not grouping faces by material.
        [http://developer.blender.org/T96511
        \#96511](http://developer.blender.org/T96511_#96511)
