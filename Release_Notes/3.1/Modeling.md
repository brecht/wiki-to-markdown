### General

  - Mesh vertex and face normals have been refactored, reducing
    unnecessary calculation, improving caching, and generally improving
    performance in more complex files
    ([cfa53e0fbe](https://projects.blender.org/blender/blender/commit/cfa53e0fbeed71)).
      - These changes do not affect pure edit mode performance.
  - Mesh bounding box calculation has been multi-threaded, which can
    make exiting edit mode faster
    ([6a71b2af66](https://projects.blender.org/blender/blender/commit/6a71b2af66cf10)).
  - The accuracy of NURBS knots calculation has been improved, allowing
    the combination of Bézier and cyclic modes
    ([45d038181a](https://projects.blender.org/blender/blender/commit/45d038181ae259)).
  - An new operator on meshes allows converting generic attributes to UV
    layers or vertex groups, or other generic data types
    ([f6888b530a](https://projects.blender.org/blender/blender/commit/f6888b530ac81a)).
      - The operator also makes it possible to change the domain of an
        attribute on original geometry.

### Subdivision Surface

  - The Subdivision Surface modifier now benefits from GPU acceleration
    ([eed45d2a23](https://projects.blender.org/blender/blender/commit/eed45d2a239a)).
      - To enable or disable the GPU acceleration, a new user preference
        setting is available under *Viewport \> Subdivision*.

<!-- end list -->

  - Vertex creasing for subdivision surfaces is now supported
    ([4425e0cd64](https://projects.blender.org/blender/blender/commit/4425e0cd64ff)).
      - This allows to mark vertices as arbitrarily sharp to create
        interesting shapes more efficiently:

![Example shapes made using vertex and edge
creasing.](../../images/Vertex_crease_cycles.png
"Example shapes made using vertex and edge creasing.")
