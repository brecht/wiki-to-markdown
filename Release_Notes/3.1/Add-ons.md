# Blender 3.1: Add-ons

## Copy Global Transform

The Copy Global Transform add-on makes it possible to copy the
world-space transform of the active object/bone, and paste it onto any
object/bone. This could be the same one at a different point in time, or
you can copy the FK control bone's global transform to the IK control
bone. Or to some object. If you want to have it locked to that
particular world transform for a number of frames, you can paste to
selected keyframes ("smart bake") or even let it create new frames (on
1s, 2s, etc.). The add-on will do all the counter-animation required to
keep the paste-target at the same global transform.

[File:blender-release-notes-31-copy-global-transform-panel.png|Global](File:blender-release-notes-31-copy-global-transform-panel.png%7CGlobal)
Transform panel, in the Animation tab of the 3D Viewport.
[File:blender-release-notes-31-copy-global-transform-baking.png|Result](File:blender-release-notes-31-copy-global-transform-baking.png%7CResult)
of "Paste and Bake": a range of keys was created to counter-animate the
parent, ensuring that the world transform remains the same.

## Rigify

  - The \`super\_finger\` rig can now place the optional IK control on a
    different layer
    ([rBA9030e2c](https://projects.blender.org/blender/blender-addons/commit/9030e2c6d1a)).
  - The advanced generate settings panel has been overhauled, removing
    the overwrite/new toggle and name field, but adding a reference to
    the widget collection
    ([rBAece39d8](https://projects.blender.org/blender/blender-addons/commit/ece39d809c)).
  - Limbs can now be uniformly scaled using the gear shaped bone at
    their base
    ([rBA2f1c38f](https://projects.blender.org/blender/blender-addons/commit/2f1c38fd507)).
  - Limb FK controls now use the Aligned inherit scale mode
    ([rBA3fc4640](https://projects.blender.org/blender/blender-addons/commit/3fc46407617)).
  - The leg rig can now generate two separate toe controls for IK and
    FK, which is necessary for 100% accurate snapping. This is enabled
    by default in metarigs.
    ([rBA0391f86](https://projects.blender.org/blender/blender-addons/commit/0391f865e12d))

## FBX I/O

  - Improved export speed of 'rested' armatures
    ([rBA1b0254b](https://projects.blender.org/blender/blender-addons/commit/1b0254b5315b)).

## OBJ I/O

  - Fix for [\#94516](http://developer.blender.org/T94516) brought back
    roughness handling into expected MTL behavior, but therefore
    introduces a value shifting of this roughness parameter when
    re-importing older OBJ files exported by Blender
    ([rBAf26299b](https://projects.blender.org/blender/blender-addons/commit/f26299bacc1)).
  - OBJ exporter is now implemented in C++, see [the Pipeline, Assets &
    I/O
    page](Pipeline_Assets_IO.md#Obj_I/O)
    for details.

## Atomic Blender (PDB/XYZ)

**PDB/XYZ importer**

  - Fix for [\#94008](http://developer.blender.org/T94008) and
    [\#94292](http://developer.blender.org/T94292) automatically changes
    to the \`OBJECT mode\`
    ([rBA8372ef9](https://projects.blender.org/blender/blender-addons/commit/8372ef96ade7),
    [rBA2829c04](https://projects.blender.org/blender/blender-addons/commit/2829c040f488)).
    No error message should appear when the \`EDIT mode\` is initially
    active just before the import.

<!-- end list -->

  - Fix in
    [rBAb825c2d](https://projects.blender.org/blender/blender-addons/commit/b825c2d31ad3)
    leads now to smooth sticks in an instancing vertice structure if
    option \`Smooth\` is set in the PDB importer.

<!-- end list -->

  - Fix in
    [rBA98003ac](https://projects.blender.org/blender/blender-addons/commit/98003acc981d)
    - sticks in instancing vertice structure: when decreasing the
    diameter of the sticks, e.g., with help of the \`Utility Panel\`
    (utility \`Change stick size\`), the equidistant planes of the mesh
    structure are not visible anymore. They now have a 100x smaller
    size. Note that these planes are needed for the instancing vertice
    structure.

<!-- end list -->

  - Improvement of the color handling for atoms
    ([rBA1b95d39](https://projects.blender.org/blender/blender-addons/commit/1b95d391dccb),
    [rBAf1d2eca](https://projects.blender.org/blender/blender-addons/commit/f1d2eca09bac),
    [rBAc00916b](https://projects.blender.org/blender/blender-addons/commit/c00916b26b08),
    [rBAefcceb6](https://projects.blender.org/blender/blender-addons/commit/efcceb62d4a8),
    [rBA7d11bb3](https://projects.blender.org/blender/blender-addons/commit/7d11bb351e58),
    [rBAee7f952](https://projects.blender.org/blender/blender-addons/commit/ee7f95212b96),
    [rBA6e910cf](https://projects.blender.org/blender/blender-addons/commit/6e910cf217f0),
    [rBA7f5d0ab](https://projects.blender.org/blender/blender-addons/commit/7f5d0ab6beb3)).
    As a standard shader, the [Principled BSDF
    shader](https://docs.blender.org/manual/en/dev/render/shader_nodes/shader/principled.html)
    is used for Cycles and Eevee. Also a few properties for Eevee are
    automatically set.

**Complete revision of the \`Utility Panel\`**

  - Fix in
    [rBAe54b9d2](https://projects.blender.org/blender/blender-addons/commit/e54b9d2a4d81)
    does not lead anymore to a crash of Blender when separating atoms
    from an instancing vertice structure.

<!-- end list -->

  - Fix in
    [rBA4009ff1](https://projects.blender.org/blender/blender-addons/commit/4009ff189d59)
    does not lead anymore to an error message when using option
    \`Default values\` in the \`Utility Panel\`.

<!-- end list -->

  - Fix in
    [rBA84f5f46](https://projects.blender.org/blender/blender-addons/commit/84f5f4699232)
    and
    [rBAfd5697e](https://projects.blender.org/blender/blender-addons/commit/fd5697ebcf87):
    changing the material properties of atoms (utility \`Change atom
    shape\`) does also change the material properties of the related
    sticks if present.

<!-- end list -->

  - All the object 'operators' in section \`Change atom shape\` got
    updated for color handling with respect to Eevee, Cycles and the
    Principled BSDF shader
    ([rBA3012911](https://projects.blender.org/blender/blender-addons/commit/3012911034aa),
    [rBA9be1af0](https://projects.blender.org/blender/blender-addons/commit/9be1af0f30f6)).

<!-- end list -->

  - Extension of the option \`Custom data file\`
    ([rBAeb9a4e7](https://projects.blender.org/blender/blender-addons/commit/eb9a4e79d3df)):
    all properties of the Principled BSDF shader can now be set in the
    ASCII data file for each element. Some important properties for
    Eevee can be set as well. With this, the sizes and material
    properties of selected atoms in an atomic structure can be changed
    at once. The user can therefore store her/his own material
    properties inside this file and use it at any time for any atomic
    structures. For more information, see the
    [documentation](https://docs.blender.org/manual/en/dev/addons/import_export/mesh_atomic.html).

## Compatibility Issues

  - OBJ/MTL roughness conversion with Blender's own BSDF roughness
    parameter has changed, see
    [rBAf26299b](https://projects.blender.org/blender/blender-addons/commit/f26299bacc1).
