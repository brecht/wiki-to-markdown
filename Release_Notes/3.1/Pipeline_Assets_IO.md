# Blender 3.1: Pipeline, Assets & IO

## Alembic

  - Animated vertex colors can now be exported to Alembic
    ([rB4e247894](https://projects.blender.org/blender/blender/commit/4e2478940eb8)).
  - Reading override layers is now supported
    ([rB0a08ac25](https://projects.blender.org/blender/blender/commit/0a08ac2528e)).
    Override layers allow to override data (attributes, animations,
    topology, etc.) from Alembic caches as long the hierarchies inside
    of the original file and the layer(s) match.

![User Interface for the Alembic override
layers|center](../../images/Override_layers_ui.png
"User Interface for the Alembic override layers|center")

![Video demonstration of updating an animation using
layers.|center](../../videos/Abc_layers.mp4
"Video demonstration of updating an animation using layers.|center")

## Asset Browser

  - Asset libraries are indexed for faster browsing
    ([rBdbeab82a](https://projects.blender.org/blender/blender/commit/dbeab82a89),
    [rB36068487](https://projects.blender.org/blender/blender/commit/36068487d0)).
  - Node groups can be added to asset libraries, and support drag & drop
    into the node editor
    ([37848d1c8e](https://projects.blender.org/blender/blender/commit/37848d1c8edf18ac52095cec)).

## Image I/O

  - Enabled support for OpenEXR DWAB compression
    ([rBb1bd0f8f](https://projects.blender.org/blender/blender/commit/b1bd0f8ffdaf))

## Obj I/O

  - A new C++ version of the .obj exporter is provided as a replacement
    for the Python addon
    ([rB4e44cfa3](https://projects.blender.org/blender/blender/commit/4e44cfa3d996))

Some speed comparisons:

<table>
<thead>
<tr class="header">
<th><p>Default cube + 9 subdivision<br />
surface levels. (triangulated faces)</p></th>
<th><p>File Size (MB)</p></th>
<th><p>Time Old (s)</p></th>
<th><p>Time New (s)</p></th>
<th><p>Speed-up</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Export</p></td>
<td><p>365</p></td>
<td><p>132</p></td>
<td><p>14</p></td>
<td><p>9.4X</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

<table>
<thead>
<tr class="header">
<th><p>Default Cylinder: 2038 copies.<br />
(non-triangulated faces)</p></th>
<th><p>File Size (MB)</p></th>
<th><p>Time Old (s)</p></th>
<th><p>Time New (s)</p></th>
<th><p>Speed-up</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Export</p></td>
<td><p>20</p></td>
<td><p>12.3</p></td>
<td><p>.9</p></td>
<td><p>13x</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

  - For updates regarding the python add-on, please check the [Add-ons
    page](Add-ons.md#OBJ_I/O).

## USD

### Exporter

  - Export USD Preview Surface shaders from simple Principled BSDF node
    networks and allow exporting textures referenced by materials to a
    textures directory next to the USD file
    ([rBc85c52f2](https://projects.blender.org/blender/blender/commit/c85c52f2ce478ab0e30c5e93fd5a5cb812db232f))

## glTF 2.0

### Importer

  - Implement a user extension system
    ([rBAa5205b0](https://projects.blender.org/blender/blender-addons/commit/a5205b0289717dc418c1a6070c89039204a2e951))
  - Performance enhancement when images are packed
    ([rBAc2eeb9b](https://projects.blender.org/blender/blender-addons/commit/c2eeb9bb4732602c324a011f7f913a33a5e8b1f2))
  - Use relative path when possible
    ([rBA4d2d118](https://projects.blender.org/blender/blender-addons/commit/4d2d118f9add7f634d6c2d7428f4c4d307c21368))
  - Set empty display size, based on distance parent/children
    ([rBAec45566](https://projects.blender.org/blender/blender-addons/commit/ec45566f05f0bebad99d2fddd1c2f2815edf9a4e))
  - Fix invalid index mesh name
    ([rBAc8445b6](https://projects.blender.org/blender/blender-addons/commit/c8445b67fc2dd3696c393c5dc8cdba0da6a911c3))

### Exporter

  - Manage tweak mode in NLA when needed
    ([rBAf35aff3](https://projects.blender.org/blender/blender-addons/commit/f35aff344ff651364a54cc6c9fa75bf2699ad2a8))
  - Better texture image management
    ([rBA97d4577](https://projects.blender.org/blender/blender-addons/commit/97d4577285f5f7a75f663559a6a3ad6c692c087a),
    [rBA38be61e](https://projects.blender.org/blender/blender-addons/commit/38be61ebb868c0b56c140286af8720b2e7d0de11),
    [rBA38be61e](https://projects.blender.org/blender/blender-addons/commit/38be61ebb868c0b56c140286af8720b2e7d0de11))
  - Fix regression when exporting armature object animation with only 1
    key
    ([rBAf1c4959](https://projects.blender.org/blender/blender-addons/commit/f1c4959cd05682855c6bfa0e96924f0b6a2676aa))
  - Fix back compatibility for use\_selection
    ([rBA503570a](https://projects.blender.org/blender/blender-addons/commit/503570ad703268ef55cfa38beaa28ad15e3ecc57))
  - Avoid issue with setting frame with python v \>= 3.10
    ([rBAa0d1647](https://projects.blender.org/blender/blender-addons/commit/a0d1647839180388805be150794a812c44e59053))
  - Use custom range on action
    ([rBA19385db](https://projects.blender.org/blender/blender-addons/commit/19385dbc57f566e27914a361768d3aa6dad95ec6))
  - Remove proxy management
    ([rBA6da09c3](https://projects.blender.org/blender/blender-addons/commit/6da09c309d756cecac66ac27257414c36f9b51b7))
  - Manage factors for Clearcoat extension
    ([rBAdc2d9b0](https://projects.blender.org/blender/blender-addons/commit/dc2d9b0864daf6805c89632004f8c98c91cc311b))
  - Workaround for image size copy not stored
    ([rBA3b088fc](https://projects.blender.org/blender/blender-addons/commit/3b088fc33dc62950887977ee3ecd0ce01d03ebd3))
  - Option to not export texture images
    ([rBA4003baf](https://projects.blender.org/blender/blender-addons/commit/4003baf03d7d6d78ef4c16be79c73edadf87759c))
  - New animation hooks
    ([rBAbfcf35f](https://projects.blender.org/blender/blender-addons/commit/bfcf35f7464b9445322b2ba3bc8214339bd91317))
  - Optimized anims can now be disabled
    ([rBAb2adbc6](https://projects.blender.org/blender/blender-addons/commit/b2adbc6ba56d9e4380325866b8a3ae6d2d907a39))
