# Image editor

  - Image editor is able to handle large images.
    ([bdd74e1e93](https://projects.blender.org/blender/blender/commit/bdd74e1e9338))
  - Tearing artifacts have been fixed on selected platforms.
    ([bdd74e1e93](https://projects.blender.org/blender/blender/commit/bdd74e1e9338))
