# Blender 3.1: User Interface

## General

  - Preferences / File Paths / Fonts now starts with an initial default
    path for Mac & Linux, as already done for Windows.
    ([02a9377da0](https://projects.blender.org/blender/blender/commit/02a9377da0da)).
  - Open File Browser in thumbnail view when browsing for fonts.
    ([9cfffe8468](https://projects.blender.org/blender/blender/commit/9cfffe84681b)).
  - View pie menu for Node Editor, Video Sequencer, Dopesheet, Graph,
    NLA, Image, Clip and Outliner.
    ([e1bd4bbb66](https://projects.blender.org/blender/blender/commit/e1bd4bbb6668)).
  - Font point sizes can now be set using floating-point values.
    ([73047c69ea](https://projects.blender.org/blender/blender/commit/73047c69ea80)).
  - The curve mapping widget layout has been slightly tweaked for
    consistency
    ([8c55481e33](https://projects.blender.org/blender/blender/commit/8c55481e337c9c)).
  - Improved feedback while doing interactive (live) area splitting
    ([fa8c2c7885](https://projects.blender.org/blender/blender/commit/fa8c2c788579)).
