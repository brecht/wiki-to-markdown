# Blender 3.1: Python API

## Bundled Python

### Python 3.10

Python has been upgraded from version 3.9 to 3.10, there are some
changes that may impact script authors.

  - Python 3.10 no longer implicitly converts floats to int's ([issue
    linked](https://bugs.python.org/issue37999)). This means functions
    that previously accepted float typed values will raise a type
    error.  
    Floating point arguments must now be explicitly converted to
    integers (see example commits
    ([rBAbb62f10](https://projects.blender.org/blender/blender-addons/commit/bb62f10715a871d7069d2b2c74b2efc97c3c350c),
    [rBA7476c1a](https://projects.blender.org/blender/blender-addons/commit/7476c1ac248472c1442b3fb1cb3e0a79747e48f6)).

## Text Editor

  - User preference to auto-close brackets and quotes
    ([c4ea5cb1a3](https://projects.blender.org/blender/blender/commit/c4ea5cb1a3811cbf685a1542a69b33ba3d6345f1)).

## Crazy space

Crazy space is an implementation of what often is called deformation
space: a mapping of per-vertex orientation between un-deformed object
and object after shape keys and deformation modifiers are applied. The
crazy space was used internally to support features like sculpting on a
deformed mesh: to "cancel out" mesh deformation on a brush stroke and
apply it on a base mesh.

The crazy space is now available via the Python API
([196da819ba](https://projects.blender.org/blender/blender/commit/196da819ba4)).The
example use is:

``` Python
object.crazyspace_eval(depsgraph, scene)

# When we have a difference between two vertices and want to convert
# it to a space to be stored, say, in shapekey:
delta_in_orig_space = rigged_ob.crazyspace_displacement_to_original(
    vertex_index=i, displacement=delta)
    
# The reverse of above.
delta_in_deformed_space = rigged_ob.crazyspace_displacement_to_deformed(
    vertex_index=i, displacement=delta)

object.crazyspace_eval_clear()
```

It has similar limitations and expectations than sculpting on a deformed
mesh. Mainly if there is a shape key then the orientation mapping is
calculated assuming the active shape key defines basis for the
undeformed space.

[The explanation and demo](https://developer.blender.org/D13892#368898)
how this is useful for riggers written up by Demeter Dzadik.

## Other Additions

  - \`Mesh\` has new \`vertex\_normals\` and \`polygon\_normals\`
    properties with that provide access to a contiguous array of values
    ([b7fe27314b](https://projects.blender.org/blender/blender/commit/b7fe27314b25a7),
    [cfa53e0fbe](https://projects.blender.org/blender/blender/commit/cfa53e0fbeed71)).
      - Internally, normals are no longer stored in \`MeshVertex\`
        directly (or the internal struct \`MVert\`), though the
        \`MeshVertex.normal\` property is still available.
  - New convenience properties \`Object.children\_recursive\` and
    \`Collection.children\_recursive\`
    ([7c568e7d36](https://projects.blender.org/blender/blender/commit/7c568e7d36710aba782a628dfff3b8bcea88be3b)).

## Breaking Changes

  - \`Action.frame\_range\` will now return the manually set frame range
    if available. Add-ons are advised to evaluate whether this is
    appropriate for their use case, or whether they need to switch to
    \`Action.curve\_frame\_range\` which always returns the range
    computed from the keys
    ([rB5d59b386](https://projects.blender.org/blender/blender/commit/5d59b38605d6)).
