# Blender 3.1: VFX & Video

## Sequencer

  - Add meta.separate() Python API function
    ([9cf3d841a8](https://projects.blender.org/blender/blender/commit/9cf3d841a823))
  - Support drag and drop for datablocks
    ([b42494cf6b](https://projects.blender.org/blender/blender/commit/b42494cf6b00))
  - Add drag and drop handler for preview area
    ([34370d9fdf](https://projects.blender.org/blender/blender/commit/34370d9fdf33))
  - Support copy pasting strips with animation across different scenes.
    Only fcurves are supported.
    ([eddad4e9a1](https://projects.blender.org/blender/blender/commit/eddad4e9a1ac))
  - Use timecodes by default, Proxy size 25% is no longer set by
    default.
    ([eab066cbf2](https://projects.blender.org/blender/blender/commit/eab066cbf2da))
  - Automatic proxy building: Build proxies only for slow movies
    ([b45e71e22c](https://projects.blender.org/blender/blender/commit/b45e71e22cc7))

## Compositor

  - Color space conversion node
    ([6beaa29791](https://projects.blender.org/blender/blender/commit/6beaa297918b))
  - Scene time node
    ([b2ccd8546c](https://projects.blender.org/blender/blender/commit/b2ccd8546c72))
