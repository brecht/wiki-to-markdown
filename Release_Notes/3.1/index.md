# Blender 3.1 Release Notes

Blender 3.1 was released on March 9, 2022.

Check out the final [release notes on
blender.org](https://www.blender.org/download/releases/3-1/).

## [Animation & Rigging](Animation_Rigging.md)

## [Core](Core.md)

## [EEVEE & Viewport](EEVEE.md)

## [Grease Pencil](Grease_Pencil.md)

## [Modeling](Modeling.md)

## [Nodes & Physics](Nodes_Physics.md)

## [Pipeline, Assets & I/O](Pipeline_Assets_IO.md)

## [Python API & Text Editor](Python_API.md)

## [Render & Cycles](Cycles.md)

## [Sculpt, Paint, Texture](Sculpt.md)

## [User Interface](User_Interface.md)

## [VFX & Video](VFX.md)

## [Add-ons](Add-ons.md)

## Compatibility

See relevant sections in
[Add-Ons](Add-ons.md#Compatibility_Issues).

## [Corrective Releases](Corrective_Releases.md)
