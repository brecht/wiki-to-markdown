# Blender 2.83: Physics

## Hair

Hair now uses the same collision engine as cloth, yielding much more
reliable collisions.
([rBd42a7bbd](https://projects.blender.org/blender/blender/commit/d42a7bbd6ea5))

<center>

|                                                                                                                                                                                                                   |
| ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| ![../../videos/Experimental\_hair\_collision\_improvement-200554110.mp4](../../videos/Experimental_hair_collision_improvement-200554110.mp4 "../../videos/Experimental_hair_collision_improvement-200554110.mp4") |

style="caption-side: bottom" | New hair collision example. Left is the
previous method and right is the new method.

</center>

## Cloth

  - Optimization for self-collision, improving overall cloth simulation
    performance by 15-20% in tests.

## Fluid

  - Performance Optimizations:
      - Optimization for scenes with static (i.e. non animated) flow /
        effector objects. Baking will automatically be faster for those
        objects.
      - Improved baking speed for scenes with lots of obstacles.
        Effector objects now use the same bounding boxes as flow objects
        when the geometry is being processed.
        ([rBa5c4a44d](https://projects.blender.org/blender/blender/commit/a5c4a44df67e))
      - Loading times for particle (liquid and/or secondary particles)
        and mesh files have been improved.
        ([rB7d59f847](https://projects.blender.org/blender/blender/commit/7d59f84708fc),
        [rB1280f3c0](https://projects.blender.org/blender/blender/commit/1280f3c0ae96))
  - New UI Options:
      - Enable / disable effector objects during the simulation
      - Delete fluid inside obstacles
      - Subframes for obstacle objects (improves detection of fast
        moving obstacles)
        ([rBcda81d5a](https://projects.blender.org/blender/blender/commit/cda81d5a4d39),
        [rB1c3ded12](https://projects.blender.org/blender/blender/commit/1c3ded12f439))
  - Usability:
      - Easier setup for planar emission / obstacle objects: Planes
        always have non-zero thickness and will always affect the
        simulation.
        ([rBcda81d5a](https://projects.blender.org/blender/blender/commit/cda81d5a4d39))
      - Cancelling bake jobs is quicker (particularly notable when using
        domains with high resolutions)
  - Important Bug Fixes:
      - Secondary (liquid) particles out of sync relative to main
        simulation ([T73988](https://developer.blender.org/T73988))
      - Secondary (liquid) particles show up in organized unrealistic
        structure ([T73552](https://developer.blender.org/T73552))
  - Other:
      - Smoke rendering in 3D viewport solid mode was changed to be more
        accurate and support rendering dark smoke. This changes the look
        in existing .blend files.
      - Resolultion default was decreased along with changing the
        default cache method Replay to improve instantaneous playback
