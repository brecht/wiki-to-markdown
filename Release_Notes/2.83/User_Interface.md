# Blender 2.83: User Interface

## Animation Editors

  - Improved graph editor channels drawing
    ([4aa0e21](https://projects.blender.org/blender/blender/commit/4aa0e21))
  - Improved graph and NLA editor sidebar panel layout
    ([04e318de3a](https://projects.blender.org/blender/blender/commit/04e318de3a4))
  - Rename 'View Frame' to 'Go to Current Frame'
    ([dee01cad19](https://projects.blender.org/blender/blender/commit/dee01cad19f))
  - Change 'Lock Time to Other Windows' \> 'Sync Visible Range' and add
    to Sequencer.
    ([fe772bf818](https://projects.blender.org/blender/blender/commit/fe772bf8186))
  - Changed dopesheet & timeline filter names
    ([9ccc73ade8](https://projects.blender.org/blender/blender/commit/9ccc73ade8a25db5bb)):
      - Only Selected → Only Show Selected
      - Show Errors → Only Show Errors
      - Display Hidden → Show Hidden
  - Show animation cancel button in all status-bars
    ([25cb12dc71](https://projects.blender.org/blender/blender/commit/25cb12dc71e))

## File Browser

  - Support for file attributes and hidden files.
    ([1fb62d1272](https://projects.blender.org/blender/blender/commit/1fb62d1272db))
  - Show icons for special folder locations.
    ([84c9a99cca](https://projects.blender.org/blender/blender/commit/84c9a99cca90))
    ([1af8e0cc6c](https://projects.blender.org/blender/blender/commit/1af8e0cc6c47))
    ([a622e29a25](https://projects.blender.org/blender/blender/commit/a622e29a2541))
  - Spport for Ctrl+F shortcut to start filtering files.
    ([e8ab0137f8](https://projects.blender.org/blender/blender/commit/e8ab0137f876))
  - Recognizes .fountain files as text files.
    ([aa4579c35f](https://projects.blender.org/blender/blender/commit/aa4579c35f93))

## Miscellaneous

  - Improve toolbar width snapping
    ([ac7eb71089](https://projects.blender.org/blender/blender/commit/ac7eb710890))
  - Consolidate masking-related brush controls
    ([c01246f6c0](https://projects.blender.org/blender/blender/commit/c01246f6c0f))
  - Change Area Duplicate Icon
    ([452834f1e3](https://projects.blender.org/blender/blender/commit/452834f1e3a))
  - On Windows, restore app from minimized when closing from taskbar
    ([22ca8b8aee](https://projects.blender.org/blender/blender/commit/22ca8b8aee99))
  - Dynamically enable and disable Edit Menu items based on whether
    currently applicable
    ([b707504973](https://projects.blender.org/blender/blender/commit/b7075049732a))
  - Visual changes to Info Editor
    ([aa919f3e82](https://projects.blender.org/blender/blender/commit/aa919f3e8202))
  - Improved keyboard symbols for menus and status bar, following
    platform conventions.
    ([dc3f073d1c](https://projects.blender.org/blender/blender/commit/dc3f073d1c52))
    ([f051d47cdb](https://projects.blender.org/blender/blender/commit/f051d47cdbce))
  - Overflowing text lines now use ellipsis character to indicate line
    continuation.
    ([63d5b974cc](https://projects.blender.org/blender/blender/commit/63d5b974ccfa))
  - Larger Alert icons for dialogs
    ([a210b8297f](https://projects.blender.org/blender/blender/commit/a210b8297f5a))
  - Language Selection on "Quick Start" Splash screen
    ([a210b8297f](https://projects.blender.org/blender/blender/commit/a210b8297f5a))
  - Improved UI Layout for 3D Mouse Settings, all settings accessible
    from Preferences
    ([ce0db0d329](https://projects.blender.org/blender/blender/commit/ce0db0d329985e4))
  - Keep outliner selection in sync for more operators
    ([5d14463e1a](https://projects.blender.org/blender/blender/commit/5d14463e1aee))
    ([c06a40006d](https://projects.blender.org/blender/blender/commit/c06a40006d6c))
    ([f772a4b8fa](https://projects.blender.org/blender/blender/commit/f772a4b8fa87))
  - Display and allow creation of drivers and keyframes for bone
    visibility in the outliner
    ([6f8d99322c](https://projects.blender.org/blender/blender/commit/6f8d99322cf6))
