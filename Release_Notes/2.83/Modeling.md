# Blender 2.83: Modeling

## Multiresolution

The multires modifier was rewritten to solve artifacts with the
following operations:

  - Subdivide
  - Reshape
  - Apply base

Subdivision type (Catmull-Clark and Simple) and Quality (accuracy of
vertex positions) can now only be changed when there are no subdivisions
yet. The reason for this is to prevent final shape distortion.

Optimal Display is now enabled by default.

## Modifiers

  - Corrective Smooth: new Scale parameter, to scale along with any
    armature scaling.
    ([915998111b](https://projects.blender.org/blender/blender/commit/915998111bc)).
  - Ocean: the random pattern was changed so that the overall surface
    shape is preserved when changing the resolution.
  - Ocean: add new spectra modes to the ocean modifier to simulate
    different bodies of water
    ([6ce709dceb](https://projects.blender.org/blender/blender/commit/6ce709dceb8)).
  - Remesh: new Voxel mode, matching the Voxel Remesh operator.
  - Solidify: option to assign vertices of the rim and/or newly
    generated shell to specified vertex groups.
  - Solidify: better cooperation with a following Bevel modifier.
  - Solidify: option to ensure generation of flat faces from
    vgroup-controled thickness in the complex non-manifold algorithm.
  - Surface Deform: influence is now specified by a strength parameter
    and a vertex group.
  - UV Warp: option to specify 2D transform parameters.
  - Warp: new option to use armature bones as from/to targets.
  - Options to invert the influence of the vertex group were added to
    many modifiers.
  - Options to invert the effect of the Weight Edit modifiers'
    mapping/falloff were added.
  - Many modifiers using texture masking can now also use armature bones
    as texture coordinates source.

## Curve Editing - Extrude Tool Improvements

  - This tool no longer duplicates points creating new splines but now
    it extrudes contiguous selected segments.

![Curve Extrude](../../images/Curve_extrude.gif "Curve Extrude")  

## Metaballs

  - Metaballs can be instanced via mesh vertices/faces. The visibility
    of the original metaballs can now be controlled for the viewport and
    render, by setting the visibility options of the instancing mesh.
    ([a8441fc900](https://projects.blender.org/blender/blender/commit/a8441fc90086))
