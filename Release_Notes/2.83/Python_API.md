# Blender 2.83: Python API

## Compatibility

  - Add-ons: in \`bl\_info\`, \`wiki\_url\` was renamed to \`doc\_url\`.
    \`wiki\_url\` is now deprecated.
    ([a0ea0153c2](https://projects.blender.org/blender/blender/commit/a0ea0153c262))
  - Custom shaders need to be patched using the function
    \`blender\_srgb\_to\_framebuffer\_space\`. This ensure the output
    color is transformed to the correct color space.
    ([21c658b718](https://projects.blender.org/blender/blender/commit/21c658b718b9),
    [\#74139](http://developer.blender.org/T74139))

Example of a patched fragment shader:

``` GLSL
out vec4 out_color;

void main() {
  vec4 srgb_color = vec4(0.5);
  out_color = blender_srgb_to_framebuffer_space(srgb_color);
}
```

  - Blender version numbers changed to standard \`MAJOR.MINOR.PATCH\`
    format. Most scripts will continue to work without changes.
      - \`bpy.app.version\` now returns \`(MAJOR, MINOR, PATCH)\`. The
        \`PATCH\` version is \`0\` for the first stable release and
        increases for every bugfix release. Sub-version numbers or
        letters are no longer used.
      - \`bpy.app.version\_string\` now includes \`Alpha\` or \`Beta\`
        for releases under development, for example \`2.90 Alpha\`.
      - Add-ons compatible with Blender 2.83 should set \`"blender": (2,
        83, 0)\` in \`bl\_info\`, or lower if compatible with earlier
        versions.

## New

  - Nodes: new \`bl\_icon\` on custom nodes to display an icon in the
    node header.
    ([120a38c](https://projects.blender.org/blender/blender/commit/120a38c)).
  - New \`foreach\_set\` and \`foreach\_get\` methods on array
    properties like image pixels. This is significantly more efficient
    than accessing individual elements.
    ([9075ec8269](https://projects.blender.org/blender/blender/commit/9075ec8269e7))
