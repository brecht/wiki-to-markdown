# Blender 2.83 Release Notes

Blender 2.83 was released on June 3, 2020.

Check out the final [release notes on
blender.org](https://www.blender.org/download/releases/2-83/).

This release includes long-term support, see the [LTS
page](https://www.blender.org/download/lts/) for a list of bugfixes
included in the latest version.

## [Grease Pencil](Grease_Pencil.md)

Completely rewritten from scratch, the new Grease Pencil is much faster
and better integrated with Blender

## [Volumes](Volumes.md)

OpenVDB files can now be imported and rendered with the new volume
object.

## [Virtual Reality](Virtual_Reality.md)

Initial native virtual reality, focused on scene inspection.

## [Cycles](Cycles.md)

Adaptive sampling, OptiX viewport denoising, improved shader nodes, and
more.

## [EEVEE](EEVEE.md)

New render passes, improved light cache and high-quality hair
transparency.

## [Sculpt](Sculpt.md)

Face sets to control visibility, cloth brush, and many more
improvements.

## [Modeling](Modeling.md)

## [Physics](Physics.md)

## [User Interface](User_Interface.md)

## [Animation & Rigging](Animation.md)

## [More Features](More_Features.md)

## [Python API](Python_API.md)

## [Add-ons](Add-ons.md)

Major updates to Collection Manager.
