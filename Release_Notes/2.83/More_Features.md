# Blender 2.83: More Features

## Undo

Performance of undo in Object and Pose mode was significantly improved,
particularly for scenes with many objects. Further optimizations are
planned for future releases.

This optimization is enabled by default. If there are stability issues,
it is possible to use the slower undo system by enabling Developer
Extras in the preferences and changing the option in the Experimental
section. See [the design task](https://developer.blender.org/T60695) for
more details.

## 3D Viewport

  - Improve selection with many small objects in the 3D viewport.
    ([3685347b41](https://projects.blender.org/blender/blender/commit/3685347b4172))
  - Revamped color management, with compositing now performed in linear
    space for overlays and grease pencil for more accurate results.

## Video Sequencer

  - Disk Cache to store cached frames on disk instead of only in memory.
    It can be enabled in Preferences \> System \> Video Sequencer.
    ([348d2fa09e](https://projects.blender.org/blender/blender/commit/348d2fa09e0c))
  - Revamped sequence strip drawing
    ([271231f58e](https://projects.blender.org/blender/blender/commit/271231f58ee3))
  - Show f-curves for opacity and volume values on the strips
    ([d0d20de183](https://projects.blender.org/blender/blender/commit/d0d20de183f1))
  - Adjust Last Operation panel
    ([a4cf2cf2de](https://projects.blender.org/blender/blender/commit/a4cf2cf2decc)).
  - Toolbar and color sample tool
    ([6a49161c8c](https://projects.blender.org/blender/blender/commit/6a49161c8c60))
    ([68ba6378b5](https://projects.blender.org/blender/blender/commit/68ba6378b5b1))
  - Option to select handles with box selection
    ([5314161491](https://projects.blender.org/blender/blender/commit/5314161491d4))

## Import & Export

### Universal Scene Description

  - Metaballs can now also be exported to USD
    ([4b2b5fe4b8](https://projects.blender.org/blender/blender/commit/4b2b5fe4b8d3b)).

### Alembic

  - Improved import and export of camera transformations. Children of
    cameras are now handled properly
    ([7c5a44c71f](https://projects.blender.org/blender/blender/commit/7c5a44c71f)).
  - Animated UV coordinates are now exported and imported correctly.
    ([65574463fa](https://projects.blender.org/blender/blender/commit/65574463fa9))
    ([94cbfb71b](https://projects.blender.org/blender/blender/commit/94cbfb71b))

### glTF

  - Exporter
      - Manage collection / collection instance / proxy
        ([rBA191bcee](https://projects.blender.org/blender/blender-addons/commit/191bcee579b3))
      - Improve texture export performance
        ([rBAdfadb30](https://projects.blender.org/blender/blender-addons/commit/dfadb306b0e9))
      - KHR\_materials\_clearcoat export
        ([rBA65bad42](https://projects.blender.org/blender/blender-addons/commit/65bad4212eb9))
      - Better image option
        ([rBA872e3e6](https://projects.blender.org/blender/blender-addons/commit/872e3e6afac9))
      - Don't take mute strip into account
        ([rBA2242495](https://projects.blender.org/blender/blender-addons/commit/22424950a3f6))
      - Fix bone cache issue
        ([rBAcc3e0cc](https://projects.blender.org/blender/blender-addons/commit/cc3e0cce4a53),
        [rBAffd8a68](https://projects.blender.org/blender/blender-addons/commit/ffd8a687247d))
      - Fix normals for skinned meshes
        ([rBAec5ca6a](https://projects.blender.org/blender/blender-addons/commit/ec5ca6a6cb54))
      - Manage user extension at glTF level
        ([rBA1ae12e6](https://projects.blender.org/blender/blender-addons/commit/1ae12e6f5d3c))
      - Animation general improvment
        ([rBAe5bde5e](https://projects.blender.org/blender/blender-addons/commit/e5bde5ec2e68),
        [rBAf3683cf](https://projects.blender.org/blender/blender-addons/commit/f3683cf7bf74),
        [rBA2425a37](https://projects.blender.org/blender/blender-addons/commit/2425a37ae57d),
        [rBA4ef5cd1](https://projects.blender.org/blender/blender-addons/commit/4ef5cd10f689),
        [rBA68d5831](https://projects.blender.org/blender/blender-addons/commit/68d5831729bf),
        [rBAb72a6c0](https://projects.blender.org/blender/blender-addons/commit/b72a6c057999))
      - Ability to undo an export
        ([rBAd52854d](https://projects.blender.org/blender/blender-addons/commit/d52854d420bd))
      - Export extra for bones
        ([rBA15d56ea](https://projects.blender.org/blender/blender-addons/commit/15d56ea627d9))
      - Don't combine image names if they're all from the same file
        ([rBA0a361e7](https://projects.blender.org/blender/blender-addons/commit/0a361e787dbd))
      - Normalized normals for skinned mesh
        ([rBAb42aecd](https://projects.blender.org/blender/blender-addons/commit/b42aecdca411))
      - Fix Vertex Group export in some cases
        ([rBAdc4c83c](https://projects.blender.org/blender/blender-addons/commit/dc4c83cffc6b))
      - fix bug when apply modifier + disable skinning
        ([rBA69d4dae](https://projects.blender.org/blender/blender-addons/commit/69d4daee6ffe))
      - Less naive file format detection for textures
        ([rBA7a3fdf0](https://projects.blender.org/blender/blender-addons/commit/7a3fdf08f3fe))
      - Fix export some animation interpolations
        ([rBA18a0f95](https://projects.blender.org/blender/blender-addons/commit/18a0f95a8482))
  - Importer
      - Fix skinning & bone hierarchy
        ([rBA75855d7](https://projects.blender.org/blender/blender-addons/commit/75855d723895),
        [rBA09bbef3](https://projects.blender.org/blender/blender-addons/commit/09bbef319f5e),
        [rBA39e2149](https://projects.blender.org/blender/blender-addons/commit/39e2149b6c62),
        [rBAe4269fc](https://projects.blender.org/blender/blender-addons/commit/e4269fc795cf))
      - Better bone rotation management & bind pose
        ([rBA6e7dfdd](https://projects.blender.org/blender/blender-addons/commit/6e7dfdd8a91f),
        [rBA96a4166](https://projects.blender.org/blender/blender-addons/commit/96a4166c8de2),
        [rBA38531bb](https://projects.blender.org/blender/blender-addons/commit/38531bbfa567))
      - Yfov near / far camera import
        ([rBAd8aa24a](https://projects.blender.org/blender/blender-addons/commit/d8aa24a9321e),
        [rBA43148f1](https://projects.blender.org/blender/blender-addons/commit/43148f17496c))
      - Rewrite material import
        ([rBA2caaf64](https://projects.blender.org/blender/blender-addons/commit/2caaf64ab10b))
      - Manage morph weights at node level
        ([rBAee61a3a](https://projects.blender.org/blender/blender-addons/commit/ee61a3a692d3))
      - Simplify/cleanup image import
        ([rBAe88c7ee](https://projects.blender.org/blender/blender-addons/commit/e88c7ee2a7c0))
      - Omit texture mapping & UVMap node when not needed
        ([rBAb629ab4](https://projects.blender.org/blender/blender-addons/commit/b629ab427c4e))
      - Use emission/alpha sockets on Principled node
        ([rBAb966913](https://projects.blender.org/blender/blender-addons/commit/b966913caab3))
      - Manage texture wrap modes
        ([rBAd0dee32](https://projects.blender.org/blender/blender-addons/commit/d0dee32d2b5f))
      - Better retrieve camera & lights names
        ([rBAfd6bfbb](https://projects.blender.org/blender/blender-addons/commit/fd6bfbb44af4))
      - Performance improvement importing meshes & normals
        ([rBAf5f442a](https://projects.blender.org/blender/blender-addons/commit/f5f442a7a665))
      - Rename option from export\_selected to use\_selection
        ([rBA120d313](https://projects.blender.org/blender/blender-addons/commit/120d313b842e),
        [rBAa35d66c](https://projects.blender.org/blender/blender-addons/commit/a35d66c1337a))
      - Bone & Animation code cleanup & checks
        ([rBAb4fc9fb](https://projects.blender.org/blender/blender-addons/commit/b4fc9fbcf4e2),
        [rBA9920e00](https://projects.blender.org/blender/blender-addons/commit/9920e00e5fbe),
        [rBA0e2a855](https://projects.blender.org/blender/blender-addons/commit/0e2a855c40c6),
        [rBA8db785e](https://projects.blender.org/blender/blender-addons/commit/8db785ea740b),
        [rBAf4e12a2](https://projects.blender.org/blender/blender-addons/commit/f4e12a20f5d0))
      - Add extension to tmp file generated when importing textures
        ([rBAea29654](https://projects.blender.org/blender/blender-addons/commit/ea2965438756))
      - Improve node names
        ([rBA02ca41d](https://projects.blender.org/blender/blender-addons/commit/02ca41d48ea3))
      - Use friendly filenames for temp image files
        ([rBA45afacf](https://projects.blender.org/blender/blender-addons/commit/45afacf7a630))
      - Better material names when using color vertex
        ([rBA0650f59](https://projects.blender.org/blender/blender-addons/commit/0650f599aa17))
      - Fix bug on windows, use absolute paty
        ([rBA520b6af](https://projects.blender.org/blender/blender-addons/commit/520b6af2be38))

## Miscellaneous

  - View Layers: when adding a new view layer, it is now possible to
    copy settings from currently active one, or to create an 'empty' one
    (where all collections are disabled)
    ([d1972e50cb](https://projects.blender.org/blender/blender/commit/d1972e50cbef)).
  - Windows: support for high resolution tablet pen events when using
    Windows Ink, so strokes more accurately match the pen motion. This
    was previously only supported on Linux.
    ([d571d61](https://projects.blender.org/blender/blender/commit/d571d61))
  - Freestyle: option to render freestyle to a separate pass.
