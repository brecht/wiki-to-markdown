# Blender 2.83: Add-ons

# Addons Updates

## Collection Manager

|                                                                                                                                                             |                                                                                              |
| ----------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------- |
| ![../../images/Collection\_Manager\_blender\_2.83.png](../../images/Collection_Manager_blender_2.83.png "../../images/Collection_Manager_blender_2.83.png") | ![../../images/Изображение.png](../../images/Изображение.png "../../images/Изображение.png") |

  - A New Quick Content Display (QCD) system was added in addition to
    the Collection Manager popup. This allows you to set up to 20
    collections as "slots" and then quickly and easily view them, and
    move objects to them, with GUI widgets or hotkeys.
    ([rBAb752de9](https://projects.blender.org/blender/blender-addons/commit/b752de9e))
  - Added preferences for QCD.
    ([rBAb752de9](https://projects.blender.org/blender/blender-addons/commit/b752de9e))
    ([rBAe61a7e8](https://projects.blender.org/blender/blender-addons/commit/e61a7e8b))
  - Added Scene Collection support. This allows you to set it as the
    active collection, move objects to it, and it encompasses the global
    Restriction Toggles which provide actions that affect all
    collections.
    ([rBAdccf56e](https://projects.blender.org/blender/blender-addons/commit/dccf56e0))
  - Added a Copy/Paste Restriction Toggle states function to all global
    Restriction Toggles.
    ([rBA57462fb](https://projects.blender.org/blender/blender-addons/commit/57462fb0))
  - Added a Swap Restriction Toggle states function to all global
    Restriction Toggles.
    ([rBAf149e0a](https://projects.blender.org/blender/blender-addons/commit/f149e0a4))
  - Added a Discard History function to all global and local Restriction
    Toggles, and Disclosure (small triangle icon) buttons.
    ([rBA61f1c0a](https://projects.blender.org/blender/blender-addons/commit/61f1c0ae))
  - Added an Isolate Expanded Tree function to the Disclosure
    buttons.([rBA1d722cb](https://projects.blender.org/blender/blender-addons/commit/1d722cb7))
  - Switched from syncing the active collection to the tree view row to
    setting the active collection by clicking on the collection icon.
    ([rBAfa71f70](https://projects.blender.org/blender/blender-addons/commit/fa71f709))
  - Improved the functionality of moving objects with the Collection
    Manager popup allowing objects to be moved to multiple excluded
    collections.
    ([rBA5c9aaca](https://projects.blender.org/blender/blender-addons/commit/5c9aaca6))
  - Added a Nested Isolation function to all local Restriction Toggles.
    ([rBA0599456](https://projects.blender.org/blender/blender-addons/commit/05994562))
  - Added more protections for the main Collection Manager popup's
    internal states.
    ([rBA507c04a](https://projects.blender.org/blender/blender-addons/commit/507c04aa))
    ([rBAcc1a2f5](https://projects.blender.org/blender/blender-addons/commit/cc1a2f5a))
  - Fixed not being able to move objects while in Local View.
    ([rBA5c9aaca](https://projects.blender.org/blender/blender-addons/commit/5c9aaca6))
