# Blender 2.83: EEVEE

## Render Passes

More render passes supported for compositing.
([be2bc97eba](https://projects.blender.org/blender/blender/commit/be2bc97eba))
([developer docs](Source/Render/EEVEE/RenderPasses))

  - Emission
  - Diffuse Light
  - Diffuse Color
  - Glossy Light
  - Glossy Color
  - Environment
  - Volume Scattering
  - Volume Transmission
  - Bloom
  - Shadow

## Light Cache Update

Reflection Probes are now stored in cubemap arrays instead of octahedron
maps arrays. This means pre-2.83 files will need to be rebaked and light
caches from 2.83 will not be loadable in previous blender versions.
([7dd0be9554](https://projects.blender.org/blender/blender/commit/7dd0be9554ae))

<center>

|                                                                                                              |                                                                                                           |
| ------------------------------------------------------------------------------------------------------------ | --------------------------------------------------------------------------------------------------------- |
| ![../../images/Cubearray\_before.png](../../images/Cubearray_before.png "../../images/Cubearray_before.png") | ![../../images/Cubearray\_after.png](../../images/Cubearray_after.png "../../images/Cubearray_after.png") |

style="caption-side: bottom" | Rough glossy surface before and after the
change

</center>

## Hair Transparency

Hair geometry now supports blending with alpha hash and alpha clip mode.
Shadow blend modes are now also supported.
([035a3760af](https://projects.blender.org/blender/blender/commit/035a3760afd2))

<center>

![../../images/Hair\_alpha.png](../../images/Hair_alpha.png
"../../images/Hair_alpha.png")

</center>

## Other Changes

  - A new \`High Quality Normals\` option has been added to the render
    settings to avoid low res normal issues on dense meshes.
    ([e82827bf6e](https://projects.blender.org/blender/blender/commit/e82827bf6ed5))
  - A new \`Half Float Precision\` option has been added to the Image
    datablock settings adding the possibility (when the option is turned
    off) to use 32 bits floating point precision when loading float
    textures.
    ([4e9fffc289](https://projects.blender.org/blender/blender/commit/4e9fffc28926))
  - Sun shadow bias have been fixed and now behave the same as point
    light shadow bias. User intervention may be required to fix certain
    scenes.
    ([31ad86884c](https://projects.blender.org/blender/blender/commit/31ad86884cb1))
  - Lookdev now have an option to display the world environment with
    variable amount of blurring.
    ([6e23433c1a](https://projects.blender.org/blender/blender/commit/6e23433c1a74))
