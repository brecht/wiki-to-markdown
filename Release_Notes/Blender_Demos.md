# Blender feature demo scene info

Information about Blender feature demo scene submissions for the
official Blender [Demo
Files](https://www.blender.org/download/demo-files/) page.

  
✍ Maintainer: [Metin Seven](../User/Metin_Seven/index.md)

📅 Updated: **April 29, 2021**

  

## Demo scene submission guidelines

#### Please follow these guidelines for Blender demo scene preparation

  

  - Include a Text Editor window with a text datablock named 'README'.

<!-- end list -->

  -   
    In the text please include:  
    ─ Some info / instructions regarding the scene, comprehensible for
    novice Blender users.  
    ─ The Creative Commons license type: CC-0 (preferred), CC-BY or
    CC-BY-ND.  
    ─ Your name or nickname.  
    ─ A URL (e.g. your portfolio or Twitter profile URL).

<!-- end list -->

  - Optionally, you may choose to add a specific info text to different
    workspaces, to explain more of your creative process (see example
    screenshots below). This is appreciated but not required.

<!-- end list -->

  - Check if the scene doesn't include settings that may confuse novice
    users, such as a locked camera. If such a setting is essential to
    the demo's functioning, please include a notice in the 'README' info
    text.

<!-- end list -->

  - Check if the scene does not only render correctly in the viewport,
    but also when pressing F12.

<!-- end list -->

  - Don't include self-promotion / signatures / logos as a part of the
    demo scene. You can include a credit in the 'README' info text.

<!-- end list -->

  - Make sure no copyrighted models, textures or other assets are
    present in the scene.

<!-- end list -->

  - If uncopyrighted assets (image textures, HDRI environments, etc.)
    are part of the scene, make sure they are packed into the Blend
    file, using File menu ➔ External Data...

<!-- end list -->

  - Save the scene from the most recent appropriate Blender version to
    ensure compatibility with the final release version.

<!-- end list -->

  - To minimize the file size, activate the Compress option in the File
    ➔ Save As... file dialog options panel when saving the Blend file.

<!-- end list -->

  - In case of multiple Blend files showcasing related features, please
    consider merging the demos into one Blend file containing multiple
    Scenes, naming the Scenes appropriately and mentioning the presence
    of multiple Scenes in the 'README' info text.

<!-- end list -->

  - Please provide a publicly accessible download link (e.g. a Google
    Drive link) to the finished scene, that will remain intact until at
    least the next Blender release. The scene will be uploaded to the
    Blender.org server, so the link does not have to remain intact after
    the Blender release.

  
![../images/Blender\_demo\_scene\_example\_screenshots.png](../images/Blender_demo_scene_example_screenshots.png
"../images/Blender_demo_scene_example_screenshots.png") *Blender demo
scene example screenshots, taken from the [Splash
Fox](https://cloud.blender.org/p/gallery/5f4cd4441b96699a87f70fcb) scene
by Daniel Bystedt.*

  

## Demo scene publication

After verification and adjustments (if necessary), the scenes are added
to the Blender [Demo
Files](https://www.blender.org/download/demo-files/) page in a
deactivated state (visible in the CMS, invisible in the Demo Files
page), and are activated around the release date of the new Blender
version.

📝 Note to Blender.org maintainers: please verify the new demo files in
the release (candidate) version, to avoid possible issues because of
changes in Blender during the alpha development stage.
