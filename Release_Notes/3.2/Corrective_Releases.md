# Blender 3.2.2

Released on August 3rd 2022, Blender 3.2.2 features the following bug
fixes:

  - Align Active Camera to Selected fails with ortho camera
    [\#99653](http://developer.blender.org/T99653)
  - Annotation lines doesn't start where clicked
    [\#99368](http://developer.blender.org/T99368)
  - Bones using empties as custom shapes can't be selected
    [\#99270](http://developer.blender.org/T99270)
  - Crash applying non-existent modifiers
    [\#99678](http://developer.blender.org/T99678)
  - Crash when render finishes
    [\#100049](http://developer.blender.org/T100049)
  - Crash when transform applied on multi-user image
    [\#100040](http://developer.blender.org/T100040)
  - Eternal loop reading blend file thumbnail
    [\#99711](http://developer.blender.org/T99711)
  - Fix crash loading factory settings in image paint mode
    [rB2b83f3d5](https://projects.blender.org/blender/blender/commit/2b83f3d52163)
  - Fix integer overflow in thumbnail extractor
    [\#99705](http://developer.blender.org/T99705)
  - Fix Python SystemExit exceptions silently exiting
    [rB5f7c677a](https://projects.blender.org/blender/blender/commit/5f7c677ac968)
  - Fix use-after-free error when handling events that close windows
    [rB3d6f6715](https://projects.blender.org/blender/blender/commit/3d6f6715595e)
  - Fix: Incorrect coordinates used in BLI\_rct\*\_isect\_segment
    functions
    [rBc07e9e08](https://projects.blender.org/blender/blender/commit/c07e9e0828d1)
  - Fix: Move DRW\_shgroup\_add\_material\_resources(grp, mat) to after
    the null-check for grp.
    [\#99646](http://developer.blender.org/T99646)
  - Gpencil Flip strokes did not support multiframe edit
    [\#99702](http://developer.blender.org/T99702)
  - GPencil multiframe falloff is scaling wrongly in rotation
    [\#99342](http://developer.blender.org/T99342)
  - GPencil strokes cannot be edited after set origin
    [\#99979](http://developer.blender.org/T99979)
  - Light group passes do not work when shadow catcher is used
    [\#98367](http://developer.blender.org/T98367)
  - Make Principled Hair IOR input behave like other IOR sliders
    [\#99785](http://developer.blender.org/T99785)
  - OBJ (new importer): does not import vertices that aren't part of any
    face [\#100017](http://developer.blender.org/T100017)
  - OBJ (new importer): fails to import faces in some cases
    [\#99532](http://developer.blender.org/T99532)
  - OBJ (new importer): fails with trailing space after wrapped lines
    [\#99536](http://developer.blender.org/T99536)
  - NULL pointer free with corrupt zSTD reading
    [\#99744](http://developer.blender.org/T99744)
  - Python: restrict name-space access for restricted evaluation
    [rBae459317](https://projects.blender.org/blender/blender/commit/ae4593179745)
  - Sculpt mode missing check for hidden active object
    [\#94633](http://developer.blender.org/T94633)
  - sculpt\_update\_object calls paint updates for nonpaint tools
    [\#99196](http://developer.blender.org/T99196)
  - Unable to select bones when custom shape display is disabled
    [\#99364](http://developer.blender.org/T99364)

# Blender 3.2.1

Released on July 6th 2022, Blender 3.2.1 features the following bug
fixes:

  - Anchored mode not working for sculpt smear brush.
    [\#98745](http://developer.blender.org/T98745)
  - Assets dropped upside down when looking through camera.
    [\#96776](http://developer.blender.org/T96776)
  - Avoid console warnings when no Sculpt brush selected.
    [\#98904](http://developer.blender.org/T98904)
  - Avoid unnecessary mesh copy.
    [\#98796](http://developer.blender.org/T98796)
  - Baking to active color attribute uses wrong layer.
    [\#98960](http://developer.blender.org/T98960)
  - Boolean modifier creates invalid material indices.
    [\#99191](http://developer.blender.org/T99191)
  - Broken vertex paint mode operators.
    [\#98975](http://developer.blender.org/T98975)
  - Color attribute fill API didn't support editmode.
    [\#98673](http://developer.blender.org/T98673)
  - Console warning using search (F3) in grease pencil draw mode.
    [\#99178](http://developer.blender.org/T99178)
  - Crash after running view\_all operator in VSE.
    [\#99110](http://developer.blender.org/T99110)
  - Crash deleting file output node with color management override.
    [\#99028](http://developer.blender.org/T99028)
  - Crash drag-dropping collection from outliner to ID property.
    [\#98715](http://developer.blender.org/T98715)
  - Crash in Volume to Mesh with 0 voxels.
    [\#94969](http://developer.blender.org/T94969)
  - Crash on startup - OpenGL4.2 without conservative depth.
    [\#98708](http://developer.blender.org/T98708)
  - Crash removing some builtin attributes.
    [\#98956](http://developer.blender.org/T98956)
  - Crash when dragging file to VSE from file browser.
    [\#99266](http://developer.blender.org/T99266)
  - Crash when moving grease pencil object has any invisible….
    [\#98853](http://developer.blender.org/T98853)
  - Crash when recursively nesting NLA meta strips.
    [\#98700](http://developer.blender.org/T98700)
  - Crash with GPU subdivision in edit mode and instanced geometry.
    [\#98813](http://developer.blender.org/T98813)
  - Crash: Curve to Mesh node crashes blender if there is a single
    vertice curve. [\#98917](http://developer.blender.org/T98917)
  - Crash: GPU subdivision crash in edit mode with loose geometry.
    [\#98866](http://developer.blender.org/T98866)
  - Crash: Grease Pencil: Fix crash when using time offset modifier.
    [rB54cfeacf](https://projects.blender.org/blender/blender/commit/54cfeacf4b7b)
  - Curve Pen NURBS extrusion creates duplicates.
    [\#98624](http://developer.blender.org/T98624)
  - Division by zero in smear code when strength is zero.
    [\#98698](http://developer.blender.org/T98698)
  - Dynamic Paint does not update normals.
    [\#98727](http://developer.blender.org/T98727)
  - Editor panels are broken.
    [\#98925](http://developer.blender.org/T98925)
  - EEVEE Bloom Pass Outputs Final Image Instead of Bloom.
    [\#98972](http://developer.blender.org/T98972)
  - EEVEE compilation error cryptomatte shaders.
    [\#98663](http://developer.blender.org/T98663)
  - EEVEE unlinked aov output nodes don't render.
    [\#98919](http://developer.blender.org/T98919)
  - EEVEE: Buffer overflow in sample name buffer.
    [\#98825](http://developer.blender.org/T98825)
  - EEVEE: Crash when using Light Output in Materials.
    [\#99104](http://developer.blender.org/T99104)
  - EEVEE: Missing custom property from volumetrics.
    [\#98697](http://developer.blender.org/T98697)
  - EEVEE: Pixelated Environment Texture.
    [\#99128](http://developer.blender.org/T99128)
  - EEVEE: Specular BSDF apply specular color input twice.
    [\#99018](http://developer.blender.org/T99018)
  - EEVEE: Specular BSDF does not apply occlusion.
    [\#99019](http://developer.blender.org/T99019)
  - EEVEE: World volume shader incorrect texture coords.
    [\#99138](http://developer.blender.org/T99138)
  - Face dot colors in UV editor was using wrong color from theme.
    [\#98699](http://developer.blender.org/T98699)
  - Face Is Planar Node Not handling Certain Conditions.
    [\#98718](http://developer.blender.org/T98718)
  - Fix armatures not visible in VR.
    [rBa7c923c6](https://projects.blender.org/blender/blender/commit/a7c923c6d556)
  - Fix artefacts with GPU subdivision and weight paint face selection.
    [rB85db51cd](https://projects.blender.org/blender/blender/commit/85db51cd21ca)
  - Fix color attribute interpolation with GPU subdivision.
    [rBc1454419](https://projects.blender.org/blender/blender/commit/c14544198109)
  - Fix edge case crashes in gpu subdiv cache code
    [\#98884](http://developer.blender.org/T98884)
  - Fix uninitialized memory use in key-down events on window
    activation.
    [rB45763272](https://projects.blender.org/blender/blender/commit/45763272c55a)
  - Freeze when changing strip source with thumbnails enabled.
    [\#99091](http://developer.blender.org/T99091)
  - Geometry nodes ignore if subdivision surface modifier is disabled.
    [\#99058](http://developer.blender.org/T99058)
  - glTF exporter: export driven SK when mesh parented to bone.
    [\#98912](http://developer.blender.org/T98912)
  - glTF exporter: Fix camera & light export when Yup is off
    [\#99306](http://developer.blender.org/T99306)
  - glTF exporter: Make sure to not modify virtual tree
    [rBA9b0f1db](https://projects.blender.org/blender/blender-addons/commit/9b0f1dbde817)
  - GPU Subdivision artifacts in weight paint with smooth shading.
    [\#99016](http://developer.blender.org/T99016)
  - GPU Subdivision displays normals for all elements.
    [\#98735](http://developer.blender.org/T98735)
  - GPU Subdivision: "Show Wire" overlay glitch.
    [\#98913](http://developer.blender.org/T98913)
  - Grease Pencil: Gradient colors in a Grease Pencil material change
    depending on the visibility of other objects.
    [\#98882](http://developer.blender.org/T98882)
  - Grease Pencil: sculpt brushes break after you delete a brush.
    [\#98904](http://developer.blender.org/T98904)
  - Grease Pencil: Sculpt Grab/Push don't work with one point.
    [\#99248](http://developer.blender.org/T99248)
  - Grease Pencil: Set Vertex Color Attribute does not color unpainted.
    [\#98756](http://developer.blender.org/T98756)
  - Install\_deps: Fix several issues with TBB.
    [rB6b15369e](https://projects.blender.org/blender/blender/commit/6b15369e243c)
  - Issue with subdivision subsurface modifier and \>= 8 levels.
    [\#98693](http://developer.blender.org/T98693)
  - Missing null check in versioning code.
    [\#98847](http://developer.blender.org/T98847)
  - OBJ (new importer): Fixed scene stats info not updated after import.
    [\#98293](http://developer.blender.org/T98293)
  - OBJ (new importer): Fixed wrong sharp edges in some cases.
    [\#97820](http://developer.blender.org/T97820)
  - OBJ (new importer): Got an option to import vertex groups.
    [\#98874](http://developer.blender.org/T98874)
  - OBJ (new importer): Ignore face normal indices if no normals are
    present. [\#98782](http://developer.blender.org/T98782)
  - OBJ (new importer): Use filename as the default object name.
    [rB581604d1](https://projects.blender.org/blender/blender/commit/581604d17ab7)
  - Outliner Unlink material in Blender File mode crashes.
    [\#98753](http://developer.blender.org/T98753)
  - PBVH\_GRIDS ignores face smooth flag on first gpu build.
    [\#98886](http://developer.blender.org/T98886)
  - PyAPI: Expose event.type\_prev, value\_prev.
    [\#99102](http://developer.blender.org/T99102)
  - Remesh modifier frees sculpt masking attributes.
    [\#99209](http://developer.blender.org/T99209)
  - Remove unused BRUSH\_PAINT icon definition.
    [\#98565](http://developer.blender.org/T98565)
  - Sculpt: Fix backwards normals in PBVH\_GRIDS raycasting.
    [rBe0dd51fc](https://projects.blender.org/blender/blender/commit/e0dd51fc1476)
  - Touch typing in text fields results in dropped key presses.
    [\#99027](http://developer.blender.org/T99027)
  - Unable to select mask points in clip editor.
    [\#98765](http://developer.blender.org/T98765)
  - Video sequencer screen corruption occurs when resizing.
    [\#98620](http://developer.blender.org/T98620)
  - Wrong anchored mode test for smear brush.
    [\#99231](http://developer.blender.org/T99231)
