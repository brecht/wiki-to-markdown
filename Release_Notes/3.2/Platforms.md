## Windows

  - Improved error message when monitors are connected to multiple
    adapters.
    ([fd1078e105](https://projects.blender.org/blender/blender/commit/fd1078e1055e)).
  - Faster blender launch when there are disconnected network shares.
    ([84fde382e4](https://projects.blender.org/blender/blender/commit/84fde382e43c)).
