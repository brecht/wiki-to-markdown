# Add-ons

## Rigify

  - Adjusted the switchable parent rig so that disabling Local Location
    on IK controls aligns location channels to world as intended. Added
    an IK Local Location option to limbs (on by default for backward
    compatibility) and disabled it in metarigs.
    ([rBA7120e9c](https://projects.blender.org/blender/blender-addons/commit/7120e9c9e087),
    [rBAc268b58](https://projects.blender.org/blender/blender-addons/commit/c268b58c247))

## FBX I/O

  - Support for camera DoF focus distance was added, including
    animation, for both import
    ([rBA5eff3aa](https://projects.blender.org/blender/blender-addons/commit/5eff3aa881bf))
    and export
    ([rBAfba4f07](https://projects.blender.org/blender/blender-addons/commit/fba4f07bc695)).
  - Imported animations of rigs now have their FCurves properly grouped
    by bones
    ([rBAa2c3cfd](https://projects.blender.org/blender/blender-addons/commit/a2c3cfdd764b)).
  - An option to triangulate geometry has been added to the exporter
    ([rBAd700a68](https://projects.blender.org/blender/blender-addons/commit/d700a6888ecd)).
  - An option to only export visible objects has been added to the
    exporter
    ([rBA4075cdb](https://projects.blender.org/blender/blender-addons/commit/4075cdbdc751)).

## BHV I/O

  - Imported animations of rigs now have their FCurves properly grouped
    by bones
    ([rBAa2c3cfd](https://projects.blender.org/blender/blender-addons/commit/a2c3cfdd764b)).

## glTF 2.0 I/O

### Exporter

  - Huge refactoring (better hierarchy / collection / instances
    management)
    ([rBA782f858](https://projects.blender.org/blender/blender-addons/commit/782f8585f4cc),
    [rBA1d5c8b5](https://projects.blender.org/blender/blender-addons/commit/1d5c8b54ee99),
    [rBA64f4623](https://projects.blender.org/blender/blender-addons/commit/64f462358522),
    [rBAdbb9b62](https://projects.blender.org/blender/blender-addons/commit/dbb9b62991b3))
  - Use rest pose for armature when 'use current frame' is off
    ([rBAd825811](https://projects.blender.org/blender/blender-addons/commit/d82581163628))
  - Add glTF settings node in shader menu
    ([rBA5838e26](https://projects.blender.org/blender/blender-addons/commit/5838e260366a))
  - Export armature without skined mesh as skin in glTF files
    ([rBA5a557e7](https://projects.blender.org/blender/blender-addons/commit/5a557e72fc1e))
  - Normalize skin weights also for all influence mode
    ([rBA96eb146](https://projects.blender.org/blender/blender-addons/commit/96eb14698456))
  - Manage active UVMap correclty, when there is no UVMap used in node
    tree
    ([rBA3a29996](https://projects.blender.org/blender/blender-addons/commit/3a299965833c))
  - Manage skinning when some vertices are not weights at all
    ([rBA564fdcd](https://projects.blender.org/blender/blender-addons/commit/564fdcdf7159))
  - Armature exports all actions
    ([rBA9818afc](https://projects.blender.org/blender/blender-addons/commit/9818afc829a0),
    [rBA787ea78](https://projects.blender.org/blender/blender-addons/commit/787ea78f7fa6),
    [rBAa0ce684](https://projects.blender.org/blender/blender-addons/commit/a0ce684afe3e))
  - Add various hooks
    ([rBA43477e0](https://projects.blender.org/blender/blender-addons/commit/43477e00ce44),
    [rBA8653db3](https://projects.blender.org/blender/blender-addons/commit/8653db380888),
    [rBAfef8f1f](https://projects.blender.org/blender/blender-addons/commit/fef8f1ffdb1f))
  - Remove back compatibility of export\_selection -\> use\_selection
    ([rBA4d8b2dc](https://projects.blender.org/blender/blender-addons/commit/4d8b2dc95f98))
  - Weight min threshold for skinning
    ([rBAc157125](https://projects.blender.org/blender/blender-addons/commit/c157125ace85))
  - Option to optimize animation size off by default
    ([rBA22c1eb5](https://projects.blender.org/blender/blender-addons/commit/22c1eb5c3d50))
  - Manage use\_nla option to avoid exporting merged animations
    ([rBA6e430d2](https://projects.blender.org/blender/blender-addons/commit/6e430d231788))
  - Performance : better way to detect weird images
    ([rBAcbb4b8c](https://projects.blender.org/blender/blender-addons/commit/cbb4b8c121a5))
  - Fix PBR values when emission node is used
    ([rBAfbb5111](https://projects.blender.org/blender/blender-addons/commit/fbb5111e1551))
  - Fix transmission export
    ([rBA4c1df0f](https://projects.blender.org/blender/blender-addons/commit/4c1df0f54ce0))
  - Fix bone export when Yup is off
    ([rBAdfe74f0](https://projects.blender.org/blender/blender-addons/commit/dfe74f0a1b1b))
  - Fix active scene export + option to use only active scene
    ([rBA1bffd88](https://projects.blender.org/blender/blender-addons/commit/1bffd880b8f0))
  - No texture export when original can't be retrieved
    ([rBAadf10c6](https://projects.blender.org/blender/blender-addons/commit/adf10c631570))
  - Use color attribute API instead of vertex color
    ([rBA485b4ac](https://projects.blender.org/blender/blender-addons/commit/485b4ac5691c))

### Importer

  - Fix when glTF file has no scene
    ([rBAb294d4c](https://projects.blender.org/blender/blender-addons/commit/b294d4c76654))
  - More animation hooks for user extensions
    ([rBA2183b1d](https://projects.blender.org/blender/blender-addons/commit/2183b1d29571))

## Dynamic Brush Menus

  - Fix and consistently support symmetry menus for all painting modes
    ([rBAc9fba91](https://projects.blender.org/blender/blender-addons/commit/c9fba919c2e1)).
  - Fix brushes menu not displaying fully in sculpt mode
    ([rBA4d53ec7](https://projects.blender.org/blender/blender-addons/commit/4d53ec76a3d8)).

## Collection Manager

  - An option to enable/disable the QCD 3D Viewport header widget has
    been added to the preferences
    ([rBAa65df67](https://projects.blender.org/blender/blender-addons/commit/a65df677f707)).

## Node Wrangler

  - Node Wrangler now handles attributes
    ([rBAdde5915](https://projects.blender.org/blender/blender-addons/commit/dde5915336d503e12b43af6611c6947767d3e355)).
