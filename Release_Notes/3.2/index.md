# Blender 3.2 Release Notes

Blender 3.2 was released on June 8, 2022.

Check out the final [release notes on
blender.org](https://www.blender.org/download/releases/3-2/).

## [Animation & Rigging](Animation_Rigging.md)

## [Core](Core.md)

## [EEVEE & Viewport](EEVEE.md)

## [Grease Pencil](Grease_Pencil.md)

## [Modeling](Modeling.md)

## [Nodes & Physics](Nodes_Physics.md)

## [Pipeline, Assets & I/O](Pipeline_Assets_IO.md)

## [Platform-Specific Changes](Platforms.md)

## [Python API & Text Editor](Python_API.md)

## [Render & Cycles](Cycles.md)

## [Sculpt, Paint, Texture](Sculpt.md)

## [User Interface](User_Interface.md)

## [VFX & Video](VFX.md)

## [Virtual Reality](Virtual_Reality.md)

## [Add-ons](Add-ons.md)

## [More Features](Reference/Release_Notes/3.2/More_Features)

## Compatibility

### Proxy Removal

The proxy system has been fully removed, see [the Core release
notes](Core.md) for details.

### Legacy Geometry Nodes

Geometry nodes from the pre-3.0 named attribute system have been
removed. See[ the geometry nodes release
notes](Nodes_Physics.md) for
details.

## [Corrective Releases](Corrective_Releases.md)
