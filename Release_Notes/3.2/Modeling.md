# Modeling

### Curve Pen Tool

  - A new "Curve Pen" tool in curve edit mode can be used to rapidly
    edit and create curves
    ([336082acba](https://projects.blender.org/blender/blender/commit/336082acba51)).
      - The tool supports quickly adding, deleting, and tweaking control
        points.
      - Much more functionality is available with various shortcuts, and
        editable in the keymap for further customization.

### Multi-User Data Object

  - When trying to apply a modifier to a multi-user data object users
    can choose to make it single user, instead of failing to apply
    ([35f34a3cf8](https://projects.blender.org/blender/blender/commit/35f34a3cf840)).
  - For applying object transform the new option allows to isolate the
    selected multi-user data objects from the others if needed
    ([8621fdb10d](https://projects.blender.org/blender/blender/commit/8621fdb10dc4)).

![../../videos/Apply-transforms.webm](../../videos/Apply-transforms.webm
"../../videos/Apply-transforms.webm")

### General

  - The "Select Similar" operator for Meshes now supports selecting by
    similar vertex crease values
    ([rB8abd8865](https://projects.blender.org/blender/blender/commit/8abd8865d2e4743035eedad21a72c92d70474907)).
  - NURBS curves knots generation has been improved
    ([0602852860](https://projects.blender.org/blender/blender/commit/0602852860dda7)).
      - "Cyclic" can be used with other knot options.
      - "Endpoint" and "Bezier" can be used at the same time.

![A scheme showing NURBS with all built-in knots generation modes. On
the sides, knot structures are visualized. Diagonal lines show by what
knots particular control point is affected. Blue and cyan arrows show to
which control points the curve is clamped and which knots create that
clamp. Purple line demonstrates where cycle in knot
begins.](../../images/NURBS_Knots_Generation_3.2.png
"A scheme showing NURBS with all built-in knots generation modes. On the sides, knot structures are visualized. Diagonal lines show by what knots particular control point is affected. Blue and cyan arrows show to which control points the curve is clamped and which knots create that clamp. Purple line demonstrates where cycle in knot begins.")

### Modifiers

  - The Vertex Weight Mix modifier now supports Minimum and Maximum as
    operations that can be applied to combine input weights
    ([fa715a158a](https://projects.blender.org/blender/blender/commit/fa715a158a4c4)).
