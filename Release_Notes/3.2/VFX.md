# VFX & Video

## Sequencer

  - Add filter method to strip transform
    ([1c5f2e49b7](https://projects.blender.org/blender/blender/commit/1c5f2e49b7bf))
  - Use float for transformation offset
    ([b4700a13c6](https://projects.blender.org/blender/blender/commit/b4700a13c6ab))
  - Now it is possible to create a new scene base on the active strip.
    The new scene is assigned to the active strip.
    ([2d24ba0210](https://projects.blender.org/blender/blender/commit/2d24ba0210e3))
  - Add channel headers - provides ability to assign label and per
    channel mute / lock buttons.
    ([277fa2f441](https://projects.blender.org/blender/blender/commit/277fa2f441f4))
  - Enable edge panning for transform operator - when dragging strip out
    of view, it pans the view.
    ([e49fef45ce](https://projects.blender.org/blender/blender/commit/e49fef45cef7))
  - Better handling of animation when transforming strips, transforming
    no longer extends selection.
    ([32da64c17e](https://projects.blender.org/blender/blender/commit/32da64c17e9d))
  - Add frame selected operator for preview.
    ([e16ff4132e](https://projects.blender.org/blender/blender/commit/e16ff4132e35))
  - Thumbnail for first image now represents handle position
    ([29b9187b32](https://projects.blender.org/blender/blender/commit/29b9187b327c))
  - Add option to limit timeline view height
    ([17769489d9](https://projects.blender.org/blender/blender/commit/17769489d920))

## Compositor

  - Add Combine and Separate XYZ Nodes
    ([9cc4861e6f](https://projects.blender.org/blender/blender/commit/9cc4861e6f7))

## Clip Editor

  - Fix tilt/scale slider of marker jumping on initial mouse grab
    ([dd7250efb1](https://projects.blender.org/blender/blender/commit/dd7250efb1a))
  - Fix for tilt/scale slider to follow mouse exactly
    ([00018bfaa2](https://projects.blender.org/blender/blender/commit/00018bfaa2a))

## Motion Tracking

  - Fixes very slow tracking with keyframe pattern matching when movie
    does not fit into cache
    ([e08180fdab](https://projects.blender.org/blender/blender/commit/e08180fdab8))
