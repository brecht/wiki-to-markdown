# User Interface

## General

  - The "Use Snap" option is no longer shared between different types of
    editors (3D View, UV, Sequencer and Node)
    ([rBe3de755a](https://projects.blender.org/blender/blender/commit/e3de755ae315))
  - Drag and Drop
      - Dragging preview thumbnails from the File Browser and Asset
        Browser doesn't require clicking exactly on the preview image
        anymore. (Almost) the entire tile can be used to start dragging
        now.
        ([6e487228a5](https://projects.blender.org/blender/blender/commit/6e487228a595))
      - It is now possible to drag materials (from Asset Browser for
        example) to Material Properties slots.
        ([fd2519e0b6](https://projects.blender.org/blender/blender/commit/fd2519e0b694))
  - File Browser search now works correctly with descriptive font names.
    ([f381c73a21](https://projects.blender.org/blender/blender/commit/f381c73a21d3))
  - Editor Management
      - Duplicated windows using corner Actions Zones are now
        immediately activated
        ([db4d5d1583](https://projects.blender.org/blender/blender/commit/db4d5d15833d)).
      - The "Swap Areas" operator (when used from corner action zones)
        can work between separate Windows
        ([29696fb725](https://projects.blender.org/blender/blender/commit/29696fb7252b)).
  - Windows OS
      - Proper handling of Full Width numbers while in Chinese IME
        character entry.
        ([82c852f387](https://projects.blender.org/blender/blender/commit/82c852f38732))
      - OneDrive link added to File Browser "System" list.
        ([ace1b6a73a](https://projects.blender.org/blender/blender/commit/ace1b6a73a0b)).
      - Improved numerical entry when using Chinese language.
        ([6b7756279f](https://projects.blender.org/blender/blender/commit/6b7756279f71),
        [4311a32bc2](https://projects.blender.org/blender/blender/commit/4311a32bc2d9)).
  - Support for font file formats "Woff" and "Woff2".
    ([55c90df316](https://projects.blender.org/blender/blender/commit/55c90df316c7)).
  - Adjustments to the Quick Setup screen to fit contents better,
    especially with High DPI monitors.
    ([7aec5b0622](https://projects.blender.org/blender/blender/commit/7aec5b062275)).

## 3D Viewport

  - Clicking in object mode won't cycle selection on first click.
    ([rBb1908f2e](https://projects.blender.org/blender/blender/commit/b1908f2e0b23988627772f6a6d968d8351dca6d7))
  - Support maintaining orthographic views when rolling 90/-90 degrees
    ([13efaa8a09](https://projects.blender.org/blender/blender/commit/13efaa8a09ab805c81164bc04a7ac4cc2c40bd1c)).
  - 3D Mouse / NDOF
      - 90/-90 degree rotation is used for roll buttons
        ([13efaa8a09](https://projects.blender.org/blender/blender/commit/13efaa8a09ab805c81164bc04a7ac4cc2c40bd1c)).
      - N2D style pan & zoom is not optionally supported in the camera
        view instead of leaving the camera view.
        ([51975b89ed](https://projects.blender.org/blender/blender/commit/51975b89edfcc02131f1f8248e1b3442ea2778fa),
        [391c3848b1](https://projects.blender.org/blender/blender/commit/391c3848b1326db1c29fc5c5f791d732d7d282a3)).
  - Correction to font size of Side Bar tab text
    ([b959f603da](https://projects.blender.org/blender/blender/commit/b959f603da45)).

## Outliner

  - Objects that use the "Curve" data type in the Outliner will be
    displayed using their respective sub-type icon (curve, surface or
    font icon).
    ([1e1d1f15e8](https://projects.blender.org/blender/blender/commit/1e1d1f15e875))
