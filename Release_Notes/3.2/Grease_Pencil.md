# Grease Pencil

## Operators

  - Improved smooth algorithm.
    ([d4e1458db3](https://projects.blender.org/blender/blender/commit/d4e1458db3a0))

## UI

  - Scale Stroke Thickness is now visible in Pie menu.
    ([acd7a648b1](https://projects.blender.org/blender/blender/commit/acd7a648b17a))

![Pie Menu](../../images/Pie_Menu_1.png "Pie Menu")

## Modifiers and VFX

  - New modifier: Envelope
    ([cee6af0056](https://projects.blender.org/blender/blender/commit/cee6af00567f))

![../../images/Envelope\_Modifier.png](../../images/Envelope_Modifier.png
"../../images/Envelope_Modifier.png")

  - Dot Dash modifier:
      - Lowered the bounds limits for dash and gap
        ([8d4244691a](https://projects.blender.org/blender/blender/commit/8d4244691a2d))

![../../images/Dot\_dash.png](../../images/Dot_dash.png
"../../images/Dot_dash.png")

  -   - New option to use Cyclic per segment
        ([97f2210157](https://projects.blender.org/blender/blender/commit/97f2210157db))

![Dot dash Cyclic](../../images/Dot_dash_04.png "Dot dash Cyclic")

  - Smooth Modifier: New option to keep the overall shape closer to the
    original, avoiding the strokes to shrink
    ([d4e1458db3](https://projects.blender.org/blender/blender/commit/d4e1458db3a0))

![../../images/Smooth\_Modifier.png](../../images/Smooth_Modifier.png
"../../images/Smooth_Modifier.png")

  - Build modifier:
      - New Additive mode.
        ([e74838d0d0](https://projects.blender.org/blender/blender/commit/e74838d0d011))
      - Adds fade support
        ([c4e4924096](https://projects.blender.org/blender/blender/commit/c4e4924096d1))
      - Adds origin object for the building process
        ([c4e4924096](https://projects.blender.org/blender/blender/commit/c4e4924096d1))

![Build Modifier](../../images/Build_Modifier.png "Build Modifier")

## Performance

  - Added a cache to speed up Grease Pencil object evaluation. This is
    currently used only for drawing strokes and sculpting using some
    brushes like the push brush.
    ([e2befa425a](https://projects.blender.org/blender/blender/commit/e2befa425a84))
  - Improve multi-user Grease Pencil data performance with modifiers.
    ([7ca13eef7c](https://projects.blender.org/blender/blender/commit/7ca13eef7c33))

## Bug Fix

  - Grease Pencil edit mode overlays now support the clipping region.
    ([69a43069e8](https://projects.blender.org/blender/blender/commit/69a43069e857))
