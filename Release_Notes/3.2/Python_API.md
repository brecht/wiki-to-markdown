# Python API

## Text Editor

  - Python exceptions from the text editor are now displayed in the
    info-space (as well as the console).
    ([rB2d2baeaf](https://projects.blender.org/blender/blender/commit/2d2baeaf04d481f284bc2f098fb6d7ee9268151f)).
  - Soft keywords are now syntax highlighted.
    ([c6ed879f9a](https://projects.blender.org/blender/blender/commit/c6ed879f9aa7cd863f146cc7104f16cfe8a73e8a))

## Additions

  - [\`Context.temp\_override\`](https://docs.blender.org/api/3.2/bpy.types.Context.html#bpy.types.Context.temp_override)
    has been added to support temporarily setting the context which can
    be useful after loading a file (for example) where the context is
    cleared making many operators fail to run.
    ([f438344cf2](https://projects.blender.org/blender/blender/commit/f438344cf243632e497772cf1f855b9c8856fd37))
  - \`Context.path\_resolve\` now supports resolving attributes such as
    "active\_object", "scene" ... etc. Expressions such as
    \`context.path\_resolve("active\_object.modifiers\[0\].name")\` now
    work as expected.
    ([7bb8eeb3a8](https://projects.blender.org/blender/blender/commit/7bb8eeb3a871eb1e72ee129f8ff441f2752b37be))
  - \`Text.region\_from\_string\` & \`Text.region\_as\_string\` methods
    can be used to get and set the selection, optionally passing in the
    region to access.
    ([rBf49a736f](https://projects.blender.org/blender/blender/commit/f49a736ff4023231483c7e535ca2a7f2869d641d)).
  - \`mathutils.Color\` gained methods to convert between the scene
    linear color space and sRGB, CIE XYZ, ACES and Linear Rec.709. Most
    values returned by Blender APIs are in scene linear color space,
    with the notable exception of user interface theming colors which
    are in sRGB.
    ([469ee7f](https://projects.blender.org/blender/blender/commit/469ee7f))
  - Persistent handlers in \`bpy.app.handlers\` can now use methods from
    a class.
    ([0f583d9d60](https://projects.blender.org/blender/blender/commit/0f583d9d604f45606e238f88eb2300eb75e492bb))

## Breaking Changes

  - \`XrActionMapItem.user\_paths\` collection property replaces
    \`XrActionMapItem.user\_path0/user\_path1\`. Similarly,
    \`XrActionMapBinding.component\_paths\` collection property replaces
    \`XrActionMapBinding.component\_path0/component\_path1\`. This
    allows XR actions to have a variable amount of bindings instead of
    being limited to 2 per action.
    ([rB6a8709ba](https://projects.blender.org/blender/blender/commit/6a8709ba136e))
  - \`gpu.shader.code\_from\_builtin\` it is no longer compatible with
    Blender's internal gpu module and had to be removed.
    ([rB3e98331a](https://projects.blender.org/blender/blender/commit/3e98331a094b))
  - The \`MeshVertex.normal\` property is now read-only, which prevents
    confusion due to the way vertex normals are calculated on-demand.
    ([891268aa82](https://projects.blender.org/blender/blender/commit/891268aa824f0f))
  - Tweak events have been removed from \`KeyMapItem.type\`.
    (\`EVT\_TWEAK\_L\`, \`EVT\_TWEAK\_M\`, \`EVT\_TWEAK\_R\`)

<!-- end list -->

  -   
    Instead of special types, regular mouse button types should be used
    (\`LEFTMOUSE\`, \`MIDDLEMOUSE\`, \`RIGHTMOUSE\`) with the
    \`KeyMapItem.value\` set to \`CLICK\_DRAG\`. Directional dragging
    can be specified by setting \`KeyMapItem.direction\`.
    ([4986f71848](https://projects.blender.org/blender/blender/commit/4986f718482b061082936f1f6aa13929741093a2),
    [7e4c031328](https://projects.blender.org/blender/blender/commit/7e4c0313283304bd8f020eaedb94b35e75b50068))

<!-- end list -->

  - The value of motion events (\`MOUSEMOVE\`, \`INBETWEEN\_MOUSEMOVE\`)
    is now always \`NOTHING\`, instead of the previous \`PRESS\` /
    \`RELEASE\` event's value
    [52af3b20d4](https://projects.blender.org/blender/blender/commit/52af3b20d45ea525a0ce66b2613ac132c9032a3f).
  - Sequence properties \`frame\_still\_start\` and
    \`frame\_still\_end\` have been removed. Same functionality can be
    now achieved by setting negative \`frame\_offset\_start\` or
    \`frame\_offset\_end\`
    ([rB8ca9ce09](https://projects.blender.org/blender/blender/commit/8ca9ce09865e6a617d6c2f78f3483ba1fd5d6aef))
  - Adding a new Geometry Nodes modifier doesn't automatically populate
    it with a new node tree anymore
    ([08b4b657b6](https://projects.blender.org/blender/blender/commit/08b4b657b64f)).

## Deprecation

  - The primitive drawing types \`'LINE\_LOOP'\` and \`'TRI\_FAN'\` used
    in \`GPUBatch\` have been deprecated as they will be removed in
    future releases.
    ([rBe2d8b6dc](https://projects.blender.org/blender/blender/commit/e2d8b6dc06))

<!-- end list -->

  - Passing the context to operators as a dictionary) has been
    deprecated as part of the inclusion of \`Context.temp\_override\`
    which should be used instead.
    ([f438344cf2](https://projects.blender.org/blender/blender/commit/f438344cf243632e497772cf1f855b9c8856fd37))

:

``` python
# Deprecated API
bpy.ops.object.delete({"selected_objects": objects_to_delete})

# New API
with context.temp_override(selected_objects=objects_to_delete):
    bpy.ops.object.delete()
```

  - \`Mesh.vertex\_colors\` is deprecated and will be removed in a
    future release. The new sculpt paint tools can now create color
    attributes, found in \`Mesh.attributes\` with type \`BYTE\_COLOR\`
    and \`FLOAT\_COLOR\`. Importers and exporters are recommended to
    support both types of color attributes, and attributes in general.  
    Different from the old vertex colors, color attribute values are in
    scene linear color space instead of sRGB color space. For file
    formats that require sRGB colors,
    \`mathutils.Color.from\_scene\_linear\_to\_srgb\` can perform the
    conversion.
