# Modeling

## Modifiers

  - The Subdivision Surface modifier and node's performance has been
    improved when many loose edges are subdivided.
    ([12c235a1c5](https://projects.blender.org/blender/blender/commit/12c235a1c515d4))

# UV Editing

## Sculpt Tools

![Show Geometry driven relax option](../../images/Geometry.png
"Show Geometry driven relax option") The big new change in the UV editor
for Blender 3.4 is the addition of a new geometry-based relax brush.
This improves the quality of the UV mapping by making the UVs more
closely follow the 3D geometry. As this is a brush, the user can drive
the relaxation process.
([74ea0bee9c](https://projects.blender.org/blender/blender/commit/74ea0bee9c0a))

To support the new relax brush mode, there has been additional UV Sculpt
and UV Relax tool improvements:

  - Fix boundary edges for *UV Relax* tool.
    ([3c351da89f](https://projects.blender.org/blender/blender/commit/3c351da89f70))
  - UV Sculpt tools now respect pinned vertices.
    ([3c351da89f](https://projects.blender.org/blender/blender/commit/3c351da89f70))
  - UV Sculpt tools now work with *constrain to bounds*.
    ([66822319d3](https://projects.blender.org/blender/blender/commit/66822319d3ee))
  - UV Sculpt tools now ignore winding, preventing orphaned islands.
    ([dcf50cf046](https://projects.blender.org/blender/blender/commit/dcf50cf04668))
  - UV Grab tool now supports *Live Unwrap*.
    ([836c07f29c](https://projects.blender.org/blender/blender/commit/836c07f29c76))

## UV Packing

  - Specify exact margin when packing.
    ([c2256bf7f7](https://projects.blender.org/blender/blender/commit/c2256bf7f714))
  - Add option to use Blender 2.8 margin calculation.
    ([c2256bf7f7](https://projects.blender.org/blender/blender/commit/c2256bf7f714))
  - Many UV Packing operations now work with non-manifold geometry.
    (many commits)

## UV Grid and Selection

  - Allow non-uniform grids.
    ([b7decab07e](https://projects.blender.org/blender/blender/commit/b7decab07ef8))
  - Add option to use pixel spacing for UV Grid. (Also
    [b7decab07e](https://projects.blender.org/blender/blender/commit/b7decab07ef8))
  - Show UV grid over the top of the image.
    ([c50335b359](https://projects.blender.org/blender/blender/commit/c50335b359e0))
  - Rename "UV Snap To Pixels" -\> "UV Round To Pixels".
    ([b5115ed80f](https://projects.blender.org/blender/blender/commit/b5115ed80f19))

## New Operators

Additional new operators have been added affecting island rotation:

  - Add new operator, *Randomize Islands*.
    ([de570dc87e](https://projects.blender.org/blender/blender/commit/de570dc87ed1))
  - Add new operator, *UV Align Rotation*.
    ([20daaeffce](https://projects.blender.org/blender/blender/commit/20daaeffce4c))

## Other Improvements and Fixes

  - Rotation operator supports *constrain to bounds*.
    ([d527aa4dd5](https://projects.blender.org/blender/blender/commit/d527aa4dd53d))
  - Respect UV Selection in *Spherical Project*, *Cylindrical Project*,
    *Cube Project* and *Smart UV
    Project*.([a5c696a0c2](https://projects.blender.org/blender/blender/commit/a5c696a0c2b9))
  - Fix UV Island calculation with hidden faces.
    ([8f543a73ab](https://projects.blender.org/blender/blender/commit/8f543a73abc4))
  - Fix bugs in UV Island calculation when in edge selection mode.
    ([178868cf42](https://projects.blender.org/blender/blender/commit/178868cf4259))
  - More options for *UV Select Similar* operator, *Face*, *Area* and
    *Area UV*.
    ([a581460728](https://projects.blender.org/blender/blender/commit/a5814607289a))
  - Fix *UV Unwrap* with degenerate triangles.
    ([94e211ced9](https://projects.blender.org/blender/blender/commit/94e211ced914))
