# EEVEE

  - Headless rendering is now supported under Linux
    ([3195a38120](https://projects.blender.org/blender/blender/commit/3195a381200eb98e6add8b0504f701318186946d))
