# Blender 3.4 Release Notes

Blender 3.4 was released on December 7, 2022.

Check out out the final [release notes on
blender.org](https://www.blender.org/download/releases/3-4/).

## [Animation & Rigging](Animation_Rigging.md)

## [Core](Core.md)

## [EEVEE & Viewport](EEVEE.md)

## [Grease Pencil](Grease_Pencil.md)

## [Modeling & UV](Modeling.md)

## [Nodes & Physics](Nodes_Physics.md)

## [Pipeline, Assets & I/O](Pipeline_Assets_IO.md)

## [Platform-Specific Changes](Platforms.md)

## [Python API & Text Editor](Python_API.md)

## [Render & Cycles](Cycles.md)

## [Sculpt, Paint, Texture](Sculpt.md)

## [User Interface](User_Interface.md)

## [Add-ons](Add-ons.md)

## Compatibility

  - The "Transfer Attribute" node has been split into three separate
    nodes. For more information, see the [geometry nodes
    section](Nodes_Physics.md#Geometry_Nodes)
  - The "MixRGB" node for Shader and Geometry Nodes is converted to the
    new "Mix Node" on loading. Files saved with the new node are not
    forward compatible with older versions of Blender
    ([rBbfa0ee13](https://projects.blender.org/blender/blender/commit/bfa0ee13d539)).

## [Corrective Releases](Corrective_Releases.md)
