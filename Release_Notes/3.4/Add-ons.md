# Add-ons

## Image import as Plane

  - Update improving the UI and adding several common material settings
    ([D15744](http://developer.blender.org/D15744),
    [rBA2ec6aba](https://projects.blender.org/blender/blender-addons/commit/2ec6aba72cd3)).

## New add-ons

  - New Storypencil add-on for Storyboarding in addons-contrib.
    ([T100665](http://developer.blender.org/T100665))

## glTF 2.0

### Importer

  - Fix import SK when there is no target on each primitives
    ([rBA70ee1a5](https://projects.blender.org/blender/blender-addons/commit/70ee1a5b6600))
  - Code cleanup
    ([rBAb9c0e28](https://projects.blender.org/blender/blender-addons/commit/b9c0e2878e91))
  - Add hook before glTF import
    ([rBAa1706bd](https://projects.blender.org/blender/blender-addons/commit/a1706bd0f04b))
  - Tweaks vertex color import
    ([rBAcd2d9df](https://projects.blender.org/blender/blender-addons/commit/cd2d9df92577))
  - Fix ortho camera frame import/export
    ([rBA8a24438](https://projects.blender.org/blender/blender-addons/commit/8a2443844daf))
  - **Import/Export lights using correct units**
    ([rBA9d903a9](https://projects.blender.org/blender/blender-addons/commit/9d903a93f03b))

### Exporter

  - **Reset pose bone between each action**
    ([rBAe77b55e](https://projects.blender.org/blender/blender-addons/commit/e77b55e45a2a))
  - **Option to export Active Collection only / with or without nested**
    ([rBA90732dd](https://projects.blender.org/blender/blender-addons/commit/90732dddff7e),
    [rBA712f007](https://projects.blender.org/blender/blender-addons/commit/712f007c7179))
  - **Big refactoring of primitive extraction, manage custom
    attributes**
    ([rBA51e15a9](https://projects.blender.org/blender/blender-addons/commit/51e15a9db4ce),
    [rBA33e8782](https://projects.blender.org/blender/blender-addons/commit/33e87824dc33),
    [rBA7e2fa37](https://projects.blender.org/blender/blender-addons/commit/7e2fa377ab1f),
    [rBA4be7119](https://projects.blender.org/blender/blender-addons/commit/4be7119ac5a7))
  - Fix object parented to bone when using rest pose
    ([rBA4cb6ebd](https://projects.blender.org/blender/blender-addons/commit/4cb6ebd8747e),
    [rBA106cb51](https://projects.blender.org/blender/blender-addons/commit/106cb51ab9ad),
    [rBAeb07144](https://projects.blender.org/blender/blender-addons/commit/eb07144ea7e8))
  - Manage delta transforms animations
    ([rBA58db76b](https://projects.blender.org/blender/blender-addons/commit/58db76bcc6ae),
    [rBA726d08c](https://projects.blender.org/blender/blender-addons/commit/726d08c9036b))
  - Avoid crash when using apply modifiers + shapekeys
    ([rBAbb77e69](https://projects.blender.org/blender/blender-addons/commit/bb77e697e13e))
  - Fix scene evaluation
    ([rBA3c19be6](https://projects.blender.org/blender/blender-addons/commit/3c19be6ffa06))
  - Fix resolve UVMap
    ([rBAbccd6c6](https://projects.blender.org/blender/blender-addons/commit/bccd6c669db6))
  - Better skin checks
    ([rBA97bb515](https://projects.blender.org/blender/blender-addons/commit/97bb515d3ac4))
  - Clamp base color factor to \[0,1\]
    ([rBAe890169](https://projects.blender.org/blender/blender-addons/commit/e890169e0a62))
  - Various hooks & fixes for animation
    ([rBAeb3dcfc](https://projects.blender.org/blender/blender-addons/commit/eb3dcfc70c7f))
  - Avoid adding multiple neutral bone on same armature
    ([rBA8466799](https://projects.blender.org/blender/blender-addons/commit/846679918718))
  - Fix TRS when parent is skined
    ([rBA83290c6](https://projects.blender.org/blender/blender-addons/commit/83290c67a63b))
  - Fix ortho camera frame import/export
    ([rBA8a24438](https://projects.blender.org/blender/blender-addons/commit/8a2443844daf))
  - **Import/Export lights using correct units**
    ([rBA9d903a9](https://projects.blender.org/blender/blender-addons/commit/9d903a93f03b))
  - Non active action objects TRS is now restored at end of actions
    ([rBA5dbcd4a](https://projects.blender.org/blender/blender-addons/commit/5dbcd4a3889f))
  - When user don't export SK, driver corrective SK animations must not
    be exported
    ([rBAc4dddb8](https://projects.blender.org/blender/blender-addons/commit/c4dddb88e7a7))
  - Do not export special attributes, used internally by Blender
    ([rBAaddeb64](https://projects.blender.org/blender/blender-addons/commit/addeb64f82f4))
  - Export SK on no modifiers objects even if 'apply modifier' is on
    ([rBA896ee46](https://projects.blender.org/blender/blender-addons/commit/896ee4698f25))
  - Fix Traceback in Variant UI
    ([rBA61ff4fa](https://projects.blender.org/blender/blender-addons/commit/61ff4faf8c7d))
  - Fix exporting skined mesh without armature
    ([rBA0b4844e](https://projects.blender.org/blender/blender-addons/commit/0b4844ede970))
  - Fix crash with weird missing datablock node group
    ([rBAb665525](https://projects.blender.org/blender/blender-addons/commit/b665525d03ce))
