# User Interface

## Outliner

  - Searching by name isn't possible anymore in the Hierarchies view of
    the Library Overrides display mode. This is to workaround big
    performance issues in complex scenes.
    ([rB21b92a5f](https://projects.blender.org/blender/blender/commit/21b92a5f31a4))
  - The data-block type filter was removed from the Properties view of
    the Library Overrides display mode. It isn't needed anymore since
    data-blocks are now grouped by type anyway.
    ([rBb5fc8f61](https://projects.blender.org/blender/blender/commit/b5fc8f611e39))
  - The context menu doesn't show whole sections of operations that
    cannot operate on the selected elements anymore.
    ([rB7eda9d8d](https://projects.blender.org/blender/blender/commit/7eda9d8dda59))
  - Improved element count display with better readability and higher
    numbers.([rBa5d3b648](https://projects.blender.org/blender/blender/commit/a5d3b648e3e2),
    [84825e4ed2](https://projects.blender.org/blender/blender/commit/84825e4ed2e0)).

<center>

|                                                                                                            |                                                                                                         |
| ---------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------- |
| ![../../images/D16284\_-\_before.png](../../images/D16284_-_before.png "../../images/D16284_-_before.png") | ![../../images/D16284\_-\_after.png](../../images/D16284_-_after.png "../../images/D16284_-_after.png") |

style="caption-side: bottom" | Element count display before and after

</center>

## Viewport

  - The select menu (accessed by holding Alt while picking objects &
    bones) now orders results by distance (nearest first)
    ([47d3e76567](https://projects.blender.org/blender/blender/commit/47d3e765679e54c09ecc4ded812e89483a1e1d9b)).
  - Improved display of text caret and selection when editing 3D Text
    Objects
    ([a1e01f4c02](https://projects.blender.org/blender/blender/commit/a1e01f4c026a)).

![../../images/3DCaret.png](../../images/3DCaret.png
"../../images/3DCaret.png")

## General

  - Added shortcuts
    <span class="hotkeybg"><span class="hotkey">PageUp</span></span> and
    <span class="hotkeybg"><span class="hotkey">PageDown</span></span>
    to the console to scroll entire pages, as well as
    <span class="hotkeybg"><span class="hotkey">↖ Home</span></span> to
    reset scrolling to the bottom.
    ([rB82fc52ff](https://projects.blender.org/blender/blender/commit/82fc52ffc881))
  - Detect existing file and add +/- auto-increase for output filepaths
    (render filepath and File Output node for now)
    ([rB78bfaf1a](https://projects.blender.org/blender/blender/commit/78bfaf1a4fe1)).

![../../images/Auto-increase.png](../../images/Auto-increase.png
"../../images/Auto-increase.png")

  - Font thumbnails now preview languages, shapes, content, and intended
    use better.
    ([4e7983e073](https://projects.blender.org/blender/blender/commit/4e7983e07320)).

![../../images/FontThumbs.png](../../images/FontThumbs.png
"../../images/FontThumbs.png")

  - Improved editing of text that contains non-precomposed diacritical
    marks.
    ([12fdf9069a](https://projects.blender.org/blender/blender/commit/12fdf9069abe)).
