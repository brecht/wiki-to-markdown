# Sculpt, Paint, Texture

## Performance

  - Performance and memory usage have been improved when face sets,
    hiding, and masking aren't used
    ([b5f7af31d6](https://projects.blender.org/blender/blender/commit/b5f7af31d6d474)).
  - Significant performance improvement for reprojecting attributes when
    using the voxel remesher
    ([bad55d56bc](https://projects.blender.org/blender/blender/commit/bad55d56bc71)).

## Auto-Masking

  - New popover in the 3D Viewport header to manage all auto-masking
    options
    ([db40b62252](https://projects.blender.org/blender/blender/commit/db40b62252e5)).

![../../images/Auto-masking\_header.png](../../images/Auto-masking_header.png
"../../images/Auto-masking_header.png")

This UI makes the auto-masking toggles more accessible and discoverable.
Each setting is now properly ordered. For even faster access use the pie
menu shortcut \`Alt-A\`.

![../../images/Auto-masking\_brush.png](../../images/Auto-masking_brush.png
"../../images/Auto-masking_brush.png")

The same UI is used for the brush settings as well. Any brush
auto-masking toggle of the active brush will override the mode
auto-masking toggles.

  - Cavity auto-masking toggles in the options and brush settings
    ([0156a677c7](https://projects.blender.org/blender/blender/commit/0156a677c7d1)).

![../../images/Create\_cavity\_mask\_header.png](../../images/Create_cavity_mask_header.png
"../../images/Create_cavity_mask_header.png")

Instead of manually creating a cavity mask, this auto-masking option
provides a faster way of painting and sculpting with Cavity.

"Inverted", "Factor", "Blur" and a "Custom Curve" give extra control to
fine tune the cavity mask. Use the "Create Mask" button to convert the
auto-mask into a regular mask attribute (To edit it further or just
visualize it).

![Artwork by Steffen Hartmann](../../images/Mask_from_cavity.png
"Artwork by Steffen Hartmann")

This includes a new "Mask From Cavity" operator, which replaced the old
"Dirty Mask" menu operator.

  - Area and View Normal auto-masking toggles in the options and brush
    settings
    ([0156a677c7](https://projects.blender.org/blender/blender/commit/0156a677c7d1)).

![Artwork by Ramil Roosileht](../../images/Normal_auto-masking.png
"Artwork by Ramil Roosileht")

These auto-masking options are great for selectively painting/sculpting
from specific angles. "View Normal" uses the viewing angle, while "Area
Normal" uses the normal direction of the brush cursor from the start of
the stroke.

"Limit" & "Falloff" define how many angles are included in the auto-mask
and how soft the mask is.

An additional "Occlusion" toggle allows more precise projection painting
at the cost of performance.

For more information see [auto-masking
documentation](https://docs.blender.org/manual/en/3.4/sculpt_paint/sculpting/controls.html)
in the manual.

## Other

  - Face Sets are now opt-in, meaning that primitive objects do not have
    a Face Set attribute by default. As a result when objects are joined
    or modeling changes are made in Edit Mode, any new geometry will not
    automatically be assigned a Face Set, improving performance and
    removing visual clutter when not used.
    ([e8291f4504](https://projects.blender.org/blender/blender/commit/e8291f4504d3))
  - Weight and Vertex painting will use the whole modifier stack if it
    produces no topology changes, to allow painting with weight layering
    using Geometry Nodes or Weight Mix modifiers.
    ([9823a8f72b](https://projects.blender.org/blender/blender/commit/9823a8f72be8088))
