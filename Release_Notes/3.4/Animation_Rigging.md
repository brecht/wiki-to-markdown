# Animation & Rigging

## Redo Panel

  - The NLA, Dopesheet, and Timeline editors now have a Redo panel
    ([rB1f828a5a](https://projects.blender.org/blender/blender/commit/1f828a5a064f),
    [rB3132d275](https://projects.blender.org/blender/blender/commit/3132d2751e61)).
    Some properties shown in these redo panels may be superfluous (like
    having a Z-axis when moving keys around); this is a known
    limitation.

## NLA

  - Draw track background based on strip's extrapolation type
    ([rB2310daed](https://projects.blender.org/blender/blender/commit/2310daed3a55)).

![../../images/blender-3.4-nla-extrapolation-drawing.png](../../images/blender-3.4-nla-extrapolation-drawing.png
"../../images/blender-3.4-nla-extrapolation-drawing.png")

  - Adding an action clip now fails immediately if no NLA track is
    selected
    ([rBddfce277](https://projects.blender.org/blender/blender/commit/ddfce277e0cb)).
    Previously this only failed after you selected a specific action to
    add.
  - Removed the "Edited Action" tab for selected Action strips in the
    NLA editor
    ([rBb6ebd559](https://projects.blender.org/blender/blender/commit/b6ebd5591c7f)).
    It is still available in the Action editor, where it is actually
    suitable/usable. Having it in the NLA got in the way of the actual
    NLA strip properties. These are now available immediately by
    default.
  - Pushing down an action to a new NLA track now automatically names
    that track after the Action
    ([rB78fe6d7a](https://projects.blender.org/blender/blender/commit/78fe6d7ab195)).

## Driver Mute

The driver editor and the "Edit Driver" popover now have a checkbox that
can mute the driver
([rBc592bff0](https://projects.blender.org/blender/blender/commit/c592bff04745)).
This is the same functionality as the checkbox in the driver editor's
channel list, but then exposed in a different place in the UI. This is
for convenience, such that a driver can now be muted by right-clicking
on the driven property, choosing "Edit Driver", then muting it there.
The same checkbox was added to the regular driver editor's header for
consistency.

[File:blender-3.4-driver-mute.png|Driver](File:blender-3.4-driver-mute.png%7CDriver)
Editor
[File:blender-3.4-driver-mute-popover.png|Edit](File:blender-3.4-driver-mute-popover.png%7CEdit)
Driver pop-over
