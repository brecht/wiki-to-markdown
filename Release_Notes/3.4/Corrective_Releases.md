# Blender 3.4.1

Released on December 20th 2022, Blender 3.4.1 features the following bug
fixes:

  - Allow Win32 Diacritical Composition
    [\#103119](http://developer.blender.org/T103119)
  - Box trim does not create face sets attribute
    [\#103052](http://developer.blender.org/T103052)
  - Changed behavior when removing a material slot
    [\#103051](http://developer.blender.org/T103051)
  - Cycles can lose default color attribute
    [\#103143](http://developer.blender.org/T103143)
  - Cycles missing full constant foler for mix float and mix vector
    [\#103066](http://developer.blender.org/T103066)
  - Cycles random animation rendering freezing up the application
    [\#103101](http://developer.blender.org/T103101)
  - Cycles specular light leak regression
    [\#103049](http://developer.blender.org/T103049)
  - Fix cursor warping display under Wayland
    [rB18cc1b11](https://projects.blender.org/blender/blender/commit/18cc1b110840)
  - glTF exporter: Fix export VertexColor for loose edges & vertices
    [rBAfea0e8e](https://projects.blender.org/blender/blender-addons/commit/fea0e8e32e2)
  - glTF importer: Fix import ShaderNodeMix with VertexColor
    [rBAb17b70d](https://projects.blender.org/blender/blender-addons/commit/b17b70d1b42)
  - glTF: Fix factors export after Mix Node change in Blender
    [rBA3fbcd35](https://projects.blender.org/blender/blender-addons/commit/3fbcd35feb2)
  - GPencil applying armature does not work
    [\#103234](http://developer.blender.org/T103234)
  - GPencil Array doesn't respect restriction in Offset
    [\#102992](http://developer.blender.org/T102992)
  - GPencil export to SVG wrong line thickness
    [\#103061](http://developer.blender.org/T103061)
  - GPencil Line Texture last point gets distorted
    [\#103037](http://developer.blender.org/T103037)
  - GPencil Multiframe Scale affects stroke thickness inversely
    [\#103293](http://developer.blender.org/T103293)
  - GPencil: Fix unreported interpolate crash in empty layers
    [rBc2e7bf39](https://projects.blender.org/blender/blender/commit/c2e7bf395352)
  - GPU: Fix using FLOAT\_2D\_ARRAY and FLOAT\_3D textures via Python.
    [rBfd9b1972](https://projects.blender.org/blender/blender/commit/fd9b197226df)
  - Ignore unavailable sockets linked to multi-input socket
    [rB7608ebe4](https://projects.blender.org/blender/blender/commit/7608ebe44aa3)
  - Initialize face sets from bevel weights broken
    [\#103195](http://developer.blender.org/T103195)
  - Invalid font size in the image editor with some scripts
    [\#102213](http://developer.blender.org/T102213)
  - Mouse escapes window during walk navigation
    [\#102346](http://developer.blender.org/T102346)
  - NodeSocket.node is None in Node.copy callback
    [\#103321](http://developer.blender.org/T103321)
  - Opening node search menu is slow because of assets.
    [\#103187](http://developer.blender.org/T103187)
  - PLY: Fix export with UVs
    [\#103203](http://developer.blender.org/T103203)
  - Prevent UV Unwrap from packing hidden UV islands
    [\#103237](http://developer.blender.org/T103237)
  - Sculpt: Wpaint gradient tool doesn't work with vertex mask
    [\#101914](http://developer.blender.org/T101914)
  - Stopping Animation in Some Scenes Immediately Crashes Blender
    [\#103008](http://developer.blender.org/T103008)
  - Storypencil add-on not being shipped with release builds.
    [rBAa61732a](https://projects.blender.org/blender/blender-addons/commit/a61732a0aa9)
  - Storypencil clears all frame\_change\_post handlers.
    [\#103028](http://developer.blender.org/T103028)
  - UI: broken texpaintslot/color attributes/attributes name filtering
    [\#102878](http://developer.blender.org/T102878)
  - UI: Hotkey conflict Alt D in Node Editor with Duplicate Linked and
    Detach [\#102276](http://developer.blender.org/T102276)
  - Unavailable socket linked to multi-input socket crashes
    [\#103208](http://developer.blender.org/T103208)
  - Undo after mask extract doesn't restore active object
    [\#103261](http://developer.blender.org/T103261)
  - ViewLayer: Crash in indirect\_only\_get due to missing null check
    [\#103031](http://developer.blender.org/T103031)
  - Workbench render crash in 3.4
    [\#103067](http://developer.blender.org/T103067)
