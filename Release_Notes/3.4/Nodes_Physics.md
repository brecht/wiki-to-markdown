# Geometry Nodes

## Viewer Node

  - The *Viewer Node* now has the ability to preview geometry and
    attributes in the viewport as well as the spreadsheet
    ([rBc55d38f0](https://projects.blender.org/blender/blender/commit/c55d38f00b8c)).
      - When a field is linked to the second input of the viewer node it
        is displayed as an overlay in the viewport.
      - A viewer node is activated by clicking on it.
      - The attribute overlay opacity can be controlled with the "Viewer
        Node" setting in the overlays popover.
      - A viewport can be configured not to show intermediate
        viewer-geometry by disabling the "Viewer Node" option in the
        "View" menu.
      - Viewer nodes now have a domain dropdown. It determines which
        domain the field will be evaluated on.

![../../videos/2022-09-28\_13-06-14.mp4](../../videos/2022-09-28_13-06-14.mp4
"../../videos/2022-09-28_13-06-14.mp4")

### General

  - The **Self Object** retrieves the current modifier object for
    retrieving transforms
    ([dd5131bd70](https://projects.blender.org/blender/blender/commit/dd5131bd700c7e)).
  - The *Transfer Attribute* node has been removed and split into
    multiple more specific nodes
    ([dedc679eca](https://projects.blender.org/blender/blender/commit/dedc679ecabb43)).
      - The **Sample Index** node retrieves data from specific geometry
        elements by index.
      - The **Sample Nearest** node retrieves the indices from the
        closest geometry elements
      - The **Sample Nearest Surface** node interpolates a field input
        to the closest location on a mesh surface.

### Meshes

  - The new **Face Set Boundaries** node finds the edges between
    different patches of faces
    ([3ff15a9e23](https://projects.blender.org/blender/blender/commit/3ff15a9e23bd8a)).
  - Access to mesh topology information has been improved, with new
    nodes and other changes .
      - **Corners of Face** Retrieves corners that make up a face
        ([482d431bb6](https://projects.blender.org/blender/blender/commit/482d431bb673)).
      - **Corners of Vertex** Retrieves face corners connected to
        vertices
        ([482d431bb6](https://projects.blender.org/blender/blender/commit/482d431bb673)).
      - **Edges of Corner** Retrieves the edges on boths sides of a face
        corner
        ([482d431bb6](https://projects.blender.org/blender/blender/commit/482d431bb673)).
      - **Edges of Vertex** Retrieves the edges connected to each vertex
        ([482d431bb6](https://projects.blender.org/blender/blender/commit/482d431bb673)).
      - **Face of Corner** Retrieves the face each face corner is part
        of
        ([482d431bb6](https://projects.blender.org/blender/blender/commit/482d431bb673)).
      - **Offset Corner in Face** Retrieves neighboring corners within a
        face
        ([482d431bb6](https://projects.blender.org/blender/blender/commit/482d431bb673)).
      - **Vertex of Corner** Retrieves the vertex each face corner is
        attached to
        ([482d431bb6](https://projects.blender.org/blender/blender/commit/482d431bb673)).
      - The *Edge Vertices* node now has an index input so it can be
        more easily combined with the other nodes
        ([4ddc5a936e](https://projects.blender.org/blender/blender/commit/4ddc5a936e07)).
  - The new **Sample UV Surface** node allows getting an attribute value
    based on a UV coordinate
    ([e65598b4fa](https://projects.blender.org/blender/blender/commit/e65598b4fa2059)).

### Curves

  - The *Trim Curves* node now supports cyclic curves
    ([eaf416693d](https://projects.blender.org/blender/blender/commit/eaf416693dcb43)).
  - New nodes give access to topology information about the mapping
    between curves and points.
      - **Curve of Point** Retrieves the curve a control point is part
        of
        ([482d431bb6](https://projects.blender.org/blender/blender/commit/482d431bb673)).
      - **Points of Curve** Retrieves a point index within a curve
        ([482d431bb6](https://projects.blender.org/blender/blender/commit/482d431bb673)).
      - **Offset Point in Curve** Offset a control point index within
        its curve
        ([8a6dc0fac7](https://projects.blender.org/blender/blender/commit/8a6dc0fac71c),
        ([248def7e48](https://projects.blender.org/blender/blender/commit/248def7e4806))).
  - The **Set Curve Normal** allows choosing the normal evaluation mode
    for curves
    ([748fda32ed](https://projects.blender.org/blender/blender/commit/748fda32ede8)).
  - The **Sample Curve** node now has inputs for the curve index and a
    custom value to sample
    ([rB5f7ca546](https://projects.blender.org/blender/blender/commit/5f7ca5462d31)).

### Volumes

  - The **Distribute Points in Volume** node creates points inside of
    volume grids
    ([b6e26a410c](https://projects.blender.org/blender/blender/commit/b6e26a410cd29f)).

### Instances

  - Attributes of instances created via geometry nodes are now
    accessible from materials through the Instancer mode of the
    Attribute node
    ([2f7234d3e1](https://projects.blender.org/blender/blender/commit/2f7234d3e1bbc)).

### Performance

  - Geometry nodes has a new evaluation system
    ([4130f1e674](https://projects.blender.org/blender/blender/commit/4130f1e674f83f)).
      - The evaluator can provide better performance when many complex
        node groups are used.
      - Performance can be increased when there are very many small
        nodes
        ([5c81d3bd46](https://projects.blender.org/blender/blender/commit/5c81d3bd469121)).
  - The *Trim Curves* node has been ported to the new data-block and can
    be 3-4 times faster than in 3.2 or 3.3
    ([eaf416693d](https://projects.blender.org/blender/blender/commit/eaf416693dcb43)).
  - Mesh and curve domain interpolation can be skipped for single values
    ([e89b2b1221](https://projects.blender.org/blender/blender/commit/e89b2b122135),
    [535f50e5a6](https://projects.blender.org/blender/blender/commit/535f50e5a6a2)).

## Node Editor

### Assets

  - Node group assets are visible in the add menus of the node editor
    ([bdb5754147](https://projects.blender.org/blender/blender/commit/bdb57541475f20)).

### User Interface

  - Show data-block use count in the Context Path and Group node header
    ([84825e4ed2](https://projects.blender.org/blender/blender/commit/84825e4ed2e0)).

![../../images/Geometry\_Nodes\_Use\_Count.png](../../images/Geometry_Nodes_Use_Count.png
"../../images/Geometry_Nodes_Use_Count.png")

  - Node links have been tweaked to make them fit better with the
    overall look of the node editor
    ([9a86255da8](https://projects.blender.org/blender/blender/commit/9a86255da8dcf0)).
      - Link curving has been adjusted to make vertical links look
        better
        ([67308d73a4](https://projects.blender.org/blender/blender/commit/67308d73a4f9ec)).
  - Input sockets are reused when creating new node groups
    ([rB14e4c96b](https://projects.blender.org/blender/blender/commit/14e4c96b64f9)).

### Duplicate Linked

  - Duplicate Linked operator and user Preference option for Node Tree
    data duplication
    ([rB6beeba1e](https://projects.blender.org/blender/blender/commit/6beeba1ef5ae)).

![../../images/Nodetree\_duplication.png](../../images/Nodetree_duplication.png
"../../images/Nodetree_duplication.png")
