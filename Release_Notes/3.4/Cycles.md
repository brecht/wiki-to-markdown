# Cycles

## Path Guiding

Path guiding was integrated
([75a6d3abf7](https://projects.blender.org/blender/blender/commit/75a6d3abf75f))
into Cycles using Intel's [Open Path Guiding
Library](http://www.openpgl.org/).

This feature improves the sampling quality of individual paths reducing
the noise in complex lighting scenarios such as; long indirect light
bounces, indirectly illuminated shadow areas, or reflected light
sources.

**Note:** While Path Guiding helps to render scenes containing simple
caustics (e.g., water surface), it is not designed to be a caustic
solver, such as MNEE, meaning it might not help render scenes containing
complex caustics.

During rendering, path guiding learns an approximation of the scene's
light distribution (direct and indirect). This information is then used
to guide paths into important directions that might not be explored well
using standard directional sampling methods that only consider the local
material or the directly visible light sources.

More details about path guiding, how it works, and how it is integrated
into Cycles are presented in the BCON2022 talk [Follow the light:
introducing path guiding in
Cycles](https://www.youtube.com/watch?v=BS1JLbNqGxI&t=17s).

At the moment, path guiding is only available for CPU rendering and can
be enabled in the render properties panel under ‘Sampling-\>Path
Guiding’. The current implementation supports guiding on surfaces with
diffuse BSDFs components as well as inside volumes.

The following images show the effects of path guiding in practice when
applied in different scenes with complex lighting scenarios. Each
comparison is rendered at an equal time.

<center>

|                                                                                                                                                     |                                                                                                                                                  |
| --------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------ |
| ![../../images/Cycles\_path\_guiding\_0\_before.png](../../images/Cycles_path_guiding_0_before.png "../../images/Cycles_path_guiding_0_before.png") | ![../../images/Cycles\_path\_guiding\_0\_after.png](../../images/Cycles_path_guiding_0_after.png "../../images/Cycles_path_guiding_0_after.png") |

style="caption-side: bottom" | Equal time render without and with path
guiding, original scene by Andy Goralczyk

</center>

<center>

|                                                                                                                                                        |                                                                                                                                                  |
| ------------------------------------------------------------------------------------------------------------------------------------------------------ | ------------------------------------------------------------------------------------------------------------------------------------------------ |
| ![../../images/Cycles\_path\_guiding\_1\_before.jpeg](../../images/Cycles_path_guiding_1_before.jpeg "../../images/Cycles_path_guiding_1_before.jpeg") | ![../../images/Cycles\_path\_guiding\_1\_after.jpg](../../images/Cycles_path_guiding_1_after.jpg "../../images/Cycles_path_guiding_1_after.jpg") |

style="caption-side: bottom" | Equal time render without and with path
guiding, scene by Alaska

</center>

<center>

|                                                                                                                                                     |                                                                                                                                                  |
| --------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------ |
| ![../../images/Cycles\_path\_guiding\_2\_before.jpg](../../images/Cycles_path_guiding_2_before.jpg "../../images/Cycles_path_guiding_2_before.jpg") | ![../../images/Cycles\_path\_guiding\_2\_after.jpg](../../images/Cycles_path_guiding_2_after.jpg "../../images/Cycles_path_guiding_2_after.jpg") |

style="caption-side: bottom" | Equal time render without and with path
guiding, scene by Jesus Sandoval

</center>

## Improvements

  - Attribute node: support for using attributes of geometry nodes
    instances. Additionally, boolean and integer array attributes can be
    read from objects.
    ([2f7234d3e1](https://projects.blender.org/blender/blender/commit/2f7234d3e1b),
    [554afabf75](https://projects.blender.org/blender/blender/commit/554afabf7580))
  - Attribute node: new View Layer mode for accessing attributes of the
    current View Layer, Scene or World
    ([f61ff22967](https://projects.blender.org/blender/blender/commit/f61ff22967c5f))
  - Baking: new option to bake specular effects from the active camera
    view, instead of above the surface. This may be useful when baking
    textures that will be viewed from a fixed position or with limited
    camera motion.
    ([b475506](https://projects.blender.org/blender/blender/commit/b475506))
  - Sampling: improved patterns to reduce noise in some scenes,
    particularly for volumes and long area lights.
    ([50df9ca](https://projects.blender.org/blender/blender/commit/50df9ca),
    [a06c9b5](https://projects.blender.org/blender/blender/commit/a06c9b5))

## GPU Rendering

  - AMD HIP
      - AMD Radeon RX 7000 series graphics cards (RDNA3 architecture)
        now supported.
      - On Linux, upgrade to ROCm 5.3 or newer to fix issues with
        textures on Vega and RDNA1 graphics cards.
  - Apple Metal
      - Shadow caustics enabled.
      - Intel GPUs now supported starting with macOS 13.
  - Intel OneAPI
      - Upgrade to driver version 101.3430 or newer to fix user
        interface crashes.

## Open Shading Language

Open Shading Language was upgraded to version 1.12.6.

This comes with change in behavior for input parameters with a
non-constant initializer. For such parameters, the input socket on the
node now shows no editable value, it can only be linked. This can also
be specified through metadata.

``` C
shader test(
  normal Normal = N,                           /* Value hidden due to initializer. */
  float A = 0.0 [[ string widget = "null" ]])  /* Value hidden through metadata. */
```
