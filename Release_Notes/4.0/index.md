# Blender 4.0 Release Notes

Blender 4.0 was released on November 14, 2023.

Check out out the final [release notes on
blender.org](https://www.blender.org/download/releases/4-0/).

## [Animation & Rigging](Animation_Rigging/index.md)

## [Geometry Nodes](Geometry_Nodes.md)

## [Modeling](Modeling.md)

## [Sculpt & Paint](Sculpt.md)

## [Shading & Texturing](Shading.md)

## [Cycles](Cycles.md)

## [Color Management](Color_Management.md)

## [Compositor & Sequencer](VFX.md)

## [Import & Export](Import_Export.md)

## [Core](Core.md)

## [User Interface](User_Interface.md)

### [3D Viewport](Viewport.md)

### [Node Editor](Node_Editor.md)

### [Keymap](Keymap.md)

## [Add-ons](Add-ons.md)

## [Python API](Python_API.md)

## [Asset Bundles](Asset_Bundles.md)

## Compatibility

### Blend Files

  - The mesh format changes from previous versions are now included in
    the Blender file format.
      - Blender 3.6 LTS can read files saved with 4.0, but earlier
        versions cannot.
      - Blender 3.6 LTS will save meshes in a format compatible with
        older versions of Blender, so it can be used as conversion tool
        if needed.
      - See the section in the [Python
        API](Python_API.md#Breaking_changes)
        section for more details.
  - The [new blendfile compatibility
    policy](../../Process/Compatibility_Handling.md)
    ([\#109151](https://projects.blender.org/blender/blender/issues/109151))
    has been implemented (see [PR
    \#110109](https://projects.blender.org/blender/blender/pulls/110109)).
  - Unused linked data is not kept anymore when saving and re-opening
    .blend files. See [the core
    page](Core.md) for details.

### Graphics Cards

  - The minimum required OpenGL version has been increased to 4.3 for
    Linux and Windows.
    ([3478093183](https://projects.blender.org/blender/blender/commit/3478093183f91ac49988a0f1e077bde590d62a82))
  - On macOS only Metal is supported now, and the OpenGL backend has
    been removed. A GPU with Metal 2.2 support is required. Intel Macs
    without an AMD GPU require a Skylake processor or higher.
    ([cdb8a8929c](https://projects.blender.org/blender/blender/commit/cdb8a8929cecd09735092e9ad1fc3adf15501615))
  - Due to driver issues, support for Intel HD4000 series GPUs has been
    dropped.

## [Corrective Releases](Corrective_Releases.md)
