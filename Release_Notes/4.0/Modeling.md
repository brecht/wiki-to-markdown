# Modeling

## Modifiers

The Add Modifier menu has been changed to a standard menu and now
includes [geometry nodes
assets](Reference/Release_Notes/4.0/Nodes_Physics#Geometry_Nodes).

A new "Shift A" shortcut opens the Add Modifier menu in the properties
editor.

## Snap and Navigation

You can now press **B** to set **base point** when you transform
objects. This allows for a fast and precise snap from vertex to vertex.

Additionally you can navigate while transforming while holding **Alt**,
and there are different snap symbols for the different snap types
(vertex, mid-point, perpendicular, ...).

![All Snap Improvements|800px](../../videos/All_Snap_Improvements.mp4
"All Snap Improvements|800px")

  - It is now possible to navigate while transform (Move, Rotate, Scale,
    Edge Slide, Vert Slide, and Shrink/Fatten), by default you have to
    hold \`Alt\` and navigate.
    ([33c13ae6e3](https://projects.blender.org/blender/blender/commit/33c13ae6e3),
    [017d4912b2](https://projects.blender.org/blender/blender/commit/017d4912b2)).
  - Transform operations now have a new feature to edit the 'Snap Base'
    ([3010f1233b](https://projects.blender.org/blender/blender/commit/3010f1233b)).
  - Bones now support snapping with "Align Rotation to Target"
    ([dea93845af](https://projects.blender.org/blender/blender/commit/dea93845af)
    and
    [4d1593c4ad](https://projects.blender.org/blender/blender/commit/4d1593c4ad)).
  - New snap symbols
    ([9c2e768f5b](https://projects.blender.org/blender/blender/commit/9c2e768f5baf),
    [fb556c75df](https://projects.blender.org/blender/blender/commit/fb556c75df4c)).

### User Interface

![../../images/Snapping\_Menu.gif](../../images/Snapping_Menu.gif
"../../images/Snapping_Menu.gif") The snapping menu has been reworked
([8e059b569b](https://projects.blender.org/blender/blender/commit/8e059b569b)).

  - *Snap With* was moved to the beginning of the popover
  - *Align Rotation to Target* and *Backface Culling* were moved closer
    to the snap targets
  - *Snap With*, *Target Selection* and *Align Rotation to Target* are
    no longer hidden by varying the mode and options
  - *Project Individual Elements* has been replaced with the *Face
    Project* option
  - *Face Nearest* has been moved to stick together with the *Face
    Project* option

  

## Shape Keys

  - The Blend From Shape and Propagate To Shapes operators now respect X
    symmetry
    ([0bd95dd963](https://projects.blender.org/blender/blender/commit/0bd95dd96331),
    [4d0dbab5b1](https://projects.blender.org/blender/blender/commit/4d0dbab5b1bfcef))

## UV Editor

  - Add Invert Pins operator.
