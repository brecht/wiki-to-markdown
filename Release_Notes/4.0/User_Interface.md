# User Interface

## Search

  - All regular dropdown and context menus can be searched by pressing
    spacebar.
    ([35d3d52508](https://projects.blender.org/blender/blender/commit/35d3d52508)).
  - Add menus can be searched by immediate typing.
    ([b688414223](https://projects.blender.org/blender/blender/commit/b688414223)).
  - Recently searched items are now at the top of search lists, with an
    option in the Preferences for disabling it.
    ([8362563949](https://projects.blender.org/blender/blender/commit/8362563949)).

## Text

  - Default user interface font changed to Inter.
    ([f58f6d0338](https://projects.blender.org/blender/blender/commit/f58f6d0338)).

![../../images/Inter.png](../../images/Inter.png
"../../images/Inter.png")

  - Text output now more pleasing and typographically correct.
    ([a0b4ead737](https://projects.blender.org/blender/blender/commit/a0b4ead737))
  - New user preference added for "subpixel anti-aliasing" for text
    output.
    ([82bfc41d0c](https://projects.blender.org/blender/blender/commit/82bfc41d0c))
  - Chinese, Japanese, and Korean punctuation characters now recognized
    in word selection.
    ([6d64c6dcd7](https://projects.blender.org/blender/blender/commit/6d64c6dcd7))
  - Use multiplication symbol instead of letter "x" where appropriate.
    ([9b4749e7c7](https://projects.blender.org/blender/blender/commit/9b4749e7c7))

## Elements

  - Consistent top-down content ordering in menus. Menus no longer
    reverse order if opening upward.
    ([b122faf705](https://projects.blender.org/blender/blender/commit/b122faf705))
  - All rotational inputs now shown with maximum precision.
    ([b34ece48f8](https://projects.blender.org/blender/blender/commit/b34ece48f8))
  - Tree-view UIs draw hierarchy lines to visually communicate the
    nesting levels better.
    ([71273df2d5](https://projects.blender.org/blender/blender/commit/71273df2d5))

![../../images/Release\_notes\_4.0\_tree\_view\_hierarchy\_lines.png](../../images/Release_notes_4.0_tree_view_hierarchy_lines.png
"../../images/Release_notes_4.0_tree_view_hierarchy_lines.png")

  - Many lists now highlight the currently-selected item.
    ([9f4b28bba8](https://projects.blender.org/blender/blender/commit/9f4b28bba8)).
  - List items that are not multiple select are now shown as radio
    buttons.
    ([6dd3c90185](https://projects.blender.org/blender/blender/commit/6dd3c90185)).
  - Resizing the toolbar no longer breaks snapping when dragged beyond
    the maximum available size.
    ([248b322896](https://projects.blender.org/blender/blender/commit/248b322896))
  - Windows and Linux: Color Picker can now pick outside of Blender
    windows.
    ([5741a5d433](https://projects.blender.org/blender/blender/commit/5741a5d433),
    [e5a0d11c4e](https://projects.blender.org/blender/blender/commit/e5a0d11c4e))
  - The Color Picker size has been increased.
    ([0c7496f74d](https://projects.blender.org/blender/blender/commit/0c7496f74d))

![../../images/PickerSize.png](../../images/PickerSize.png
"../../images/PickerSize.png")

  - Improvements to Color Ramp drawing.
    ([8a3766e241](https://projects.blender.org/blender/blender/commit/8a3766e241))
  - Fix sidebar scrollbar overlapping category tabs when zooming.
    ([080a00bda2](https://projects.blender.org/blender/blender/commit/080a00bda2))
  - Fixed scrollbars being highlighted while outside of their region.
    ([4f6785774a](https://projects.blender.org/blender/blender/commit/4f6785774a))
  - Progress indicators now exposed to Python, including new ring
    version.
    ([c6adafd8ef](https://projects.blender.org/blender/blender/commit/c6adafd8ef)).

![../../images/UpdatingProgress.png](../../images/UpdatingProgress.png
"../../images/UpdatingProgress.png")

  - Allow transparency when editing text in widgets.
    ([2ec2e52a90](https://projects.blender.org/blender/blender/commit/2ec2e52a90))
  - UI element outlines have changeable opacity, allowing for flat or
    minimalistic themes.
    ([c033e434c5](https://projects.blender.org/blender/blender/commit/c033e434c5))

## Window

  - Window title improved order and now includes Blender version.
    ([636f3697ee](https://projects.blender.org/blender/blender/commit/636f3697ee)).
  - New Window now works while having an area maximized.
    ([bb31df1054](https://projects.blender.org/blender/blender/commit/bb31df1054)).
  - Save Incremental in File menu, to save the current .blend file with
    a numerically-incremented name.
    ([a58e5ccdec](https://projects.blender.org/blender/blender/commit/a58e5ccdec))
  - Small changes to the File and Edit menus.
    ([347e4692de](https://projects.blender.org/blender/blender/commit/347e4692de))

## Files and Asset Browsers

  - File Browser side bar now showing Bookmarks, System, Volumes,
    Recent.
    ([9659b2deda](https://projects.blender.org/blender/blender/commit/9659b2deda))
  - File and Asset Browser show a wait icon while previews are loading
    ([4a3b6bfeac](https://projects.blender.org/blender/blender/commit/4a3b6bfeac))
  - Thumbnail views now allow any preview size from 16-256.
    ([fa32379def](https://projects.blender.org/blender/blender/commit/fa32379def))
  - Image previews in File Browser now have checkerboard background if
    transparent.
    ([e9e12015ea](https://projects.blender.org/blender/blender/commit/e9e12015ea))
  - File Browser now shows thumbnail previews for SVG images.
    ([565436bf5f](https://projects.blender.org/blender/blender/commit/565436bf5f))

<center>

|                                                                                                                             |                                                                                                    |
| --------------------------------------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------------- |
| ![../../images/Transparent\_Thumbnail.png](../../images/Transparent_Thumbnail.png "../../images/Transparent_Thumbnail.png") | ![../../images/SVGThumbnails.png](../../images/SVGThumbnails.png "../../images/SVGThumbnails.png") |

style="caption-side: bottom" | Transparent and SVG thumbnails.

</center>

## Splash

Splash screen changes to make it easier for users to bring previously
saved settings into a new install.
([13f5879e3c](https://projects.blender.org/blender/blender/commit/13f5879e3c))

![../../images/SplashScreen.png](../../images/SplashScreen.png
"../../images/SplashScreen.png")

## Text Editor

  - Support auto-closing brackets around the selection
    ([96339fc313](https://projects.blender.org/blender/blender/commit/96339fc3131789e6caa41f7ef01cc1edf5a38a28))
  - Removed syntax highlighting support for LUA
    ([95ca04dc20](https://projects.blender.org/blender/blender/commit/95ca04dc207f8bbbdb18287223ba8fdabdd54511))

## Python Console

  - Support cursor motion, selection & more text editing operations
    ([18e07098ef](https://projects.blender.org/blender/blender/commit/18e07098ef25e7659e0c14e169a96b49c5a54cbf))
  - Support tab-stops instead of expanding to spaces
    ([a5cd497543](https://projects.blender.org/blender/blender/commit/a5cd4975433db6ab28f13c49d14a071f3ca8d2c2))

## Other Editors

  - Preferences: layout and style tweaks
    ([d0aa521ea8](https://projects.blender.org/blender/blender/commit/d0aa521ea8))
  - Timeline: Invalid caches now show with striped lines.
    ([8d15783a7e](https://projects.blender.org/blender/blender/commit/8d15783a7e)).
  - Outliner: Drag & Drop now works between multiple windows
    ([d102536b1a](https://projects.blender.org/blender/blender/commit/d102536b1a)).
  - Outliner: Select Hierarchy now works with multiple selected objects
    ([594dceda7f](https://projects.blender.org/blender/blender/commit/594dceda7f)).
  - Top Bar and Status Bar colors now exactly match their theme colors
    ([d86d2a41e0](https://projects.blender.org/blender/blender/commit/d86d2a41e0)).

## Windows Integration

  - Blend File association can now be done for all users or just the
    current users in the preferences. Unassociate is now available as
    well. Support for side-by-side installations was improved.
    ([9cf77efaa0](https://projects.blender.org/blender/blender/commit/9cf77efaa0))
  - Recent file lists are now per Blender version.
  - Pinning a running Blender to the taskbar will properly pin the
    launcher.
  - Multiple Blender version are available for Open With in Windows
    Explorer.
  - Explorer "Quick Access" items added to File Browser System List
    ([f1e7fe5492](https://projects.blender.org/blender/blender/commit/f1e7fe5492)).

![../../images/NewRegister1.png](../../images/NewRegister1.png
"../../images/NewRegister1.png")
![../../images/NewRegister2.png](../../images/NewRegister2.png
"../../images/NewRegister2.png")

## Translations

The Catalan language is now one of six languages with **complete**
coverage. ![../../images/Catalan.png](../../images/Catalan.png
"../../images/Catalan.png")
