# Asset Bundles

## Human Base Meshes

The Human Base Meshes bundle got an update with new models, improved
topology, and various fixes. Download it from the [Demo Files
page](https://www.blender.org/download/demo-files/).

![../../images/Base\_meshes\_update.png](../../images/Base_meshes_update.png
"../../images/Base_meshes_update.png")

### New Assets

  - **Planar Head**
      - For reference or as a sculpting base
      - Generic base topology for flexible deformation

<!-- end list -->

  - **Generic Head Topology**
      - Generic base topology for flexible deformation and
        sculpting/modeling base
      - Includes various shape keys for various example head shapes

<!-- end list -->

  - **Various Primitive Assets**
      - For quick start into new character design sculpts
      - Stylized and realistic assets
      - Body and head assets
      - Separated into multiple objects with parenting for fast
        non-destructive object posing
      - Linked object data between left and right side objects.
      - Includes subdiv modifier

<!-- end list -->

  - **Replaced Realistic Male Body**
      - Based on scan data for increased realism

### Fixes

  - Tooltips now are now added via the asset description
  - Improved eye scale on realistic assets
  - Various small fixes and improvements to existing assets
