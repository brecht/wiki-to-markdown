# Compositor & Sequencer

## Compositor

### Viewport Nodes

The following nodes are now supported in the [Viewport
Compositor](https://docs.blender.org/manual/en/4.0/compositing/realtime_compositor.html):

  - [Movie
    Distortion](https://docs.blender.org/manual/en/4.0/compositing/types/transform/movie_distortion.html)
  - [Sun
    Beams](https://docs.blender.org/manual/en/4.0/compositing/types/filter/sun_beams.html)
  - [Keying](https://docs.blender.org/manual/en/4.0/compositing/types/keying/keying.html)
  - [Kuwahara](https://docs.blender.org/manual/en/4.0/compositing/types/filter/kuwahara.html)
    (classic and anisotropic)
  - [Inpaint](https://docs.blender.org/manual/en/4.0/compositing/types/filter/inpaint.html)
  - [Double Edge
    Mask](https://docs.blender.org/manual/en/4.0/compositing/types/mask/double_edge_mask.html)

### Kuwahara Filter

Kuwahara filter node for the compositor, to give images a painterly
look.
([f3cb157452](https://projects.blender.org/blender/blender/commit/f3cb157452b8af6782bac88d34946e929a4fa6f8))

![Multiple iterations of Anisotropic Kuwahara
filter](../../images/Anisotropic_Kuwahara_Filter_Example.png
"Multiple iterations of Anisotropic Kuwahara filter")

### User Interface

  - The compositor nodes add menu has been re-organized for consistency
    with other editors.
    ([75919610b4](https://projects.blender.org/blender/blender/commit/75919610b40e6f6c6eea8246185fc565adbc950a))
  - Node preview images have been moved to an overlay displayed above
    the node. The overlays menu has an option to show and hide all
    previews ([PR
    \#108001](https://projects.blender.org/blender/blender/pulls/108001)).

![../../images/Blender4.0\_node\_previews.png](../../images/Blender4.0_node_previews.png
"../../images/Blender4.0_node_previews.png")

## Sequencer

### Interactive Retiming

A new system for interactively retiming strips was added in the
sequencer.
([e1f6587f12](https://projects.blender.org/blender/blender/commit/e1f6587f12f127b9a59fa35d6e5977fda2449a2b),
[4dc026ec8e](https://projects.blender.org/blender/blender/commit/4dc026ec8e91c21d9b53c7d2f8bc0f3c56cc586f)),
[6d215b87ce](https://projects.blender.org/blender/blender/commit/6d215b87ce5d1c524c17a06535b6c324a9366945),
[f4d6edd476](https://projects.blender.org/blender/blender/commit/f4d6edd4764a780cf673a2007b45a7de871fc6f1)),
[86a0d0015a](https://projects.blender.org/blender/blender/commit/86a0d0015ac7bb114cbc44a03dd6b422a5ac709f))

Adjust the speed of strips by dragging retiming keys. Additional keys
can be added by pressing the I key. The keys can be edited by pressing
the Ctrl + R shortcut.

Retiming also supports smoothly transitioning from one speed to another.
Transitions can be created by right clicking on a key, or from retiming
menu and choosing Add Speed Transition. Freeze frames can be added
similar to transition keys.

The speed of whole strips or a retimed segment can be set by using Set
Speed operator from retiming menu.

Retiming keys can be hidden by turning off the retiming overlay.

### Sound Strips

  - Sound strip waveforms are now processed much faster.
    ([b11540a65f](https://projects.blender.org/blender/blender/commit/b11540a65f))
  - New equalizer modifier for sound strips.
    ([1015bed2fd](https://projects.blender.org/blender/blender/commit/1015bed2fd))

## Breaking Changes

  - The Depth socket was removed from the non-multilayer image node,
    viewer, and composite output nodes. This was a legacy feature,
    replaced by more general multilayer OpenEXR output.
    ([e1b60fdb91](https://projects.blender.org/blender/blender/commit/e1b60fdb913ebe60a57e2459f0d1fbc1f921e643))
  - The "Distort" socket was renamed to "Distortion" in the Lens
    Distortion compositing node.
    ([fe62eebba4](https://projects.blender.org/blender/blender/commit/fe62eebba45ac865179f29993529438d3240b439))
