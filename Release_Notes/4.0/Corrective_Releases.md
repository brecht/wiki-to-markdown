# Blender 4.0.2

Released on December 5th 2023, Blender 4.0.2 features 41 bug fixes:

  - Adjust Real Snow addon to Principled BSDF changes
    [\#105040](https://projects.blender.org/blender/blender-addons/issues/105040)
  - Blender 4.0 crashes when trying to render scene strips
    [\#114982](https://projects.blender.org/blender/blender/issues/114982)
  - Boolean behaves as if the object is on it's origin
    [\#115715](https://projects.blender.org/blender/blender/issues/115715)
  - BSurfaces: 'ToolSettings' object has no attribute
    'use\_snap\_project'
    [\#105026](https://projects.blender.org/blender/blender-addons/issues/105026)
  - Compose key sequences includes continuation keys on Wayland
    [\#114609](https://projects.blender.org/blender/blender/issues/114609)
  - Crash deleting tool node group from outliner
    [\#115037](https://projects.blender.org/blender/blender/issues/115037)
  - Crash opening files with particle boid goal objects
    [\#115471](https://projects.blender.org/blender/blender/issues/115471)
  - Crash undoing painting on linked image from 3DView
    [\#115140](https://projects.blender.org/blender/blender/issues/115140)
  - Crash when creating transition from last reiming key
    [\#115007](https://projects.blender.org/blender/blender/issues/115007)
  - Crash when using snap to volume and clipping region
    [\#115570](https://projects.blender.org/blender/blender/issues/115570)
  - Cycles: Avoid long wait times of compile-gpu step
    [fe904e9ff9](https://projects.blender.org/blender/blender/commit/fe904e9ff9)
  - Draw: Buffers never shrink
    [\#114742](https://projects.blender.org/blender/blender/issues/114742)
  - Edge Crease Doesn't Transfer
    [\#115105](https://projects.blender.org/blender/blender/issues/115105)
  - Error when trying to create texture with VDM Brush Baker
    [\#105028](https://projects.blender.org/blender/blender-addons/issues/105028)
  - FBX IO: Fix import of shape key animations without any keyframes
    [db7a31b51f](https://projects.blender.org/blender/blender-addons/commit/db7a31b51f)
  - File selector shrinks on each display under
    KDE[\#113059](https://projects.blender.org/blender/blender/issues/113059)
  - Fix "make doc\_man" not working
    [175991df4f](https://projects.blender.org/blender/blender/commit/175991df4f)
  - Fix Blender 4.01 crash "EXCEPTION\_ACCESS\_VIOLATION"
    [\#115144](https://projects.blender.org/blender/blender/issues/115144)
  - Fix build error on architectures without SSE or sse2neon
    [641b7808f2](https://projects.blender.org/blender/blender/commit/641b7808f2)
  - Fix rare crashes when reading files with multi-levels of libraries.
    [6f625cedf1](https://projects.blender.org/blender/blender/commit/6f625cedf1)
  - Fix shape\_key\_remove operator leaves shared normals
    [\#115572](https://projects.blender.org/blender/blender/issues/115572)
  - Fix VSE crash when editing retiming
    [4d0de9022f](https://projects.blender.org/blender/blender/commit/4d0de9022f)
  - FModifier.type enum 'NULL' item was renamed to 'nullptr'
    [\#115279](https://projects.blender.org/blender/blender/issues/115279)
  - ID property's id\_type enum is missing the SCREEN type
    [\#115151](https://projects.blender.org/blender/blender/issues/115151)
  - Image Editor - Fill Tool doesn't work as expected
    [\#114963](https://projects.blender.org/blender/blender/issues/114963)
  - Increase thread stack size for musl libc
    [a33d2ce7bf](https://projects.blender.org/blender/blender/commit/a33d2ce7bf)
  - Keyboard layout sometimes ignored on Wayland
    [\#115160](https://projects.blender.org/blender/blender/issues/115160)
  - Man page generation fails with Python error
    [90d4364b35](https://projects.blender.org/blender/blender/commit/90d4364b35)
  - Man-page fails to build with non-portable
    install[\#115056](https://projects.blender.org/blender/blender/issues/115056)
  - Missing normals on first chunk of array modifier
    [\#115526](https://projects.blender.org/blender/blender/issues/115526)
  - OpenGL: Mark Legacy Intel Drivers Unsupported
    [\#115228](https://projects.blender.org/blender/blender/issues/115228)
  - PyAPI: call 'sys.excepthook' for text editor exceptions
    [\#115090](https://projects.blender.org/blender/blender/issues/115090)
  - RMB Select sometimes to enter pose mode when selected
    [\#115181](https://projects.blender.org/blender/blender/issues/115181)
  - Scrollbars for template\_lists in popups update issue
    [\#115363](https://projects.blender.org/blender/blender/issues/115363)
  - Sheen renders incorrectly when viewed head on
    [\#115206](https://projects.blender.org/blender/blender/issues/115206)
  - Snap Base in Camera View crashes Blender
    [\#115153](https://projects.blender.org/blender/blender/issues/115153)
  - Snap to face nearest failing with transformed objects
    [\#114596](https://projects.blender.org/blender/blender/issues/114596)
  - Transform operations not working in particle edit mode
    [\#115025](https://projects.blender.org/blender/blender/issues/115025)
  - VSE offset drawing not working
    [6d324dbaec](https://projects.blender.org/blender/blender/commit/6d324dbaec)
  - Workbench: Negative scaled sculpt mesh artifacts
    [\#114918](https://projects.blender.org/blender/blender/issues/114918)
  - Zero-sized curve leads to OptiX error
    [\#113325](https://projects.blender.org/blender/blender/issues/113325)

# Blender 4.0.1

Released on November 17th 2023, Blender 4.0.1 features 11 bug fixes:

  - Crash opening old files with text objects
    [\#114892](https://projects.blender.org/blender/blender/issues/114892)
  - Crash when adding specular texture slot in texture paint mode
    [\#114848](https://projects.blender.org/blender/blender/issues/114848)
  - Cycles: MetalRT compilation errors on base M3 MacBook Pros with
    factory installed OS
    [\#114919](https://projects.blender.org/blender/blender/issues/114919)
  - glTF: Fix regression in image quality option
    [e5ad2e2c16](https://projects.blender.org/blender/blender-addons/commit/e5ad2e2c16)
  - Incorrect display of Rec.1886 and Rec. 2020 view transforms
    [\#114661](https://projects.blender.org/blender/blender/issues/114661)
  - Node Wrangler: Principled Textures Setup emissive texture
    [\#104999](https://projects.blender.org/blender/blender-addons/issues/104999)
  - Sculpt Multires drawing broken with no mask
    [\#114841](https://projects.blender.org/blender/blender/issues/114841)
  - Sequencer: bpy.ops.sequencer.split ignores passed frame
    [\#114891](https://projects.blender.org/blender/blender/issues/114891)
  - UI: Keep text field empty for global search
    [\#114758](https://projects.blender.org/blender/blender/issues/114758)
  - Windows: Crash Loading Win32 Quick Access
    [\#114855](https://projects.blender.org/blender/blender/issues/114855)
  - Windows: Invalid reuse of shell\_link
    [\#114906](https://projects.blender.org/blender/blender/issues/114906)
