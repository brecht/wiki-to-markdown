# Import & Export

## USD

  - Skeleton and blend shape animation import through UsdSkel. ([PR
    \#110912](https://projects.blender.org/blender/blender/pulls/110912))
  - Generic mesh attribute import and export for meshes. ([PR
    \#109518](https://projects.blender.org/blender/blender/pulls/109518))
  - Light import and export improvements to follow USD conventions more
    closely. ([PR
    \#109795](https://projects.blender.org/blender/blender/pulls/109795))
  - Camera import and export improvements. ([PR
    \#112905](https://projects.blender.org/blender/blender/pulls/112905))
  - Export can now be extended with Python scripts through hooks. See
    the \`USDHook\` [API
    documentation](https://docs.blender.org/api/4.0/bpy.types.USDHook.html)
    for details and example code. ([PR
    \#108823](https://projects.blender.org/blender/blender/pulls/108823))

## 3DS

#### Export Animation

  - The 3ds exporter now got the property to export animation keyframes.
    ([PR
    \#104613](https://projects.blender.org/blender/blender-addons/pulls/104613))
  - Keyframe export is optional to avoid crashing any importers wich can
    not handle the keyframe section.
    ([585a682ef8](https://projects.blender.org/blender/blender-addons/commit/585a682ef8),
    [da9bb97bea](https://projects.blender.org/blender/blender-addons/commit/da9bb97bea))

#### Export Hierarchy

  - 3ds objects now got hierarchy chunks to preserve the object tree.
    ([PR
    \#104682](https://projects.blender.org/blender/blender-addons/pulls/104682))
  - The object tree can now be exported without the keyframe section
    needed.
    ([0d26a2a017](https://projects.blender.org/blender/blender-addons/commit/0d26a2a017))

#### Export Scale

  - Global scale for exporting a 3ds can now be setted. ([PR
    \#104767](https://projects.blender.org/blender/blender-addons/pulls/104767))
  - Since many applications are using millimeters, this option is useful
    to convert the measure units.
    ([17f142e55d](https://projects.blender.org/blender/blender-addons/commit/17f142e55d))

#### Convert Measure

  - The 3ds importer and exporter now got the ability to converts the
    unit measure for all metric and imperial units. ([PR
    \#104769](https://projects.blender.org/blender/blender-addons/pulls/104769))
  - Many 3ds files were exported in millimeter unit scale, this option
    converts too big meshes to Blender meter scale.
    ([675987e938](https://projects.blender.org/blender/blender-addons/commit/675987e938))

#### Object Filter

  - Both, the importer and the exporter got the new option to filter
    objects for import and export. ([PR
    \#104778](https://projects.blender.org/blender/blender-addons/pulls/104778),
    [PR
    \#104782](https://projects.blender.org/blender/blender-addons/pulls/104782))

#### Volume, fog, gradient and background bitmap

  - Atmosphere settings, layerfog parameter and background image can now
    be imported and exported. ([PR
    \#104795](https://projects.blender.org/blender/blender-addons/pulls/104795),
    [6fad9e9725](https://projects.blender.org/blender/blender-addons/commit/6fad9e9725))
  - Gradient and fog chunks can now be imported and exported. ([PR
    \#104811](https://projects.blender.org/blender/blender-addons/pulls/104811),
    [a53788722b](https://projects.blender.org/blender/blender-addons/commit/a53788722b),
    [7cd6969b3e](https://projects.blender.org/blender/blender-addons/commit/7cd6969b3e))

#### Spotlight aspect and projector

  - If a spotlight includes a gobo image, it will be exported in the
    light bitmap chunk and imported the same way. ([PR
    \#104813](https://projects.blender.org/blender/blender-addons/pulls/104813),
    [f1e443f119](https://projects.blender.org/blender/blender-addons/commit/f1e443f119))
  - The x/y scale of a spotlight can now be exported in the aspect ratio
    chunk, the importer calculates the aspect back to x/y scale. ([PR
    \#104815](https://projects.blender.org/blender/blender-addons/pulls/104815),
    [7e2f96796a](https://projects.blender.org/blender/blender-addons/commit/7e2f96796a))

#### Cursor location

  - The 3D cursor can now saved to a 3ds file and imported again. ([PR
    \#104797](https://projects.blender.org/blender/blender-addons/pulls/104797),
    [75ea7633f6](https://projects.blender.org/blender/blender-addons/commit/75ea7633f6))

#### New Principled BSDF material support

  - Moved specular texture to specular tint. ([PR
    \#104918](https://projects.blender.org/blender/blender-addons/pulls/104918),
    [fef728a568](https://projects.blender.org/blender/blender-addons/commit/fef728a568))
  - Added transmission and coat weight parameters.
    ([a5d3364c33](https://projects.blender.org/blender/blender-addons/commit/a5d3364c33),
    [d82ce1770f](https://projects.blender.org/blender/blender-addons/commit/d82ce1770f),
    [9d57b190b8](https://projects.blender.org/blender/blender-addons/commit/9d57b190b8),
    [8f6d8d9fc9](https://projects.blender.org/blender/blender-addons/commit/8f6d8d9fc9))
  - Added tint colors for coat and sheen.
    ([ec069c3b6a](https://projects.blender.org/blender/blender-addons/commit/ec069c3b6a))

## FBX

  - FBX binary file reading and parsing speed has been improved.
    ([890e51e769](https://projects.blender.org/blender/blender-addons/commit/890e51e769),
    [a600aeb2e2](https://projects.blender.org/blender/blender-addons/commit/a600aeb2e2))
  - Animation import performance was made faster.
    ([abab1c9343](https://projects.blender.org/blender/blender-addons/commit/abab1c93437322bbf882079a43323e353a5c5b5f),
    [e7b1962893](https://projects.blender.org/blender/blender-addons/commit/e7b19628938d57498b5d9c23bef2bd14610f41e7))
  - Animation export performance was made faster.
    ([73c65b9a44](https://projects.blender.org/blender/blender-addons/commit/73c65b9a44ff6a5182d433bc991359a7a2a43676),
    [461e0c3e1e](https://projects.blender.org/blender/blender-addons/commit/461e0c3e1e0a8748106189fde3c7daf61c9fb46b))
  - Vertex Group export performance was made faster.
    ([63d4898e1d](https://projects.blender.org/blender/blender-addons/commit/63d4898e1d9ebf59abd86e7b4b02ce29b273dc7f))
  - Armature data custom properties can now be imported and exported.
    ([2b8d9bf2b8](https://projects.blender.org/blender/blender-addons/commit/2b8d9bf2b8f229adb150fdbcfd901fe6fad3c43d)).
  - Shape keys can now be exported when Triangulate Faces is enabled or
    when Objects have Object-linked materials.
    ([bc801d7b1d](https://projects.blender.org/blender/blender-addons/commit/bc801d7b1dad4be446435e0cab4d3a80e6bb1d04))

## OBJ and PLY

The OBJ and PLY I/O add-ons have been removed. Importing and exporting
this format is now implemented natively in Blender, with significantly
better performance.
([\#104504](https://projects.blender.org/blender/blender-addons/issues/104504),
[67a4aab0da](https://projects.blender.org/blender/blender-addons/commit/67a4aab0da2f3),
[\#104503](https://projects.blender.org/blender/blender-addons/issues/104503),
[67a4aab0da](https://projects.blender.org/blender/blender-addons/commit/67a4aab0da2f3)).

## Collada

  - Armatures exported to Collada now include the armature's bone
    collections, including their visibility
    ([83306754d4](https://projects.blender.org/blender/blender/commit/83306754d48)).
    More info about [bone
    collections](Animation_Rigging/index.md#Bone_Collections).

## glTF 2.0

Find the changelog in the
[Add-ons](Add-ons.md) section.
