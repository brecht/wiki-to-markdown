# Node Editor

## Node Panels

  - **Node panels** feature for node groups and built-in nodes
    ([\#108895](https://projects.blender.org/blender/blender/issues/108895))
      - Panels can be used to group sockets in a collapsible section.
        They can be added to node groups and to built-in nodes.
  - Group sockets are managed in new **UI tree view** together with
    panels. Drag-and-drop support for ordering sockets and inserting
    into panels.
  - Node group API has breaking changes, see [Python
    API](Python_API.md) for details
    and migration help.

![../../images/Node\_panels\_principled\_bsdf.png](../../images/Node_panels_principled_bsdf.png
"../../images/Node_panels_principled_bsdf.png")

## Editing

  - The "Make Group" and "Ungroup" operators are no longer available
    from the add menu.
    ([7c2dc5183d](https://projects.blender.org/blender/blender/commit/7c2dc5183d03d867c100b29059fca83ba9dd8451))
  - Node group assets unassigned to any node group are displayed in a
    separate "No Catalog" add menu submenu.
    ([d2d4de8c71](https://projects.blender.org/blender/blender/commit/d2d4de8c710c4e8e648a86f904e6436133664836))

## Other

  - In Preferences → Editing, a new panel was added to group all Node
    Editor specific settings
    ([eb57163f](https://projects.blender.org/blender/blender/commit/eb57163f)).
  - Improvements to Node links to help readability at different zooms
    and monitor DPI
    ([899d723da8](https://projects.blender.org/blender/blender/commit/899d723da8)).
  - Node snapping grid no longer changes size with changes to line width
    ([04285c2d0e](https://projects.blender.org/blender/blender/commit/04285c2d0e)).
