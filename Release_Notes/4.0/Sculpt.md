# Sculpt, Paint, Texture

## Enhancements

  - Vertex Paint: Add set vertex colors option to lock alpha ([PR
    \#111002](https://projects.blender.org/blender/blender/pulls/111002))
  - Sculpt: Remove dynamic topology "Smooth Shading" option ([PR
    \#110548](https://projects.blender.org/blender/blender/pulls/110548))
  - Sculpt: Expand Mask inverted fill ([PR
    \#111665](https://projects.blender.org/blender/blender/pulls/111665))
  - Weight Paint: Allow setting weights (Ctrl+X) without paintmask
    enabled
    ([0b1d2d63fe](https://projects.blender.org/blender/blender/commit/0b1d2d63fe))
