# Translation

<div class="bd-lead">

Information about the internationalization of Blender's UI.

</div>

  - [Process/Translate\_Blender](Process/Translate_Blender/index.md)
  - [Source/Interface/Internationalization](Source/Interface/Internationalization.md)

The Blender user manual translation is documented on [the manual
itself](https://docs.blender.org/manual/en/dev/about/contribute/index.html#translations).
