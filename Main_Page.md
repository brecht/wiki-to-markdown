\_\_NOTOC\_\_

<div class="card-deck">

<div class="card bg-light mb-3">

<div class="card-body">

<h4 class="card-title">

[New Developer Introduction](Developer_Intro/index.md)

</h4>

<h6 class="card-subtitle mb-2 text-muted">

Welcome\! Advice on how to get started.

</h6>

</div>

</div>

<div class="card bg-light mb-3">

<div class="card-body">

<h4 class="card-title">

[Communication](Contact)

</h4>

<h6 class="card-subtitle mb-2 text-muted">

The most important thing.

</h6>

</div>

</div>

<div class="card bg-light mb-3">

<div class="card-body">

<h4 class="card-title">

[Code & Design Documentation](Source/index.md)

</h4>

<h6 class="card-subtitle mb-2 text-muted">

Technical documentation about the code and big picture design.

</h6>

</div>

</div>

</div>

<div class="card-deck">

<div class="card bg-light mb-3">

<div class="card-body">

<h4 class="card-title">

[Building Blender](Building_Blender/index.md)

</h4>

<h6 class="card-subtitle mb-2 text-muted">

Instructions for compiling Blender locally.

</h6>

</div>

</div>

<div class="card bg-light mb-3">

<div class="card-body">

<h4 class="card-title">

[Modules](Modules/index.md)

</h4>

<h6 class="card-subtitle mb-2 text-muted">

Blender components and their owners.

</h6>

</div>

</div>

<div class="card bg-light mb-3">

<div class="card-body">

<h4 class="card-title">

[Style Guide](Style_Guide/index.md)

</h4>

<h6 class="card-subtitle mb-2 text-muted">

Coding guidelines and committer etiquette.

</h6>

</div>

</div>

</div>

<div class="card-deck">

<div class="card bg-light mb-3">

<div class="card-body">

<h4 class="card-title">

[Tools](Tools/index.md)

</h4>

<h6 class="card-subtitle mb-2 text-muted">

Setup your development environment.

</h6>

</div>

</div>

<div class="card bg-light mb-3">

<div class="card-body">

<h4 class="card-title">

[Process](Process/index.md)

</h4>

<h6 class="card-subtitle mb-2 text-muted">

Release cycle, BugTracker, Code Reviews and Testing.

</h6>

</div>

</div>

<div class="card bg-light mb-3">

<div class="card-body">

<h4 class="card-title">

[Release Notes](Release_Notes/index.md)

</h4>

<h6 class="card-subtitle mb-2 text-muted">

What changed in each Blender version.

</h6>

</div>

</div>

</div>

<div class="card-deck">

<div class="card bg-light mb-3">

<div class="card-body">

<h4 class="card-title">

[Google Summer of Code](GSoC/index.md)

</h4>

<h6 class="card-subtitle mb-2 text-muted">

A program that introduces students to open source software development.

</h6>

</div>

</div>

<div class="card bg-light mb-3">

<div class="card-body">

<h4 class="card-title">

[Python](Python.md)

</h4>

<h6 class="card-subtitle mb-2 text-muted">

Learn about scripting and add-ons.

</h6>

</div>

</div>

<div class="card bg-light mb-3">

<div class="card-body">

<h4 class="card-title">

[Translation](Translation.md)

</h4>

<h6 class="card-subtitle mb-2 text-muted">

Blender UI internationalization

</h6>

</div>

</div>

</div>

<div class="card-deck">

<div class="card bg-light mb-3">

<div class="card-body">

<h4 class="card-title">

[Infrastructure](Infrastructure/index.md)

</h4>

<h6 class="card-subtitle mb-2 text-muted">

Details about the online ecosystem that supports Blender development.

</h6>

</div>

</div>

<div class="card bg-light mb-3">

<div class="card-body">

<h4 class="card-title">

[FAQ](FAQ.md)

</h4>

<h6 class="card-subtitle mb-2 text-muted">

Common questions about the development process.

</h6>

</div>

</div>

</div>
