# Blender Human Interface Guidelines

The Blender Human Interface Guidelines (HIG) aim to provide a clear path
towards understanding Blender's user interaction schemes and how they
apply to its variety of workflows.  
They should be a practical resource for contributors, including Add-on
authors, designers and core developers.

Please respect that the HIG is rather extensive and that we can't
reasonably expect contributors to know all of it.

<table>
<tbody>
<tr class="odd">
<td><div class="note_title">
<p><strong>Work in Progress</strong></p>
</div>
<div class="note_content">
<p>The Blender Human Interface Guidelines are still in development. They are unpolished and a lot of content is missing still.</p>
</div></td>
</tr>
</tbody>
</table>

**Subpages**:
[:Special:PrefixIndex/API/](:Special:PrefixIndex/API/)

<table>
<tbody>
<tr class="odd">
<td><div class="note_title">
<p><strong>Proposal for restructuring</strong></p>
</div>
<div class="note_content">
<p>The current page structure is temporary, see <a href="http://developer.blender.org/T97989">T97989</a>.</p>
</div></td>
</tr>
</tbody>
</table>

## TODO

Here are the things we should address before sharing the HIG more
widely:

  - Initial vision statement.
  - Add more contents to
    [Layouts](Layouts.md) page.
  - Add/improve contents on
    [Process](Process.md) page.
  - Update [Sidebar
    Tabs](Sidebar_Tabs.md) for 2.8
    changes.
  - Clearly mark which pages are "approved".
  - Make things prettier :) (mostly through pictures).
