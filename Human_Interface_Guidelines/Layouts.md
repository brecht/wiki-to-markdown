# Layouts

## Property Split

When to use it, why it makes sense...

## Order of importance

![../images/Order\_of\_importance.png](../images/Order_of_importance.png
"../images/Order_of_importance.png") In general, the most important and
most commonly used widgets should be exposed more accessibly. Lesser
used widgets should be placed below, or in sub-panels where applicable.
![../images/Checkbox\_enablers.png](../images/Checkbox_enablers.png
"../images/Checkbox_enablers.png") In this situation, every setting in
the panel depends on the enabling property, so it always accessible in
the panel's header.

## Enums

Enums should be represented as dropdown if:

  - It contains more than 2-3 items.
  - Its labels do not fit the default editor size on supported screen
    resolutions.

Exceptions for this rule apply when e.g. quick toggling is important for
a workflow, or if the enum can be represented as icon-only (e.g.
viewport shading modes).

## Mode toggling buttons

![../images/Mode\_toggling.png](../images/Mode_toggling.png
"../images/Mode_toggling.png") An enum should be expanded to full width
at the top of a panel if it meets the following criteria:

1.  The property plays a significant role in defining the functionality
    of the panel.
2.  The name of the property can be inferred from the panel's label. For
    example, "mode" or "type" properties fit well.
3.  The text of the choices fits in default width of the UI area for
    supported screen resolutions.

## Headings

Related properties should be grouped with headings where possible.
Headings reduce the amount of repetition needed in UI text, meaning it's
easier and faster to scan to find what you're looking for.
![../images/Checkbox\_headings.png](../images/Checkbox_headings.png
"../images/Checkbox_headings.png") This is especially applicable for
widgets that contain text, like check-boxes.

## Sub-panels

When a label would help give context to multiple buttons, it often makes
sense to organize them in a subpanel.
![../images/Sub-panels.png](../images/Sub-panels.png
"../images/Sub-panels.png") The use of subpanels is generally preferred
over a single label button in a row above a block of buttons.

The subpanel title takes only a little more space than a regular label
button, it helps organization more, and allows collapsing.

One the other hand, instead of a subpanel with a checkbox in its header
and one button inside, it's usually better to align them on the same row
with a header.
![../images/Checkbox\_value.png](../images/Checkbox_value.png
"../images/Checkbox_value.png")

## Decorators

A quick and visual way to animate properties that also provides a visual
representation of whether a property is animate-able and its animation
state...

## Boxes

When to use a box instead of a subpanel... Box style panels...

## Lists

When to use, when to avoid...

Is the same data already represented somewhere else? For instance,
collections provide a more integrated way to deal with lists of
objects...

## Pie Menus

**Pie menus should generally be limited to 8 items.** Sometimes it's
hard to avoid more items. If in such cases the quick gesture based
selection is an significant time saver (compared to regular menus), a
pie menu may still be acceptable.

**Pie menus should not contain menus, popovers, radio buttons, etc.**
Besides displaying operators, pie menus can be an alternative way to
display an enumeration, or to toggle simple options. But avoid complex
interactions. The idea of pies is that you can use them with quick
gestures.

**The same item should always appear at the same position.** Don't
confuse users by having items be in different places based on context.
Muscle memory is key to an efficient usage of pie menus.

## Things to Avoid

![**Do *not* do this**: Spacial layouts to communicate
meaning.](../images/Spacial_Layout_\(avoid_this!\).png
"Do not do this: Spacial layouts to communicate meaning.")

**Do not use "fancy" spacial layouts to communicate meaning.** While
such layouts arguably help users understand the purpose of options, they
just don't integrate well with surrounding UI patterns. It's not just a
visual, but also a practical issue (for example, it needs special
handling for properties searching). The Blender layout and widget system
isn't designed for such cases and doesn't handle them too well, not
living up to our quality demands.  
If things can be improved so that such layouts become more appealing and
practical, they *could* become a common pattern.

  
**Do not use complex pie menus**. As mentioned above, pie menus should
be limited to 8 items. For add-ons it's common to have more complex
pies, that is fine. But not for pie menus implemented as core user
interfaces.
