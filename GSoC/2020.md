# Google Summer of Code - 2020 Projects

<div class="bd-lead">

Progress updates for 2020

</div>

## Current State

'''1<sup>st</sup> evaluation - *' 29 June - 3 July  
*'2<sup>nd</sup> evaluation - *' 27 - 31 July  
*'Final submission - ''' 24 - 31 August

## Communication

### Development Forum

All Blender and code related topics will go to the devtalk forum. Here
students will also post their weekly reports, and can ask for feedback
on topics or share intermediate results with everyone.

<https://devtalk.blender.org/c/blender/summer-of-code>

### blender.chat

For real-time discussions between students and mentors. Weekly meetings
will be scheduled here as well.

<https://blender.chat>

## Projects

Google has granted 10 projects for the Blender Foundation.

-----

### Continued Improvements to the Outliner

by **Nathan Craddock**  
*Mentors:* William Reynish, Dalai Felinto, Julian Eisel

  - [Proposal](../User/Zachman/GSoC2020/Proposal.md)
  - [Discussion](https://devtalk.blender.org/t/gsoc-2020-outliner-discussion-and-suggestions/13178)
  - [Design Task](https://developer.blender.org/T77408)
  - [Weekly
    Reports](https://devtalk.blender.org/t/gsoc-2020-outliner-weekly-reports/13710)
  - [Final Report](../User/Zachman/GSoC2020/Report.md)

-----

### Customizable Menus

by **Alexia Legrand**  
*Mentors:* Campbell Barton, Dalai Felinto

  - [Proposal](../User/Tempo/GSoC2020/Proposal.md)
  - [Weekly
    Report](https://devtalk.blender.org/t/gsoc-2020-custom-menus-weekly-reports/13721?u=tempo)
  - [Final Report](../User/Tempo/GSoC2020/Final_Report.md)

-----

### Editing Grease Pencil Strokes Using Curves

by **Falk David**  
*Mentors:* Antonio Vazquez, Matias Mendiola

  - [Proposal](../User/Filedescriptor/GSoC_2020_editing_grease_pencil_strokes_using_curves.md)
  - [Weekly
    Report](https://devtalk.blender.org/t/gsoc-2020-editing-grease-pencil-strokes-using-curves-weekly-reports/13618)
  - [Final Report](../User/Filedescriptor/GSoC_2020/Report.md)

-----

### Improving IO performance for big files

by **Ankit Meel**  
*Mentors:* Sybren Stüvel, Howard Trickey

  - [Proposal](User:Ankitm/GSoC_2020_Proposal_IO_Perf)
  - Discussion: [Design](https://developer.blender.org/T68936) and
    [Feedback](https://devtalk.blender.org/t/gsoc-2020-faster-io-for-obj-stl-ply/13528)
  - [Weekly Report](User:Ankitm/GSoC_2020_Weekly_Reports)
  - [Final Report](../User/Ankitm/GSoC_2020/Final_report.md)

-----

### Info Editor Improvements

by **Mateusz Grzeliński**  
*Mentors:* Bastien Montagne, William Reynish, Campbell Barton

  - [ Proposal](../User/Grzelins/GSoC2020/Proposal.md)
  - [Weekly
    Report](https://devtalk.blender.org/t/gsoc-2020-info-editor-improvements-weekly-reports/13659)
  - [Final Report](../User/Grzelins/GSoC2020/Report.md)

-----

### Interaction Support in VR through OpenXR Action System

by **Peter Klimenko**  
*Mentors:* Julian Eisel  
*Project not completed.*

  - [ Proposal](../User/PeterK/GSoC2020/Proposal.md)
  - [Weekly
    Report](https://devtalk.blender.org/t/gsoc-2020-openxr-actions-vr-interaction-weekly-reports/13726)

-----

### Liquid Simulation Display Options

by **Sriharsha Kotcharlakot**  
*Mentors:* Sebastián Barschkis, Jacques Lucke

  - [Proposal](../User/Sriharsha/GSoC2020/Proposal.md)
  - [Weekly
    Report](https://devtalk.blender.org/t/gsoc-2020-liquid-simulation-display-options-weekly-reports/13524)
  - [Final Report](../User/Sriharsha/GSoC2020/Final_Report.md)

-----

### Production Ready Many Light Sampling

by **Sam Kottler**  
*Mentors:* Brecht Van Lommel

  - [Proposal](../User/Samkottler/GSoC2020/Proposal.md)
  - [Weekly
    Report](https://devtalk.blender.org/t/gsoc-2020-production-ready-light-tree-weekly-reports/13682)
  - [Final Report](../User/Samkottler/GSoC2020/FinalReport.md)

-----

### Regression Testing Frameworks

by **Himanshi Kalra**  
*Mentors:* Habib Gahbiche, Bastien Montagne

  - [Proposal](../User/HimanshiKalra/Proposal.md)
  - [Weekly
    Report](https://devtalk.blender.org/t/gsoc-2020-regression-testing-framework-weekly-reports/13389)
  - [Final Report](../User/HimanshiKalra/Report.md)

-----

### Volumetric Soft Body Simulation

by **Matt Overby**  
*Mentors:* Sebastian Parborg

  - [Proposal](User:mattoverby/GSoC2020/mattoverby_proposal)
  - [Weekly
    Report](https://devtalk.blender.org/t/gsoc-2020-soft-body-sim-weekly-reports/13674?u=mattoverby)
  - [Roadmap](User:mattoverby/GSoC2020/gsoc-2020-soft-body-feature-request)
  - [Final
    Report](User:mattoverby/GSoC2020/mattoverby_final_report)
