# Release Cycle

## Release Dates and Schedule

For information on the release dates of specific Blender versions, see
the [milestones on
project.blender.org](https://projects.blender.org/blender/blender/milestones).

## Release Cycle

A new Blender version is targeted to be released every 4 months. The
actual release cycle for a specific release is longer, and overlaps the
previous and next release cycle.

![../images/Blender\_release\_cycle\_01.png](../images/Blender_release_cycle_01.png
"../images/Blender_release_cycle_01.png")

### Branches

Work is done in two branches:

  - \`blender-v{VERSION}-release\` branch: bug fixing only for the
    upcoming or LTS release
  - \`main\` branch: new features and improvements for the release after
    that

The \`blender-v{VERSION}-release\` branch will be available for 6 weeks
prior to the release date. At the same time \`main\` will be open for
the next release, giving 12 weeks to add new features for the next
release, and another 5 weeks to improve them.

## Bcon Phases

Each Blender version has its own Bcon phase, indicating which types of
changes are allowed to be committed and what developers are focusing on.

That means for example that Blender 3.4 can be in Bcon3 (bug fixing
only) while Blender 3.5 is in Bcon1 (open for new features).

<table>
<tbody>
<tr class="odd">
<td><p><strong>Phase</strong></p></td>
<td><p><strong>Description</strong></p></td>
<td><p><strong>Duration</strong></p></td>
<td><p><strong>Details</strong></p></td>
<td><p><strong>Branch</strong></p></td>
<td><p><strong>Splash Shows</strong></p></td>
</tr>
<tr class="even">
<td><p>Bcon1</p></td>
<td><p>New features and changes</p></td>
<td><p>6 weeks</p></td>
<td><p>Add bigger features and merge branches. Risky changes that are likely to cause bugs or additional work must be done before the end of this phase.<br />
<br />
The first 6 weeks overlap with the Bcon3 &amp; Bcon4 phases of the previous release, and many developers will focus on bug fixing in those first weeks.<br />
<br />
Developers dedicate a steady 1-2 days/week to the bug tracker, focusing on triaging and newly introduced bugs.</p></td>
<td><p>`main`</p></td>
<td><p>Alpha</p></td>
</tr>
<tr class="odd">
<td><p>6 weeks</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>Bcon2</p></td>
<td><p>Improve and stabilize</p></td>
<td><p>5 weeks</p></td>
<td><p>Work to improve, optimize and fix bugs in new and existing features. Only smaller and less risky changes, including small features, should be made in this phase.<br />
<br />
If a new feature is too unstable or incomplete, it will be reverted before the end of this phase.<br />
<br />
Developers spend 2-3 days/week in the bug tracker, triaging, fixing recently introduced or prioritized module bugs.</p></td>
<td><p>`main`</p></td>
<td><p>Alpha</p></td>
</tr>
<tr class="odd">
<td><p>Bcon3</p></td>
<td><p>Bug fixing only</p></td>
<td><p>5 weeks</p></td>
<td><p>Focus on bug fixing and getting the release ready.<br />
<br />
Development moves to the `blender-v{VERSION}` <a href="Process/Using_Stabilizing_Branch" title="wikilink">stabilizing branch</a> and the splash screen gets updated. In `main` Bcon1 for the next release starts. `blender-v{VERSION}-release` is regularly merged into `main`.<br />
<br />
The Python API remains stable during this phase, so add-ons have time to update before the release.<br />
<br />
High priority bugs dictate how much time developers will spend in the tracker as oppose to work on the next release Bcon1 features.</p></td>
<td><p>`blender-v{VERSION}-release`</p></td>
<td><p>Beta</p></td>
</tr>
<tr class="even">
<td><p>Bcon4</p></td>
<td><p>Prepare release</p></td>
<td><p>1 week</p></td>
<td><p>Stable branch is frozen to prepare for the release. Only critical and carefully reviewed bug fixes allowed.<br />
<br />
Release candidate and release builds are made.<br />
<br />
Developers spend a short time 5 days/week with an eye in the tracker for any unexpected high priority regression.</p></td>
<td><p>`blender-v{VERSION}-release`</p></td>
<td><p>Release Candidate</p></td>
</tr>
<tr class="odd">
<td><p>Bcon5</p></td>
<td><p>Release</p></td>
<td><p>1-2 days</p></td>
<td><p>Stage where the final builds are packaged for all platforms, last tweaks to the logs and promotional images, social media, video announcements.<br />
<br />
The final switch is flicked on <a href="https://www.blender.org">blender.org</a> for the new release to show up in the <a href="https://www.blender.org/download">Download page</a>.</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td><p>Bcon6*</p></td>
<td><p>Long-term release</p></td>
<td><p>2 years</p></td>
<td><p><a href="Process/LTS#Criteria_to_determine_what_to_be_ported" title="wikilink">Critical fixes</a> from `main` are ported over after passing quality assurance tests.</p></td>
<td><p>`blender-v{VERSION}-release`</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

*\* Bcon6 only happens for [LTS releases](LTS/index.md)*.
