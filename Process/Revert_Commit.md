# Reverting Commits

Sometimes a commit is pushed that introduces severe problems that won't
be solved immediately. Those are fine candidates to be reverted.

## When to revert a commit

Only serious breakages are considered for reversion. Those affect the
working process of the team or users:

  - Saving corrupted .blend file.
  - Regression tests are broken.
  - Build is broken without obvious way to solve it.
  - Any other reason the author of the commit find reasonable.

## No need to rush

If the fix for the issue will happen soon, no revert is needed.

  - Work with other team members to try to fix the issues before
    reverting it.
  - Sometimes unit tests simply need to be updated - consult the
    maintainer of the area/test.
  - Allow extra time to resolve platform specific breakages.

## How to proceed

People can respond emotionally when their code is reverted, so we do it
only when it is absolutely necessary. Be nice and reach out as best as
you can. But also, feel empowered to revert something if is for the best
of the project.

  - Talk the author of the commit into either finish the fix or revert
    their commit.
  - If they are not online and it is a serious breakage, revert the
    commit.
  - The reason to revert is to be stated explicitly in the revert commit
    message.
      - No matter whether it is an author or someone else is doing the
        revert.
      - Refer to task numbers of bug reports - closed or the ones
        created for the occasion - to make sure they will work once the
        reverted commit is re-introduced.
