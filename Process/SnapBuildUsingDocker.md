# Build snap package using docker

Assuming docker has been installed the following command can be executed
in the top of the Blender git clone

    docker run -v $(pwd):/my-snap snapcore/snapcraft sh -c "apt update && apt-get install -y python3 && cd /my-snap/release/freedesktop/snap && python3 bundle.py --version 2.83.1 --url https://download.blender.org/release/Blender2.83/blender-2.83.1-linux64.tar.xz"
