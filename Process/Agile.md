# Agile Development Process

In 2020Q3 we started an experiment of executing the [Geometry
Nodes](../Projects/Geometry_Nodes/index.md) project (part of Everything
Nodes) using Scrum Agile Methodology practices. This page describes the
process and our findings. It is intended as a framework. The actual
implementation of the framework for geometry nodes are described in the
project wiki.

Source of this page is
<https://en.wikipedia.org/wiki/Scrum_%28software_development%29>. It has
been altered to fit the blender project. (e.g. distributed developers,
open)

## Roles

### Product Owner

The product owner is responsible for delivering good results. Hence, the
product owner is accountable for the product backlog and for maximizing
the value that the team delivers.

The product owner defines the product in customer-centric terms
(typically user stories), adds them to the Product Backlog, and
prioritizes them based on importance and dependencies.

The product owner should focus on product development. The product owner
should not dictate how the team reaches a technical solution, but rather
will seek consensus among the team members.

### Development Team

While team members are referred to as developers, the term refers to
anyone who plays a role in the development and support of the system or
product.

The team is self-organizing. While no work should come to the team
except through the product owner, and the scrum master is expected to
protect the team from too much distraction, the team should still be
encouraged to interact directly with customers and/or stakeholders to
gain maximum understanding and immediacy of feedback.

### Scrum Master

Scrum is facilitated by a scrum master, who is accountable for removing
impediments to the ability of the team to deliver the product goals and
deliverables. The scrum master acts as a buffer between the team and any
distracting influences. The scrum master ensures that the scrum
framework is followed.

Very important is that the Product Owner must never be the same person
as the Scrum Master. The Product Owner manages the product and the Scrum
master manages the process.

## Workflow

A sprint is the basic unit of development in Scrum. The sprint is a
time-boxed effort; that is, the length is agreed and fixed in advance
for each sprint. Most common is 2 weeks.

### Sprint Planning

At the beginning of a sprint, the scrum team holds a sprint planning:

  - Forecast the amount of story points the team can handle for the next
    sprint, based on the amount of story points burned the last sprint
    and amount of time the team member has.
  - Select product backlog items that can be completed in one sprint
    backlog items are scored and story points are added to the items.
  - Prepare a sprint backlog that includes the work needed to complete
    the selected product backlog items
  - Agree the sprint goal, a short description of what they are
    forecasting to deliver at the end of the sprint.
  - During the first half, the whole scrum team (development team, scrum
    master, and product owner) selects the product backlog items they
    believe could be completed in that sprint
  - During the second half, the development team identifies the detailed
    work (tasks) required to complete those product backlog items;
    resulting in a confirmed sprint backlog As the detailed work is
    elaborated, some product backlog items may be split or put back into
    the product backlog if the team no longer believes they can complete
    the required work in a single sprint
  - Once the development team has prepared their sprint backlog, they
    forecast (usually by voting) which tasks will be delivered within
    the sprint.

The sprint planning is lead by the Product Owner.

### Standup

Each day during a sprint, the team holds a daily scrum (or stand-up)
with specific guidelines

  - All members of the development team come prepared. The daily scrum:
      - starts precisely on time even if some development team members
        are missing
      - should happen at the same time and place every day
      - is limited (time-boxed) to fifteen minutes
  - During the daily scrum, each team member typically answers three
    questions:
      - What did I complete yesterday that contributed to the team
        meeting our sprint goal?
      - What do I plan to complete today to contribute to the team
        meeting our sprint goal?
      - Do I see any impediment that could prevent me or the team from
        meeting our sprint goal?

Any impediment (e.g., stumbling block, risk, issue, delayed dependency,
assumption proved unfounded) identified in the daily scrum should be
captured by the scrum master and displayed on the team's scrum board
(Task is gets an impediment state. While the currency of work status is
the whole team's responsibility, the scrum master updates the sprint
burndown chart.

No detailed discussions should happen during the daily scrum. Once the
meeting ends, individual members can get together to discuss issues in
detail (after party).

### Sprint Demo

At the end of a sprint, the team holds two events: the sprint review and
the sprint retrospective. At the sprint review, the team:

  - reviews the work that was completed and the planned work that was
    not completed
  - demonstrated the completed work to the stakeholders
      - Incomplete work cannot be demonstrated.

### Sprint Review

At the sprint retrospective, the team:

  - Reflects on the past sprint
  - Identifies and agrees on continuous process improvement actions

<!-- end list -->

  - Three main questions arise in the sprint retrospective:
      - What went well during the sprint? (one item per participant)
      - What did not go well? (one item per participant)
      - What could be improved for better productivity in the next
        sprint? (one item per participant)
          - The team member should propose something (no complaining,
            but actions) that should be shortly discussed and approved
            on or dismissed.

Make sure you prepared 2-3 improvement as someone else could also have
the same item to address. But you are only allowed to put your top
improvement on the table. The improvement should be solvable by the team
them selves.

### Backlog Grooming

Backlog refinement (formerly called grooming) is the ongoing process of
reviewing product backlog items and checking that they are appropriately
prepared and ordered in a way that makes them clear and executable for
teams once they enter sprints via the sprint planning activity. Product
backlog items may be broken into multiple smaller ones.

The backlog can also include technical debt (also known as design debt
or code debt). This is a concept in software development that reflects
the implied cost of additional rework caused by choosing an easy
solution now instead of using a better approach that would take longer.

## Process Artifacts

Items that are managed for the Scrum Process

### Product Backlog

The product backlog is a breakdown of work to be done and contains an
ordered list of product user stories and use cases. They can define
features, bug fixes, non-functional requirements, etc.—whatever must be
done to deliver a product. The product owner prioritizes product backlog
items.

### Sprint Backlog

The sprint backlog is the list of work the development team promised to
address during the next sprint. The list is derived by the scrum team
progressively selecting product backlog items in priority order from the
top of the product backlog until they feel they have enough work to fill
the sprint. The development team should keep in mind its past
performance assessing its capacity for the new-sprint (fuel), and use
this as a guideline of how much 'effort' they can complete.

The product backlog items may be broken down into tasks by the
development team.

The sprint backlog is the property of the development team, and all
included estimates are provided by the development team. Often an
accompanying task board is used to see and change the state of the tasks
of the current sprint, like to do, in progress and done.

### Burndown Chart

The sprint burndown chart is a publicly displayed chart showing
remaining work in the sprint backlog. Updated every day, it gives a
simple view of the sprint progress. It also provides quick
visualizations for reference. The horizontal axis of the sprint burndown
chart shows the days in a sprint, while the vertical axis shows the
amount of work remaining each day (typically representing the estimate
of hours of work remaining).

### Definition of Ready

The start criteria to determine whether the specifications and inputs
are set enough to start the work item, i.e. a user story.

### Definition of Done

The exit-criteria to determine whether a product backlog item is
complete. In many cases, the DoD requires that all regression tests be
successful. The definition of done may vary from one scrum team to
another but must be consistent within one team.

### Spike

A time-boxed period used to research a concept or create a simple
prototype. Spikes might be accepted as one of many sprint delivery
objectives. Spikes are often introduced before the delivery of large or
complex product backlog items in order to expand knowledge. The duration
and objective(s) of a spike is agreed between product owner and
development team. For example, the objective of a spike might be to
successfully reach a decision on a course of action. The spike is over
when the time is up, not necessarily when the objective has been
delivered.
