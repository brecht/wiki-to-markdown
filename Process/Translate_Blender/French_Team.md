# Traduction de l’interface de Blender en français

Cette page a pour but de servir de référence pour la traduction de
l’interface de Blender en français.

## Contact

L’équipe de traduction peut être contactée sur [le canal
translations](https://blender.chat/channel/translations) de
blender.chat.

## Règles et conventions

### Infinitif en début de phrase

Les messages en anglais utilisent majoritairement cette forme :

`"Use X Coordinate"`  
`"Define Color"`

ce qui se traduit par :

`"Utiliser coordonnée X"`  
`"Définir couleur"`

<table>
<tbody>
<tr class="odd">
<td><div class="note_title">
<p><strong>Attention</strong></p>
</div>
<div class="note_content">
<p>Cette règle ne s’applique qu’aux verbes initiaux, pas forcément à ceux que vous pouvez rencontrer au sein des phrases…</p>
</div></td>
</tr>
</tbody>
</table>

### Casse des titres

En anglais, [les titres](wikipedia:Title_Case) utilisent des
majuscules sur la plupart des mots, ce que l’on retrouve dans la plupart
des libellés de Blender :

`"Copy Location"`  
`"Auto Run Python Scripts"`

Ce n’est pas le cas en français \! Donc, hors exceptions (noms propres,
références à d’autres éléments de l’interface, etc.), les libellés ne
prennent une majuscule qu’en début de phrase :

`"Copier position"`  
`"Lancement auto scripts Python"`

### Pas de points en fin de phrase

Les conventions de Blender veulent qu’il n’y ait pas de point final aux
phrases de l’interface (en général, cela concerne les astuces ou
*tooltips*). Ce point est ajouté automatiquement par Blender.

### Verbes factorisés

Certaines phrases peuvent se traduire par deux verbes opposés. Par
exemple,

` Toggle Selection`

se traduit en français par « Sélectionner ou déselectionner ». Dans ces
cas, il est possible d’utiliser des parenthèses pour réduire la phrase
en

` (Dé)sélectionner`

### Ponctuation

Le français s’écrit avec une ponctuation légèrement différente de
l’anglais. En particulier, il vaut mieux utiliser les guillemets
français en chevrons « » que les guillemets anglais “”, les guillemets
droits "" ou les apostrophes droites '' et courbes ‘’.

Contrairement à la plupart des autres langues, plusieurs [signes de
ponctuation](https://fr.wikipedia.org/wiki/Ponctuation#Signes_de_ponctuation_occidentaux)
prennent des espaces avant et après. Il existe plusieurs types d’espaces
typographiques, et les conventions varient d’un pays à l’autre. Pour ce
projet, les conventions de France s’appliquent.

Dans le tableau suivant, il y a trois types d’espaces :

  - justifiante : c’est l’espace normale. Quand elle est suivie d’un mot
    en fin de ligne, ce mot peut être rejeté à la ligne suivante.
  - insécable : le mot qui la suit reste collé au précédent. En fin de
    ligne, les deux mots sont rejetés à la suivante.
  - fine insécable, pareil, mais plus fine que les autres espaces.
  - case vide, le mot est collé au signe de ponctuation.

| Espace avant   | Signe | Espace après |
| -------------- | ----- | ------------ |
|                | .     | justifiante  |
|                | ,     | justifiante  |
|                | …     | justifiante  |
| fine insécable | ;     | justifiante  |
| fine insécable | ?     | justifiante  |
| fine insécable | \!    | justifiante  |
| insécable      | :     | justifiante  |
| justifiante    | —     | justifiante  |
| justifiante    | «     | insécable    |
| insécable      | »     | justifiante  |
| justifiante    | (     |              |
|                | )     | justifiante  |
| justifiante    | \[    |              |
|                | \]    | justifiante  |

#### Saisie

Il n’est pas toujours facile de saisir ces espaces différentes. Cet
article Wikipédia [donne des
conseils](https://fr.wikipedia.org/wiki/Espace_ins%C3%A9cable#%C3%80_la_saisie).

<table>
<tbody>
<tr class="odd">
<td><div class="note_title">
<p><strong>Attention</strong></p>
</div>
<div class="note_content">
<p>En cas de doute, le mieux est d’utiliser l’espace normale partout.</p>
</div></td>
</tr>
</tbody>
</table>

## Conseils de traduction

### Comprendre avant de traduire

Pour bien traduire un texte, il faut comprendre dans quel contexte il
est utilisé : où dans l’interface, et ce qu’il désigne. Certains mots
peuvent avoir plusieurs traductions très différentes, et il est alors
indispensable de savoir laquelle pour éviter les contresens. Beaucoup de
mots anglais peuvent se comprendre soit comme un verbe, soit comme un
nom. Ainsi :

  - *Pin Cloth* se traduit par « Épingler tissu ».
  - *Pin Stiffness* se traduit par « Raideur d’épinglage »  (en fait,
    « *Pinning stiffness* » serait plus juste, ici…).

## Terminologie

Un
[glossaire](https://translate.blender.org/browse/blender-ui/glossary/fr/)
est disponible sur l’interface de traduction. La plupart des mots et
expressions anglaises sont traduites quand il existe un équivalent, mais
certains ont été conservés si le terme anglais est majoritairement
utilisé.

Quelques ressources de traduction :

  - Le [Grand Dictionnaire
    terminologique](https://vitrinelinguistique.oqlf.gouv.qc.ca/) édité
    par l’Office québécois de la langue française.
  - Wikipédia : si un article existe en anglais et en français, les
    liens interlangues permettent souvent d’obtenir la traduction.
  - Le Wiktionnaire, soit dans les traductions de l’entrée en anglais,
    soit directement dans l’entrée en français.
