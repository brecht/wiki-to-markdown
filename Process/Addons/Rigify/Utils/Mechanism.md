# Mechanims Utilities (rigify.utils.mechanism)

This module provides utilities for creating custom properties,
constraints and drivers.

## Custom Properties

For creating custom properties, the module provides a thin wrapper
around the standard \`rna\_idprop\_ui\_create\` function, which changes
some of the defaults for the keyword parameters.

  - \`make\_property(owner, name, default, \*, min=0, max=1,
    soft\_min=None, soft\_max=None, description=None,
    overridable=True)\`  
    Creates a custom property '\`name\`' with the initial and default
    values set to 'default', and the appropriate range.
    The soft range defaults to the hard range, and the description
    defaults to the property name.

The function returns the metadata dictionary for the property.

### Utilities

  - \`deactivate\_custom\_properties(obj, \*, reset=True)\`  
    Mute drivers on custom properties of the object (not required to be
    ID), and optionally reset their values to defaults.

<!-- end list -->

  - \`reactivate\_custom\_properties(obj)\`  
    Unmute drivers on custom properties of the object.

<!-- end list -->

  - \`copy\_custom\_properties(src, dest, \*, prefix="",
    dest\_prefix="", link\_driver=False, overridable=True)\`  
    Copy custom properties from \`src\` to \`dest\` objects (not
    required to be ID).
    \`prefix\` only copies properties with the specified name prefix,
    removing it.
    \`dest\_prefix\` adds a new prefix to the name.
    \`link\_driver\` creates drivers feeding values from the copied to
    the original properties.
    \`overridable\` specifies if the copied properties should be set as
    overridable.

<!-- end list -->

  - \`copy\_custom\_properties\_with\_ui(rig, src, dest\_bone, \*,
    ui\_controls=None, ...)\`  
    Copy custom properties from \`src\` to the \`dest\_bone\` owned by
    the given \`rig\` object using \`copy\_custom\_properties\`, and
    create UI panel entries for them.
    \`src\` may be an object or a bone name.
    \`ui\_controls\` is the list of control bone names to associate with
    the entries, falling back to all controls of the rig if None.
    The UI option names are made more readable by replacing delimiters
    with spaces, splitting CamelCase and capitalizing words.

## Constraints

Almost all constraints can be created via a single call to the
\`make\_constraint\` utility function, which allows immediately
configuring the properties of the newly created constraint via its
keyword parameters.

  - \`make\_constraint(owner, type, target=None, subtarget=None, \*,
    insert\_index=None, space=None, track\_axis=None, use\_xyz=None,
    use\_limit\_xyz=None, invert\_xyz=None, targets=None, ...)\`  
    Creates a new constraint of type \`type\` belonging to \`owner\`.
    Target and subtarget are assigned to the matching properties if they
    exist.
    The \`insert\_index\` parameter allows inserting the constraint at a
    specific position in the stack, rather than at the end.
    The \`space\` parameter is used as the value for both
    \`owner\_space\` and \`target\_space\`, if they are not provided
    directly.
    The \`track\_axis\` parameter accepts shorter values: 'X', 'Y', 'Z',
    '-X', '-Y', '-Z'.
    The \`use\_xyz\` parameter must be a list, the items of which are
    assigned to \`use\_x\`, \`use\_y\`, and \`use\_z\`.
    The \`use\_limit\_xyz\` parameter must be a list, the items of which
    are assigned to \`use\_limit\_x\`, etc.
    The \`invert\_xyz\` parameter must be a list, the items of which are
    assigned to \`invert\_x\`, etc.
    The \`targets\` parameters specifies Armature constraint targets as
    a list of name strings, \`(\[target,\] subtarget, weight)\` tuples
    or dicts.
    If the \`min\_x\`, \`max\_x\`, etc parameters are supplied, the
    matching \`use\_min\_x\`, \`use\_limit\_x\` etc properties are set
    to True if they exist.
    Other keyword parameters are simply assigned to the matching
    properties of the constraint.

The function returns the newly created constraint object.

Most of the constraint parameter values may be lazy (i.e. instead be
functions without arguments that return the final value).

### Utilities

  - \`move\_constraint(source, target, con)\`  
    Move the constraint from one owner to another one, together with
    drivers. Target may be a bone name string.
    Drivers are only moved correctly when moving constraints between
    bones of the same armature.

<!-- end list -->

  - \`move\_all\_constraints(obj, source, target, \*, prefix="")\`  
    Move all constraints with the given name prefix from source to
    target bones within obj. Source and target may be pose bones or
    their names.

## Drivers

Drivers can be created and initialized via the following utility
function.

  - \`make\_driver(owner, path, index=-1, type='SUM', expression=None,
    variables={}, polynomial=None, target\_id=None)\`  
    Creates and initializes a new driver FCurve.
    The \`owner\`, \`path\` and \`index\` parameters specify the
    property to create the driver for.
    If \`expression\` is specified, the type is always set to scripted
    expression.
    The \`variables\` parameter contains the specifications for the
    required variables.
    The \`polynomial\` parameter allows specifying a list of
    coefficients for the polynomial Generator modifier.
    The \`target\_id\` parameter provides the default ID reference for
    Simple Property driver variables.

The function returns the new FCurve.

### Driver Variables

Driver variables are configured via a multiple-level data structure
passed through the \`variables\` parameter. The top level of the
structure must be either a dictionary mapping variable names to their
specifications, or a list if variable names can be assigned implicitly.

The following specifications are equivalent:

``` Python
make_driver(... variables=[ ..., ..., ... ] ...)
make_driver(... variables={ 'var': ..., 'var1': ..., 'var2': ... } ...)
```

The variables themselves may be specified either as a simplified tuple
representing the most common case, or a nested dictionary structure that
matches the actual data structures to be created.

The following specifications are equivalent:

``` Python
( target, subtarget, '.foo', 'bar', 0 )

{ 'type': 'SINGLE_PROP', 'targets':[( target, subtarget, '.foo', 'bar', 0 )] }

{ 'type': 'SINGLE_PROP',
  'targets':[{ 'id': target,
               'data_path': subtarget.path_from_id() + '.foo["bar"][0]' }] }
```

### Simple Variable Specification

The most common Single Property driver variables can be created using
the simplified specification format, consisting of a simple tuple. The
tuple generally can have two forms:

  - ( *target*, path\_string )
  - ( *target,* subtarget, path\_item, path\_item... )

The first element is the reference to the target ID object, and may be
omitted if the \`target\_id\` parameter is passed to \`make\_driver\`.

If the target is followed by a single string, it is used as the complete
property path for the driver variable target.

If it is instead followed by multiple tuple elements, it is interpreted
as a complex path. The first \`subtarget\` element should be either a
sub-object reference (e.g. \`PoseBone\`), or a string, which is looked
up in \`target.pose.bones\` as a bone name. The \`subtarget\` may be
equal to \`target\` if accessing properties of the target object itself.

The \`subtarget\`/\`path\_string\` element may be a 'lazy' callable
closure without arguments that returns the actual value.

The items following \`subtarget\` must be strings or numbers. Strings
are directly added to the path if they start with \`'.'\`, or quoted in
\`'\["..."\]'\` otherwise. Non-strings are wrapped in \`'\[...\]'\`.

### Transform Channel Variables

Transform Channel variables can be easily created using a utility
function:

  - \`driver\_var\_transform(target, bone=None, \*, type='LOC\_X',
    space='WORLD', rotation\_mode='AUTO')\`  
    Returns a specification for a Transform Channel driver variable.
    The \`bone\` parameter may be a 'lazy' callable closure without
    arguments.
    The \`type\` parameter specifies the transform channel to access.
    The \`space\` parameter sets the transform space to one of 'WORLD',
    'TRANSFORM', 'LOCAL'.
    The \`rotation\_mode\` parameter sets the mode for the rotation
    channels.

For example:

``` Python
target_scale = driver_var_transform(self.obj, target, type='SCALE_AVG', space='LOCAL')
make_driver(... variables=[target_scale] ...)
```

### Distance Variables

Variables returning the distance between two objects can be created
using a utility:

  - \`driver\_var\_distance(target, \*, bone1=None, target2=None,
    bone2=None, space1='WORLD', space2='WORLD')\`  
    Returns a specification for a Distance driver variable.
    The \`bone1\` and \`bone2\` parameters may be a 'lazy' callable
    closure without arguments.
    \`target2\` defaults to \`target\` if None.

### Utilities

  - \`refresh\_drivers(obj)\`  
    Cause all drivers belonging to the object to be re-evaluated,
    clearing any errors.

<!-- end list -->

  - \`refresh\_all\_drivers()\`  
    Cause all drivers in the file to be re-evaluated, clearing any
    errors.

## MechanismUtilityMixin class

This mixin provides the same utilities as methods that implicitly use
\`self.obj\` to look up bones.

  - \`self.make\_property(bone\_name, prop\_name, default, ...)\`  
    Create a custom property attached to the specified bone.

<!-- end list -->

  - \`self.make\_constraint(bone\_name, type, subtarget=None, ...)\`  
    Create a constraint attached to the specified bone, and implicitly
    using the armature as the target.

<!-- end list -->

  - \`self.make\_driver(owner, path, ...)\`  
    Create a driver, allowing the use of a bone name as \`owner\`, and
    using the armature as \`target\_id\`.
