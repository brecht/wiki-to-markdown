# Development Process

## Release

  - [Release Cycle](Release_Cycle.md)
  - [Release Checklist](Release_Checklist/index.md): things to
    do for a release (bcon3 / bcon6)
  - [Release on Steam](Release_On_Steam.md)
  - [Release on Windows
    Store](Release_On_Windows_Store.md)
  - [Who does what for a
    release](Release_Responsibilities.md)
  - [Long-term Support](LTS/index.md)

## Bugs

  - [The Life of a Bug](A_Bugs_Life.md): Triaging,
    classification, assignment and fix
  - [Bug Reports](Bug_Reports/index.md): how to make good bug
    reports
  - [Help Triaging Bugs](Help_Triaging_Bugs.md): how to
    help triaging bugs
  - [Bug Triaging
    Playbook](Bug_Reports/Triaging_Playbook.md): canned
    responses for triaging bugs
  - [Vulnerability Reporting
    Policy](Vulnerability_Reports.md)

## Patches

  - [Contributing Code](Contributing_Code/index.md): how to
    contribute fixes & new features, and get them reviewed
  - [Code Review
    Playbook](Contributing_Code/Review_Playbook.md):
    canned responses for reviewing patches

## Projects

  - [Projects](Projects.md): how to clearly structure a
    project
  - [Compatibility Handling](Compatibility_Handling.md):
    about compatibility of blend files across Blender versions.
  - [Code Quality Day](../Style_Guide/Code_Quality_Day.md): Code
    Quality Day

## Committing

  - [Commit Rights](Commit_Rights.md): how to get commit
    rights
  - [Stabilizing Branch](Using_Stabilizing_Branch.md):
    how to commit and merge fixes in the stabilizing branch
      - [ Playbook Merge
        Handling](Playbook_Merge_Handling.md): playbook
        for merges
  - [Tests](../Tools/Tests/Setup.md): How to set up & run automated
    tests
  - [Reverting Commits](Revert_Commit.md): when and why
    to revert a problematic commit

## Translations

  - [How to translate Blender](/Translate_Blender)
  - [How to use i18n feature in Blender
    code](../Source/Interface/Internationalization.md)

## Add-ons

  - [Addons](Addons/index.md): getting your add-on bundled and
    maintaining it

## Maintainers

  - [About Module Owners and Admins](Process/Module_Owners)
  - [Overview of hardware in use by Blender
    developers](Hardware_List.md)

## Development Organization

  - [Agile Development Process](Agile.md)
