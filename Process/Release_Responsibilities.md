# Release Roles and Responsibilities

The following roles and responsibilities are needed for a fluent release

## Roles

  - **PM** - product manager (currently handled by development
    coordinator)
  - **DC** - development coordinator
  - **RM** - release manager
  - **PR** - Public relations / social media
  - **CA** - Chief architect
  - **DOC** - Documentation
  - **i18n** - Localisation

## Responsibilities

The product manager confirms what are the features in a release, and to
be sure they are on track and fulfill all the steps of the release
process. At the moment this role is performed by the development
coordinator.

The release manager oversees the release process. This includes [release
cycle
tasks](Release_Checklist/index.md#Start_New_Release_Cycle)
like branching, buildbots and checking with other roles and rest of team
everything is on track

PR/SM has traditionally been handled at the end of each release cycle,
very close to the actual release date, but this can't be a parachute
role, needs more involvement throughout the proces. The release manager
will work together with PR to ensure the release notes for
www.blender.org get started in time. Also splash screen selection should
be started at around the beginning of bcon3

The chief architect works together with product manager to ensure
features are fitting in well. Further the chief architect ensures also
undocumented commits get properly logged for release. If necessary also
tag for documentation.

Development coordinator ensures tests keep passing, code quality is
maintained and Blender stays stable.

Documentation ensures missing features are documented in the reference
manual.

The different tasks imagined for a release cycle have been documented in
[Actions per
Bcon](Release_Checklist/Actions_Per_Bcon.md).
