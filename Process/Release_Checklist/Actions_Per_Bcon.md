Collection of release management related actions throughout the entire
release cycle

By default each bcon phase rolls into next mid-week on Wednesday. This
means that preparations and activating necessary people should start
already on the week prior. Ideally on Thursday, latest on Friday so we
can determine we are still go for move to next bcon phase.

### bcon 1

  - Follow commits into \`master\`, especially new features
  - On a weekly basis ensure release notes are current
  - Check with Chief Architect and module owners that planned major
    features and overhauls are on track for release
  - Prior to move to bcon 2 check work boards (per milestone)

### bcon 2

  - Follow commits into \`master\`. All major works should have landed
    prior to move into bcon 2, but small features and improvements
    (meaning potential changes needing also documentation changes) can
    still happen
  - On a weekly basis ensure release notes are current

### bcon 3

  - Create release stabilizing branch ([release
    checklist](index.md))
      - Verify localization status
      - Verify add-ons status
      - When created ensure first release test builds are created and
        published
  - Start working with PR towards creating the main release notes page
  - Check with PR status of splash, at latest start selection process at
    beginning of bcon 3
  - Check with Documentation that work is known. Help poking devs for
    minimum amount of docs so manual can be written and improved

### bcon 4

  - Work with chief architect to ensure release notes on wiki are
    complete (undocumented commits missing from notes)
  - Work with PR to finalize release notes on front page
  - Work with PR to receive splash screen
  - Work with PR to get release art completed (including art for Steam
    store page and announcement)
  - Work with Documentation to ensure manual is complete before release

### bcon 5

  - Roll versions
  - Land splash
  - Tag sources
  - Ensure all release platform packages are ready (blender.org, Steam,
    Windows Store, Snap)
  - Ensure from PR release messages are ready and queued (blender.org,
    social media, Steam, Snap?)
  - Release
