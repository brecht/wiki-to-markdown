# OpenXR-SDK Dependency

Notes about maintaining the OpenXR-SDK library dependency.

## Updating to new Version

Repository: <https://github.com/KhronosGroup/OpenXR-SDK>  
Note: Use the OpenXR-SDK repository, NOT the OpenXR-SDK-Source one\!

  - \`build\_files/build\_environment/cmake/xr\_openxr.cmake\`: may
    require version number changes
  - \`build\_files/build\_environment/cmake/versions.cmake\`: update
    \`XR\_OPENXR\_SDK\_VERSION\` and \`XR\_OPENXR\_SDK\_HASH\` (MD5 of
    .tar.gz)
  - \`build\_files/build\_environment/install\_deps.sh\`: update
    \`XR\_OPENXR\_VERSION\` and \`XR\_OPENXR\_REPO\_UID\` (commit hash)
  - \`build\_files/cmake/platform\_win32.cmake\`: may require version
    number changes

Remember to always check the specification's change log for
compatibility breaking changes.
