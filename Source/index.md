# Code and Design Documentation

<div class="bd-lead">

Reference documentation and diagrams about Blender's architecture and
components, and projects under development.

</div>

Old but often still useful design documents can be found in the
<span class="plainlinks">[Wiki
Archive](http://archive.blender.org/wiki)</span>.

## General

  - [File Structure](File_Structure.md)
  - [Context](Architecture/Context.md)
  - [Transform](Architecture/Transform.md)
  - [Extensible Architecture
    Proposal](Architecture/Extensibility.md)
  - [Dependency Graph](Depsgraph.md)
  - [Blender Projects](Blender_Projects.md)

## Data Management, Import/Export

  - [IDs & ID Management](Architecture/ID/index.md)
  - [IDProperties](Source/Architecture/IDProperty)
  - [DNA](Architecture/DNA.md)
  - [RNA](Architecture/RNA.md)

<!-- end list -->

  - [.blend File
    Read/Write](Source/Architecture/File_Read_Write)
      - [Libraries &
        Link/Append](Source/Architecture/File_Read_Write/Library_Link_Append)

<!-- end list -->

  - [Asset System](Architecture/Asset_System/index.md)
  - [Overrides](Architecture/Overrides/index.md)
      - [Library
        Overrides](Architecture/Overrides/Library/index.md)

<!-- end list -->

  - [Undo System(s)](Architecture/Undo.md)
      - [Global Undo](Source/Architecture/Undo/Global)
      - [Edit Undo](Source/Architecture/Undo/Edit)
      - [Sculpt Undo](Source/Architecture/Undo/Sculpt)

<!-- end list -->

  - [Alembic](Architecture/Alembic.md)
  - [Universal Scene Description](Architecture/USD.md)

## Modeling

  - [BMesh Design](Modeling/BMesh/Design.md)

## Animation & Rigging

  - [Module-specific page for Animation &
    Rigging](Animation/index.md)

## Rendering

  - [Cycles](Render/Cycles/index.md)
  - [Color Management](Render/ColorManagement.md)

## User Interface

  - [Internationalization](Interface/Internationalization.md)
  - [Experimental
    Features](Interface/ExperimentalFeatures.md)
  - [Virtual Reality](Interface/XR.md)
  - [Operators](Interface/Operators.md)
  - [Icons](Interface/Icons.md)
  - [Views](Interface/Views/index.md)
  - [Editors](Interface/Editors.md)
  - [Window Manager](Interface/Window_Manager.md)
  - [Windows](Interface/Window.md)
  - [Screens](Interface/Screen.md)
  - [Text Output](Interface/Text.md)
  - [Outliner](Interface/Outliner.md)
  - [Preferences &
    Defaults](Interface/Preferences_and_Defaults.md)

Technical documentation ToDos:
[T104114](http://developer.blender.org/T104114)

## Sculpt, Paint, Texture

Technical documentation ToDos:
[T104113](http://developer.blender.org/T104113)

  - [PBVH](Sculpt/PBVH.md)

## EEVEE & Viewport

For a quick overview for new developers to the module check the
\[<https://www.youtube.com/watch?v=OxwrSdt6I4w>| on-boarding video \].

  - [GPU Module Overview](EEVEE_&_Viewport/GPU_Module/index.md)
  - [Draw Module
    Overview](Source/EEVEE_&_Viewport/Draw_Module)
  - [Color Management Drawing
    Pipeline](EEVEE_&_Viewport/Color_Management_Drawing_Pipeline.md)
  - [EEVEE](EEVEE_&_Viewport/Draw_Engines/EEVEE/index.md)
  - [Workbench](Source/EEVEE_&_Viewport/Draw_Engines/Workbench)
  - [Overlay](Source/EEVEE_&_Viewport/Draw_Engines/Overlay_Engine)
  - [External](Source/EEVEE_&_Viewport/Draw_Engines/External_Engine)
  - [Selection](Source/EEVEE_&_Viewport/Draw_Engines/Selection_Engine)
  - [UV Image
    Editor](EEVEE_&_Viewport/Draw_Engines/Image_Engine.md)

## Objects

  - [Mesh Object](Objects/Mesh.md)
  - [Volume Object](Objects/Volume.md)
  - [Point Cloud Object](Objects/PointCloud.md)
  - [Curves Object](Objects/Curves.md)
  - [Geometry Sets](Objects/Geometry_Sets.md)
  - [Instances](Objects/Instances.md)
  - [Attributes](Objects/Attributes.md)

## Nodes & Physics

  - [Viewer Node](Nodes/Viewer_Node.md)
  - [Modifier Nodes](Nodes/Modifier_Nodes.md)
  - [Spreadsheet Editor](Editors/Spreadsheet.md)
  - [Fields](Nodes/Fields.md)
  - [Anonymous Attributes](Nodes/Anonymous_Attributes.md)
  - [Fluid system - Mantaflow](Source/Physics/Mantaflow)
  - [Rigid body - Bullet](Source/Physics/Rigid_Body)
  - [Cloth](Source/Physics/Cloth)
  - [Caching system](Source/Physics/Caching)

## VFX & Video

  - [Video Sequencer Editor](VSE.md)
  - [Compositor](Compositor.md)
  - [Realtime Compositor](RealtimeCompositor/index.md)
  - [Motion Tracking](Source/Motion_Tracking)

## Grease Pencil

## Lineart

## Library Dependencies

  - [OpenXR-SDK](OpenXR_SDK_Dependency.md)
