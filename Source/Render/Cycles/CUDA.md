# CUDA Status

CUDA support is now stable and part of regular Blender builds. For
further optimizations ideas see [this
task](https://developer.blender.org/T87836).
