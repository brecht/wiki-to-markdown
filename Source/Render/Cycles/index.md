# Cycles Renderer

For source code and contact details, see the [Cycles project page on
developer.blender.org](https://developer.blender.org/project/manage/26/).

The [workboard](https://developer.blender.org/project/board/99/) has an
overview of ongoing and planned tasks.

## Design

  - [Design Goals](DesignGoals.md)
  - [Node Guidelines](NodeGuidelines.md)

## Code

  - [Source Layout](SourceLayout.md)
  - [Style Guide](../../../Style_Guide/C_Cpp.md)
  - [Cycles Standalone](Standalone.md)

## Implementation

#### Scene Graph

  - [Scene Graph](SceneGraph.md)
  - [BVH](BVH.md)

#### Device Abstraction

  - [Devices](Devices.md)
  - [Kernel Language](Source/Render/Cycles/Kernel/Language)

#### Scheduling

  - [Render
    Scheduling](RenderScheduling.md)
  - [Kernel
    Scheduling](Source/Render/Cycles/Kernel/Scheduling)
  - [Multi Device
    Scheduling](MultiDeviceScheduling.md)

#### Output

  - [Tiling and OpenEXR Cache](Tiling.md)
  - [Output and Display
    Drivers](Drivers.md)

## Algorithms

  - [Sampling
    Patterns](SamplingPatterns.md)
  - [Closures](Source/Render/Cycles/Closures)
  - [Volumes](Source/Render/Cycles/Volumes)
  - [Light Sampling](Source/Render/Cycles/LightSampling)
  - [Units & Colors](Units.md)
  - [Light and Shadow
    Linking](LightLinking.md)

## Outdated

To be updated or removed.

  - [Threads](Threads.md)
  - [Papers](Papers.md)
  - [Optimization Ideas](Optimization.md)
  - [Network Render](Network_Render.md)
  - [Volume Render](Volume.md)
