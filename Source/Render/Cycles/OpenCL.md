# OpenCL Status

OpenCL support was removed in Blender 3.0.

Instead there are [HIP](https://developer.blender.org/T91571) and
[Metal](https://developer.blender.org/T92212) backends.
