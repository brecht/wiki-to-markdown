## EEVEE

EEVEE is a GPU rasterize based renderer. It uses gaming rasterizer
techniques to render very fast, but uses higher quality renderings as we
don't need to stick to a minimum framerate.

### Overview

  - [GPU Pipeline](GPUPipeline.md) (WIP)
  - [Render Passes](Source/Render/EEVEE/RenderPasses)
