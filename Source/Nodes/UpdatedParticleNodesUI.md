# Updated Particle Nodes UI

Some weeks ago I published a proposal on how a particle system can be
represented with nodes
([link](ParticleSystemNodes.md)). Now, after
working on the system for a couple of weeks, I found a couple of weak
points. This document explains the problems and presents an updated
proposal.

## Problems

There are two main problems, both of which I consider deal breakers.

### Node Tree Growth

Users need to be able to extend node trees without getting in their own
way all the time. The issue is that often other nodes are at the place
where a new node is supposed to be. This has been partially solved with
the [Auto-Offset](https://vimeo.com/135125839) feature some years ago.
With it, other nodes can automatically move out of the way when
necessary.

In the original particle nodes ui proposal, this does not work well.
This is because the node tree grows in too many directions at the same
time. When the node tree becomes more complex, there is no room to
expand.

![../../images/Tree\_grow\_directions.png](../../images/Tree_grow_directions.png
"../../images/Tree_grow_directions.png")

### Composition

One of the great features of nodes is that they can be packaged and
reused. However, this was difficult with the original proposal. For
example, when an Age Reached Event and a Gravity Force are put in a
group for reuse, the group would have an "Events" input and a
"Modifiers" output. It is not clear how that should be connected to the
Particle Type node.

## New Proposal

The main change I suggest is to get rid of all the links to the Particle
Type node. Then it would not need the Emitters, Modifiers and Events
sockets anymore. Semantically, they all were the same anyway. They just
told the simulation system that these nodes belong together (unlike
others, that transfer e.g. data). Fortunately, there are other ways to
present this relationship to the user. One possible solution is to have
an enum property in every relevant node that allows the user to select,
which particle type it belongs to. Control flow through actions and data
flow would still work exactly the same way.

![../../images/Enums\_instead\_of\_links.png](../../images/Enums_instead_of_links.png
"../../images/Enums_instead_of_links.png")

This approach also simplifies solving the second problem. Every group
can have an arbitrary number of Particle Type inputs. Instead of
selecting a concrete particle type in the group, a "placeholders" can be
selected. The particle types are then exposed as properties when the
group is actually used.

## Implications

This update has a couple of implications which are presented here.

Most prominently, the relation between particle types and the components
(set of connected nodes) that control its behavior is less obvious now.
Just having the name of the particle type in the node, might not be
enough. However, there are more ways in which the relation can be hinted
at the user. For example, whenever a Particle Type node is selected, the
corresponding components are highlighted. Furthermore, users can
organize the nodes such that everything related to one particle type is
close together. Additionally, all the components, that a effect a
particle type can be listed in the node itself.

It can be considered annoying to have to select the right particle type,
every time a new component is added. Fortunately, this can be mostly
automated. For example, a particle type node could have a button to that
opens a search/menu. Nodes inserted from it will be setup already.

Previously, all nodes had to fit in one of the categories Emitter,
Modifier and Event. This is fine for the basic building block nodes.
However, when users group those together, the group does not fit into
any of these categories anymore. In the new system, the categories don't
exist anymore at the UI level, so it is no issue.

In the original proposal, nodes usually belong to one type. This was not
really enforced, but connecting them to multiple particle type nodes
would create a big mess. Now, a node can more easily use more than one
particle type. For that it just has multiple particle type enum
properties.

With that, the semantic of adding a new component also changes.
Previously, adding a component meant adding new behavior to a particle
type. Now, the semantic is more like adding behavior to the entire
particle system (which in many cases just happens to affect a single
particle type).

I'd argue that this new system is easier to learn, because there is less
to learn. The distinction between emitters, modifiers and events does
not have to be learned, because it does not exist. Also an entire class
of links, links that represent just relations, is removed. So instead of
having links with three different semantics, there are only two
semantics left: data flow and control flow.

The concept of gates as presented in the original proposal does not work
after this update. Alternatives can be found when necessary, but maybe
this does not have to be a built-in feature. When custom attributes are
supported, users should be able to build this themselves.

A limitation compared to the original proposal is, that a group cannot
add new particle types to the system. So the number of particle types is
exactly the number of Particle Type nodes in the node tree at the top of
the hierarchy. I do not think that this is a bad limitation. In fact,
most users would probably have a hard time conceptualizing more dynamic
amounts of particle types. Having all particle types at the top level
also makes creating identifiers for them easier. Furthermore, this will
probably become beneficial when it comes to rendering and the user wants
to control the shading of individual particle types.
