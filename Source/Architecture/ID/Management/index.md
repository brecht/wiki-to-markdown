# ID Management

ID management covers creation, copying, deletion of data-blocks,
remapping of ID usages, and handling of their common properties, like
naming, user refcounting, etc.

This is performed through a generic API defined in
[BKE\_lib\_id.h](https://developer.blender.org/diffusion/B/browse/master/source/blender/blenkernel/BKE_lib_id.h)
and
[BKE\_lib\_remap.h](https://developer.blender.org/diffusion/B/browse/master/source/blender/blenkernel/BKE_lib_remap.h).
This code relies on various callbacks in the \`IDTypeInfo\` type of each
ID to perform parts of those operations that are specific to each type.

Some more complex are similarly provided by additional modules:

  - [Handling of relationships between
    IDs](Relationships.md),
    including a main \`foreach\` looper over all pointers to other IDs
    for a given data-block, and some general higher-level helpers, in
    [BKE\_lib\_query.h](https://developer.blender.org/diffusion/B/browse/master/source/blender/blenkernel/BKE_lib_query.h).
  - [Handling of file paths in
    IDs](File_Paths.md),
    including a main \`foreach\` looper over all file paths, and some
    higher-level features like conversion between relative and absolute
    paths, in
    [BKE\_bpath.h](https://developer.blender.org/diffusion/B/browse/master/source/blender/blenkernel/BKE_bpath.h).
