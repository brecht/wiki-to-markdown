# IDs and File Paths

## Basics

  - Several ID types needs to keep references to external files (library
    \`.blend\` files, images or videos files, etc.).
  - File paths in Blender are generally stored in fixed-sized char
    buffers, with following maximum length defines:
      - Full filepath: \`FILE\_MAX\`
      - Filename: \`FILE\_MAXFILE\`
      - Directory path: \`FILE\_MAXDIR\`
      - Full library-path to a data-block: \`FILE\_MAX\_LIBEXTRA\`
      - A lot of the code assumes that those values are valid for paths
        passed around.

## Looping over ID filepaths

The
[BKE\_bpath.h](https://developer.blender.org/diffusion/B/browse/master/source/blender/blenkernel/BKE_bpath.h)
module provides an API to loop over all filepaths of a given ID (or for
all IDs in the whole Main data-base).

For each filepath, a user-defined callback is called, with the \`self\`
ID owner of the path, the filepath itself, and some meta information.

This code is used e.g. to find missing files, or to rebase relative file
paths, convert between relative and absolute paths, etc.
