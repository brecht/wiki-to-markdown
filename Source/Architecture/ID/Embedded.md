# Embedded IDs

Embedded IDs are data-blocks that do not exist in the [Main
data-base](Main.md), but rather act as
sub-data of an owner ID, which is their only exclusive user.

Embedded IDs are flagged with \`LIB\_EMBEDDED\_DATA\`. \`IDTypeInfo\` of
the ID types that can be embedded data must implement the \`owner\_get\`
callback to retrieve the owner ID from its embedded ID.
