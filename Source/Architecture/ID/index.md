# IDs

IDs (also known as data-blocks, especially at user level) are the main
core data structure in Blender. Almost any persistent data is an ID, or
belongs to an ID, even mostly UI-related data like the \`WindowManager\`
or \`Screen\` ID types.

<table>
<tbody>
<tr class="odd">
<td><div class="note_title">
<p><strong>Disclaimer</strong></p>
</div>
<div class="note_content">
<p>A lot of those pages are still stubs and need to be fully written, reviewed, etc.</p>
</div></td>
</tr>
</tbody>
</table>

## ID Type

  - **[ID, ID\_Type,
    IDTypeInfo](ID_Type.md)**: Basic
    structure of an ID, and the ID type system.

## ID Runtime Storage

  - **[Main data-base](Main.md)**:
    Runtime storage of persistent IDs.
  - **[Runtime-only IDs](Runtime.md)**:
    The various kinds of temporary, runtime-only ID datablocks.
  - **[Embedded IDs](Embedded.md)**: Some
    IDs (Collections, NodeTrees) can be 'embedded' into other IDs.

## ID Management

  - **[ID Management](Management/index.md)**:
    Basics over ID creation, copying/duplication, and freeing.
  - **[Relationships Between
    IDs](Management/Relationships.md)**:
    Relationships between different IDs, usage refcounting, remapping...
  - **[File
    Paths](Management/File_Paths.md)**:
    File paths handling in IDs.

## Related Topics

*TODO: Also needs links to related topics: read/write, link/append,
liboverride, animation, DNA, RNA, etc.*
