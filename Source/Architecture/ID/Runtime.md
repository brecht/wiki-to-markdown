# Runtime IDs

Runtime IDs have a relatively short lifetime, at most a Blender session,
but very often much shorter, if used e.g. as temporary work data during
some operation.

<table>
<tbody>
<tr class="odd">
<td><div class="note_title">
<p><strong>Warning</strong></p>
</div>
<div class="note_content">
<p>This data is also pending <a href="https://developer.blender.org/T88555">ID Management: Sanitize and clarify our ID tags</a> and related tasks, which among other things aim at clarifying the temporary IDs types and management.</p>
</div></td>
</tr>
</tbody>
</table>

There are four main variations of runtime IDs currently in Blender:

  - Evaluated data, tagged with \`LIB\_TAG\_COPIED\_ON\_WRITE\` or
    \`LIB\_TAG\_COPIED\_ON\_WRITE\_EVAL\_RESULT\`: The
    [depsgraph](../../Depsgraph.md) stores the results of its
    evaluation in temporary data-blocks, which lifetime and
    allocation/freeing are managed by itself. Those evaluated IDs can
    share some data with their original version, especially heavy sets
    of data or caches.
  - Fully out-of-main, independent data, tagged with
    \`LIB\_TAG\_NO\_MAIN\`: Those are fully independent data generated
    during some processing, they should never share any data with any
    other ID. Those are 'convertible' by generic code into regular 'in
    Main' IDs.
  - IDs from temporary Mains, tagged with \`LIB\_TAG\_TEMP\_MAIN\`:
    Those IDs essentially behave like regular data-blocks, and follow
    the same rules. But they do not belong to the Main instance
    representing the current .blend file, but to some other, temporary
    data-base.
  - Localized IDs, tagged with \`LIB\_TAG\_LOCALIZED\`: Currently used
    for some ID types during rendering process. //To be
    precised/refined/extended.//
