# Main Data-Base

The Main structure is the runtime equivalent of a .blend file: it stores
all IDs representing the whole data of the current file.

It also stores runtime-only data, like the full version of all [linked
IDs](Source/Architecture/File_Read_Write/Library_Link_Append),
and some short-life helping information like relationships between
data-blocks.

It is defined, along with its core API, in
[\`BKE\_main.h\`](https://developer.blender.org/diffusion/B/browse/master/source/blender/blenkernel/BKE_main.h).

## ID storage in Main

*One list by id type, ordered by name, with linked IDs after local
ones.*
