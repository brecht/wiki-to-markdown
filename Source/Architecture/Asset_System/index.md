# Asset System

Assets in Blender are data-blocks that are packaged for sharing/reuse.
The asset system is the technical foundation that makes Blender able to
deal with assets, and includes features like asset libraries, asset
library loading, asset catalogs, asset metadata, etc. Assets can be
displayed in the UI in various ways, including in the Asset Browser and
asset views ("mini" Asset Browsers optimized for using, not organizing).
*Longer term, the asset system seeks to bring a future-proof
understanding of assets for sharing and reuse as first class citizens to
Blender.*

Currently assets have to represent ID data-blocks (Blender objects,
collections, materials, etc.). But the asset system design foresees
assets that can represent arbitrary entities, including files, or
entities stored in some data-base.

<table>
<tbody>
<tr class="odd">
<td><div class="note_title">
<p><strong>Note</strong></p>
</div>
<div class="note_content">
<p>The asset system is still in heavy development.</p>
</div></td>
</tr>
</tbody>
</table>

## Subpages

[Frequently Asked
Questions](FAQ.md)

**Functional design:**

  - [Asset
    Bundles](Asset_Bundles/index.md)
  - [Brush
    Assets](Brush_Assets.md)

**Technical design:**

  - [Asset System
    Backend](Back_End.md)
  - [Asset
    Catalogs](Catalogs.md)
  - [ User Interface](UI.md)
  - [Asset
    Indexing](Asset_Indexing.md)
