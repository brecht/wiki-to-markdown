# Line Art Desired Functionality

This is just a list of functions we want to implement into Line Art in
the future.

## Crease lines by using smoothing groups

![../../images/Pencil\_line\_001.png](../../images/Pencil_line_001.png
"../../images/Pencil_line_001.png")

### Status:

Already in LineArt. Can be done using edge groups.

### Discussion:

ZedDB: Does LineArt support Freestyle face marks? Perhaps we could do
something like: "Only do crease lines inside (or outside) of marked
faces"
<https://docs.blender.org/manual/en/latest/render/freestyle/parameter_editor/line_set.html#face-marks>

## Line at material borders

![../../images/Pencil\_line\_002.png](../../images/Pencil_line_002.png
"../../images/Pencil_line_002.png")

### Status:

Already supported

## Intersection lines for objects that are not visible on render

![../../images/Pencil\_line\_003.png](../../images/Pencil_line_003.png
"../../images/Pencil_line_003.png")

### Status:

Intersection lines are supported, but not currently from hidden
geometry.

### Discussion:

Yiming: If we need exact illustration as shown in the image, we need to
implement a "intersection only" flag to hide the intersection object.

## Line thickness is affected by distance from camera

![../../images/Pencil\_line\_007b.png](../../images/Pencil_line_007b.png
"../../images/Pencil_line_007b.png")

### Status:

This is already supported

### Discussion:

Yiming: GPencil shader could improve something regards the
configurability of that.

## Lines that stop a bit before their actual end point

![../../images/Pencil\_line\_005\_c.png](../../images/Pencil_line_005_c.png
"../../images/Pencil_line_005_c.png")

The outline shape is unaffected.

In the example here, the effect is done by using mesh intersections.

### Status:

We have a line length modifier but some more investigation is needed to
see if we can get nice results.

### Discussion:

Yiming: Probably need some sort of weight and type selection for
influenced area. (e.g. in the example image it only shortens crease
lines)

## Easy creation of lines on subdiv surfaces

![../../images/Pencil\_line\_006\_a.png](../../images/Pencil_line_006_a.png
"../../images/Pencil_line_006_a.png")
![../../images/Pencil\_line\_006\_b2.png](../../images/Pencil_line_006_b2.png
"../../images/Pencil_line_006_b2.png")
![../../images/Pencil\_line\_006\_c2.png](../../images/Pencil_line_006_c2.png
"../../images/Pencil_line_006_c2.png")

I other programs that does not have edge marks, you need to do
workarounds to create line on (for example) a cylinder with a subdiv
modifier on it.

### Status:

This is not an issue for us currently as the edge marks do not affect
the final shape of the mesh. So we can simply use edge marks here to
define where we want the strokes to be.

### Discussion:

Yiming: We might need an additional feature that specify only to show
material separation line between specific material pairs.

ZedDB: Yes, I agree. It might be nice to have the ability to specify
that you want strokes generated on the border between material A and B,
but not between material A and C or B and C.

## Morphing lines on deformed surfaces

![../../images/Desired\_line\_001.png](../../images/Desired_line_001.png
"../../images/Desired_line_001.png")

### Status:

Workflow proposal still pending.

### Discussion:

Yiming: If the artist only edits the original stroke/texture, then we
need some sort of projection, either UV or directional. In my opinion
this depends on how you are gonna want to do the animation. Currently
the best method for the artist would involve texture tracing on code
side.

ZedDB: The artists could also have multiple object and a do thing using
a surface deform modifier. But if there is a lot of shapes, then this
will be quite cumbersome...

K and Q: In the example is just a 'A' to 'B' tranformation. But
specically, this is used for facial wrinkles. I would be happy if I
could increase or decrease the lines during the animation.

## Line shading in response with lighting

![../../images/Image\_1.png](../../images/Image_1.png
"../../images/Image_1.png")
![../../images/Screenshot.1.png](../../images/Screenshot.1.png
"../../images/Screenshot.1.png")

### Status:

Grease Pencil supports getting shading from the scene. However in this
case it will probably be quite tricky to have the shadows on the mesh
nicely transfer over to the strokes.

### Discussion:

Yiming: Currently enable "use lighting" in corresponding GPencil layer
would do the trick. Anything more than that would require some more
development, and also some approximation strategy. One of the problem is
how to determine whether a line is illuminated or shaded if the line
itself is the separation. "Shading" with specified color in shaded
region require more development. Probably with a "main light ray
tracing" method or use some sort of composition mask.

## Texture outline strokes

![../../images/Image\_2.png](../../images/Image_2.png
"../../images/Image_2.png")

### Status:

Not currently supported.

### Discussion:

Yiming: Tricky to do, but achievable.

ZedDB: Perhaps we can use potrace for this?

## Reduced details on distant objects

For characters, only some predetermined lines should vanish.

![../../images/Image\_3.png](../../images/Image_3.png
"../../images/Image_3.png")

### Status:

Currently not supported

### Discussion:

Yiming: Some discussion pointed out that one of the way to do this is
maybe with weighted mesh that tells the renderer to "fade out" some
lines.

K and Q:

Sometimes you want it to be dynamic and sometimes you don't. In the
sample image, I've left only the outline, but that's not always the
case.

In some cases, as in the image, I'll erase a line every single line. In
the case of a character, I've erased the lines of some parts of the
character, such as the eyes, nose, and clothing decorations, to make the
lines less dense and so it does hide the underlying colors.

It may already be supported ( MarkFreestyleEdge? I'm sorry if I'm
wrong), how does it behave when you animate? Will the transparency be
animated? Will it be animated in thickness?

## Outlines

### Status:

Currently not supported

### Discussion:

Yiming: Need either another round of computation or more memory,
probably both. (This is due to the nature of the algorithm).

## Object shown inside a window

### Status:

![../../images/LineArt\_TransparencyWindow\_1.png](../../images/LineArt_TransparencyWindow_1.png
"../../images/LineArt_TransparencyWindow_1.png")
![../../images/LineArt\_TransparencyWindow\_2.png](../../images/LineArt_TransparencyWindow_2.png
"../../images/LineArt_TransparencyWindow_2.png")

Supported as of 0816. Currently allow up to 8 different "window"
materials and their combinations.

### Discussion:

<s>Already have data structure to do so, but the region selection part
is still tricky.</s>

Added one more byte in each triangle and line structures, might not be
ideal but we can squeeze more memory out of them later.

## Details on specific topics

### Line visibility calculation order

I think the main problem is that their method can't really handle
subdivision geometry so they calculate lines in low poly and once the
surface is subdivided, the surface is out of alignment with pre-existing
lines. Or, they are not able to preserve edge marks after subdiv?
Opinions?

LANPR handles geometry and intersections pretty robustly so I don't
really think this is a problem on our side.

### Morhping lines on deformed surfaces

I think the main problem is rigging method. We need to have a preferred
method of projecting the pattern, then the tracing solution is
straightforward.
