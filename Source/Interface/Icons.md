# Icons

There are multiple types of icons in Blender:

1.  \`blender\_icons.svg\` - Main icon sheets.
2.  \`alert\_icons.svg\` - Large icons for dialogs (warning, info, etc).
3.  \`prvicons.svg\` - Used for large preview of documents, drives, etc.
4.  \`brushicons/\` - A folder with PNG images for the built-in brushes
    (sculpt, Grease Pencil, etc)
5.  \`../lib/resources/icon\_geom.blend\` - Tool icons (as in the
    toolbar).

The source files for most icons are stored in \`/release/datafiles/\`,
except the tool icons which are in the \`lib/resources\` [folder in
SVN](https://svn.blender.org/svnroot/bf-blender/trunk/lib/resources/).

## blender\_icons

![../../images/UI\_icons.png](../../images/UI_icons.png
"../../images/UI_icons.png")

These are the small icons used throughout the interface, including
menus.

The source is a vector SVG document containing a grid of icons. A Python
script exports this file into two PNG images using Inkscape, then crops
them into individual bitmaps as .dat files using Blender.

This results in two bitmap sizes, 16x16 and 32x32. We use the former
when the interface is at 1x or smaller, and the larger for all other
sizes. But this means that our resolution maxes out at normal resolution
on high-dpi displays. We may consider replacing both sets with one at
64x64.

### Design

These icons are generally 14x14 within a 16x16 pixel grid. Features
should be aligned to the grid and should not be smaller or narrower than
one unit. These are designed to be viewed at very small sizes so are
purposefully simple and chunky.

If in doubt just copy the style in use in the source document. See the
[Human Interface
Guidelines](../../Human_Interface_Guidelines/Icons.md) on best
practices to design Blender icons. Before adding new icons please
consult with members of the [User Interface
module](../../Modules/index.md#User_Interface).

### Requirements

The following has been tested on GNU/Linux (Ubuntu), and on Windows
through WSL (Windows Subsystem for Linux).

  - To modify or add icons you need to know [how to build
    Blender](../../Building_Blender/index.md)
  - [Inkscape](https://inkscape.org/) 1.0+
      - On Debian/Ubuntu/WSL: \`apt install inkscape\` (Ubuntu 20.04 LTS
        ships with Inkscape 0.9x, you need to get it from the
        \`ppa:inkscape.dev/stable\` PPA).
  - Blender
      - Needs to be accessible by running \`blender\`
  - Python 3+

### How to edit icons

  - Open \`blender\_icons.svg\` in Inkscape
  - Edit or add an icon in one of the empty rows, try to put it in a
    place where it makes sense (a row of similar icons), see
    \`UI\_icons.h\` for reference.
  - Run \`make icons\`
  - Compile Blender (you can use the faster \`make lite\` to quickly
    iterate through versions)

### How to add a new icon

  - Add the icon to \`blender\_icons.svg\`
      - Place the icon in a group named after the coordinates in the
        grid (see others for reference)
      - Respect the alphabetical order already present in the list
  - Open \`/source/blender/editors/include/UI\_icons.h\`
  - Find the section that makes more sense for the icon you want to add.
      - e.g. if you want to add an icon for the Sequencer, look for
        icons starting with \`SEQ\_\`.
  - Find a \`DEF\_ICON\_BLANK(<number>)\` available and replace it with:
      - \`DEF\_ICON(EXAMPLE\_NAME)\`
  - Some icons are coloured by the theme based on their category.
      - \`DEF\_ICON\_SCENE(<name>)\`
      - \`DEF\_ICON\_COLLECTION(<name>)\`
      - \`DEF\_ICON\_OBJECT(<name>)\`
      - \`DEF\_ICON\_OBJECT\_DATA(<name>)\`
      - \`DEF\_ICON\_MODIFIER(<name>)\`
      - \`DEF\_ICON\_SHADING(<name>)\`
      - \`DEF\_ICON\_FOLDER(<name>)\`

`` e.g. If you are adding a new modifier, use `DEF_ICON_MODIFIER(``<name>``)` instead of `DEF_ICON`.``

For other categories read the header of \`UI\_icons.h\`

Your new icon will be accessible just like regular icons:

  - \`layout.prop(sc, "example\_prop", text="My Prop",
    icon='EXAMPLE\_NAME')\`

### Committing the icon

  - Add the icon name to
    \`/source/blender/editors/datafiles/CMakeLists.txt\`
      - Use lowercase, and place it in the same order than in
        \`UI\_icons.h\`
  - \`blender\_icons.svg\`
      - Make sure that the changes only add the icon and nothing else
        (no header changes, metatags, etc).

For reference, [check out this
commit](https://developer.blender.org/rB57c41a9c633e4c48aff716d85a0977e4b39a8bdd)
or [this
commit](https://developer.blender.org/rBce71357711eb0d79916045f1e67523af21b1791e)
that add icons.

## Alert Icons

![../../images/Alert\_Icons.png](../../images/Alert_Icons.png
"../../images/Alert_Icons.png")

These are large icons (256x256) for use in dialog and alert boxes.

  - Edit: \`alert\_icons.svg\`
  - Run \`make icons\`
  - Result: \`alert\_icons.png\`
  - Rebuild Blender.

## File Browser Icons

![../../images/Prvicons.png](../../images/Prvicons.png
"../../images/Prvicons.png")

These are large icons (256x256) for use in the File Browser.

  - Edit: \`prvicons.svg\`
  - Run \`make icons\`
  - Result: \`prvicons.png\`
  - Rebuild Blender.

## brushicons

![../../images/Brush\_Icons.png](../../images/Brush_Icons.png
"../../images/Brush_Icons.png")

Brush icons are simple PNG files that can be edited using any image
editing software like [GIMP](https://www.gimp.org/) or
[Krita](https://www.krita.org/).

These images can be found in \`/release/datafiles/brushicons/\`.
Assigning images to brushes is done manually as part of
\`startup.blend\`.

## icon\_geom

![../../images/Geomicons.png](../../images/Geomicons.png
"../../images/Geomicons.png")

This type of icon, used for toolbar tools, is larger and scalable.

These are created in a blend file and exported as vertex data, color in
8-bit RGBA and position scaled to 8-bit (so an effective resolution of
256x256). The display process involves rasterizing (with CPU) at the
requested size and caching the results for performance. One flaw is that
this single cache is rebuilt if the size changes, so this will happen
constantly if you show multiple sizes simultaneously. We could display
them faster without caching if they were treated as 3D geometry and used
the GPU, but the jittering required for antialiasing messes up the
layers of opacity used in their design. Another issue is that the only
color information is the RGBA values, so we can't make highlight colors
consistent through theming. So this area could use improvement.

You can find the sources of these icons in \`icon\_geom.blend\` in the
library with the Blender dependencies.
(\`../lib/resources/icon\_geom.blend\`). This library is located in
Blender's [Resources
SVN](https://svn.blender.org/svnroot/bf-blender/trunk/lib/resources)
repository.

To create the actual icons, follow the built-in \`README.txt\` inside
\`icon\_geom.blend\`. This will guide you through creating the icon mesh
correctly.

Once you do the geometry in Blender, you can transform it into a .dat
file with the command: \`make icons\_geom\`

## Built-in Assets

When adding matcaps, HDRIs or other EXR files, they must be compressed
to be smaller before being committed using the following command:

\`oiiotool in.exr --ch R,G,B -d half --compression dwab -o out.exr\`
