# Screen

The **bScreen** struct generally contains the positions and arrangements
of areas and regions within a
[Window](Window.md).

A window can contain more than one Screen, but only one is active at a
time. For example, when you have an editor "Maximize Area" it creates
another Screen with just the one
[editor](Editors.md), which is closed when you
are done to reveal the previous layout. The list of screens is kept in
the [Main](../Architecture/ID/Main.md) database struct.
\`WorkSpaceLayout\` has a \`screen\` pointer, and each screen also has a
\`winid\` that matches that of its parent window's \`winid\`.

## How Editors are Arranged in Screens

bScreen is generally keeping track of where each non-global editor
(ScrArea) is in the screen. Global areas (TopBar and StatusBar) are
tracked in the Window itself - you can find this state with
\`ED\_area\_is\_global\`.

Each ScrArea has four *pointers* (V1 - V4) to \`ScrVert\` structs that
live in a list in the bScreen (vertbase), that represent that editor's
four corners. it is important to note that these screen verts are shared
by neighbors. The best way to think about this is that each editor is
not just immediately adjacent to its neighbors, but actually overlap by
a pixel. This way we can move one shared \`ScrEdge\` between them when
resizing, for example.

The internal visible part of each editor (ScrArea-\>totrct) is then
inset from its corners by \`U.pixelsize\` in \`area\_calc\_totrct\`.
This results (when the UI scale is 1) in a 1 pixel gap between each
editor. The border we see is actually wider and drawn around each
editor, overlapping each one a bit. This difference is why can sometimes
see inconsistencies in header height as the size of the occluded portion
can vary.

## Getting the Active Screen

It is a simple call to get the current screen. In most cases you need it
for a user-interactive purpose, so you can often avoid having to test
the return for NULL. The exception to be aware of is when Blender is run
headless from the command-line. You get the current window manager from
the [Context](../Architecture/Context.md) like this:

``` cpp
bScreen *screen = CTX_wm_screen(C);
```

If you have a pointer to a window (active or not), you can also get its
active screen:

``` cpp
bScreen *screen = WM_window_get_active_screen(win);
```

## Screen Operations

Operations with screens are generally about the arrangement and
locations of editors and regions, and with changing them. For example if
you want to know what region is at a particular location you need to
pass the screen to \`BKE\_screen\_find\_region\_xy\`. To find the
largest editor in a window you pass one to
\`BKE\_screen\_find\_big\_area\`. You can iterate over all the screen's
editors with \`ED\_screen\_areas\_iter\`. You can also also alter
screens by calling \`screen\_area\_close\` to remove one,
\`area\_split\` to cut one into two, or \`screen\_area\_join\` to merge
one into another's space.

## API

For more specific information, look at these files:

``` cpp
source/blender/makesdna/DNA_screen_types.h
source/blender/editors/screen/screen_intern.h
source/blender/editors/include/ED_screen.hh
source/blender/editors/screen/screen_draw.cc
source/blender/editors/screen/screen_geometry.cc
source/blender/editors/screen/screen_edit.cc
source/blender/editors/screen/screen_ops.cc
```
