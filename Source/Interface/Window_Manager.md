# Window Manager

The **wmWindowManager** struct can be considered the parent of all user
interaction. At its simplest you could think of it as the list of all
the [Windows](Window.md), but in reality it
contains much data about the current running state of the user
interface. This includes which window is active and which is drawable,
operators list, reports, jobs, key configs, [undo
stack](../Architecture/Undo.md), and much more.

## Getting the Window Manager

It is a simple call to get the current Window Manager. In most cases you
need it for a user-interactive purpose, so you can often avoid having to
test the return for NULL. The exception to be aware of is when Blender
is run headless from the command-line. You get the current window
manager from the [Context](../Architecture/Context.md) like
this:

``` cpp
wmWindowManager *wm = CTX_wm_manager(C);
```

## API

For more specific information, look at these files:

``` cpp
source/blender/makesdna/DNA_windowmanager_types.h
source/blender/windowmanager/intern/wm.cc
```
