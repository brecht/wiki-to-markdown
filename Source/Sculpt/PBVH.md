Sculpt mode uses a coarse BVH tree, \`PBVH\`. The leaves are fairly
large compared to most BVHs; this is because the leaves are used to
split the GPU mesh.

# Data modes

PBVH has three different modes, each of which has a different data
backing:

1.  PBVH\_FACES - Normal [Mesh](/Source/Objects/Mesh) data.
2.  PBVH\_BMESH - Dynamic topology, uses
    [BMesh](../Modeling/BMesh/Design.md).
3.  PBVH\_GRIDS - Multiresolution data.

You get can the mode with \`BKE\_pbvh\_type\`.

# Vertex/Face references

The PBVH API stores vertex references in a special structure,
\`PBVHVertRef\`. It contains a single \`intptr\_t\` value, the meaning
of which varies by PBVH data mode. \`PBVH\_BMESH\` stores pointers in
the value, while the other two store indices. The null reference is
PBVH\_REF\_NONE.

# Vertex Iteration

You can iterate over the vertices in a \`PBVHNode\`. There are two modes
for this, \`PBVH\_ITER\_UNIQUE\` and \`PBVH\_ITER\_ALL.\`
\`PBVH\_ITER\_UNIQUE\` iterates over the vertices uniquely assigned to
this node, while \`PBVH\_ITER\_ALL\` iterates over the vertices that
belong to this node's faces.

    PBVHVertIter vd;
    BKE_pbvh_vertex_iter_begin(pbvh, node, vd, PBVH_ITER_UNIQUE) {
      //PBVHVertIter has various bits of data:
      //vd.co    - position
      //vd.no    - normal
      //vd.mask  - pointer to mask
      //vd.color - active color attribute data, if it exists
    }
    BKE_pbvh_vertex_iter_end;
