# Code & Functional Design: Animation & Rigging Module

This section describes the code design and functional requirements of
Animation & Rigging related features.

## Core Concepts

  - [Active Keyframe](Active_Keyframe.md)
  - [Animation Channel
    Filtering](Source/Animation/Animation_Channel_Filtering),
    used for obtaining animation channels for drawing in UIs, performing
    operations, and evaluating animation.
  - [Animation Data](Source/Animation/Animation_Data), how
    animation data is attached to ID datablocks.
  - [Armature/Skeletal
    Animation](Source/Animation/Armature_Animation), how pose
    data is stored, evaluated, and applied.
  - [Armature/Skeleton
    Editing](Source/Animation/Armature_Editing), how
    armatures are created and edited, including bone naming conventions,
    mirrorring, symmetrizing.
  - [Constraints](Source/Animation/Constraints)
  - [Evaluation Pipeline](Source/Animation/Evaluation), how
    animation data is evaluated and applied.
  - [IK: iTaSC algorithm](IK.md)
  - [Interpolation /
    Blending](Source/Animation/Interpolation)
  - [Keying Sets and
    Auto-Keying](Source/Animation/Keying_Sets_and_Auto-Keying)
  - [Motion Paths](Source/Animation/Motion_Paths)
  - [Parenting](Parenting.md)
  - [Quaternions](Source/Animation/Quaternions)
  - [B-Bone Vertex
    Mapping](B-Bone_Vertex_Mapping.md)

## Tools

  - [In-Between tools](Source/Animation/Tools/In-Between),
    like the Pose Breakdowner, Relax To Rest Pose, etc.
  - [Pose Library](Tools/Pose_Library.md)

## Animation & Rigging Related Editors

  - [Dope Sheet / Action Editor](Source/Animation/Dope_Sheet)
  - [Graph Editor](Source/Animation/Graph_Editor)
  - [Non-Linear Animation](NLA.md)
  - [Timeline](Source/Animation/Timeline)

## Historical Design

[Animato](Animato.md) is the animation system
introduced by Joshua Leung in 2009. Many of its parts are still in use
in Blender. The design document is spread between the Wiki, a
Word-document, and screenshotted text in that document. It's probably
best to extract the still-current information from it, and move it into
the pages linked above.
