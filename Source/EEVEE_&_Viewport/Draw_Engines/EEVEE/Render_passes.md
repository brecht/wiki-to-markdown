## EEVEE Render Passes

EEVEE supports render passes. This page does over some of the details
how these render passes extracts the needed data during rendering. The
original implementation was done in [http://developer.blender.org/T72007
\#72007](http://developer.blender.org/T72007_#72007).

### Overall process

This section describes the overall process how render passes are
extracted and calculated in EEVEE. The process has 3 phases.

1.  Allocate the buffers needed to store the data. Most of the passes
    use a accumulated buffer.
2.  During rendering per sample the pass data can be extracted from
    EEVEE and added to the accumulate buffers.
3.  After rendering or just before displaying the buffers are post
    processed to get the actual value of the render pass.

#### Implementation

\`EEVEE\_renderpasses.c\` is the central point for render passes.

  - \`EEVEE\_renderpasses\_init\` will determine what render\_passes are
    needed to be calculated. Passes like the light passes can activate
    other passes when they rely on them. Light passes for example will
    store the light multiplied by the color in one buffer, but in the
    end divide the result by another buffer that only contain the color
    part.
  - \`EEVEE\_renderpasses\_output\_init\` will allocate all the needed
    \`GPUTexture\`, \`DRWPass\` and \`DRWShadingGroup\` for that active
    render passes. Based on the number of samples that needs to be
    calculated a more precise GPUTexture will be allocated. This
    increases the performance with few samples, but increases the
    quality when more samples are used.
  - \`EEVEE\_renderpasses\_output\_accumulate\` Is called for every
    sample. At this moment every render pass can extract/render the data
    it needs and store it in their buffers. This function is called
    twice per sample. Once during just after a sample has drawn and once
    when samples have been merged (for effects that work on the
    integrated result like bloom).
  - \`EEVEE\_renderpasses\_postprocess\`: As final step when rendering,
    or just before displaying a render pass to the screen the buffers
    will be post processed to get the actual values for the render
    passes.

**Materials**

For material based passes (\`EEVEE\_RENDER\_PASS\_EMIT\`,
\`EEVEE\_RENDER\_PASS\_DIFFUSE\_COLOR\`,
\`EEVEE\_RENDER\_PASS\_DIFFUSE\_LIGHT\`,
\`EEVEE\_RENDER\_PASS\_GLOSSY\_COLOR\`,
\`EEVEE\_RENDER\_PASS\_GLOSSY\_LIGHT\`, \`
EEVEE\_RENDER\_PASS\_ENVIRONMENT\`) we added another mechanism. The
reason is that the BSDF shader outputs the full composited color for
these passes and we needed to add a mechanism to extract different
components from the output.

The main idea behind the implementation is that the BSDF shader can be
configured using uniforms to only output a subset of the individual
components. For the active material based render passes the sample will
be re-rendered, but with different uniforms (stored in
\`EEVEE\_RenderPassData\`) to extract the required components. The BSDF
shaders can be configured this way to output one of the next set of
components:

| Render Pass                         | Diffuse Light | Diffuse Color | Glossy Light | Glossy Color | Emission | Subsurface Albedo | Effective Radiance                                                  |
| ----------------------------------- | ------------- | ------------- | ------------ | ------------ | -------- | ----------------- | ------------------------------------------------------------------- |
| EEVEE\_RENDER\_PASS\_COMBINED       | V             | V             | V            | V            | V        |                   | \`radiance = dif\_light\*dif\_col + glos\_light\*glos\_col + emit\` |
| EEVEE\_RENDER\_PASS\_DIFFUSE\_LIGHT | V             | V             |              |              |          |                   | \`radiance = dif\_light\*dif\_col\`                                 |
| EEVEE\_RENDER\_PASS\_DIFFUSE\_COLOR |               | V             |              |              |          | V                 | \`radiance = dif\_col + sss\_albedo\`                               |
| EEVEE\_RENDER\_PASS\_GLOSSY\_LIGHT  |               |               | V            | V            |          |                   | \`radiance = glos\_light\*glos\_col\`                               |
| EEVEE\_RENDER\_PASS\_GLOSSY\_COLOR  |               |               |              | V            |          |                   | \`radiance = glos\_col\`                                            |
| EEVEE\_RENDER\_PASS\_EMIT           |               |               |              |              | V        |                   | \`radiance = emit\`                                                 |
| EEVEE\_RENDER\_PASS\_ENVIRONMENT    | V             | V             | V            | V            | V        |                   |                                                                     |

For simplicity the refraction components are added to glossy and the
transmission components are added to the diffuse.

` In the future we want to enhance this mechanism to use multi-target rendering. This would bring us a step closer to use compositing directly in the viewport.`

**Post Processing**

There is a screen space fragment shader that can be used for post
processing. All of them are implemented in
\`renderpass\_postprocess\_frag.glsl\`. There are several kind of
post-processings where a renderpass can select from.

  - \`PASS\_POST\_ACCUMULATED\_COLOR\`: Will read a color from the
    accumulated buffer and divide it with the current sample number.
  - \`PASS\_POST\_ACCUMULATED\_LIGHT\`: Will read a (color multiplied by
    light) from the accumulated buffer and divide it by the color in a
    color buffer.
  - \`PASS\_POST\_ACCUMULATED\_VALUE\`: Will read a value from the
    accumulated buffer and divide it with the current sample number.
  - \`PASS\_POST\_DEPTH\`: Will read the depth from the depth buffer and
    de-normalize the value to a view depth.
  - \`PASS\_POST\_AO\`: Will read the accumulated AO value clamps the
    value.
  - \`PASS\_POST\_NORMAL\`: Normals in EEVEE are encoded to save GPU
    memory and to increase performance. This post process will decode
    the encoded normals to how Blender stores normals.
  - \`PASS\_POST\_TWO\_LIGHT\_BUFFERS\`: Screen space reflections are
    calculated separate from the glossy color/light pass. This post
    process will add the screen space reflection to the accumulated
    light+color buffer before dividing it by the color buffer. This way
    the screen space reflections are part of the glossy passes. This
    post processing is also done for the diffuse light pass when SSS is
    enabled.

### Specific Passes

This section gives an overview of how the different render passes are
implemented.

**Glossy Light**

If Screen Space Reflections are enabled
\`EEVEE\_RENDER\_PASS\_GLOSSY\_LIGHT\` will also stores the results of
the Screen Space Reflections in a accumulate buffer. During Post
Processing the Glossy light is calculated as

``  `final_glossy_light = (glossy_light * glossy_color + screen_space_reflection) / glossy_color` ``

When Screen Space Reflections are disabled the glossy light is
calculated as

``  `final_glossy_light = (glossy_light * glossy_color) / glossy_color` ``

**Diffuse Light**

If SSS are enabled \`EEVEE\_RENDER\_PASS\_DIFFUSE\_LIGHT\` will also
stores the results of the SSS in a accumulate buffer. During Post
Processing the Diffuse light is calculated as

``  `final_diffuse_light = (diffuse_light * diffuse_color + sss_light * sss_albedo) / diffuse_color` ``

When SSS are disabled the diffuse light is calculated as

``  `final_diffuse_light = (diffuse_light * diffuse_color) / diffuse_color` ``

SSS output is initialized after all the materials have been added to the
draw cache. At this moment we can decide if SSS is needed.

**Volume Transmittance/Scattering Passes**

See \`EEVEE\_volumes\_output\_init\` and
\`EEVEE\_volumes\_output\_accumulate\`.

**Shadow**

The shadow pass is implemented as a screen space shadow pass. Check
\`EEVEE\_shadow\_output\_init\` and
\`EEVEE\_shadow\_output\_accumulate\` and \`shadow\_accum\_frag.glsl\`.
It uses the shadowing component of all light objects and divide them by
the number of lights.

**Bloom**

The bloom effect isn't sample based when needed it can be calculated
from the final color buffer. When needed we execute the resolve pass,
but with a different blend mode so the bloom effect is stored and not
added on top of the framebuffer.

### Adding New Passes

When adding new passes the next steps can help:

  - Add the pass to DNA\_layer\_types.h - \`eViewLayerEEVEEPassType\`
  - Add the pass to rna\_scene.c - \`rna\_def\_view\_layer\_eevee\`
  - Add the pass to rna\_space.c -
    \`rna\_enum\_view3dshading\_render\_pass\_type\_items\`
  - Add the new ViewLayerEEVEE property to properties\_view\_layer.py
  - In eevee\_renderpasses.c: Add the render pass to defines where
    needed
  - Implement an \`EEVEE\_\*\_output\_init\` function and add it to the
    \`EEVEE\_renderpasses\_output\_init\`
  - Implement an \`EEVEE\_\*\_output\_accumulate\` function and add it
    to the \`EEVEE\_renderpasses\_output\_accumulate\`
  - Check if a special case needs to be added to
    \`EEVEE\_renderpasses\_draw\`
  - Add the render pass to \`EEVEE\_renderpasses\_postprocess\`
  - In eevee\_render.c update \`EEVEE\_render\_update\_passes\` (add a
    \`CHECK\_PASS\_EEVEE\`)
  - In eevee\_render.c update \`EEVEE\_render\_draw\` add
    \`eevee\_render\_result\_\*\` call and implement that function. This
    last part will perform the post processing on the GPU, download the
    final image and store it in the render result.
