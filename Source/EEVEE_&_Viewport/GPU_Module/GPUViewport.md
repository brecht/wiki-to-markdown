# GPUViewport

The GPUViewport (\`gpu/intern/gpu\_viewport.c\`) contains the render
buffers, framebuffer and engine data for an editor. Editors that make
use of it are the 3D Viewport and the UV/Image editor. Other editors use
offscreens.

## Data management

Any draw engine can store data in the 3d viewport. This data is
organized in 4 lists:

  - Passes
  - Framebuffers
  - Textures
  - Other data

Engine can store data here as a cache specific for the editor. It is
also used for TAA where the draw engine continues where it stopped the
previous redraw. Workbench and EEVEE make use of this.

## Color Management

A \`GPUViewport\` has a depth- and 2 color-textures. The main color
texture (\`DefaultTextureList.color\`) is used for drawing in Linear
Scene Reference Space. It is used by the render and draw engines
(Cycles, EEVEE, Workbench or any external engine) to draw the scene. The
second color texture (\`DefaultTextureList.color\_overlay\`) is the
overlay texture. This texture is in linear display space. The color
overlay texture is encoded in SRGB to save GPU memory. These textures
can be retrieved by calling \`GPU\_viewport\_texture\_list\_get\`. There
are also some predefined framebuffers that can be used. The framebuffers
can be retrieved by calling \`GPU\_viewport\_framebuffer\_list\_get\`.
There is a shared depthbuffer that is used by both color textures.

When drawing the GPUViewport to the display the textures are combined
and transformed to display space. For this the default texture is
transferred to display space (in rendered mode the view transform is
also applied). The overlay texture is transferred (linear to non-linear)
to match the display space.

This process is normally hidden for the development as it is triggered
by \`WM\_draw.c\#wm\_draw\_region\_blit\`, which calls
\`GPU\_viewport\_draw\_to\_screen\`. The actual drawing is done in
\`gpu\_viewport\_draw\_colormanaged\`. Inside the draw manager the
function \`drw\_viewport\_colormanagement\_set\` determines the color
spaces and related configuration. The fragment shader that does the
conversion is \`gpu\_shader\_display\_transform\` which is located in
\`/intern/opencolorio/\`.

## Stereo rendering
