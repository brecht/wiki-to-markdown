## Copyright Rules

The Blender project encourages originality and innovation in all aspects
of the development process. We expect contributions or modification
proposals on blender.org to be presented with original mockups and
designs that are self-explanatory, using concepts and methods that build
on top of the rich original design history of Blender itself.

Contributors to blender.org channels should avoid using references to
proprietary or strictly copyrighted software products. In cases where it
is essential to mention such a product, limit it to referring to public
information that’s (for example) on a website like Wikipedia.

We do not allow copyrighted elements from proprietary software being
added to any part of Blender, most importantly because that would
violate the proprietary software’s EULA. Any user of proprietary
software should be carefully aware that you don’t enjoy the freedom to
share that. A freedom you obviously do have for everything we do within
the Blender project.

The moderators and administrators on blender.org reserve the right to
remove screenshots or texts they consider potentially damaging for
Blender. In case you’re in doubt about posting something, please ask for
advice from one of the Blender module team members.

— Blender Foundation, January 2021
