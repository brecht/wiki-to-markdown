# Communication

<div class="bd-lead">

Blender is a community project. It relies on its users to support each
other, spread news, and continuously improve Blender.

</div>

With millions of downloads, Blender has relatively few contributors. It
is essential that information flows in both directions between users and
developers in a useful manner.

### Copyright and Guidelines

Blender is an open source project, but developers and contributors need
to take copyrights of proprietary software into account. Please read the
[Copyright Rules](Contact/CopyrightRules) carefully before
you start contemplating a contribution to Blender / submit a proposal to
any of the Blender communication platforms.

General guidelines for communication have been outlined in our [Code of
Conduct](Contact/CodeOfConduct).

## Contact Developers

### Developer Forum

The official development discussion platform is
[DevTalk](https://devtalk.blender.org/). A few useful sections:

  - [Building
    Blender](https://devtalk.blender.org/c/contributing-to-blender/11)
  - [Contributing to
    Blender](https://devtalk.blender.org/c/contributing-to-blender/29)
  - [Documentation and
    Translations](https://devtalk.blender.org/c/contributing-to-blender/12)

Until 2023, mailing lists were used to coordinate development, and were
progressively replaced with the DevTalk forum. The lists archives are
available at [archive.blender.org](https://archive.blender.org/lists/).

### Chat

Most developers are active on the [\#blender-coders channel on
blender.chat](https://blender.chat/channel/blender-coders).

See here for a full [overview of popular chat
channels](Chat.md).

### Vulnerability Reporting

While most bug reporting should be done via Blender's bug tracker,
sensitive vulnerabilities may be reported privately, see: [Vulnerability
Reporting](../../Process/Vulnerability_Reports.md).

## Decisions and Planning

Blender's code is organized in modules; libraries or directories with
files sharing a certain functionality. For each module, the module
owners are responsible for maintenance and bug fixes. They also define -
together with their team - roadmaps or todos for the module, aligned
with the overall release cycles and roadmaps.

### Weekly Development Updates

Every Monday an update report is published to
[devtalk](https://devtalk.blender.org/c/announcements/weekly-updates/14).
This is the one document to direct anyone interested on finding about
what is happening in Blender.

This report is compiled by multiple hands:

  - Announcements: Everyone.
  - Modules and Projects: Modules and project members.
  - New Features and Changes:
      - The list of commits is organized manually, but cleanup and fix
        commits are filtered out automatically with this script: \`git
        log --no-merges --since 7.days.ago
        --pretty=format:"%h%x09%ar%x09%s
        (\[commit\](https://projects.blender.org/blender/blender/commit/%h))
        (\_%an\_)" | grep -v "Fix " | grep -v "Cleanup:"\`
  - Welcomes, final edit and publishing: Development Coordinator
    (*Thomas Dinges*).

### Modules Meetings

Most modules have regular meetings. Their announcements and final notes
are in [devtalk](https://devtalk.blender.org/tag/meeting).

## User Feedback and Requests

Blender has magnitudes of more users than people who are involved with
making it. That makes handling user feedback and feature requests - or
even tracking them sanely - nearly impossible.

Instead we try to organize ourselves in ways that development and users
can cooperate closely. For each module in Blender, there's a small and
competent team working on it - including users helping out. You can find
this information listed above. Feel welcome to use our channels and get
involved with making Blender better.

As an alternatives we like to mention:

  - Add ideas on the community-run [Right-Click
    Select](https://rightclickselect.com/p/) website
  - Make sure your proposal is documented and published in public. Post
    this on the appropriate public lists, or on public forums (such as
    Blenderartists.org). It doesn't really matter where, a lot of users
    out there probably can tell you whether it's already there, already
    planned, a great new idea, or simply not possible.
