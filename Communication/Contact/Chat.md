# Chat

[blender.chat](https://blender.chat) is the platform where many Blender
developers and users hang out. See the [channel
listing](https://blender.chat/directory) for all available channels. The
most popular channels are listed below.

Previous mostly activity was on IRC, and a few channels are still active
on [libera.chat](https://libera.chat/).

## Channels

### Developers

**[\#blender-coders](https://blender.chat/channel/blender-coders)**:
Official Blender development chat room for discussions about Blender
core development, where most Blender developers hang out.

**[\#docs](https://blender.chat/channel/docs)**: Official Blender
documentation chat room where discussions about the Blender manual.

**[\#python](https://blender.chat/channel/python)**: If you need help
regarding Blender's Python functionality, this is the place to visit.

**[\#blender-builds](https://blender.chat/channel/blender-builds)**:
Need help compiling the Blender source code? Although it's quick and
easy for some people, not everyone is as lucky getting Blender to build
on their platform. Drop by if you need assistance.

#### Modules

**[\#animation-module](https://blender.chat/channel/animation-module)**:
Animation development

**[\#blender-triagers](https://blender.chat/channel/blender-triagers)**:
Bug triaging team coordination

**[\#core-module](https://blender.chat/channel/core-module)**: Core
module development

**[\#data-assets-io-module](https://blender.chat/channel/data-assets-io-module)**:
Data, Assets & IO development

**[\#eevee-viewport-module](https://blender.chat/channel/eevee-viewport-module)**:
EEVEE & Viewport development

**[\#geometry-nodes](https://blender.chat/channel/geometry-nodes)**:
Geometry Nodes development

**[\#grease-pencil-module](https://blender.chat/channel/grease-pencil-module)**:
Grease Pencil development

**[\#render-cycles-module](https://blender.chat/channel/render-cycles-module)**:
Render & Cycles development

**[\#nodes-physics-module](https://blender.chat/channel/nodes-physics-module)**:
Nodes & Physics development

**[\#sculpt-paint-texture-module](https://blender.chat/channel/sculpt-paint-texture-module)**:
Sculpt, Paint and Texture development

**[\#user-interface-module](https://blender.chat/channel/user-interface-module)**:
User Interface development

### Artists and Users

**[\#support](https://blender.chat/channel/support)**: General support
channel for those seeking help and advice with Blender, in a family
friendly moderated environment.

**[\#general](https://blender.chat/channel/general)**: Where
Blenderheads meet and chat, ask questions, or just have fun\!

**[\#vse](https://blender.chat/channel/vse)**: Support channel for
discussions about the video sequence editor, rendering, encoding,
processing, color spaces and editing in general.

**[\#gameengines-armoury-godot-up-bge-panda-etc](https://blender.chat/channel/gameengines-armoury-godot-up-bge-panda-etc)**:
Blender no longer has an integrated game engine. But here you can find
discussions about the game engines, and their integrations.

### International

**[\#aidez-moi](https://blender.chat/channel/aidez-moi)** and
**[\#blender-fr](https://blender.chat/channel/blender-fr)**: Blender
chat in French

**[\#blender-heute](https://blender.chat/channel/blender-heute)**:
Blender chat in German

**[\#vandaag](https://blender.chat/channel/vandaag)**: Blender chat in
Dutch

**[\#hoy](https://blender.chat/channel/hoy)**: Blender chat in Spanish

### Support

For issues on blender.chat please go to the
**[\#blender-chat-meta](https://blender.chat/channel/blender-chat-meta)**
channel.

## IRC

While most activity has moved to blender.chat, some channels remain
active. Previously freenode.net was used. Like much of the open-source
community, activity has moved to libera.chat.

Note that the Blender Foundation no longer has an official presence on
IRC, channels here are community operated.

### Channels

**[\#blender](irc://irc.libera.chat/blender)**: General support channel
for those seeking help and advice with Blender, in a family-friendly
moderated environment.

**[\#blendercoders](irc://irc.libera.chat/blendercoders)**: Development
discussion channel.

### Using IRC

Many IRC clients are available. The easiest way to get started is to use
the [libera.chat web client](https://web.libera.chat/).
