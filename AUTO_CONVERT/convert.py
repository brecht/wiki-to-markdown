#!/usr/bin/python3

import os
import pathlib
import re
import requests
import subprocess
import tempfile
import time

scripts_path = pathlib.Path(os.path.abspath(__file__)).parent
out_path = scripts_path.parent
wiki_path = scripts_path / "wiki"
pages_path = scripts_path / "pages.txt"
remap_path = scripts_path / "remap.txt"
images_path = out_path / "images"
videos_path = out_path / "videos"

tmp_dir = tempfile.TemporaryDirectory()
tmp_path = pathlib.Path(tmp_dir.name)

log_file = open(scripts_path / "convert.log", "w")

def log(text):
    print(text)
    log_file.write(text + "\n")

# Downloaded wiki pages with templates expanded
pages = pages_path.read_text().strip().replace(' ', '_').split('\n')
pages = [page.strip('/') for page in pages]
convert_paths = []
known_md_links = set()
known_directories = set()

remaps = remap_path.read_text().strip().split('\n')
remaps = [line.split(" ", 1) for line in remaps]

def remap_page(page):
    for remap_from, remap_to in remaps:
        if page.startswith(remap_from):
            page = page.replace(remap_from, remap_to, 1)
    return page

for page in pages:
    if page.find("/") != -1:
        known_directories.add(page.rsplit('/', 1)[0])

for page in pages:
    page_path = wiki_path / pathlib.Path(page + ".wiki")
    md_path = out_path / pathlib.Path(remap_page(page) + ".md")

    if str(page_path.with_suffix("").relative_to(wiki_path)) in known_directories:
        md_path = md_path.with_suffix("") / "index.md"
    
    convert_paths += [(page_path, md_path)]
    known_md_links.add(str(md_path))

    if page_path.exists():
        # print(f"CACHED {page}")
        continue

    params = {
        "action": "expandtemplates",
        "text": "{{:" + page + "}}",
        "prop": "wikitext",
        "format": "json"}

    response = requests.get(url="https://wiki.blender.org/w/api.php", params=params)
    if response.status_code == requests.codes.ok:
        log(f"GOT {page_path.relative_to(wiki_path)}")
        text = response.json()['expandtemplates']['wikitext']
        page_path.parent.mkdir(exist_ok=True, parents=True)
        page_path.write_text(text)
    else:
        log(f"ERROR {response.status_code} {page_path.relative_to(wiki_path)}")

    time.sleep(1.0)

files = []

# Convert mediawiki to markdown
for page_path, md_path in convert_paths:
    if not page_path.exists():
        continue

    md_path.parent.mkdir(exist_ok=True, parents=True)

    log(f"CONVERT {md_path.relative_to(out_path)}")

    # Gather files, and make paths relative
    def detect_file(matchobj):
        global files
        filename = matchobj.group(1).strip().replace(" ", "_")
        if filename.endswith(".mov") or filename.endswith(".mp4") or filename.endswith(".webm"):
            filepath = videos_path / filename
        else:
            filepath = images_path / filename
        files += [filepath]
        filepath = os.path.relpath(filepath, md_path.parent)
        return matchobj.group(0).replace(matchobj.group(1), filepath)

    wiki_text = page_path.read_text()
    wiki_text = re.sub(r"\[\[File:([\w. -_]+)\]\]", detect_file, wiki_text)
    wiki_text = re.sub(r"\[\[File:([\w. -_]+)\|", detect_file, wiki_text)
    wiki_text = re.sub(r"\[\[Image:([\w. -_]+)\]\]", detect_file, wiki_text)
    wiki_text = re.sub(r"\[\[Image:([\w. -_]+)\|", detect_file, wiki_text)
    ## Workaround table parse error with pandoc.
    wiki_text = re.sub(r"^ \|", "|", wiki_text, flags=re.M)

    tmp_page_path = tmp_path / page_path.name
    tmp_page_path.write_text(wiki_text)
    subprocess.run(["pandoc", "--from", "mediawiki", "--to", "gfm", tmp_page_path, "-o", md_path])

    # Fix links to be relative
    def relative_link(matchobj):
        link = matchobj.group(1)
        if link.startswith("http:") or link.startswith("https:"):
            return matchobj.group(0)

        page_link = link
        internal_link = None
        if link.find('#') != -1:
            page_link, internal_link = link.split('#', 1)

        page_link = remap_page(page_link)

        md_link = out_path / page_link / "index.md"
        if str(md_link) not in known_md_links:
            md_link = out_path / (page_link + ".md")
            if str(md_link) not in known_md_links:
                log(f"MISSING LINK {link}")
                return matchobj.group(0)

        md_link = os.path.relpath(md_link, md_path.parent)
        if internal_link:
            md_link += "#" + internal_link
        return matchobj.group(0).replace(link, md_link)

    if md_path.exists():
        md_text = md_path.read_text()
        md_text = re.sub('\]\(([\w. -_]+) "wikilink"\)', relative_link, md_text)
        md_text = md_text.replace(' "wikilink")', ")")
        md_path.write_text(md_text)

# Download files
for i, filepath in enumerate(files):
    progress = f"{filepath.name} ({i+1} / {len(files)})"

    if filepath.exists():
        # print(f"CACHED {progress}")
        continue

    filepath.parent.mkdir(exist_ok=True, parents=True)

    url = "https://wiki.blender.org/wiki/Special:Redirect/file/" + filepath.name
    response = requests.get(url)
    if response.status_code == requests.codes.ok:
        log(f"GOT {progress}")
        with open(filepath, "wb") as f:
            f.write(response.content)
    else:
        log(f"ERROR {response.status_code} {progress}")

    time.sleep(1.0)
