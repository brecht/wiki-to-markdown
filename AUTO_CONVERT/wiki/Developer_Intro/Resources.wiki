=== Video Resources for Code Navigation and Set-up ===
The videos can give a fair idea of the workflow.

=== Small ===
* [https://www.youtube.com/watch?v=_kniMTbcCko&t=38s Campbell's Tips for Code Navigation]
* [https://www.youtube.com/watch?v=5Ymoav0nNWQ Campbell's Code Navigation and Code Fix]

=== Big ===
* [https://www.youtube.com/watch?v=tCdx7gzp0Ac Dalai's Code Dive Video]
* [https://www.youtube.com/watch?v=tQzKjf2_Hmk&t=157s Dalai's Bug Fix]