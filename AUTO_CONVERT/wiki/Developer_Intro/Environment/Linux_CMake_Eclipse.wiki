== Starting out ==
* This document assumes you have built blender using the [[Building_Blender/Linux/Ubuntu/CMake|Linux/CMake Quickstart]].
* Tested with Eclipse 3.7.2

== Create Project Files ==
Create the project files. in your existing cmake build dir run.
  cmake . -G"Eclipse CDT4 - Unix Makefiles"

== Import Project ==

* Start Eclipse (for the first time)
* Close Welcome Screen
* File -> Import -> General -> Existing Project into Workspace
* Select root Directory (Browse to your CMake build dir)<br>... (acceps defaults), press 'Finish'

== Configure Eclipse ==
Open preferences: (Menu) Window -> Preferences
* General -> Editors -> Text Editors
** Enable 'Show Print Margin', set to 120
** Enable 'Show Line Numbers'

* General -> Workspace
** Disable 'Build Automatically'
** Enable 'Refresh on Access'

* C/C++
** Indexer
*** Disable 'Index Source Files not included in the build'
*** Skip Files Larger then '1'mb

(Optional)
* C/C++
** Editor
*** Typing
**** Disable all 'Automatically Close' options.

== Configure C/C++ Code Style ==

See: [[Source/Code_Style/Configuration#Eclipse]]