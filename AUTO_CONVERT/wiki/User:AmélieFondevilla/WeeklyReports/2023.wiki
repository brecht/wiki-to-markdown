= Weekly Reports 2023 =

Unless specific mention, all my work is related to Grease Pencil 3 (GPv3).

== August 21 - August 25 (week 9) ==
Last week at Blender HQ. I finished the patch for soft eraser. Many patches are pending for review until Falk comes back from his holidays.

Merged : 
 - Display layer groups in grease pencil dopesheet ([https://projects.blender.org/blender/blender/pulls/111015 111015]).
 - Fix: Stroke Eraser does not work with one-point strokes ([https://projects.blender.org/blender/blender/pulls/111387 111387]).
 - Refactor code for the hard eraser ([https://projects.blender.org/blender/blender/pulls/111390 111390]).

In review:
 - Soft Eraser tool ([https://projects.blender.org/blender/blender/pulls/110310 110310]).
 - Duplicate keyframes ([https://projects.blender.org/blender/blender/pulls/111051 111051]).
 - Frame All/Selected keyframes ([https://projects.blender.org/blender/blender/pulls/111480 111480]).
 - Jump to Selected/Previous/Next keyframe for grease pencil frames ([https://projects.blender.org/blender/blender/pulls/111476 111476]).
 - Set keyframe type ([https://projects.blender.org/blender/blender/pulls/111472 111472]).
 - Snap selected grease pencil frames ([https://projects.blender.org/blender/blender/pulls/111507 111507]). 
 - Mirror grease pencil frames ([https://projects.blender.org/blender/blender/pulls/111511 111511]). 
 - Frame selected channels for grease pencil ([https://projects.blender.org/blender/blender/pulls/111512 111512]).

== August 14 - August 18 (week 8) ==
This week I worked on an improved version of the soft eraser, which is still in review, but significantly changed. Plus, I addressed the changes requested on 111015 and 111051 which should be merged soon.

Merged : 
 - Fix layer renaming synchronization from the dopesheet ([https://projects.blender.org/blender/blender/pulls/111038 111038]) 

In review:
 - Soft Eraser tool ([https://projects.blender.org/blender/blender/pulls/110310 110310]).
 - Display layer groups in grease pencil dopesheet ([https://projects.blender.org/blender/blender/pulls/111015 111015]).
 - Duplicate keyframes ([https://projects.blender.org/blender/blender/pulls/111051 111051]).

== August 7 - August 11 (week 7) ==

Merged : 
 - Fix floating-point error on the hard eraser ([https://projects.blender.org/blender/blender/pulls/110801 110801]).
 - Transform action for grease pencil frames ([https://projects.blender.org/blender/blender/pulls/110743 110743]).
 - Display layer properties in animation channels ([https://projects.blender.org/blender/blender/pulls/110991 110991])
 - Include summary and datablock keyframes in the grease pencil dopesheet ([https://projects.blender.org/blender/blender/pulls/110962 110962])

In review:
 - Soft Eraser tool ([https://projects.blender.org/blender/blender/pulls/110310 110310]).
 - Display layer groups in grease pencil dopesheet ([https://projects.blender.org/blender/blender/pulls/111015 111015]).
 - Duplicate keyframes ([https://projects.blender.org/blender/blender/pulls/111051 111051]).

== July 31 - August 4 (week 6) ==

Merged : 
 - Insert Keyframes ([https://projects.blender.org/blender/blender/pulls/110649 110649]).
 - Delete Keyframes ([https://projects.blender.org/blender/blender/pulls/110746 110746]).

In review:
 - Soft Eraser tool ([https://projects.blender.org/blender/blender/pulls/110310 110310]).
 - Fix floating-point error on the hard eraser ([https://projects.blender.org/blender/blender/pulls/110801 110801]).
 - Transform action for grease pencil frames([https://projects.blender.org/blender/blender/pulls/110743 110743]).

== July 25 - July 28 (week 5) ==
I've created a todo list for the dopesheet support : [https://projects.blender.org/blender/blender/issues/110056 110056].

Some of them are now community tasks :
 - Select box ([https://projects.blender.org/blender/blender/issues/110521 110521]),
 - Select circle/lasso ([https://projects.blender.org/blender/blender/issues/110522 110522]).

Merged : 
 - Stroke Eraser tool ([https://projects.blender.org/blender/blender/pulls/110304 110304]).
 - Click selection for frames ([https://projects.blender.org/blender/blender/pulls/110492 110492]).
 - Column-based selection in the dopesheet ([https://projects.blender.org/blender/blender/pulls/110523 110523]).
 - Filtering grease pencil channels in the dopesheet ([https://projects.blender.org/blender/blender/pulls/110484 110484]).
 - Some smaller commits for cleanup and/or refactor ([https://projects.blender.org/blender/blender/pulls/110525 110525], [https://projects.blender.org/blender/blender/pulls/110460 110460], and [https://projects.blender.org/blender/blender/pulls/110486 110486])

In review:
 - Soft Eraser tool ([https://projects.blender.org/blender/blender/pulls/110310 110310]).

== July 17 - July 21 (week 4) ==
Merged : 
 - Smoothing algorithm ([https://projects.blender.org/blender/blender/pulls/109635 109635]).
 - Hard Eraser tool ([https://projects.blender.org/blender/blender/pulls/110063 110063]).

In review:
 - Stroke Eraser tool ([https://projects.blender.org/blender/blender/pulls/110304 110304]).
 - Soft Eraser tool ([https://projects.blender.org/blender/blender/pulls/110310 110310]).


== July 10 - July 13 (week 3) ==
Merged : 
 - Initial Dopesheet Support ([https://projects.blender.org/blender/blender/pulls/108807 108807]).
 - Accessors for opacity and radius ([https://projects.blender.org/blender/blender/pulls/109733 109733]).

In review : 
 - Smoothing algorithm ([https://projects.blender.org/blender/blender/pulls/109635 109635]).
 - Hard Eraser tool ([https://projects.blender.org/blender/blender/pulls/110063 110063]).


== July 3 - July 7 (week 2) ==
Initial Dopesheet Support ([https://projects.blender.org/blender/blender/pulls/108807 108807]) done, pending for review.

The smoothing algorithm ([https://projects.blender.org/blender/blender/pulls/109635 109635]) works, but may need some additions in the curves API ([https://projects.blender.org/blender/blender/pulls/109733 109733]). Both PR are pending for review.

Started working on the eraser tool, which was not as easy as one may think. Still WIP.


== June 28 - June 30 (week 1) ==
First days at Blender.

Almost done with the Initial Dopesheet Support [https://projects.blender.org/blender/blender/pulls/108807 #108807].

Started working on the smoothing algorithm.