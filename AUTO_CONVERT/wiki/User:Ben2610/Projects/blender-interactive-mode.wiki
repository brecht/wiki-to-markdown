== Interactive mode in blender 2.8 ==
It's now official: the BGE has been removed from blender 2.8. It will be replaced by an interactive mode that should bring enough functionality to allow game prototyping, interactive applications, etc. 
The interactive mode is possible because of 2 major features of blender 2.8: Eevee, a fast photo-realistic render engine for the 3D view, and depsgraph, a granular dependency graph that allows to refresh only what needs to be refreshed. 
Much effort is put in these 2 essential parts of blender as efficient so that the interactive mode should be real-time or close to it.

On September 24 I started a one year project to get the interactive mode operational in blender 2.8. I'll be working 1.5 days a week on average, so progress will be slow at start. Here is the preliminary project plan, subject to change:

# bring 'continued physics' mode back : physics simulation runs even if scene frame is not incrementing
# bring local time to animations so that animations can run independently of scene frame
# upgrade bullet physics to latest version to benefit from performance improvements
# create an internal event system to exchange events and messages between scene entities
# create events from physics simulation and interaction
# create a logic node tree system to allow game logic

The last 3 items will be implemented in collaboration with Jacques Luke. The order in this list is not necessarily the order of implementation.

Watch progress : [[User:Ben2610/Reports/2018|Blender Interactive Mode Reports 2018]]