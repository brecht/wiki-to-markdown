__TOC__

== Week 202251 ==

'''Sculpt texture & paint'''

* Experiment with XR painting
* Designing missing pieces to fix seam bleeding.

'''Patch review'''

* Eevee: Mark Properties on Samples Panel Animatable [[http://developer.blender.org/D16827 D16827]]

== Week 202250 ==
'''Vulkan'''

* Updated patch to include Eevee shaders support and landed in master. [[http://developer.blender.org/D16610 D16610]]

'''Module work'''

* Wrote minutes of the Eevee/viewport module meeting [https://devtalk.blender.org/t/2022-12-12-eevee-viewport-module-meeting/26903]
* GPU: Select GPU Backend from Preferences [[http://developer.blender.org/D16774 D16774]]

'''Sculpt texture & paint'''

* Brush: Swap Texture Slots for Sculpt Brushes [[http://developer.blender.org/D16781 D16781]]
* Sculpt: Vertex painting color textures [[http://developer.blender.org/D16758 D16758]]

'''Patch review'''

* Fix empty asset index files after bug in asset loading [[http://developer.blender.org/D16678 D16678]]

== Week 202249 ==

'''Vulkan'''

* Test building vulkan on Windows.
* Report macro unrolling issue in Shaderc [https://github.com/google/shaderc/issues/1288]

'''Image Engine'''

* Initial documentation how image engine works and what are the ideas behind it [https://hackmd.io/rcNzi50QRL640DrFKCNIKg]
* Did several cleanups and improvements most notable:
** Improve performance image engine [[https://projects.blender.org/blender/blender/commit/2ffc9b72ad8d 2ffc9b72ad]]
** Reduce memory size by dividing the region in smaller parts. [[https://projects.blender.org/blender/blender/commit/c20e456ee04f c20e456ee0]]

'''Python GPU module'''

* Fix using FLOAT_2D_ARRAY and FLOAT_3D textures via Python [[https://projects.blender.org/blender/blender/commit/fd9b197226df48f1b328d97cf138659e1228a372 fd9b197226]]
'''Workshops'''

* Initial meeting with Reality check festival. [https://hackmd.io/@55n-91bZTVmcvTOc9vZV5Q/BkTP7wiDi]

== Week 202248 ==

'''Vulkan'''

* Compile vulkan shaders to Spir-V binaries [[http://developer.blender.org/D16610 D16610]]

'''Masterclass'''

* Prepared and gave a masterclass on Blender/OpenSource development focused on performance to a network of computer science schools (Bachelor/Master degree) around Europe.

'''Personal development'''

* Initial workshop with a coach about organizing development communities.

== Week 202247 ==

'''Vulkan'''

* Checked on potential license risks when using Vulkan validation layers. [[http://developer.blender.org/T102055 #102055]]
* Comitted the GHOST vulkan backend. [[https://projects.blender.org/blender/blender/commit/6dac345a64b1 6dac345a64]]
* Made MoltenVK as dependency as of the VulkanSDK [[https://projects.blender.org/blender/blender/commit/dd5fdb937087 dd5fdb9370]]
* Hooked shaderc to VKShader and shader builder; started fixing compilation errors [[http://developer.blender.org/D16610 D16610]]

'''Bug fixing'''

* Looked into the cause of [[http://developer.blender.org/T101402 #101402], [http://developer.blender.org/T102434 #102434], [http://developer.blender.org/T102675 #102675], [http://developer.blender.org/TT102706 #T102706]]
* Reviewed patches [[http://developer.blender.org/D16538 D16538], [http://developer.blender.org/DD16517 DD16517]]

'''Documentation'''

* Added technical documentation on asset indexing. Work still ongoing https://hackmd.io/5tgPlY98TCycQAo8T4qFUw

== Week 202246 ==

'''Bug fixing'''

* Fix hair/curve drawing artifacts when workarounds are enabled [[https://projects.blender.org/blender/blender/commit/5be3a68f58d3 5be3a68f58]]
* Regression: EEVEE: AMD driver 22.7.1 - 22.8.2: Volumes created from VDB and Geometry Nodes show artifacts [[https://projects.blender.org/blender/blender/commit/51914d4a4ada 51914d4a4a]]

'''Code review'''

* Fix T101775 : Remove double listing of grease pencil data in dopesheet summary [[http://developer.blender.org/D16369 D16369]]
* WIP: Use GPU_finish() instead of BLI_task_graph_work_and_wait() [[http://developer.blender.org/D13785 D13785]]
* GPU: Improve Codegen variable names [[http://developer.blender.org/D16496 D16496]]
* FIX T102076: Add support for int attributes to draw manager. [[http://developer.blender.org/D16420 D16420]]
* Fix T102365: Wireframe skips edges after recent cleanup [[http://developer.blender.org/D16451 D16451]]
* Fix T102210: Clicking on the Camera icon of the jobs panel should focus Render Window during rendering [[http://developer.blender.org/D16372 D16372]]
* Fix T69000: Hair: changing children related settings not updating in particle edit mode [[http://developer.blender.org/D13698 D13698]]
* Fix T100926: Show UV outline in texture paint mode [[http://developer.blender.org/D16490 D16490]]
* Fix T101211: Image jitters when scaling by large values [[http://developer.blender.org/D16517 D16517]]
* Images: Add "mirror" extension type [[http://developer.blender.org/D16432 D16432]]

'''Triaging'''

* Regression: Grease Pencil: Drawing lines appear after releasing the mouse [[http://developer.blender.org/T102251 #102251]]
* PNG 8bit and 16bit loads differently in compositor [[http://developer.blender.org/T102484 #102484]]
* Regression: Repeating resizing image and undoing causes view in image editor strange state, and editing it causes crash. [[http://developer.blender.org/T99356 #99356]]


'''Metal/Vulkan backend'''

* GPU: Enabled Metal test cases [[https://projects.blender.org/blender/blender/commit/f0ce95b7b91d f0ce95b7b9]]
* GPU: Update Vulkan backend with latest API changes. [[https://projects.blender.org/blender/blender/commit/277b2fcbfa4a 277b2fcbfa]]
* GHOST: Vulkan Backend. [[http://developer.blender.org/D13155 D13155]]

'''3D Texture painting'''

* 3D Texturing: Fix seam bleeding. [[http://developer.blender.org/D14970 D14970]]
* PBVH Texture Paint Node Splitting [[http://developer.blender.org/D14900 D14900]]

== Week 202245 ==

* Bug fixing
** The "Texture Paint" mode is very slow. [[http://developer.blender.org/T101966 #101966]]
* Setting up an AMD renderfarm for project Heist as fall-back.
* Interview with applicants.

== Week 202244 ==

* Added placeholder for gpu vulkan backend
* Send GHOST vulkan context for review.
* Started discussion about virtual cameras [[http://developer.blender.org/T102182 #102182]]
* Testing ShaderC and Vulkan SDK libraries

== Week 202243 ==

* BConf 2022 preparation.
* BConf 2022 attending.

== Week 202242 ==

* Refactoring brush code to work with multiple textures
* Prepare BConf presentation
* Blender: Add command line argument to switch gpu backends [[http://developer.blender.org/D16297 D16297]]

== Week 202241 ==

* 3D texturing brush
* Prepare BConf presentation
* Ensure float values are clamped to half float when uploading to GPU. Fixes some visual artifacts in the compositor backdrop and image editor. [[https://projects.blender.org/blender/blender/commit/872a45f42bc5a582fac59e9cd24239ee25821bff 872a45f42b]]

== Week 202240 ==

* 3D texturing brush
** Research potential performance improvements

* GPU
** Look into render glitch on NVIDIA.

* BConf
** Prepare presentation

== Week 202239 ==

* 3D texturing brush
** Cleanup UV seam bleeding patch.
** Research potential performance improvements

* GPU
** Python: Add platform feature support methods to `gpu.capabilities` module. [[https://projects.blender.org/blender/blender/commit/267aee514adb 267aee514a]]
** System info: Added GPU device type and feature support. [[https://projects.blender.org/blender/blender/commit/6075b04cf970 6075b04cf9]]
** GPU: Disable SSBO support from commandline. [[https://projects.blender.org/blender/blender/commit/0210c4df1793 0210c4df17]]

== Week 202238 ==

* 3D texturing brush.
** Extend unmanifold meshes.
* Participated as speaker on the EUE-ReConnect conference.

== Week 202237 ==

* Eevee/Viewport module meeting
* Create presentation for EUE-ReConnect conference.
* Debugging volumetric issue for Project Heist.

== Week 202236 ==

* Eevee/Viewport module meeting
* Research depth test issues with AMD 22.7/22.8 drivers.
* Vulkan status report
* NVIDIA driver issue with complex scenes + EEVEE (Project Hiest)

== Week 202235 ==

* Eevee/Viewport module meeting. https://devtalk.blender.org/t/2022-08-29-eevee-viewport-module-meeting/25640
* Fixing Metal backend compilation errors.
* EEVEE-Next: Cryptomatte [[http://developer.blender.org/D15753 D15753]]

== Week 202234 ==

* Eevee/Viewport module meeting. https://devtalk.blender.org/t/2022-08-22-eevee-viewport-module-meeting/25569
* Fixing EEVEE-Next shader compilation errors.
* EEVEE-Next: Cryptomatte
* Investigating platform issues AMD 22.7/22.8 drivers.

== Week 202233 ==

* Writing report out for Siggraph.
* Catching up with people met at Siggraph.
* Moving inside the office to a new location.
* Eevee/Viewport meeting

== Week 202232 ==

Siggraph 2022.

== Week 202231 ==

* Image: Display GPU layout in `uiTemplateImageInfo` [[http://developer.blender.org/D15575 D15575]]
* Fixing failing GPU test cases.
* Added support of curve objects in eevee-cryptomatte.

== Week 202230 ==

'''Code Review'''

* Improve image undo performance [[http://developer.blender.org/D15415 D15415]]
* Python API: ViewLayer.aovs.remove isn't available [[http://developer.blender.org/D15341 D15341]]
* Replace GLEW with libepoxy [[http://developer.blender.org/D15291 D15291]]

== Week 202229 ==

* Did some R&R this week.

== Week 202228 ==

* Recruitements
* GPU module meeting
* Code reviews
** Image Undo Performance
* 3d Texturing seam fixing
** Select more edges to be fixed.
** Code cleanup

== Week 202227 ==

* Recruitement
* GPU Module meeting (https://devtalk.blender.org/t/2022-07-04-eevee-viewport-module-meeting/24984)
* Review hair selection overlay
* Finalized Object Mode Hair selection outline
* 3d texturing seam fixing
** UDIM support
** Many performance improvements.

== Week 202226 ==

* Recruitment
** Speed dating with students
* 3d texture painting
* Add viewport playback performance to benchmark [[http://developer.blender.org/T99136 #99136]]
* Object mode curve outline [[http://developer.blender.org/D15308 D15308]]

== Week 202225 ==

* Recruitment
* Eevee/Viewport meeting
* 3d texture painting
* Fix regression tests draw module to work on NVIDIA 10, 20, 30-series card.
* Fix workbench regression tests on Mac Intel
* Add platform specific reference images for regression tests

== Week 202224 ==

* Recruitment
* Write blog post about the final days of Blender 2.83 LTS release.
* Looking into performance issues in master/viewport related
* Eevee/Viewport meeting
* 3d texture painting


'''Code review'''

* Fix T98699: Face dot colors in UV editor was using wrong color from theme

== Week 202223 ==

* Fix Video sequencer screen corruption occurs when resizing [[http://developer.blender.org/T98620 #98620]]
* Recruitment
* Write blog post about the final days of Blender 2.83 LTS release.
* Meeting about CI/CD testing with industry partners
* Sculpt, paint & texture module meeting
* Looking into performance issues in master/viewport related
* Get my head back at uv seam fixing.

== Week 202222 ==

* Recruitment
* Bug Fixing GPU
* Meeting about viewport compositing
* Sculpt, paint & texture module meeting

== Week 202221 ==

'''GPU Module'''

* Image.update doesn't update the image editor [[http://developer.blender.org/T98321 #98321]]
* GPU Module meeting
* Share GPUTextures between image data-blocks.

'''3d Texture painting'''

* UV Seams fixing

== Week 202220 ==

New design UV seam fixing.

== Week 202219 ==

'''Bug fixing'''

* Crash deferred shader compilation
* Windows NVidia regression light widgets
* MacOS/AMD: Drawing artifacts in VSE.
* GPU Subdiv compiler error [[http://developer.blender.org/T97330 #97330]]
* Eevee support for Geometry Nodes Color Attributes. [[http://developer.blender.org/T97895 #97895]]
* Color Attributes shading turns black after switching mode [[http://developer.blender.org/T97173 #97173]]
* DrawManager: Hide lock acquire behind experimental feature. [[http://developer.blender.org/T97988 #97988], [http://developer.blender.org/T97600 #97600]]
* Lag when resizing viewports. [[http://developer.blender.org/T97272 #97272]]
* Eevee: Fix GLSL compilation error.[[https://projects.blender.org/blender/blender/commit/35594f4b92fa4cbb5b848f447b7a3323e572b676  rB35594f4b]]
* GPU: Unable to compile material shaders.
* GPU: Fix crash deferred shader compilation.

'''Heist'''

* Compositor meta data to support different workflows [[http://developer.blender.org/T97905 #97905]]

'''Texture painting'''

* UV Seams; Concluded that the current approach is to limited. Discussed a possible
new approach, but that would need more research.

'''Meetings'''

* GPU Module meeting
* Meeting how to solve new hair requirements in draw manager
* Sculpt, texture & paint module

== Week 202218 ==

'''Texture painting'''

* Improve quality UV seam fixing
* Undo

'''Code review'''

* Add general Combine/Separate Color nodes [[http://developer.blender.org/D14034 D14034]]

'''Bug fixing'''

* Custom Normals tools dont update immediately [[http://developer.blender.org/T95165 #95165]]
* Image/UV Editor poor performance [[http://developer.blender.org/T95428 #95428]]

'''Heist'''

* Compositor meta data to support different workflows [[http://developer.blender.org/T97905 #97905]]

== Week 202217 ==

Not in office

== Week 202216 ==

'''Texture painting'''

* Stability of the 3d texturing brush
** Fix use correct colors when painting on byte textures.
* UV Seams

== Week 202215 ==
'''Texture painting'''

* PBVH Pixels
* UV Seams

== Week 202214 ==
'''Texture painting'''

* PBVH Pixels
* UV Seams

== Week 202213 ==

'''Texture painting'''

* Paint slot selection. [[http://developer.blender.org/D14455 D14455]]

'''Bug fixing'''

* Several Image editing refresh issues for 3.1.1 release.

'''Code review'''

* Fix T96575: Can't set vertex theme color
* Python: Add more useful information when attempting to pass incorrect attributes to GPUVertBuf.attr_fill [[http://developer.blender.org/D14103 D14103]]

== Week 202212 ==

'''Texture painting'''

* Design

'''Color management'''

* GLSL compilation error in OCIO generated code. [[http://developer.blender.org/D14425 D14425]]

'''Code review'''
* Color Management: support different settings for render and compositing output [[http://developer.blender.org/D14402 D14402]]

== Week 202211 ==

'''Texture painting'''

* Prototype 4.
** Reduced memory usage by pixel encoding.
* Comparing features between sculpt vertex paint, sculpt texture paint and old texture paint.

== Week 202210 ==

'''Bug Fixing'''

* Undo images.

'''Code Review'''

* Remap performance
* Canvas compositing
* widget 2d shaders.

'''Texture painting'''

* Prototype 2+3.

== Week 202209 ==

'''Bug Fixing'''

* Crash: painting 2D textures with anchored strokes in 3.1 Beta [[http://developer.blender.org/T95992 #95992]]
* Setting Masking->Stencil Mask->Stencil Image crashes Blender [[http://developer.blender.org/T95981 #95981]]
* Multi-view images fail to display properly in the Image Editor [[http://developer.blender.org/T95298 #95298]]
* Render slot issues.

'''Texture painting'''

* Research for technical implementation
* Rasterizer quality

== Week 202208 ==

'''Bug fixing'''
* Float options are shown next to non float textures.

'''Texture Painting'''
* Rasterizer for ImBuf.

== Week 202207 ==

'''Big fixing'''
* Compositor backdrop not updating

'''Texture painting'''
* Project setup
* Worked on a rasterizer for ImBuf

== Week 202206 ==

'''ID Management'''
* ID Remapping performance.

'''Projects'''
* Role description meetings
* Project planning meetings

== Week 202205 ==

'''Workshops'''

* Texture painting
* Material layering

'''Bug fixing'''

* Image editor transparency [[http://developer.blender.org/T95200 #95200]]
* Crash old scenes image editor [[http://developer.blender.org/T95332 #95332]]
* BGL builtin shaders color mismatch. [[http://developer.blender.org/T95341 #95341]]
* Crash shader dependency [[http://developer.blender.org/T95376 #95376]]
* Compositor viewer backdrop not visible.
* GPUTexture memory leak 
* Textures disappear when going to Edit Mesh on Solid Texture mode [[http://developer.blender.org/T95467 #95467]]

== Week 202204 ==

* Remap multiple items in UI performance [[http://developer.blender.org/D13615 D13615]]
* GPUShaderCreateInfo for hair refinement compute shader
* Sequence backgrouns shows green [[http://developer.blender.org/T94900 #94900]]
* Image engine [[http://developer.blender.org/D13424 D13424]]

== Week 202203 ==

'''Eevee/Viewport'''

* Investigating
** OCIO 1D lut support
** Eevee AOV renderpasses AA sampling
** Sequence background shows green [[http://developer.blender.org/T94900 #94900]]
* GPU shader descriptors [[http://developer.blender.org/D13360 D13360]]
* Regression compiling eevee materials [[http://developer.blender.org/T95003 #95003]]
* Binding keyword not supported on all platforms [[http://developer.blender.org/T95039 #95039]]
* Dragged node links are invisible [[http://developer.blender.org/T94987 #94987]]
* 3D Viewport is missing overlay [[http://developer.blender.org/T95052 #95052]]

'''Asset Browser'''
* Add ID Properties to Asset Indexer [[http://developer.blender.org/D12990 D12990]]

== Week 202202 ==

* GPU shader descriptors [[http://developer.blender.org/D13360 D13360]]
* Add testcases for library remapping

== Week 202201 ==

* Add ID Properties to Asset Indexer [[http://developer.blender.org/D12990 D12990]]
* Remap multiple items in UI, core, [[http://developer.blender.org/D13615 D13615]]
* Convert color space node, [[http://developer.blender.org/D12481 D12481]]
* GPU shader descriptors, [[http://developer.blender.org/D13360 D13360]]