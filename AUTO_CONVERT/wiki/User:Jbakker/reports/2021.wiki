__TOC__
== Week 202151 ==

'''Resync perfmance'''
* Experiment with screen id_remap callback. [[http://developer.blender.org/D13615 D13615]]

== Week 202150 ==

'''Eevee/Viewport'''

* Image Engine
** Support RGB float buffers.
** Node editor backdrop.
* Provided feedback on GPU shader language backend design.

'''Planning'''

* Project planning 2022.

'''Asset Browser'''

* Asset indexing ID Properties.

== Week 202149 ==

'''Eevee/viewport'''

* Image Engine
** Added border around image.
** Fixed several out of bound crashes in different places in Blender.
*** Ocio
*** ImBuf
*** OpenEXR
** Refactored IMB_transform with generic templates.

'''Code Review'''
* https://developer.blender.org/D13284

== Week 202148 ==

'''Eevee/viewport'''
* Image Engine
* Draw Manager: Engine Instance Data
* Document the GPU module on the wiki [[Source/EEVEE_%26_Viewport/GPU_Module]]

'''Patch review'''

* https://developer.blender.org/D13426
* https://developer.blender.org/D13385
* https://developer.blender.org/D13360

== Week 202147 ==

'''Eevee/Viewport'''

* Shader-C
* Vulkan GHOST device selection
* Image engine

'''Performance'''

* Brush mask generation.

== Week 202146 ==

'''Eevee/Viewport'''
* GPU Partial Update [T92613]
* Release notes.

'''Meeting'''
* Improving the development process

== Week 202145 ==

'''Bug Fixing'''

* Remesh Function Crashes Blender after Reconstructing Data [T91518]
* Fix ASAN crash in image editor
* Shader warnings with --debug-gpu [T92992]
* AMD Radeon 6900XT/ 6700 XT: Viewport Tearing Artifact in weight paint mode [T87927]
* Fix T89260: Eevee crashes with custom node sockets. [T89260]
* Crash GPU_material_compile FragShader trying to create custom GUI with Qt (PySide6) [T92925]

'''Review'''

* UI: Speed up icon scaling [D13144]

'''Eevee/Viewport'''

* Vulkan Support [T68990]
* Image Engine Redesign [T92525]

== Week 202144 ==

'''Asset Browser'''
* AssetBrowser: Add IDProperties to Asset Indexer. [T92306]

'''Code Review'''
* Pointcloud selection support. [D13059]
* Fix T92608: Image Editor does not display stereo images. [D13063]
* Basic engine shaders test [D13066]

'''Bug fixing'''
* Incorrect display planar tracking [T92807]

'''Eevee/Viewport'''
* Image engine code cleanup.

== Week 202143 ==

'''Asset Browser'''
* AssetBrowser: Add IDProperties to Asset Indexer. [T92306]

'''Bug fixing'''
* Draw: Crash on undo after adding new texture. [T91294]

'''Code Review'''
* Lattice still shows edges with "Bounds" display type [D11343].
* 3D View: UV Checker Pattern Overlay [D11250]
* Exclude 'Render Result' and 'Viewer' images where not supported T73182 [D11179]
* Crop node cropping is one pixel short [D12768]
* Compositor: Support backdrop offset for the Viewer node [D12750]
* Movie Clip Editor - frame indicator/controller is not displayed [D12659]

== Week 202142 ==

'''Asset Browser'''
* Asset Browser Indexing
* AssetBrowser : Fix Crash when marking assets from python [D12922]
* AssetBrowser: Add IDProperties to Asset Indexer. [T92306]
* AssetBrowser: Add cleanup operator for asset index. [T92335]

== Week 202141 ==

'''Asset Browser'''
* Localizing external files

== Week 202140 ==

'''Asset Browser'''
* Localizing external files


== Week 202139 ==

'''Asset Browser'''
* Asset Browser Indexing [D12693]
* Regression not showing idblocks when recursion is set to `Blend file`. [T91823]

'''GPU Viewport'''
* Move Viewport Texture to draw manager [D11966]

'''Blender LTS'''
* Release Blender 2.93.5

== Week 202138 ==

'''Eevee/Viewport'''
* Describe roadmap

'''Compositor'''
* Canvas compositing

'''Blender LTS'''
* Apply patches to 2.93
* Apply patches to 2.83
* Release Blender 2.83.18

== Week 202137 ==

'''Code Review'''
* Allow World assets to be drag/dropped onto viewport [D12566]
* UI: Blender 101: Mouse Hover Highlight [D12549]
* Compositor: Canvas Compositing [D12466]

'''Asset Browser'''
* AssetBrowser: Localize external files. [D12423]
* AssetBrowser: Performance file browser [D12449]
* Blender Libraries: Add JSON Library. [D12567]

'''Planning Sessions'''

* Current running GPU Viewport projects
** Eevee rewrite
** Viewport compositor
** Vulkan
* Planning upcoming projects
** Performance
** Texturing

== Week 202136 ==

'''Code Review'''
* Hair Info Length Attribute [D10481]
* GHOST: Unify behavior of offscreen context creation [D12455]

'''Asset Browser'''
* AssetBrowser: Localize external files. [D12423]
* AssetBrowser: Performance file browser [D12449]
* Compositor: Canvas Compositing [D12466]

== Week 202135 ==

'''Blender LTS

* Backported patches to 2.83.18
* Backported patches to 2.93.5

'''Code Review'''
* Hair Info Length Attribute [D10481]
* Fix Drivers Editor showing frame scrubbing [D12391]
* Fix Drivers Editor not hiding vertical part of cursor [D12391]
* Compositor: Add OIDN prefiltering option to Denoise node [D12342]

'''Eevee/Viewport'''
* Fix crash drawing hair with older GPUs. [861b7071a24b]
* Performance texture painting with limited scale. [T90825]

'''Asset Browser'''
* Asset Browser: Localize external images. [D12417]

== Week 202134 ==

'''Meetings'''
* Meeting with AMD about future projects.

'''Render Module'''
* Tested Cycles-X AMD patch and reported some findings.
* Check with Clement how to move some changes forward.
* Performance texture painting.

'''Blender LTS'''
* Backported patches to 2.83.18
* Released 2.93.4

'''Code Review'''
* Compositor: Full frame filter nodes [D12219]
* Compositor: Merge equal operations [D12341]
* Hair Info Length Attribute [D10481]

== Week 202133 ==

'''Blender LTS'''
* Backported patches to 2.83.18
* Backported patches to 2.93.4

'''Code Review'''
* Fix T90651: camera reconstruction crash without scene camera [D12230]
* Compositor: Full frame filter nodes [D12219]
* UDIM: Show the UV grid even when images are loaded [D11860]
* Make knife drawing anti aliased [D12287]
* Compositor: Add more render tests [D12286]
* Fix T90772: Image Editor not sampling color from the the currently selected pass [D12267]
* Compositor: Full frame vector nodes [D12233]
* Cleanup: Move 'tris_per_mat' member out of 'MeshBufferCache' [D12227]
* Cleanup: rearrange the 'struct MeshBufferCache' [D12223]
* Compositor: New Posterize Node [D12304]

'''Module work'''
* Added testcases to narrow down issues in D12052. [D12302]
* Draw Manager: Add callback for color management and populate loop. [D12332]

'''Blender 101'''
* Prototyped and stressed Blender 101 to create a minimal blender version and find limitations

'''Asset Browser'''
* Improve thumbnail loading from blend files. [T90908]

== Week 202132 ==

'''Blender LTS'''

* Backported patches to 2.83.18
* Released 2.93.3

'''Code review'''
* UDIM: Support tile sets that do not start at 1001 [D11859]
* UDIM: Show the UV grid even when images are loaded [D11860]
* Compositor: Full frame matte nodes [D12220]
* Compositor: Full frame filter nodes [D12219]
* Fix T89998: Cryptomatte node output values doubled with Multi-View [D12216]
* Assets: Recursive reading of asset libraries [D12139]
* DRW: Move buffer & temp textures & framebuffer management to DrawManager [D11966]

== Week 202131 ==

'''Blender LTS'''

* Backported patches to 2.93.3
* Released 2.83.17

'''Code Review'''

* Cryptomatte always needs full float precision. This patch will allow for that even in 16bit sequences [D12070]
* Compositor: Full frame Bokeh Blur and Blur nodes [D12167]
* Compositor: Full frame distort nodes [D12166]
* Compositor: Full frame transform nodes [D12165]
* Compositor: Add sampling methods for full frame [D12164]
* Compositor: Full frame input nodes [D12090]
* Assets: Recursive reading of asset libraries [D12139]
* Enable Asset Browser by default for poses, rest stays experimental [D12120]
* UDIM: Fix tile number calculation when adding a range of image tiles [D11857]
* UDIM: Support tile sets that do not start at 1001 [D11859]
* Fix FTBFS on mips64el architecture [D12194]

'''Viewport'''

* T73434: Improve Weight Paint Overlay Drawing. [D12170]
* Fix issue in CyclesX GPUDisplay patch [D12039]

'''Performance'''

* Poor performance in Timeline, Dope Sheet, etc when at the top of the editor. [T78995]
** Reduced GPU context switches [5f6033e0919a]
** Enable threading for building keylists. [D12198]
** Use CPP binary search [D12052]
** Use drawlist to draw keyframes [e53afad24149]


'''Asset Browser'''

* Asset: Dropping Material assets on material slot under mouse cursor. [D12190]

== Week 202130 ==

'''Blender LTS'''

* Backported patches to 2.83.17
* Release 2.93.2

'''Code Review'''

* Fix T87041: Driver Editor not updated in realtime [D12003]
* Compositor: Full frame Mask node [D11751]
* Compositor: Full frame convert nodes [D12095]
* Compositor: Full frame curve nodes [D12093]
* Compositor: Full frame color nodes [D12092]
* Compositor: Full frame output nodes [D12091]
* Cryptomatte always needs full float precision. [D12070]
* Cycles X: Use GPUDisplay for non-interactive render [D12039]
* Asset Browser: Proper context menu for assets [D12062]
* Asset Browser: Don't show inapplicable filter and display settings [D12059]
* Asset Browser: Adjust header pulldowns to be Asset Browser specific [D12057]
* Enable Asset Browser by default for poses, rest stays experimental [D12120]

'''Asset Manager'''

* WindowManager: Support Dynamic tooltips when dragging [D12104]
* Asset: Drop Material Tooltip [D12106]
* Link images when applying material assets [D12116]

== Week 202129 ==

'''Blender LTS'''
* Merged fixes into 2.93.2
* Merged fixes into 2.83.17

'''Code Review'''
* Compositor: Full frame Scale node [D11944]
* Compositor: Full frame Levels node [D11749]
* Compositor: Full frame Box Mask node [D11627]
* Fix T87041: Driver Editor not updated in realtime [D12003]
* Cleanup: Rearrange files used for mesh extraction [D11991]
* Compositor: Buffer iterators tests [D12001]
* DRW: Move buffer & temp textures & framebuffer management to DrawManager [D11966]

'''Asset Browser'''
* Assets: When dropping material use active material slot [D12056]

== Week 202128 ==

'''GPU Module'''
* Checked on preliminary vulkan support of proposed PyGPU functions.

'''Code review'''
* Draw: extract tris in parallel ranges [[http://developer.blender.org/D11445 D11445]]
* Compositor: Full frame Ellipse Mask node [[http://developer.blender.org/D11635 D11635]]
* Compositor: Full frame Viewer node [[http://developer.blender.org/D11698 D11698]]
* Compositor: Export operation results as debug option [[http://developer.blender.org/D11722 D11722]]
* Compositor: Full frame Levels node [[http://developer.blender.org/D11749 D11749]]
* Compositor: Full frame Double Edge Mask node [[http://developer.blender.org/D11750 D11750]]
* shot-builder: add frame_start and frame_end attribute to Shot and use that to set frame range of scene on building [[http://developer.blender.org/D11777 D11777]]
* Compositor: Buffer iterators [[http://developer.blender.org/D11882 D11882]]
* Compositor: Full frame Scale node [[http://developer.blender.org/D11944 D11944]]

'''Performance'''
* Cleanup: Hide implementation details for ED_keyframe_keylist. [[http://developer.blender.org/D11974 D11974]]

'''Asset Browser'''
* Improve Icon previews reflective materials. [[http://developer.blender.org/D11988 D11988]]

== Week 202127 ==

Unavailable due to personal time.

== Week 202126 ==

'''Code Review'''

* Compositor: Constant folding [[http://developer.blender.org/D11490 D11490]]
* Compositor: Full frame Ellipse Mask node [[http://developer.blender.org/D11635 D11635]]
* Compositor: Full frame Bilateral Blur node [[http://developer.blender.org/D11634 D11634]]
* Compositor: Full frame Pixelate node [[http://developer.blender.org/D11801 D11801]]
* Bone Overlay: support bone wireframe opacity settings. [[http://developer.blender.org/D11804 D11804]]
* VSE: Fix cache bar not visible [[http://developer.blender.org/D11779 D11779]]
* Fix: Compositor wrapping pixels outside buffer area [[http://developer.blender.org/D11784 D11784]]
* Compositor: Full frame Gamma node [[http://developer.blender.org/D11767 D11767]]
* Compositor: Full frame Exposure node [[http://developer.blender.org/D11766 D11766]]
* Compositor: Full frame Color Correction node [[http://developer.blender.org/D11765 D11765]]
* Compositor: Full frame Color Balance node [[http://developer.blender.org/D11764 D11764]]
* Compositor: Full frame Mask node [[http://developer.blender.org/D11751 D11751]]
* Compositor: Full frame Double Edge Mask node [[http://developer.blender.org/D11750 D11750]]
* Compositor: Full frame Levels node [[http://developer.blender.org/D11749 D11749]]
* Compositor: Export operation results as debug option [[http://developer.blender.org/D11722 D11722]]
* Compositor: Graphviz improvements [[http://developer.blender.org/D11720 D11720]]
* Compositor: Full frame Viewer node [[http://developer.blender.org/D11698 D11698]]
* Compositor: Full frame Mix node [[http://developer.blender.org/D11686 D11686]]
* Compositor: Full frame Sun Beams node [[http://developer.blender.org/D11694 D11694]]
* Compositor: Full frame Render Layers node [[http://developer.blender.org/D11690 D11690]]
* Compositor: Full frame Brightness node [[http://developer.blender.org/D11677 D11677]]

'''Vulkan'''

* GPU Push Constants [[http://developer.blender.org/T89553 #89553]]
* Added support for Wayland [[https://projects.blender.org/blender/blender/commit/8bf8db8ca2 8bf8db8ca2]]
* GPU builtin uniforms refactor [[http://developer.blender.org/T89684 #89684]]

== Week 202125 ==

'''GPU Module'''

* Refactored shader compilation log parser.
* Refactored draw test cases.
* Proposal for push constants [T89553]

'''Bug Fixing'''
* Viewport Render Preview glitching [T89405]

'''Blender LTS'''

* Prepared 2.83.17
* Prepared 2.93.2

== Week 202124 ==

'''Edit mesh performance'''

* Selecting all vertices on a face doesn't select the face and selects only some edges [T89271]
* Performance: Flush selection to edges/faces. [D11644]

'''Patch Review'''

* Expose Color Management as argument for gpu.types.GPUOffScreen.draw_view3d() [D11645]
* Compositor: Full frame Image node [D11559]
* Compositor: Full frame ID Mask node [D11638]
* Compositor: Full frame Ellipse Mask node [D11635]
* Compositor: Full frame Bilateral Blur node [D11634]
* Compositor: Full frame Box Mask node [D11627]
* Compositor: Constant folding [D11490]

'''LTS'''
* Blender 2.93.1 release

'''EEVEE/Viewport'''

* Vulkan: Add GLSL SpirV compiler [T89364]

== Week 202123 ==

'''Sequencer Performance'''

* VSE: Improve animation evaluation performance [D11544]
* Fix: Sequencer backdrop not updated during playback. [462bd81399]

'''Edit mesh performance'''

* DrawManager: Cache material offsets. [D11612]
* DrawManager: Multithreaded counting of material buckets [D11615]
* BMesh: use threading to count total selection. [D11622]
* Performance: Flush selection to edges/faces. [D11644]
* Performance: Limit recounting during selection mode flushing. [D11647]
'''Patch Review'''

* Compositor: Full frame Value node [D11594]
* Compositor: Full frame RGB node [D11593]
* DRAW Cache: sanitize 'DRW_mesh_batch_cache_dirty_tag' [D11588]
* Compositor: Full frame Image node[D11559]
* Fix: Image node alpha socket converted to operations twice [D11566]

'''LTS'''

* Blender 2.83.16 release
* Prepared 2.93.1 release

== Week 202122 ==

'''Mesh Editing Performance'''

* Multithreaded index builder [T88822]
* Use threaded ibo.tris extraction for single material meshes [T88352]

'''Sequencer Performance'''

* Research bottlenecks.
* Sequencer: Performance image crop transform [2e19649bb91b]
* Sequencer: Transform ImBuf Processor [D11549]
* Performance: Use parallel range for ImBuf scanline processor [D11578]
* Sequencer: Do not redraw during playback [D11580]

'''Code Review'''
* Fix: Compositor test desintegrate failing on arm64 [D11546]
* Compositor: Refactor recursive methods to iterative [D11515]
* Fix T88598: Support disabling task isolation in task pool. [D11415]
* Draw Cache: avoid recalculating 'poly_normals' [D11527]
* Draw Manager: Use threading for Mesh extract lines [D11467]
* Refactor: Draw Cache: use 'BLI_task_parallel_range' [D11558]

'''LTS'''

* Prepared 2.93.1.
* Prepared 2.83.16.

== Week 202121 ==

'''Code Review'''
* Fix (unreported): TextureOperation inputs have no resolution [D11381]
* GPUIndexBuf: Find the minimum and maximum index through the builder [D11455]
* Draw Manager: Mesh Extract: Balance the execution time of threads [D11450]
* Draw Manager: Extract tris in parallel ranges [D11445]

'''Bug Fixing'''
* Cryptomatte: EXR sequence does not update when scrubbing the timeline [T88666]
* Cryptomatte only works for the first View Layer [T88567]

'''Release'''
* Blender 2.93.0 LTS

'''Mesh Editing Performance'''

* Multithreaded index builder [T88822]

== Week 202120 ==

'''Code Review'''

* Compositor: Full-frame base system [D11113]
* Cleanup/Refactor: DRW: pass the 'MeshBufferCache' as a parameter instead of a GPU Buffer for the init and finish callbacks of the extracts [D11375]
* Refactor: DRW Mesh Extractor: Join the extractors in a same loop [D11375]
* Fix T88598: Support disabling task isolation in task pool [D11415]

'''Development'''
* Explicit colors [D10978]
* VSE shading mode ignores Grease Pencil Vertex colors. [T86956]

'''Edit mesh performance'''

* Cleanup patch.

== Week 202119 ==

'''Code Review'''

* Compositor: Full-frame base system [D11113]
* Overlay: Flash on Mode Transfer overlay [D11055]
* Cleanup: Refactor PlaneTrack and PlaneDistort operations [D11273]

'''Bug Fixing'''
* Perfomance drop if Texture Limit Size enabled [T87133]

'''Edit mesh performance'''

* Research areas to improve
** Use threading for simple gpu buffer mesh extractions [T88350, T88351, T88352, T88353]

* Experiments
** DrawManager: Task counters atomics.[T88369]
** Optimize `extract_edit_data` extraction. [T88450]
* Development
** DrawManager: Keep subset RenderMeshData around when geometry does not change. [D11339]

'''Development'''

* Explicit colors [D10978]

== Week 202118 ==

'''Code Review'''

* Compositor: Add vars and methods for easier image looping [D11015]
* Exclude 'Render Result' and 'Viewer' images where not supported T73182 [D11179]
* Compositor: force full float EXR for File Output node if Depth is connected [D11157]
* Compositor: Full-frame base system [D11113]
* 3D View: UV Checker Pattern Overlay [D11250]

'''Bug Fixing'''

* Updated test cases cryptomatte.
* [T87764] memory leak per frame during EEVEE viewport render animation with cryptomatte enabled.
* [T88180] Eevee not working with AMD RX580

'''Performance'''

* Researched edit mesh transform operations.
* Create benchmark baseline. [D11255]

== Week 202117 ==

'''Development'''
* DrawManager: Use Compute Shader to Update Hair. [D11057]
* GPU: Mesh Drawing Performance [T87835]

'''Code Review'''
* Replace COM_DEBUG #define with constexpr. Fixes T87035
* Fix T84815 : Missing preview image in compositing file output node [D10995]
* Compositor: add vars and methods for easier image looping [D11015]
* Fix T87771: Immediate Crash on "Edit Source" in Compositor [D11084]
* Fix Compositor: WorkScheduler task model deletes works [D11102]
* Overlay: Flash on Mode Transfer overlay [D11055]

== Week 202116 ==

'''Development'''
* Explicit colors [D10978]
* DrawManager: Use Compute Shader to Update Hair. [D11057]

'''Code Review'''
* Fix T87440 EEVEE: Specular Light Viewport Render Pass wrong behavior [D11028]
* Fix T86037 EEVEE: SSR option changes render passes result [D11033]
* macOS: Fix unknown -Wsuggest-override warning [D11012]

'''Bug Fixing'''
* Fix T78845: Eevee wrong material selection [D11036]

'''GSoC'''
* Reviewing proposals

== Week 202115 ==
'''Code Review'''
* D10956: Convert vertex colors to linear color space
* T74680: Incorrect mixing in Glare node
* D10971: Fix compositor: opencl device queue deleted after creation
* D10972: Fix (unreported) compositor resolution propagation broken by some nodes
* D10609: Modifiers: Performance Simple Deformation

'''Bug Fixing'''
* T87252: file output node broken with more than 4 inputs
* T87292: "File Output" node does not saves anything
* T77023: Using multiple Denoise Nodes.
* T81809: Compositor sometimes fails to composite (Image Editor involved)

== Week 202113 ==

'''Code Review'''
* Compositor: Add Anti-Aliasing node [D2411, D10778]

'''Cleanup'''
* Compositor: Enable suggest-override warning [D10846]

'''Bug Fixing'''
* Cryptomatte affected by Color Space change in OpenEXR multilayer [T68239]
* Cryptomatte: Fix When Image based Cryptomatte Aren't On The First Render Layer

'''Library Override'''
* Library Overrride API
** [D10848]

== Week 202112 ==

'''Code Review'''
* Compositor: Add Anti-Aliasing node [D2411]
* Compositor automated testing [D6334]
* PNG Banding [T86560]

'''Bug Fixing'''
* Fix: Cryptomatte Metadata Trimmed to 1024 [D10825]

'''Library Overrides'''
* API for restrictive mode

== Week 202111 ==

'''Render Module'''

* Finalize Cryptomatte [D3959]
* Fixed crash when loading malformed cryptomatte metadata.
* User manual cryptomatte.
* Release notes cryptomatte.

'''Code Review'''
* Fix T86542: Crash going to UV editing workspace with an instancer that is hidden from the viewport [D10724]
* Compositor: Add Anti-Aliasing node [D2411]
* Compositor automated testing [D6334]

== Week 202110 ==

'''Bug Fixing'''
* Cryptomatte Crash on Load [T86026]

'''Cleanup'''
* Resource manager for cryptomatte sessions [D10667]

'''Code Review'''
* Fix T86417: Crash deleting Shader AOV from an empty list [D10666]
* LibOverride: Add a utils to check if override has been user-edited. [D10649]
* Compositor: Silence -Wself-assign [D10653]
* Compositor: silence clang/clang-tidy override warnings [D10654]

'''Manual'''
* Library Overrides: Manual [D10642]

'''Blender 2.83'''
* Released Blender 2.83.13.

== Week 202109 ==

'''Overrides'''
* Technical Overview
* Manual

'''Bug Fixing'''
* Performance View 3d Viewport Crash [T86122]
* Cryptomatte Crash on Load [T86026]

'''Cryptomatte'''
* Construct Cryptomatte session from metadata.

'''Cleanup'''
* Modernize Compositor code to CPP 17.

== Week 202108 ==

'''Cryptomatte'''
* Windows build

'''Viewport'''
* Review/Discussion about re-arranging eevee render passes/stages.
* GPencil: Anti-aliasing quality
* Fix T85939: Eevee Specular BSDF shader compile error

'''Blender 2.92 release'''
* Making sure release can be done this week.

== Week 202107 ==

'''Override project'''
* Moved Overrides functional design to [https://wiki.blender.org/wiki/Source/Architecture/Overrides/Library/Functional_Design]
* Research overrides + USD compatibility [https://wiki.blender.org/wiki/Source/Architecture/Overrides/Library/USD_Mapping]

'''Code Review'''
* T85499: Crash on switching to edit mode with uv editor open
* D10414: Workbench: Improve AntiAliasing sampling.

'''Cryptomatte'''
* Code review comments.

'''Viewport Module'''
* Fix T85650: Switch for Fresnel Effect Wires in Viewport [D10436]

== Week 202106 ==

'''Override project'''
* Functional design

'''Bug fixing'''
* Initial design how to tackle the double CM issues
* T79999: Double color management applied during viewport animation render [D10371]
* Fix T81206: Do not limit gl texture size in image editor

'''Code review'''
* Fix T85499: Crash on switching to edit mode with uv editor open [D10369]
* Fix issue with render_passes AOVs in viewport [D10375]
* Fix T85499: Crash on switching to edit mode with uv editor open [D10369]
* Fix T74680: Incorrect mixing in Glare node [D7138]

'''Cryptomatte'''

* Load cryptomatte meta data from OpenEXR file.
* Rebase master into cryptomatte branch.

== Week 202105 ==

Shotbuilder:
* Production hooks are responsible for loading files.
* Added hook filter on task types.
* Save file after building
* Add camera rig.
* Started with override of props and characters
* Link animation in lighting file.
* Fix loading BBQ grill.
* Fix loading pencil and notepad.

Overrides:
* Tested overrides from UI perspective.
* Checked at the basic code structure and listed the important function points. On friday will dive into these function points.

Code Review

* D2411: Compositor: Anti-Aliasing node
* Eevee: DoF refactoring

== Week 202104 ==

* T83529: Horizontal stripe artifacts in image editor
* Fix T84878: Eevee cryptomatte broken with stereoscopy
* Fix T84398: Multiview images show only one view.
* Buildbot: Fixed crash when building RC builds
* Fix T84324: Crash when combining two scenes in compositor.
* Fix T84438: Crashing when compositing 2 scenes
* Fix T83198: Crash when switching to in-progress render slot (2.91)
* Potential Fix T83022: Transparency in Solid View Causes Glitch Artifacts
* Researched several CM issues [T77909].
* Added new AMD Drivers to the HQ normals workaround.
* Helped out Blender v2.83.12 release
* Fix T83187: Unselected UVs show selected on linked meshes.
* Fix to add 2.90.1 to opendata benchmark
* Fix to add 2.91.? to opendata benchmark
* Fix T81169: Grease Pencil Z-depth drawing issue on OSX + AMD Graphic Cards

== Week 202103 ==

Bug Fixing
* WindowsStore: Fix Search Bar Icon: [[https://projects.blender.org/blender/blender/commit/d5c7a4b3697319d73d7322e85a886edb9e9f2e6a d5c7a4b369]]

Code review
* Fix T84823: crash rendering with unconnected input socket in File Output node [[http://developer.blender.org/D10137 D10137]]
* Fix T84784: Time Remapping not displaying in the timeline [[http://developer.blender.org/D10134 D10134]]

Shot builder
* Loading assets
* Adding hooks
* Set render engine + settings
* Added cleaner method to sync data from external systems.

Releases
* Blender 2.91.2 release

== Week 202102 ==

* Fixed wireframe draw issues AMD drivers
* Fixed edit normal draw issues AMD drivers
* Fix T64953: Add cryptomatte meta data to file output node.
** Applied feedback from code review

== Week 202101 ==


'''Bug Fixing'''
* DrawManager: High quality normals for non meshes [[https://projects.blender.org/blender/blender/commit/d11a87b88c4d d11a87b88c]]
* GPU: Add HQ normals workaround [[https://projects.blender.org/blender/blender/commit/a3fcbb54f4b0 a3fcbb54f4]]
* GPU: Enable HQ normal work around for AMD Polaris [[https://projects.blender.org/blender/blender/commit/cb2517016ba1 cb2517016b]]
* Fix: Update normals when switching scene quality [[https://projects.blender.org/blender/blender/commit/c716b9862aa21b920e395d5acd751dccb2472e89 c716b9862a]]
* Regression : Eevee vextex color isn't working with hair [[https://projects.blender.org/blender/blender/commit/c6e5b3f42dfc05e911ef30df12821138e1225e91 c6e5b3f42d]]

* Fix T84053: Mask overlay in image editor not working [[http://developer.blender.org/T84053 #84053]]
* Compositor: Alpha Mode [[http://developer.blender.org/D9630 D9630]]
* Fix T64953: Add cryptomatte meta data to file output node. [[http://developer.blender.org/T64953 #64953]]
** Added example file and verified with users in external compositor
** Optimized the patch. (Mostly passing remove passing of std::string)
* Researched reason why wireframe rendering isn't working on specific platforms. [[http://developer.blender.org/T84459 #84459]]
** Updated the platform detection of failing platforms

'''Patch Review'''
* Fix T84160: Wrong DOF when camera is overriden [[http://developer.blender.org/D9952 D9952]]
* Fix T84389: RGB Curves node shows "tone" option outside of compositor [[http://developer.blender.org/D10005 D10005]]