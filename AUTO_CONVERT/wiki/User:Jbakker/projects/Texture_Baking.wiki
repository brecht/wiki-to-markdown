In the past couple of months I did several tests related to different topics around texture baking. I am getting a full picture of the project and therefore I started putting them all together in this doc.

= Definitions =

'''Material Baking''': Material baking is a mechanism that evaluates a full shader tree and extracts a selected components from the final closure into a texture.
'''Texture Baking''': A mechanism that extracts a part if a shader tree into a texture.

= Project Overview =

== Goal ==

When using complex materials in EEVEE the GPU (or driver) isn't able to create a performing shader. To work around this limitation, texture baking should be able to bake a (static) part of the material into a texture. This texture will then be used by the GPU shader. This will reduce the complexity of the compilation and execution as part of the shader is replaced by a texture lookup.

Cycles will also benefit from texture baking for the same reasons.

== Use Cases ==

* Optimize rendering
* Export textures to game engines
* ...

== Requirements ==

* It must be very easy to update and use the system. The idea is to have a TextureBaking node inside the shader graph. This enables fast switching between the full graph or a baked part of the graph. This is also allows a `Bake all Textures` operation.
* The scene render engine is used for texture baking.
* The resolution of the textures should be configurable. (option of the Baking operation (default) with an override in the Baking Node)?


= Functional Impact =

== Shader Editor ==

The shading editor will have a `Texture Baking Node`.

=== Texture Baking Node ===

The Texture Baking Node has an image selector. By default no image is selected. When no image is selected the texture baking node can already be used by the `Texture Baking Preview Overlays`. When an image is selected the image will store the results of the texture baking operator.
The node has a single input socket and output socket. By default these are color sockets. When the output socket is connected with another socket it can become a vector or a value socket. The input socket of the texture baking node always take over the type of its output socket.

== Image Editor ==

=== Texture Baking Preview Overlay ===

Inside the image and UV editor there will be a texture baking preview overlay. When active this overlay will show an `interactive` baking result.
The shown interactive baking result isn't the baked texture, as that can already be seen in the image editor by selecting the image. It is an interactive baking preview mode where the input part of the active Texture Baking node is shown of the material of the active object. it should be possible to select what materials will be shown. It should be possible to select between Active Material or All Materials.

When in the uv editor the user can still edit the UV's.

== 3D Viewport ==
=== Texture Baking Preview Overlay ===

Inside the 3d Viewport there will be a texture baking preview overlay. When active this overlay will show baking result on top of the scene in the viewport.
The shown baking result can be the actual baked texture or the input part of the active Texture Baking node of the material of the active object. 
It should be possible to select what materials will be shown. It should be possible to select between Active Material or All Materials.
It should be possible to switch from the baked texture to the interactive preview of the texture baking input socket.


== Render API ==
== EEVEE ==
== Cycles ==

= Limitations =

* When using texture baking a material cannot be shared with other models as there is a close link between the generated textures and the models.
* Texture Baking Node cannot be part of a node group

= Related Projects =

'''Migration of Image/UV editor to draw manager''':