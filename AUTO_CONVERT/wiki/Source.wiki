=Code and Design Documentation=


<div class="bd-lead">
Reference documentation and diagrams about Blender's architecture and components, and projects under development.
</div>

Old but often still useful design documents can be found in the <span class="plainlinks">[http://archive.blender.org/wiki Wiki Archive]</span>.

== General ==
* [[Source/File_Structure|File Structure]]
* [[Source/Architecture/Context|Context]]
* [[Source/Architecture/Transform|Transform]]
* [[Source/Architecture/Extensibility|Extensible Architecture Proposal]]
* [[Source/Depsgraph|Dependency Graph]]
* [[Source/Blender_Projects|Blender Projects]]

== Data Management, Import/Export ==
* [[Source/Architecture/ID|IDs & ID Management]]
* [[Source/Architecture/IDProperty|IDProperties]]
* [[Source/Architecture/DNA|DNA]]
* [[Source/Architecture/RNA|RNA]]

* [[Source/Architecture/File_Read_Write|.blend File Read/Write]]
** [[Source/Architecture/File_Read_Write/Library_Link_Append|Libraries & Link/Append]]

* [[Source/Architecture/Asset_System|Asset System]]
* [[Source/Architecture/Overrides|Overrides]]
** [[Source/Architecture/Overrides/Library|Library Overrides]]

* [[Source/Architecture/Undo|Undo System(s)]]
** [[Source/Architecture/Undo/Global|Global Undo]]
** [[Source/Architecture/Undo/Edit|Edit Undo]]
** [[Source/Architecture/Undo/Sculpt|Sculpt Undo]]

* [[Source/Architecture/Alembic|Alembic]]
* [[Source/Architecture/USD|Universal Scene Description]]

== Modeling ==
* [[Source/Modeling/BMesh/Design|BMesh Design]]

== Animation & Rigging ==
* [[Source/Animation|Module-specific page for Animation & Rigging]]

== Rendering ==
* [[Source/Render/Cycles|Cycles]]
* [[Source/Render/ColorManagement|Color Management]]

== User Interface ==
* [[Source/Interface/Internationalization|Internationalization]]
* [[Source/Interface/ExperimentalFeatures|Experimental Features]]
* [[Source/Interface/XR|Virtual Reality]]
* [[Source/Interface/Operators|Operators]]
* [[Source/Interface/Icons|Icons]]
* [[Source/Interface/Views|Views]]
* [[Source/Interface/Editors|Editors]]
* [[Source/Interface/Window Manager|Window Manager]]
* [[Source/Interface/Window|Windows]]
* [[Source/Interface/Screen|Screens]]
* [[Source/Interface/Text|Text Output]]
* [[Source/Interface/Outliner|Outliner]]
* [[Source/Interface/Preferences_and_Defaults|Preferences & Defaults]]

Technical documentation ToDos: [http://developer.blender.org/T104114 T104114]


== Sculpt, Paint, Texture ==

Technical documentation ToDos: [http://developer.blender.org/T104113 T104113]

* [[Source/Sculpt/PBVH|PBVH]]


== EEVEE & Viewport ==

For a quick overview for new developers to the module check the [https://www.youtube.com/watch?v=OxwrSdt6I4w| on-boarding video ].

* [[Source/EEVEE & Viewport/GPU Module|GPU Module Overview]]
* [[Source/EEVEE & Viewport/Draw Module|Draw Module Overview]]
* [[Source/EEVEE & Viewport/Color Management Drawing Pipeline|Color Management Drawing Pipeline]]
* [[Source/EEVEE & Viewport/Draw Engines/EEVEE|EEVEE]]
* [[Source/EEVEE & Viewport/Draw Engines/Workbench|Workbench]]
* [[Source/EEVEE & Viewport/Draw Engines/Overlay Engine|Overlay]]
* [[Source/EEVEE & Viewport/Draw Engines/External Engine|External]]
* [[Source/EEVEE & Viewport/Draw Engines/Selection Engine|Selection]]
* [[Source/EEVEE & Viewport/Draw Engines/Image Engine|UV Image Editor]]

== Objects ==
* [[Source/Objects/Mesh|Mesh Object]]
* [[Source/Objects/Volume|Volume Object]]
* [[Source/Objects/PointCloud|Point Cloud Object]]
* [[Source/Objects/Curves|Curves Object]]
* [[Source/Objects/Geometry_Sets|Geometry Sets]]
* [[Source/Objects/Instances|Instances]]
* [[Source/Objects/Attributes|Attributes]]

== Nodes & Physics ==
* [[Source/Nodes/Viewer_Node|Viewer Node]]
* [[Source/Nodes/Modifier_Nodes|Modifier Nodes]]
* [[Source/Editors/Spreadsheet|Spreadsheet Editor]]
* [[Source/Nodes/Fields|Fields]]
* [[Source/Nodes/Anonymous_Attributes|Anonymous Attributes]]
* [[Source/Physics/Mantaflow|Fluid system - Mantaflow]]
* [[Source/Physics/Rigid_Body|Rigid body - Bullet]]
* [[Source/Physics/Cloth|Cloth]]
* [[Source/Physics/Caching|Caching system]]

== VFX & Video ==
* [[Source/VSE|Video Sequencer Editor]]
* [[Source/Compositor|Compositor]]
* [[Source/RealtimeCompositor|Realtime Compositor]]
* [[Source/Motion_Tracking|Motion Tracking]]

== Grease Pencil ==

== Lineart ==

== Library Dependencies ==
* [[Source/OpenXR_SDK_Dependency|OpenXR-SDK]]