= Weekly Reports 2023 =
== Week 14: January 02 - 06 ==
Cycles/Geometry Nodes: working on the [https://developer.blender.org/diffusion/B/history/microfacet_hair/ microfacet_hair] branch
* Try out different normal modes in the geometry node for the hair curve, such as aligning with the [https://developer.blender.org/rB93e0559f54c087a64121b4b30708be096a3d7c3e curvature vector] or the direction with the [https://developer.blender.org/rB5015c8219b69e7f632ec966634b2b8e7749e6c90 minimal potential energy] (Cycles only supported hair with circular cross-sections. The new hair BSDF supports elliptical cross-section, it is therefore important to compute meaningful orientations of the ellipse along the curve.)

== Week 15: January 09 - 12 ==
Cycles: working on the [https://developer.blender.org/diffusion/B/history/microfacet_hair/ microfacet_hair] branch
* Replaced the default interpolation weight between the minimal twist vector and the curvature vector with a user-defined weight ([https://developer.blender.org/rB8ee7e626a5a6e4b5471fc1bddb508d77f072e0c2 rB8ee7e626a5a6])
* Discussed with Jacques/Hans/Brecht, the new normal mode will be set aside for now, in order to separate Cycles and Geometry Nodes development.
* Small cleanups and bug fixes in preparation of merging the branch to master.

== Week 16: January 16 - 20 ==
Cycles: Refactoring and cleanups.
* Renamed `I`/`omega_in` to `wi`/`wo` in the BSDFs, to better distinguish the incoming/outgoing directions. ([http://developer.blender.org/rB543bf28fb1ff rB543bf28fb1ff])
* Refactored `bsdf_microfacet.h`, use function template to reduce repetition ([http://developer.blender.org/rB320757bc6111 rB320757bc6111])
* Fixed anisotropic Beckmann using isotropic sampling (https://wiki.blender.org/wiki/Reference/Release_Notes/3.5/Cycles#Bug_Fixes)
* Clean up code in the [https://developer.blender.org/diffusion/B/history/microfacet_hair/ microfacet_hair] branch

== Week 17: January 23 - 27 ==
Cycles: cleanups and patch reviews.
* More cleanups in the `microfacet_hair` branch (API changes and comments).
* Reviewed some Microfacet BSDF-related patches ([http://developer.blender.org/D17123 D17123], [http://developer.blender.org/D17100 D17100]).
* Added support for non-uniform spot light size to match the gizmo in the view port ([http://developer.blender.org/D17129 D17129]).
Experimenting with new gizmo types for lights.

== Week 18: January 30 - February 3 ==
Viewport:
* Created task [https://developer.blender.org/T104280 T104280]: Improve visualisation and manipulation of lights in the viewport, including 
# Add new cage2d draw style for circular shapes ([https://developer.blender.org/rBfe5d54d3d0ee328674caff7aa73180f7abfb85f6 fe5d54d3d0ee])
# Add gizmo for adjusting spot light blend ([https://developer.blender.org/rB3c8c0f1094a3fad5ee47888ce0507c9707d71772 rB3c8c0f1094a3])
# Add gizmos for spot and point light radius ([https://projects.blender.org/blender/blender/commit/afd6358fc0de4179d7dbc8828c8aea8b141f67b8afd6358fc0 afd6358fc0])
* Fixed spot light blend circle drawn at the wrong position ([https://developer.blender.org/rBce42906b8962155b50cc043b3a0b5b24f191a41c rBce42906b8962])

== Week 19: February 6 - 9 ==
Viewport:
* Fixed area light gizmo not following the cursor. Perform constrained resizing when dragging the edges, and free-form resizing when dragging the corners ([https://projects.blender.org/blender/blender/commit/701f90c677cacfd2fa377249a3463128848c8a38 701f90c677])
* Hold shift key to resize area light uniformly. ([https://projects.blender.org/blender/blender/pulls/104418 104418])
* Some explorations of showing the light color in overlay and enabling undo for gizmos

== Week 20: February 13 - February 15, February 17 ==
Viewport:
* Enable undo for gizmos ([https://projects.blender.org/blender/blender/commit/4a7fc3d5b2bac3ec99f35243335f59157e916972 4a7fc3d5b2])
* Add gizmo description to tooltip ([https://projects.blender.org/blender/blender/commit/55826ea5a6aee721817c6989d07cb85068434d72 55826ea5a6])

== Week 21: February 20 - 24 == 
Viewport:
* Some improvements of gizmo undo pull request
* Support scaling of light gizmos with zero size ([https://projects.blender.org/blender/blender/commit/6166dac3ee 6166dac3ee])

== Week 22: February 27 - March 2 ==
Viewport:
* Continuing explorations of showing the light color in overlay
* Failed attempt of changing the cursor orientation depending on the hover region on a cage2d gizmo
Misc:
* Reading recent rendering papers

== Week 23: March 6 - 10 ==
Viewport:
* Added an optional overlay to show light colors ([https://projects.blender.org/blender/blender/commit/6fbc52bdca3 6fbc52bdca3])
* For area light gizmo, different constraints apply depending on the hover region (edges for X/Y axis constraint, corners for free-form). Now different handles are shown depending on the hover region to make the constraint more clear ([https://projects.blender.org/blender/blender/commit/275d69467be 275d69467be])
* Slightly increased cage2d corner margins for better free-form scaling ([https://projects.blender.org/blender/blender/commit/48944e7a8ea 48944e7a8ea])
EEVEE:
* Hair BSDFs return black color due to lacking proper implementation. Temporarily fall back to diffuse BSDF to at least show some feedback ([https://projects.blender.org/blender/blender/commit/b9649c2e72 b9649c2e72])
Cycles:
* Fix custom normals not normalized in Cycles shader nodes ([https://projects.blender.org/blender/blender/commit/e7a3a2c261 e7a3a2c261])
* A bit on Microfacet Hair BSDF again, fixed initialising with zero Transmission due to copying the Transmission property from the default Principled BSDF ([https://projects.blender.org/weizhen/blender/commit/6858fba43d 6858fba43d])

== Week 24: March 13 - 18 ==
Cycles:
* Fix artefacts caused by trying to lift the shading normal of diffuse materials ([https://projects.blender.org/blender/blender/pulls/105776 105776])
* Fix growing `BoundBox` with empty `BoundBox` modifies the original `BoundBox` ([https://projects.blender.org/blender/blender/commit/d9273d85786018f66511d59db1918d3adaaf4634 d9273d8578])
* Build light tree in parallel for large scenes ([https://projects.blender.org/blender/blender/pulls/105862 105862])

== Week 25: March 20 - 24 ==
Cycles:
* Refactor light tree implementation to improve readability and efficiency ([https://projects.blender.org/blender/blender/commit/17c8e0d309 17c8e0d309])
* Fix bugs related to light tree bounding cone computation ([https://projects.blender.org/blender/blender/commit/791f35e2c0 791f35e2c0], [https://projects.blender.org/blender/blender/commit/e77b666ae9 e77b666ae9])
* Implement faster `make_orthonormals()` ([https://projects.blender.org/blender/blender/pulls/106053 #106053])

== Week 26: March 27 - 31 ==
Cycles:
* Working on adding instance support in light tree

== Week 27: April 3 - 7 ==
Cycles:
* Refactor and clean up light tree implementation ([https://projects.blender.org/blender/blender/commit/f692010ae1079197626018f18ddb87bd5955709a f692010ae1], [https://projects.blender.org/blender/blender/commit/1205111fe9a8db4e66d57b7db52cf0fd88ed2f09 1205111fe9], [https://projects.blender.org/blender/blender/commit/e58a05ca68e493393456aab23f11abb37f8ff441 e58a05ca68], [https://projects.blender.org/blender/blender/commit/87cbdcbe7cf6249dae4acf28662504715b80bbdf 87cbdcbe7c])
* Continue working on instance support in light tree
* Fix light tree nodes having NaN axis ([https://projects.blender.org/blender/blender/commit/d872240983 d872240983])
* Fix motion triangle normals not normalised ([https://projects.blender.org/blender/blender/commit/792da15d53 792da15d53])
* De-duplicate code in motion triangle normal computations ([https://projects.blender.org/blender/blender/commit/390a63b264 390a63b264])

== Week 28: April 11 - 14 ==
Cycles:
* Finish adding instance support in light tree ([https://projects.blender.org/blender/blender/commit/bfd1836861 bfd1836861])
* Clean up Microfacet Hair BSDF implementation ([https://projects.blender.org/blender/blender/pulls/105600 #105600])
* Some code reviews

== Week 29, 30: April 17, 18, 28 ==
Cycles:
* Finish cleaning up Microfacet Hair BSDF implementation

== Week 31: May 1 - 5 ==
Cycles:
* Address energy loss issue in the Microfacet Hair BSDF ([https://projects.blender.org/blender/blender/pulls/105600#issuecomment-934862 #105600])
* Review pull request ([https://projects.blender.org/blender/blender/pulls/107560 #107560])

== Week 32: May 8 - 13 ==
Cycles:
* Fix wrong ray-offset of motion triangles ([https://projects.blender.org/blender/blender/pulls/107748 #107748])
* Fix light tree stack overflow due to queried the area of an empty bounding box ([https://projects.blender.org/blender/blender/commit/8e70ab9905471c94a0b8b79a92401fdff59a4588 8e70ab9905])
* Fix bugs in area lights regarding MNEE (Manifold Next Event Estimation) ([https://projects.blender.org/blender/blender/commit/3d96cab01fb25044475c9eea7754f438ffb25f99 3d96cab01f] [https://projects.blender.org/blender/blender/commit/1a1f06bd9a7a1d785556ba36acc3ba8da4a7cf13 1a1f06bd9a])

== Week 33: May 15 - 19 ==
* Review pull request ([https://projects.blender.org/blender/blender/pulls/107958#issuecomment-941199 #107958])
* Investigate issues regarding conversion between power and radiance of light sources ([https://projects.blender.org/blender/blender/issues/108505 #108505])

== Week 34: May 22 - 26 ==
Cycles:
* Cleanup: remove unused function and unnecessary code ([https://projects.blender.org/blender/blender/commit/d1dca2d15ca0430c59ae310225688dd7f0df2706 d1dca2d15c] [https://projects.blender.org/blender/blender/commit/bbe5b1e8cf022cd01b465b949ef4d9f660112c7b bbe5b1e8cf]), rename function for clarity ([https://projects.blender.org/blender/blender/commit/4758299d86588201798cb3bb48b113e7d88cc39e 4758299d86])
* Refactor: group multiple floats to `float2` or `float3` ([https://projects.blender.org/blender/blender/commit/41e49d7ece86bb26a1923af99da45114adb72231 41e49d7ece])
* Review pull request ([https://projects.blender.org/blender/blender/pulls/108323#issuecomment-947755 #108323])
* Change point light to single-sided sphere light ([https://projects.blender.org/weizhen/blender/commit/5db87bb7732690ebaa1ce1293b8b0614389ce032 5db87bb773])

== Week 35: May 29 - June 2 ==
Cycles:
* Implement more efficient and better-behaved sampling of spherical triangles ([https://projects.blender.org/blender/blender/commit/97d9bbbc97f85e4d309c843a063a73123265e317 97d9bbbc97])
* Further implementation of sphere light regarding sampling ([https://projects.blender.org/blender/blender/pulls/108506 #108506])

== Week 36: June 5 - 9 ==
Cycles:
* Fix compiler error ([https://projects.blender.org/blender/blender/commit/0e20e2320b4c1c6764a79b3da84138aeadf421c4 0e20e2320b])
* Fix wrong normal transformation in Vector Transform shader node ([https://projects.blender.org/blender/blender/commit/ebd431d58080a300b266ffec4e87c0e1f457bfd8 ebd431d580])
* Fix light texture invisible when looking at a light ([https://projects.blender.org/blender/blender/commit/84863dae89f19c3054180e4d691e8685d8960420 84863dae89])
* Enable normal transformation of lights in Vector Transform node ([https://projects.blender.org/blender/blender/commit/2ab020cc3c7111eca4d0929de8c9cbe097a86ff3  2ab020cc3c])
* Further implementation of sphere light regarding UV
EEVEE:
* Cleanup: simplify computation in disk LTC ([https://projects.blender.org/blender/blender/commit/d54fe4080724a8dc5296a72dcc6fc9e2079722d5 d54fe40807])

== Week 38: June 19 - 23 ==
Cycles:
* Review pull request regarding sun light, fix wrong conversion factor ([https://projects.blender.org/blender/blender/pulls/108996 #108996])
* Add regression test for light UV ([https://projects.blender.org/blender/blender/pulls/108691#issuecomment-962880 #108691])
* Small optimization regarding light sources ([https://projects.blender.org/blender/blender/commit/9aaf28954b814b87f1a2250e8c4ef807c94b8b42 9aaf28954b] [https://projects.blender.org/blender/blender/commit/37d3daaea6d7a5d5742caf69fe9fe3b4b426c820 37d3daaea6])
* Replace spotlight disk sampling with sphere sampling, add texture and reduce noise ([https://projects.blender.org/blender/blender/pulls/109329 #109329])
Cycles/EEVEE:
* Change point light to double-sided sphere light ([https://projects.blender.org/blender/blender/pulls/108506 #108506])
* Fix wrong conversion from power to radiance of area lights ([https://projects.blender.org/blender/blender/pulls/109153 #109153])

== Week 39 June 26 - 30 == 
Mainly attending EGSR

== Week 40 July 3 - 7 ==
Cycles:
* Implementing faster visible normal distribution sampling ([https://projects.blender.org/blender/blender/pulls/109757 PR #109757], not used in the end)
* Use low-distortion mapping when sampling cone and hemisphere ([https://projects.blender.org/blender/blender/pulls/109774 PR #109774])
* Add texture to sun light ([https://projects.blender.org/blender/blender/pulls/109842 PR #109842])
EEVEE:
* Adjust sun light factors to match the recent change in Cycles ([https://projects.blender.org/blender/blender/pulls/109831 PR #109831])

== Week 41 July 10 - 14 ==
Cycles: 
* Remove `SHARP` distribution internally for consistency and better code readability ([https://projects.blender.org/blender/blender/pulls/109902 PR #109902])
* Review pull request ([https://projects.blender.org/blender/blender/pulls/109951 PR #109951], [https://projects.blender.org/blender/blender/pulls/109950 PR #109950])
* Bug fix ([https://projects.blender.org/blender/blender/issues/109945 #109945]) and Cleanup ([https://projects.blender.org/blender/blender/commit/a86506b741 a86506b741])

== Week 42 July 17 - 21 ==
Start working on spectral Cycles, mainly working on the design and going through the code base

== Week 43 July 24 - 28 ==
Spectral Cycles:
* Clean up the code base
* Implement wavelength-dependent Fresnel ([https://projects.blender.org/blender/blender/commit/14ac7ddfe2 14ac7ddfe2])
* Fix some visibility issues

== Week 44 July 31 - August 4 ==
Spectral Cycles:
* Change pdf type from `float` to `Spectrum` ([https://projects.blender.org/blender/blender/commit/807d86eee5 807d86eee5])
* Weight paths by the pdf to reduce color noise in dispersive materials ([https://projects.blender.org/blender/blender/commit/cb1b7c7a53 cb1b7c7a53])
* Cleanups in microfacet BSDF
Nodes:
* Copy socket values with the same `identifier` instead of `name` ([https://projects.blender.org/blender/blender/pulls/110792 PR #110792])
Regular Cycles:
* Cleanup microfacet hair branch
* Review pull requests ([https://projects.blender.org/blender/blender/pulls/110307 PR #110307], [https://projects.blender.org/blender/blender/pulls/110374 PR #110374], [https://projects.blender.org/blender/blender/pulls/110377 PR #110377], [https://projects.blender.org/blender/blender/pulls/110376 PR #110376])

== Week 45 August 7 - 11 ==
Mainly attending SIGGRAPH

== Week 46 August 14 - 18 ==
Cycles:
* Fixing transparency issues in microfacet hair, address reviews ([https://projects.blender.org/blender/blender/pulls/105600 PR #105600])
* Merge microfacet hair into main (renamed as Principled Hair BSDF, Huang model, [https://projects.blender.org/blender/blender/commit/6f8011edf7 6f8011edf7]), added TODOs [https://projects.blender.org/blender/blender/issues/111257 #111257])
* Review pull request ([https://projects.blender.org/blender/blender/pulls/111155 PR #111155])
EEVEE/Cycles:
* Default roughness distribution to Multiscatter GGX ([https://projects.blender.org/blender/blender/pulls/111267 PR #111267])

== Week 47 August 21 - 25 == 
Cycles:
* Fix illegal address error in Cycles Light Tree when no emitter is selected ([https://projects.blender.org/blender/blender/commit/c076202e23 c076202e23])
EEVEE/EEVEE-Next:
* Fix incorrect GGX BRDF pdf and BTDF evaluation ([https://projects.blender.org/blender/blender/pulls/111591 PR #111591])
* Fix incorrect GGX BRDF LUT coordinates ([https://projects.blender.org/blender/blender/commit/67843e188b 67843e188b])
Others:
* Review pull requests regarding panoramic camera projection ([https://projects.blender.org/blender/blender/pulls/111310 PR #111310]) and Cycles light tree ([https://projects.blender.org/blender/blender/pulls/111292 PR #111292])
* Debugging issues when overwriting newer files in an older Blender version ([https://projects.blender.org/blender/blender/commit/b075c84ba3 b075c84ba3])

== Week 48 August 28 - September 1 == 
EEVEE/EEVEE-Next:
* Cleanup terms in multiscatter GGX ([https://projects.blender.org/blender/blender/pulls/111634 PR #111634])
* Use utility function for LUTs parametrized by `cos_theta` and `roughness` ([https://projects.blender.org/blender/blender/pulls/111651 PR #111651])
* Fix runtime-generated BSDF LUT does not match the precomputed LUT ([https://projects.blender.org/blender/blender/pulls/111632 PR #111632])
* Change Glass BSDF to match Cycles ([https://projects.blender.org/blender/blender/pulls/111687 PR #111687])
* Change Principled BSDF to match Cycles ([https://projects.blender.org/blender/blender/pulls/111754 PR #111754])
Cycles:
* Add distance-dependent transmission color in Principled BSDF ([https://projects.blender.org/blender/blender/pulls/111806 PR #111806])

== Week 49 September 4 - 8 ==
Cycles:
* List several inconsistencies in BSDF sampling and evaluation ([https://projects.blender.org/blender/blender/issues/111941 #111941])
* Fix NaN in Principled Huang Hair `sphg_dir()` ([https://projects.blender.org/blender/blender/commit/df26271db4 df26271db4])
* Fix precision issues in ray sphere intersection ([https://projects.blender.org/blender/blender/commit/fedeaab30f fedeaab30f])
* Fix Principled Huang Hair renders black on horizontal particle hair ([https://projects.blender.org/blender/blender/commit/077022e45f 077022e45f])
* Remove defensive sampling at the first interaction ([https://projects.blender.org/blender/blender/commit/bf82f9442c bf82f9442c])
* Cleanup microfacet BSDF implementation ([https://projects.blender.org/blender/blender/commit/21ca47d81f 21ca47d81f],[https://projects.blender.org/blender/blender/commit/f96b9db610 f96b9db610]) and path guiding ([https://projects.blender.org/blender/blender/commit/4c6164e292 4c6164e292])
* Fix inconsistent normal checks when sampling and evaluating BSDF ([https://projects.blender.org/blender/blender/commit/063a9e8964 063a9e8964])
* Fix wrong refractive index in path guiding ([https://projects.blender.org/blender/blender/pulls/112157 PR #112157])
* Adjust the pdf of picking reflection/refraction lobes by the tint ([https://projects.blender.org/blender/blender/pulls/112158 PR #112158])
* Add tint control in Specular and Transmission component of Principled BSDF ([https://projects.blender.org/blender/blender/pulls/112192 PR #112192])
EEVEE:
* Fix Principled BSDF not matching Cycles near `IOR == 1` ([https://projects.blender.org/blender/blender/commit/db4a2de620 db4a2de620])