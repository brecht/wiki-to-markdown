== Introduction ==
== Steps ==
* Setup accounts
# wiki.blender.org  wiki
# devtalk.blender.org devtalk
# developer.blender.org developer
# id.blender.org id
== Setting up your wiki ==
The wiki is used to record your work for the Blender Foundation and also to 
== blender.chat quick intro ==
== Wiki setup tips ==
== Your first commit ==
===How to generate a bug===
=== Code style ===
=== Running tests ===
=== Builds regular, debug & release ===
==== Edit config ====
==== arc ====
arc diff
You can use ##arc diiff## to create a patch on developer.blender.org. To do this you can just use ##arc diff## in your source directory to generate a diff against ##HEAD~1## or if there are local changes it will use those. Follow the prompts to fill out the details of your patch.

arc patch
This can be used to create a local branch containing the changes in the patch. This is really useful for testing changes.  

For me ##arc land## never seemed to work. Other recomened the following
* Create change using ##arc patch D????##
* Run make format to make sure that you are using the correct style.
* commit any code format changes.
* Rebase your patch with master to make sure it is in sync with the latest changes.
* Switch to the the branch you intend to push your patch to then use ##git cherry-pick commit-id## to have your commit in the branch.
* test your changes. run ##make test## to run the unit tests don't forget the build the debug release first with ##make debug## otherwise it may run an old build instead
* now use ##git push## to land your changes.