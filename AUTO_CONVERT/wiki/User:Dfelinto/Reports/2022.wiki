==July 4 - 8==
The main work this week was to finish the blogpost about the new hair system, prepare its images, update the documentation and work with the team on any missing task.

I also had a meeting with Arnd about the new Gitea workflows. I will post the meeting notes next week.

[[File:White-board-new-hair.jpg|800px|center]]


;'''Links'''
** [https://code.blender.org/2022/07/the-future-of-hair-grooming The Future of Hair Grooming]
** [https://docs.blender.org/manual/en/3.3/modeling/curves/primitives.html#empty-hair User Manual: Hair Primitive]
** [https://docs.blender.org/manual/en/3.3/sculpt_paint/curves_sculpting/introduction.html User Manual: Curves Sculpting]

;'''Commits'''
* Hair Curves: The new curves object is now available [https://projects.blender.org/blender/blender/commit/becb1530b1c becb1530b1].
* Cleanup: make format [https://projects.blender.org/blender/blender/commit/2c55d8c1cf6 2c55d8c1cf].
* UI: Curves Sculpting - Remove duplicated entry for Curve Length [https://projects.blender.org/blender/blender/commit/c52a18abf84 c52a18abf8].
* Fix curves sculpting Selection Paint missing refresh [https://projects.blender.org/blender/blender/commit/abbc8333ac1 abbc8333ac].


==June 27 - July 1==
* Join the FOSS podcast with Pablo Vazquez.
* Presented and discussed further the Library Overrides design with Bastien Montagne and Julian Eisel.
* Implemented a better preview for the curves sculpt density distance preview (with Julian Eisel).
* Tested a few patches regarding the new hair system.
* Started a draft for a new blogpost about the design behind the new hair system.

==Hiatus==

==March 21 - 25==

Documented the [[Process/Third_Party_Licenses|process to update the third party licences]] and resumed my work on the apply modifier and transform for multi-user data objects.

[[File:Apply-transforms.jpg|frameless|500px|center]]


;'''Patches'''
* https://developer.blender.org/D14377
* https://developer.blender.org/D14381

;'''Commits'''
* Fix `make source_archive_complete` for release branches [https://projects.blender.org/blender/blender/commit/643da14a4ef 643da14a4e].