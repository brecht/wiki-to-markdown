= Weekly Reports: 2023 =

== Since May 1 ==

'''New location for 2023 reports: https://projects.blender.org/JulianEisel/Weekly-Reports/src/branch/main/2023.md'''

== April 24 - April 30 ==

3 work days only (national holiday plus one day off). Focused on catching up with reviews and GSoC proposal reviews (there were plenty for me to go through).

* GSoC proposal reviews (signed up to mentor a project).

'''Main Branch Commits:'''
* Cleanup: Avoid unnecessary & misleading Outliner context function call ([https://projects.blender.org/blender/blender/commit/1fad165c1a 1fad165c1a])

'''Review:'''
* Fix #107186: Curve assets don't generate preview images ([https://projects.blender.org/blender/blender/pulls/107214 PR #107214])
* macOS: Add open files to system recent files ([https://projects.blender.org/blender/blender/pulls/107174 PR #107174])
* Fix #107294 : Node Editor "Frame All/Selected" ignore Smooth View ([https://projects.blender.org/blender/blender/pulls/107296 PR #107296])
* UI: OS File Operations Within File Browser ([https://projects.blender.org/blender/blender/pulls/104531 PR #104531])
* macOS/File Browser: Support external operations ([https://projects.blender.org/blender/blender/pulls/107267 PR #107267])
* Initial implicit sharing docs ([https://projects.blender.org/JulianEisel/devdocs/pulls/1 PR #1])
* Suport relative path option per-asset library ([https://projects.blender.org/blender/blender/pulls/107345 PR #107345])
* Fix #104722: Outliner Renaming Double Click Error ([https://projects.blender.org/blender/blender/pulls/105872 PR #105872])

=== Next Week ===
* Focus on brush assets / asset shelf.

== April 17 - April 23 ==

4 days of sick leave.

* Looked into [https://projects.blender.org/blender/blender/pulls/104737 PR #104737] (Fix #89479: Unable to select hierarchies of multiple objects from outliner). Needs further investigation.

'''Main Branch Commits:'''
* Cleanup: Define type for object type enum ([https://projects.blender.org/blender/blender/commit/fe7540d39a fe7540d39a])

'''Review:'''
* Fix #90159: Inconsistent display of active filters for import/export file dialogs ([https://projects.blender.org/blender/blender/pulls/107034 PR #107034])
* Brush Assets: Create polished mockup for the Draft operators ([https://projects.blender.org/blender/blender/issues/106898 #106898])
* Assets: Do Not Show Blank Read-Only Metadata ([https://projects.blender.org/blender/blender/pulls/105812 PR #105812])

=== Next Week ===

* Focus on brush assets / asset shelf.
* More GSoC proposal reviews.

== April 10 - April 16 ==

Day off on Monday (Easter).

* Asset shelf (brush assets project) - a bunch of fixes and general polish, things look a lot more closer to being usable now:
** Fix error when re-registering a `bpy.types.AssetShelf` subclass ([https://projects.blender.org/blender/blender/commit/73a2c3453c 73a2c3453c])
** Fix uninitialized asset shelf theme color in existing theme ([https://projects.blender.org/blender/blender/commit/a3baa62487 a3baa62487])
** Support region overlap for entire asset shelf, fix overlapping regions ([https://projects.blender.org/JulianEisel/blender/commit/6e713bbdb3 6e713bbdb3])
** Use slight transparency for asset shelf by default ([https://projects.blender.org/JulianEisel/blender/commit/0cf345a673 0cf345a673])
** Decrease default asset preview size in asset shelf ([https://projects.blender.org/JulianEisel/blender/commit/41d8c3944d 41d8c3944d])
** Add display popover and "Show Names" option ([https://projects.blender.org/blender/blender/commit/2acf304e14 2acf304e14])
** [Grid view branch] Make new "Show Names" option work ([https://projects.blender.org/JulianEisel/blender/commit/5fc17af505 5fc17af505])
** Draw a scrollbar for the main asset shelf region ([https://projects.blender.org/JulianEisel/blender/commit/6340d47454 6340d47454])
** More work on paginated scrolling, still WIP.

'''Main Branch Commits:'''
* Python: Support multiple custom script directories in Preferences ([https://projects.blender.org/blender/blender/commit/ba25023d22 ba25023d22])
* Cleanup: Update versioning code after subversion bump ([https://projects.blender.org/blender/blender/commit/d299b1157f d299b1157f])
* Fix #106840: Add-ons not loading from custom script directories ([https://projects.blender.org/blender/blender/commit/ac09d18e4e ac09d18e4e])
* Fix user add-ons not showing up with "User" filter in Preferences ([https://projects.blender.org/blender/blender/commit/da75495ada da75495ada])
* UI: Quick tooltip showing tab name for Properties editor tabs ([https://projects.blender.org/blender/blender/pulls/106906 PR #106906])

'''Review:'''
* Fix #106775: File Draw Negative Width Buttons ([https://projects.blender.org/blender/blender/pulls/106777 PR #106777])
* Assets: Implement Traits ([https://projects.blender.org/blender/blender/pulls/105841 PR #105841])
* Assets: Do Not Show Blank Read-Only Metadata ([https://projects.blender.org/blender/blender/pulls/105812 PR #105812])
* UI: Tooltips for Assets ([https://projects.blender.org/blender/blender/pulls/106189 PR #106189])
* Fix #66722: Blender doesn't open file if isn't focused ([https://projects.blender.org/blender/blender/pulls/106769 PR #106769])

=== Next Week ===

* Focus on brush assets / asset shelf.
* More GSoC proposal reviews.

== April 3 - April 9 ==

* First round of GSoC proposal reviews.
* Asset shelf (brush assets project):
** Add theme options for the asset shelf ([https://projects.blender.org/blender/blender/commit/253b417225 253b417225])
** Fix wrong theme setting used for asset shelf footer/header ([https://projects.blender.org/blender/blender/commit/d067e77ee6 d067e77ee6])
** Change default color for asset shelf footer/header ([https://projects.blender.org/blender/blender/commit/b7a9fed4bc b7a9fed4bc])
** Remove unnecessary & problematic header layout changes ([https://projects.blender.org/JulianEisel/blender/commit/b35dd323c3 b35dd323c3])
** Add "All" catalog tab that is always visible ([https://projects.blender.org/blender/blender/commit/b93fe0f9e0 b93fe0f9e0])
** Further work on [https://projects.blender.org/blender/blender/pulls/105088 PR #105088] (UI: Region polling support). Merged into `main` now (see below).
** Use new region poll to remove asset shelf based on context ([https://projects.blender.org/JulianEisel/blender/commit/2e288dad63 2e288dad63])
** Support and use custom page size for scrolling whole rows in asset shelf ([https://projects.blender.org/JulianEisel/blender/commit/852becd76c 852becd76c])
** Show asset shelf regions by default as soon as poll succeeds ([https://projects.blender.org/JulianEisel/blender/commit/e813afe7c5 e813afe7c5])
** Snap asset shelf region to rows ([https://projects.blender.org/JulianEisel/blender/commit/6da90998e2 6da90998e2])
** Support zooming in the asset shelf ([https://projects.blender.org/JulianEisel/blender/commit/32c080f65a 32c080f65a])
** Experiments on paginated scrolling with thumbnail scaling support. A bit tricky.
* Further work on [https://projects.blender.org/blender/blender/pulls/104876 PR #104876] (Python: Support multiple custom script directories in Preferences).
* Got branch to rewrite the Asset Browser UI up to date with overlapping changes in `main` ("fun day" work).
* Investigated [https://projects.blender.org/blender/blender/pulls/106374 PR #106374] (Fix #106246: Crash on deleting the multi-level hierarchy).

'''Main branch commits:'''
* UI: Region polling support ([https://projects.blender.org/blender/blender/commit/fa0f295b53 fa0f295b53])
* Fix crash loading pre 2.5 test file (Cycles test failure) ([https://projects.blender.org/blender/blender/commit/56640440f0 56640440f0])

=== Next Week ===

* Focus on brush assets / asset shelf.
* Review of asset system/browser patches (asset traits support, asset tooltips, etc.).
* More GSoC proposal reviews.

== March 27 - April 2 ==

* Brush assets project:
** Further work on asset weak references, merged into main branch now (commit below):
*** Fix missing error check ([https://projects.blender.org/JulianEisel/blender/commit/9d4374e182 9d4374e182])
*** Cleanup: More clear naming, comments, etc ([https://projects.blender.org/JulianEisel/blender/commit/e93f1868ec e93f1868ec])
*** Fix failing unit tests after previous fix, was hidden by casts ([https://projects.blender.org/JulianEisel/blender/commit/e58fa6de99 e58fa6de99])
*** Bring back lost comment ([https://projects.blender.org/JulianEisel/blender/commit/ff0a251166 ff0a251166])
*** Avoid unsigned <-> signed integer casts ([https://projects.blender.org/JulianEisel/blender/commit/a943c68d42 a943c68d42])
*** Small readability improvement ([https://projects.blender.org/JulianEisel/blender/commit/a2597f98ab a2597f98ab])
*** Fix crash in unit tests ([https://projects.blender.org/JulianEisel/blender/commit/aac318fe3c aac318fe3c])
** WIP work to enable asset shelf region snapping to rows.
* Fixed high priority crash in node add menu, was a bit tricky (pull request below).
* Feedback on GSoC proposals
* Technical Documentation Day:
** Pair documentation session with Falk David to kick off Grease Pencil 3.0 documentation work.
** Fixed own [https://projects.blender.org/JulianEisel/devdocs "devdocs" repository] setup to bring back an [https://julianeisel.github.io/devdocs/ online version] of the generated documentation (for now using GitHub pages still).

'''New pull requests''':
* Fix #105855: Crash with node add menu assets and keyboard navigation ([https://projects.blender.org/blender/blender/pulls/106237 PR #106237])
* WIP: Brush assets project ([https://projects.blender.org/blender/blender/pulls/106303 PR #106303])
* WIP: Asset Shelf Grid-View ([https://projects.blender.org/blender/blender/pulls/106316 PR #106316])

'''Main/release branch commits:'''
* UI: Support minimum row count for tree views ([https://projects.blender.org/blender/blender/commit/0256bfd309 0256bfd309])
* Asset System: New "weak" asset reference for storing in .blend files ([https://projects.blender.org/blender/blender/commit/d90795bc3c d90795bc3c])

=== Next Week ===

* GSoC proposals (feedback and first pass review)
* Focus on brush assets / asset shelf.

== March 20 - March 26 ==

* Worked on generalizing drop support for UI elements, needed for the Cycles light linking project:
** UI: Generalize drop controllers, support them for UI views ([https://projects.blender.org/JulianEisel/blender/commit/71e0c6c043 71e0c6c043])
** Rename drop controller -> drop target ([https://projects.blender.org/JulianEisel/blender/commit/232a9a9f10 232a9a9f10])
** Move new API functions to blender::ui namespace, remove `UI_` prefix ([https://projects.blender.org/JulianEisel/blender/commit/a3886ebbbc a3886ebbbc])
* Further work on weak asset references for the brush assets project:
** Some refactoring for function to resolve to an exploded path ([https://projects.blender.org/JulianEisel/blender/commit/693be95b2d 693be95b2d])
** Basic unit tests for the new path resolving and exploding function ([https://projects.blender.org/JulianEisel/blender/commit/9a56b28771 9a56b28771])
** Fix test failure when test asset path is absolute with ".." components ([https://projects.blender.org/JulianEisel/blender/commit/cb96a82aa6 cb96a82aa6])
** Address points from review ([https://projects.blender.org/JulianEisel/blender/commit/873097b0e6 873097b0e6])
** Fix test failure on macOS because of short string optimization of moved result ([https://projects.blender.org/JulianEisel/blender/commit/4710dc0337 4710dc0337])
* Further work on [https://projects.blender.org/blender/blender/pulls/105088 PR #105088] (UI: Region polling support), needed for the brush assets project:
** Fix broken sequencer when used from existing workspace ([https://projects.blender.org/JulianEisel/blender/commit/e58ac364af e58ac364af])
** Fix missing sequencer channels region when reading from old files ([https://projects.blender.org/JulianEisel/blender/commit/63b8e31a9c 63b8e31a9c])
** Avoid region state management in sequencer refresh callback ([https://projects.blender.org/JulianEisel/blender/commit/b5e69fd195 b5e69fd195])
* Further work on [https://projects.blender.org/blender/blender/pulls/104876 PR #104876] (Python: Support multiple custom script directories in Preferences):
** Address points from review ([https://projects.blender.org/JulianEisel/blender/commit/135c8aa032 135c8aa032])
** Make generic DNA struct specific to script directories ([https://projects.blender.org/JulianEisel/blender/commit/2663599d78 2663599d78])

'''Main/release branch commits:'''
* Cleanup: Refactor UI tree & grid view building API ([https://projects.blender.org/blender/blender/commit/8f6415418e 8f6415418e])
* UI: Generalize drop target API, support drop targets for UI views ([https://projects.blender.org/blender/blender/commit/a2d0f7049a a2d0f7049a])
* Fix UI view drag target not using correct boundaries with panels ([https://projects.blender.org/blender/blender/commit/64f3afb267ed7770778caf13ed47892a91815e1c 64f3afb267])
* Cleanup: Make drop target creation functions non const ([https://projects.blender.org/blender/blender/commit/8069b01c28 8069b01c28])

'''Review:'''
* UI: align tab labels towards panels ([https://projects.blender.org/blender/blender/pulls/105835 PR #105835])
* Fix blender_test not re-linked on macOS after changes ([https://projects.blender.org/blender/blender/pulls/106051 PR #106051])

=== Next Week ===

* Focus on brush assets / asset shelf.
* Technical Documentation Day.

== March 13 - March 19 ==

Mostly focused on high priority reports and asset brush tasks.

* Day off on Monday.
* Looked into remaining high priority reports for UI and assets, all fixed (see below) or classified as normal priority now.
** Includes a bigger & complicated fix for large asset libraries ([https://projects.blender.org/blender/blender/issues/104305 #104305]).
* Meeting with Harley to organize his development grant work.
* New tasks for asset traits:
** Asset Types & Traits ([https://projects.blender.org/blender/blender/issues/105807 #105807])
** Implement Asset Traits ([https://projects.blender.org/blender/blender/issues/105808 #105808])
* Further work on weak asset references:
** Add functions to resolve a full path to an asset from the weak reference ([https://projects.blender.org/JulianEisel/blender/commit/84503ebf34 84503ebf34])

'''Main/release branch commits:'''
* Fix #105180: "All" asset library includes subfolders of current file ([https://projects.blender.org/blender/blender/commit/c4d6f766de c4d6f766de])
* File Browser: Make full file name label draggable ([https://projects.blender.org/blender/blender/commit/c3dfe1e204 c3dfe1e204])
* Fix tiny file browser highlight offset on retina screens ([https://projects.blender.org/blender/blender/commit/bb0ee9fdb4 bb0ee9fdb4])
* Cleanup: Move blend file path utilities out of blenloader module ([https://projects.blender.org/blender/blender/commit/48814c25fd 48814c25fd])
* Cleanup: Improve comment on library path utility function ([https://projects.blender.org/blender/blender/commit/b80f6c4017 b80f6c4017])
* Refactor: Avoid unsafe strcpy in library path utility function ([https://projects.blender.org/blender/blender/commit/d1b8a827a6 d1b8a827a6])
* Assets: Add function to query data-block library path from asset ([https://projects.blender.org/blender/blender/commit/55811b2919 55811b2919])
* Fix build error after previous merge ([https://projects.blender.org/blender/blender/commit/cb20f2cbf9 cb20f2cbf9])
* Fix #104305: Crash in node editor with large asset libraries ([https://projects.blender.org/blender/blender/commit/a958ae36e8 a958ae36e8])

'''Review:'''
* Fix #105216: Clear Asset does not immediately redraw the outliner ([https://projects.blender.org/blender/blender/pulls/105287 PR #105287])
* UI: Increase the size of the "Open Recent" list ([https://projects.blender.org/blender/blender/pulls/105703 PR #105703])
* Asset Browser/File Browser: Add display name tooltip to tile view ([https://projects.blender.org/blender/blender/pulls/104693 PR #104693])
* WIP: Assets: Implement Traits ([https://projects.blender.org/blender/blender/pulls/105841 PR #105841])

=== Next Week ===

* Focus on brush assets / asset shelf.

== March 6 - March 12 ==

=== Next Week ===

== February 27 - March 5 ==

* Brush assets / asset shelf work:
** New grid-view based layout, to replace the current prototype-y UI: ([https://projects.blender.org/JulianEisel/blender/commit/50292f1018b88668a654046525e63e3a1371bbf9 50292f1018])
** Split asset shelf file into multiple files ([https://projects.blender.org/blender/blender/commit/62af85aed4b4b2847a9637ac36747a4bc0b032c2 62af85aed4])
* Technical Documentation:
** ''Created task:'' [Draft] New Developer Documentation Infrastructure Proposal (Replacing Wiki?) ([https://projects.blender.org/blender/blender/issues/105348 #105348])
** ''Created task:'' [WIP] New Developer Documentation Infrastructure: Tasks, Questions, Plan of Action ([https://projects.blender.org/blender/blender/issues/105349 #105349])
** ''Created task:'' Proposal: Rename repository to "blender-manual" ([https://projects.blender.org/blender/documentation/issues/104380 #104380])
** A bit of reading about buildbot so I can help setting up a CD environment for the proposed platform.
* Discussed changes to asset metadata fields for the 3.5 release: Support "license" and "copyright" meta-data instead of "author" ([https://projects.blender.org/blender/blender/issues/105300 #105300])
* Meeting/discussion on Grease Pencil 3.0 design, mainly with regards to grease pencil assets and technical design.
* WBSO sheets (government subsidy)

'''Main Branch Commits:'''
* UI: Refactor path dropping so logic doesn't depend on icons ([https://projects.blender.org/blender/blender/commit/17e92711d3bdf8b587bd484977f2ca1eca4116f8 17e92711d3])
* Cleanup: More clear variable name in File Browser drawing ([https://projects.blender.org/blender/blender/commit/0786b05175b13f88b305cadb4f4ab64ca3fd75c5 0786b05175])
* Remove duplicated code from merge conflict ([https://projects.blender.org/blender/blender/commit/7d15670a2ac38a4b3f19b27835d21ccd0f071453 7d15670a2a])

'''Review:'''
* Outliner: New Grease Pencil filter ([https://projects.blender.org/blender/blender/pulls/104473 PR #104473])

=== Next Week ===

* Focus on asset shelf work

== February 20 - February 26 ==

* Asset shelf work (`asset-shelf` branch):
** Support registering asset shelves in BPY (see PR below)
** Update pose library add-on for new asset shelf BPY support ([https://projects.blender.org/blender/blender-addons/commit/2b118a1cda29 2b118a1cda])
** Added support for polling regions, necessary to make the asset shelf unavailable based on context (See PR below).
* Technical documentation:
** Copied missing task from Phabricator to Gitea ([https://projects.blender.org/blender/blender/issues/105075 #105075: Technical Documentation: Guidelines, Templates, Examples & Other Resources])
** [New task:] Prioritized Documentation TODOs ([https://projects.blender.org/blender/blender/issues/105077 #105077])
** Organized the (small) project board.
* Updated `asset-browser-grid-view` branch which was a bit behind (branch to rewrite the Asset Browser UI as a separate editor).
* [Suggested fix:] VSE Preview - assign hotkeys to the toolshelf tools adds them to the Window category ([https://projects.blender.org/blender/blender/issues/92140 #92140])
* WBSO sheets (government subsidy)

'''New pull requests''':
* [Old work] WIP: Rewrite asset browser as separate editor ([https://projects.blender.org/blender/blender/pulls/104978 PR #104978])
* WIP: Basic support for registering asset shelf as a type in BPY ([https://projects.blender.org/blender/blender/pulls/104991 PR #104991])
* UI: Region polling support ([https://projects.blender.org/blender/blender/pulls/105088 PR #105088])

'''Master/release branch commits:'''
* Revert "GPencil: Include UV information in simplify->sample modifier." ([https://projects.blender.org/blender/blender/commit/3eed00dc5 3eed00dc5])
* Revert release branch only commit after merge ([https://projects.blender.org/blender/blender/commit/c437a8aea8 c437a8aea8])
* Fix unused variable warnings in release build ([https://projects.blender.org/blender/blender/commit/5fd4d47206f 5fd4d47206])
* Fix new essentials asset library not being covered in library path query ([https://projects.blender.org/blender/blender/commit/a280554b75 a280554b75])
* UI: Properly cancel dragging on escape or right-click ([https://projects.blender.org/blender/blender/commit/d58e422ac3 d58e422ac3])

'''Review:'''
* Icons maintenance ([https://projects.blender.org/blender/blender/pulls/104954 PR #104954])
* UI: Add color selector to Bone Groups list ([https://projects.blender.org/blender/blender/pulls/105036 PR #105036])
* Fix #103269: node group asset description not showing as tooltip in the Add menu ([https://projects.blender.org/blender/blender/pulls/104968 PR #104968])
* Fix #104992: Crash on calling operation search in outliner ([https://projects.blender.org/blender/blender/pulls/105004 PR #105004])

=== Next Week ===

* Brush assets project (asset shelf)
* UI & Asset Browser patch review
* Organize UI work for/with Harley

== February 13 - February 19 ==

Mostly work to wrap up asset features and changes for the 3.5 release.

* Joined Animation & Rigging module meeting to demo the asset shelf for pose libraries.
* Internal development planning meeting for 2023.
* Removed outdated GSoC project idea (asset catalog selector for asset view templates).
* Worked on UI changes to improve asset import method selection (PRs and commits are below):
** ''[Created design task]'' Improve import method selection for assets ([https://projects.blender.org/blender/blender/issues/104686 #104686])
* Updated 3.5 release notes for UI changes & the new essentials asset library.

'''Created pull requests:'''
** ''[Created & merged]'' Assets: Preference for default import method for an asset library ([https://projects.blender.org/blender/blender/pulls/104688 PR #104688])
** ''[Created & merged]'' Assets/UI: Use UI-list for asset library preferences UI ([https://projects.blender.org/blender/blender/pulls/104710 PR #104710])
* File Browser: Make full file name label draggable ([https://projects.blender.org/blender/blender/pulls/104830 PR #104830])
* UI: Properly cancel dragging on Esc & right-click ([https://projects.blender.org/blender/blender/pulls/104838 PR #104838])
* Updated some patches and created new PRs for them on gitea:
** WIP: Asset Shelf ([https://projects.blender.org/blender/blender/pulls/104831 PR #104831])
** WIP: Basic Blender Project Support (experimental feature) ([https://projects.blender.org/blender/blender/pulls/104883 PR #104883])
** Python: Support multiple custom script directories in Preferences ([https://projects.blender.org/blender/blender/pulls/104876 PR #104876])

'''Master & release branch commits:'''
* Assets: Store pointer to owning asset library in asset representation ([https://projects.blender.org/blender/blender/commit/99e71ec1f2 99e71ec1f2])
* Cleanup: Remove redundant translation markers in context poll message ([https://projects.blender.org/blender/blender/commit/9b7d71cec2 9b7d71cec2])
* Assets: Preference for default import method for an asset library ([https://projects.blender.org/blender/blender/commit/ae84a2956e ae84a2956e])
* Asset Browser: Rename "Import Type" to "Import Method" ([https://projects.blender.org/blender/blender/commit/972f58c482 972f58c482])
* Assets/UI: Use UI-list for asset library preferences UI ([https://projects.blender.org/blender/blender/commit/0d798ef57c 0d798ef57c])
* Asset Browser: Hide import method menu for essentials ([https://projects.blender.org/blender/blender/commit/e3b5a2ae15 e3b5a2ae15])
* Asset Browser: Add separator line & icon padding for import method menu ([https://projects.blender.org/blender/blender/commit/5bac672e1a 5bac672e1a])
* Fix weird icon padding in asset library selector menu button ([https://projects.blender.org/blender/blender/commit/1116d821dc 1116d821dc])
* UI: Ensure menus with icons align all menu item labels ([https://projects.blender.org/blender/blender/commit/58752ad93c 58752ad93c])
* Revert changes to align asset library labels in menus ([https://projects.blender.org/blender/blender/commit/05b177b0b3 05b177b0b3])
* Cleanup: Refactor File/Asset Browser button dragging code ([https://projects.blender.org/blender/blender/commit/1e9564864c 1e9564864c])
* UI: Refactor how draggable part of button is determined ([https://projects.blender.org/blender/blender/commit/6da512f0bc 6da512f0bc])

'''Review:'''
* Assets: bundle essentials with Blender ([https://projects.blender.org/blender/blender/pulls/104474 PR #104474])
* GPencil: New support for Asset Manager ([https://projects.blender.org/blender/blender/pulls/104413 PR #104413])
* Fix #104785: Quick fur keeps asset status of appended node groups ([https://projects.blender.org/blender/blender/pulls/104828 PR #104828])

=== Next Week ===

* Brush assets project.
* Technical documentation day.

== February 6 - February 12 ==

3 days of sick leave. Remaining time was spent on asset related tasks for 3.5 and the Gitea move happened this week (while I was sick)

* Updated own local repository for the Gitea workflow and started getting familiar with the new environment.
* Ported experimental technical developer documentation setup from own github to a [https://projects.blender.org/JulianEisel/devdocs repository on projects.blender.org].

Master Commits:
* Remove Phabricator based weekly report script ([https://projects.blender.org/blender/blender-dev-tools/commit/e0b412c9401e e0b412c940])

Review:
* Assets: Implement viewport drag and drop for geometry nodes ([https://projects.blender.org/blender/blender/pulls/104430 PR #104430])
* Assets: bundle essentials with Blender ([https://projects.blender.org/blender/blender/pulls/104474 PR #104474])
* Did an in person review of the proposed hair assets with Simon and Dalai.

=== Next Week ===

* UI changes to make asset import method selection more clear.
* Review Grease Pencil asset patch.
* Brush assets project.

== January 30 - February 5 ==

== January 23 - February 29 ==

== January 16 - January 22 ==

Worked for 4 days only. Managed to focus mostly on brush assets.

* Further work on the asset shelf (as part of the brush assets project):
** Basic asset shelf prototype ([https://projects.blender.org/blender/blender/commit/8ddf492e7cf6  rB8ddf492e])
** Extend asset shelf region with a region for the catalogs & options ([https://projects.blender.org/blender/blender/commit/b3ee7ad2ccbf  rBb3ee7ad2])
** Popup to select which catalogs are displayed in the asset shelf footer ([https://projects.blender.org/blender/blender/commit/4fa69fbda887  rB4fa69fbd])

'''Master Commits:'''
* Cleanup: Rename confusing region variable ([https://projects.blender.org/blender/blender/commit/2c6ed49c0343  rB2c6ed49c])
* Fix crash when listing assets repeatedly in node search menus ([https://projects.blender.org/blender/blender/commit/e4e91bf8301a  rBe4e91bf8])

'''Created patches:'''
* Python: Support multiple custom script directories in Preferences. ([http://developer.blender.org/D17007 D17007])

'''Review:'''
* [Accepted] Fix T103881: Unlink operation crash in Blender File view ([http://developer.blender.org/D17017 D17017])

=== Next Week ===

* Focus on brush assets.
* Catch up with high priority reports.
* General code review.
* Technical documentation day.

== January 9 - January 15 ==

Only halfway working this week (I'm moving!).

* Finished "All" asset library patch and merged into master (see below for master commits)
** Fix all library always tagging catalogs as changed ([https://projects.blender.org/blender/blender/commit/a83d40c497c9  rBa83d40c4])
** Cleanups after informal review with Sybren ([https://projects.blender.org/blender/blender/commit/59eec2f67da3  rB59eec2f6])
* Updated Blender projects branch:
** Support & use new asset library path query from master ([https://projects.blender.org/blender/blender/commit/e63671c21d7b  rBe63671c2])
** Support project asset libraries in new asset library loading ([https://projects.blender.org/blender/blender/commit/b999b79d087c  rBb999b79d])
* Worked on patch to support multiple custom script directories in the Preferences. ([http://developer.blender.org/P3438 P3438])
* Added [[Human_Interface_Guidelines/Best_Practices#Workflow_Oriented|Human Interface Guidelines entry]] on workflow oriented design as opposed to entity related design (

'''Master Commits:'''
* Cleanup: Compile `filesel.c` in C++ ([https://projects.blender.org/blender/blender/commit/9b8c2f91f606  rB9b8c2f91])
* Assets: "All" asset library ([https://projects.blender.org/blender/blender/commit/35e54b52e6ec  rB35e54b52])
* Refactor: Use new "All" asset library to extend node menus with assets ([https://projects.blender.org/blender/blender/commit/66af16571dfe  rB66af1657])

'''Review:'''
* [Accepted] Fix: Missing null checks for asset catalog. ([http://developer.blender.org/D16995 D16995])
* [Gave feedback] Widen the NLA action drop-down menu. ([http://developer.blender.org/D16857 D16857])]

=== Next Week ===

* Focus on brush assets.
* Technical documentation day?


== January 2 - January 8 ==

Came back from holidays mid-week, so had to deal with the usual: Catching up with mails, broken Blender builds, merge branches, etc.

'''Master Commits:'''
* Fix empty asset index files after bug in asset loading ([https://projects.blender.org/blender/blender/commit/1229b966524b  rB1229b966])

'''Review:'''
* [Accepted] UI: Skip expensive view button searching when drawing ([http://developer.blender.org/D16882 D16882])
* [Accepted] BLI: Refactor matrix types & functions to use templates ([http://developer.blender.org/D16625 D16625])

=== Next Week ===

* More catch up work.
* Brush assets.