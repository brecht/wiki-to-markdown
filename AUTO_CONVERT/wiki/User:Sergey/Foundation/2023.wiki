= Weekly Reports for year 2023 =

== Week 576: 2nd - 8th January ==

* <strong>General development</strong>
** Catching up with emails and other backlog since the holiday
** Regular design and code reviews

== Week 577: 9th - 15th January ==

* <strong>General development</strong>
** Regular design and code reviews
** Most of the week is the Gitea migration project

== Week 578: 16th - 22th January ==

* <strong>General development</strong>
** Full week working on the Gitea migration

== Week 578: 23th - 29th January ==

* <strong>General development</strong>
** Full week working on the Gitea migration

== Week 579: 30th January - 5th February ==

* <strong>General development</strong>
** Full week working on the Gitea migration ...
** ... But also managed to catch up with the backlog of the code/design reviews

== Week 580: 6th - 12th February ==
** Full week working on the Gitea migration ...
** ... aaand it's migrated!
** Quite some time spent dealing with the unforeseen consequences
** But also managed to put a bit of time for patch review

== Week 581: 13th - 19th February ==

* <strong>General development</strong>
** Still some time spent dealing with the consequences of migration
** Worked on the Git submodules re-organization to solve the confusion with Git tooling
** Blender Admins meeting
** Was catching up with the module backlog (the unclassified reports list)
** Small amount of regular design and code reviews

== Week 582: 20th - 26th February ==

* <strong>General development</strong>
** Changes in the submodules organization for blender.git
** Changes in `make update` to support new submodule organization, and Github style of remotes
** Regular design and code reviews

== Week 583: 27th February - 5th March ==

* <strong>General development</strong>
** Regular design and code reviews
*** The most remarkable is the grease pencil workshop here

== Week 584: 6th -12th March ==

* <strong>General development</strong>
** Workshops for grease pencil, Eevee, rendering for the upcoming open movie project
** Smaller fixes for the current studio project

== Week 585: 13th - 19th March ==

* <strong>General development</strong>
** Regular design and code reviews
** Worked on the light linking project. Together with the Team got the basics of the UI and Cycles kernel supported

== Week 586: 20th - 26th March ==

* <strong>General development</strong>
** Regular design and code reviews
** Continued working on the light linking project

== Week 587: 27th March - 2nd April ==

* <strong>General development</strong>
** Regular design and code reviews
** Continued working on the light linking project
** Fix un-initialized variable, which lead to a difficult to track random wrong results of masking node
** Buildbot: run tests in parallel (making this step 2-4x faster depending on a platform)
*** Needed to fix non-parallelizable tests (IO tests using the same temp file name)

== Week 588: 3rd - 9th April ==

* <strong>General development</strong>
** Regular design and code reviews
** Continued working on the light linking project
** Parallel regression tests dead-lock investigation
*** Turns out to be an issue in the idiff.exe
*** Worked around on the Blender buildbot side, but upstream repro case and report is needed

== Week 589: 10th - 16th April ==

* <strong>General development</strong>
** Regular design and code reviews
** Continued working on the light linking project
** Still some time was spent dealing with dead-lock of idiff.exe on the buildbot

== Week 590: 17th - 23rd April ==

* <strong>General development</strong>
** Regular design and code reviews
** Continued working on the light linking project (although, did not have as much time as I've hoped to)
** Cleanup of compiler warnings with default compilation configuration on Apple Silicon
** Fix regression tests failure on the latest Xcode

* <strong>Bug tracker</strong>
** Fix [http://developer.blender.org/T107081 #107081] Slow selection with context variables
** Fix [http://developer.blender.org/T107127 #107127] Context property driver to view layer does not work
** Fix [http://developer.blender.org/T106977 #106977] Crash when OpenEXR IO fails

== Week 591: 24th - 30th April ==

* <strong>General development</strong>
** Regular design and code reviews
** Continued working on the light linking project: got initial support of shadow linking + MIS
** Fix intersection distance offset in Cycles (precision error, could cause back pixels and maybe infinite loop in some rare cases)

== Week 592: 1st - 7th May ==

* <strong>General development</strong>
** Regular design and code reviews
** Refactor of ImBuf and RenderResult areas to support implicit sharing. The former part is under the review now
** Continued working on the light linking project: shadow linking + MIS is in the cycles-light-linking branch now

== Week 593: 8th - 14th May ==

* <strong>General development</strong>
** Regular design and code reviews
** Infrastructure/buildbot support for CUDA 12
** Continued working on the light linking project: multiple shadow linked lights, distant lights, mesh lights

== Week 594: 15th - 21st May ==

* <strong>General development</strong>
** Regular design and code reviews
** The ImBuf refactor is committed, the implicit sharing for the render result is reviewed and ready land
** Continued working on the light linking project. Still some things to do there, but there is only one merge blocker

== Week 595: 22nd - 28th May ==

* <strong>General development</strong>
** Regular design and code reviews
** Cycles: light and shadow linking committed to the main branch
** BLI: Replace some macros with inlined functions for C++
** Refactor Render Result to allow implicit buffer sharing

* <strong>Bug tracker</strong>
** Fix [http://developer.blender.org/T108316 #108316] CUDA error rendering Attic scene
** Fix [http://developer.blender.org/T108315 #108315] Illegal CUDA address with shadow-linked mesh emitters
** Fix [http://developer.blender.org/T108240 #108240] Cycles fails on macOS and AMD GPU
** Fix [http://developer.blender.org/T108250 #108250] Cycles: Light Linking disabled after modifying object
** Fix [http://developer.blender.org/T108243 #108243] Crash when save before close with modified image
** Fix [http://developer.blender.org/T107248 #107248] Compositor ACCESS VIOLATION when updating datablocks from handlers
** Fix [http://developer.blender.org/T108136 #108136] Regression : Texture paint is broken
** Fix specialized light tree building for distant lights
** Fix hiding receiver disabling light/shadow linking
** Fix assert in light linking when making an asset.
** Fix crash doing viewport animation render
** Fix make update for upstream workflow and missing branch in submodule

== Week 596: 29th May - 4th June ==

* <strong>General development</strong>
** Regular design and code reviews

* <strong>Bug tracker</strong>
** Fix [http://developer.blender.org/T108342 #108342] Regression: Cycles: Light group does not work
** Fix [http://developer.blender.org/T108374 #108374] Sun light linking issue on direct sampling
** Fix [http://developer.blender.org/T108363 #108363] Light linking does not work when linked to self

== Week 597: 5th - 11th June ==

* <strong>General development</strong>
** Regular design and code reviews
** Quite some time spent helping the studio (processes, tools design etc)
** Work on fixes and optimizations for the newly landed Kuwahara filter

* <strong>Bug tracker</strong>
** Fix [http://developer.blender.org/T108778 #108778] Crash when rendering multiple view layers

== Week 598: 12th - 18th June ==

* <strong>General development</strong>
** Regular design and code reviews
** Various fixes and improvements in Cycles and its automated benchmarking script

* <strong>Bug tracker</strong>
** Fix [http://developer.blender.org/T109019 #109019] Crash when creating a new collection for light linking

== Week 599: 19th - 25th June ==

* <strong>General development</strong>
** Regular design and code reviews
** Small refactors, as a preparation for a bigger change for the render pass support for the viewport compositor

== Week 600: 26th June - 2nd July ==

* <strong>General development</strong>
** Regular design and code reviews.
** Short week, the most of it was spent at EGSR.

== Week 601: 3rd - 9th July ==

* <strong>General development</strong>
** Catching up after the EGSR.
** Regular design and code reviews.
** Various fixes for compilers and regression tests.
** Removed z-buffer image save option.
** Finished refactoring the RenderPass storage on Blender side. Will be committed on Monday. (This and above is a part of the render passes support for viewport compositor).

== Week 602: 10th - 16th July ==

* <strong>General development</strong>
** Regular design and code reviews.
** Refactor: Use ImBuf to store passes in RenderResult

* <strong>Bug tracker</strong>
** Fix [http://developer.blender.org/T109887 #109887] Adding driver from python might crash

== Week 603: 17th - 23rd July ==

* <strong>General development</strong>
** Regular design and code reviews.
** Refactors around the RenderResult to move forward with the viewport compositor passes

== Week 604: 24th - 30th July ==

* <strong>General development</strong>
** Regular design and code reviews.
** Various fixes.

== Week 605: 31st July - 6th August ==

* <strong>General development</strong>
** Shorter week, took some time off
** Regular design and code reviews
** Main development targets were the passes for viewport compositor and C-side changes needed for the AgX view transform

== Week 606: 7th - 13th August ==

* <strong>General development</strong>
** Regular design and code reviews
** Changes in color management related code needed to move forward with AgX

* <strong>Bug tracker</strong>
** Fix [http://developer.blender.org/T109201 #109201] Consistent mapping boundaries of brush textures

== Week 607: 14th - 20th August ==

* <strong>General development</strong>
** Regular design and code reviews
** More changes and fixes in color management related code needed to move forward with AgX:
*** Validate Look on View Transform changes
*** Validate look is compatible with view on load
*** Fix GradingPrimaryTransform OCIO transform in Metal
*** Tweak compatible look check: if a view has any specialized looks, only list specialized looks for the view

* <strong>Bug tracker</strong>
** Fix [http://developer.blender.org/T111165 #111165] Regression: Blender Crashes On Scene Switch

== Week 608: 21st - 27th August ==

* <strong>General development</strong>
** Regular design and code reviews

* <strong>Bug tracker</strong>
** Fix [http://developer.blender.org/T110328 #110328] Clay Strip symmetry does not mirror rotation
** Fix [http://developer.blender.org/T111504 #111504] Regression: Mesh draw corruption in sculpt mode

== Week 609: 28th August - 3rd September ==

* <strong>General development</strong>
** Regular design and code reviews

== Week 610: 4th - 10th September ==

* <strong>General development</strong>
** Regular design and code reviews
** Fixes for Cycles Metal and RT + shadow linking
** Fix crash in Cycles when cancelling render during light tree building

== Week 611: 11th - 17th September ==

* <strong>General development</strong>

* <strong>General development</strong>
** Regular design and code reviews
** Planning sessions at the studio
** Code sign certificate investigation

== Week 612: 18th - 24th September ==

* <strong>General development</strong>
** Regular design and code reviews
** Fix crash when toggling link linking state in the list
** Light linking: Make list interface look more similar to other places
** Design/prototype for procedural texturing in Compositor

* <strong>Bug tracker</strong>
** Fix [http://developer.blender.org/T112486 #112486] VSE: Rendering transparent frames produces errors/crashes

== Week 613: 25th September - 1st October ==

* <strong>General development</strong>
** Regular design and code reviews
** Light linking: Allow re-ordering in the light linking collections
** New code signing system for the buildbot and releases. Deployed to UATEST, PROD is the next step

== Week 614: 2nd - 8th October ==

* <strong>General development</strong>
** VSE: Fix Split Strips operator not showing shortcut
** macOS: Enable secure coding for restorable state
** Fix light linking state being lost on collection copy and duplicate

* <strong>Bug tracker</strong>
** Fix [http://developer.blender.org/T113186 #113186] The Render Result attribute "has_data" is always False
** Fix [http://developer.blender.org/T113280 #113280] Incorrect display of Cycles border render while rendering

* <strong>Sculpting</strong>
** Fix: Noisy false-positive assert in dyntopo sculpt
** Add vertex interpolation for the dynamic topology
** Improve support of face sets with dyntopo
*** Do not clear face sets when enabling dynamic topology
*** Draw face sets in viewport when dynamic topology is enabled
** Fix assert in PBVH face set drawing code

== Week 615: 9th - 15th October ==

* <strong>General development</strong>
** Regular design and code reviews
** Sculpt: Support Float2 attributes for dynamic topology
** Cycles: Add option to control smoothing when using bump map
** Buildbot: A lot of tweaks to make the code-sign and packaging pipeline robust.

* <strong>Bug tracker</strong>
** Fix [http://developer.blender.org/T113496 #113496] Crash in sculpt mode when Float2 attributes used on Vertices
** Fix [http://developer.blender.org/T112935 #112935] Wrong window order handling on macOS 14

== Week 616: 16th - 22nd October ==

* <strong>General development</strong>
** Regular design and code reviews
** Sculpt: Preserve edge attribute on split with dyntopo
** Sculpt: Worked on custom data preservation on edge collapse

* <strong>Bug tracker</strong>
** Fix [http://developer.blender.org/T113875 #113875] 2D Animation template's view transform set to AgX

== Week 617: 23rd - 29th October ==

* <strong>General development</strong>
** Short week due to BCON23
** Regular design and code reviews
** Continued with the dynamic topology: the edge subdivide/collapse with attribute preservation path is ready for review

== Week 618: 30th October - 5th November ==

* <strong>General development</strong>
** Regular design and code reviews
** Sculpt: Implement face set brush for dyntopo
** Sculpt: Better boundary preservation with dyntopo
** Depsgraph: Fix missing relations built for camera referenced by markers
** Buildbot: Solved issues with compile-gpu step timing-out

* <strong>Bug tracker</strong>
** Fix [http://developer.blender.org/T114129 #114129] Crashes on boolean operation macOS Intel

== Week 619: 6th - 12th November ==

* <strong>General development</strong>
** Regular design and code reviews
** Depsgraph: Fix missing relations build for cameras referenced from timeline markers
** Sculpt: Use C++ in more public API
** Sculpt: Experiment with preserving face set boundaries for dynamic topology. Ended up on requirement to piece extra state in many common areas. Which lead to the following work
** Sculpt: Looked into re-formalizing the brushes. Split implementation on the top level, split logic into more of a "kernel" approach. This should help with passing states, keeping it small and clear, as well as should help performance.

== Week 620: 13th - 19th November ==

* <strong>General development</strong>
** Regular design and code reviews

* <strong>Bug tracker</strong>
** Fix [http://developer.blender.org/T114661 #114661] Incorrect display of Rec.1886 and Rec. 2020 view transforms

== Week 621: 20th - 26th November ==

* <strong>General development</strong>
** Regular design and code reviews
** Helping with the infrasturcture