= Menus =

* Use separators to group similar items.
* Only add icons to the first item in a group, unless an icon for that item is often used (e.g. Copy and Paste go in the same group but both use icons).
  [[File:Menu example with separators and icons.png|frameless]]