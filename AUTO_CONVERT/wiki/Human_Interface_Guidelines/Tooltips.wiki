
{|class="note"
|-
|<div class="note_title">'''Guideline status'''</div>
<div class="note_content">Needs approval</div>
|}


= Tooltips =

Tooltips are little popups that give more information about the element under the mouse cursor.

== General rules ==

Follow the general [[Human_Interface_Guidelines/Writing_Style|writing style guidelines]].

What tooltips should contain:
* The name of the item, not followed by a period.
* A description of the item (see [[#The_tool_description|below]]).
* A "disabled hint" for disabled buttons: If a button is disabled, this text will explain why. (TODO document how to set this!).
* The shortcut, if any.
* The library path, if any.
* Additional <b>useful</b> information (expressions for drivers, BPY path, etc. - ''These fields may be automatically generated and may be disabled through a Preference option'').

== The Item Description ==

Tooltips should first and foremost be helpful. If more than a short sentence is needed to achieve that, by all means, use more than one. Yet, try to keep it short. In other words: Short '''and''' to the point.</b>
* It is recommended to start descriptions with verbs. Using the [https://en.wikipedia.org/wiki/Imperative_mood imperative conjugation] generally works best. <br/>'' '''Don't''': "The factor determine'''s''' how the geometry end factor is mapped to a spline"<br>'''Do''': "Determine how the geometry end factor is mapped to a spline"''
* Do '''not''' use more than a couple of lines, try to stick with two or three.
* Use full sentences (periods and commas are allowed).
* It's fine to mention examples of situations where you might use the tool.
* Do '''not''' document default values; these can change and are hard to keep updated.
* Do '''not''' use the described term to explained itself.<br/>
** ''Limit Surface<br/>'''Don't:''' "Put vertices at the limit surface"<br/> '''Do''': "Place vertices at the surface that would be produced with infinite levels of subdivision (smoothest possible object)".''
** '' Proportional Editing <br/>'''Don't''': "Enable Proportional Editing"<br>'''Do''': "Transform the neighbouring elements in a falloff from the selection" ''
* Explanations of how the result is affected by different values are fine.<br>''"Higher values reduce render time, lower values render with more detail."''
* Only describe the ''positive'' state of a toggle, '''not''' the negative<br/>'' '''Don't''': "Hide in Viewport" <br/>'''Do''': "Show in Viewport"''
* Do '''not''' use redundant words such as "''enables''", "''activates''" etc.<br/>'' '''Don't''': "Enables snapping during transform"<br/>'''Do''': "Snap during transform"''