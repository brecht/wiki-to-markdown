__NOTOC__
<div class="card-deck">

<div class="card bg-light mb-3">
  <div class="card-body">
    <h4 class="card-title">[[Developer_Intro|New Developer Introduction]]</h4>
    <h6 class="card-subtitle mb-2 text-muted">
Welcome! Advice on how to get started.</h6>
  </div>
</div>



<div class="card bg-light mb-3">
  <div class="card-body">
    <h4 class="card-title">[[Contact|Communication]]</h4>
    <h6 class="card-subtitle mb-2 text-muted">
The most important thing.</h6>
  </div>
</div>



<div class="card bg-light mb-3">
  <div class="card-body">
    <h4 class="card-title">[[Source|Code & Design Documentation]]</h4>
    <h6 class="card-subtitle mb-2 text-muted">
Technical documentation about the code and big picture design.</h6>
  </div>
</div>


</div>
<div class="card-deck">

<div class="card bg-light mb-3">
  <div class="card-body">
    <h4 class="card-title">[[Building_Blender|Building Blender]]</h4>
    <h6 class="card-subtitle mb-2 text-muted">
Instructions for compiling Blender locally.</h6>
  </div>
</div>



<div class="card bg-light mb-3">
  <div class="card-body">
    <h4 class="card-title">[[Modules|Modules]]</h4>
    <h6 class="card-subtitle mb-2 text-muted">
Blender components and their owners.</h6>
  </div>
</div>



<div class="card bg-light mb-3">
  <div class="card-body">
    <h4 class="card-title">[[Style_Guide|Style Guide]]</h4>
    <h6 class="card-subtitle mb-2 text-muted">
Coding guidelines and committer etiquette.</h6>
  </div>
</div>

</div>

<div class="card-deck">

<div class="card bg-light mb-3">
  <div class="card-body">
    <h4 class="card-title">[[Tools|Tools]]</h4>
    <h6 class="card-subtitle mb-2 text-muted">
Setup your development environment.</h6>
  </div>
</div>



<div class="card bg-light mb-3">
  <div class="card-body">
    <h4 class="card-title">[[Process|Process]]</h4>
    <h6 class="card-subtitle mb-2 text-muted">
Release cycle, BugTracker, Code Reviews and Testing.</h6>
  </div>
</div>



<div class="card bg-light mb-3">
  <div class="card-body">
    <h4 class="card-title">[[Reference/Release_Notes|Release Notes]]</h4>
    <h6 class="card-subtitle mb-2 text-muted">
What changed in each Blender version.</h6>
  </div>
</div>

</div>

<div class="card-deck">

<div class="card bg-light mb-3">
  <div class="card-body">
    <h4 class="card-title">[[GSoC|Google Summer of Code]]</h4>
    <h6 class="card-subtitle mb-2 text-muted">
A program that introduces students to open source software development.</h6>
  </div>
</div>



<div class="card bg-light mb-3">
  <div class="card-body">
    <h4 class="card-title">[[Python|Python]]</h4>
    <h6 class="card-subtitle mb-2 text-muted">
Learn about scripting and add-ons.</h6>
  </div>
</div>



<div class="card bg-light mb-3">
  <div class="card-body">
    <h4 class="card-title">[[Translation|Translation]]</h4>
    <h6 class="card-subtitle mb-2 text-muted">
Blender UI internationalization</h6>
  </div>
</div>

</div>

<div class="card-deck">

<div class="card bg-light mb-3">
  <div class="card-body">
    <h4 class="card-title">[[Infrastructure|Infrastructure]]</h4>
    <h6 class="card-subtitle mb-2 text-muted">
Details about the online ecosystem that supports Blender development.</h6>
  </div>
</div>



<div class="card bg-light mb-3">
  <div class="card-body">
    <h4 class="card-title">[[FAQ|FAQ]]</h4>
    <h6 class="card-subtitle mb-2 text-muted">
Common questions about the development process.</h6>
  </div>
</div>

</div>