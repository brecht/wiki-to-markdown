= Regression Testing of Geometry Nodes - Final Report =
This page documents the work done by Himanshi Kalra for Google Summer of Code 2021 on the project Regression Testing of Geometry Nodes.

== Summary ==

Over the Summer of 2021, I worked on developing a framework for Regression Testing of Geometry Nodes. The deliverables outlined in the [[User:HimanshiKalra/GSoC21/Proposal| Project Proposal]] have been implemented. Other tasks necessary for Regression Testing were implemented originally not part of the part of the proposal.


== Links ==
* Git branch: `soc-2021-geometry-nodes-regression-test`
* [[User:HimanshiKalra/GSoC21/Proposal|Proposal]]
* [https://devtalk.blender.org/t/gsoc-2021-regression-testing-of-geometry-nodes-weekly-reports/18954 Weekly Reports]
* [https://wiki.blender.org/wiki/User:HimanshiKalra/GSoC21/GeometryNodeTests Adding test for Geometry Nodes.]
* [[User:HimanshiKalra/GSoC21/log|Daily Log]]

== Patches ==
* [https://developer.blender.org/D11611 D11611 Regression Testing: Running tests based on blend files]
* [https://developer.blender.org/D12137 D12137 Tests: Custom Data comparison for generic attributes]
* [https://developer.blender.org/D12149 D12149 Refactor: Move comparison of non-generic attributes]
* [https://developer.blender.org/D12192 D12192 Add cutom data color property]
* [https://developer.blender.org/D12250 More test cases] (yet to be merged)

== Completed Work ==
Extended the framework to directly test blend files without using Blender API for adding tests. Extended the comparison function to include comparison for generic custom data.

Added test for the following nodes:

Curve Primitives

Mesh Primitives

Attributes
*  attribute clamp
*  attribute color ramp
*  attribute curve map
*  attribute map range
*  attribute mix
*  attribute proximity
*  attribute randomize
*  attribute sample texture
*  attribute vector math

Curves
*  curve endpoints
*  curve length
*  curve resample
*  curve reverse
*  curve reverse attributes
*  curve subdivide
*  curve to mesh
*  curve to points
*  curve trim
*  mesh to curve

Geometry
*  bounding box
*  convex hull
*  delete geometry
*  join geometry

Mesh
*  boolean diff
*  boolean intersect
*  boolean union
*  edge split
*  subdivide
*  subdivision surface
*  triangulate

Points
*  point distribute
*  point instance

Utilities
*  math add
*  switch

Volumes
*  volume to mesh

== Acknowledgements ==
Lots of thanks to my mentors - Habib Gahbiche and Jacques Lucke and blender community. They helped me out whenever I was stuck. Had a hard-working summer :)

== Future Work ==
Hoping tests gets more firmly included in the workflow. Merging a new node in master requires blend test file whenever possible. As for the project itself, more tests can always be added.