All links for GSoC 2021

'''Project''': This year I will be working on Regression Testing of Geometry Nodes.

'''Mentors''': Jacques Lucke, Habib Gahbiche

* [[User:HimanshiKalra/GSoC21/Proposal|Proposal]]
* [[User:HimanshiKalra/GSoC21/log|Daily Log]]
* [https://devtalk.blender.org/t/gsoc-2021-regression-testing-of-geometry-node-weekly-reports Weekly Reports]
* [https://wiki.blender.org/wiki/User:HimanshiKalra/GSoC21/GeometryNodeTests Adding test for Geometry Nodes.]
* [[User:HimanshiKalra/GSoC21/FinalReport|Final Report]]