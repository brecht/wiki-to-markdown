Sculpt mode uses a coarse BVH tree, `PBVH`.  The leaves are fairly large compared to most BVHs; this is because the leaves are used to split the GPU mesh.

= Data modes =

PBVH has three different modes, each of which has a different data backing:
# PBVH_FACES - Normal [[/Source/Objects/Mesh|Mesh]] data.
# PBVH_BMESH - Dynamic topology, uses [[Source/Modeling/BMesh/Design|BMesh]].
# PBVH_GRIDS - Multiresolution data.

You get can the mode with `BKE_pbvh_type`.

= Vertex/Face references =

The PBVH API stores vertex references in a special structure, `PBVHVertRef`.  It contains a single `intptr_t` value, the meaning of which varies by PBVH data mode.  `PBVH_BMESH` stores pointers in the value, while the other two store indices.  The null reference is PBVH_REF_NONE.

= Vertex Iteration =

You can iterate over the vertices in a `PBVHNode`.  There are two modes for this, `PBVH_ITER_UNIQUE` and `PBVH_ITER_ALL.`  `PBVH_ITER_UNIQUE` iterates over the vertices uniquely assigned to this node, while `PBVH_ITER_ALL` iterates over the vertices that belong to this node's faces.

<pre>
PBVHVertIter vd;
BKE_pbvh_vertex_iter_begin(pbvh, node, vd, PBVH_ITER_UNIQUE) {
  //PBVHVertIter has various bits of data:
  //vd.co    - position
  //vd.no    - normal
  //vd.mask  - pointer to mask
  //vd.color - active color attribute data, if it exists
}
BKE_pbvh_vertex_iter_end;
</pre>