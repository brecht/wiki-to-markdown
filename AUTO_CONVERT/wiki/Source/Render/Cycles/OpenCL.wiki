=OpenCL Status=

OpenCL support was removed in Blender 3.0.

Instead there are [https://developer.blender.org/T91571 HIP] and [https://developer.blender.org/T92212 Metal] backends.