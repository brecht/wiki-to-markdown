== Glossary ==
'''Node''': A compositor node in blender node system. Compositor execution system translates all nodes into operations. A node can be translated into 1 or more operations.

'''Operation''': A functional node in compositor execution system that contains an image manipulation algorithm. 

'''Registered reads''': Number of operations that read from an operation. This is equivalent to the number of links its output socket has. Calculated prior rendering execution.

'''Received reads''': Number of operations that have finished reading from an operation during rendering execution. When count reaches registered reads the operation buffer can be disposed.

'''Registered areas''': Operation areas that will be rendered during rendering execution.

'''Render operation''': Execute operation algorithm saving result in a buffer. Input operations must be rendered too.

'''Area of interest''': Input area that an operation needs to read in order to render its own area. An operation may have different areas of interest for each input.

'''Active/shared operations buffers''': Current operations buffers available. Only currently running operations and their inputs buffers are kept alive.

'''Execution model''': System implementation that defines how operations are rendered. Currently there are two models:
* '''Tiled''': Operations are executed from outputs to inputs grouped in execution groups and rendered in tiles. Only execution groups are buffered.
* '''FullFrame''': Operations are fully rendered in order from inputs to outputs. All operations are buffered.

'''Resolution''': Operation canvas size.

'''Constant operation''': An operation with constant pixel values. That is all pixels have same values.

'''Set operation''': An operation which single pixel value is set prior rendering execution. Still operation may have any resolution, being a constant operation too.

'''Single element buffer''': A buffer with virtually any resolution size (width/height) but only one element in memory. When looping same element will be returned for all coordinates. It's used to save memory on constant operations.