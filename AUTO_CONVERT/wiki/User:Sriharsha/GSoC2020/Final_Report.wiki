'''Google Summer of Code 2020'''

= Liquid Simulation Display Options - Final Report =

This summer, I worked on providing visualization options for liquid simulation grids and improving fluid simulation display options on the whole in Workbench. 

== Links ==

* Git branch used for this project: [https://developer.blender.org/diffusion/B/browse/soc-2020-fluid-tools/ '''`soc-2020-fluid-tools`''']
* All the changes made in the branch `soc-2020-fluid-tools` are included in this patch for review: [https://developer.blender.org/D8705 '''`D8705`''']
* Master Commit: [https://developer.blender.org/rBf137022f9919f4dd315ec6b325a08e1bf5aec6fb '''`f137022f9919`''']
* [https://devtalk.blender.org/t/gsoc-2020-liquid-simulation-display-options-weekly-reports/13524 Weekly Reports Devtalk thread]
* [https://devtalk.blender.org/t/gsoc-2020-liquid-simulation-display-options-feedback-suggestions/13719 Feedback & Suggestions Devtalk thread]
* [[User:Sriharsha/GSoC2020/Proposal|Project Proposal]]

== Work Done ==

Implemented and added the following to the viewport display options for fluids:

=== Viewport Display ===

* Raw voxel display or ''closest ([https://en.wikipedia.org/wiki/Nearest-neighbor_interpolation nearest-neighbor])'' interpolation based on the centers of the grid cells for displaying the underlying voxel data of the simulation grids more clearly.
* An option to display gridlines when the slicing method is ''single''.

==== Grid Display ====

* Visualization for flags, pressure and level-set representation grids with a fixed color coding based on [http://mantaflow.com/gui.html Mantaflow's integrated GUI].
** Flags (`mFlags`)
*** [[File:Final Fluid Flags.png|thumb|center|Visualization of a 2D slice of the ''flags'' grid in a liquid domain. (''Display Thickness'' used: '''0.03''') ]]
*** The ''flags'' grid is an integer grid which stores the information about the state (or ''contents'') of the underlying simulation grid cells. The color coding used for the visualization of ''flags'' is as follows (''Cell Type -> Color''):
**** Fluid    -> Blue
**** Obstacle -> Gray
**** Empty    -> Purple
**** Inflow   -> Green
**** Outflow  -> Orange
*** When more ''cell types'' coexist in a cell, the specified colors get added up to get the resultant color.
** Level-set Representation Grids
*** [[File:Fluid Levelset Representation.png|thumb|center|Visualization of a 2D slice of the fluid level-set representation in a liquid simulation. (''Display Thickness'' used: '''0.02''')]]
*** Visualization options for the grids `mPhi`, `mPhiIn`, `mPhiOutIn` and `mPhiObsIn` which contain the level-set representations for the ''liquid'', ''inflow'', ''outflow'' and ''obstacles'' respectively in the liquid simulation.

==== Vector Display ====

* Made vector display options available for visualizing external forces of the fluid domain.
* '''''M'''arker '''A'''nd '''C'''ell'' grid visualization options for vector fields like velocity or external forces.
** [[File:Final Fluid MAC Vectors.png|thumb|center|''MAC Grid'' visualization for a 2D slice of the velocity vector field in a liquid simulation.]]
* A toggle for scaling the display vectors (''needles'' and ''streamlines'' only) with magnitude of the actual vectors.

==== Coloring Options for ''Gridlines'' ====

* Range highlighting and cell filtering options for displaying the simulation grid data more precisely.
** The cells with values of the color mapped field within the user-specified range are highlighted with a user-specified color on the grid lines to display the grid values with better precision. These highlighted cells, filtered by a user-specified range, can be further filtered based on the cell type. (''Only usable when Grid Display is enabled.'')
* An option for coloring ''gridlines'' with ''flags''.

Made various minor improvements and options for better user control over the visualization.
I hope these changes helped the fluid viewport display to be more informative and usable.

== Acknowledgements ==

Many thanks to my mentors Sebastián Barschkis, for the invaluable guidance throughout the project and Jacques Lucke, for mending my ''ways'' of coding! Thanks to Ray Molenkamp (LazyDodo) for supporting by providing the builds throughout.
Thanks to the Blender community and Google Summer of Code for introducing development and collaboration to me on a larger scale. It has been an extraordinary learning experience. Thanks.