== About Me ==
I am Howard Trickey, from Westfield, New Jersey, USA. I have been a volunteer developer for Blender since May 2011, when I started helping get the switch to BMesh over the finish line. Since then, I have mainly worked on the Knife Tool, the Bevel tool and modifier, and lately, the Boolean tool and modifier.

== Design Documents ==

[[User:Howardt/Bevel|Bevel]]

[[User:Howardt/Boolean|Boolean]]

== [[User:Howardt/Reports|Reports]] ==