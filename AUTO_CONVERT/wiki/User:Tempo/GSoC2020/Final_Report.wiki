= Custom Menus Final Report =

Over those 3 last month, I worked on implementing a '''user menus editor''' for the users to customize their blender experiences and work more efficiently.
Most of the features of the [[User:Tempo/GSoC2020/Proposal|proposal]] has been implemented as other has been discarded (see details).

By the end of the [[GSoC|Google Summer of Code]] a basic '''menu editor''' has been created, while the old system has been kept at the user demand.

== Result ==

The '''menus editor''' is located in  <code>Edit > Preferences > User Menu</code>

[[File:Preferences location.png|300px|Preferences location]]
[[File:My menu list none.png|440px|the user menus editor]]

Here you can customize, add and remove your own menu.

=== Operation ===

The '''User Menus Group system''' differ from the old '''Quick Favorites system''' by adding a level in the hierarchy.

The old '''Quick Favorite system''' was a menu named ''Quick Favorites'' composed of different menus (one per combination of context and spacetype), themselves composed of items added by the user.
The new '''User Menus Group system''' is like a list of multiples ''Quick Favorites'' menus, but with more customizables data about the ''root menu''.

A '''User Menus Group''' contain different '''User Menus''' which contain different '''User Items'''.

[[File:My menu list display.png|200px|right|thumb|list type]]
[[File:My menu pie display.png|200px|right|thumb|pie type]]
==== <u>User Menus Group</u> ====

Each '''User Menus Group''' contain a ''display name'', an ''unique name'', a ''type'' (list or pie), and a list of '''User Menus''' (one per context and spacetype).

* The ''display name'' is the name the user give to the menus group
* The ''unique name'' (or ID name) is automatically generated depending of its ''display name'' and is updated when the ''display name'' (chosen by the user) is changed. The ''unique name'' is an identifier and is used to map the menus group to a keymap.
* the ''type'' can be chosen between pie menu and list menu, changing the display and the usage of the menu group
* the list of ''User Menus'', since each operator, property, ... differ from the context and the spacetype.

'''Note :''' The editor change depending on the type of the '''User Menus Group'''.

==== <u>User Menu</u> ====

An '''User Menu''' contain its ''context'' and its ''spacetype'', as well as the items linked to it.
* The ''context'' is the edition mode in the 3D view (Object Mode, Edit Mode, ...)
* The ''spacetype'' is the type of the area (View 3D, Node, Image, ...)

==== <u>User Items</u> ====

The '''User Item''' is an item of the menu, it is composed of a ''name'', an ''icon'', a ''type'' and the content of its type.

The different types are :
* '''Operator''' : execute an action, can contains properties.
* '''Property''' : display and edit a property.
* '''Menu''' : a sub menu that can be edited by the user to be more organized and structured.
* '''Packed Menu''' : a static menu, defined by an identifier.
* '''Separator''' : a separator (or nothing in the case of a pie menu).

==== <u>key mapping</u> ====
[[File:Key mapping user menu.png|300px|thumb|key mapping]]
Initially, key mapping was setup automatically as a new '''User Menus Group''' was created, but it had be decided to discard the feature as creating key items without the user decision, can be annoying, and cause bugs.

Never the less, the key item, once linked to a menu through its identifier via the [https://docs.blender.org/manual/en/latest/editors/preferences/keymap.html keymap editor], can be edited directly in the '''User Menus Editor''' so the user can quickly know how to use their menu and modify it.
If no keymap is set, it will not be possible to modify the key item (since it doesn't exist) and the edition option will be hiden, as well as the the message "''no key set''" will be displayed.

==== <u>Compatibility</u> ====

Since it been asked to keep the old system working, the first '''User Menus Group''' is the "''Quick Favorites''" menu. Its name and type can't be modified and the group can't be removed. Its content can be modified, from the editor and from the right click (old system) too.

=== Initial Proposal ===

Most parts of the initial mockup have been created, while some of them have been discarded.

[[File:Mockup_Custom_Menu_new_preference.png|500px|initial mockup]]
[[File:My menu list operator.png|500px|final result]]

==== Achieved elements ====

* you can add, remove and rename ''User Menus Group''
* change context and spacetype (change ''User menu'').
* change type from list to pie (and change the editor depending on it).
* add, remove and reorders items in a menu.
* edit the items :
** choose icon
** change name
** change type
** change item specific parameters (identifier, properties, ...).
* create and edit submenu
* keymap '''User Menu Group'''

==== Discarded elements ====

* '''macro : ''' an item which would execute multiples operator at once
** it was not originally implemented and it was not considered as important enough (too much work to be worth the implementation in the GSoC context + need feedback from other users to know how wanted is the features)
* '''import/export : ''' import and export menu between user to use premade config files.
* '''add/remove from right click :''' for all the '''User Menus Group'''
* '''User Area : ''' an area displaying an '''User Menu''' so the user can create his own custom area
** this feature has been implemented but was removed mid-way as it had been decided that a whole area was to much and a panel was enough.

== To do ==

=== News Features ===

==== From GSoC ====

* except '''macro''' and '''User Area''', all discarded elements can e implemented
* Create the User Panel

==== From Users suggestions ====

* User TopBar/SideBar (most asked)
* Reorder addons bar
* stack key items

=== Improvement ===

* change icon selection to ''search menu''.
* make right and left part of the editor scrolling independently.
* make operator's properties on one column instead of two (not enough space for two columns).
* fix light jump to the left when adding an item to a new menu.

== Links ==

* [[User:Tempo/GSoC2020/Proposal|Proposal]]
* [https://devtalk.blender.org/t/gsoc-2020-custom-menus-weekly-reports/13721 Weekly Reports]
* [https://devtalk.blender.org/t/gsoc-2020-idea-to-improve-the-custom-menu-idea/12275/10 Discussion]
* [[User:Tempo/GSoC2020/Documentation|User Documentation]]
* [https://summerofcode.withgoogle.com/projects/#6635647869124608 GSoC' page]
* Initial thread : [https://developer.blender.org/T73178 T73178]