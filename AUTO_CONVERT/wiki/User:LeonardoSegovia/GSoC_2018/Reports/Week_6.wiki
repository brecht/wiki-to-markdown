= Week 6 =

Hi everyone! This is my report for the present week.

== Objectives ==

We're still on Weeks 4-5:

* Test shader functionality. If working, release test build.
* Evaluate received feedback. Start bug fixes.

bzztploink and Lukas released the relevant builds on Monday.

I was stuck this weekend because of university constraints; however, today was the end of the Digital Image Processing course, so I should have more free time to devote to the project.

== What's been done ==

This week, I implemented the agreed cleanup and organization changes. These were breaking changes in some instances, outlined below.

* Removed Lukas's debug &quot;physical&quot; parametrization and reordered the rest ([https://projects.blender.org/blender/blender/commit/20ed03722fad0954d961aeaa0a3b0b5df548afb7  rB20ed0372], [https://projects.blender.org/blender/blender/commit/32ec7d77d7ebd236bd1f724ab735415f2d97389d  rB32ec7d77]).
** Parametrizations should now be in order of usability: Direct coloring (plug and play), Melanin concentration (for realistic results), and Absorption coefficient (if you have a paper, you can enter its values here).
** This change means that existing <code>.blend</code>s need to be adjusted.
* Reordered and renamed sockets according to most useful to least ([https://projects.blender.org/blender/blender/commit/45abb2eee8030e1058182d30f1a6f232bbed8ab1  rB45abb2ee], [https://projects.blender.org/blender/blender/commit/06fb2a06738ccf0e9d90d41906ab18ea37bdb6f1  rB06fb2a06]).
* Refactors:
** Randomization into <code>factor +/- the given percentage</code> ([https://projects.blender.org/blender/blender/commit/c36722f96287b226f02783925f52bb413c6b3c0c  rBc36722f9]).
*** It is expected that some <code>.blend</code>s need to be adjusted.
** Melanin into Tungsten's quantity and redness ratio ([https://projects.blender.org/blender/blender/commit/f91635e64e50eaa50bd911af3f93da881eacf4d0  rBf91635e6], [https://projects.blender.org/blender/blender/commit/b005c15f6734ca20816d4820394f09667aaa853b  rBb005c15f]).
* Automagically get the RNG info even if the user hasn't socketed a Hair Info -&gt; Random node ([https://projects.blender.org/blender/blender/commit/23d92659c7e9a603f40e121f671ca35b101a1831  rB23d92659]).
** After this change, the only way to disable randomization is to ''socket a Value node'' and set it to 0.5 (just setting the value will be ignored), or set Randomization to 0.
* Remove two possible sources of NaNs I and nirved experienced during debugging ([https://projects.blender.org/blender/blender/commit/c59ed14619e7b2e3bad075ec6e04edf0c7fc70ec  rBc59ed146]).

== Next up ==

I don't expect to spend more time on coding (except for the reason below). During the weekend and from now on, I'll begin porting the existing hair tests to the new shader. I reiterate the previous request for .blend hairstyles for this purpose -- it'll be great if you could post them to the BA thread!

== Questions ==

In the BA thread, jemain said:

<blockquote>I don’t know why but in real size (40 to 100 microns) the hair is melted into a mass without details like molten resin. I have to put them at 300 micron to hope to have details.
</blockquote>
I understand that this is a near-field shader, i.e. it should work fine at that level of resolution. I'm not well versed in the innards of Cycles, so I'd like to ask the community:

* Firstly, how can one set the measurement units so as to achieve real-sized hair?
* What could be possible sources of the lack of definition the poster mentioned? Could it mean a problem with the path tracer? Or is it a symptom of noise caused by our shader?