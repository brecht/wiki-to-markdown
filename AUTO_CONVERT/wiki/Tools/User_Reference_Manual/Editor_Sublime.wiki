= Sublime =

Setup and Workflow for the Blender Manual & Sublime text editor.

The setup works with either Sublime Text 2 or 3.

Preferences > Settings - User:
<pre>
{
    "spell_check": true,
    "dictionary": "Packages/Language - English/en_US.dic",

    "rulers": [120],
    "tab_size": 3,
    "translate_tabs_to_spaces": true,
    "show_encoding": true,
}
</pre>

== InstantRST (optional) ==
For installing the instant visualization and completion plugins, first follow:
https://sublime.wbond.net/installation

After, you can install packages in Preferences > Package Control > install, and search for:
*'Restructured Text (RST) Snippets' - for autocompletion, see how to make the most of it [https://github.com/mgaitan/sublime-rst-completion here], with video available.
*'OmniMarkupPreviewer' - for previewing the result in the browser with Ctrl+Alt+O