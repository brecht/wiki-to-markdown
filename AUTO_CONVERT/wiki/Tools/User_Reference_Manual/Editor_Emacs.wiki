= Editing reStructuredText in Emacs =

This page contains additions you can make to your <tt>emacs.d/init.el</tt>

<source lang=lisp>
(add-hook
  'rst-mode-hook
  (lambda ()
    (setq-local fill-column 120)
    (setq-local indent-tabs-mode nil)
    (setq-local tab-width 3)
    (setq-local evil-shift-width 3)

    (add-to-list 'write-file-functions 'delete-trailing-whitespace)

    ;; package: find-file-in-project
    (setq-local ffip-patterns '("*.rst" "*.py"))
    )
  )
</source>