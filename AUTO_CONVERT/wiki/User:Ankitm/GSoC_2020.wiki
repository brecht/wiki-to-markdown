
* [[/Proposal_IO_Perf | Proposal]] 
* [[/Daily_Reports | Daily]] Reports, [[ /Weekly_Reports | Weekly]] Reports, Project Status Tracker & design discussions: [http://developer.blender.org/T68936 #68936]
* IO Benchmarks
** [[ /gperftools_installation_on_mac | gperftools installation and use]]
**  [[/Final_report#Comparisons|Comparisons]]: Currently links to final report's comparisons section. Older timings can be found in Weekly Reports too.
* [https://devtalk.blender.org/t/gsoc-2020-faster-io-for-obj-stl-ply/13528 Devtalk thread ]for community feedback & testing
* Branch on diffusion: [https://developer.blender.org/diffusion/B/browse/soc-2020-io-performance/source/blender/io/wavefront_obj/ soc-2020-io-performance]
* [[/Final_report|Final Report]]