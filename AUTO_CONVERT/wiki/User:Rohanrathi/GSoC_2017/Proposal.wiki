= GSoC 2017: Normal Editing Tools =

== Name  ==

Rohan Rathi

=== Email / IRC  ===

; IRC: RohanRathi

=== Synopsis  ===

Several studio quality games have been developed with custom edited normals and the importance of normal editing tools is growing since the past few years. 
Normals in Blender are currently only editable through modifiers and there is no way to directly edit and control normals of a mesh. I wish to provide creative as well as editing freedom to artists and uncap the potential for game development in blender with the improvements I propose.
I plan to add a Normals Edit mode to manipulate normals with tools to supplement this mode as well as add operations in edit and object mode. The workflow will be maintained to be simple and user friendly.


=== Benefits  ===

Editable normals can improve visual quality of a mesh without altering its geometry, this is especially useful for game development. The toolset will allow users to alter vertex normals in blender and simplify the workflow which will in turn benefit the blender community in game development. The users will be provided with a consistent and concrete method to edit normals, hence much more artists will be able to utilise blender to complete the workflow and eliminate the need of scripts as well as exporting to other software.
I will be documenting all existing code related to normals as well as compile my own documentation to help developers understand the code and artists
familiarise themselves with the features.

=== Deliverables  ===
I propose to add a normals editing mode in the edit mode itself which allows manipulation of vertex normals based on vertices, edges or faces selected.
* Introduce a vertex normal editing mode within edit mode with the following functionality:
** Allow users to manually rotate normals
** Split/merge normals
** Average normals
** Point normals to target
** Align all normals
* Add several types of weighted vertex normals
* Ability to copy normals from vertex, edge or face
* Ability to harden/soften all normals of object


=== Project Details  ===

==== Normal Rotation ====
Similar to modal rotation in edit mode, the user can directly rotate or set a single or a set of normals to a specific coordinate relative to each vertex by selecting the vertex. The rotation can also be restricted to either of 3 axes. 

==== Split/ Merge Normals ====
Split or merge all custom normals of a vertex. Split works by separating and reordering normals relative to face normal or corner angle. Merge works by joining the set of normals in the given vertex. Merging works on the basis of the weighting mode selected. This mode will work by selecting vertices, edges or faces and split or merge the vertices highlighted.

==== Average Normals ====
Calculate the average of split normals of each selected vertex and set the direction of the split normals to this average. This tool is useful to provide smooth shading while providing weight to the orientation of split normals of vertex instead of face normals. 

==== Point normals to target ====
This works by selecting all the vertices that you want to edit. Then select the target location to direct these normals. Target location can be object origin, cursor or specified vertex or face. Then the normals will point to target. A separate check will specify whether to point towards or away from target.

Align normals with face

This works by selecting all the faces you wish to edit. It then aligns the vertex normals of each individual face to its respective face normal. If multiple adjacent faces to a vertex are selected, the normals are aligned according to the weighting mode specified. An additional feature may be added in the future to align only split normals of the face with the face normal.

==== Align normals ====
Align all normals of the object. Modes available are:
# Align to origin Align all normals to object origin.
# Align to cursor: Set cursor to the desired location. This feature then aligns all normals of the object with the cursor location.
# Align to direction: Works by pointing the normals in a fashion similar to the hands of a clock. This tool sets all normals to face a direction in the plane of the viewport camera. This feature can be used to point normals vertically upwards, downwards or any other direction in the given plane. 

==== Weighted Vertex Normal ====
Add new normal weighting modes to allow calculation of normals on the given preset.
# Average weighted: normals are calculated by the average of the sum of adjacent face normals
# Face weighted: normals are calculated based on area of adjacent faces
# Corner angle: normals are calculated based on corner angle of shared vertex in each face

==== Copy normals ====
* Copy orientation of active vertex normal and apply that orientation to all selected vertices.
* Copy normals of a active edge and apply it to all selected edges
* Copy face normal of a active face and apply it to all vertex normals of selected faces

==== Preset to harden or soften normals ====
A slider moving from hard to soft to modify all (or a set of) vertex normals of the object which will move the normals closer to the face normal average with each increment, making the shading appear somewhat smoother. Similar to smoothing angle in autosmooth, this preset will instead rotate the normals based on a percentage of the delta. The preset values will vary from 0-100.


=== Project Schedule  ===

I will use the community bonding period to further discuss the implementation of the added functionality to better serve artists workflow. I will study the code responsible for normals editing to understand the physical implementation of the structures of how normals are stored and think upon how to rework this to support my added functionality. I will also use this period to provide the base for normals editing mode and discuss with my mentor the plans I will have devised. I have my papers from the mid till end of May and will strive to handle all concepts during exams as well. 

When coding begins, my exams will have ended and I will work on the normals edit mode and aim to add nearly all its described functionality. My prior focus will be to add the ability to rotate, point and align normals. After which I will work on the split/merge feature and averaging normals. I plan to complete this by end of june and prepare for midterm evaluation. After evaluation I will move to add the later described functionality that is weighted vertex normals, copy normals and harden or soften all normals of the object; I will triage weighted vertex normals as high priority and plan on adding weighted normals and the ability to copy normals by the third week of july and then work on harden/soften normals. Alongside this, I will be displaying all the features I will have implemented and their cohesion and take input from the community. My college will start early August and I will wrap up most of the work by the first week of august to give me time to refine the features.
I will spend the last 2 weeks to debug any errors, clean up the code, document my work and prepare for final evaluation.

=== Bio  ===

I am a 19 years old student enrolled in Delhi Technological University studying Computer Science and Engineering, currently in my 2nd year of college. I have been a blender user since 5 years with modelling and designing being my core interest; hence I use the toolset frequently and understand its strengths and flaws. I have been programming in C/C++ for
the past 6 years and can easily implement algorithms. I first realised that blender takes part in GSoC last year and have been very interested since, as this brings out the best of both worlds. I have been reading and understanding the source code of blender for the past few months and feel I have established a strong base for development in blender. Although it may seem trivial, I implemented a patch (https://developer.blender.org/D2568) which shows that I can compile and build blender. I have a much larger vision for developing in blender and will work on more projects as well as current projects after GSoC. I’m particularly interested in improving snapping and work on adding manual snapping modes in blender later on.

=== Links ===

http://polycount.com/discussion/154664/a-short-explanation-about-custom-vertex-normals-tutorial

https://wiki.blender.org/index.php/User:Mont29/Foundation/Split_Vertex_Normals

https://mont29.wordpress.com/2014/08/01/custom-split-normals-first-testbuild-available/

https://www.blend4web.com/en/community/article/131/

http://polycount.com/discussion/85809/face-weighted-normals