= Blender 3.0: Animation & Rigging =

* New keying set: '''Location, Rotation, Scale, and Custom Properties''' ([https://projects.blender.org/blender/blender/commit/7192ab614b49  rB7192ab61]). Effectively, it combines the behaviour of ''Whole Character (selected bones only)'' (which keys loc/rot/scale/customprops, but only works in pose mode) with ''Location, Rotation, and Scale'' (which works in both object and pose mode, but doesn't key custom properties). This makes it possible to use one keying set for animating both characters and props.
* Ctrl+F in animation editors no longer blocks the UI with a popup, but simply shows & activates the channel search textbox ([https://projects.blender.org/blender/blender/commit/41a4c62c3155  rB41a4c62c]).
* FCurve and NLA '''modifier properties can now be overridden''' ([https://projects.blender.org/blender/blender/commit/d2311de21853  rBd2311de2]). Previously an armature/object with overrides could get new FCurve/NLA modifiers, but the properties would be read-only. Now they can be edited.
* Keyframe removal (Default: Alt+I, Industry Compatible: Alt+S) now respects the active keying set ([https://projects.blender.org/blender/blender/commit/7fc220517f87  rB7fc22051], [https://projects.blender.org/blender/blender/commit/1364f1e35c20  rB1364f1e3]).
* Custom bone shapes now have full translation/rotation/scale options ([https://projects.blender.org/blender/blender/commit/fc5bf09fd88c  rBfc5bf09f]).
[[File:release-notes-300-animation-rigging-custom-bone-transform.png|thumb|300px|center]]
* The Asset Browser now supports rendering previews for Action datablocks ([https://projects.blender.org/blender/blender/commit/17534e28ff44  rB17534e28]). This is documented further in the [[Reference/Release_Notes/3.0/Asset_Browser|3.0 Asset Browser]] release notes.
* FCurves and all their keys can be selected by box- or circle-selecting the curve itself. ([https://projects.blender.org/blender/blender/commit/2246d456aa84  rB2246d456])
** Box selecting a curve selects all the keyframes of the curve.
** Ctrl + box selecting of the curve deselects all the keyframes of the curve.
** Shift + box selecting of the curve extends the keyframe selection, adding all the keyframes of the curves that were just selected to the selection.
** In all cases, if the selection area contains a key, nothing is performed on the curves themselves (the action only impacts the selected keys).
[[File:release-notes-300-animation-rigging-fcurve-select.mp4|thumb|1280px|center]]
* Make Single User: in addition to '''object''' animation, now '''object data''' (mesh, curve, ...) animation can be made single user as well. ([https://projects.blender.org/blender/blender/commit/d0c5c67e940b  rBd0c5c67e])
* FCurve modifiers are now correctly evaluated in Restrict Range Borders ([https://projects.blender.org/blender/blender/commit/9dee0a10c81d  rB9dee0a10]).
** When using FModifier Restrict Frame Range, the resulting influence was zero being exactly on Start / End range borders (so borders were exclusive). This made it impossible to chain FModifers together (forcing the user to specify values slightly below the desired border in following FModifiers). This is now corrected to be inclusive on Start / End range borders.
<gallery mode="packed" widths=400px heights=200px perrow=2>
File:Before.png|FCurve modifiers were evaluated wrong on restrict range borders
After.png|FCurve modifiers are now evaluated correctly
</gallery>

== Motion Paths ==

New operator: '''Update All Visible Motion Paths''' ([https://projects.blender.org/blender/blender/commit/4de0e2e7717f 4de0e2e771]). Besides having this handy new button, there is also a smaller, related improvement that updating the motion paths for an armature now updates all the bones motion paths simultaneously. 

[[File:release-notes-300-animation-motion-paths-refresh-all.png|thumb|264px|center]]

== Vertex Groups ==

* The vertex group names are now stored in meshes directly instead of objects, which causes various compatibility issues with files from previous versions. See the [[Reference/Release_Notes/3.0/Core#Compatibility|compatibility section]] in the core release notes for more details. ([https://projects.blender.org/blender/blender/commit/3b6ee8cee708  rB3b6ee8ce], [https://projects.blender.org/blender/blender/commit/fc32567cdaa5  rBfc32567c])

== Pose Library ==

A new Pose Library has been added to Blender ([https://projects.blender.org/blender/blender/commit/9473c61b366 9473c61b36], [https://projects.blender.org/blender/blender/commit/28dc07a153d 28dc07a153], [https://projects.blender.org/blender/blender/commit/f3610a23d12 f3610a23d1], [https://projects.blender.org/blender/blender/commit/e01b52100f e01b52100f]). It is based on the [[Reference/Release_Notes/3.0/Asset_Browser|Asset Browser]]. For demo videos & a thorough explanation, see the [https://code.blender.org/2021/05/pose-library-v2-0/ Pose Library v2.0] blog post. The pose library is partially implemented as add-on (enabled by default; [https://projects.blender.org/blender/blender/commit/34771cc9f56 34771cc9f5]) such that studios can create their own alternative implementations.

=== Converting poses from the old to the new pose library ===

[[File:release-notes-300-animation-pose-library-convert.png|thumb|301px|center]]

To convert an old-style pose library to a new one, follow these steps:

# Make sure the active object is the character rig.
# In the Action Editor, select the pose library Action.
# In the sidebar (press N if not visible), choose "Convert Old-Style Pose Library".

This will create an Action asset for each pose in the library.

== Bendy Bones ==

[[File:release-notes-300-animation-rigging-bbone-scale.png|thumb|301px|center]]

* Renamed confusingly named Curve Y and Scale Y channels to Z. Animation curves and drivers are automatically updated, but Python scripts have to be changed manually. ([https://projects.blender.org/blender/blender/commit/682a74e0909  rB682a74e0])
* Added actual Scale Y channels that produce non-uniform segment lengths. ([https://projects.blender.org/blender/blender/commit/638c16f4101  rB638c16f4]).
* Added simple toggles that replace up to 6-8 trivial drivers copying handle bone local scale to the corresponding properties. ([https://projects.blender.org/blender/blender/commit/b6030711a24  rBb6030711])

{|class="note"
|-
|<div class="note_title">'''Forward Incompatibility Breakage'''</div>
<div class="note_content">Blend files saved in Blender 3.0 with these features can cause unwanted behavior in older versions of Blender, see [http://developer.blender.org/T89621 #89621].</div>
|}


== Constraints ==

* New "Local Space (Owner Orientation)" choice for Target Space of bone targets, which allows copying local transformation while adjusting for the difference in rest pose orientations aiming to produce the same global motion. ([https://projects.blender.org/blender/blender/commit/5a693ce9e3  rB5a693ce9])

  [[File:Demo-owner-local.mp4|600px]]

* Constraints now have Apply Constraint, Duplicate Constraint, and Copy To Selected operators in an "extras" menu, similar to modifiers ([https://projects.blender.org/blender/blender/commit/d6891d9bee2  rBd6891d9b]).

  [[File:Release Notes-Blender-3.0-Constraints-extras-menu.png|347px]]

=== Limit Rotation ===

* The constraint now correctly removes shear before processing, and without any limits can be used for that explicit purpose. ([https://projects.blender.org/blender/blender/commit/edaaa2afddb2  rBedaaa2af])
* Added an Euler Order option similar to Copy Rotation. ([https://projects.blender.org/blender/blender/commit/d2dc452333a4  rBd2dc4523])

=== Copy Transforms ===

* New Remove Target Shear option for removing shear from the result of the Target Space transformation. ([https://projects.blender.org/blender/blender/commit/bc8ae58727  rBbc8ae587])
* More Mix mode options representing different ways to handle scale and location, resulting in a complete set of six "Before/After Original (Full/Aligned/Split Channels)" choices in addition to Replace. ([https://projects.blender.org/blender/blender/commit/bc8ae58727  rBbc8ae587])

  [[File:Demo-mix-full-aligned-split1.mp4|600px]]

=== Action ===

* More Mix mode options to complete the same set of six "Before/After Original (Full/Aligned/Split Channels)" choices as in Copy Transforms. ([https://projects.blender.org/blender/blender/commit/cf10eb54cc  rBcf10eb54])
* For constraints on ''bones'', the default Mix mode is now "Before Original (Split Channels)". ([https://projects.blender.org/blender/blender/commit/cf10eb54cc  rBcf10eb54])

=== Stretch To ===

* The default Rotation Type for newly created constraints has been changed to Swing, which was introduced in 2.82 to replicate the behavior of the common Damped Track + Stretch To combination using just the Stretch To constraint. ([https://projects.blender.org/blender/blender/commit/8da23fd5aaa  rB8da23fd5])

== Pose Sliding / In-Betweens Tools ==

[[File:release-notes-300-animation-rigging-pose-sliding-tools.mp4|thumb|1280px|center]]

These "In Betweens" tools have been improved ([https://projects.blender.org/blender/blender/commit/9797b95f6175  rB9797b95f]):
* Push Pose from Rest Pose
* Relax Pose to Rest Pose
* Push Pose from Breakdown
* Relax Pose to Breakdown
* Pose Breakdowner

These all now use the same new sliding tool:
* Actual visual indication of the blending/pushing percentage applied.
* Mouse wrapping to allow for extrapolation without having to worry about the initial placement of the mouse. This also means these tools are actually usable when chosen from the menu.
* Precision mode by holding Shift.
* Snapping to 10% increments by holding Ctrl.
* Overshoot protection; by default the tool doesn't allow overshoot (lower than 0% or higher than 100%), and it can be enabled by pressing the E key.
* Bones are hidden while sliding, so the pose itself can be seen more clearly. This can be toggled by pressing the H key while using the tool.


New Pose Sliding Operator - Blend To Neighbour ([https://projects.blender.org/blender/blender/commit/f7a492d46054 f7a492d460]):
* Nudge the current pose to either the left or the right pose.
* Useful for dragging parts in an inbetween without losing your current pose.
* Found in Pose Mode under <span class="literal">Pose</span>&nbsp;» <span class="literal">In-Betweens</span>&nbsp;» <span class="literal">Blend To Neighbour</span>.
* Also available with the <span class="hotkeybg"><span class="hotkey">Alt</span><span class="hotkey">⇧ Shift</span><span class="hotkey">E</span></span> shortcut.
[[File:Blend to neighbour 2.mp4|thumb|1280px|center]]