= Blender 3.0: Cycles =

== GPU Rendering ==

GPU kernels and scheduling have been rewritten for better performance, with rendering between 2x and 8x faster in real-world scenes.

=== NVIDIA ===

The hardware minimum requirements have remained the same, however NVIDIA driver must be version 470 or newer now.

OptiX has the following improvements:
* Rendering of hair as 3D curves (instead of ribbons) is significantly faster, by using the native OptiX curve support
* Baking now supports hardware ray-tracing
* Runtime kernel compilation time was significantly reduced, in many scenes it is less than a second now

[[File:Cycles_3.0_optix_render_time.png|800px|Render time with an NVIDIA Quadro RTX A6000 and OptiX|thumb|center]]
[[File:Cycles_3.0_benchmark_images.png|800px|thumb|center]]

=== AMD ===

For AMD GPUs, there is a new backend based on the [https://github.com/ROCm-Developer-Tools/HIP HIP platform]. In Blender 3.0, this is supported on Windows with RDNA and RDNA2 generation discrete graphics cards. It includes Radeon RX 5000 and RX 6000 series, and Radeon Pro W6000 series GPUs. Driver Radeon Pro 21.Q4 or Adrenalin 21.12.1 (or newer) is required.

We are working with AMD to add support for Linux and investigate earlier generation graphics cards, for the Blender 3.1 release.

Note that in the AMD driver, HIP is only officially supported for RDNA2 graphics cards. However we have found that RDNA also works in practice.

[[File:Cycles_3.0_hip_render_time.png|800px|Render time with an AMD Radeon RX 5500 XT and HIP|thumb|center]]

Benefits of the new HIP backend include:
* Reduced render time
* No runtime kernel compilation, so renders start quickly
* Feature parity with NVIDIA GPUs

=== Apple ===

Support for GPU rendering through Metal is under development for Blender 3.1.

=== OpenCL ===

OpenCL rendering support was removed. The combination of the limited Cycles kernel implementation, driver bugs, and stalled OpenCL standard has made maintenance too difficult.

We are working with hardware vendors to bring back GPU rendering support using other APIs.

== Viewport Interactivity ==

Viewport rendering is significantly more responsive due to new scheduling and display algorithms.

[[File:Blender 3 0 cycles navigation 2.mp4|thumbnail|center|800px|Viewport interactivity compared in Blender 2.93 and 3.0. Demo by Statix / redhoot]]

== Sampling ==

The recommended workflow is now to set a noise threshold (instead of number of samples), and let Cycles adaptively refine the image until the denoiser can eliminate the remaining noise. The default sampling settings and user interface are geared towards this workflow, though it is not required.

More specifically:
* Redesigned sampling user interface and default settings.
* Improved adaptive sampling. Noise threshold behaviour changed, it needs to be manually tweaked to get comparable results to older versions. [https://docs.blender.org/manual/en/3.0/render/cycles/render_settings/sampling.html#adaptive-sampling See manual].
* Time-based rendering limit. [https://docs.blender.org/manual/en/3.0/render/cycles/render_settings/sampling.html#bpy-types-cyclesrendersettings-time-limit See manual].
* Separate presets for viewport and render sampling.

=== Scrambling Distance ===

A new Scrambling Distance setting was added to the Advanced panel which can be used to control the amount of correlation between pixels with a range from 0 (highly correlated, not recommended) to 1 (no correlation). This can decrease render times for GPU rendering. To automatically estimate a good value, there is an Automatic setting. This automatically configures the scrambling distance and is the recommended way to use this setting. [https://docs.blender.org/manual/en/3.0/render/cycles/render_settings/sampling.html#bpy-types-cyclesrendersettings-scrambling-distance See manual].  ([https://projects.blender.org/blender/blender/commit/366262bef542 366262bef5], [https://projects.blender.org/blender/blender/commit/82cf25dfbfad 82cf25dfbf])

== Denoising ==

OpenImageDenoise was upgraded to version 1.4, with improved detail preservation. A new prefilter option was added to control the denoising of auxiliary albedo and normal passes. ([https://projects.blender.org/blender/blender/commit/c5e5ac4 c5e5ac4])

Auxiliary albedo and normal passes for volumes were improved, to better preserve volume detail and reduce low frequency noise. ([https://projects.blender.org/blender/blender/commit/d18a3b7 d18a3b7])

<center>
{| class="transparent" style="text-align: center"
 |+  style="caption-side: bottom" | Before and after denoiser improvements, click to enlarge.<br/>Renders by Jesus Sandoval
 |valign=top|[[File:Cycles_3.0_volume_denoise_before.jpeg|400px|center|link=https://wiki.blender.org/w/images/f/f7/Cycles_3.0_volume_denoise_before.jpeg]]
 |valign=top|[[File:Cycles_3.0_volume_denoise_after.jpeg|400px|center|link=https://wiki.blender.org/w/images/1/12/Cycles_3.0_volume_denoise_after.jpeg]]
 |-
 |valign=top|[[File:Cycles_3.0_archviz_denoise_before.jpeg|400px|center|link=https://wiki.blender.org/w/images/c/cd/Cycles_3.0_archviz_denoise_before.jpeg]]
 |valign=top|[[File:Cycles_3.0_archviz_denoise_after.jpeg|400px|center|link=https://wiki.blender.org/w/images/1/1d/Cycles_3.0_archviz_denoise_after.jpeg]]
 |}
</center>

== Tiles & Memory Usage ==

The render is now progressively refined by default, giving a quicker preview of full render. However when rendering high resolution images with many render passes, memory usage can be significant. For this reason high resolution renders are automatically split into large tiles, and each tile is cached to disk as it is completed.

Previously tweaking tile size was important for maximizing CPU and GPU performance. Now this is mostly automated. There are still two situations where reducing tile size from the default can be helpful:
* When there is not enough memory to fit both the scene and render result, reducing tile size makes more memory available for the scene.
* When a small object renders very slowly compared to other objects (for example transparent hair), rendering with smaller tiles can help keep the GPU busy.

== Shadow Catcher ==

The shadow catcher was rewritten. New features include:
* Indirect and environment light support, for more accurate compositing
* Option for lights to be considered included or excluded, that is if they are real or synthetic objects

By default the shadow catcher only affects the alpha channel of the Combined pass. However to fully handle colored indirect light and emission, the new Shadow Catcher pass should be used. The background should be multiplied by the Shadow Catcher pass, after which the Combined pass can be composited over it with alpha over.

<center>
{| class="transparent" style="text-align: center"
 |+  style="caption-side: bottom" | Shadow catcher passes and compositing setup<br/>Renders by Joni Mercado
 |valign=top|[[File:Cycles_3.0_shadow_catcher_background.jpg|198px|center]]
 |valign=top|[[File:Cycles_3.0_shadow_catcher_pass.jpg|198px|center]]
 |valign=top|[[File:Cycles_3.0_shadow_catcher_combined.jpg|198px|center]]
 |valign=top|[[File:Cycles_3.0_shadow_catcher_composited.jpg|197px|center]]
 |-
 |colspan=4 valign=top|[[File:Cycles_3.0_shadow_catcher_compositing.png|800px|center]]
 |}
</center>

== Shadow Terminator ==

New option to reduce shadow artifacts that often happen with low poly game models. Offset rays from the flat surface to match where they would be for a smooth surface as specified by the normals. ([https://projects.blender.org/blender/blender/commit/9c6a382 9c6a382])

The Geometry Offset option works as follows:
* 0: disabled
* 0.001: only terminated triangles (normal points to the light, geometry doesn't) are affected
* 0.1 (default): triangles at grazing angles are affected, and the effect fades out
* 1: all triangles are affected

Generally, using one or a few levels of subdivision can get rid of artifacts faster than before.

[[File:Cycles_3.0_shadow_terminator.png|800px|Low poly rendering compared between Blender 2.93 and 3.0<br/>Render by Juan Gea Rodriguez|thumb|center]]

== Subsurface Scattering ==

Subsurface scattering now supports anisotropy and index of refraction for Random Walk. Realistic skin has an anisotropy around 0.8. ([https://projects.blender.org/blender/blender/commit/7811c11 7811c11])

<center>
{| class="transparent" style="text-align: center"
 |+  style="caption-side: bottom" | Subsurface scattering anisotropy 0.0 and 0.8, click to enlarge.<br/> Renders by Metin Seven
 |valign=top|[[File:Cycles_3.0_zombie_anisotropy_0.0.png|400px|center|link=https://wiki.blender.org/w/images/c/cf/Cycles_3.0_zombie_anisotropy_0.0.png]]
 |valign=top|[[File:Cycles_3.0_zombie_anisotropy_0.8.png|400px|center|link=https://wiki.blender.org/w/images/0/08/Cycles_3.0_zombie_anisotropy_0.8.png]]
 |}
</center>

== More Features ==

* Light option to be visible to camera rays
* New Position render pass
* Volume sampling quality is now the same for CPU and GPU rendering
* Baking support for denoising and adaptive sampling
* Light clamping was modified to be more fine grained, preserving more non-noisy light.
* Open Shading Language metadata support for UI labels and checkboxes, following the specification. ([https://projects.blender.org/blender/blender/commit/e6bbbd9 e6bbbd9])
<source>
[[ string label = "UI Label" ]]
[[ string widget = "checkBox" ]]
[[ string widget = "boolean" ]]
</source>

== Removed Features and Compatibility ==

* Branched path tracing was removed. We recommend using adaptive sampling to automatically allocate more samples where needed.
* Cubic and Gaussian subsurface scattering. The physically based Random Walk and Christensen-Burley methods remain.
* The CMJ sampling pattern was removed. Only Sobol and PMJ remain.
* The NLM denoiser was removed. We recommend using OpenImageDenoise instead for final frames.