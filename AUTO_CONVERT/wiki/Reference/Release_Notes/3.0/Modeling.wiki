= Blender 3.0: Modeling =

== Modifiers ==
* Weld: new "Loose Edges" option that allows us to collapse only the edges without connected faces. ([https://projects.blender.org/blender/blender/commit/2beff6197e98  rB2beff619])

* Surface Deform: new Sparse Bind option that omits recording bind data for vertices excluded by the vertex group at the time of bind, in order to save memory and slightly speed up modifier evaluation. ([https://projects.blender.org/blender/blender/commit/1ab6d5c1dc  rB1ab6d5c1])

* Mesh Cache: support for blending influence using vertex groups ([https://projects.blender.org/blender/blender/commit/c8f80453d5c8e7004b46b866ca9923ce59766c7b c8f80453d5]).

== General Changes ==
* Laplacian smooth now supports N-gons ([https://projects.blender.org/blender/blender/commit/c15635bd8d5483a56107b5c31d8dc0b6a691a767 c15635bd8d]).
* Support flipping sides in mesh bisect ([https://projects.blender.org/blender/blender/commit/1bcdd1c54e6e9034af2ad1d78b30b891ac42a655 1bcdd1c54e]).
* Boolean rename "Self" option to "Self Intersection" ([https://projects.blender.org/blender/blender/commit/82f0e4948c36a4d69d0c3dc3e14d9b4376e0be37 82f0e4948c]).

== Knife Tool ==
The 2021 GSoC Knife Tool project made a number of improvements to the knife tool, which are now incorporated in this release ([https://projects.blender.org/blender/blender/commit/6e77afe6ec7b6a73f218f1fef264758abcbc778a 6e77afe6ec])

=== New Features ===
* Constrained Angle Mode Improvements
** Added Knife Tool setting to control snapping angle increment
** Added support for entering snapping angle increment via number keys
** Added a relative constrained angle mode (`A` key, with `R` to cycle through possible reference edges)
** Added Knife Tool setting for constrained angle default mode
* Snapping to Global and Local Orientation
** Press `X`, `Y` or `Z` to snap cut line to respective global axis
** Pressing the same key again will snap to local axis
** Respects scene orientation setting if set, allowing for custom orientation
* Visible Distance and Angle Measurements
** Pressing `S` will enable measurements
** Pressing `S` repeatedly will cycle between three modes - Only Distance, Only Angles, Both
** Shows length of current cut segment
** Shows angle between current cut segment and previous cut segment or mesh edge
** Shows angle between current cut segment and any snapped to cut segment or mesh edge
* Undo Capability
** Press Ctrl-Z to undo previous cut segment
** Starting point for current cut segment is adjusted accordingly
** If cut is a drag cut the entire drag cut is undone
* X-Ray Mode
** Press `V` to toggle depth checking for cut segment drawing code
* Multi-Object Edit Mode
** Added support for multi-object edit mode to Knife Tool
** Cuts can be made across objects and from the geometry of one object to another

=== Key Remappings ===
* Cancel
** `RIGHTMOUSE` to `ESC`
* New Cut
** `E` to `RIGHTMOUSE`
* Snap Midpoints
** `LEFT_CTRL` to `LEFT_SHIFT`
** `RIGHT_CTRL` to `RIGHT_SHIFT`
* Ignore Snapping
** `LEFT_SHIFT` to `LEFT_CTRL`
** `RIGHT_SHIFT` to `RIGHT_CTRL`
* Angle Snapping
** `C` to `A`
* Cut Through Toggle
** `X` to `C`