= Blender 2.80: Addon API =

== Version Info ==

Make sure the <code>"blender"</code> key in the <code>bl_info</code> dictionary is set to:<br>
<code>(2, 80, 0)</code> not <code>(2, 8, 0)</code> or <code>(2, 79, 0)</code>
:Example: <code>"blender": (2, 80, 0),</code>
Otherwise, this error is reported:
:<code>Exception: Add-on 'ADDON_NAME' has not been upgraded to 2.8, ignoring</code>

== Registration ==

=== Module Registration ===

Module registration (<tt>bpy.utils.register_module</tt>) convenience function has been removed, since keeping track of this information adds unnecessary overhead.

Add-on's should assign their classes to a tuple or list and register/unregister them directly.

eg:

<syntaxhighlight lang="Python">
classes = (
    FooClass,
    BarClass,
    BazClass,
)

def register():
    from bpy.utils import register_class
    for cls in classes:
        register_class(cls)

def unregister():
    from bpy.utils import unregister_class
    for cls in reversed(classes):
        unregister_class(cls)
</syntaxhighlight>

'''Tip''': To avoid having to copy & paste the functions above, you can use <tt>bpy.utils.register_classes_factory</tt> utility function.

<syntaxhighlight lang="Python">
classes = (
    FooClass,
    BarClass,
    BazClass,
)
register, unregister = bpy.utils.register_classes_factory(classes)
</syntaxhighlight>

'''Tip''': If you need to register only one class, then add a trailing "," to the classes list. 
<syntaxhighlight lang="Python">
classes = (
    FooClass,
)
register, unregister = bpy.utils.register_classes_factory(classes)
</syntaxhighlight>
Otherwise this error is reported:
:<code>TypeError: 'RNAMetaPropGroup' object is not iterable</code>

'''Tip''': If you have an addon with many classes, the list can be generated using this patch: https://developer.blender.org/P455

=== Class Registration ===

See [http://developer.blender.org/T52599 #52599] for proposal and details.

==== Access (bpy.types) ====

Classes registered by addons are no longer available in <tt>bpy.types</tt>. Instead addons can import their own modules and access the classes directly.

However subclasses of [Header, Menu, Operator, Panel, UIList] remain accessible from <tt>bpy.types</tt>.

==== Naming ====

In Blender2.7x it was too easy to accidentally register multiple classes with the same name.

To prevent collisions 2.8x enforces naming conventions ''(already in use across much of Blender's code-base)'' for classes `bl_idname`.

For operator <tt>bl_idname</tt>, the same naming conventions as in 2.7x remain. For headers, menus and panels, the <tt>bl_idname</tt> is expected to match the class name (automatic if none is specified).

The `bl_idname` convention is: <tt>UPPER_CASE_{SEPARATOR}_mixed_case</tt>, in the case of a menu the regular expression is:

    <tt>[A-Z][A-Z0-9_]*_MT_[A-Za-z0-9_]+</tt>

'''The separator for each identifier is listed below:'''

* Header -> <tt>_HT_</tt>
* Menu -> <tt>_MT_</tt>
* Operator -> <tt>_OT_</tt>
* Panel -> <tt>_PT_</tt>
* UIList -> <tt>_UL_</tt>

'''Valid Examples:'''

* <tt>class OBJECT_OT_fancy_tool</tt> (and <tt>bl_idname = "object.fancy_tool"</tt>)
* <tt>class MyFancyTool</tt> (and <tt>bl_idname = "MYADDON_MT_MyFancyTool"</tt>)
* <tt>class SOME_HEADER_HT_my_header</tt>
* <tt>class PANEL123_PT_myPanel</tt> ''(lower case is preferred but mixed case is supported)''.

'''Class names:'''

Matching the class name to the `bl_idname` is optional.

=== Class Property Registration ===

Classes that contain properties from `bpy.props` now use Python's type annotations (see [https://www.python.org/dev/peps/pep-0526/ PEP 526]) and should be assigned using a single colon <code>:</code> in Blender 2.8x instead of equals <code>=</code> as was done in 2.7x:

'''2.7x:'''

<syntaxhighlight lang="Python">
class MyOperator(Operator):
    value = IntProperty()
</syntaxhighlight>

'''2.8x:'''
<syntaxhighlight lang="Python">
class MyOperator(Operator):
    value: IntProperty()
</syntaxhighlight>

Using the 2.7x syntax in 2.80 or later will result in this error:
:<code>Warning: class Foo "contains a property which should be an annotation!"</code>