= Blender 2.80: Python API Changes =

Blender 2.80 is an API breaking release. Addons and scripts will need to be updated to run on 2.80, both to handle the new features and adapt to changes to make the API more consistent and reliable.

== [[Reference/Release_Notes/2.80/Python_API/Addons|Addon API]] ==

Module and class registration has been changed, all addons need to be updated to handle these.

== [[Reference/Release_Notes/2.80/Python_API/Scene_and_Object_API|Scene and Object API]] ==

The new view layers, collections, selection and dependency graph systems require updates to scripts.

== [[Reference/Release_Notes/2.80/Python_API/Mesh_API|Mesh API]] ==

A few changes were made to the Mesh API, mainly the data structures for triangles tessellation.

== [[Reference/Release_Notes/2.80/Python_API/Preferences_API|Preferences API]] ==

To match the redesigned and reorganized Preferences UI, the Python API got updated too.

== [[Reference/Release_Notes/2.80/Python_API/UI_API|User Interface API]] ==

New toolbar, property layouts and icons require addon updates.

== [[Reference/Release_Notes/2.80/Python_API/UI_DESIGN|Adapting Your Addon for the 2.8 User Interface]] ==

Blender 2.8 has a new UI that requires your addon to adapt.

== [[Reference/Release_Notes/2.80/Python_API/Draw_API|Draw API]] ==

Blender was upgraded to the OpenGL core profile, which affects addons.

== [[Reference/Release_Notes/2.80/Python_API/Timer_API|Timer API]] ==

A new API to run functions in specific time intervals.

== [[Reference/Release_Notes/2.80/Python_API/Animation_API|Animation & Rigging API]] ==

A number of utility methods related to animation and rigging have been added.

== [[Reference/Release_Notes/2.80/Python_API/Modules|Helper Modules]] ==

Various helper Python modules have been added, removed or changed.

== Renaming ==

A few terms have been renamed in the user interface, and the Python API follows along. The main ones are:
* Lamp to Light
* Group to Collection
* Dupli to Instance
* Render Layer to View Layer
* User Preferences to Preferences

== Keyword Only Arguments ==

All optional arguments must be given as keywords and cannot be passed by position:

'''2.7x'''
<syntaxhighlight lang="Python">
context.scene.frame_set(10, 0.25)
noise.noise(position, noise.types.BLENDER)
</syntaxhighlight>

'''2.8x'''
<syntaxhighlight lang="Python">
context.scene.frame_set(10, subframe=0.25)
noise.noise(position, noise_basis='BLENDER')
</syntaxhighlight>

== Matrix Multiplication ==

Matrix multiplication previously used *, scripts should now use @ for multiplication (per [https://www.python.org/dev/peps/pep-0465/ PEP 465]). This applies to:
* Vector * Vector
* Quaternion * Vector
* Matrix * Vector
* Vector * Matrix
* Matrix * Matrix

Note: In the future * will be used for element-wise multiplication
 
'''2.7x:'''
<syntaxhighlight lang="Python">
mat = Matrix()
vec = Vector()
result = mat * vec
</syntaxhighlight>

'''2.8x:'''
<syntaxhighlight lang="Python">
mat = Matrix()
vec = Vector()
result = mat @ vec
</syntaxhighlight>

Using the 2.7x syntax in 2.80 or later will result in this error:
:<code>TypeError: Element-wise multiplication: not supported between 'xxx' and 'yyy' types</code>


=== In Place Matrix Multiplication ===

Note that <tt>mat_a *= mat_b</tt> did not change the original matrix, now in 2.8 it does (matching other types - Vector and Python lists for example).

Noting this since it could be a breaking change for scripts that relied on it creating a new matrix, especially if in cases where a matrix is passed as a function argument and changed to it aren't expected.

== Importing Text ==

The ability to import Blender text data blocks as modules has been removed ([https://projects.blender.org/blender/blender/commit/dc8dd24351 dc8dd24351]).

If you need to get a text data-block as a module use:

<syntaxhighlight lang="Python">
my_module = bpy.data.text["my_module"].as_module()
</syntaxhighlight>