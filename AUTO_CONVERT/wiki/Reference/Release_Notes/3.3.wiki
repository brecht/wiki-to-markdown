= Blender 3.3 Release Notes =

Blender 3.3 was released on September 7, 2022.

Check out out the final [https://www.blender.org/download/releases/3-3/ release notes on blender.org].

This release includes long-term support, see the [https://www.blender.org/download/lts/ LTS page] for a list of bugfixes included in the latest version.

== [[Reference/Release Notes/3.3/Animation_Rigging|Animation & Rigging]] ==

== [[Reference/Release Notes/3.3/Core|Core]] ==

== [[Reference/Release Notes/3.3/Grease_Pencil|Grease Pencil]] ==

== [[Reference/Release Notes/3.3/Modeling|Modeling]] ==

== [[Reference/Release Notes/3.3/Nodes_Physics|Nodes & Physics]] ==

== [[Reference/Release Notes/3.3/Pipeline_Assets_IO|Pipeline, Assets & I/O]] ==

== [[Reference/Release Notes/3.3/Python_API|Python API & Text Editor]] ==

== [[Reference/Release Notes/3.3/Cycles|Render & Cycles]] ==

== [[Reference/Release Notes/3.3/Sculpt|Sculpt, Paint, Texture]] ==

== [[Reference/Release Notes/3.3/User_Interface|User Interface]] ==

== [[Reference/Release Notes/3.3/VFX|VFX & Video]] ==

== [[Reference/Release Notes/3.3/Add-ons|Add-ons]] ==

== [[Reference/Release Notes/3.3/More_Features|More Features]] ==