= User Interface =

* Tabs in the Properties editor show the name of the tab in a tooltip, quicker than the usual tooltips. ([https://projects.blender.org/blender/blender/commit/1b94e60fb0 1b94e60fb0]).
* Drag & drop inside Blender now supports <span class="hotkeybg"><span class="hotkey">Esc</span></span> or <span class="hotkeybg"><span class="hotkey" style="padding-left: 3px;">RMB [[File:Template-RMB.png]]</span></span> to cancel dragging. ([https://projects.blender.org/blender/blender/commit/d58e422ac3 d58e422ac3]).
* Hover highlighting of UIList rows. ([https://projects.blender.org/blender/blender/commit/7216eb8879 7216eb8879]).
* Some search boxes are widened if needed, for example if there is a linked scene. ([https://projects.blender.org/blender/blender/commit/cfb112edcb cfb112edcb]).
* Outliner now allows the filtering of Grease Pencil objects. ([https://projects.blender.org/blender/blender/commit/b558fff5b8 b558fff5b8]).
* Resetting to defaults, eyedropper, and other operations working correctly in popovers. ([https://projects.blender.org/blender/blender/commit/c402b3f52f c402b3f52f]).
* The "Open Recent" menu list default size increased to 20 items. ([https://projects.blender.org/blender/blender/commit/19ca2e905e 19ca2e905e]).
* Fallback font "Noto Emoji" updated with new Unicode 15.0 emojis. ([https://projects.blender.org/blender/blender/commit/2215ee9358 2215ee9358]).
* Color Picker can now adjust position to better fit within available space.  ([https://projects.blender.org/blender/blender/commit/fa13058fa6 fa13058fa6]).
* Searching for nodes with a slash character in their names works correctly now. ([https://projects.blender.org/blender/blender/commit/2b565c6bd0 2b565c6bd0]).
* Alt-click support to edit string properties for multiple objects. ([https://projects.blender.org/blender/blender/commit/43eb3fe21a 43eb3fe21a]).
* Better positioning of text with Measure/Ruler tools. ([https://projects.blender.org/blender/blender/commit/b367a2b5f9 b367a2b5f9]).
* Clicking the NLA/Graph Editor search box no longer scrolls the items below it. ([https://projects.blender.org/blender/blender/commit/d1219b727c d1219b727c]).
* Node Editor supports Smooth View. ([https://projects.blender.org/blender/blender/commit/2f0b166dce 2f0b166dce]).
* Support operator enums in Quick Favorites. ([https://projects.blender.org/blender/blender/commit/04d50f4b23 04d50f4b23]).
* More Context Menus open with last-used option. ([https://projects.blender.org/blender/blender/commit/eac3d37ab1 eac3d37ab1]).
* Many Operator confirmations can be now be disabled with the "Confirm" checkbox (on by default) in the keymap. ([https://projects.blender.org/blender/blender/commit/6ae44c4770 6ae44c4770]).
  [[File:Keymap confirm.png||600px|thumb|center]]
== Asset Browser ==

* Assets now shows tooltips with the full name and description. ([https://projects.blender.org/blender/blender/commit/0ee0e8c0d4 0ee0e8c0d4]).
[[File:AssetTooltips.png|600px|thumb|center]]
* Meta-data fields in the sidebar are not shown if blank and read-only. ([https://projects.blender.org/blender/blender/commit/199c7da06d 199c7da06d]).
* ''Clear Asset'' now works for all selected assets ([https://projects.blender.org/blender/blender/commit/20f54a5698 20f54a5698]).

== File Browser ==
* File Browser list mode items can now be dragged by the full name, not just by the icon. ([https://projects.blender.org/blender/blender/commit/c3dfe1e204 c3dfe1e204]).
* File Browser items now have "External" menu for opening and other operations outside of Blender.  ([https://projects.blender.org/blender/blender/commit/694f792ee1 694f792ee1]).

== Platform-Specific Changes ==
* MacOS: recently opened/saved files will now show in Dock context menu and in App Expose. ([https://projects.blender.org/blender/blender/commit/f04a7a07e3 f04a7a07e3]).
[[File:MacDock.png|thumb|center]]
* MacOS: double-clicking a blend file will now always load even if Blender loses focus while launching. ([https://projects.blender.org/blender/blender/commit/ed4374f089 ed4374f089]).
* Windows: autofocus child windows on hover. ([https://projects.blender.org/blender/blender/commit/defd95afcd defd95afcd]).
* Windows: Image Editor can now copy/paste images with other applications.  ([https://projects.blender.org/blender/blender/commit/39bcf6bdc9 39bcf6bdc9]).

== 3D Text Objects == 
* Text selection by mouse dragging. Word selection by double-click. ([https://projects.blender.org/blender/blender/commit/68f8253c71 68f8253c71]).
* Nicer control and feedback for text styles.  ([https://projects.blender.org/blender/blender/commit/f7ba61d3a6 f7ba61d3a6]).
* "To Uppercase" and "To Lowercase" now supports most languages.  ([https://projects.blender.org/blender/blender/commit/e369bf4a6d e369bf4a6d]).
* Operators to move the cursor to the top and bottom. ([https://projects.blender.org/blender/blender/commit/6d2351d26b 6d2351d26b]).

== Transform Settings ==
* Proportional Size can now be set in the Proportional Editing popover ([https://projects.blender.org/blender/blender/commit/6b5b777ca3 6b5b777ca3]).
[[File:ProportionalSize.png|600px|thumb|center]]