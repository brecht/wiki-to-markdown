= Blender 2.93 Release Notes =

Blender 2.93 was released on June 2, 2021.

Check out the final [https://www.blender.org/download/releases/2-93/ release notes on blender.org].

This release includes long-term support, see the [https://www.blender.org/download/lts/ LTS page] for a list of bugfixes included in the latest version.

== [[Reference/Release Notes/2.93/Geometry_Nodes|Geometry Nodes]] ==

== [[Reference/Release Notes/2.93/User_Interface|User Interface]] ==

== [[Reference/Release Notes/2.93/Modeling|Modeling]] ==

== [[Reference/Release Notes/2.93/Sculpt|Sculpt, Paint, Texture]] ==

== [[Reference/Release Notes/2.93/Animation-Rigging|Animation & Rigging]] ==

== [[Reference/Release Notes/2.93/Grease_Pencil|Grease Pencil]] ==

== [[Reference/Release Notes/2.93/EEVEE|EEVEE & Viewport]] ==

== [[Reference/Release Notes/2.93/Cycles|Render & Cycles]] ==

== [[Reference/Release Notes/2.93/Core|Data Management, Linking, Overrides]] ==

== [[Reference/Release Notes/2.93/IO|Pipeline, Assets & I/O]] ==

== [[Reference/Release Notes/2.93/Python_API|Python API & Text Editor]] ==

== [[Reference/Release Notes/2.93/VFX|VFX & Video]] ==

== [[Reference/Release Notes/2.93/Physics|Nodes & Physics]] ==

== [[Reference/Release Notes/2.93/Add-ons|Add-ons]] ==

== [[Reference/Release Notes/2.93/More_Features|More Features]] ==

== Compatibility ==

Windows 7 is no longer supported. Windows 8.1 or newer is required. [https://support.microsoft.com/en-us/windows/windows-7-support-ended-on-january-14-2020-b75d4580-2cc7-895a-2c9c-1466d9a53962 Microsoft discontinued Windows 7 support in January 2020].