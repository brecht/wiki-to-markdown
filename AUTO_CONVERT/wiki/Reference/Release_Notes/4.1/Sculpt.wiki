= Sculpt, Paint, Texture =

* Voxel remesh attribute preservation has been changed to propagate ''all'' attributes and to improve performance ([https://projects.blender.org/blender/blender/commit/ba1c8fe6a53a00c9324607db6a1836132cef5352 ba1c8fe6a5]).