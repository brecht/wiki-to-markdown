= Blender feature demo scene info =

Information about Blender feature demo scene submissions for the official Blender [https://www.blender.org/download/demo-files/ Demo Files] page.

<br />

✍ Maintainer: [[User:Metin_Seven|Metin Seven]]

📅 Updated: '''April 29, 2021'''

<br />

== Demo scene submission guidelines ==

==== Please follow these guidelines for Blender demo scene preparation ====

<br />

* Include a Text Editor window with a text datablock named 'README'.
:In the text please include:<br />
:─ Some info / instructions regarding the scene, comprehensible for novice Blender users.<br />
:─ The Creative Commons license type: CC-0 (preferred), CC-BY or CC-BY-ND.<br />
:─ Your name or nickname.<br />
:─ A URL (e.g. your portfolio or Twitter profile URL).

* Optionally, you may choose to add a specific info text to different workspaces, to explain more of your creative process (see example screenshots below). This is appreciated but not required.

* Check if the scene doesn't include settings that may confuse novice users, such as a locked camera. If such a setting is essential to the demo's functioning, please include a notice in the 'README' info text.

* Check if the scene does not only render correctly in the viewport, but also when pressing F12.

* Don't include self-promotion / signatures / logos as a part of the demo scene. You can include a credit in the 'README' info text.

* Make sure no copyrighted models, textures or other assets are present in the scene.

* If uncopyrighted assets (image textures, HDRI environments, etc.) are part of the scene, make sure they are packed into the Blend file, using File menu ➔ External Data...

* Save the scene from the most recent appropriate Blender version to ensure compatibility with the final release version.

* To minimize the file size, activate the Compress option in the File ➔ Save As... file dialog options panel when saving the Blend file.

* In case of multiple Blend files showcasing related features, please consider merging the demos into one Blend file containing multiple Scenes, naming the Scenes appropriately and mentioning the presence of multiple Scenes in the 'README' info text.

* Please provide a publicly accessible download link (e.g. a Google Drive link) to the finished scene, that will remain intact until at least the next Blender release. The scene will be uploaded to the Blender.org server, so the link does not have to remain intact after the Blender release.

<br />

[[File:Blender demo scene example screenshots.png|1200px|center]]
''Blender demo scene example screenshots, taken from the [https://cloud.blender.org/p/gallery/5f4cd4441b96699a87f70fcb Splash Fox] scene by Daniel Bystedt.''

<br />

== Demo scene publication ==

After verification and adjustments (if necessary), the scenes are added to the Blender [https://www.blender.org/download/demo-files/ Demo Files] page in a deactivated state (visible in the CMS, invisible in the Demo Files page), and are activated around the release date of the new Blender version.

📝 Note to Blender.org maintainers: please verify the new demo files in the release (candidate) version, to avoid possible issues because of changes in Blender during the alpha development stage.