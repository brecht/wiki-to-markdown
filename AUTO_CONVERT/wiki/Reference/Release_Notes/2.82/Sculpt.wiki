= Blender 2.82: Sculpt and Paint =

== Sculpting ==
=== Tools ===
*Slide/Relax tool: slides the topology of the mesh in the direction of the stroke, trying to preserve the volume. 
**When pressing Shift, the brush enters Relax mode and tries to create an even distribution of quads without deforming the volume of the mesh.
**The Relax mode is also available as a new option in the Mesh Filter tool, which applies the same effect to the whole mesh. 
*Multiplane Scrape: Scrapes the mesh with two planes at the same time, producing a sharp edge between them.
**The plane angle is controlled by the Plane Angle property. When pressing Ctrl, the angle is inverted
**Multiplane Scrape Dynamic mode: When enabled, the base angle is sampled from the mesh surface. The Plane Angle property controls how much the angle will increase when applying pen pressure. When pressing Ctrl, it locks the plane angle to 0 degrees.

=== Operators ===
*Mask Slice operator: removes the masked vertices from the mesh, with options to fill the produced holes or creating a new mesh object with the removed vertices. 
*The symmetrize operator can now run with Dyntopo disabled.
*The sample detail size operator now works with the voxel remesher. It sets the voxel size to a value that produces a mesh with an approximate detail resolution to the area that was sampled.

=== Pose Brush ===
*Smooth Iterations property to control how smooth is the falloff of the deformation.
*IK segments property to control how many IK bones are going to be created for posing. 
*When pressing Ctrl, the pose brush applies twist rotation to the posing segments instead of using the rotation or an IK deformation. The falloff of the rotation across multiple segments is controlled by the brush falloff curve.

=== Brush Updates ===
*The Clay brush tool deformation was modified. It now samples the maximum displacement distance and tries to adapt to the surface to fill the holes in the volume.
*Clay and Clay Strips brush tools now have non-linear input pressure/strength and pressure/size curves to increase their range values.
*The mask brush tool now applies the mask in a way that does not produce line artifacts in the border of the gradients when using soft falloffs.
*The Scrape and Fill brush tools have a property to invert the brush to Scrape/Fill instead of Peaks when pressing Ctrl.
*The cursor has more saturated colors by default to make it more visible on the default matcaps.
*The cursor color now supports alpha.

== Paint ==
*Dash ratio and dash samples brush properties to create dashed strokes. Dash ratio controls the ratio of samples that are enabled during a cycle with the number of brush samples specified in dash samples. 
*Unified size/pressure and alpha/pressure toggles were removed to avoid overriding by accident the input curves of the brushes. 
*New Smoother curve falloff which is slightly different to smooth, which a more flatten falloff towards the center of the brush.