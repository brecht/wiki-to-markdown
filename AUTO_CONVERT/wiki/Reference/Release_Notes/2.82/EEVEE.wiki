= Blender 2.82: EEVEE =

== Viewport Render Passes ==

In the 3d viewport an option was added to preview an EEVEE render pass. Render passes that are supported are

* Combined
* Ambient Occlusion
* Normal
* Mist
* Subsurface Direct
* Subsurface Color

In the future more render passes will be added.

== Volumetric with partial transparency fix ==

A bug was fixed that changes how Alpha Blended surfaces reacts to volumetric effect. The old behavior was adding more light if the surface was partially transparent.

<center>
{| class="transparent" style="text-align: center"
 |+  style="caption-side: bottom" | Mr Elephant demo file lamps before and after the fix
 |valign=top|[[File:Capture_d’écran_du_2020-02-04_16-13-17.png|240px|center]]
 |valign=top|[[File:EEVEE_282_volume_fix_after.png|300px|center]]
 |}
</center>

This fix will alter the appearance of old file, there is no workaround other than to tweak the affected materials.

([https://projects.blender.org/blender/blender/commit/39d5d11e022a 39d5d11e02])

== Improvements ==

* Group node sockets now converts the values as in Cycles. ([https://projects.blender.org/blender/blender/commit/c2e21b23296 c2e21b2329])
* Curve, surface and text object can now use normal mapping without workaround. ([https://projects.blender.org/blender/blender/commit/15350c70be8 15350c70be] [https://projects.blender.org/blender/blender/commit/c27d30f3eaf c27d30f3ea] [https://projects.blender.org/blender/blender/commit/bcacf47cbcf bcacf47cbc])
* Disk lights artifact appearing on certain drivers have been fixed. ([https://projects.blender.org/blender/blender/commit/3d73609832d 3d73609832])
* Volumetric objects with zero volume do not fill the scene anymore. ([https://projects.blender.org/blender/blender/commit/0366c46ec64 0366c46ec6])
* "Bake Cubemap Only" operator does not reset the Irradiance Volume after file load or if world is updated. ([https://projects.blender.org/blender/blender/commit/5d5add5de2b 5d5add5de2])
* Improved quality of render passes with high samples count ([https://projects.blender.org/blender/blender/commit/17b63db4e272 17b63db4e2])
* Improved performance when calculating render passes ([https://projects.blender.org/blender/blender/commit/9d7f65630b20 9d7f65630b])
* Improved performance during viewport rendering ([https://projects.blender.org/blender/blender/commit/7959dcd4f631 7959dcd4f6])