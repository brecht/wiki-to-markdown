= Blender 2.82: Import & Export =

== Universal Scene Description ==

USD file export is now supported, using File → Export → Universal Scene Description. This is an initial implementation, and USD support will be improved over time to support more complete scene interchange. Information can be found in the [https://docs.blender.org/manual/en/dev/files/import_export/usd.html USD chapter in the Blender manual].

[[File:Blender-2.82-USD-usdview-spring.png|600px|thumb|center|USD file exported from Blender and shown in USDView]]

For developers, there is more [[Source/Architecture/USD|USD technical documentation]] about the implementation. ([https://projects.blender.org/blender/blender/commit/ec62413f803ee506 ec62413f80])

== Alembic ==

* Mesh normals are now exported properly to Alembic. ([https://docs.blender.org/manual/en/dev/files/import_export/alembic.html#custom-split-normals-of-meshes Manual]). ([https://projects.blender.org/blender/blender/commit/b0e7a1d4b492 b0e7a1d4b4])
* Changed "Visible Layers" to "Visible Objects" in export options. Similarly, the Alembic exporter operator `bpy.ops.wm.alembic_export()` changed the keyword argument `visible_layers_only` to `visible_objects_only`. ([https://projects.blender.org/blender/blender/commit/d9e61ce1953b d9e61ce195])

== glTF ==

=== Importer ===

* Multiple files import feature ([https://projects.blender.org/blender/blender-addons/commit/67e42c79e5c3  rBA67e42c7])
* Better names for UVMap and VertexColor ([https://projects.blender.org/blender/blender-addons/commit/08352b3af770  rBA08352b3])
* Size of bone at creation are now based on armature scale ([https://projects.blender.org/blender/blender-addons/commit/76fc4142b518  rBA76fc414])
* Import data from texture samplers + exporter fix ([https://projects.blender.org/blender/blender-addons/commit/9f1b91ca1316  rBA9f1b91c])
* Use vertexcolor node instead of attributenode ([https://projects.blender.org/blender/blender-addons/commit/74a8fb2aa8f9  rBA74a8fb2])
* Set custom props from extras for bones ([https://projects.blender.org/blender/blender-addons/commit/0141c7f4cf49  rBA0141c7f])
* Accessor/buffer decoding enhancements ([https://projects.blender.org/blender/blender-addons/commit/eb4085f1dc15  rBAeb4085f], [https://projects.blender.org/blender/blender-addons/commit/47bef1a8a535  rBA47bef1a])
* Import extra data as custom properties ([https://projects.blender.org/blender/blender-addons/commit/ecdaef952383  rBAecdaef9])
* Fix bad vertex color alpha import ([https://projects.blender.org/blender/blender-addons/commit/6087f4c26540  rBA6087f4c])
* Fix avoid crash on invalid glTF file with multiple same target path ([https://projects.blender.org/blender/blender-addons/commit/9189c4595844  rBA9189c45])
* Fix import of bone rest pose of negative scaled bones ([https://projects.blender.org/blender/blender-addons/commit/2b04a1ab6829  rBA2b04a1a])
* Fix bug assigning pose bone custom prop ([https://projects.blender.org/blender/blender-addons/commit/fe5f339eb0b4  rBAfe5f339])

=== Exporter ===

* Option to export only deformation bones ([https://projects.blender.org/blender/blender-addons/commit/b9b1814a4c26  rBAb9b1814])
* Basic SK driver export (driven by armature animation) ([https://projects.blender.org/blender/blender-addons/commit/f505743b2f9f  rBAf505743])
* Performance improvement on image export ([https://projects.blender.org/blender/blender-addons/commit/289fb2b8b89e  rBA289fb2b])
* Define a user extension API ([https://projects.blender.org/blender/blender-addons/commit/18246268e802  rBA1824626], [https://projects.blender.org/blender/blender-addons/commit/73b85949a06c  rBA73b8594], [https://projects.blender.org/blender/blender-addons/commit/9733d0f73654  rBA9733d0f], [https://projects.blender.org/blender/blender-addons/commit/8304b9bac7dc  rBA8304b9b])
* Alphabetically sorting of exported animation ([https://projects.blender.org/blender/blender-addons/commit/5f1ad50707e7  rBA5f1ad50])
* Add option to export textures into a folder ([https://projects.blender.org/blender/blender-addons/commit/60a11a0fc442  rBA60a11a0])
* Keep constant/step interpolation when sampled animation export ([https://projects.blender.org/blender/blender-addons/commit/b90cbbdf4801  rBAb90cbbd])
* Detach last exported animation if needed ([https://projects.blender.org/blender/blender-addons/commit/bad59573c39e  rBAbad5957])
* Add export support for Emission socket of Principled BSDF node ([https://projects.blender.org/blender/blender-addons/commit/78fe8ec049fa  rBA78fe8ec])
* Do not split in primitives if materials are not exported ([https://projects.blender.org/blender/blender-addons/commit/d709b46340b5  rBAd709b46])
* Remove primitive splitting ([https://projects.blender.org/blender/blender-addons/commit/45c87c1ae354  rBA45c87c1])
* Export action custom props as animation extras ([https://projects.blender.org/blender/blender-addons/commit/334ca375b5e6  rBA334ca37])
* Fix exporting area lights that are not supported ([https://projects.blender.org/blender/blender-addons/commit/c172be1c93a6  rBAc172be1])
* Fix exporting instances when apply modifier is disabled ([https://projects.blender.org/blender/blender-addons/commit/c23d2a741e21  rBAc23d2a7])
* Fix crash when armature action is linked to mesh object ([https://projects.blender.org/blender/blender-addons/commit/7003720257c9  rBA7003720], [https://projects.blender.org/blender/blender-addons/commit/a6ce1b121eb7  rBAa6ce1b1])
* Fix transforms for mesh parented directly to bones ([https://projects.blender.org/blender/blender-addons/commit/a96fa1e6111a  rBAa96fa1e])
* Fix skinning export when using draco compression ([https://projects.blender.org/blender/blender-addons/commit/1470f353c650  rBA1470f35])
* Fix extension required parameter check ([https://projects.blender.org/blender/blender-addons/commit/619f3b5b2d8c  rBA619f3b5])
* Fix animation that was removed when using both TRS + SK animation ([https://projects.blender.org/blender/blender-addons/commit/24cc252dc6f6  rBA24cc252])
* Fix Draco bug when exporting Blender instances ([https://projects.blender.org/blender/blender-addons/commit/d504a09ab5ea  rBAd504a09])
* Fix buffer size with Draco compression ([https://projects.blender.org/blender/blender-addons/commit/dc0281955383  rBAdc02819])
* Allow draco compression when exporting without normals ([https://projects.blender.org/blender/blender-addons/commit/5367ebad4813  rBA5367eba])
* Fix export of light with shader node ([https://projects.blender.org/blender/blender-addons/commit/329f2c4a8b9f  rBA329f2c4])
* Fix normals when changed by modifiers ([https://projects.blender.org/blender/blender-addons/commit/6238248739dc  rBA6238248], [https://projects.blender.org/blender/blender-addons/commit/3a774beff73d  rBA3a774be])
* Fix avoid exporting rotation twice when both euler and quaternion ([https://projects.blender.org/blender/blender-addons/commit/97b11857d320  rBA97b1185])

== VFX Reference Platform ==

Blender is now compatible with the [https://vfxplatform.com VFX Reference Platform] 2020 for library dependencies and Python. This helps improve compatibility between Blender and other VFX software, especially now that the VFX industry is also switching to Python 3.