== Bevel ==

=== Custom Profiles ===

The bevel tool and modifier now have the ability use modified profiles beyond the existing super-ellipse control ([https://projects.blender.org/blender/blender/commit/ba1e9ae4733a ba1e9ae473]).

Once the "Custom Profile" option is checked, the profile can be changed with the curve widget which is sampled with the number of segments from the bevel.

The orientation of asymmetrical profiles is regularized, so that the profile will be flipped consistently on successive groups of edges.

Presets such as steps and a few simple architectural mouldings are included.

[[File:Custom_Bevel_Profile_Release_Notes.png|600px|thumb|center|A simple custom profile along a curving group of edges]]

=== Cutoff Vertex Mesh Method ===

A simpler method of creating meshes to replace vertices was added, mostly for use with more complicated asymmetrical bevel profiles where greater than two-way intersections can result in arbitrary geometry ([https://projects.blender.org/blender/blender/commit/ba1e9ae4733a ba1e9ae473]).

[[File:GSoC2019_Bevel_Cutoff_Method.png|400px|thumb|center|An example of the cutoff method]]

== Weld Modifier ==

Now we have the weld modifier that merges groups of vertices within a threshold ([https://projects.blender.org/blender/blender/commit/f050fa325375 f050fa3253]).
[[File:Weld Modifier Panel.png|600xpx|thumb|center|Weld modifier panel]]

== Solidify Modifier ==

The solidify modifier is now able to create thickness in edges with more
two adjacent faces, for example when three walls of a building join in a
T-formation. It is now also able to produce perfect results on
meshes with a normal flip seam like a mobius strip. The algorithm to
calculate the positioning of the vertices is also improved to get much
closer to perfectly even thickness than it was possible with the old
simple extrude algorithm. Also the new algorithm will handle degenerate cases as well as possible.
([https://projects.blender.org/blender/blender/commit/e45cfb574ee7 e45cfb574e]).

There is now an option to use the angle for clamping as well as the length
of the shortest adjacent edge from before. This will give better results in
many cases. ([https://projects.blender.org/blender/blender/commit/e45cfb574ee7 e45cfb574e]).

[[File:Blender 2-82 releasenotes updatesolidify2.png|600px|thumb|center|Improved Solidify Modifier]]