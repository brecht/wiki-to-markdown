= Color Management =

== AgX View Transform ==

The AgX view transform has been added, and replaces Filmic as the default in new files. ([https://projects.blender.org/blender/blender/commit/a9053f7efbe8b25429b11bccd679dbe81ea60e3f a9053f7efb])

This view transform provides better color handling in over-exposed areas compared to Filmic. In particular bright colors go towards white, similar to real cameras. Technical details and image comparisons can be found in [https://projects.blender.org/blender/blender/pulls/106355 PR #106355].

<center>
{| class="transparent" style="text-align: center"
 |+  style="caption-side: bottom" | Filmic and AgX, using bright saturated colors.<br/>Scenes by Eary Chow, Alaska Young and Leonard Siebeneicher. Using assets from by Blender Studio, AmbientCG and Poly Haven.
 |valign=top|[[File:Render4.0-einar-filmic.jpeg|400px|center|link=https://wiki.blender.org/w/images/c/c7/Render4.0-einar-filmic.jpeg]]
 |valign=top|[[File:Render4.0-einar-agx.jpeg|400px|center|link=https://wiki.blender.org/w/images/b/bb/Render4.0-einar-agx.jpeg]]
 |-
 |valign=top|[[File:Render4.0-stray-filmic.jpg|400px|center|link=https://wiki.blender.org/w/images/6/63/Render4.0-stray-filmic.jpg]]
 |valign=top|[[File:Render4.0-stray-agx.jpg|400px|center|link=https://wiki.blender.org/w/images/3/37/Render4.0-stray-agx.jpg]]
 |-
 |valign=top|[[File:Render-4.0-circle-filmic.jpeg|400px|center|link=https://wiki.blender.org/w/images/2/21/Render-4.0-circle-filmic.jpeg]]
 |valign=top|[[File:Render-4.0-circle-agx.jpeg|400px|center|link=https://wiki.blender.org/w/images/9/9f/Render-4.0-circle-agx.jpeg]]
 |}
</center>

== HDR Display on macOS ==

A new High Dynamic Range option was added in the Color Management > Display panel. It enables display of extended color ranges above 1.0 for the 3D viewport, image editor and render previews. ([https://projects.blender.org/blender/blender/commit/2367ed2ef241846d99fc8bab73abad4d48b9e67b 2367ed2ef2])

This requires a monitor that can display HDR colors, and a view transform designed for HDR output. The Standard view transform works, but Filmic and AgX do not as they were designed to bring values into the 0..1 range for SDR displays.

== New Color Spaces ==

New linear spaces and display devices were added, together with some renaming to improve clarity. Forward compatibility code is included into Blender 3.6. ([https://projects.blender.org/blender/blender/commit/6923f7a1539 6923f7a153])

== Removed Features ==

* Textures and other areas of Blender now always considers the color management is enabled. This used to be a compatibility option for the "No Color Management" setting in Blender pre 2.64. To achieve the same functionality the images need to be set to Non-Color space. ([https://projects.blender.org/blender/blender/commit/63e2832057 63e2832057])
* Unused color spaces and display devices were removed from the OCIO configuration ([https://projects.blender.org/blender/blender/commit/b2b7b37139 b2b7b37139])
* The XYZ display was only used to output images in XYZ space, for which there is now an option in the image output settings. Instead of the None display use Raw vie of the sRGB display. There is a versioning code in place to migrate the display settings to the new notation.