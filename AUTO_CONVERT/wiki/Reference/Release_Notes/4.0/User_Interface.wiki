= User Interface =

== Search ==

* All regular dropdown and context menus can be searched by pressing spacebar. ([https://projects.blender.org/blender/blender/commit/35d3d52508 35d3d52508]).
* Add menus can be searched by immediate typing. ([https://projects.blender.org/blender/blender/commit/b688414223 b688414223]). 
* Recently searched items are now at the top of search lists, with an option in the Preferences for disabling it. ([https://projects.blender.org/blender/blender/commit/8362563949 8362563949]).

== Text ==

* Default user interface font changed to Inter. ([https://projects.blender.org/blender/blender/commit/f58f6d0338 f58f6d0338]).
[[File:Inter.png|400px|thumb|center]]
* Text output now more pleasing and typographically correct. ([https://projects.blender.org/blender/blender/commit/a0b4ead737 a0b4ead737])
* New user preference added for "subpixel anti-aliasing" for text output. ([https://projects.blender.org/blender/blender/commit/82bfc41d0c 82bfc41d0c])
* Chinese, Japanese, and Korean punctuation characters now recognized in word selection. ([https://projects.blender.org/blender/blender/commit/6d64c6dcd7 6d64c6dcd7])
* Use multiplication symbol instead of letter "x" where appropriate. ([https://projects.blender.org/blender/blender/commit/9b4749e7c7 9b4749e7c7])

== Elements ==

* Consistent top-down content ordering in menus. Menus no longer reverse order if opening upward. ([https://projects.blender.org/blender/blender/commit/b122faf705 b122faf705])
* All rotational inputs now shown with maximum precision. ([https://projects.blender.org/blender/blender/commit/b34ece48f8 b34ece48f8])
* Tree-view UIs draw hierarchy lines to visually communicate the nesting levels better. ([https://projects.blender.org/blender/blender/commit/71273df2d5 71273df2d5])
[[File:Release notes 4.0 tree view hierarchy lines.png|thumb|center]]
* Many lists now highlight the currently-selected item. ([https://projects.blender.org/blender/blender/commit/9f4b28bba8 9f4b28bba8]).
* List items that are not multiple select are now shown as radio buttons. ([https://projects.blender.org/blender/blender/commit/6dd3c90185 6dd3c90185]).
* Resizing the toolbar no longer breaks snapping when dragged beyond the maximum available size. ([https://projects.blender.org/blender/blender/commit/248b322896 248b322896])
* Windows and Linux: Color Picker can now pick outside of Blender windows. ([https://projects.blender.org/blender/blender/commit/5741a5d433 5741a5d433], [https://projects.blender.org/blender/blender/commit/e5a0d11c4e e5a0d11c4e])
* The Color Picker size has been increased. ([https://projects.blender.org/blender/blender/commit/0c7496f74d 0c7496f74d])
[[File:PickerSize.png|600px|thumb|center]]
* Improvements to Color Ramp drawing. ([https://projects.blender.org/blender/blender/commit/8a3766e241 8a3766e241])
* Fix sidebar scrollbar overlapping category tabs when zooming. ([https://projects.blender.org/blender/blender/commit/080a00bda2 080a00bda2])
* Fixed scrollbars being highlighted while outside of their region. ([https://projects.blender.org/blender/blender/commit/4f6785774a 4f6785774a])
* Progress indicators now exposed to Python, including new ring version. ([https://projects.blender.org/blender/blender/commit/c6adafd8ef c6adafd8ef]).
[[File:UpdatingProgress.png|200px|thumb|center]]
* Allow transparency when editing text in widgets. ([https://projects.blender.org/blender/blender/commit/2ec2e52a90 2ec2e52a90])
* UI element outlines have changeable opacity, allowing for flat or minimalistic themes. ([https://projects.blender.org/blender/blender/commit/c033e434c5 c033e434c5])

== Window ==

* Window title improved order and now includes Blender version. ([https://projects.blender.org/blender/blender/commit/636f3697ee 636f3697ee]).
* New Window now works while having an area maximized. ([https://projects.blender.org/blender/blender/commit/bb31df1054 bb31df1054]).
* Save Incremental in File menu, to save the current .blend file with a numerically-incremented name. ([https://projects.blender.org/blender/blender/commit/a58e5ccdec a58e5ccdec])
* Small changes to the File and Edit menus. ([https://projects.blender.org/blender/blender/commit/347e4692de 347e4692de])

== Files and Asset Browsers ==

* File Browser side bar now showing Bookmarks, System, Volumes, Recent. ([https://projects.blender.org/blender/blender/commit/9659b2deda 9659b2deda])
* File and Asset Browser show a wait icon while previews are loading ([https://projects.blender.org/blender/blender/commit/4a3b6bfeac 4a3b6bfeac])
* Thumbnail views now allow any preview size from 16-256. ([https://projects.blender.org/blender/blender/commit/fa32379def fa32379def])
* Image previews in File Browser now have checkerboard background if transparent. ([https://projects.blender.org/blender/blender/commit/e9e12015ea e9e12015ea])
* File Browser now shows thumbnail previews for SVG images. ([https://projects.blender.org/blender/blender/commit/565436bf5f 565436bf5f])

<center>
{| class="transparent" style="text-align: center"
 |+  style="caption-side: bottom" | Transparent and SVG thumbnails.
 |valign=top|[[File:Transparent Thumbnail.png|300px|center]]
 |valign=top|[[File:SVGThumbnails.png|300px|center]]
 |}
</center>

== Splash ==

Splash screen changes to make it easier for users to bring previously saved settings into a new install. ([https://projects.blender.org/blender/blender/commit/13f5879e3c 13f5879e3c])

[[File:SplashScreen.png|600px|thumb|center]]

== Text Editor ==

* Support auto-closing brackets around the selection ([https://projects.blender.org/blender/blender/commit/96339fc3131789e6caa41f7ef01cc1edf5a38a28 96339fc313])
* Removed syntax highlighting support for LUA ([https://projects.blender.org/blender/blender/commit/95ca04dc207f8bbbdb18287223ba8fdabdd54511 95ca04dc20])

== Python Console ==

* Support cursor motion, selection & more text editing operations ([https://projects.blender.org/blender/blender/commit/18e07098ef25e7659e0c14e169a96b49c5a54cbf 18e07098ef])
* Support tab-stops instead of expanding to spaces ([https://projects.blender.org/blender/blender/commit/a5cd4975433db6ab28f13c49d14a071f3ca8d2c2 a5cd497543])

== Other Editors ==

* Preferences: layout and style tweaks ([https://projects.blender.org/blender/blender/commit/d0aa521ea8 d0aa521ea8])
* Timeline: Invalid caches now show with striped lines. ([https://projects.blender.org/blender/blender/commit/8d15783a7e 8d15783a7e]).
* Outliner: Drag & Drop now works between multiple windows ([https://projects.blender.org/blender/blender/commit/d102536b1a d102536b1a]).
* Outliner: Select Hierarchy now works with multiple selected objects ([https://projects.blender.org/blender/blender/commit/594dceda7f 594dceda7f]).
* Top Bar and Status Bar colors now exactly match their theme colors ([https://projects.blender.org/blender/blender/commit/d86d2a41e0 d86d2a41e0]).

== Windows Integration ==

* Blend File association can now be done for all users or just the current users in the preferences. Unassociate is now available as well. Support for side-by-side installations was improved. ([https://projects.blender.org/blender/blender/commit/9cf77efaa0 9cf77efaa0])
* Recent file lists are now per Blender version.
* Pinning a running Blender to the taskbar will properly pin the launcher.
* Multiple Blender version are available for Open With in Windows Explorer.
* Explorer "Quick Access" items added to File Browser System List ([https://projects.blender.org/blender/blender/commit/f1e7fe5492 f1e7fe5492]).

[[File:NewRegister1.png|600px|thumb|center]]
[[File:NewRegister2.png|600px|thumb|center]]

== Translations ==
The Catalan language is now one of six languages with '''complete''' coverage.
[[File:Catalan.png|400px|thumb|center]]