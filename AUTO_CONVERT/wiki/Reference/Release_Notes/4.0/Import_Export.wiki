= Import & Export =

== USD ==

* Skeleton and blend shape animation import through UsdSkel. ([https://projects.blender.org/blender/blender/pulls/110912 PR #110912])
* Generic mesh attribute import and export for meshes. ([https://projects.blender.org/blender/blender/pulls/109518 PR #109518])
* Light import and export improvements to follow USD conventions more closely. ([https://projects.blender.org/blender/blender/pulls/109795 PR #109795])
* Camera import and export improvements. ([https://projects.blender.org/blender/blender/pulls/112905 PR #112905])
* Export can now be extended with Python scripts through hooks. See the `USDHook` [https://docs.blender.org/api/4.0/bpy.types.USDHook.html API documentation] for details and example code.  ([https://projects.blender.org/blender/blender/pulls/108823 PR #108823])

==3DS==
==== Export Animation ====
* The 3ds exporter now got the property to export animation keyframes. ([https://projects.blender.org/blender/blender-addons/pulls/104613 PR #104613])
* Keyframe export is optional to avoid crashing any importers wich can not handle the keyframe section. ([https://projects.blender.org/blender/blender-addons/commit/585a682ef8 585a682ef8], [https://projects.blender.org/blender/blender-addons/commit/da9bb97bea da9bb97bea])
==== Export Hierarchy ====
* 3ds objects now got hierarchy chunks to preserve the object tree. ([https://projects.blender.org/blender/blender-addons/pulls/104682 PR #104682])
* The object tree can now be exported without the keyframe section needed. ([https://projects.blender.org/blender/blender-addons/commit/0d26a2a017 0d26a2a017])
==== Export Scale ====
* Global scale for exporting a 3ds can now be setted. ([https://projects.blender.org/blender/blender-addons/pulls/104767 PR #104767])
* Since many applications are using millimeters, this option is useful to convert the measure units. ([https://projects.blender.org/blender/blender-addons/commit/17f142e55d 17f142e55d])
==== Convert Measure ====
* The 3ds importer and exporter now got the ability to converts the unit measure for all metric and imperial units. ([https://projects.blender.org/blender/blender-addons/pulls/104769 PR #104769])
* Many 3ds files were exported in millimeter unit scale, this option converts too big meshes to Blender meter scale.  ([https://projects.blender.org/blender/blender-addons/commit/675987e938 675987e938])
==== Object Filter ====
* Both, the importer and the exporter got the new option to filter objects for import and export. ([https://projects.blender.org/blender/blender-addons/pulls/104778 PR #104778], [https://projects.blender.org/blender/blender-addons/pulls/104782 PR #104782])
==== Volume, fog, gradient and background bitmap ====
* Atmosphere settings, layerfog parameter and background image can now be imported and exported. ([https://projects.blender.org/blender/blender-addons/pulls/104795 PR #104795], [https://projects.blender.org/blender/blender-addons/commit/6fad9e9725 6fad9e9725])
* Gradient and fog chunks can now be imported and exported. ([https://projects.blender.org/blender/blender-addons/pulls/104811 PR #104811], [https://projects.blender.org/blender/blender-addons/commit/a53788722b a53788722b], [https://projects.blender.org/blender/blender-addons/commit/7cd6969b3e 7cd6969b3e])
==== Spotlight aspect and projector ====
* If a spotlight includes a gobo image, it will be exported in the light bitmap chunk and imported the same way.  ([https://projects.blender.org/blender/blender-addons/pulls/104813 PR #104813], [https://projects.blender.org/blender/blender-addons/commit/f1e443f119 f1e443f119])
* The x/y scale of a spotlight can now be exported in the aspect ratio chunk, the importer calculates the aspect back to x/y scale.  ([https://projects.blender.org/blender/blender-addons/pulls/104815 PR #104815], [https://projects.blender.org/blender/blender-addons/commit/7e2f96796a 7e2f96796a])
==== Cursor location ====
* The 3D cursor can now saved to a 3ds file and imported again. ([https://projects.blender.org/blender/blender-addons/pulls/104797 PR #104797], [https://projects.blender.org/blender/blender-addons/commit/75ea7633f6 75ea7633f6])
==== New Principled BSDF material support ====
* Moved specular texture to specular tint. ([https://projects.blender.org/blender/blender-addons/pulls/104918 PR #104918], [https://projects.blender.org/blender/blender-addons/commit/fef728a568 fef728a568])
* Added transmission and coat weight parameters. ([https://projects.blender.org/blender/blender-addons/commit/a5d3364c33 a5d3364c33], [https://projects.blender.org/blender/blender-addons/commit/d82ce1770f d82ce1770f], [https://projects.blender.org/blender/blender-addons/commit/9d57b190b8 9d57b190b8], [https://projects.blender.org/blender/blender-addons/commit/8f6d8d9fc9 8f6d8d9fc9])
* Added tint colors for coat and sheen. ([https://projects.blender.org/blender/blender-addons/commit/ec069c3b6a ec069c3b6a])

==FBX==
* FBX binary file reading and parsing speed has been improved. ([https://projects.blender.org/blender/blender-addons/commit/890e51e769 890e51e769], [https://projects.blender.org/blender/blender-addons/commit/a600aeb2e2 a600aeb2e2])
* Animation import performance was made faster. ([https://projects.blender.org/blender/blender-addons/commit/abab1c93437322bbf882079a43323e353a5c5b5f abab1c9343], [https://projects.blender.org/blender/blender-addons/commit/e7b19628938d57498b5d9c23bef2bd14610f41e7 e7b1962893])
* Animation export performance was made faster. ([https://projects.blender.org/blender/blender-addons/commit/73c65b9a44ff6a5182d433bc991359a7a2a43676 73c65b9a44], [https://projects.blender.org/blender/blender-addons/commit/461e0c3e1e0a8748106189fde3c7daf61c9fb46b 461e0c3e1e])
* Vertex Group export performance was made faster. ([https://projects.blender.org/blender/blender-addons/commit/63d4898e1d9ebf59abd86e7b4b02ce29b273dc7f 63d4898e1d])
* Armature data custom properties can now be imported and exported. ([https://projects.blender.org/blender/blender-addons/commit/2b8d9bf2b8f229adb150fdbcfd901fe6fad3c43d 2b8d9bf2b8]).
* Shape keys can now be exported when Triangulate Faces is enabled or when Objects have Object-linked materials. ([https://projects.blender.org/blender/blender-addons/commit/bc801d7b1dad4be446435e0cab4d3a80e6bb1d04 bc801d7b1d])

==OBJ and PLY==
The OBJ and PLY I/O add-ons have been removed. Importing and exporting this format is now implemented natively in Blender, with significantly better performance. ([https://projects.blender.org/blender/blender-addons/issues/104504 #104504], [https://projects.blender.org/blender/blender-addons/commit/67a4aab0da2f3 67a4aab0da], [https://projects.blender.org/blender/blender-addons/issues/104503 #104503], [https://projects.blender.org/blender/blender-addons/commit/67a4aab0da2f3 67a4aab0da]). 

== Collada ==

* Armatures exported to Collada now include the armature's bone collections, including their visibility ([https://projects.blender.org/blender/blender/commit/83306754d48 83306754d4]). More info about [[Reference/Release_Notes/4.0/Animation_Rigging#Bone_Collections|bone collections]].

== glTF 2.0 ==

Find the changelog in the [[Reference/Release Notes/4.0/Add-ons|Add-ons]] section.