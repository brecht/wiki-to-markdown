= Writing Style =

== Features ==
* Add link to the git commit at the end of each feature:<br/> <code><nowiki>({{GitCommit|<commit-hash>}})</nowiki></code> <br/> Replace `<commit-hash>` with the hash of the commit, e.g. `d0fabb318eff`.<br/><br/> Not ''every'' commit relating to the feature needs to be listed. The main commits introducing it are sufficient.
* If possible, link to the manual entry where to read more about the feature.
** Make sure to use the right version `https://docs.blender.org/manual/en/3.2/`

== Text ==
* Do not use abbreviations for features, editors, properties, etc.
  Example: Use "Grease Pencil" instead of "gpencil"

== Images and Videos == 
* Use the default theme
* Keep videos short
* Do not use the Blender logo for demonstrations
* Do not write text nor add arrows to the screencaptures, explain in the caption
* Remove unnecessary clutter
** Capture only the relevant editor(s)
** Hide irrelevant sidebars or panels