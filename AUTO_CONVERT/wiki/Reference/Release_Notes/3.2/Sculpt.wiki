= Sculpt, Paint, Texture =

== Color Attributes ==

*Vertex colors have been refactored into generic color attributes ([https://projects.blender.org/blender/blender/commit/eae36be372a6b16ee3e76eff0485a47da4f3c230  rBeae36be3]).
[[File:ColorAttributes.png|600px|thumb|Color Attributes in UI|none]]
*Color attributes support both 8-bit byte and 32-bit floating point colors and can be stored in Vertex or Face Corner domains. The default is using Vertex -> Color setting, to reduce file sizes, RAM usage and improve color quality.
[[File:CreateAttribute.png|600px|thumb|Creating a new Color Attribute|none]]
*"Vertex color" was renamed to "Color Attribute" in various places ([https://projects.blender.org/blender/blender/commit/eae36be372a6b16ee3e76eff0485a47da4f3c230  rBeae36be3],[https://projects.blender.org/blender/blender/commit/07b10638aa0a6a1d91740c2aed749a69b878b9cd  rB07b10638]). 

== Painting in Sculpt Mode ==

*Color attributes can be painted inside vertex paint mode or in sculpt mode using the new sculpt paint tools ([https://projects.blender.org/blender/blender/commit/eae36be372a6b16ee3e76eff0485a47da4f3c230  rBeae36be3]).
**Painting tools in Sculpt mode have advantages like masking options, face sets/masks support and faster performance. They will serve as a base for future painting improvements and modes.
**Vertex paint mode has been ported to support painting color attributes. ([https://projects.blender.org/blender/blender/commit/575ade22d4de472ccf9e7d2dc1ffca37416c58f6  rB575ade22]).
*Using painting tools in solid view switches viewport shading color to “Attribute” ([https://projects.blender.org/blender/blender/commit/90042b7d796608cf680620041785bfa432975d48  rB90042b7d]).
**This ensures that the color attributes are visible when painting, without making the visualisation mandatory in sculpt mode.
*Paint Brush - default brush for painting color attributes.
**Holding Shift key switches Paint brush to Blur.
**Can modulate wet paint effect for color smudging.
**Includes extra sliders to control the tip roundness and width
**Flow and Density also control how much color is applied during the stroke
[[File:PaintBrush.mp4|600px|thumb|Showcase of different brush presets from the demo file|none]]
<br>
*Smear Brush - brush for smearing color attributes
**Has various deformation modes.
[[File:SmearBrush.mp4|600px|thumb|Smear brush deformation modes|none]]
<br>
*Color Filter - tool for modifying existing colors. Filter operations include:
**Fill
**Hue
**Saturation
**Value
**Brightness
**Contrast
**Smooth
**Red
**Green
**Blue
[[File:ColorFilterVideo.mp4|600px|thumb|Color filter modes|none]]
<br>
*Mask By Color - tool for creating masks from the active color attribute
[[File:MaskByColorVideo.mp4|600px|thumb|Making a mask from a color|none]]

== Voxel Remesher ==
*Edit Voxel Size operator now displays units properly ([https://projects.blender.org/blender/blender/commit/b226236b017f5f84c5ce029689b0707f639299dc b226236b01]).
*Voxel Remesh default settings have been changed ([https://projects.blender.org/blender/blender/commit/d1418dd151b1b446fd97108959ef0ba5c0404424 d1418dd151]).
**Voxel Remesher supports color attributes.
**Color attributes, face sets and masks are now being preserved.
**'Fix poles'  is for now disabled to drastically increase remeshing speed.