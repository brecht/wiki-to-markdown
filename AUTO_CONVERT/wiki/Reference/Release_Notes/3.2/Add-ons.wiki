= Add-ons =

== Rigify ==

* Adjusted the switchable parent rig so that disabling Local Location on IK controls aligns location channels to world as intended. Added an IK Local Location option to limbs (on by default for backward compatibility) and disabled it in metarigs. ([https://projects.blender.org/blender/blender-addons/commit/7120e9c9e087  rBA7120e9c], [https://projects.blender.org/blender/blender-addons/commit/c268b58c247  rBAc268b58])

== FBX I/O ==

* Support for camera DoF focus distance was added, including animation, for both import ([https://projects.blender.org/blender/blender-addons/commit/5eff3aa881bf  rBA5eff3aa]) and export ([https://projects.blender.org/blender/blender-addons/commit/fba4f07bc695  rBAfba4f07]).
* Imported animations of rigs now have their FCurves properly grouped by bones ([https://projects.blender.org/blender/blender-addons/commit/a2c3cfdd764b  rBAa2c3cfd]).
* An option to triangulate geometry has been added to the exporter ([https://projects.blender.org/blender/blender-addons/commit/d700a6888ecd  rBAd700a68]).
* An option to only export visible objects has been added to the exporter ([https://projects.blender.org/blender/blender-addons/commit/4075cdbdc751  rBA4075cdb]).

== BHV I/O ==

* Imported animations of rigs now have their FCurves properly grouped by bones ([https://projects.blender.org/blender/blender-addons/commit/a2c3cfdd764b  rBAa2c3cfd]).

== glTF 2.0 I/O ==

=== Exporter ===

* Huge refactoring (better hierarchy / collection / instances management) ([https://projects.blender.org/blender/blender-addons/commit/782f8585f4cc  rBA782f858], [https://projects.blender.org/blender/blender-addons/commit/1d5c8b54ee99  rBA1d5c8b5], [https://projects.blender.org/blender/blender-addons/commit/64f462358522  rBA64f4623], [https://projects.blender.org/blender/blender-addons/commit/dbb9b62991b3  rBAdbb9b62])
* Use rest pose for armature when 'use current frame' is off ([https://projects.blender.org/blender/blender-addons/commit/d82581163628  rBAd825811])
* Add glTF settings node in shader menu ([https://projects.blender.org/blender/blender-addons/commit/5838e260366a  rBA5838e26])
* Export armature without skined mesh as skin in glTF files ([https://projects.blender.org/blender/blender-addons/commit/5a557e72fc1e  rBA5a557e7])
* Normalize skin weights also for all influence mode ([https://projects.blender.org/blender/blender-addons/commit/96eb14698456  rBA96eb146])
* Manage active UVMap correclty, when there is no UVMap used in node tree ([https://projects.blender.org/blender/blender-addons/commit/3a299965833c  rBA3a29996])
* Manage skinning when some vertices are not weights at all ([https://projects.blender.org/blender/blender-addons/commit/564fdcdf7159  rBA564fdcd])
* Armature exports all actions ([https://projects.blender.org/blender/blender-addons/commit/9818afc829a0  rBA9818afc], [https://projects.blender.org/blender/blender-addons/commit/787ea78f7fa6  rBA787ea78], [https://projects.blender.org/blender/blender-addons/commit/a0ce684afe3e  rBAa0ce684])
* Add various hooks ([https://projects.blender.org/blender/blender-addons/commit/43477e00ce44  rBA43477e0], [https://projects.blender.org/blender/blender-addons/commit/8653db380888  rBA8653db3], [https://projects.blender.org/blender/blender-addons/commit/fef8f1ffdb1f  rBAfef8f1f])
* Remove back compatibility of export_selection -> use_selection ([https://projects.blender.org/blender/blender-addons/commit/4d8b2dc95f98  rBA4d8b2dc])
* Weight min threshold for skinning ([https://projects.blender.org/blender/blender-addons/commit/c157125ace85  rBAc157125])
* Option to optimize animation size off by default ([https://projects.blender.org/blender/blender-addons/commit/22c1eb5c3d50  rBA22c1eb5])
* Manage use_nla option to avoid exporting merged animations ([https://projects.blender.org/blender/blender-addons/commit/6e430d231788  rBA6e430d2])
* Performance : better way to detect weird images ([https://projects.blender.org/blender/blender-addons/commit/cbb4b8c121a5  rBAcbb4b8c])
* Fix PBR values when emission node is used ([https://projects.blender.org/blender/blender-addons/commit/fbb5111e1551  rBAfbb5111])
* Fix transmission export ([https://projects.blender.org/blender/blender-addons/commit/4c1df0f54ce0  rBA4c1df0f])
* Fix bone export when Yup is off ([https://projects.blender.org/blender/blender-addons/commit/dfe74f0a1b1b  rBAdfe74f0])
* Fix active scene export + option to use only active scene ([https://projects.blender.org/blender/blender-addons/commit/1bffd880b8f0  rBA1bffd88])
* No texture export when original can't be retrieved ([https://projects.blender.org/blender/blender-addons/commit/adf10c631570  rBAadf10c6])
* Use color attribute API instead of vertex color ([https://projects.blender.org/blender/blender-addons/commit/485b4ac5691c  rBA485b4ac])

=== Importer ===

* Fix when glTF file has no scene ([https://projects.blender.org/blender/blender-addons/commit/b294d4c76654  rBAb294d4c])
* More animation hooks for user extensions ([https://projects.blender.org/blender/blender-addons/commit/2183b1d29571  rBA2183b1d])

== Dynamic Brush Menus ==

* Fix and consistently support symmetry menus for all painting modes ([https://projects.blender.org/blender/blender-addons/commit/c9fba919c2e1  rBAc9fba91]).
* Fix brushes menu not displaying fully in sculpt mode ([https://projects.blender.org/blender/blender-addons/commit/4d53ec76a3d8  rBA4d53ec7]).


== Collection Manager ==

* An option to enable/disable the QCD 3D Viewport header widget has been added to the preferences ([https://projects.blender.org/blender/blender-addons/commit/a65df677f707  rBAa65df67]).

== Node Wrangler ==

* Node Wrangler now handles attributes ([https://projects.blender.org/blender/blender-addons/commit/dde5915336d503e12b43af6611c6947767d3e355  rBAdde5915]).