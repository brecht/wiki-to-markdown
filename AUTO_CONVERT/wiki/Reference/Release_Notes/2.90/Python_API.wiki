= Blender 2.90: Python API =

== Compatibility ==

==== Operator Search ====

With the new operator search that only searches through menus, add-ons that expose operators only through search need to be updated. In general it is good practice to always expose operators through menus so users can find them.

For more obscure operators that are for example intended mainly for developers, we recommend adding them in the `TOPBAR_MT_app_system` menu. This is accessible through the System menu under the Blender icon in the top bar.

==== Other Changes ====

* Libraries: [https://docs.blender.org/api/master/bpy.types.BlendDataLibraries.html?highlight=write#bpy.types.BlendDataLibraries.BlendDataLibraries.write `bpy.data.libraries.write`] `relative_remap` boolean argument has been replaced by `path_remap` enumeration which can select different kinds of path remapping on write.
* `BevelModifier.use_custom_profile` property is now part of a new a `profile_type` enum. ([https://projects.blender.org/blender/blender/commit/6703c7f7f1f6 6703c7f7f1]) This applies to the bevel tool as well.
* The bevel operator's `vertex_only` property was changed to a new `affect` enum, with `VERTICES` and `EDGES` options. ([https://projects.blender.org/blender/blender/commit/4aa2a5481cec 4aa2a5481c])
* The modifier apply operator's `apply_as` argument was moved to a new operator `modifier_apply_as_shapekey`. The new operator can also save to a shapekey without removing the modifier ([https://projects.blender.org/blender/blender/commit/01c8aa12a177 01c8aa12a1])

== User Interface ==

* Columns and rows can now group together related settings with a heading. For example:<br/> [[File:Release notes layout headings.png|right]] <source lang="python">
col = layout.column(heading="Tooltips")
col.prop(view, "show_tooltips")
col.prop(view, "show_tooltips_python")
</source> The layout system will try to insert the heading to the left column of a property split layout, but may insert the heading as usual column or row item as fallback.
* [https://docs.blender.org/api/master/bpy.types.UILayout.html#bpy.types.UILayout.prop_decorator `UILayout.prop_decorator(data, property, index=-1)`] got added to allow manual insertion of decorators (to set keyframes or indicate other animation states for the property). There must be an item with the same parameters within the root layout for this to work.
* Checkboxes respect [https://docs.blender.org/api/master/bpy.types.UILayout.html#bpy.types.UILayout.use_property_split `UILayout.use_property_split`] now. With that some layout conventions changed:
** Related checkboxes should be grouped together in a column with a heading (see above).
** In cases where a checkbox controls the availability of a single property, the checkbox should be in a row together with the property:<br/>[[File:Release notes properties checkbox subpanel.png|400px]] <br/>Decorator items have to be manually inserted with some boilerplate code, which looks like this:<br/><source lang="python">
row = layout.row(align=True, heading="Auto Smooth")
row.use_property_decorate = False # Don't automatically insert decorators.
sub = row.row(align=True)
sub.prop(mesh, "use_auto_smooth", text="")
subsub = sub.row(align=True)
subsub.active = mesh.use_auto_smooth and not mesh.has_custom_normals
subsub.prop(mesh, "auto_smooth_angle", text="")
row.prop_decorator(mesh, "auto_smooth_angle")
</source> It is important that the decorator is inserted to a row-layout that is the parent of the row-layout for the checkbox and the actual property.
** Checkboxes with property splitting often look bad if placed alone in a layout, or within flow layouts. In these cases [https://docs.blender.org/api/master/bpy.types.UILayout.html#bpy.types.UILayout.use_property_split `UILayout.use_property_split`] should be disabled or a fixed, non-flow layout be used.
* [https://docs.blender.org/api/master/bpy.types.UILayout.html#bpy.types.UILayout.menu `UILayout.menu()`], [https://docs.blender.org/api/master/bpy.types.UILayout.html#bpy.types.UILayout.operator_menu_enum `UILayout.operator_menu_enum()`] and [https://docs.blender.org/api/master/bpy.types.UILayout.html#bpy.types.UILayout.prop_menu_enum `UILayout.prop_menu_enum()`] respect [https://docs.blender.org/api/master/bpy.types.UILayout.html#bpy.types.UILayout.use_property_split `UILayout.use_property_split`] now.

== Additions ==

* In the event a Python script causes Blender to crash, Blender's crash log will include a Python stack-trace so Python developers can find what caused the error ([https://projects.blender.org/blender/blender/commit/e9c4325515aed9cb3a35183d4093cda2b6bffd9f e9c4325515]).
* Mathutils: element-wise multiplication for vectors matrices and quaternions ([https://projects.blender.org/blender/blender/commit/fa7ace221584be4dfd8c897a1c6b74209fed4415 fa7ace2215]).
* `constraints.copy()` methods for objects and bones to duplicate a constraint with all its settings, including from other objects. ([https://projects.blender.org/blender/blender/commit/64a584b38a7 64a584b38a])
* `Screen.is_scrubbing`, indicating when the user is scrubbing through time in the timeline, dopesheet, graph editor, etc. ([https://projects.blender.org/blender/blender/commit/2be7a11e4331 2be7a11e43])
* `Sequences.new_movie()` support for creating movie strips with missing files, consistent with image and sound strips. `MovieSequence.reload_if_needed()` to try and reload movie strips, in case the missing file has appeared. This makes it possible to create a movie strip for video files that are synced between computers via some network connection. ([https://projects.blender.org/blender/blender/commit/b9f565881e15 b9f565881e])
* Drivers get a `depsgraph` variable in their local scope. See the [https://wiki.blender.org/wiki/Reference/Release_Notes/2.90/Animation-Rigging#Dependency_Graph_passed_to_Drivers Animation-Rigging] release notes for more details.
* New `bl_math` module with `lerp`, `clamp` and `smoothstep` functions (inspired by GLSL `mix`, `clamp` and `smoothstep`), which are also made available to drivers. ([https://projects.blender.org/blender/blender/commit/f8cc01595d f8cc01595d])
* Option to use OpenGL context in `RenderEngine.render`. ([https://projects.blender.org/blender/blender/commit/52543be9a6a 52543be9a6])