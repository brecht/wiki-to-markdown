= Blender 2.90: Grease Pencil =

== User Interface ==
* Onion skinning support for annotations in the video sequencer.  ([https://projects.blender.org/blender/blender/commit/fda754145ae5 fda754145a])
* Layer Mask and Use Lights properties are animatable. ([https://projects.blender.org/blender/blender/commit/221a7002a981 221a7002a9])
* New Material selector in Context menu and new Material menu using `U` key. ([https://projects.blender.org/blender/blender/commit/6a7e9f2b7647 6a7e9f2b76])

[[File:Material context menu.png|600px|thumb|center|Material selector context menu]]

* New option to put Canvas Grid in front of any object. ([https://projects.blender.org/blender/blender/commit/b369d46eb775 b369d46eb7])
* First keyframe is displayed only if the current frame is equals or greater ([https://projects.blender.org/blender/blender/commit/11ba9eec7017 11ba9eec70])

`This change can produce changes in old animations. Review your drawings and set the keyframe number to be displayed at the right time. Usually, The fix is as simple as move or duplicate the first keyframe to frame 1.` 

* Keyframe type now is accessible using python. ([https://projects.blender.org/blender/blender/commit/326db1b7cd4e 326db1b7cd])
* Default Vertex Color mode changed to `Stroke and Fill`. ([https://projects.blender.org/blender/blender/commit/e079bf6996fc e079bf6996])
* Added missing UV Fill property to RNA. ([https://projects.blender.org/blender/blender/commit/f382109f3885 f382109f38])

== Operators ==
* Reproject stroke now allows to keep original stroke.  ([https://projects.blender.org/blender/blender/commit/37d68871b7c5 37d68871b7])
* New Bevel Depth and Resolution parameters converting strokes to curves. ([https://projects.blender.org/blender/blender/commit/146473f08335 146473f083])
* New Convert mesh and Bake animation to grease pencil. ([https://projects.blender.org/blender/blender/commit/bc7a4b126afb bc7a4b126a])

== Tools ==
* Annotate Line tool now supports different types of arrows.  ([https://projects.blender.org/blender/blender/commit/668dd146f647 668dd146f6])

[[File:Annotate arrows.png|600px|thumb|center|Annotate arrows types]]

* Annotate tool now supports mouse stabilizer.  ([https://projects.blender.org/blender/blender/commit/9a7c4e2d444f 9a7c4e2d44])
* New Brush options and curves to randomize effects. ([https://projects.blender.org/blender/blender/commit/b571516237a9 b571516237])
* When Using the Eraser tool a new frame is created if Additive Drawing is enabled. ([https://projects.blender.org/blender/blender/commit/29afadcb15f7 29afadcb15])
* Improved selection of strokes in the fill area ([https://projects.blender.org/blender/blender/commit/244ed645e08e 244ed645e0])

[[File:GifSelectAfter.gif|600px|thumb|center|Fill Area Select]]

* Improved fill paint in vertex color mode and Tint tool. ([https://projects.blender.org/blender/blender/commit/abeda01ac6da abeda01ac6])

== Modifiers and VFX ==
* New Texture Mapping modifier to manipulate UV data.  ([https://projects.blender.org/blender/blender/commit/a39a6517af66 a39a6517af])
* New Restrict Visible Points Build parameter to define the visibility of stroke.  ([https://projects.blender.org/blender/blender/commit/a1593fa05baf a1593fa05b])
* New Antialiasing parameter for Pixel effect. ([https://projects.blender.org/blender/blender/commit/11a390e85e2b 11a390e85e])