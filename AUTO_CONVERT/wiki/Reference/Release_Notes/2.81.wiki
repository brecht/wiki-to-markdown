= Blender 2.81 Release Notes =

Blender 2.81 was released on November 21, 2019.

Check out the final [https://www.blender.org/download/releases/2-81/ Release Notes on blender.org]

<br style="clear:both;">

== [[Reference/Release Notes/2.81/a|2.81a Corrective Release]] ==

This corrective release does not include new features and only fixes a few serious regressions introduced in 2.81 release.

== [[Reference/Release Notes/2.81/Cycles|Cycles]] ==

[[File:Cycles_displacement_cracks.jpg|thumb|400px]]

New shaders for texturing, denoising with OpenImageDenoise, and NVIDIA RTX support.

<br style="clear:both;">

== [[Reference/Release Notes/2.81/Eevee|Eevee]] ==

[[File:Eevee2.81_transparent_bsdf1.png|thumb|400px]]

Shadows, transparency and bump mapping redesigned for easier setup and better quality.

<br style="clear:both;">

== [[Reference/Release Notes/2.81/Viewport|Viewport]] ==

[[File:Demonstrating_Multi-layered_Matcaps_(Image_from_Pablo_Dobarro).png|thumb|400px]]

New options for look development with Cycles and Eevee.

<br style="clear:both;">

== [[Reference/Release Notes/2.81/Grease_Pencil|Grease Pencil]] ==

[[File:New_Brushes.png|thumb|400px]]

User interface, tools, operators, modifiers, new brushes and presets and material self overlap.

<br style="clear:both;">

== [[Reference/Release Notes/2.81/Sculpt|Sculpt & Retopology]] ==

New sculpt tools, poly build tool, voxel remesh and quad remesh. 

== [[Reference/Release Notes/2.81/Transform|Transform & Snapping]] ==

[[File:Snap_Perpendicular.png|thumb|200px]]

Transform origins, new snapping, mirroring and auto merge options.

<br style="clear:both;">

== [[Reference/Release Notes/2.81/UI|User Interface]] ==

[[File:2_81_file_browser_vertical_list.png|thumb|400px]]

Outliner improvements, new file browser and batch rename.

<br style="clear:both;">

== [[Reference/Release Notes/2.81/Library_Overrides|Library Overrides]] ==

A new system to replace proxies, to make local overrides to linked characters and other data types.

== [[Reference/Release Notes/2.81/Rigging|Animation & Rigging]] ==

Finer control over rotations and scale in bones, constraints and drivers.

== [[Reference/Release Notes/2.81/More_Features|More Features]] ==

Alembic, audio & video output, library management and video sequencer editor.

== [[Reference/Release Notes/2.81/Python API|Python API]] ==

Python version upgrade, dynamic tooltip for operators, new handlers and other API changes.

== [[Reference/Release Notes/2.81/Add-ons|Add-ons]] ==

Enabled add-ons only, glTF, FBX, import images as planes, blenderkit, rigify among others.

<br style="clear:both;">