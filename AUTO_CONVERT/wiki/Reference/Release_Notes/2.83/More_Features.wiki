= Blender 2.83: More Features =

== Undo ==

Performance of undo in Object and Pose mode was significantly improved, particularly for scenes with many objects. Further optimizations are planned for future releases.

This optimization is enabled by default. If there are stability issues, it is possible to use the slower undo system by enabling Developer Extras in the preferences and changing the option in the Experimental section. See [https://developer.blender.org/T60695  the design task] for more details.

== 3D Viewport ==

* Improve selection with many small objects in the 3D viewport. ([https://projects.blender.org/blender/blender/commit/3685347b4172 3685347b41])
* Revamped color management, with compositing now performed in linear space for overlays and grease pencil for more accurate results.

== Video Sequencer ==

* Disk Cache to store cached frames on disk instead of only in memory. It can be enabled in Preferences > System > Video Sequencer. ([https://projects.blender.org/blender/blender/commit/348d2fa09e0c 348d2fa09e])
* Revamped sequence strip drawing ([https://projects.blender.org/blender/blender/commit/271231f58ee3 271231f58e])
* Show f-curves for opacity and volume values on the strips ([https://projects.blender.org/blender/blender/commit/d0d20de183f1 d0d20de183])
* Adjust Last Operation panel ([https://projects.blender.org/blender/blender/commit/a4cf2cf2decc a4cf2cf2de]).
* Toolbar and color sample tool ([https://projects.blender.org/blender/blender/commit/6a49161c8c60 6a49161c8c]) ([https://projects.blender.org/blender/blender/commit/68ba6378b5b1 68ba6378b5])
* Option to select handles with box selection ([https://projects.blender.org/blender/blender/commit/5314161491d4 5314161491])

== Import & Export ==

=== Universal Scene Description ===

* Metaballs can now also be exported to USD ([https://projects.blender.org/blender/blender/commit/4b2b5fe4b8d3b 4b2b5fe4b8]).

=== Alembic ===

* Improved import and export of camera transformations. Children of cameras are now handled properly ([https://projects.blender.org/blender/blender/commit/7c5a44c71f 7c5a44c71f]).
* Animated UV coordinates are now exported and imported correctly. ([https://projects.blender.org/blender/blender/commit/65574463fa9 65574463fa]) ([https://projects.blender.org/blender/blender/commit/94cbfb71b 94cbfb71b])

=== glTF ===

* Exporter
** Manage collection / collection instance / proxy ([https://projects.blender.org/blender/blender-addons/commit/191bcee579b3  rBA191bcee])
** Improve texture export performance ([https://projects.blender.org/blender/blender-addons/commit/dfadb306b0e9  rBAdfadb30])
** KHR_materials_clearcoat export ([https://projects.blender.org/blender/blender-addons/commit/65bad4212eb9  rBA65bad42])
** Better image option ([https://projects.blender.org/blender/blender-addons/commit/872e3e6afac9  rBA872e3e6])
** Don't take mute strip into account ([https://projects.blender.org/blender/blender-addons/commit/22424950a3f6  rBA2242495])
** Fix bone cache issue ([https://projects.blender.org/blender/blender-addons/commit/cc3e0cce4a53  rBAcc3e0cc], [https://projects.blender.org/blender/blender-addons/commit/ffd8a687247d  rBAffd8a68])
** Fix normals for skinned meshes ([https://projects.blender.org/blender/blender-addons/commit/ec5ca6a6cb54  rBAec5ca6a])
** Manage user extension at glTF level ([https://projects.blender.org/blender/blender-addons/commit/1ae12e6f5d3c  rBA1ae12e6])
** Animation general improvment ([https://projects.blender.org/blender/blender-addons/commit/e5bde5ec2e68  rBAe5bde5e], [https://projects.blender.org/blender/blender-addons/commit/f3683cf7bf74  rBAf3683cf], [https://projects.blender.org/blender/blender-addons/commit/2425a37ae57d  rBA2425a37], [https://projects.blender.org/blender/blender-addons/commit/4ef5cd10f689  rBA4ef5cd1], [https://projects.blender.org/blender/blender-addons/commit/68d5831729bf  rBA68d5831], [https://projects.blender.org/blender/blender-addons/commit/b72a6c057999  rBAb72a6c0])
** Ability to undo an export ([https://projects.blender.org/blender/blender-addons/commit/d52854d420bd  rBAd52854d])
** Export extra for bones ([https://projects.blender.org/blender/blender-addons/commit/15d56ea627d9  rBA15d56ea])
** Don't combine image names if they're all from the same file ([https://projects.blender.org/blender/blender-addons/commit/0a361e787dbd  rBA0a361e7])
** Normalized normals for skinned mesh ([https://projects.blender.org/blender/blender-addons/commit/b42aecdca411  rBAb42aecd])
** Fix Vertex Group export in some cases ([https://projects.blender.org/blender/blender-addons/commit/dc4c83cffc6b  rBAdc4c83c])
** fix bug when apply modifier + disable skinning ([https://projects.blender.org/blender/blender-addons/commit/69d4daee6ffe  rBA69d4dae])
** Less naive file format detection for textures ([https://projects.blender.org/blender/blender-addons/commit/7a3fdf08f3fe  rBA7a3fdf0])
** Fix export some animation interpolations ([https://projects.blender.org/blender/blender-addons/commit/18a0f95a8482  rBA18a0f95])
* Importer
** Fix skinning & bone hierarchy ([https://projects.blender.org/blender/blender-addons/commit/75855d723895  rBA75855d7], [https://projects.blender.org/blender/blender-addons/commit/09bbef319f5e  rBA09bbef3], [https://projects.blender.org/blender/blender-addons/commit/39e2149b6c62  rBA39e2149], [https://projects.blender.org/blender/blender-addons/commit/e4269fc795cf  rBAe4269fc])
** Better bone rotation management & bind pose ([https://projects.blender.org/blender/blender-addons/commit/6e7dfdd8a91f  rBA6e7dfdd], [https://projects.blender.org/blender/blender-addons/commit/96a4166c8de2  rBA96a4166], [https://projects.blender.org/blender/blender-addons/commit/38531bbfa567  rBA38531bb])
** Yfov  near / far camera import ([https://projects.blender.org/blender/blender-addons/commit/d8aa24a9321e  rBAd8aa24a], [https://projects.blender.org/blender/blender-addons/commit/43148f17496c  rBA43148f1])
** Rewrite material import ([https://projects.blender.org/blender/blender-addons/commit/2caaf64ab10b  rBA2caaf64])
** Manage morph weights at node level ([https://projects.blender.org/blender/blender-addons/commit/ee61a3a692d3  rBAee61a3a])
** Simplify/cleanup image import ([https://projects.blender.org/blender/blender-addons/commit/e88c7ee2a7c0  rBAe88c7ee])
** Omit texture mapping & UVMap node when not needed ([https://projects.blender.org/blender/blender-addons/commit/b629ab427c4e  rBAb629ab4])
** Use emission/alpha sockets on Principled node ([https://projects.blender.org/blender/blender-addons/commit/b966913caab3  rBAb966913])
** Manage texture wrap modes ([https://projects.blender.org/blender/blender-addons/commit/d0dee32d2b5f  rBAd0dee32])
** Better retrieve camera & lights names ([https://projects.blender.org/blender/blender-addons/commit/fd6bfbb44af4  rBAfd6bfbb])
** Performance improvement importing meshes & normals ([https://projects.blender.org/blender/blender-addons/commit/f5f442a7a665  rBAf5f442a])
** Rename option from export_selected to use_selection ([https://projects.blender.org/blender/blender-addons/commit/120d313b842e  rBA120d313], [https://projects.blender.org/blender/blender-addons/commit/a35d66c1337a  rBAa35d66c])
** Bone & Animation code cleanup & checks ([https://projects.blender.org/blender/blender-addons/commit/b4fc9fbcf4e2  rBAb4fc9fb], [https://projects.blender.org/blender/blender-addons/commit/9920e00e5fbe  rBA9920e00], [https://projects.blender.org/blender/blender-addons/commit/0e2a855c40c6  rBA0e2a855], [https://projects.blender.org/blender/blender-addons/commit/8db785ea740b  rBA8db785e], [https://projects.blender.org/blender/blender-addons/commit/f4e12a20f5d0  rBAf4e12a2])
** Add extension to tmp file generated when importing textures ([https://projects.blender.org/blender/blender-addons/commit/ea2965438756  rBAea29654])
** Improve node names ([https://projects.blender.org/blender/blender-addons/commit/02ca41d48ea3  rBA02ca41d])
** Use friendly filenames for temp image files ([https://projects.blender.org/blender/blender-addons/commit/45afacf7a630  rBA45afacf])
** Better material names when using color vertex ([https://projects.blender.org/blender/blender-addons/commit/0650f599aa17  rBA0650f59])
** Fix bug on windows, use absolute paty ([https://projects.blender.org/blender/blender-addons/commit/520b6af2be38  rBA520b6af])

== Miscellaneous ==

* View Layers: when adding a new view layer, it is now possible to copy settings from currently active one, or to create an 'empty' one (where all collections are disabled) ([https://projects.blender.org/blender/blender/commit/d1972e50cbef d1972e50cb]).
* Windows: support for high resolution tablet pen events when using Windows Ink, so strokes more accurately match the pen motion. This was previously only supported on Linux. ([https://projects.blender.org/blender/blender/commit/d571d61 d571d61])
* Freestyle: option to render freestyle to a separate pass.