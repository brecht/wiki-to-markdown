== Linux ==

* The Wayland windowing environment is now supported in addition to X11, see the [https://docs.blender.org/manual/en/3.4/getting_started/installing/linux_windowing_environment.html user-manual] & this [https://code.blender.org/2022/10/wayland-support-on-linux blog-post] for details.

== Windows ==
* Allow Windows Explorer thumbnails of Blend files at maximum resolution. ([https://projects.blender.org/blender/blender/commit/ff27b68f41ce ff27b68f41]).