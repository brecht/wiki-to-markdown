= Blender 2.93: Grease Pencil =

== LineArt Modifier ==

New Line Art modifier is now included to provide functionality of generating stylized lines from geometries in the scene. ([https://projects.blender.org/blender/blender/commit/3e87d8a4315d 3e87d8a431])

[[File:Cutaway 0401 bw 3k.png|800px|center|Line art demonstration render of a 727 cut-away drawing.]]

* Produce accurate occlusion result from any geometry.
* Intersection lines can be generated and selected separately for styling.
* Chaining allows strokes to be connected thus allow smooth display of grease pencil styles and textures.
* Vertex weight in the mesh can be transferred into grease pencil to allow further style control of generated strokes.
* Strokes can be baked to allow further edits in the viewport.

== Import/Export ==
* New operators to Import SVG to grease pencil objects and export grease pencil objects in SVG and PDF format. ([https://projects.blender.org/blender/blender/commit/a8a92cd15a52 a8a92cd15a])
* SVG Importer use vertex color for colors.

<div align="center">
<youtube align="center">BMm0KeMJsI4</youtube>
</div>

== User Interface ==
* '''IMPORTANT CHANGE:''' Now `Autokey` button must be enabled to generate new keyframes in Draw, Edit or Sculpt mode. If the Autokey button is disabled, the last available frame is used. This is more aligned with how Blender works and with other 2D softwares. Also, 2D template has now Autokey button enabled by default. ([https://projects.blender.org/blender/blender/commit/6a662ffda836 6a662ffda8])
** Note: Old files are not changed. Enable `Autokey` button manually to get the old behaviour.

* Now the multiframe is disabled when play animation. Before, it was impossible to see the animation because all frames were visible. ([https://projects.blender.org/blender/blender/commit/e7c36ce43cf6 e7c36ce43c])
* Swap positions of Pie menu options for Vertex Paint and Weight Paint. ([https://projects.blender.org/blender/blender/commit/20d0bb43afc8 20d0bb43af])
* New multiframe display mode. Keyframes before or after of the active frame are displayed using onion colors. This can be disable using Onion overlay options.
* Dopesheet now display a bar between keyframes to indicate that keyframe is still used. ([https://projects.blender.org/blender/blender/commit/e44b2ada3e4a e44b2ada3e]) ([https://projects.blender.org/blender/blender/commit/296984b40e03 296984b40e])
* Dopesheet context menu has been reorganized and some options added. ([https://projects.blender.org/blender/blender/commit/15a4ed3aff40 15a4ed3aff])
* Materials `Options` panel renamed to `Settings` to keep consinstency. ([https://projects.blender.org/blender/blender/commit/51c7ff9222c3 51c7ff9222])
* Annotations now has a `Opacity` parameter (this was removed during the refactor and never was added again). ([https://projects.blender.org/blender/blender/commit/c62e60f01f2a c62e60f01f])

== Operators ==
* `Resolution` parameter for Fill Tool has been renamed to `Precision` and allows values below 1. This is very handy for storyboarding. ([https://projects.blender.org/blender/blender/commit/6704372f047c 6704372f04])
* New operator to Duplicate Layers but with empty Keyframes. This operator is very useful to duplicate the keyframes but don't copy the strokes to be used in cleanup or painting works. ([https://projects.blender.org/blender/blender/commit/cfd54ad7d78e cfd54ad7d7])
* New option to Trace images starting in the current frame and not always 1. ([https://projects.blender.org/blender/blender/commit/e747c07156cc e747c07156])

== Tools ==
* New Layer transform parameters. ([https://projects.blender.org/blender/blender/commit/e02d84eb3bea e02d84eb3b])
* New Multiframe support in Draw mode (included in fill tool refactor commit).

=== Fill Tool ===
A refactor of the fill tool has been done inorder to solve several problems we had since the first version of the tool. ([https://projects.blender.org/blender/blender/commit/1352d81b1747 1352d81b17])

Changes:

* The filling speed has been improved for each step of the process with the optimization of each algorithm/function.
* New `AutoFit` option to fill areas outside of the viewport. When enable, the total size of the frame is calculated to fit the filling area.
* New support multiframe filling. Now it is possible to fill multiple similar frames in one go.
* New `Stroke Extension` option to create temporary closing strokes. These strokes can be displayed and adjusted dynamically using wheel mouse or PageUp/Down keys. Use `Wheel + Shift` key for precise adjustments.
* Parameter `Resolution` now is named `Precision` and has been moved to topbar.
* `Resolution` now has decimals and can be lower than 1 to allow quick filling in storyboarding workflows. Maximum value has been set as 5.
* Parameter `Simplify` has been moved to Advanced panel.
* Improved fill outline detection. In some cases, the outline penetrated the area to be filled with unexpected results.
* Fill is canceled if the filled area is not closed. This avoids ugly full screen fill. ([https://projects.blender.org/blender/blender/commit/c489bb7c016f c489bb7c01])
* Fixes some corner case bugs with infinite loops.

<div align="center">
<youtube align="center">Jtp0vF3PlbE</youtube>
</div>

=== Interpolate Tools ===
Following with the changes included to interpolate strokes of different number of points, a full review has been done in the interpolation tools. ([https://projects.blender.org/blender/blender/commit/6bba83058986 6bba830589])

* Interpolate now is a tool and not an operator. It was not logic to have this tool as a button.
* Interpolate tool parameters have been moved to topbar.
* Interpolate popover has been removed from topbar and interpolate `Sequence` operator has been moved to grease pencil menu.
* Interpolate Sequence now include a Redo panel.
* Interpolate tool now allows to select the strokes by pairs. This allows to interpolate any stroke with any stroke and not as before that it was only possible by drawing order. If no stroke is selected, the interpolation is done as before.
* Now is possible interpolate again if a previous keyframe exist. Before, it was impossible to interpolate two times in same frame and this made impossible to do the interpolation by groups of frames.
* New automatic option to `Flip strokes` if the initial stroke and end stroke are not in the right position because direction is different. Also the flip can be set manually for corner cases.
* Cleanup of menus related to interpolate.
* Fixed some bugs and removed parameters from scene because now all are tool or operator contained.
* Some code cleanup and function renames.

<div align="center">
<youtube align="center">88FsF6ontPk</youtube>
</div>

This commit also includes the some codebase to future implementation of the concept `Vertex Active` that now does not exist in grease pencil.

== Modifiers and VFX ==

* `Seed` parameter moved outside Randomize panel in Noise Modifier. ([https://projects.blender.org/blender/blender/commit/3191b76eb534 3191b76eb5])
* New Noise Offset parameter in Noise Modifier. ([https://projects.blender.org/blender/blender/commit/7a4bdc3a4feb 7a4bdc3a4f])
* Remove limitation to use only one Lattice modifier. ([https://projects.blender.org/blender/blender/commit/670c1fdf64d1 670c1fdf64])

== Python ==
* New API to force the stroke recalculation. In some scripts, the coordinates of the points are manipulated and there was no way to update the stroke. ([https://projects.blender.org/blender/blender/commit/faf45091b454 faf45091b4])
* New API to manipulate weight points. The old RNA prop was wrong and never was usable. ([https://projects.blender.org/blender/blender/commit/71e63153ebf3 71e63153eb])