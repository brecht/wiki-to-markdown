= Blender 2.93: Pipeline, Assets & I/O =


== glTF 2.0 ==

=== Importer ===
* Fix morph target import when named 'basis' ([https://projects.blender.org/blender/blender-addons/commit/7521a4e27be9  rBA7521a4e])
* Fix shapekey import with negative weight ([https://projects.blender.org/blender/blender-addons/commit/0dc2141207df  rBA0dc2141])
* Better extra error management ([https://projects.blender.org/blender/blender-addons/commit/30012da83522  rBA30012da])

=== Exporter ===
* New feature: export 'Loose Points' and 'Loose Edges' ([https://projects.blender.org/blender/blender-addons/commit/8d24537a6e9c  rBA8d24537])
* New option to export only visible/renderable/active collection ([https://projects.blender.org/blender/blender-addons/commit/91f57b489943  rBA91f57b4])
* Draco : Increase maximal compression limit ([https://projects.blender.org/blender/blender-addons/commit/55ec46913b99  rBA55ec469])
* Inverse matrix only when there is a parent ([https://projects.blender.org/blender/blender-addons/commit/8adb0dce852d  rBA8adb0dc])
* Hook entire glTF data for extensions ([https://projects.blender.org/blender/blender-addons/commit/4e1ab4a386d5  rBA4e1ab4a])
* Fix shapekey export in some cases ([https://projects.blender.org/blender/blender-addons/commit/5ef60cb44a0c  rBA5ef60cb])
* Fix exporting with draco when duplicate geometry ([https://projects.blender.org/blender/blender-addons/commit/3d7d5cead543  rBA3d7d5ce])
* Fix crash when mesh is not created, without crash ([https://projects.blender.org/blender/blender-addons/commit/f172f7724749  rBAf172f77])