= Blender Compatibility =

This page is to document known incompatibilities with software and hardware.

{|class="note"
|-
|<div class="note_title">'''NOTE'''</div>
<div class="note_content"> All items here must link to verified references (bug reports for example) or they will be removed.</div>
|}


Some applications which integrate themselves into your system can cause problem’s with Blender.

Here are some known compatibility issues listed by platform.

== Linux ==

...

== Mac OS X ==


=== Mouse Motion Jitters (SmoothMouse) ===

;Problem
: When grabbing an object or orbiting the view, cursor motion is jittery. [http://developer.blender.org/T47224 #47224]
;Solutions
:* Uninstall SmoothMouse.
:* Disable Continuous Grab.



== MS-Windows ==

=== Blender Hangs on Window Duplication (Nahimic for MSI) ===

;Problem
: Accessing ''Window -> Duplicate Window'', hangs Blender, using 100% of one core. [http://developer.blender.org/T47224 #47224]
;Solution
: Uninstall Nahimic for MSI