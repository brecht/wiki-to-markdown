=Blender Release Notes=
What's new in each Blender version. Visit [https://www.blender.org/download/releases/ blender.org] for the full list.

==Currently Active==
* [[Reference/Release_Notes/4.1|Blender 4.1]] ''(alpha)''
* '''[[Reference/Release_Notes/4.0|Blender 4.0]]''' ''(current stable release)''
* [[Reference/Release_Notes/3.6|Blender 3.6 LTS]] ''(support until June 2025)''
* [[Reference/Release_Notes/3.3|Blender 3.3 LTS]] ''(support until September 2024)''

==Older versions==
* [[Reference/Release_Notes/3.5|Blender 3.5]]
* [[Reference/Release_Notes/3.4|Blender 3.4]]
* [[Reference/Release_Notes/3.2|Blender 3.2]]
* [[Reference/Release_Notes/3.1|Blender 3.1]]
* [[Reference/Release_Notes/3.0|Blender 3.0]]

* [[Reference/Release_Notes/2.93|Blender 2.93 LTS]]
* [[Reference/Release_Notes/2.92|Blender 2.92]]
* [[Reference/Release_Notes/2.91|Blender 2.91]]
* [[Reference/Release_Notes/2.90|Blender 2.90]]

* [[Reference/Release_Notes/2.83|Blender 2.83 LTS]]
* [[Reference/Release_Notes/2.82|Blender 2.82]]
* [[Reference/Release_Notes/2.81|Blender 2.81]]
* [[Reference/Release_Notes/2.80|Blender 2.80]]
* [[Reference/Release_Notes/2.79|Blender 2.79]]
* <span class="plainlinks">[https://en.blender.org/index.php/Dev:Ref/Release_Notes Older Versions] ''(archived)''</span>

== Guidelines ==
* [[Reference/Release_Notes/Writing_Style|Writing Style]]
* [[Reference/Release_Notes/Blender_Demos|Demo Files]]
* [[Reference/Release_Notes/Blender_Asset_Bundle|Asset Bundle]]