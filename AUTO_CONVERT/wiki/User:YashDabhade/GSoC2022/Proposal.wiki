== GSOC 2022 - Blender 3D Text Usability Improvements ==
== Name ==
Yash Dabhade

== Contact ==
* Email: yashdabhade5@gmail.com
* blender. Chat nickname: yashhh
* Devtalk ID: yashhh
* developer.blender.org: yashdabhade

== Synopsis ==
The need for more easy-to-use features to edit text has been felt with the increased use of 3D text in blender. A good subset of the suggested improvements and smaller requests make ideal tasks that can be accomplished in a GSoC project. The improvements made will provide more flexibility to the text editor with easy formatting of text. New tools and features will be added for better and faster formatting of text.
== Benefits ==
This project will improve the support for working and formatting 3D text. After the completion of this project users will have a bunch of new features to work with and find more flexibility to a few existing tools. Operations such as changing the font and state of the text will be improved, while the ability to click-drag to select regions of text will also be added. Various features to simplify the usage of Text nodes will also be added .
== Deliverables ==

*Improvements to the 3D text editor.
*New features and Improvements to the Font option in the object editor:
** New option to select regions of text.
** Toggle between Bold, Italics and Underline for selected text.
*Secondary tasks - features and improvements based on community feedback.
*User documentation and notes.
== Project Details ==
==== Improvements to the 3D text editor : ====
Currently we have the transform option in which we can change the thickness, and position of the underline and the Scale of the small capitals. These options appear even in the absence of underline or Small Caps respectively. 
<br>
[[File:1st.png|thumb|center|UI Mockup]]
This in turn affects the text which will further be added in the same text object . We can introduce a way to hide the second half of “transform” in the absence of Underline on the text.
<br>
==== New features and Improvements to UV Editor Snapping option(s): ====
Blender currently provides very little flexibility for formatting 3D text. We cannot
directly underline, bold or italicise our text. We have to go through 2 steps and select
the fonts that have the options.
#1 '''Select Regions of Text:'''
The selection option can be devised in such a way that it selects along with the mouse pointer, which can be implemented following the precedents of the selection during the editing of the text i.e., after the TAB button is pressed.
<br>
[[File:Screenshot from 2022-06-14 22-23-10.png|thumb|center]]
<br>

The first idea that came into my mind was to compare the coordinates of the selection square with the coordinates of the text to be selected and thus select the region specified by the coordinates.This will include making changes to the usage of Python API with object.select_set() where the text can be stored into a data structure where each character is stored along with its coordinates .

#2 '''Toggle between Bold, Italics and Underline for selected text:'''
After the selection of text, we cannot directly change the text to Bold or Italics.
Bold, Italic & Bold+Italic fonts have to be loaded manually by the user. T68887
<br>
[[File:2nd.png|thumb|centre]]
A new palette will be added having the options to change the selected text to Bold or italics corresponding to the selected font in the Regular option.
A way to implement this can be inspired by D12977: BLF: Add Support for Variable Fonts, keeping in mind all the problems associated with it. 
#3 '''Features to simplify editing of 3D text:'''
* Presently there are no shortcut keys for the conversion of text to Bold, Italics
or Underline. A new set of shortcut keys can be generated for the fast and
handy formatting of text.
* Nodes to simplify the working and editing of Text Nodes will be added.
* The procedure to change the color of the text can be simplified by adding the
option in the same palette, combining all the options to form a rich text editor.
==== Secondary tasks : ====
A list of Secondary tasks to improve the 3D text editor for blender will be prepared on discussing with the mentors and on the basis of user/community suggestions. Based on talks with my mentors, I'll prioritise these ideas and begin working on them over the last weeks of GSoC. If the primary deliverables are done sooner than expected, I'll immediately begin working on the supplementary assignments. I intend to continue working on these tasks even after the completion of GSoC.
* Introduction of new nodes to format Text nodes.
* Text object font picker UI T71484 : (This is one of the tasks I personally am inclined towards and wish to contribute towards it .)
== Project Schedule ==
As per the official University schedule, final exams for the current semester will conclude on 22nd May.
Until the official announcement (20th May), I will spend time learning more about the code-base. I will also be working on smaller tasks and bug fixes related to 3D text and modelling to familiarize myself with the existing code and the development style

==== Community Bonding period (May 20 to June 13) ====
During this time, I'll work with mentors and developers to finalise the assignment details. I'll also be actively seeking other ideas from the community to put in the list of Secondary tasks. These ideas will be worked upon during the final weeks of GSoC and I’ll continue working on them after GSoC as well.

Tentative Schedule for the ideas listed in this proposal : 

{| class="wikitable"
|-
! Week !! Task Details
|-
| 1-2-3 ||
* Discuss with mentors and finalize the implementation details for deliverable 1- Improvements to the 3D text editor. 
* Work on implementation of deliverable 1
* Complete implementation of Deliverable 1
* Prepare for the next task .
|-
| 4-5-6 ||
* Discuss with mentors and finalize the implementation details for deliverable 2- Select regions of Text
* Work on implementation of the click-drag ability to select text.
* Complete implementation of Deliverable 2
|-
| 7-8-9 ||
* Discuss with mentors and finalize the implementation details for deliverable 3 - Toggle between Bold, Italics and Underline for selected text
* Work on implementation of deliverable 3
* Complete implementation of Deliverable 3
|-
| 10-11 ||
* Work on secondary tasks
* Buffer period - keeping a light work schedule for this period since the next semester is expected to start around this time
* Finish any pending tasks and backlogs
* Collect user feedback and bug reports
* Fix any major bugs and deviations from the intended behavior
|-
| 12 ||
* Code clean up
* User documentation and notes
* Prepare for final evaluations
|}

The above timeline is provisional, and I expect tasks to be completed sooner or take longer than anticipated. Any unexpected completion delays will be compensated during the buffer period in weeks 10 and 11. If the tasks are finished ahead of schedule, I'll start working on the secondary task ideas 

== Bio ==
Hello, I am Yash Dabhade , a 2nd Year undergraduate student at BITS Pilani, Goa Campus, India and I am pursuing my major in Computer Science engineering .
I began using Blender API for a software development project,which led to me viewing Blender Guru tutorials to understand basic design.
Regarding my background in programming:
o   Used C++ and Python in devising algorithms for my Quant trading project.
o   Used JAVA for the Object Oriented Programming project as a part of the university course.
I haven’t worked on any major tasks or patches yet, since I am still working on understanding the code-base. I hope that GSoC will provide the opportunity to make valuable contributions to Blender and I can continue contributing after the Summer of Code program as well.