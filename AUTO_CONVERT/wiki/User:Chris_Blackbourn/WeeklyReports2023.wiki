See also [[User:Chris_Blackbourn/WeeklyReports|WeeklyReports2022]]

== Calendar Week 1 ==
January 3 - January 9

* Fix [https://developer.blender.org/T103469 T103469] Very bad UV "sphere projection" unwrap [https://developer.blender.org/rB280502e630e9 commit]
* Investigate [https://developer.blender.org/T103670 T103670] UV Editing: Lock Borders - Incorrectly defines borders
* [https://developer.blender.org/D16876 Differential]  Simplify matrix multiply logic (Part 1 of 2)
* WIP [https://developer.blender.org/T78402 UV Grid Unwrap]

== Calendar Week 2 ==
January 10 - January 16

* Fix [https://developer.blender.org/T103787 T103787] [https://developer.blender.org/T103670 T103670]. UV islands with mark seam would tear at boundaries. [https://developer.blender.org/D16992 Differential] [https://developer.blender.org/rB326e1eeb569f Cleanup] [https://developer.blender.org/rB4160da187c9ed003354c08d3be2a4cbd504cd973 Commit]
* Debug [https://developer.blender.org/T103707 T103707] UV Select Overlap incorrectly detects some faces as overlaps
* WIP [https://developer.blender.org/D16876 Simplify Matrix Multiply] (Part 1 of 2)
* Investigate [https://developer.blender.org/T99249 T99249] Ply Exporter broken with use_uv=True if uv non-unique
* Investigate [https://developer.blender.org/T102683 T102683] Triangulation of UVs in UV editor
* Investigate [https://developer.blender.org/T103868 T103868] 2D Transform gizmo not drawn and non-responsive if it would have zero area.
* WIP [https://developer.blender.org/T78402 UV Grid Unwrap]

== Calendar Week 3 ==
January 17 - January 23

* Grid Unwrap UV, Design review build now for [https://builder.blender.org/download/patch download]
* Simplify matrix logic [https://developer.blender.org/rB6769acbbba7f commit] Part 1 of 2
* WIP Simplify matrix logic (Part 2 of 2) (offline)
* Fix regression, [https://developer.blender.org/T103971 T103971]  Live unwrapping wraps hidden faces [https://developer.blender.org/rB6672b5373f172e32351ee4d233c8a434ff95417a commit]
* Investigate [https://developer.blender.org/T103868 T103868] UV Manipulator disappears if AABB of selection has zero area.
* WIP extended feature [https://developer.blender.org/T103469 T103469] UV "sphere projection" unwrap with Seams

== Calendar Week 4 ==
January 24 - January 30

* Fix [https://developer.blender.org/T98594 T98594], UV Editor Redraw problem with geometry nodes. [https://developer.blender.org/rB34a6591a073f commit]
* Fix [https://developer.blender.org/T103868 T103868], Missing UV Transform gizmo if it has zero area. [https://developer.blender.org/rB6c8db7c22ba4 commit]
* Resolve [https://developer.blender.org/T103975 T103975], Add UV Select Similar Object and UV Select Similar Winding. [https://developer.blender.org/rB2b4bafeac68e commit]
* Investigate [https://developer.blender.org/T103956 T103956], UV Sculpt brushes should work in multi-object edit mode.
* Cleanup UV winding calculation, reducing memory allocation pressure. [https://developer.blender.org/rBcef03c867b05 commit]

* WIP [https://developer.blender.org/D17039 Grid Unwrap] (offline)
* WIP [https://developer.blender.org/T68889 Merge UV Packing Engines.] (offline)

== Calendar Week 5 ==
January 31 - February 6

* Add UV Seam support for UV Sphere Unwrap [https://developer.blender.org/D17177 Differential]

* WIP [https://developer.blender.org/D17039 Grid Unwrap] (offline)
* WIP [https://developer.blender.org/T68889 Merge UV Packing Engines.] (offline)

== Calendar Week 7 ==
February 7 - February 13

* Gitea migration

* UV Alternate face selection [https://projects.blender.org/blender/blender/issues/104374 Issue] [https://builder.blender.org/download/patch/PR104610 Download]

* WIP [https://developer.blender.org/D17039 Grid Unwrap] (offline)
* WIP [https://developer.blender.org/T68889 Merge UV Packing Engines.] (offline)

== Calendar Week 8 ==
February 14 - February 20

* More Gitea migration

* UV Alternate face selection, now with click-select, lasso select, circle select and rectangle select. Updated version [https://builder.blender.org/download/patch/PR104846/ Download]
* Test and Fix for `mul_m3_series` and `mul_m4_series` with aliased parameters [https://projects.blender.org/blender/blender/pulls/104934 Pull Request]
* Add seam support to UV Sphere Project and UV Cylinder Project [https://projects.blender.org/blender/blender/pulls/104847 Pull Request]

== Calendar Week 9 ==
February 21 - February 27

* Migrating WIP from previous blender repo to gitea.
* Commit fix for `mul_m3_series` and `mul_m4_series` [https://projects.blender.org/blender/blender/commit/ba6c9a6f59c6ef138a7a2c6aa9d64c3859a4c382 commit]
* Packing research, incremental migrate from Editor to bf_geometry. [https://projects.blender.org/blender/blender/pulls/105212 Pull Request]

== Calendar Week 10 ==
February 28 - March 6

* Update UV Sphere Projection and UV Cylinder Projection with Seam support. [https://projects.blender.org/blender/blender/commit/6b8cdd5979964f39b01bc4def194255fa3766a40 commit] 
* Migrate UV packing from Editor to bf_geometry [https://projects.blender.org/blender/blender/commit/cb1af1fbd9c4395c63cd24b4ec39f65e9d4d63cb commit]
* Investigate advanced "Minimize Stretch" techniques with Julien Kaspar

* WIP Fix #102843: Add UV Packer with O(nlogn) performance. [https://projects.blender.org/blender/blender/pulls/105393 Pull Request]

== Calendar Week 11 ==
March 7 - March 13

* Fix [https://projects.blender.org/blender/blender/issues/102843 #102843]. Add novel UV Packing strategy, the incredibly fast "Alpaca", ("L-Packer") which packs in `O(n log n)` time complexity. [https://projects.blender.org/blender/blender/commit/b1185da40341 commit]

* Fix unreported UV regression, margin was offset. [https://projects.blender.org/blender/blender/commit/c77b78ad53e9 commit]

* UV Cleanup [https://projects.blender.org/blender/blender/commit/b06edc289709 commit] [https://projects.blender.org/blender/blender/commit/10f06221c1ef commit] [https://projects.blender.org/blender/blender/commit/d474732f91c8 commit] [https://projects.blender.org/blender/blender/commit/5c5650f0f8d1 commit] [https://projects.blender.org/blender/blender/commit/d46a0f5a1ab5 commit] [https://projects.blender.org/blender/blender/commit/d41a1e18066c commit] [https://projects.blender.org/blender/blender/commit/a3b0a77f50cb commit] [https://projects.blender.org/blender/blender/commit/507c44228969 commit]

* WIP Alpaca_Rotate, the fast L-Packer which also supports rotation.

== Calendar Week 12 ==
March 14 - March 20

* Improve packing efficiency on non-square materials. [https://projects.blender.org/blender/blender/commit/c38fe871276178e24273baa98e1ca466402694e9 commit]
* WIP xatlas packer, the brute force, bitmap occupancy, packer that supports irregular shapes. [https://projects.blender.org/blender/blender/pulls/105821 Pull Request] 
* WIP Alpaca_Rotate, the fast L-Packer which also supports rotation.

== Calendar Week 13 ==
March 21 - March 27

[[File:UVFreezeDetail.png|thumb|Detail from xatlas packing of UVFreeze.blend]]

* Add xatlas packer, the brute force, bitmap occupancy, packer that supports irregular shapes. [https://projects.blender.org/blender/blender/commit/e0d05da8262d84d21a0a1bfb0c7a48d9c29a216c commit]
[https://projects.blender.org/blender/blender/commit/f7946b486b0f6e2f1594155f60a3152dfa522ff5 commit]
[https://projects.blender.org/blender/blender/commit/c685d1935a10960a08430cbd7aab2ce07a88a4ec commit]

* WIP Alpaca_Rotate, the fast L-Packer which also supports rotation.
[https://projects.blender.org/blender/blender/pulls/105977 Pull Request] 

* Investigate [https://projects.blender.org/blender/blender/issues/105860 105860], Crash when adding UV Layer.

* Planning "perfect" packer, the packer which produces optimal results for some inputs. e.g. [https://www.combinatorics.org/files/Surveys/ds7/ds7v5-2009/ds7-2009.html optimal square packing]

== Calendar Week 14 ==
March 28 - April 3

* Investigate [https://projects.blender.org/blender/blender/issues/106314 #106314] UV Copy/Paste problem  (Issue for 3.5 backporting)

* Fix [https://projects.blender.org/blender/blender/issues/78396 #78396] Pack UVs to original bounding box. [https://projects.blender.org/blender/blender/commit/957ac41237fb19e03956dda0a293455b604a95ac commit]

* Add Alpaca_Rotate, the fast L-Packer which also supports rotation. [https://projects.blender.org/blender/blender/commit/b828641a93f9302562122aa66ff3a7a3b1ee6a3d commit] 

* Change packing to use a {pivot, half-diagonal} representation [https://projects.blender.org/blender/blender/commit/531f99ffbd6d2c17005f3b6f8441dae309781c91 commit]

* UV Packing cleanup [https://projects.blender.org/blender/blender/commit/ed0e4f404345369b32648d7051ca9a84b471fbde commit] [https://projects.blender.org/blender/blender/commit/40f050aadc55cb2c3df7c5d133ce5c59f8400424 commit] [https://projects.blender.org/blender/blender/commit/3cfc3eab936813a9e0d0fb69180bcf0107feea94 commit] [https://projects.blender.org/blender/blender/commit/9ea6771d100e53a398c5392bce98d9f557b13392 commit] [https://projects.blender.org/blender/blender/commit/abb879175e307b8013a9f19f92a3dece5824dd56 commit] [https://projects.blender.org/blender/blender/commit/86b1e5e3b6efa64bf1335e364536e450adc1d768 commit]

* Simplify aspect ratio usage in UV Unwrapper [https://projects.blender.org/blender/blender/commit/8ac67a798bfbe93a79d543082b3fb012b34abff5 commit]

* Improve performance of xatlas strategy when packing aligned quads [https://projects.blender.org/blender/blender/commit/c685d1935a10960a08430cbd7aab2ce07a88a4ec commit]

* WIP support rotation in the xatlas packer (offline)

* Planning "perfect" packer, the packer which produces optimal results for some inputs. (offline)

== Calendar Week 15 ==
April 4 - April 10

* Add support for non-square bitmaps in alpaca_rotate [https://projects.blender.org/blender/blender/commit/564418f6a482d0cb643594defacabca0e42bb3e7 commit] 

* Add rotation in the xatlas UV Packer [https://projects.blender.org/blender/blender/commit/43476e2d71ce6b537b07bb63cc57c135b5feaa3f commit] 

* UV Packing cleanup [https://projects.blender.org/blender/blender/commit/bc86223975575701e351da367811d73b7162eac6 commit]

* Fix [https://projects.blender.org/blender/blender/issues/106314 #106314], UV Paste operator, warn when fail to find a match ([https://projects.blender.org/blender/blender/commit/6e1c04825012b335d0c8b7448a7cf2077282a47d commit]) and return OPERATOR_CANCELLED when no change to prevent creating an UNDO state. ([https://projects.blender.org/blender/blender/commit/63c0e1be319abf301b5be627fbbb86b9a6741570 commit])

* Adding "job" support to UV Packer, to add a progress bar and stop button. (WIP)

* Adding UV Packing new feature, "Overlapping islands stick together" (WIP)

* Planning: pin support for UV Packing. (offline)

* Planning "perfect" packer, the packer which produces optimal results for some inputs. (offline)

== Calendar Week 16 ==
April 11 - April 17
[[File:OverlapIslands.png|thumb|Overlapping islands remain overlapping after packing. Useful for games and low poly assets.]]
Mostly clean up work, paving the way for a few new packing features (Pack overlap, pin support, jobs)

* New feature for UV Packing, "Overlapping islands stick together" [https://projects.blender.org/blender/blender/commit/b601ae87d064cbde29e8ae7191ebba54213c709a commit]

* UV Packing, move pre-rotation inside packing engine [https://projects.blender.org/blender/blender/commit/6a0b90bc92b968b263884aaf32258f015a024fa4 commit]

* UV Packing cleanup [https://projects.blender.org/blender/blender/commit/e078419c9c12ac903a89db262d61d0677583e094 commit] [https://projects.blender.org/blender/blender/commit/d48939f103c00807acc75921a74f4bf6586941ed commit] [https://projects.blender.org/blender/blender/commit/5ba30e07f2b839da0869e9c0264150cb7bbbee0c commit] [https://projects.blender.org/blender/blender/commit/0a460d470718d7c4b54a1e54a4c6ae242318cee3 commit]

* UV Unwrap cleanup [https://projects.blender.org/blender/blender/commit/e078419c9c12ac903a89db262d61d0677583e094 commit]

* Planning "perfect" packer, the packer which produces optimal results for some inputs. (offline)

== Calendar Week 17 ==
April 18 - April 24

As Blender 3.6 enters Bcon2, mostly clean up work, checking regressions and investigating repros.

* Updating documentation for Blender 3.6. (Awaiting write-access to `svn commit`)

* Planning UV improvements for Blender 4.0

* Planning "perfect" packer, the packer which produces optimal results for some inputs. (offline)

== Calendar Week 18 ==
April 25 - May 1
[[File:FourSquare.png|thumb|Optimal packing with `n=1` islands. Notice how the single island touches all four sides of the unit square.]]

* More updating documentation

* UV Packing, rotate to minimum square [https://developer.blender.org/rB5c1c45cd592282a852de10faeb36846ca39f4dc6 Commit]
    UV Packing now produces ''optimal'' layouts for the important `n=1` case on all inputs and options.
    (e.g. The UV Unwrapper often produces a single island, which can now be packed optimally into the unit square.)

* Add new UV Packing option, `scale_to_fit`, to pack without scaling [https://developer.blender.org/rBbf56df3236cc4d075e6f474f7d18b17d0ed318ed Commit]

* Fix UV Packing regression with `fraction` margin method when UVs don't fit in unit square. [https://developer.blender.org/rB69d52c5f1c302a710ca2ceada9bbf705dc228411 Commit]

* Preparing to add progress bars and "stop" functionality to UV packing. [https://developer.blender.org/rB5c6f254a66bcb5d0bc663e37534f6ed06ddeaa86 Commit] [https://developer.blender.org/rB91020ccde15a4d473e2b6360e96b660384a47ae5 Commit]

== Calendar Week 19 ==
May 2 - May 8

[[File:Optimal67.png|thumb|An optimal packing of 67 uniform quads. The layout shown is slightly tighter than a 9x9 grid with 14 quads missing.]]

* UV Packing, improve UV layout efficiency. [https://developer.blender.org/rBb01f5444a3fd1964446a3587023bb7f613c3013c Commit]
 
    UV packing now produces ''optimal'' layouts for many uniform-square packing inputs.

* UV Packing, add multiple options for pinned islands, lock scale, lock position etc. [https://developer.blender.org/rB5abb3c96cfa5e16f4432241aafab8712c70945e9 Commit]

* Improve UV Packing efficiency further by re-ordering packing operations. [https://developer.blender.org/rB3ea060860a4dd758dfeaee5c63fb4aa4a8a0b82b Commit]

* Debugging and partial fix for #104513, different UV unwrap results on x86_64 and apple silicon CPUs. [https://developer.blender.org/rB0eba9e41bff97659bdb5e51ba814d3e5dd150b43 Commit]

== Calendar Week 20 ==
May 9 - May 15

* (With Campbell), Add progress bar and ability to cancel UV Packing [https://developer.blender.org/edc12436c66a6abc456f1113389035ab391b0adf Commit]

* Add Pinned islands support for UV Packing [https://developer.blender.org/5abb3c96cfa5e16f4432241aafab8712c70945e9 Commit]
  - Ignore pinned islands
  - Normal (default)
  - Lock scale
  - Lock rotation
  - Lock position of pinned UV islands.

* Further efficiency improvements for UV Packing [https://developer.blender.org/ccb2dbddac3a2c19b0c5a6ae923d433f0e1ef1b1 Commit]

* Fix #107898: UV Packing regression [https://developer.blender.org/f44795ce17d96d5d208cb6158a435e5702d27a2a Commit]

* Debugging #104513, different UV unwrap results on x86_64 and apple silicon CPUs.

== Calendar Week 21 ==
May 16 - May 22

* Debugging #104513: Different UV unwrap results on x86_64 and apple silicon CPUs.

* Debugging #108117: Crash in uv editor with hidden geometry (PR in progress)

* UV: Packing problems with locked-position. [https://projects.blender.org/blender/blender/pulls/108063 Pull Request]

* UV: Fix packing problem with locked-position and large island count [https://developer.blender.org/rB6daa3f6b8a9b711c0697fb48f30c8c04af783c77 Commit]

* UV: Fix packing problems with locked islands. [https://developer.blender.org/rB4b4babae8b495936ed8c9c00b7bf1486d2107107 Commit]

* WIP: [https://projects.blender.org/blender/blender/issues/103956 SculptUV on multiple objects] (large!)

* Investigate circle packing (offline)

== Calendar Week 22 ==
May 23 - May 29

* Online meeting with artists from a major games studio which uses Blender to discuss enhancements for Vertex Color Attributes. (More to come here)

* UV: Add additional options for rotation while UV Packing, "Any", "Cardinal" and "Axis-Aligned" [https://developer.blender.org/rBf96e108b6399440fc63be909cc97359a73bbea6a commit]

* Debugging [https://projects.blender.org/blender/blender/pulls/104513 #104513]: Different UV unwrap results on x86_64 and apple silicon CPUs.

* UV: Fix additional packing problems with locked-position and original-bounding-box. [https://developer.blender.org/rB71d89dfa810e17be730e2e647b371e1d734b0db9 commit]

* UV: Update UI for UV Packing [https://projects.blender.org/blender/blender/pulls/108253 Pull Request]

* Fix #108117: Crash in uv editor with hidden geometry [https://developer.blender.org/rBae9ac99d2b93236887dfc87402c725d68a72689d commit]

* Debugging [https://projects.blender.org/blender/blender/pulls/108222 #108222]: Copy/Paste UVs sometimes merges vertices

* Fix #108037, Omnibus UV packing problems. [https://developer.blender.org/rB96101a66fc83dc58c61b86af1d4d021036626b9e commit] [https://developer.blender.org/rB47cbeabb1182fbd964d8cbefb04d30a2ced6efc4 commit] [https://developer.blender.org/rBde4f456ea2e4ab6290f2446bf2d179a12bc6a989 commit]

Research continues on "Optimal" packing, for n=11 and n=17 squares.

== Calendar Week 23 ==
May 30 - June 5
[[File:PackIslandsUI.png|thumb|Updated UI for UV Pack Islands]]
[[File:Pack18.png|thumb|Packing 18 quads into the unit square, optimally]]
* Add optimal packing for n=11, n=18, n=19 and n=26 squares. [https://developer.blender.org/rB608fdc9c5846120d62dc4050f0e9ada10e9108d6 commit]

* Update UI for the Pack Islands operator [https://developer.blender.org/rBf87f119c2fc4727ca791fd5b8a30f1d32a162ff0 commit]

* Fix a memory leak during Pack Islands [https://developer.blender.org/rBf78d1fd1146f6b460a7fe6964eaff086e34da862 commit]

* Write proposal for Channels in the Vertex Paint Mode [https://projects.blender.org/blender/blender/issues/108404 #108404]

* Update manual with latest UV Packing changes [https://projects.blender.org/blender/blender-manual/pulls/104468 PR]

== Calendar Week 24 ==
June 5 - June 12

* Fix #108786: Logic errors with pinned islands inside uv packer [https://developer.blender.org/rB31ce1435698f700a04fbd97e6cdbc0d4941c40b4 commit]

* Fix #104513: UV packing produces different results on x86 vs apple silicon [https://developer.blender.org/rB9d25c4aaa6b1a53573b975fdd792b6308d449f46 commit]

* Fix Unreported: Invalid UV Packing on windows debug build [https://developer.blender.org/rBe08dde04c195e71b77d69ca132398d109b7805b1 commit]

* More blog writing [https://wiki.blender.org/wiki/User:Chris_Blackbourn/BlogDraft draft]

* Debugging [https://projects.blender.org/blender/blender/issues/108811 #108811] Follow Active Quads changes unselected UVs.

* Closed [https://projects.blender.org/blender/blender/issues/108335 #108335] UV packing issue with cardinal rotation, as "Works-as-intended"

* Investigate [https://projects.blender.org/blender/blender/issues/108838 #108838] UV unwrap issues

== Calendar Week 25 ==
June 13 - June 19

Blog writing! [https://wiki.blender.org/wiki/User:Chris_Blackbourn/BlogDraft WIP Draft]

* Fix unreported, Improve UV Packing when Alpaca packs tighter than all the alternatives. [https://developer.blender.org/rBb8d263ee5a38e98c9852869fc664982b8027b141 commit]

* First pass [https://projects.blender.org/blender/blender/issues/108811 #108811], follow active quads and selection. [https://projects.blender.org/blender/blender/issues/108811 Pull Request] (Was harder than expected.)

* WIP [https://projects.blender.org/blender/blender/issues/103956 #103956] Sculpt UV tools on multiple objects.

* Close [https://projects.blender.org/blender/blender/issues/108838 #108838], fixed by [https://developer.blender.org/rBe08dde04c195e71b77d69ca132398d109b7805b1 previous commit]

* Debug [https://projects.blender.org/blender/blender/issues/107500 #107500], Editing UVs after normals with a previously assigned variable doesn't work.

* Debug [https://projects.blender.org/blender/blender/issues/107416 #107416], Debug assert in uv layer data.foreach_get when called on an Edit Mode mesh.

* Investigate [https://projects.blender.org/blender/blender/issues/108840 #108840] Inconsistent UV margin in lightmap pack. (Confirmed, no easy fix.)

Next week:
* Pushing for a fix on [https://projects.blender.org/blender/blender/issues/107500 #107500], Editing UVs after normals with a previously assigned variable doesn't work.

== Calendar Week 26 ==
June 20 - June 26

* Research [https://projects.blender.org/blender/blender/issues/78101 #78101], LSCM_Spectral method for unwrapping.

* Debugging [https://projects.blender.org/blender/blender/issues/107500 #107500], Editing UVs after normals with a previously assigned variable doesn't work.

* Debugging [https://projects.blender.org/blender/blender/issues/108840 #108840], Margin during UV Lightmap Pack.

* New feature, Invert Pins, [https://projects.blender.org/blender/blender/pulls/109187 Pull Request]

* Impromptu brainstorm session with @BlenderBob, identifying lots of areas for improvement:
 * [https://projects.blender.org/blender/blender/issues/109272 #109272] Export UVs checklist
 * [https://projects.blender.org/blender/blender/issues/109271 #109271] Image/NoImage changes zoom in UV Editor
 * [https://projects.blender.org/blender/blender/issues/109270 #109270] UV rendering in UV Editor in Object Mode
 * [https://projects.blender.org/blender/blender/issues/109265 #109265] Average island scale, common origin
 * [https://projects.blender.org/blender/blender/issues/109264 #109264] UV sync-selection should support island mode
 * [https://projects.blender.org/blender/blender/issues/109263 #109263] Option to respect pins when moving UVs
 * [https://projects.blender.org/blender/blender/issues/109190 #109190] Adding 3D gizmos to the exising UV projection operators
 * [https://projects.blender.org/blender/blender/issues/109189 #109189] UV Editor rendering enhancements
 * [https://projects.blender.org/blender/blender/issues/109184 #109184] Additional options for "UV Select Similar"

Next week: More [https://projects.blender.org/blender/blender/issues/107500 #107500], followup from brainstorming, more blog writing.

== Calendar Week 27 ==
June 27 - July 3

More blog writing: [https://wiki.blender.org/wiki/User:Chris_Blackbourn/BlogDraft WIP_Draft]

* Experiments with [https://projects.blender.org/blender/blender/issues/107500 #107500] UV custom data layers from python... Complicated...

* Investigate [https://projects.blender.org/blender/blender/issues/109605 #109605] UV Align Rotation with non-square aspect ratio

* Investigate [https://projects.blender.org/blender/blender/issues/109278 #109278] Correct Face Attribute

* More research, LSCM_Spectral.

Next week, more of the same.

== Calendar Week 28 ==
July 4 - July 10

More blog writing: [https://wiki.blender.org/wiki/User:Chris_Blackbourn/BlogDraft WIP_Draft]

* More experiments with [https://projects.blender.org/blender/blender/issues/107500 #107500] UV custom data layers from python...

* Fix [https://projects.blender.org/blender/blender/issues/109605 #109605] UV Align Rotation with non-square aspect ratio [https://projects.blender.org/blender/blender/commit/374935fb56da8ec35b68ade54aaddcc26c5dc264 commit]

* Investigate [https://projects.blender.org/blender/blender/issues/109816 #109816] UV Packing query

* [https://projects.blender.org/ChrisLend/blender/commit/95424af1b7f418eac12d3386c3f4328905c76294 Commit] UV Invert Pins

* Fix [https://projects.blender.org/blender/blender/issues/109674 #109674] UV Follow Active Quads with [https://projects.blender.org/blender/blender/commit/374935fb56da8ec35b68ade54aaddcc26c5dc264 commit]

* Fix [https://projects.blender.org/blender/blender/issues/109673 #109673] Crash in Debug when using UV Packer or UV Unwrapper [https://projects.blender.org/blender/blender/commit/9da64ac391286b2670fe39707067db62824c0f15 commit]

* Fix [https://projects.blender.org/blender/blender/issues/109605 #109605] Add correct_aspect to uv align rotation operator [https://projects.blender.org/blender/blender/commit/374935fb56da8ec35b68ade54aaddcc26c5dc264 commit]

Next week, hopefully finish off Blog, and try a fix for UV Custom Data Layers

== Calendar Week 29 ==
July 11 - July 17

More blog writing: [https://wiki.blender.org/wiki/User:Chris_Blackbourn/BlogDraft WIP_Draft]

* More experiments with [https://projects.blender.org/blender/blender/issues/107500 #107500] UV custom data layers from python...

* Investigate [https://projects.blender.org/blender/blender/issues/109906 #109906] UV Unwrap has different layout behaviour in Blender 3.5 and Blender 3.6

* Close [https://projects.blender.org/blender/blender/issues/109816 #109816] UV Packing query, working correctly.

* Investigate [https://projects.blender.org/blender/blender/issues/109271 #109271] UV Editor draw scaling issue when textures are showing.

== Calendar Week 30 ==
July 18 - July 25

* More experiments with [https://projects.blender.org/blender/blender/issues/107500 #107500] UV custom data layers from python. (This one proving much more tricky than expected)

* Fix [https://projects.blender.org/blender/blender/issues/109271 #109271] UV Editor draw scaling issue when textures are showing. [https://projects.blender.org/blender/blender/pulls/110413 Pull Request]

== Calendar Week 31 ==
July 25 - July 31

* Even more experiments with [https://projects.blender.org/blender/blender/issues/107500 #107500] UV custom data layers from python.

* Fix [https://projects.blender.org/blender/blender/issues/110626 #110626] ABF UV Unwrap regression. [https://projects.blender.org/blender/blender/commit/a35ad5899fca168e9457af98b7ff11a7c4000ac9 commit]

* Improve UV Packing comments, [https://projects.blender.org/blender/blender/commit/fc2ee7d912fab664391cb24ad11805cb4c9eba00 commit]