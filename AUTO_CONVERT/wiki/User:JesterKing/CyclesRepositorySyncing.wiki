== Notes on syncing Cycles repository ==

== 2020.01.14 ==

00:03 - nvm, I realized that I think I already know what the problem is: need to adapt the application to do the new tricks Cycles can, like ensure a display pass is set

== 2020.01.13 ==

After some back and forth between different ways of automatically detecting potentially failing patches there was still at least one patch failing, namely [https://developer.blender.org/rBc30d6571bb47734e0bcb2fced5cf11cb6d8b1169 support for tiled images and UDIM naming scheme].

Only today I realized that a patch was missing from the Cycles stand-alone repository, one that should have been synced on August 22nd, but for some reason was left out. The patch in question is [https://developer.blender.org/rBfeed46c4ae480ccd3f1b1ee6bb61adaf308f85c3 Merge per-shader SVM nodes in the main update thread to avoid locking]. I don't know really why this was missing, but hacking the sync script a bit to also take this one along made all patches apply nicely.

Now back up a bit, divvy up the patches according releases and put in-between Cycles version commits. They'll be missing from the Blender repository, but prior to releasing 2.82 we can ensure that both Blender and Cycles stand-alone agree again wrt versioning.

Still need to compile the stand-alone and ensure that it starts properly.

23:59 - finally compiled, but running on MacOS gives images with full transparency. Need to test on Windows and Linux still.

== 2020.01.08 ==

* realization: check with is in stabilization branch '''and''' what is added to master during bcon1 while stabilization branch exists.
** in theory all commits in stabilization branch should also be in master due to merging

(note: linked ranges are for whole repo, need to pick out cycles commits from these)



Steps

# ''standalone'': commit Cycles version ''1.10.0''
# ''standalone'': tag ''v1.10.0''
# ''standalone'': apply Cycles patches [https://developer.blender.org/diffusion/B/compare/?head=v2.81&against=c6f8ea7b45af c6f8ea7b45af..v2.81]
# ''standalone'': commit Cycles version ''1.11.0''
# ''standalone'': tag ''v1.11.0''
# <strike>''standalone'': apply Cycles patches [https://developer.blender.org/diffusion/B/compare/?head=v2.81a&against=v2.81 v2.81..v2.81a]
# ''standalone'': tag ''v1.11.1''</strike> (no commits to cycles between v2.81..v2.81a)
# ''standalone'': apply Cycles patches [https://developer.blender.org/diffusion/B/compare/?head=master&against=v2.81a v2.81a..master(HEAD)]
# <strike>''blender'': commit Cycles version ''v1.12.0'' ? or wait until prior to actual release in February?</strike>
# ''source/tools'': update cycles_commits_sync.py

At the moment of writing master head is [https://developer.blender.org/rB010c551257d5e54eb4ca58116b745ca21a09c379 010c551257d5], good for 137 patches to check and sync.

<hr/>


scratchpad text, this isn't the droid you're looking for.
* v2.80 tag was created on July 15th, 2019. ''No'' version bump was made. Should have been: ''1.10.0''. 
* currently Cycles standalone repo is at [https://developer.blender.org/rC89cb9a6f74b28e6f797e38304ffafdbb9b395030 89cb9a6f74b2], corresponding to Blender repo [https://developer.blender.org/rBc6f8ea7b45af72fa7f7d1a47140fd946c1db3d5e c6f8ea7b45af]. Difference between v2.80 tag and last synced commit: [https://developer.blender.org/diffusion/B/compare/?head=c6f8ea7b45af&against=v2.80 v2.80..c6f8ea7b45af]
** should insert Cycles version commit ''1.10.0'' in '''stand-alone''' before applying patches from last synced to v2.81
* last commit in v2.80 tag blender.git: [https://developer.blender.org/rB65168825e0b0fbc64efd5d2920f1824d66408782 65168825e0b0]
* last synced commit until v2.81 tag in blender.git: [https://developer.blender.org/diffusion/B/compare/?head=v2.81&against=c6f8ea7b45af c6f8ea7b45af..v2.81]
** should insert Cycles version commit ''1.11.0'' in '''stand-alone''' before applying patches from v2.81a till current head.
* commits in blender.git between v2.81 and v2.81a: [https://developer.blender.org/diffusion/B/compare/?head=v2.81a&against=v2.81 v2.81..v2.81a]


Before moving to bcon3 commit ''1.12.0'' in '''blender repository'''? Probably not, looks like just need to bump Cycles version as part of release day.