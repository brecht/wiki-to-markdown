__TOC__


= Reports 2020 =

== June 22 - June 26 / #52 ==
* Phabricator on docker

== June 15 - June 19 / #51 ==
* Work on [[User:JesterKing/BlenderLtsReleaseProcessProposal|LTS release proposal]] and [[User:JesterKing/CiProposal|CI flow proposal]]

== June 8 - June 12 / #50 ==
* Follow Blender LTS task
* Figure out release plan for 2.83.1 (unresolved)
* Some bug triaging

== June 1 - June 5 / #49 ==
* Released Blender 2.83

== May 25 - May 29 / #48 ==
* Looking out after workboard

== May 18 - May 22 / #47 ==
* Looking out after workboard

== May 11 - May 15 / #46 ==
* Complete set up of release account for Windows Store and Steame
* Finally figured out where the Windows Store page media and text can be changed

== May 4 - May 8 / #45 ==
* some light release management

== April 27 - May 1 / #44 ==
* release 2.83 tasks
* release docs
* rpm building
* phab on docker prep

== April 19 - April 25 / #43 ==
* Mostly release management related tasks (writing to several involved people, checking workboard)
* Started on devops tasks, but dropped it due to many distractions
* Wrote Dalai and Ton that I need to cut back on hours because with current situation I can't find enough continuous time blocks for coding work

==== review #43 ====
Too many distractions due to strict measure situation in Finland to be able to concentrate on work in coherent periods. Glad I decided to cut back on hours so I don't lose my mind.

==== plan #44 ====

* Ensure release tasks are on track
** follow up with Pablo Vazquez
** follow up with docs
** push devs for release notes, test files, demos
* Set up snap and rpm packaging


== April 11 - April 18 / #42 ==
* Release management: bcon3
* Fix up steps for bcon3 to include buildbot infra changes needed for beta branch
* Create release todo for 2.83
* Look into an issue alerted to by Dalai Felinto (T71290 status)
* Test latest blender-tweaks branch on Docker, this now works


==== review #42 ====
* Still finding it hard to find a good balance with COVID-19 induced mess. Very tired so not as effective as normally would be. Devops work suffered as a result

==== plan for #43 ====
* Do phabricator stuff mostly, especially now that docker stuff appears to be off the list finally

== April 6 - April 9 / #41 ==
* Release management documentation
* Communication meeting
* Short week due to starting Easter holidays

==== review #41 ====
* Biggest issue still tightened measures due to COVID-19 outbreak, and this will continue until May 13th at least. Some teachers said to mentally prepare for homeschooling until end of school year, which is begin of June.

==== plan for #42 ====
* #42 starts on Tuesday April 14
* Ensure devops plan is drawn up and discussed with Sergey S
* Roll 2.83 bcon3
* Act on several [[Process/Release_Checklist/Actions_Per_Bcon|bcon3 action points]]

== March 30 - April 4 / #40 ==
* Some docker debugging
* Primarily worked on release management items
** [[User:JesterKing/LtsProcess]] (Long Term Support)
** [[Process/Release_Checklist/Actions_Per_Bcon]] (Start of release playbook per bcon phase)

==== review #40 plan ====
* Did not manage to do as much code-related work as planned due to same reasons as last week. This will go on at least until '''May 13th''' as announced by Finnish government begin this week. I'll try to juggle as much as I can, though.

==== plan for #41 ====
* Work out more release management steps, especially wrt LTS
** documentation/manual branching and committing policies / playbooks
** triaging playbook considerations and updates
** bug fixing playbook considerations and updates
** clarify branching and tagging strategy
*** clarify steps/relative dates per bcon from developer PoV
* hopefully have better grip on daily life to be able to devote more time to infra, but release management has now priority
* roll into bcon3 for 2.83 -> stabilizing branch and all that, including for documentation!
* '''note''' working until bcon3 roll-over, after that easter holidays starting Friday April 10th until Monday April 13th


== March 23 - March 28 / #39 ==
* Progress canned response app. Doing an entire app is more complex than expected. Still progressing well, though.
* A few hours looking into the phabricator docker setup, but not fixed yet
* Crashpad testing, it works! Thanks to update from LazyDodo to crashpad patch compiling went flawlessly. DIF (Debug Information File) uploading needed to get proper unwinding done of callstack
* Struggling with finding good daily rythm in these exceptional times now with the entire family home all the time. Juggling with attention between work and three kids

==== review #39 plan ====
* didn't get to patch/review checklist
* struggling with compartmentalizing a bit
* need to structure days differently, old, pre-covid19 approach not adequate
** Group tasks more per day
** Release management: Monday, Saturday
** Infra features: Tuesday, Wednesday
** Friday misc day
** Start each day with communication tasks, end with communication tasks (~30min each side)

==== plan for #40 ====
* complete canned responses
* get started with checklist
* docker
* release management (expand playbook for release cycle and actual release steps)

== March 16 - March 21 / #38 ==
* Mess a bit up with 2.83 bcon2, but all ok
* Sick whole week with sore lungs, coughing, fatigue and muscle pain

==== plan for #39 ====
* canned responses
* start patch/review checklist
* crashpad test
* phabricator docker debug, help would be appreciated.

== March 9 - March 14 / #37 ==
* Fix `transaction.search` to have `mergedinto` and `mergedfrom`
* Debug phabricator docker. I have a running version, but unclear why creating from fresh clone does not complete build properly
* Kids sick

== March 2 - March 7 / #36 ==
* Not much due to most of family being sick for the entire week
* Start looking into `mergedinto` data missing from `transaction.search`

== February 23 - February 29 / #35 ==
* Fix to Phabricator for Dalai: transaction.search is missing subtype transaction information
* GSoC chats
* phabricator
* sentry/crashpad


== Ferbuary 17 - February 22 / #34 ==
* sentry/crashpad
* phabricator
* more AFK, recovery from  previous illness not complete

== February 10 - February 15 / #33 ==
* release 2.82 / bcon5 (Steam, Windows Store)
* AFK due to illness

== February 3 - February 8 / #32 ==
* release 2.82 prep / bcon4
* write phabricator tool to reset task view/edit policies when they have been changed to obj.maniphest.author
* investigate why users can't edit their own tasks when they are not a member in the Moderators project

== January 27 - February 1 / #31 ==
* continued work on task planning (buildbot, git, phab, etc)
* test some approaches to patch building, ongoing
* investigate git lfs for blender manual (read up on different server implementations)

== January 20 - January 25 / #30 ==
* dev contact (e-cycles)
* phabricator docker image update (to latest OpenSUSE 15.1)
* planning tasks based on milestones from Dalais

== January 13 - January 18 / #29 ==
* update Cycles stand-alone repository
** fix stand-alone app to work with latest Cycles
* study & tinker up on docker / saltstack

== January 06 - January 11 / #28 ==
* roll out blender-v2.82-release branch
* chip away at improving cycles_commits_sync.py
** unlink empty patches
** handle duplicate patches (due to merge)
*** not using --no-merges, since it ''is'' possible devs fix conflicts in merge commits
*** md5 of actual patch content
*** prune based on content hash
** started phabricator-setup-from-scratch script