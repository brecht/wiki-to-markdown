==GSoC Week7 Status==

Hello everyone, here's my week 7 report!

===Things have done===

1. Software rendering now working! (Still have some bugs, and it does not produce the exact same result as my standalone program. Need inspection)

2. Intersection line calculation is also OK. the calculation result can then be displayed using DPIX realtime method.

3. Removed old line style data in struct SceneLANPR. DPIX now uses the first line layer for style reference.

4. Added support structure for object/material/collection-based line selection, function not implenented yet. (artists demand this)

5. Dealed with many small bugs in software rendering, mainly:

   - Depth value incorrect due to inconsistent log->linear process 0065c90(now ok)
   - Earcut triangulate produce strange result. b612938(now use default BEAUTY method, seems ok)
   - normal matrix error a63777b(now fixed)

Actually I could consider all the basic functions in LANPR (both CPU and GPU side) are working now. What I need to do next should be making it more compatible with existing render pipeline, and of course doucument most of the main usages. 

Incorrect result are displayed in the image.
[[File:LANPR_Week7_1.png|thumb|Incorrect result on the edge]]
[[File:LANPR_Week7_2.png|thumb|Intersection calculation result (blocks on the surfaces)]]

===Next week===

1. Add multithread support for software side (which already has thread-safe structures so would be easy.)

2. Make LANPR work more stable:

   - Try to solve the occlusion test bugs.
   - LANPR internally uses 64bit float format, but the obmat is converted directly from 32bit ob->obmat so this could have some pricision problem, I'll see what I can do. (and it may be the cause of current occlusion bugs)

3. Document LANPR usage. Also modify the ui to be more easily understood.

4. If I had more time, add object/material/collection-based line selection functions.

===Questions===

Hey, I got some more questions, and most of them I can't seem to find a existing solution. And if I want to make LANPR into a useful thing then I will have to solve these.

1. About BMesh: I want to access each face's material infomation after triangulation. I can't find anything that achieve this in BMHead. And since the geometry has changed, I can't access the material info in Mesh using the new index in BMesh. How do I achieve this?

2. About render to image: Still can't get render_to_image to draw anything. I tried using multisample buffer and normal texture buffer, the init and drawing code is exactly the same as what I've done for the viewport. (and viewport actually draw them perfectly). One strange thing that happens is that in commit 3ea9277 file lanpr_engine.c line 456 and 458, When I run this code, it seems that it uses the shader program and matrix from line 458 (which is for calculated line result) to render the content in line 456 (which contains a default cube batch). How could this even happen? (And this is the only time I got any dispaly on render_to_image) If any developer happend to have some time to check this ikt would be great. I don't know if I missed anything but from eevee's render_to_image I can't see any special treatments either.

3. Read Collection RNA property from blend file gives invalid pointer (very likely the last run time memory address instead of the newly created one). Any thing could cause this to happen? 

Thanks and have a great weekend!

YimingWu