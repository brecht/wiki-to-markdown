==Basic Usage==

Switching to DPIX Mode will immediately trigger real time line extraction.

If you don't have at least one layer, you need to create one using the button "Add Line Layer". Then you should be able to see the line result in your viewport in no time.

Line style adjustment is the same as the ones in software mode.

note: If you already have multiple layers in Software Mode, DPIX will only choose the top most one to draw in viewport.

===Notes on Intersection Calculation===

[[File:LANPR_Doc_08.png|thumb|Intersection option enabled]]

You can also draw intersection lines in DPIX mode, but it requires cached software calculation result. In DPIX tab, you can directly enable/disable intersection calculation. If enabled, click "Recalculate" button will do the job, but please notice, this is the same as software calculation, you need an active camera in your scene, and only regions within active camera's viewport can display intersection lines.

==Styles==

[[File:LANPR_Doc_09.png|thumb|DPIX Line styles]]

===Crease Control===

Crease Threshold means "How sharp an edge can be considered as a crease", the value is adjustable. Default value is 0.7, which gives a face angle at around 130 deg.

Crease Fade is another threshold valve, if set greater than Crease Threshold, then LANPR allows edges within the threshold range to be displayed as a gradient.

===Depth Controlled Style===

[[File:LANPR_Doc_14.png|thumb|Depth controlled rendering of a cityscape]]

Depth value can be used to control line width and alpha in DPIX mode.

Influence values decides how much strength will be applied to.

Curve values controls the exponent value of original pixel depth. When this value is bigger, line styles tend to be more sensitive to depth, and if it's a smaller number the influence looks more "dull". The image on the right shows depth controlled style on a cityscape rendering in LANPR.

note: Depth value is in a range of 0 to 1 from near to far clipping plane. So clipping value will influence the behavior of these styles.

note: DPIX line alpha is pre-multiplied with background color.

==Known Issues==

Currently DPIX implementation have some slight visual errors when a long line crosses camera plane, it rarely happens, but it suggests clipping code have something to do with it. Future improvements is needed.