= LineArt Development Log Continued (2020/09) =

[[User:Yiming/LANPR_GP_New_Solutions|Previous Log]]

=== 0901 ===

* [Done 0911] <s>Reported that lack of far-clipping is also causing trouble. Because we rarely extend anything beyond far-clipping, didn't really show at first. Will do the far clipping part as well.</s>

=== 0910 ===

* [Done] <s>Per-object vertex transformation and crease marking.</s>
* [Done 0911] <s>Far clipping adaptation.</s>

=== 0911 ===

Solved far clipping issue, and it's working with new data iteration path.

Can choose to show or hide lines on the clipping plane.

Potential use: Depth stack to achieve camera distortion, very good for Anime kind of stuff.

Fixed clipping-plane-induced topology linking errors.

=== 0913 ===

Fixed occlusion errors in intersection stage, caused buy mis-linking of queue data.

=== 0918 ===

* [Should be done] <s>Reported crashes during adjusting the numbers. Suspected to be caused by cancelling mechanism.</s>
** Didn't lock the thread while the object loading function is still cleaning up, resulting in accidental accesses to now invalid mesh data
** Temporally disabled cursor progress, this causes problem on XWindow systems.
* Reportedly not working with EEVEE motion blur turned on.
** Currently still not working and it's not going to be working for a while. Due to lack of updating status in render pipeline (don't know if it's a motion blur update or an actual frame update). For now the strokes will be rendered when motion blur is on, but each frame was triggered twice, so twice the time that Line Art runs.

Added supplemental debug information. (`G.debug_value==4000`)

=== 0921 ===

* [Done 0922, also see notes below] <s>Chaining reportedly not working properly with linked meshes.</s>
** This is due to overlapping between linked object and make-local ones.
** Also: this also need to work, as deleting linked object or make-local ones will result in disappearance of both. This needs to be supported by line art, as at least one of them should be automatically turned off from calculation.
* [Done 0922] <s>Reported occlusion issue with some architectural scenes.</s> Two bugs actually:
** Incorrect internal FOV in vertical camera configurations.
** A typo in vector 2D intersection function.

=== 0922 ===

* [See notes] Reported problem with rendering, modifiers seems not applied sometimes.
** Multiple updates during rendering will include evaluations for renderer and also for viewport, in parallel, so it creates some race conditions in caching. Now the code path will ensure all the data only cached once per rendering frame.
** Motion blur still doesn't work, it will produce lines, but will cause line art to run twice. Please bake when you can.

=== 0923 ===

Fixed: Compiler warnings.

Added a new function which provides flexible baking. Now you can bake line art strokes in every X frames or only bake them in existing GPencil key frames.

Some tweaks of updating code path to allow stable updates in viewport/render and with lock-interface mode.

=== 0925 ===

* [Done] <s>Viewport auto update is broken.</s>
** To solve this we need to have retained memory configuration (cache from line art is not deleted until next cache happens), this would be beneficial to later optimizations like fast adjustments of arguments and so on...

=== 0927 ===

Added a new flag "No Intersection" for Object/Collections, so that these meshes will not generate intersection lines within the scene, this is useful for dense particle like objects, e.g. trees and grass objects, where intersection features are not visually prominent.

Fixed logic for usage flags, so now object flags will override collection flags properly. e.g. you set a parent collection to "exclude" but you can now set a child object to "include".

Reported line type determination problem in edge marked lines. Turns out it's a flaw in line marking design in line art calculating stage. Now fixed.

Now uses angle instead of cosine value for crease threshold. (finally LOL)

Looked into how to access vertex weight data during loading. This might be useful later. Notes:

* Use `ED_vgroup_vert_weight` for accessing an specific weight of a vertex. This might be a little bit slow because it's vertex based and we need face/lines.
* Face Map name placeholder is `struct bFaceMap`.

[[User:Yiming/LineArt_Development_Log_2020_10|Next Log]]