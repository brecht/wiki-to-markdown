==Week 3 Report==

===Things from the last week===

#'''Fixed''' engine_free() callback, now no memory leaks on LANPR side.
#'''Fixed''' LANPR shader free problems.
#Software mode now support camera lens shift. Should be usable with any camera configurations now. (Only fisheye not implemented)
#Implemented Image-space chain connection to reduce jaggy lines.
#After some back and forth, finally settled down on a more solid design for Bridging LANPR with Blender and how to use GPencil modifiers. More information to come.
#Minor bug fixes.

===Still to come===

#Intersection lines needs near-far clipping before perspective division and coordinate shift. Need to prove this isn't needed.
#Perspective correction for occlusion points on the chains or use world-space split points.
#Still have some VBO utility structures not cleared correctly when LANPR exits.
#Move stroke generation modifier into Object modifiers as part of the design.
#Implement the new LANPR to GP path, including selections and generation.
#Image-space chaining reported to have some bugs in it. Look into it during the weekend.

I typically do more things in the weekends because I'm still enrolling in school. So expect updates before Monday. Happy weekend every one! :)

-----

YimingWu