= Effects in LANPR engine =

LANPR engine offers some basic line effects that can be easily achieved.

(Note: Effects are configured per layer.)

== Simple effects ==

'''Depth controlled effect''' is pretty straightforward. Depth currently can affect line thickness and alpha.

* Use <code>Strength</code> value to adjust the effectiveness.
* Use <code>Curve</code> value to set the exponent coefficient of this influence. This is to make the influence “sharper” or “smoother”.

(Note: Instead of using an absolute distance parameter, depth controlled effect maps the range between camera z-near/z-far into 0-1. So different camera clipping distance may result in unexpected different results)

'''Taper effect''' is only available when line chaining is enabled. It tapers the line tip using given <code>Distance</code> and <code>Strength</code>, and you can make two tips of a line to taper differently.

== Normal based line weight ==

This effect is for achieving a more dynamic line expression in LANPR engine. You need to have a control object (Typically an Empty object) for direction reference.

This effect currently has two '''Mode'''s.

* '''Directional mode''' is for imitating sun light effect. Line weight will be controlled by the dot product of the ''surface'' normal and the z+ direction of your control object.
* '''Point mode''' works basically the same way, but it imitates point light. (Maybe not working correctly at the moment)

There will be default values below that allow you to view the effect immediately after setting the control object. Meanings of the values are explained below:

* '''Ramp Begin/End''': This is to adjust the range where the thickness gradient takes place. The value works by scaling the normal dot product value into the desired range.
* '''Thickness Begin/End''': These values are linearly mapped to assigned gradient area of the line.
* '''Invert''': The ramp is inverted.