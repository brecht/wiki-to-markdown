=Google Summer of Code - 2021 Projects=

<div class="bd-lead">

Progress updates for 2021
</div>


==Current State==
'''1<sup>st</sup> evaluation - ''' 12 - 16 July<br/>
'''Final submission - ''' 23 - 30 August

==Communication==
===Development Forum===
All Blender and code related topics will go to the devtalk forum. Here students will also post their weekly reports, and can ask for feedback on topics or share intermediate results with everyone.

https://devtalk.blender.org/c/blender/summer-of-code

===blender.chat===
For real-time discussions between students and mentors.
Weekly meetings will be scheduled here as well. 

https://blender.chat

==Projects==
Google has granted 8 projects for the Blender Foundation.

----

===Adaptive Cloth Simulator===
by '''Ish Bosamiya'''<br />
''Mentors:'' Sebastian Parborg, Sebastián Barschkis
 
* [[User:Ishbosamiya/GSoC2021/Proposal|Proposal]]
* [https://devtalk.blender.org/t/gsoc-2021-continued-development-on-adaptive-cloth-simulator-for-blender-weekly-reports/19205?u=ish_bosamiya Weekly Report]
* [https://devtalk.blender.org/t/gsoc-2021-continued-development-on-adaptive-cloth-simulator-for-blender-feedback/19206?u=ish_bosamiya Feedback]
* [[User:Ishbosamiya/GSoC2021/FinalReport|Final Report]]

----

===Curve Improvements===
by '''Dilith Jayakody'''<br />
''Mentors:'' Hans Goudey, Falk David
 
* [https://wiki.blender.org/wiki/User:Dilithjay/GSoC_2021/Curve_Improvements Proposal]
* [https://devtalk.blender.org/t/gsoc-2021-curve-improvements-feedback/19109 Feedback]
* [https://devtalk.blender.org/t/gsoc-2021-curve-improvements-weekly-reports/19108 Weekly Report]
* [https://wiki.blender.org/wiki/User:Dilithjay/GSoC_2021/Final_Report Final Report]

----

===Display simulation data for rigid bodies and cloth===
by '''Soumya Pochiraju'''<br />
''Mentors:'' Sebastián Barschkis, Sebastian Parborg
 
* [[Displaying simulation data for rigid bodies and cloth|Proposal]]
* [https://devtalk.blender.org/t/gsoc-2021-displaying-simulation-data-for-rigid-bodies-and-cloth-weekly-reports/19061 Weekly Report]
* [[User:Forest/Final_Report|Final report]]
 
----

===Knife Tool Improvements===
by '''Cian Jinks'''<br />
''Mentors:'' Howard Trickey
 
* [https://wiki.blender.org/wiki/User:HobbesOS/GSOC2021/Proposal Proposal]
* [https://devtalk.blender.org/t/gsoc-2021-knife-tool-improvements-feedback/19047 Feedback]
* [https://devtalk.blender.org/t/gsoc-2021-knife-tool-improvements-weekly-reports/ Weekly Report]
* [https://wiki.blender.org/wiki/User:HobbesOS/GSOC2021/FinalReport Final Report]
 
----

===Porting popular modifiers to Geometry Nodes===
by '''Fabian Schempp'''<br />
''Mentors:'' Jacques Lucke, Hans Goudey
 
* [[User:FabianSchempp/Proposal|Proposal]]
* [https://devtalk.blender.org/t/gsoc-2021-porting-popular-modifiers-to-geometry-nodes-weekly-reports/ Weekly Report]
* [https://devtalk.blender.org/t/gsoc-2021-porting-popular-modifiers-to-geometry-nodes-feedback/ Feedback] 
* [[User:Fabian Schempp/FinalReport|Final Report]]
----

===Regression Testing of Geometry Nodes===
by '''Himanshi Kalra'''<br />
''Mentors:'' Habib Gahbiche, Jacques Lucke
 
* [[User:HimanshiKalra/GSoC21/Proposal|Proposal]]
* [https://devtalk.blender.org/t/gsoc-2021-regression-testing-of-geometry-nodes-weekly-reports/18954 Weekly Reports]
* [[User:HimanshiKalra/GSoC21/log|Daily Log]]
* [[User:HimanshiKalra/GSoC21/FinalReport|Final Report]]
 
----

===UV Editor Improvements===
by '''Siddhartha Jejurkar'''<br />
''Mentors:'' Campbell Barton
 
* [[User:Sidd017/Proposal|Proposal]]
* [https://devtalk.blender.org/t/gsoc-2021-uv-editor-improvements-weekly-reports/19060 Weekly Report]
* [[User:Sidd017/FinalReport|Final Report]]
 
----

===Video Sequence Editor strip previews and modification indicators===
by '''Aditya Jeppu'''<br />
''Mentors:'' Richard Antalík
 
* [https://wiki.blender.org/wiki/User:Quantimoney/GSoC2021/Proposal Proposal]
* [https://devtalk.blender.org/t/gsoc-2021-video-sequence-editor-strip-previews-and-modification-indicators-feedback/19096 Feedback]
* [https://wiki.blender.org/wiki/User:Quantimoney/GSoC2021/Weekly_Report Weekly Report]
* [https://wiki.blender.org/wiki/User:Quantimoney/GSoC2021/Final_Report Final Report]