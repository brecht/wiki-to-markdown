= Weekly Reports 2021 =
------

[[User:Rjg/Reports/2020|Previous Reports 2020]]

------
== January 04 - 10 (WN 01) ==
All tickets are listed only once with their last status change made by me at the end of the week. Note: Due to technical issues nearly no work was done on 2020-01-07.

=== Bug Tracker (104) ===
Confirmed (12):
* [https://developer.blender.org/T84397 T84397], [https://developer.blender.org/T84394 T84394], [https://developer.blender.org/T84402 T84402], [https://developer.blender.org/T84444 T84444], [https://developer.blender.org/T84447 T84447], [https://developer.blender.org/T84512 T84512], [https://developer.blender.org/T84513 T84513], [https://developer.blender.org/T84545 T84545], [https://developer.blender.org/T84539 T84539], [https://developer.blender.org/T84529 T84529], [https://developer.blender.org/T84532 T84532], [https://developer.blender.org/T84299 T84299]

Resolved (3):
* [https://developer.blender.org/T84386 T84386], [https://developer.blender.org/T83647 T83647], [https://developer.blender.org/T84217 T84217]

Invalid (47):
* [https://developer.blender.org/T84067 T84067], [https://developer.blender.org/T84061 T84061], [https://developer.blender.org/T83902 T83902], [https://developer.blender.org/T84153 T84153], [https://developer.blender.org/T84162 T84162], [https://developer.blender.org/T84171 T84171], [https://developer.blender.org/T84083 T84083], [https://developer.blender.org/T84090 T84090], [https://developer.blender.org/T84120 T84120], [https://developer.blender.org/T84353 T84353], [https://developer.blender.org/T84355 T84355], [https://developer.blender.org/T84347 T84347], [https://developer.blender.org/T84377 T84377], [https://developer.blender.org/T84403 T84403], [https://developer.blender.org/T84363 T84363], [https://developer.blender.org/T84361 T84361], [https://developer.blender.org/T84357 T84357], [https://developer.blender.org/T84399 T84399], [https://developer.blender.org/T84441 T84441], [https://developer.blender.org/T84440 T84440], [https://developer.blender.org/T84443 T84443], [https://developer.blender.org/T84446 T84446], [https://developer.blender.org/T84332 T84332], [https://developer.blender.org/T84457 T84457], [https://developer.blender.org/T84131 T84131], [https://developer.blender.org/T84194 T84194], [https://developer.blender.org/T84201 T84201], [https://developer.blender.org/T84198 T84198], [https://developer.blender.org/T84478 T84478], [https://developer.blender.org/T84499 T84499], [https://developer.blender.org/T84000 T84000], [https://developer.blender.org/T84232 T84232], [https://developer.blender.org/T84213 T84213], [https://developer.blender.org/T84219 T84219], [https://developer.blender.org/T84127 T84127], [https://developer.blender.org/T84102 T84102], [https://developer.blender.org/T84281 T84281], [https://developer.blender.org/T84542 T84542], [https://developer.blender.org/T84538 T84538], [https://developer.blender.org/T84265 T84265], [https://developer.blender.org/T84157 T84157], [https://developer.blender.org/T84295 T84295], [https://developer.blender.org/T84174 T84174], [https://developer.blender.org/T84308 T84308], [https://developer.blender.org/T84319 T84319], [https://developer.blender.org/T84323 T84323], [https://developer.blender.org/T84318 T84318]

Needs Developer to Reproduce (2):
* [https://developer.blender.org/T84422 T84422], [https://developer.blender.org/T83375 T83375]

Needs Information from User (17):
* [https://developer.blender.org/T84368 T84368], [https://developer.blender.org/T84356 T84356], [https://developer.blender.org/T84329 T84329], [https://developer.blender.org/T84350 T84350], [https://developer.blender.org/T84343 T84343], [https://developer.blender.org/T84333 T84333], [https://developer.blender.org/T84372 T84372], [https://developer.blender.org/T84390 T84390], [https://developer.blender.org/T84381 T84381], [https://developer.blender.org/T84439 T84439], [https://developer.blender.org/T84436 T84436], [https://developer.blender.org/T84429 T84429], [https://developer.blender.org/T84486 T84486], [https://developer.blender.org/T84514 T84514], [https://developer.blender.org/T84541 T84541], [https://developer.blender.org/T84528 T84528], [https://developer.blender.org/T84537 T84537 ]

Merged Duplicate (15):
* [https://developer.blender.org/T84359 T84359], [https://developer.blender.org/T84365 T84365], [https://developer.blender.org/T84351 T84351], [https://developer.blender.org/T84337 T84337], [https://developer.blender.org/T84396 T84396], [https://developer.blender.org/T84400 T84400], [https://developer.blender.org/T84366 T84366], [https://developer.blender.org/T84412 T84412], [https://developer.blender.org/T84411 T84411], [https://developer.blender.org/T84450 T84450], [https://developer.blender.org/T84454 T84454], [https://developer.blender.org/T84519 T84519], [https://developer.blender.org/T84544 T84544], [https://developer.blender.org/T84486 T84486], [https://developer.blender.org/T84540 T84540]

Investigated / Tagged / Helped / Other (7):
* [https://developer.blender.org/T84355 T84355], [https://developer.blender.org/T84309 T84309], [https://developer.blender.org/T84395 T84395], [https://developer.blender.org/T84438 T84438], [https://developer.blender.org/T84435 T84435]
* Helped Vilem Duha understand the reloading issue in the Blenderkit add-on
* Split the AMD graphics card / driver related from [[T82856https://developer.blender.org/T82856|T82856]] into subtasks

=== Patches and Commits ===

Patches (0):
* 

Commits (0):
*

=== On-Boarding of Developers ===
* Helping Calra commit changes without arcanist

== January 11 - 17 (WN 02) ==
All tickets are listed only once with their last status change made by me at the end of the week.

=== Bug Tracker (94) ===
Confirmed (19):
* [https://developer.blender.org/T84586 T84586], [https://developer.blender.org/T84588 T84588], [https://developer.blender.org/T84616 T84616], [https://developer.blender.org/T84646 T84646], [https://developer.blender.org/T84655 T84655], [https://developer.blender.org/T84645 T84645], [https://developer.blender.org/T84654 T84654], [https://developer.blender.org/T84702 T84702], [https://developer.blender.org/T84708 T84708], [https://developer.blender.org/T84701 T84701], [https://developer.blender.org/T84743 T84743], [https://developer.blender.org/T84742 T84742], [https://developer.blender.org/T84739 T84739], [https://developer.blender.org/T84734 T84734], [https://developer.blender.org/T84767 T84767], [https://developer.blender.org/T84777 T84777], [https://developer.blender.org/T84777 T84777], [https://developer.blender.org/T84757 T84757], [https://developer.blender.org/T84780 T84780]

Resolved (3):
* [https://developer.blender.org/T77499 T77499], [https://developer.blender.org/T84619 T84619], [https://developer.blender.org/T84773 T84773]

Invalid (30):
* [https://developer.blender.org/T75917 T75917], [https://developer.blender.org/T77063 T77063], [https://developer.blender.org/T83976 T83976], [https://developer.blender.org/T84296 T84296], [https://developer.blender.org/T84291 T84291], [https://developer.blender.org/T84175 T84175], [https://developer.blender.org/T84271 T84271], [https://developer.blender.org/T77741 T77741], [https://developer.blender.org/T83883 T83883], [https://developer.blender.org/T84615 T84615], [https://developer.blender.org/T84620 T84620], [https://developer.blender.org/T84622 T84622], [https://developer.blender.org/T84611 T84611], [https://developer.blender.org/T84631 T84631], [https://developer.blender.org/T84632 T84632], [https://developer.blender.org/T84333 T84333], [https://developer.blender.org/T84343 T84343],  [https://developer.blender.org/T84656 T84656], [https://developer.blender.org/T84666 T84666], [https://developer.blender.org/T84704 T84704], [https://developer.blender.org/T84720 T84720], [https://developer.blender.org/T84745 T84745], [https://developer.blender.org/T84716 T84716], [https://developer.blender.org/T84755 T84755], [https://developer.blender.org/T84756 T84756], [https://developer.blender.org/T83866 T83866], [https://developer.blender.org/T83913 T83913], [https://developer.blender.org/T84286 T84286], [https://developer.blender.org/T84283 T84283], [https://developer.blender.org/T82604 T82604]

Needs Developer to Reproduce (2):
* [https://developer.blender.org/T82675 T82675], [https://developer.blender.org/T84766 T84766]

Needs Information from User (17):
* [https://developer.blender.org/T77191 T77191], [https://developer.blender.org/T84690 T84690], [https://developer.blender.org/T84695 T84695], [https://developer.blender.org/T84696 T84696], [https://developer.blender.org/T84706 T84706], [https://developer.blender.org/T84681 T84681], [https://developer.blender.org/T84677 T84677], [https://developer.blender.org/T84738 T84738], [https://developer.blender.org/T84737 T84737], [https://developer.blender.org/T84747 T84747], [https://developer.blender.org/T84726 T84726], [https://developer.blender.org/T84768 T84768], [https://developer.blender.org/T84762 T84762], [https://developer.blender.org/T84763 T84763], [https://developer.blender.org/T84752 T84752], [https://developer.blender.org/T84770 T84770], [https://developer.blender.org/T84772 T84772]

Merged Duplicate (12):
* [https://developer.blender.org/T84040 T84040], [https://developer.blender.org/T84195 T84195], [https://developer.blender.org/T84199 T84199], [https://developer.blender.org/T83894 T83894], [https://developer.blender.org/T84618 T84618], [https://developer.blender.org/T84617 T84617], [https://developer.blender.org/T84630 T84630], [https://developer.blender.org/T84633 T84633], [https://developer.blender.org/T84635 T84635], [https://developer.blender.org/T84653 T84653], [https://developer.blender.org/T84744 T84744], [https://developer.blender.org/T84754 T84754]

Investigated / Tagged / Helped / Other (11):
* [https://developer.blender.org/T84587 T84587], [https://developer.blender.org/T84575 T84575], [https://developer.blender.org/T84558 T84558], [https://developer.blender.org/T84627 T84627], [https://developer.blender.org/T84577 T84577], [https://developer.blender.org/T84609 T84609], [https://developer.blender.org/T84661 T84661], [https://developer.blender.org/T84636 T84636], [https://developer.blender.org/T83818 T83818], [https://developer.blender.org/T83353 T83353], [https://developer.blender.org/T84736 T84736]

=== Patches and Commits ===

Patches (4):
* [https://developer.blender.org/D10072 D10072] - Fix cache handling in `rna_Particle_uv_on_emitter` when `particle->num_dmcache` is `DMCACHE_ISCHILD`. This case wasn't considered by the implementation before and led to an incorrect negative offset for a pointer. 
* [https://developer.blender.org/D10089 D10089] - Update Power Sequencer to use current Python API for selection
* [https://developer.blender.org/D10108 D10108] - Properly register optional keymap for Bool Tools based on user preferences
* [https://developer.blender.org/D10110 D10110] - Fix versioning code for the Set Alpha node

Commits (3):
* [https://developer.blender.org/rBf5c0ef52cf2f4ae333269eec33e5bd7a89a00a23 rBf5c0ef52cf2f4ae333269eec33e5bd7a89a00a23] - Fix [https://developer.blender.org/T84588 T84588]: Cache access in `rna_Particle_uv_on_emitter`
* [https://developer.blender.org/rBA87889365fe562e594fa989ef6315c001e2cbe63a rBA87889365fe562e594fa989ef6315c001e2cbe63a] - Fix [https://developer.blender.org/T84616 T84616]: Power Sequencer 2.90 API update
* [https://developer.blender.org/rB27b78c9c94baf6fa43268e851de58da96f7d7123 rB27b78c9c94baf6fa43268e851de58da96f7d7123] - Compositor: "Save as Render" for the file output node

=== On-Boarding of Developers ===
* 

== January 18 - 24 (WN 03) ==
All tickets are listed only once with their last status change made by me at the end of the week.

=== Bug Tracker (102) ===
Confirmed (9):
* [https://developer.blender.org/T84813 T84813], [https://developer.blender.org/T84650 T84650], [https://developer.blender.org/T84752 T84752], [https://developer.blender.org/T84898 T84898], [https://developer.blender.org/T84908 T84908], [https://developer.blender.org/T84911 T84911], [https://developer.blender.org/T84636 T84636], [https://developer.blender.org/T84962 T84962], [https://developer.blender.org/T84959 T84959]

Resolved (4):
* [https://developer.blender.org/T84786 T84786], [https://developer.blender.org/T84822 T84822], [https://developer.blender.org/T84889 T84889], [https://developer.blender.org/T84844 T84844]

Invalid (25):
* [https://developer.blender.org/T84801 T84801], [https://developer.blender.org/T84835 T84835], [https://developer.blender.org/T84851 T84851], [https://developer.blender.org/T84861 T84861], [https://developer.blender.org/T84886 T84886], [https://developer.blender.org/T84890 T84890], [https://developer.blender.org/T84892 T84892], [https://developer.blender.org/T83545 T83545], [https://developer.blender.org/T84523 T84523], [https://developer.blender.org/T84107 T84107], [https://developer.blender.org/T84575 T84575], [https://developer.blender.org/T77191 T77191], [https://developer.blender.org/T84558 T84558], [https://developer.blender.org/T84350 T84350], [https://developer.blender.org/T84919 T84919], [https://developer.blender.org/T84897 T84897], [https://developer.blender.org/T84381 T84381], [https://developer.blender.org/T84442 T84442], [https://developer.blender.org/T84961 T84961], [https://developer.blender.org/T84952 T84952], [https://developer.blender.org/T84696 T84696], [https://developer.blender.org/T84981 T84981], [https://developer.blender.org/T84762 T84762], [https://developer.blender.org/T84738 T84738], [https://developer.blender.org/T84747 T84747]

Needs Developer to Reproduce (3):
* [https://developer.blender.org/T84807 T84807], [https://developer.blender.org/T83936 T83936], [https://developer.blender.org/T84752 T84752]

Needs Information from User (22):
* [https://developer.blender.org/T84802 T84802], [https://developer.blender.org/T84794 T84794], [https://developer.blender.org/T84818 T84818], [https://developer.blender.org/T84856 T84856], [https://developer.blender.org/T84841 T84841], [https://developer.blender.org/T84831 T84831], [https://developer.blender.org/T84877 T84877], [https://developer.blender.org/T84917 T84917], [https://developer.blender.org/T84916 T84916], [https://developer.blender.org/T84918 T84918], [https://developer.blender.org/T84925 T84925], [https://developer.blender.org/T84910 T84910], [https://developer.blender.org/T84902 T84902], [https://developer.blender.org/T84963 T84963], [https://developer.blender.org/T84956 T84956], [https://developer.blender.org/T84950 T84950], [https://developer.blender.org/T84949 T84949], [https://developer.blender.org/T84947 T84947], [https://developer.blender.org/T84966 T84966], [https://developer.blender.org/T84984 T84984], [https://developer.blender.org/T84983 T84983], [https://developer.blender.org/T84977 T84977]

Merged Duplicate (26):
* [https://developer.blender.org/T84812 T84812], [https://developer.blender.org/T84790 T84790], [https://developer.blender.org/T84792 T84792], [https://developer.blender.org/T84817 T84817], [https://developer.blender.org/T84787 T84787], [https://developer.blender.org/T84775 T84775], [https://developer.blender.org/T84793 T84793], [https://developer.blender.org/T84828 T84828], [https://developer.blender.org/T84827 T84827], [https://developer.blender.org/T84826 T84826], [https://developer.blender.org/T84793 T84793], [https://developer.blender.org/T84858 T84858], [https://developer.blender.org/T84843 T84843], [https://developer.blender.org/T84839 T84839], [https://developer.blender.org/T84768 T84768], [https://developer.blender.org/T84887 T84887],  [https://developer.blender.org/T84887 T84887], [https://developer.blender.org/T84891 T84891], [https://developer.blender.org/T84893 T84893], [https://developer.blender.org/T84309 T84309], [https://developer.blender.org/T84537 T84537], [https://developer.blender.org/T84922 T84922], [https://developer.blender.org/T84924 T84924], [https://developer.blender.org/T84914 T84914], [https://developer.blender.org/T84955 T84955], [https://developer.blender.org/T84976 T84976]

Investigated / Tagged / Helped / Other (13):
* [https://developer.blender.org/T84799 T84799], [https://developer.blender.org/T84848 T84848], [https://developer.blender.org/T84840 T84840], [https://developer.blender.org/T84885 T84885], [https://developer.blender.org/T83823 T83823], [https://developer.blender.org/T84528 T84528], [https://developer.blender.org/T84831 T84831], [https://developer.blender.org/T84962 T84962], [https://developer.blender.org/T84939 T84939], [https://developer.blender.org/T84681 T84681], [https://developer.blender.org/T84986 T84986], [https://developer.blender.org/T84985 T84985], [https://developer.blender.org/T84690 T84690]

=== Patches and Commits ===

Patches (0):
* 

Commits (0):
*

=== On-Boarding of Developers ===
* 

== January 25 - 31 (WN 04) ==
All tickets are listed only once with their last status change made by me at the end of the week.

=== Bug Tracker (121) ===
Confirmed (11):
* [https://developer.blender.org/T85027 T85027], [https://developer.blender.org/T85023 T85023], [https://developer.blender.org/T75258 T75258], [https://developer.blender.org/T84782 T84782], [https://developer.blender.org/T85093 T85093], [https://developer.blender.org/T85107 T85107], [https://developer.blender.org/T85142 T85142], [https://developer.blender.org/T85144 T85144], [https://developer.blender.org/T85121 T85121], [https://developer.blender.org/T85188 T85188], [https://developer.blender.org/T85225 T85225]

Resolved (3):
* [https://developer.blender.org/T75433 T75433], [https://developer.blender.org/T72875 T72875], [https://developer.blender.org/T85114 T85114]

Invalid (37):
* [https://developer.blender.org/T84528 T84528], [https://developer.blender.org/T84986 T84986], [https://developer.blender.org/T84772 T84772], [https://developer.blender.org/T85044 T85044], [https://developer.blender.org/T85017 T85017], [https://developer.blender.org/T85071 T85071], [https://developer.blender.org/T85070 T85070], [https://developer.blender.org/T85068 T85068], [https://developer.blender.org/T85077 T85077], [https://developer.blender.org/T85100 T85100], [https://developer.blender.org/T85101 T85101], [https://developer.blender.org/T85103 T85103], [https://developer.blender.org/T83727 T83727], [https://developer.blender.org/T84482 T84482], [https://developer.blender.org/T84238 T84238], [https://developer.blender.org/T84436 T84436], [https://developer.blender.org/T84439 T84439], [https://developer.blender.org/T84794 T84794], [https://developer.blender.org/T84818 T84818], [https://developer.blender.org/T84841 T84841], [https://developer.blender.org/T84372 T84372], [https://developer.blender.org/T85143 T85143], [https://developer.blender.org/T84916 T84916], [https://developer.blender.org/T84918 T84918], [https://developer.blender.org/T83683 T83683], [https://developer.blender.org/T83870 T83870], [https://developer.blender.org/T85111 T85111], [https://developer.blender.org/T85115 T85115],  [https://developer.blender.org/T85128 T85128], [https://developer.blender.org/T85196 T85196], [https://developer.blender.org/T85198 T85198], [https://developer.blender.org/T85197 T85197], [https://developer.blender.org/T85181 T85181], [https://developer.blender.org/T85180 T85180], [https://developer.blender.org/T85164 T85164], [https://developer.blender.org/T85213 T85213], [https://developer.blender.org/T85205 T85205 ]

Needs Developer to Reproduce (2):
* [https://developer.blender.org/T82552 T82552], [https://developer.blender.org/T85148 T85148]

Needs Information from User (32):
* [https://developer.blender.org/T85039 T85039], [https://developer.blender.org/T85040 T85040], [https://developer.blender.org/T84983 T84983], [https://developer.blender.org/T85031 T85031], [https://developer.blender.org/T76145 T76145], [https://developer.blender.org/T85072 T85072], [https://developer.blender.org/T75655 T75655], [https://developer.blender.org/T78142 T78142], [https://developer.blender.org/T85073 T85073], [https://developer.blender.org/T85074 T85074], [https://developer.blender.org/T85013 T85013], [https://developer.blender.org/T84844 T84844], [https://developer.blender.org/T85098 T85098], [https://developer.blender.org/T85095 T85095], [https://developer.blender.org/T85102 T85102], [https://developer.blender.org/T85109 T85109], [https://developer.blender.org/T85133 T85133], [https://developer.blender.org/T85134 T85134], [https://developer.blender.org/T85147 T85147], [https://developer.blender.org/T85120 T85120], [https://developer.blender.org/T83183 T83183], [https://developer.blender.org/T85192 T85192], [https://developer.blender.org/T85175 T85175], [https://developer.blender.org/T84844 T84844], [https://developer.blender.org/T85162 T85162], [https://developer.blender.org/T85168 T85168], [https://developer.blender.org/T85216 T85216], [https://developer.blender.org/T85222 T85222], [https://developer.blender.org/T85223 T85223], [https://developer.blender.org/T85226 T85226], [https://developer.blender.org/T85227 T85227], [https://developer.blender.org/T85209 T85209]

Merged Duplicate (24):
* [https://developer.blender.org/T84366 T84366], [https://developer.blender.org/T85043 T85043], [https://developer.blender.org/T85030 T85030], [https://developer.blender.org/T85022 T85022], [https://developer.blender.org/T74801 T74801], [https://developer.blender.org/T85089 T85089], [https://developer.blender.org/T85108 T85108], [https://developer.blender.org/T85106 T85106], [https://developer.blender.org/T85140 T85140], [https://developer.blender.org/T85138 T85138], [https://developer.blender.org/T85146 T85146], [https://developer.blender.org/T84902 T84902], [https://developer.blender.org/T85186 T85186], [https://developer.blender.org/T85182 T85182], [https://developer.blender.org/T85199 T85199], [https://developer.blender.org/T74770 T74770], [https://developer.blender.org/T85163 T85163], [https://developer.blender.org/T85172 T85172], [https://developer.blender.org/T85215 T85215], [https://developer.blender.org/T84831 T84831], [https://developer.blender.org/T85218 T85218], [https://developer.blender.org/T85206 T85206], [https://developer.blender.org/T85186 T85186], [https://developer.blender.org/T85202 T85202]

Investigated / Tagged / Helped / Other (12):
* [https://developer.blender.org/T85036 T85036], [https://developer.blender.org/T82899 T82899], [https://developer.blender.org/T85052 T85052], [https://developer.blender.org/T84763 T84763], [https://developer.blender.org/T85136 T85136], [https://developer.blender.org/T85131 T85131], [https://developer.blender.org/T85149 T85149], [https://developer.blender.org/T70584 T70584], [https://developer.blender.org/T85174 T85174], [https://developer.blender.org/T85220 T85220], [https://developer.blender.org/T85217 T85217]


=== Patches and Commits ===

Patches (1):
* [https://developer.blender.org/D10258 D10258] - Fix variable scope for ridged multi-fractal noise

Commits (2):
* [https://developer.blender.org/rBc399651637a3a9c8e416b8baffc571fa8cfa571f rBc399651637a3a9c8e416b8baffc571fa8cfa571f] - Fix [https://developer.blender.org/T84708 T84708]: Versioning for Set Alpha node
* [https://developer.blender.org/rB821df20797be514b8aee81b8ae73d3efe486bd29 rB821df20797be514b8aee81b8ae73d3efe486bd29] - Fix [https://developer.blender.org/T84588 T84588]: Set parameter as required for uv_on_emitter

=== On-Boarding of Developers ===
* 

------

[[User:Rjg/Reports/2020|Previous Reports 2020]]