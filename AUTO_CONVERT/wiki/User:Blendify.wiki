=Welcome=

Welcome to the wiki page for Aaron Carlisle.

I currently manage the Documentation project see below for a page describing my responsibilities.

[[User:Blendify/Responsibilities|Documentation Project Administration Responsibilities]]


[https://developer.blender.org/p/Blendify/ Development History]

==Weekly Reports==

* [[User:Blendify/Foundation/2023|2023]]
* [[User:Blendify/Foundation/2022|2022]]
* [[User:Blendify/Foundation/2021|2021]]
* [[User:Blendify/Foundation/2020|2020]]
* [[User:Blendify/Foundation/2019|2019]]