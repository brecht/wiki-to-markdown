== Pull Request workflow ==
This guide explains how to create a pull request (PR) on projects.blender.org. This is the new prefered way of submitting patches.

=== Fork the repository ===
* Go to the repository you want to create a pull request for, https://projects.blender.org/blender/blender for Blender.
* Click on the '''Fork''' button in the upper right.  
[[File:Gitea_PR_CreateFork.jpg|600px]]
* Confirm the fork by clicking on the green '''Fork Repository''' button. (XXX Private / Non Private? Guess private should be avoided here and won't work for PRs later?)
[[File:Gitea_PR_CreateForkConfirmation.jpg|600px]]

=== Checkout & Development ===
* The fork has been creeated now and can be checked out as usual via the address shown. (See [[Tools/Git#Commit_Access|How to setup git and commit access]]) 
[[File:Gitea_PR_ForkCheckout.jpg|600px]]

=== Create a new pull request ===
* Once a change has been commited and pushed to a branch on your fork, a new pull request can be created by switching to the branch and then clicking on the PR button next to it. 
[[File:Gitea_PR_CreatePR.jpg|600px]]
* You can see all the changes that are ahead of master now and can click on '''New Pull Request'''. Enter a title and a description, attach additional files if needed and submit it.


== Alternative text ==

=== One Time Setup ===
This assumes you have the blender/blender repository already checked out on your computer, following the build instructions.

* Go to https://projects.blender.org/blender/blender and click the '''Fork''' button.
* Confirm the fork with the default settings.
* Now you will have to add your personal fork as a remote in your local git repository.  Click '''SSH''' to see the correct URL, and then add it like this:

<source lang="bash">
git remote add me git@projects.blender.org:<USERNAME>/blender.git
</source>

=== Create a Pull Request ===

Now in your git local git repository, make your changes in a branch:
<source lang="bash">
git checkout -b my-feature master
git commit file1 file2
</source>

When you are done, push this branch to your fork:
<source lang="bash">
git push me my-feature
</source>

If all goes well, a message will be printed to the console with a link to create a PR:

<source lang="bash">
TODO: get example of message
</source>

Alternatively, you can go to the page of your fork on projects.blender.org/<USERNAME>/blender. There you can select the branch, and click the icon next to it create a PR from the branch.

=== Update a Pull Request ===

You can update the code by simply pushing the same branch, and the changes will be reflected in the pull request.
<source lang="bash">
git checkout my-feature
git commit --amend file3 file4
git push -f me my-feature
</source>

You can also add additional commits instead of amending an existing one, if changes are more clear when separated into multiple commits. Reviewers are able to squash everything into a single commit if needed.

=== Checkout a Pull Request ===

For a given pull request `#123`, checkout it out into a local branch `their-feature` like this:
<source>
git fetch origin pull/123/head:their-feature
</source>

=== Merge a Pull Request ===

Blender developers with commit access to the main repository can merge pull requests. There are two strategies:
* '''Rebase and fast-forward''': push all the commits in the PR, with commit messages unchanged except `Pull Request #123` being added at the end of the last commit.
* '''Squash''': squash everything into one commit, using the PR title and description as the commit message. You will have the opportunity to edit the title and description in the user interface before the push goes through.