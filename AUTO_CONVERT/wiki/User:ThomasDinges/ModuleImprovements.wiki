= Module Improvements =

Blender modules are present on various channels. Chat, Phabricator, Forum, Wiki... New contributors look for information or want to get in contact with developers. These channels and pages are not consistent though, some information is missing or hard to find. This is a proposal to improve the Module pages. 

== 1) Module landing page (Accepted)==
=== Current status ===
Currently, module information are scattered throughout two places, the wiki and phabricator. 
* The wiki [[Modules]]
* And the landing pages on phabricator, for example https://developer.blender.org/tag/sculpt_paint_texture/

=== Proposal ===
In order to avoid duplication and to have one single place with all relevant information I suggest the following re-structure: 
* The wiki page will be simplified, and will become an overview of the modules with links to the landing pages [[User:ThomasDinges/Notepad|Proposal - Wiki page]]
* All further information will be on the modules landing page then: [https://developer.blender.org/project/manage/139/ Example landing page]

These information include:
* Scope / Information about the module
* The tag to use for reports / patches
* Status of the module (actively developed, in maintenance etc...)
* List of Members / Subprojects
* Contact information (Chat, forum, mailing list, meetings...)
* Links to bug reports, known issues, easy tasks to work on...
* Link to documentation

Modules can add additional information if they like. 

== 2) Chat ==
Apart from the main channel #blender-coders, there are several channels dedicated to the modules. These have inconsistent naming and information in their header though. 

Proposal: 
* Make the channel names consistent. Every module channel shold start with '''#module-'''
* Have basic information in every channel header: 

Link to landing page (minimum requirement) | Additional information can be added here (Meeting Info / Link, Documentation...)

After the rename of the channels, a redirect should be added (empty room with the old name, with info to new) or some fancier redirect. 

== 3) Role of Artist members ==
'''This part needs further work.'''
Every module ideally consists of developers and artists. The role of artist members is very important and should be properly defined and strengthened.

Current responsibilities according to [[Modules/Roles]]
* Make sure the module is well tested for a release
* Help documenting and with release logs
* Feedback on public forums or mailing lists on feature requests

Possible additions:
* Check on the most demanding feature requests (for example from Right Click Select) and work them into a roadmap, in consultation with developers. 
* There are a lot of opinions and feedback online and it is time consuming for developers to read through. The artist members can act as kind of gatekeepers (in an including way) and bring up certain opinions to developers if many people share an idea or concern. Basically channel important feedback.