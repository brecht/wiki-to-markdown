== Week 1: September 17 - 21 ==

* New "Load Image as Empty" operator ([https://developer.blender.org/D3708 D3708])
* Rewrote the Export UV Layout addon ([https://developer.blender.org/D3715 D3715])
* Removed wrapper code for modifiers that was used to get rid of DerivedMesh ([https://developer.blender.org/rB3614d9d2a218bb5c739832484b98989cdee3b3d3 3614d9d] [https://developer.blender.org/rBb5dbe43d3ebfdc238d56bbb71ec17735cebdc951 b5dbe43])
* Fixed part of [https://developer.blender.org/T56865 T56865]

== Week 2: September 24 - 28 ==

* Refactor: Copy/Paste values from/to UI + ability to copy from disabled fields ([https://developer.blender.org/rBefc4862ee69ca9565c1176c59628e023cdea17d0 efc4862])
* Units: new "Fixed Units" option ([https://developer.blender.org/D3740 D3740])
* Fix T56912: bpy.data.masks.new() crashed ([https://developer.blender.org/rB554b26cf88de613ac94121c652cfff45a8f03c88 554b26c])
* Bsurfaces: Cleanup some statements that make no sense ([https://developer.blender.org/rBAbbf44f7484487f3a2913f53715062fff05ac449d bbf44f7])
* Port "STL format" addon to Blender 2.8 ([https://developer.blender.org/rBA08f39b75b9bf459d7b2cd2a06a258abdcebba840 08f39b7])
* Weight Paint: Multiply overlay on the mesh ([https://developer.blender.org/rB19f46c6ac071e32482969319f43d148ada97a63f 19f46c6], [https://developer.blender.org/D3733 D3733])
* Port "Demo Mode" addon to Blender 2.8 ([https://developer.blender.org/rBAb192f64fc3d8a617d4ea604019f8184e7aba3625 b192f64])
* Minor Fix: insert missing break statement ([https://developer.blender.org/rB920173072ea2791bf16b801bc5a9849ae6d8a4af 9201730])

== Week 3: October 1 - 5 ==
* Python GPU API: ''GPUVertFormat.from_shader(...)'' ([https://developer.blender.org/D3772 D3772]); ''vbo.fill_attribute(...)'' and ''GPUVertBuf(...)'' ([https://developer.blender.org/rB564d37c4b67af534b6c12d2bebbd7883c3d3817c 564d37c])
* Vertex Paint: multiply overlay ([https://developer.blender.org/rB7c443ded1e2b5b0b4cec940871cee9602004f17c 7c443de])
* Texture Paint: multiply overlay (not complete) ([https://developer.blender.org/D3770 D3770])
* Gizmo: Fix artifacts when having large angles ([https://developer.blender.org/rB4933dd716c10e899257ea025e9dadf4967e68671 4933dd7])
* Unit System: Implement fixed units ([https://developer.blender.org/rB2d21eb79ad48485bc7b3385d6df5c2c25fd88ee0 2d21eb7] [https://developer.blender.org/rB43885491241cf44fdab1dee8ebb24c1df17d5e47 4388549])
* Addons: port BVH Format addon to Blender 2.8 ([https://developer.blender.org/rBA7bc4655e7b000bfe49336257edcb3a934d01650a 7bc4655])
* Addons: port Export Camera Animation addon to Blender 2.8 ([https://developer.blender.org/rBA043b5bc972d9faadd3027b2c88656260860bc119 043b5bc])
* Addons: port PLY Format addon to Blender 2.8 ([https://developer.blender.org/rBA3659c4da92d039c4045ac0af3fb2dfa9bca8b32e 3659c4d])
* Addons: port MDD Format addon to Blender 2.8 ([https://developer.blender.org/rBA608cf349ae10c42228edf2e12fdc5c0868e80f8f 608cf34])

== Week 4: October 8 - 12 ==
* Python GPU API: GPUBatch for GPUShader ([https://developer.blender.org/D3779 D3779])
* Python GPU API: ''GPUShader.format_calc()'' ([https://developer.blender.org/rB9560fe60e42d4fe098a24c17f31b6313eaae3364 9560fe6])
* Addons: new 'Scatter Objects' addon ([https://developer.blender.org/D3787 D3787])
* Image Empties: Improve usability ([https://developer.blender.org/rB85944a2d7e73d1ed070b19e50bc6927b47070091 85944a2])
* Small changes to the [https://gitlab.com/blender-institute/BlenRig BlenRig] addon

== Week 5: October 15 - 19 ==
* Outliner: Open empty collection when something is dragged into it ([https://developer.blender.org/rBc17f2c2eb61f387c278a8dbc8ec559fc1d4bcad6 c17f2c2])
* Outliner: Separate Moving and Parenting operations ([https://developer.blender.org/D3812 D3812])
* Outliner: Only drag element under mouse if it was not selected before ([https://developer.blender.org/rB3c30d3bcbd93da4e4cded76c3fc3a30152a43cb4 3c30d3b])
* Outliner: Drop into master collection when below everything else ([https://developer.blender.org/rBce0a468408cd7e639785b77363f090f91af6f2f6 ce0a468])
* Cleanup: Started to remove some #if 0 blocks ([https://developer.blender.org/rB41216d5ad4c722e2ad9f15c968af454fc7566d5e 41216d5], [https://developer.blender.org/rB21744217cea9bb8f767f47cb6c41446563e645ac 2174421])
* Tools: Implemented the a Curve Tilt tool to learn and test the new API ([https://developer.blender.org/D3804 D3804], [https://developer.blender.org/T57224 T57224])
* Dropping Files: Fix image dropping as empty or camera background ([https://developer.blender.org/rB09cd651bb7e59044cbcd0664e8b1064cb37734ed 09cd651])
* Dropping Files: Support Open/Link/Append when dropping .blend file ([https://developer.blender.org/D3801 D3801])
* Event System: Fix - Don't invoke operator that should only be executed ([https://developer.blender.org/rBeba1b0487c8e24c13b1b8c134710a6c65875100c eba1b04])
* Addons: removed Custom Normals Tools addon ([https://developer.blender.org/rBA295cdaa5c7ddb8e99994f2ae9d3de732fcb01014 295cdaa])
* Addons: Port "Math Vis (Console)" addon to Blender 2.8 ([https://developer.blender.org/D3795 D3795])
* Texture Paint: Don't create material if operation is cancelled ([https://developer.blender.org/rB2875fb9e7bbc183ae2c40f9e0c8961ace12e767e 2875fb9])
* Texture Paint: Always show "No Textures" when there are no textures ([https://developer.blender.org/rBd1145306ec606d8b271eabcb359aaa821c37e688 d114530])
* External: Start porting Animation Nodes to Blender 2.8 ([https://twitter.com/JacquesLucke/status/1052613822354268162 Twitter])

== Week 6: October 22 - 26 ==
* Fix: Buffer overflow when creating gpu.types.GPUIndexBuf ([https://developer.blender.org/rB341306995be6839fe091d23b511d89ad8deb616a 3413069])
* Fix: Missing sequencer update ([https://developer.blender.org/rB767b49e49249cbc6679f79e1241d7a55749ab64c 767b49e])
* GPUShader: shader.uniform_float parameters ([https://developer.blender.org/rBfc3d771801ab3a864ae73cf881037063abcf89ce fc3d771])
* Drag & Drop: Support Open/Link/Append when dropping .blend file ([https://developer.blender.org/rB650cdc6b2d91b12a80d7c8e680224c4713cfeb5e 650cdc6])
* SVG Format Addon: minor Blender 2.8 fixes ([https://developer.blender.org/rBAdc212f77d1c82bacbe941a0d84fcfdead9ace17a dc212f7])
* Python API Docs Fixes: [https://developer.blender.org/rB59054d5eba487a0c79661e7b8f774edc75e582e0 59054d5], [https://developer.blender.org/rBc49142dafef614102ae6b1e01ffb9111ddde03d2 c49142d]
* Outliner: drag&drop - separate moving from parenting ([https://developer.blender.org/rB0f54c3a9b75be8f8db9022fb0aeb0f8d0d4f0299 0f54c3a])
* Release Notes: wrote some examples for the new gpu api ([https://wiki.blender.org/wiki/Reference/Release_Notes/2.80/Python_API/Draw_API#Examples Link])
* Blender Conference

== Week 7: October 29 - 2 ==
* Addons: Started to port "3D Viewport Pie Menus" addon ([https://developer.blender.org/D3883 D3883])
* Python API: Proposed using namedtuples as return values in some cases ([https://developer.blender.org/D3877 D3877])
* Addons: Port "IvyGen" addon to Blender 2.8 ([https://developer.blender.org/rBA8406aafbf27732e87fbc5588012c99e301fe16a8 8406aaf])
* Image Empties: More visibility settings ([https://developer.blender.org/rBa3802f66e22e57115f48545a39bf41959eb16fad a3802f6])
* Addons: Committed "Scatter Objects" addon ([https://developer.blender.org/rBA54d50aec6f135236e6a5346b61be56e3f550da55 54d50ae])
* Addons: Removed "Object Grease Scatter" addon ([https://developer.blender.org/rBAb8a0f8c8fe65337a718e215db9ebd53c39a7a173 b8a0f8c])
* Addons: Committed port for "Math Vis (Console)" addon ([https://developer.blender.org/rBAf80599f36929782af1289fdf0e88f339c8a546ab f80599f])
* Fix: Pose Breakdown operator ([https://developer.blender.org/rB1c326e50798d73f38ad797e2d6b8468cd3c19cee 1c326e5])
* Drag&Drop: Started working on a completely new drag and drop implementation to make it less hacky...

== Week 8: November 5 - 9 ==
* GPU Docs: ([https://developer.blender.org/rBa4bfccc439792be10df92541595f8f3ef0ca7fe7 a4bfccc]) 
* API Docs: Add warnings that some functions are slower than one might expect ([https://developer.blender.org/rB7c227e4740457dc02afdff733e13c97d983f3aae 7c227e4])
* GPU: Free GPUOffScreens owned by Python when the corresponding context is discarded ([https://developer.blender.org/D3919 D3919])
* Image Empties: Operational Border Drawing ([https://developer.blender.org/D3917 D3917])
* GPU Docs: Show gpu_extras module in docs ([https://developer.blender.org/rB9a38526be04bddd99565172165a2a49b50019e54 9a38526])
* GPU: Frame Buffer stack ([https://developer.blender.org/rB50b43ff6d49cb101884e26f2c04aa9c19512b6cc 50b43ff])
* Addons: View 3D Pie Menus ([https://developer.blender.org/rBAbaaf58872bf83049637f2e4da9c444265846f2af baaf588] [https://developer.blender.org/rBA30e26a00827fc67fab468591f4f2fb3c47615ff1 30e26a0])
* GPU API: draw_texture_2d function ([https://developer.blender.org/D3901 D3901])
* Addons: Export UV Layout using GPUOffScreen ([https://developer.blender.org/D3897 D3897])
* Addons: Ported and sped up Jiggle Armature addon for potential use in Spring ([https://github.com/JacquesLucke/JiggleArmature Github])
* Addons: Removed a lot of stuff from BlenRig, request from Animators ([https://gitlab.com/blender-institute/BlenRig Gitlab])

== Week 9: November 12-16 ==
* GPU Docs: Fixing, Cleaning, Extending ([https://developer.blender.org/rB4fbde56f5167d91e60f489403bc8f725d397c8da 4fbde56],  [https://developer.blender.org/rB4d04235d5f5ef71c30fcc706ebe99caf5c52db87 4d04235], [https://developer.blender.org/rB78207eac9b9b576ae66731ea257103f84b0ea41b 78207ea], [https://developer.blender.org/rB46ac317a292d94f73e1583929423d8a3c5f14530 46ac317], [https://developer.blender.org/rB9caa587519b40e3fb2479d16328f91042d9eaeee 9caa587],
[https://developer.blender.org/rB3aa30406dd0f862f8360741ff77118505aa93302 3aa3040],
[https://developer.blender.org/rB77238819f46fd53aa4e34636a09c2658abd9ea4f 7723881],
[https://developer.blender.org/rBdc6ba4f2ec1f66e4c182ef7e651da6badf6497aa dc6ba4f], [https://developer.blender.org/rBc8975b0fc71c0e02f7bb7bc4680077b5745a6931 c8975b0], [https://developer.blender.org/rB444f1fd4237d68151cb0cb844ad25276102291bd 444f1fd], [https://developer.blender.org/rB3f478f4260ef0ea2db27e5008c193dfec3288749 3f478f4], [https://developer.blender.org/rB6ae84ddc6e7c40d89896c3c5a4f7c67dceb586d5 6ae84dd], [https://developer.blender.org/rBbb39e33d2537befd54d8ddda54669749855ba7a3 bb39e33], [https://developer.blender.org/rB15a5cc6ca0b93a923e1bfd25850d3d645aaa43b5 15a5cc6], [https://developer.blender.org/rB492dbae4d1a05785741b5370fab618b38b8fdfa0 492dbae])
* Unit System: Use upper case and underscores for unit identifiers in Python ([https://developer.blender.org/rBbdca86395697a0deed05948cda1ca612448e63cb bdca863])
* Addons: Committed UV Layout export using GPUOffScreen ([https://developer.blender.org/rBA4f22bef9eb7e31c50bcfe5c42a80dfd741dc62da 4f22bef])
* Took one day off.

== Week 10: November 19-23 ==
* Python API: bpy_extras.callback_utils ([https://developer.blender.org/D3977 D3977])
* Python API: bpy.app.handlers.depsgraph_update_pre/post ([https://developer.blender.org/rB4b06d0bf51c38c4358c07823358589ca1cdc6f47 4b06d0b])
* Images: "Image" submenu in "Add" menu ([https://developer.blender.org/rB86e0d13218d6e9f918ca857890ee1c0aa681091b 86e0d13])
* Image Empties: Option to not display the backside of image empties ([https://developer.blender.org/rB5f21030a81fa306f807be5bee8bb1df8dd93a98e 5f21030])
* Py API Docs: minor fixes ([https://developer.blender.org/rB422992a135bda9b4da2311a7ebca1f896391c027 422992a], [https://developer.blender.org/rB6fe1b7158bb9b562e2213f9a976b293e3902545d 6fe1b71])
* Developer Tools: Continued working on my Blender extension for Visual Studio Code ([https://github.com/JacquesLucke/blender_vscode Github])

== Week 11: November 26-30 ==
* Python API: Two versions of bpy.app.timers, one of which is now in Blender ([https://developer.blender.org/D3990 D3990], [https://developer.blender.org/D3994 D3994], [https://developer.blender.org/rBc1adf938e6c8ecaec805f4cf95c73480de1bf980 c1adf93])
* Python API Docs: Minor fixes and timer api docs ([https://developer.blender.org/rB884638494d5cc0352195b44c08bad48c2640bbad 8846384], [https://developer.blender.org/rBdcb86689b037e545b08ff35c355db08889b73ef1 dcb8668])
* Bugs: Lots of bug report triaging after the beta release.
* LLVM: Started learning how to use the C and C++ api inside of Blender ([https://gist.github.com/JacquesLucke/1bddc9aa24fe684d1b19d4bf51a5eb47 Test on Github])
* Addons: Wrote an auto_load.py file that can find and register all the classes in an addon. ([https://gist.github.com/JacquesLucke/11fecc6ea86ef36ea72f76ca547e795b Github])
* Developer Tools: Continued working on my VS Code extension. ([https://www.youtube.com/watch?v=Tjl7p6Wx5VM Youtube])

== Week 12: December 3-7 ==
* Speedup: New EdgeHash and EdgeSet implementation ([https://developer.blender.org/D4050 D4050])
* Speedup: New OldNewMap implementation for file loading ([https://developer.blender.org/D4038 D4038])
* Speedup: Optimize DNA_elem_array_size to speedup file loading ([https://developer.blender.org/rB50d26e7c78cd1d226550d94ff761af5128f0ca8e 50d26e7])
* Minor Fixes: [https://developer.blender.org/rBeeabed2132f98131ac896f15e68b1805e0ce69a7 T58771], [https://developer.blender.org/rB828c4d1c54e10824445c3f0978d161424c766965 T58863]
* Bugs: More general bug tracker work.

== Week 13: December 10-14 ==
* Finished new OldNewMap implementation ([https://developer.blender.org/rB33993c056a557d8c51ff9d01ff3666ab81d40c29 33993c0])
* Finished new EdgeHash and EdgeSet implementation ([https://developer.blender.org/rBaa63a87d37d3b190ec8c957c892af9c1be2ea301 aa63a87])
* UI: show frame rate in movie clip editor ([https://developer.blender.org/rB233b7806354f27a52fb1aee672c7c58149d0b4a4 233b780])
* Drag & Drop: proposed new Drag & Drop system ([https://developer.blender.org/D4071 D4071])
* Reviewed: [https://developer.blender.org/D4070 D4070]
* Fixed: [https://developer.blender.org/rBae5f19e32ad334dd4a69af193fc4e0236ac3ca2e T59125], [https://developer.blender.org/rB4e0291f185570b12abe82ce57ff38c4ae27aa2ac T59208]

== Week 14: December 17-21 ==
* Python Docs: added documentation for draw_handler_add/remove methods ([https://developer.blender.org/rB2dee1772e1ee200ab822cd4a7943ab1ea99adb6b 2dee177]) 
* Reviewed: [https://developer.blender.org/D4085 D4085]
* Bug Fixes: [https://developer.blender.org/rBbb583542324ffe5ed6a1d71e1ab341e413736499 T59490], [https://developer.blender.org/rB6dcf788d1222ac6a04b075291a09867810a841e3 T59493], [https://developer.blender.org/rBA91686697aad63a42d479ac62c6a39031c1827870 T59504], [https://developer.blender.org/rB5fa749ace29a60c1b0dd596c42973ac5e5cea28e T59536], [https://developer.blender.org/rBbe98fcc6e27e5e42aaf515d238ba6a1297f751a1 T59273], [https://developer.blender.org/rB42bf7e440cfcc29bdd6cf417d02de54c7f0f7831 T58376], [https://developer.blender.org/rBb0478664597f87c29512b3909f39aa54fd5098c8 T58404], [https://developer.blender.org/rBae028ef206cff475ec314db68d6d111f83b96a5d T57045], [https://developer.blender.org/rBc02f67fa8ae56c8b9e5015b0228de9844a015b71 T57777], [https://developer.blender.org/rB63ed86b06f2eb9b1b9b6ab5f9472365c2b42bbf8 T59674], [https://developer.blender.org/rB8095e660c0d8694022c6f76cab26c953a7fb527b T59672]
* General bug tracker work.