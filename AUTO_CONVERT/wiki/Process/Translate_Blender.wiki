=Blender UI Translation Guide=

Here is a brief explanation of how translation works and how to update translation files.

{|class="note"
|-
|<div class="note_title">'''Note'''</div>
<div class="note_content">All this makes references to the Blender UI Translations [https://translate.blender.org/projects/blender-ui/ website] and [https://projects.blender.org/blender/blender-ui-translations git repository].<br/>
''Before being able to do anything, translators must register on [https://projects.blender.org the development website] and request to join the translation team (by opening an issue for this, or asking on [https://blender.chat/channel/translations the UI translations chat])!''
Also please subscribe to the [https://devtalk.blender.org/tag/ui-translation translation tag] on devtalk.blender.org, so that you can receive announcements about the translations.</div>
|}


==Translating==
Most translation efforts happen on the [https://translate.blender.org/projects/blender-ui/ Blender translate] website, currently an instance of Weblate.

Anonymous or basic accounts can already suggest edits to existing translations. These will need to be reviewed and accepted by the translation team before they get published.

Once a user has an account on the website and has requested to be added as translator, they can directly edit the translations online. Contributions from a same translator will be packed into a single commit typically, after a few hours.

Translating teams are also encouraged to make use of the Glossary feature of Weblate, it can help a lot with keeping consistency in terminology across a whole translation.

===Issues With the Original Message===
Sometimes you may see some text in the UI, but no matching message exists in the translation files. This probably means that the developers forgot to make them translatable.

Another issue is about contexts. The translations use contexts, to disambiguate between different meanings of a single word. By default no context is used, so sometimes it’s impossible to translate one message without breaking another.

And sometimes, a message is just wrong, has a typo, or is not very clear, even in English.

If any such issue arises, please notify developers in [https://projects.blender.org/blender/blender/issues/43295 this task].

===About Translating "Blender" ===
Some usages of the program name "Blender" are marked as translatable, like for window titles. In almost all cases, you should not translate the string "Blender". And '''never''' completely replace it.

Translating the string should only be ''considered'' if the Latin text of "Blender" cannot be used as a noun by your target audience. For an example of this, consider that most English users could not use "トヨタ" as a noun, but instead use "Toyota". So if your language characters are ideographic, the phonemes in your language do not support the sounds in "Blender" well or make it rude or weird, or otherwise render "Blender" as a noun unusable on its own, consider '''appending''' to the string.  For example if "Blender" would not be pronounceable, consider translating the string so that it includes both "Blender " '''and''' a short transliteration of the name sounds in your script. If your script is ideographic, you could similarly consider appending translation characters after "Blender ". Ideographic translations of Blender would indicate what it is and what it does, and would never have anything to do with the kitchen appliance of the same name.

An example of this can be seen on the signs for McDonald's restaurants in China. These generally show "McDonald's 麦当劳", a transliteration of the English name as 'Maidanglao', though literally meaning 'wheat serves labor.' Although they have recently rebranded to 'Jingongmen' (金拱门), which translates to 'Golden Arches.'

The same idea applies to other products inside of Blender which are considered as brands and must not be translated. These include:
* Cycles
* EEVEE (must be written uppercase)
* Grease Pencil
* Freestyle
* Line Art

===Advanced Operations===
Translators can also download the PO file of their language to work offline (see below), and upload it again later. Be aware though that this may create conflicts, in which case part of the offline work may be rejected by Weblate on upload.

{|class="note"
|-
|<div class="note_title">'''Patience Required'''</div>
<div class="note_content">There is a known issue with the current tool behind the web interface, which will make heavy processing like upload and integration of a PO file take several minutes, with the web page staying in refresh mode for the whole time. If it takes more than ten minutes, it will even apparently fail with a server timeout error message.

There is usually no actual problem though, so no need to re-try uploading the PO file then, refreshing the page after a few minutes should be enough to see the contribution in the web interface.</div>
|}


{|class="note"
|-
|<div class="note_title">'''Tools'''</div>
<div class="note_content">Gettext tools (`msgfmt` and `msgmerge`) are very helpful for most of these operations! If you are working with RTL languages (Arabic, Persian, Hebrew…), you’ll also need the C library FriBidi for your platform.</div>
|}


====How it Works====
PO translation files can be downloaded and uploaded from the `Files` menu of a language page in the translation project.

The `blender-ui-ui-<language>.po` file follows the `gettext` fairly simple format:

 #: One or more lines of comments stating origins of the message.
 #, fuzzy [optional] see below
 # One or more lines of free, general comments
 msgid "some message id"
 msgstr "translation for this message"

In case `msgid` or `msgstr` are too long, they will be split on several lines, like this:

 msgid ""
 "A very very very very very very very very very very very very very very very"
 " very very very very dummy long message id!"

All this means that when the string `"some message id"` is used as operator name, tool-tip, menu and so on, it will be displayed on the screen as `"translation for this message"`.

====Edit Translations - Translators====
Any string with an empty `msgstr` needs to be translated.

If the `msgid` is marked as `fuzzy`, i.e.

 #, fuzzy
 msgid "some message id"
 msgstr "translation for this message"

...it means that the translation used to exist for this message, but the message was modified, so the translation also has to be updated. When the translation has been updated, the line with <code>#, fuzzy</code> should be removed.

{|class="note"
|-
|<div class="note_title">'''Note'''</div>
<div class="note_content">Using a dedicated editor for po files is recommended, like e.g. Poedit.</div>
|}


====Edit Translations from Blender - Translators====
{|class="note"
|-
|<div class="note_title">'''Currently Unavailable'''</div>
<div class="note_content">The move to git repository and weblate web interface requires some updates to the add-on before this feature can be used again.</div>
|}


====Add New Languages - Translators====
Adding a language has to be done by an admin/developer of the project. The best way to request for it is to open an issue on the tracker, typically while also asking to become a translator (see above at the start of this page).

====Merge Translations - translators====
When a PO file has been worked on offline for some times, and there are conflicts with the current status of translations on the website, the translator can re-download an up-to-date translation file, and try to merge their current work in it.

Using `msgcat` gettext tool is the best solution currently (<code>msgcat -o output_file.po input_file1.po input_file2.po …</code>, see its help for more info).

====Test Translations - Translators====
To test offline translations, translators can run `msgfmt` on their edited PO file:

 $ msgfmt --statistics blender-ui-ui-<langauge>.po -o blender.mo

Then copy the produced `blender.mo` binary file under your <code>.blender/locale/<language>/LC_MESSAGES/</code> directory.

{|class="note"
|-
|<div class="note_title">'''Note'''</div>
<div class="note_content">Some PO editors (like poEdit) generate by default a .mo file when saving. In this case, just copy this file into the relevant directory as stated above, and rename it to <code>blender.mo</code>!</div>
|}


====Languages Needing PO RTL Processing====
This concerns at least, currently, Arabic, Persian and Hebrew.

The process is now standardized! Please follow these rules:
* The translated strings in the PO file or on the web interface '''must be the raw ones''' (i.e. non-processed).
* The language must be specified as RTL by adding its uid (ISO code) to the <code>bl_i18n_utils</code> module settings' <code>IMPORT_LANGUAGE_RTL</code> set. This is a dev task, please ask e.g. to an i18n maintainer to do so, if needed.

This way, i18n addon's tools will do the conversion automatically (using underlying fribidi library) when needed.

==Administrating==

===Manage UI Translations Add-on===
Most of the maintenance work of translating is achieved through that official add-on (found under the <span class="literal">System</span> category). Though it is not mandatory for translation-only tasks, it may be useful, and it is also needed for script authors that want to translate their own add-ons (please note that official add-ons are already handled by the main translation project).

The various features of this add-on will be described in relevant sections below.

{|
 |[[File:Blender-Dev-Doc-I18nAddon-Preferences.png|thumb|400px|I18n add-on preferences.]]
 |[[File:Blender-Dev-Doc-I18nAddon-I18nUpdateTranslationPanel.png|thumb|250px|<span class="literal">I18n Update Translation</span> panel.]]
 |}
These are advanced and/or reference-affecting operations, translators cannot perform them. This also assumes that the admin has valid blender sources, blender build (should be as much similar to official ones as possible), and bf-translations repository, and that the i18n add-on is enabled and its settings are valid.

===Adding a New Language===
First of all, edit the <code>scripts/modules/bl_i18n_utils/settings.py</code> file, <code>LANGUAGES</code> tuple, to add this new language. Then rebuild/reinstall Blender, the new language should appear in the menus now. Use the add-on to update with the new language selected, to generate a new void PO file for it.

===Updating all PO's on the reference Repository===
Ensure that the weblate instance is '''fully synced''' with the git repository, and '''lock it'''.

In the <span class="literal">I18n Update Translation</span> panel (bottom of <span class="literal">Render</span> buttons), select all languages, and hit <span class="literal">Update Branches</span> (keep <span class="literal">Skip POT</span> deselected!).

Note you can restrict the update to a list of languages, simply deselecting unwanted ones.

You should also typically run <span class="literal">Clean up Branches</span> to remove existing invalid entries, commented ones, etc.

Once all (necessary) language updates have been committed to the git repository, re-sync weblate, and unlock it.

===Updating Blender Translations===
The translations in the git repository and the web interface are not directly used by Blender builds. They need to me imported into the source code of Blender first.

In the <span class="literal">I18n Update Translation</span> panel (bottom of <span class="literal">Render</span> buttons), select all languages, and hit <span class="literal">Update Trunk</span>.

This will:
* Check all PO's, and process the valid ones (and translated above 0% by default, you can specify another threshold within <code>bl_i18n_utils</code>' settings).
* Clean up the PO's (i.e. remove all commented messages in them), and run some consistency checks.
* Copy and strip to their bare minimal all PO's into the relevant Blender git repository directory.

The actual commit and upstream push in the Blender source repository has to be done manually afterwards.

==Translating Non-Official Add-ons==
Translation of these add-ons is primarily handled through a Python `dict` named <code>translations_dict</code>, a mapping of mappings, with language uids (ISO codes) as higher-level keys, and messages → translations mappings as values. This dict must be registered by a call to <code>bpy.app.translations.register()</code> and unregistered by a call to <code>bpy.app.translations.unregister()</code>, exactly as the add-on itself.

Typically, this dict is generated from a tuple named <code>translations_tuple</code>, which is a tuple of tuples <code>((msgctxt, msgid), (sources, gen_comments), (lang, translation, (is_fuzzy, comments)), ...)</code>.

Add-ons authors are not expected to write and maintain such code, keep track of changes in the UI messages, etc. So the i18n add-on is here again to help with these tasks.

In the <span class="literal">I18n Update Translation</span> panel (bottom of <span class="literal">Render</span> buttons), the language(s) to be worked on must be selected. There are several operations listed under <span class="literal">Addons:</span> – they all will always ask to select the affected add-on through a search box pop-up:
;<span class="literal">Refresh I18n Data…</span>
:Extract the UI messages specific to the selected add-on, and update (or create) relevant py code (the <code>translations_tuple</code> tuple).
;<span class="literal">Export PO…</span>
:Export current py code into PO files for the selected languages (and optionally generate a POT file too).
;<span class="literal">Import PO…</span>
:Import all PO's under given directory, and update the selected add-on's py code accordingly.

{|class="note"
|-
|<div class="note_title">'''Important'''</div>
<div class="note_content">
*If a message is already present in main Blender translation, it won’t be added to the add-on’s ones, the user will have to use a custom context if a custom translation is absolutely needed.
*The tools use some “markup” to locate the generated code, so the add-on author may move that block of code anywhere they like.</div>
|}


See the <code>render_copy_settings</code> add-on as an example.

==Tips==
===Quickly toggle the UI language===
It’s possible to create a keyboard shortcut to toggle the UI between English and your language, in order to check that the translation matches the original text, or to reload it by toggling twice.
* Go to the <span class="literal">User Preferences</span>&nbsp;» <span class="literal">Keymap</span>&nbsp;» <span class="literal">Window</span>&nbsp;» <span class="literal">Add New</span>.
* Unfold the new keymap by clicking the arrow to the left, and replace `none` by `wm.context_toggle_enum`.
* In the `Context Attributes` field, enter `preferences.view.language`.
* In the `Value` fields, enter `DEFAULT` and the desired language code (for instance, `pt_BR`; you can get the code in the `Translation` preferences by hovering the `Language` field).
* Assign the shortcut to a free combination, for instance <span class="hotkeybg"><span class="hotkey">Ctrl</span><span class="hotkey">Alt</span><span class="hotkey">⇧ Shift</span><span class="hotkey">L</span></span>.

==See Also==
*The [[Source/Interface/Internationalization|developer internationalization page]].

===Language teams===
*[[/French Team|French team]]

===Local blender.chat channels===
Language teams can use their own channel on blender.chat to collaborate on the translation. If yours doesn’t exist yet, feel free to create it and announce it on [https://blender.chat/channel/translations the translations channel] for others to see.
* [https://blender.chat/channel/translations-nl Dutch]
* [https://blender.chat/channel/translations-de German]
* [https://blender.chat/channel/translations-it Italian]
* [https://blender.chat/channel/translations-es Spanish]