# Mentorship

Right now every new hire comes with a high cost in patch review. That
strangles even further our "bottleneckers" - developers sole responsible
for overseeing many areas in Blender.

The mentorship program tries to reduce our overhead while building up
seniority across the team - this is a win-win in the short and long
term. We are pairing developers who fully understand and maintain
certain parts of Blender, with new-comers of that area. The *mentor*
will help finding random things that are a good learning experience and
need doing.

Example of tasks can be:

  - Long stand todos
  - Technical debts
  - Low hanging fruits
  - Documentation

All in all these are developing opportunities that also helping to move
our existing agenda further.
