# Development Management Tasks

The following represents the tasks related to the development
coordinator as well as chief architect roles. Based on our current
capabilities we split them in three groups:

  - Feature / code / design review.
  - Release, bug tracker, communication.
  - Documentation, forum, new contributors/onboarding, infrastructure

## Review

  - Assign appropriate reviewers to patches
  - Make sure patches are not ignored too long
  - Review contributions on a high level
  - Help code reviews stuck on design or decisions get unstuck
  - Read and review design docs
  - Come up with alternative designs when needed

## Release Management

  - Define and communicate roadmaps
  - Keep track of all tasks that need to be done for release
  - Reorganize and close tasks on projects.blender.org to reflect
    current status
  - Assign release tasks to developers
  - Talk to developers to ensure they are working on the release targets
  - Make decisions on what is safe to add in the current release
  - Make sure manual is in a ready state

## Development Planning

  - Help decide which major features to focus on
  - Find developers to work on important features that are not assigned
    to anyone
  - Help define vision for how to do these new features, work through
    design with developers
  - Ensure module owners are organizing and communicating development of
    their module well and help as needed

## Follow Development

  - Keep eye on commits for any problematic code changes
  - Keep eye on usability or design issues introduced by commits
  - Follow up with developers and designers to ensure issues get fixed
  - Ensure daily builds stay stable, and that major breakage is fixed
    immediately
  - Ensure tests keep passing, poke developers when they break
  - Understand what developers are spending time on

## Bug Tracker

  - Follow bug tracker reports on a high level
  - Set priorities to low / medium / high so developers focus on
    important things
  - Help bug triagers to resolve tricky reports and define policy for
    how to handle reports
  - Ensure bug tracker remains friendly on-topic place, deal with rude
    users or developers
  - Help bug fixes get unstuck when needed (Brecht)

## Communication

  - Write meeting notes, ensure all developers have weekly reports
  - Write weekly overview of new features and changes
  - Write blog posts on code.blender.org
  - Coordinate with web team
  - Organize Blender conference and other presentations

## Team

  - Organize and lead weekly meeting on blender.chat
  - Set up meetings between developers if issues need to be worked out
  - Talk to developers to see if things are going ok, what to improve
  - Detect when developers are stuck too long on things, and find
    solutions
  - Onboarding of new developers and other employees
  - Make sure developers have software and hardware they need
  - Keep Ton in the loop on important decisions and things happening in
    development

## Contributors

  - Answer new developer questions on
    [devtalk](https://devtalk.blender.org)
  - Answer new developer questions on chat
  - Set up commit rights for developers and explain rules
  - Keep close eye on new committers to ensure they are following
    guidelines
  - Guide and encourage contributors to work on topics that fit
    Blender’s goals

## Forums

  - Answer users questions or get the right person to answer them
  - Moderate forums to stay on topic, handle flags
  - Warn or ban users when they show bad behavior
  - Pass on information to relevant developers when needed

## Docs

  - Ensure release notes are complete, poke devs who don't update
  - Define overall structure of release notes, ensure consistency and
    fill in blanks
  - Keep eye on Blender manual changes to ensure correctness and
    completion
  - Help manual writers understand Blender features and release process
  - Keep developer wiki documentation up to date

## Development Infrastructure

  - When any developer website or infrastructure goes down, organize it
    to get fixed
  - Keep track which infrastructure is working well, and where problems
    are
  - Lead any development infrastructure changes
  - Make decision with sysadmin on infrastructure setup and policies
  - Create wiki, blog and other accounts for developers
  - Organize better continuous integration and testing infrastructure
  - Organize improvements to projects.blender.org
