# Hair Nodes

*The hair design was re-visited in February 2021 to check if it is still
aligned with the [geometry-nodes
design](../../../../Source/Nodes/Modifier_Nodes.md). The design is still
compatible.*

This adds a new object type (hair) that can receive its own modifiers
and be groomed. It also adds a new geometry data-set that can be
combined with the regular mesh and point clouds in the same nodetree.

## Scalp

The hair object uses a reference surface (commonly known as scalp) for
interpolation and grooming. The relation between the hair and the scalp
is similar to the armature and the mesh object. The hair root is mapped
in relation to the scalp if the scalp is available.

![../../../../images/EverythingNodes-Hair-Scalp.png](../../../../images/EverythingNodes-Hair-Scalp.png
"../../../../images/EverythingNodes-Hair-Scalp.png")

## Pipelines

There are different pipelines to work with hair. From fully procedural
to a mixed approach combining initial distribution:

  - *100% procedural*
  - *Mixed and destructive*
  - *Imported*
  - *Non-destructive grooming*

### 100% procedural

Generate hair and deform it 100% in the nodes.

![../../../../images/EverythingNodes-Hair-Pipeline-Procedural.png](../../../../images/EverythingNodes-Hair-Pipeline-Procedural.png
"../../../../images/EverythingNodes-Hair-Pipeline-Procedural.png")

  - What happens when you join different hair objects with different
    scalps.

### Mixed and destructive

*Start procedural + apply + grooming + nodes.*

For this the user starts with the "Hair Object Type" so the initial
procedural nodes can be applied.

### Imported

  - It can still use a scalp for reference.
  - The scalp is useful for grooming and transforms.
  - The imported hair can still be groomed and have nodes.

### Non-destructive grooming

*Start procedural + \[ bake / freeze \] + grooming + nodes*' When
unfreezing the grooming is lost.

If the user wants to tweak the distribution of hair after started to
grooming they will loose all the grooming.

<table>
<tbody>
<tr class="odd">
<td><div class="note_title">
<p><strong>Mesh Editing</strong></p>
</div>
<div class="note_content">
<p>This is no different than mesh editing. And it will become more evident once procedural modeling is better supported. That means the same solution should serve both systems.</p>
</div></td>
</tr>
</tbody>
</table>
