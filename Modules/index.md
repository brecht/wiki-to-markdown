# Modules

<div class="bd-lead">

Blender contributors are organized into Modules. New contributors to the
Blender project can find the right point of contact for design or code
review here.

</div>

Modules consist of owners and members. Read more about their
[roles](Roles.md).

A module always maps to some components inside of Blender as the
software. The Blender project as a whole also includes a number of
[other free and open source
projects](https://developer.blender.org/diffusion/), such as websites,
pipeline tools, documentation, infrastructure, ... These are organized
differently.

## Blender

**Blender Foundation Chairman**: [Ton
Roosendaal](https://projects.blender.org/Ton)  
**Product Management**: [Ton
Roosendaal](https://projects.blender.org/Ton) (*stand-in*)  
**Development Coordinators**: [Dalai Felinto](../User/Dfelinto/index.md),
[Thomas Dinges](../User/ThomasDinges/index.md)  
**Admins**: [Bastien Montagne](../User/Mont29/index.md), [Brecht Van
Lommel](https://projects.blender.org/brecht), [Campbell
Barton](https://projects.blender.org/ideasman42), [Dalai
Felinto](../User/Dfelinto/index.md), [Sergey
Sharybin](https://projects.blender.org/Sergey)

## [Module List](https://projects.blender.org/blender/blender/wiki/Home)

Information about the modules, workboards, meetings, developers involved
can be found on [projects.blender.org](https://projects.blender.org).
