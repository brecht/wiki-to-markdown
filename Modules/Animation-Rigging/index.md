# Animation & Rigging

Notes and documentation about the Animation & Rigging module.

**Subpages:**

  - [Bigger
    Projects](Bigger_Projects.md):
    broad ideas for the future.
  - [Code Documentation](../../Source/Animation/index.md)
  - [Code Review](Code_Review.md)
  - [Weak Areas](Weak_Areas.md): areas
    of the Animation & Rigging system that could use improvement.
  - [Might Revisit](Modules/Animation-Rigging/Might_Revisit):
    design limitations that we have no intention of lifting at the
    moment, but that could be revisited in the future.
  - [Modules/Animation-Rigging/Character Animation Workshop
    2022](Character_Animation_Workshop_2022.md)
