# User Interface Module

See the [**Module Landing
Page**](https://developer.blender.org/project/view/12/) for general
information.

  - [Getting Involved as a
    Designer](Get_Involved_As_Designer.md)

## Release Notes

  - [Blender 2.93 LTS - May 26,
    2021](../../Release_Notes/2.93/User_Interface.md)
  - [Blender 2.92 - February 24,
    2021](../../Release_Notes/2.92/User_Interface.md)
  - [Blender 2.91 - November 25,
    2020](../../Release_Notes/2.91/User_Interface.md)
  - [Blender 2.90 - August 31,
    2020](../../Release_Notes/2.90/User_Interface.md)
  - [Blender 2.83 LTS - June 3,
    2020](../../Release_Notes/2.83/User_Interface.md)
  - [Blender 2.82 - February 14,
    2020](../../Release_Notes/2.82/UI.md)
  - [Blender 2.81a - November 21,
    2019](../../Release_Notes/2.81/UI.md)
  - [Blender 2.80 - July 30,
    2019](../../Release_Notes/2.80/UI.md)
  - [Blender 2.79b - September 11,
    2017](../../Release_Notes/2.79/UI.md)
