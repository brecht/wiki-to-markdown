# UI Workboard Notes

<table>
<tbody>
<tr class="odd">
<td><div class="note_title">
<p><strong>Archived Page</strong></p>
</div>
<div class="note_content">
<p>Page was moved to the <a href="https://developer.blender.org/project/manage/12/">UI project landing page.</a></p>
</div></td>
</tr>
</tbody>
</table>
