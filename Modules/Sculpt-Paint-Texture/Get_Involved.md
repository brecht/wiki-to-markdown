# Sculpt, Paint & Texture: Get Involved

There are plenty of community tasks and bugs to help out with on the
[Module Workboard](https://developer.blender.org/project/board/27/). If
nobody is assigned to it, it's free to pick up. If you'd like to get in
touch or have any questions, tag the module coordinators Julien Kaspar
and Joe Eagar in the [Module
Chat](https://blender.chat/channel/sculpt-paint-texture-module).

### Getting Code Review

Before submitting a patch, read through [Contributing
Code](../../Process/Contributing_Code/index.md).

When a patch is to be reviewed by the Sculpt, Paint & Texture module, do
the following:

  - Make sure the patch description follows the **[Ingredients of a
    Patch](../../Process/Contributing_Code/index.md#Ingredients_of_a_Patch)**
    guideline.
  - Add developers and artists from the Sculpt, Paint & Texture module
    as reviewers. Be sure to pick at least one developer (when in doubt,
    add Joe) as **blocking reviewer**. That way artists are free to
    approve any changes in functionality and user interface, without the
    patch immediately going to "Approved" state.
  - **Tag** the patch with the 'Sculpt, Paint & Texture' project. This
    can only be done after the patch has been created, so you'll have to
    edit it after creation.

### Contributing Art & Documentation

If code is not your goal, consider contributing in other ways. Every new
feature needs art as a showcase.

''' Artworks & Demo Files: '''

For the [Release Notes](https://www.blender.org/download/releases/) and
[Demo Files](https://www.blender.org/download/demo-files/). Include your
visuals in a new Blender release for others to see what Blender can do.

''' User Manual ''' Visual material, additions and fixes to the
[manual](https://docs.blender.org/manual/en/dev/sculpt_paint/sculpting/introduction/general.html)
to keep users and educators informed on Blenders functionality is also
important. While this is maintained by the module itself, it's a big
help to [submit
issues/bugs](https://developer.blender.org/maniphest/task/edit/form/default/?project=PHID-PROJ-c4nvvrxuczix2326vlti)
or even [edits to the
manual](https://developer.blender.org/project/view/53/) and submit those
changes.

Ideally these contributions should be a gift to the wider community.
That's why shared art & files are licensed as Creative Commons (Ideally
[CC0](https://creativecommons.org/share-your-work/public-domain/cc0/) or
[CC-BY](https://creativecommons.org/licenses/by/4.0/))

''' Educational Content & Showcases '''
[Tutorials](https://www.youtube.com/playlist?list=PLa1F2ddGya_-UvuAqHAksYnB0qL9yWDO6)
& [feature showcases](https://www.youtube.com/watch?v=0zaG13Do12Q) via
your own or the [official Blender
channel](https://www.youtube.com/@BlenderOfficial) are also welcome and
help even more to communicate the use of Blenders features and
capabilities. Reach out to us any time.

### Testing & Feedback

New features and fixes are added frequently to the [latest Alpha
version](https://builder.blender.org/download/daily/) (\`master\`). Any
contributor and user is welcome to give feedback via the
[sculpt-paint-texture
chat](https://blender.chat/channel/sculpt-paint-texture) and [report
bugs](https://wiki.blender.org/wiki/Process/Bug_Reports). This is
invaluable for polishing features for the next release.

Certain prototypes and early patches can also be found in the
\`sculpt-dev\` branch at the [built download
site](https://builder.blender.org/download/daily/). Expect the branch to
be unstable and subject to frequent big changes.
