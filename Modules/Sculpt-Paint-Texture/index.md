Notes and documentation about the Sculpt-Paint & Texture module.

### Subpages

  - [Future
    Projects](Future_Projects.md):
    Bigger projects and opportunities to work on next, or define
    further.
  - [Code Documentation](../../Source/index.md#Sculpt.2C_Paint.2C_Texture):
    Get started with the source code of the module.
  - [Get
    Involved](Get_Involved.md):
    Info on contributing with code patches, documentation, education
    content & feedback.
  - [Weak Areas](Modules/Sculpt-Paint-Texture/Weak_Areas):
    Known issues and problems that aren't currently in discussion.
