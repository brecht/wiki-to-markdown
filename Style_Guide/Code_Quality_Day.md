# Code Quality Day

The team will work together on making the Blender code easier to
understand, rather than focusing on bugs and other tasks. This will help
in the long term, making it easier to work on Blender for new and
experienced developers.

**What:** A full day dedicated to improve the Blender code quality.

**Who:** Blender Foundation developers and any other contributors.

**When:** The first Friday of every month.

**Where:** The coordination will happen in the [\#blender-coders channel
on blender.chat](https://blender.chat/channel/blender-coders).

### Example Tasks

  - [Proposed tasks (T73586)](https://developer.blender.org/T73586).
  - Add code comments, like explaining non-obvious code or giving
    high-level overviews.
  - Rename obscure functions and variables.
  - Rename poorly named structures and members in DNA (with aliasing to
    keep compatibility).
  - Rename data structures and functions for consistency.
  - Split up big files into more organized smaller ones.
  - Simplify and clarify code through refactoring.
  - Write or update wiki documentation with a high-level overview of
    modules and algorithms.

The idea is to do smaller tasks that can be completed, reviewed and
committed within a day.

### Tools

Refactoring tools can make operations like renaming quicker and safer.
We recommend using:

  - [VS Code
    Refactoring](https://code.visualstudio.com/docs/editor/refactoring)
  - [Example Python script for
    renaming](https://developer.blender.org/P1241)
