# Style Guide

<div class="bd-lead">

Here you can find a collection of conventions for various development
activities.

</div>

  - [C and C++ Style Guide](C_Cpp.md)
  - [C and C++ Best
    Practice](Best_Practice_C_Cpp.md)
  - [Python Style Guide](Python.md)
  - [GLSL Style Guide](GLSL.md)
  - [Commit Message Guidelines](Commit_Messages.md)
  - [Release Notes Writing
    Style](../Release_Notes/Writing_Style.md)

See also:

  - [IDE Configuration](Style_Guide/IDE_Configuration)
  - [Code Quality Day](Code_Quality_Day.md)
