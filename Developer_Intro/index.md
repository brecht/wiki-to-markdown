# New Developer Introduction

<div class="bd-lead">

Getting started with Blender development

</div>

  - [New Developer Overview](Overview.md)
  - [New Developer Advice](Advice.md)
  - [New Developer Environment](Environment/index.md)
  - [New Developer Commit Access](Committer.md)
  - [New Developer Resources](Resources.md)
  - [Good First
    Issue](https://projects.blender.org/blender/blender/issues?labels=302)
