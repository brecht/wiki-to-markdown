# For All Developers

You've been added as Blender project member with Git write access,
welcome\! You can use this access now to work on Blender. You're also
welcome to work on other features or fixes as discussed with the team
online. Please always verify with the module owners listed [on this wiki
page](Process/Module_Owners/List), or with one of the
bf-blender admins (*Bastien Montagne, Brecht Van Lommel, Campbell
Barton, Dalai Felinto or Sergey Sharybin*) before committing. You can
also talk with us on one of the [blender.chat
channels](Contact) if in doubt.

Here's the standard checklist & guidelines all new devs need to read:

1.  Always ensure that what you commit is either on your "own code", or
    you've verified the patch with the owner of the module. If you want
    to change things in other parts of the code, check with one of the
    developers who owns/maintains that.  
      
2.  Always carefully check which files you have included in your local
    Git commit before pushing it to the main repository. Use 'git
    status', 'git show' and similar commands to inspect your commit.
    Check carefully if the files marked as modified are actually what
    you want to commit. Also remember to pull in recent changes to
    ensure your own changes are still working.  
    If needed, use 'git rebase --interactive' to reorder, edit, fixup
    (join), or split the **not yet pushed** commits so they keep a
    comprehensible history and do not break 'git bisect' or other tools
    like automatic builds.  
      
3.  Verify that the licensing and copyright information is correct in
    files you commit (especially for new files).  
      
4.  We have adopted [Clang Format](../Tools/ClangFormat.md) as the
    styling tool. You should ensure your coding environment is set up
    for this tool. Before committing or contributing code ensure the
    tools has been run on the files you modify.  
      
5.  Blender is strictly cross-platform and we only accept code that
    compiles correctly for all current platforms (see currently
    supported compiler versions
    [here](../Building_Blender/index.md#Compiler_Versions)). For OpenGL
    (nothing older than 3.3, or correctly wrapped), Python (3.7), etc.
    If in doubt, ask on blender.chat.  
      
6.  if you think your commit may cause errors on others configurations,
    note this in your commit log, or better yet: go to
    [\#blender-coders](https://blender.chat/channel/blender-coders) on
    blender.chat.  
      
7.  Document new features and changes in the [release
    notes](../Release_Notes/index.md), either before or
    directly after committing a feature.  
      
8.  Blender has stages in its development cycle called BCon levels.
    Patches may be delayed or refused (even if the patch is acceptable)
    if a new Blender version is being prepared for release. To avoid
    this, make sure to check on the current BCon stage. ([Release Cycle
    Docs](../Process/Release_Cycle.md))

# Best Practice

## Etiquette

  - When making very large changes to blender's source, keep yourself
    available for some time after committing them (1-3 hrs) in case your
    changes break blender in some way.
  - Don't make large changes just before a release  
    See: [Release Cycle Docs](../Process/Release_Cycle.md)
  - Developers may reply to your commit logs. Be sure to reply comments
    on your commit.
  - When committing contributions from others, make sure the log
    includes their name and task / pull request number.

## Raising Concerns

As a developer you may raise concerns with a commit, or someone may
raise concerns with changes you have made.

Concerns should not be raised for issues already reported as bugs,
instead, link to the commit from the bug report is sufficient. Assign
the committer to the bug report (and add the patch creator as a
subscriber).

## Code Style

### C/C++

There's [Code Style Guidelines](Source/Code_Style) which
should be followed when working on source code. Note that much of this
has been superseded by [Clang Format](../Tools/ClangFormat.md).
Naming conventions from the [Code Style
Guidelines](Source/Code_Style) are still applicable.

Some areas of the code haven't been ported to these code guidelines and
if this is the case, the existing style should be followed when working
on them. Do not mix feature implementations / bugfixes with a style
cleanup.

### Python

For python we follow pep8 where possible.

See: [Best Practice - Style
Conventions](https://docs.blender.org/api/master/info_best_practice.html#style-conventions)

## Commit (Best Practice)

  - **Always** double check your commits before pushing  
    *(just as a quick verification to make sure other changes aren't
    accidentally being included)*.
  - Make sure you're using SSH to push commits, as described in
    [Tools/Git\#Commit\_Access](../Tools/Git.md#Commit_Access).
  - As mentioned under the C/C++ section, do not combine code
    cleanup/refactoring commits with functional changes, since this
    makes it harder to investigate when a commit causes new bugs.
  - Avoid having trailing whitespace in new code.
  - Store test files in:
    <https://svn.blender.org/svnroot/bf-blender/trunk/lib/tests/>
  - Observe the [commit message
    guidelines](../Style_Guide/Commit_Messages.md).

## Branch Usage

Committers can create their own branches, this section notes some
guidelines.

  - Avoid cryptic names or wordplay, branches should be named clearly,
    relating to their purpose.  
    **Good**: \`cycles\_ctests\`, \`ui-api-refactor\`,
    \`temp-mathutils-doc\`, \`particles\_refactor\`  
    **Bad**: \`test\`, \`experiment\`, \`terrible\_consequencer\`.
  - If you intend to make a short-lived branch for some development
    purpose, use the prefix **temp-**.  
    This lets others know it is a branch which will eventually be
    deleted.  
    *We sometimes use these to more easily collaborate on a patch before
    going into master*.
  - Even though you have a lot of freedom to make changes in your own
    branch, please avoid committing **large binary files**, since this
    increases the size of Blender's repo for everyone.  
    This includes changes to the splash image, startup file, fonts...
    etc. Or anything which isn't essential to your development.
  - If any branches you create no longer serves a useful purpose *(such
    as branches that have been abandoned entirely or branches that were
    rebased into master and don't have a history worth keeping),* you
    can delete them:  
    ``` bash
    git push origin --delete <branchName>
    ```
  - If you need to rebase or force update your branch, use the [staging
    repository](https://developer.blender.org/project/view/56/) instead.
  - Developers can create
    [builds](https://wiki.blender.org/wiki/Infrastructure/BuildBot#Branch_Builds)
    for users to provide early feedback.
