## Starting out

  - This document assumes you have built blender using the
    [Building\_Blender/Windows](../../Building_Blender/Windows.md).

## Project File

TODO

## Launching Blender

TODO / WIP

  - set start project
  - ensure debug settings
  - start debug process

or

  - start blender from command-line, shortcut link or file explorer
  - attach Visual Studio to Blender process

## Configuring The Editor

See:
[Tools/ClangFormat\#IDE\_Integration](../../Tools/ClangFormat.md#IDE_Integration)
