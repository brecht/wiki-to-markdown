# Development Environment Setup

This document is split into 2 sections, one for C/C++ another for
python, since you may be using one of the other and they both have very
different requirements.

## C/C++

If you already are familiar with a C/C++ development environment, most
likely you can use it for blender development.

Whichever environment you choose, make sure you *also* set up [Clang
Format](../../Tools/ClangFormat.md) for your environment. This is the
required method for code formatting.

Otherwise, if you have no strong preference - its safe to choose the
default IDE's on each platform.

  - OSX - XCode
  - Microsoft Windows - Microsoft Visual Studio (MSVC)
  - Linux - *(no default IDE but there are popular ones you can try)*

### IDE Quick-Start Links

Once you have blender building you may want to develop one of the many
IDE's (or editors) that can be used with blender.

Since there are too many OS/IDE combinations, maintaining a
comprehensive list of all would be difficult, so here is a list of
documented configurations known to work well.

  - [Linux / CMake /
    QtCreator](Linux_CMake_QtCreator.md)
  - [Linux / CMake /
    NetBeans](Linux_CMake_NetBeans.md)
  - [Linux / CMake /
    Eclipse](Linux_CMake_Eclipse.md)
  - [Linux / CMake /
    KDevelop](Linux_CMake_KDevelop.md)
  - [OSX / CMake /
    Xcode](OSX_CMake_XCode.md)

<!-- end list -->

  - [Windows / CMake /
    MSVC2017/MSVC2019](../../Building_Blender/Windows.md)

<!-- end list -->

  - [Portable / CMake /
    VSCode](Portable_CMake_VSCode.md)
    (Recommended for Linux/OSX only, \*NOT\* recommended for novice
    users on windows)

note:

  - These docs assume you have blender already building.
  - Some of these pages can be easily used between different operating
    systems.

## Python

While python scripts can be developed entirely inside Blender, the
built-in text editor is limited. Its convenient for short scripts or
making minor edits but for large projects you will want to use an
external editor.

With python development the choice of IDE/Editor is less of an issue as
with C/C++, most developers just use their favorite editor.

Editors known to work well: scite, geany, notepad++, gedit and eric5.

If you're already familiar with vim or emacs they are fine too.

For more information on how to execute scripts externally see: [Using an
External
Editor](https://docs.blender.org/api/current/info_tips_and_tricks.html#use-an-external-editor)
