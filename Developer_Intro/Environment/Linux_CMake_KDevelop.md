KDevelop supports loading blender projects directly by opening
\`CMakeLists.txt\`, so this document mainly covers configuration.

## Loading the project file

  - Project (Menu) -\> Open / Import Project
  - Navigate to the \`blender\` directory and Select \`CMakeLists.txt\`
  - ''Accept defaults for **Project Information** ''

Now the project is loaded, you can run:

  - Project (Menu) -\> Build Selection

## Configuring Runtime Options

### Launching Blender

  - Run (menu) -\> Configure Launchers
  - Select 'blender' and press the "+" icon.  
    *A new options box will appear*
  - Under 'Executable', set the 'Project Target' to 'blender' (there
    will be makesdna and makesrna as alternatives)
  - Under 'Behavior' set the working directory to the source directory
    (where \`CMakeLists.txt\` is located)
  - Under 'Dependencies' set the 'Action' to 'Build and Install'

### Application Preferences

  - Settings (menu) -\> Configure KDevelop
      - *Background Parser*
          - Set 'Maximum Number of Threads' to the number of
            processors/cores you have  
            *This makes a BIG difference to initial parsing time on
            load\!*

## Configuring The Editor

See:
[Source/Code\_Style/Configuration\#KDevelop](Source/Code_Style/Configuration#KDevelop)
