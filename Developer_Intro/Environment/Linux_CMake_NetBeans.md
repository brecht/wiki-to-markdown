## Starting out

  - This document assumes you have built blender using the [Linux/CMake
    Quickstart](Building_Blender/Linux/Ubuntu/CMake).
  - Tested with NetBeans 7.4

<!-- end list -->

  - Blender uses a lot of memory in netbeans, for much faster parsing
    start netbeans with the argument: **-J-Xmx1024m**
  - For smooth text display you can use this argument:
    **-J-Dawt.useSystemAAFontSettings=lcd**
  - *You may need to enable the C/C++ plug-in from within NetBeans (it
    may not be on by default)*
      - Tools (Menu) -\> Plugins
      - Settings (Tab) -\> check Netbeans Distribution
      - Available Plugins (Tab) -\> check C/C++ and install it

## Project File

To create a project file for netbeans run

`python3 BLENDER_SOURCE/build_files/cmake/cmake_netbeans_project.py BLENDER_BUILD`

... where **BLENDER\_SOURCE** Is blenders source directory and
**BLENDER\_BUILD** is the cmake build directory containing
CMakeCache.txt

*you may want to use a bash alias or shell script for this command.*

To load the project file:

  - File (Menu) -\> Open Project
  - Navigate to the CMake Build directory, double clicking on it will
    load it.  
    *When loading netbeans in the future this project will be available
    in the welcome screen*

<!-- end list -->

  - netbeans will ask if it should assosiate 'inl' with C/C++ header  
    Click yes.

<!-- end list -->

  - NOTE: For an unknown reason code assistance is not parsing Blender's
    code on loading the project file.  
    This can be worked around by right clicking on the 'Blender' project
    icon and selecting **Code Assistance -\> Reparse Project** (netbeans
    8.0.2, --[Ideasman42](../../User/Ideasman42/index.md) 00:08, 18 June
    2015 (UTC)).

## Launching Blender

  - File (menu) -\> Project Properties - Blender
      - *Run*
      - Set 'Run Directory' to your blender source-code path.
      - Disable 'Build First'

## Configuring The Editor

See:
[Source/Code\_Style/Configuration\#NetBeans](Source/Code_Style/Configuration#NetBeans)
